* Whenever a page uses the `$state->t`, update the `Vary:` header to consider `Accept-Language`.
  May even also consider monitoring Request::getHeader().

* Update REFERENCE.md with documentation for Table, Hooks and Controllers.

* Make it possible to define services by creating a file in `@CONFIG/services/$serviceId.php`.
  The PHP file should instantiate whatever service you need and return it. If the instance needs
  caching, that needs to be implemented inside the $serviceId function.
  Look into alternative implementations - for example by declaring a Fubber\Services\<ServiceId>::create()
  method, which would allow services to installable via composer easily.

* Move Meta API into Fubber Framework

* Make Fubber Framework work well with Swoole.
