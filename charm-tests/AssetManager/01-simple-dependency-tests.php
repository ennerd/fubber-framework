<?php

use Fubber\AssetManager\Asset;
use Fubber\AssetManager\Bucket;
use Fubber\AssetManager\RepositoryInterface;
use Fubber\AssetManager\BundleBuilder;
use Fubber\AssetManager\Dependency;
use Fubber\AssetManager\UnsatisfiableDependencyException;

require(__DIR__.'/../../vendor/autoload.php');

Fubber\Kernel::init();

$bucket = new Bucket();
$bucket->add(new Asset("jquery", "3.6.0", "jQuery 3.6.0"));
$bucket->add(new Asset('jquery', '3.6.0', 'jQuery 3.6.0'));
$bucket->add(new Asset('jquery', '1.6.0', 'jQuery 1.6.0'));
$bucket->add(new Asset('jquery-ui', '1.0.0', 'jQuery UI 1.0.0', 
    new Dependency('jquery', '^1.0.0'),
));
$bucket->add(new Asset('jquery-ui', '1.13.3', 'jQuery UI 1.13.3',
    new Dependency('jquery', '>=1.8.0 < 4.0.0')
));
$bucket->add(new Asset('jquery-bad-actor', '1.0.0', 'jQuery Bad Actor',
    new Dependency('jquery', '^1.0'),
    new Dependency('jquery-ui', '^1.0')
));
$bucket->add(new Asset('frode', '44.0.0', 'Frode as 44 year old',
    new Dependency('marianne', '*')
));
$bucket->add(new Asset('marianne', '45.0.0', 'Marianne as 45 year old'));
$bucket->add(new Asset('vilja', '14.0.0', 'Vilja as 14-year-old',
    new Dependency('frode', '>=29.0'),
    new Dependency('marianne', '>=29.0')
));
$bucket->add(Asset::create('julie', '24.0.0', 'Julie as 24-year-old')
    ->withDependencies(
        new Dependency('frode', '>=19.0'),
        new Dependency('marianne', '>=19.0')
    )
);
$bucket->add(new Asset('julie', '10.0.0', 'Julie as child',
    new Dependency('marianne', '>=29.0.0 <105.0.0'),
    new Dependency('frode', '>=29.0.0 <85.0.0')
));

function dumpBundle(BundleBuilder $builder) {
    foreach ($builder as $id => $asset) {
        echo " - asset $id: ".$asset."\n";
    }
}

echo "1 Requesting jquery-ui *\n";


$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-ui');
dumpBundle($bundleBuilder);


echo "2 Requesting jquery-ui <2\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-ui', '<2');
dumpBundle($bundleBuilder);

echo "3 Requesting jquery-ui >1\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-ui', '>1');
dumpBundle($bundleBuilder);

echo "4 Requesting jquery-ui <=1.13 >1.12\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-ui', '<=1.13 >1.12');
try {
    dumpBundle($bundleBuilder);
    die("OK");
} catch (UnsatisfiableDependencyException $e) {
    echo " - Error: " . get_class($e) . "\n";
}

echo "5 Requesting jquery-ui * AND jquery *\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-ui');
$bundleBuilder->add('jquery');
dumpBundle($bundleBuilder);

echo "6 Requesting jquery-ui ^1.0 AND jquery *\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-ui', '<=1.12');
$bundleBuilder->add('jquery', '^2.0');
try {
    dumpBundle($bundleBuilder);
} catch (UnsatisfiableDependencyException $e) {
    echo " - Error: " . get_class($e) . "\n";
}

echo "7 Requesting jquery-bad-actor *\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('jquery-bad-actor');
try {
    dumpBundle($bundleBuilder);
} catch (UnsatisfiableDependencyException $e) {
    echo " - Error: " . get_class($e) . "\n";
}

echo "8 Requesting vilja\n";
$bundleBuilder = new BundleBuilder($bucket);
$bundleBuilder->add('vilja');
dumpBundle($bundleBuilder);
