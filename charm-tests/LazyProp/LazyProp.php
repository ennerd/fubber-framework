<?php
require(__DIR__.'/../../vendor/autoload.php');

Fubber\Kernel::init();

class TestClass {
    use Fubber\LazyProp;
}

TestClass::declareGetter("only_getter", function() {
    echo "Getter only_getter=only_getter\n";
    return "only_getter";
});
TestClass::declareSetter("only_setter", function($value) {
    echo "Setter only_setter=$value\n";
});
TestClass::declareGetter("both", function() {
    echo "Getter both=both\n";
    return "both";
});
TestClass::declareSetter("both", function($value) {
    echo "Setter both=$value\n";
});

$instance = new TestClass();
$count = 0;
$instance->lazy("frode", function() {
    global $count;
    return $count++;
});
echo $instance->only_getter."\n";
echo $instance->both."\n";
$instance->only_setter = "only_setter";
$instance->both = "both";
echo $instance->frode."\n";
echo $instance->frode."\n";
