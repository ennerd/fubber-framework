<?php
namespace Fubber {
    /**
     * This file exists to help IDEs provide autocompletion when they are unable to resolve
     * aliased classes.
     */
    class Cache implements Caching\ICache {}
    class Table implements Table\ITable {}
    class TableSet extends Table\TableSet {}
    class Service extends Services\Service {}
    class EmptyTableSet extends Table\EmptyTableSet {}
    class Session extends Session\CacheSession {}
    class I18n extends I18n\I18n {}
    class Db extends Db\PdoDb {}
    class Logger extends Logging\MonologLogger {}
    class Template implements Templating\ITemplate {}
    class Mail implements Mailer\IMail {}
    class Mailer implements Mailer\IMailer {}
    class FlashMessage extends Util\FlashMessage {}
    class State extends Kernel\State {}
}
