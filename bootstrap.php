<?php
declare(strict_types=1);

require("vendor/autoload.php");

/**
 * This is an example index.php file for your project
 */
$kernel = Fubber\Kernel::serve();
