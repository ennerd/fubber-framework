# PHP tricks (and perhaps ideas for a PHP RFC)

## Vectors

Internally in PHP the array implementation has an optimization where if the array contains only numeric keys in sequence. The function `array_is_list()` can be used to check if this is true.

Vectors are different in a couple of ways:

 *  You can remove from the beginning or from the end of the vector in O(1) time.
 *  You can append items in O(1)
 *  Removing other items require O(n) time (copying all remaining offsets)
 *  Offsets always appear to start at offset 0.
 *  All items have the same type.

This is a VERY useful data structure for efficient algorithms and data structures.

Primitive types:

```php
$integers = int[]; // creates an empty vector of int
$floats = float[1.23, 3.50]; // creates a vector of 2 floats
$strings = string[];
$objects = object[];
```

More complex types:
```php
$objectsOfType = SomeInterface[];
// alternatively (possibly with fewer side effects)
$objectsOfType = object<SomeInterface>[];
```

## Abstract properties, and properties in interfaces

Given the new `readonly` type attributes, properties are much more useful to expose to consumers of an API.

This creates a need for abstract properties in abstract classes, and being able to declare properties in interfaces.

```php
interface SomeInterface {
    protected readonly string $name;
}
```

and 

```php
abstract class SomeClass {
    abstract protected readonly string $name;
}
```

There is no good reason to restrict this to readonly properties, unless one wants to force a particular programming paradigm on developers - which I resist.


## Traits with `extends` causes class `implements` 

It is a repeating desire to have a trait declare some requirement to the consuming class. This is partially solved by being able to declare abstract methods in traits, but abstract methods are simply two sides of the same coin - an undeclared method.

> Also see: *Abstract properties and properties in traits*

I propose the following:

### Trait extends a class or interface X

A class `UsingClass` uses a trait `Trait` that extends a class or interface identified by `X`.

The following test is performed:

```php
if (!(UsingClass instanceof X)) {
    throw new CompileError("The class is not an implementation of ".X::class);
}
```

### Trait extends another trait

Allowing a trait to extend another trait would possibly be more controversial because of the details involved in method aliases.

This would not be supported.


## Array concatenation

```php
$arr = [ 0, 1, 2 ];
// followed by
$arr[] = 3;
$arr[] = 4;
```

### Currently the best alternative

```php
$arr = [ 0, 1, 2 ];
// followed by
$arr = [ ...$arr, 3, 4 ];
```

This approach presumably triggers a full copy of the original array while destructuring.

This should probably be optimized by PHP, but it might have side-effects I am unaware of.


### Alternative; array concatenation

Currently this is supported:

```php
$array1 = $array1 + $array2;
```

This should be supported as well, and behave identically:

```php
$array1 += $array2;
```

