List of services provided
=========================

Interface                   | Instantiator                                      | Default Implementation
------------------------------------------------------------------------------------------------------
Fubber\Table\ITable         | extends Fubber\Table                              | Fubber\Table\Backends\DbTable
Fubber\Session\ISession     | Fubber\Session::getCurrent($state)                | Fubber\Session\CacheSession
Fubber\Cache\ICache         | Fubber\Cache::create();                           | Fubber\Cache\DbCache
Fubber\I18n\II18n           | Fubber\I18n::
Fubber\Db\IDb               | Fubber\Db::create();                              | Fubber\Db\PdoDb
Fubber\Logger\ILogger       | Fubber\Logger::create();                          | Fubber\Logger\MonologLogger
Fubber\Mailer\IMailer       | Fubber\Mailer::create();                          | Fubber\Mailer\SwiftMailer
Fubber\Template\ITemplate   | Fubber\Template::create($path, array $vars=[])    | Fubber\Template\Template
Collator                    | Fubber\Service::get(Collator::class)              | <native PHP implementation>
PDO                         | Fubber\Service::get(PDO::class)                   | <native PHP implementation>
Memcached                   | Fubber\Service::get(Memcached::class)             | <native PHP implementation>

