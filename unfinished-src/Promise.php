<?php
namespace Fubber;

class Promise {
    const PENDING = 'pending';
    const RESOLVED = 'resolved';
    const REJECTED = 'rejected';
    protected $_state = self::PENDING;
    protected $_result;
    protected $yes = [];
    protected $no = [];
    protected $finally = [];
    
    public function __construct($resolver) {
        $resolver = \Closure::fromCallable($resolver)->bindTo(null, null);
        $resolver(\Closure::fromCallable([$this, 'resolve']), \Closure::fromCallable([$this, 'reject']));
    }
    
    public function catch($onrejected) {
        return $this->then(null, $onrejected);
    }
    
    public function finally($onfinally) {
        $onfinally = \Closure::fromCallable($onfinnaly)->bindTo(null, null);
        if($this->_state !== self::PENDING) {
            self::defer($onfinally);
        } else {
            $this->finally[] = $onfinally;
        }
    }
    
    public function resolve($value) {
        if($this->_state !== self::PENDING)
            throw new \Exception("Promise already resolved");
        $this->_state = self::RESOLVED;
        $this->_result = $value;
        foreach($this->yes as $yes) {
            self::defer(function() use($yes){
                $yes($this->_result);
            });
        }
        foreach($this->finally as $onfinally) {
            self::defer($onfinally);
        }
    }
    
    public function reject($reason) {
        if($this->_state !== self::PENDING)
            throw new \Exception("Promise already resolved");

        $this->_state = self::REJECTED;
        $this->_result = $reason;
        foreach($this->no as $no) {
            self::defer(function() use($no){
                $no($this->_result);
            });
        }
        foreach($this->finally as $onfinally) {
            self::defer($onfinally);
        }
    }
    
    public function then($onfulfilled=null, $onrejected=null) {
        if($this->_state == self::RESOLVED && is_callable($onfulfilled)) {
            $onfulfilled = \Closure::fromCallable($onfulfilled)->bindTo(null, null);
            self::defer(function() use($onfulfilled) {
                $onfulfilled($this->_result);
            });
        } else if($this->_state == self::REJECTED && is_callable($onrejected)) {
            $onrejected = \Closure::fromCallable($onrejected)->bindTo(null, null);
            self::defer(function() use($onrejected) {
                $onrejected($this->_result);
            });
        } else {
            if(is_callable($onfulfilled))
                $this->yes[] = \Closure::fromCallable($onfulfilled)->bindTo(null, null);
            if(is_callable($onrejected))
                $this->no[] = \Closure::fromCallable($onrejected)->bindTo(null, null);
        }
    }
    
    protected static function defer($cb) {
        $loop = \Nerd\Glue::get(Loop\LoopInterface::class);
        $loop->futureTick($cb);
    }
    
    public function __toString() {
        return "[Error: Promises can't be cast to strings. Consider (yield \$promise).]";
    }
}