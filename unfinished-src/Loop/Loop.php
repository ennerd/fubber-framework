<?php
namespace Fubber\Loop;

/**
 * Fubber Framework Loop implementation.
 * 
 * High precision timer execution and low CPU load.
 * 
 * Future:
 * - Should accept
 */
class Loop implements LoopInterface {
    const DEFAULT_USLEEP = 10000;
    
    protected $running = false;
    protected $readStreams = [];
    protected $writeStreams = [];
    protected $microLock = 0;
    
    /**
     * Options passed via the constructor. Reference:
     * 
     *  [
     *      'ultraprecise' => false,                                            // If true, will use NOOP-loop when execution is nigh. Don't use this.
     *      'worker_pool' => null,                                              // Not implemented yet. It adjusts timings, because events may be added while
     *                                                                          // sleep waiting. Intention is that callbacks are handled by the first available
     *                                                                          // worker thread.
     *  ]
     */
    protected $options;

    /**
     * An array of all callables that will be invoked on the next tick
     */
    protected $queue = [];

    /**
     * A linked list pointing to the timer that will be run next
     */
    protected $nextTimer = null;
    
    public function __construct(array $options=[]) {
        $this->lockFile = tempnam(sys_get_temp_dir(), 'FubberLoopLock');
        unlink($this->lockFile);
        $this->options = $options + [
            'ultraprecise' => false,
            'worker_pool' => true,
            ];
    }
    
    
    /**
     * LoopInterface
     */

    /**
     * @see LoopInterface::futureTick()
     * @return null
     */
    public function futureTick($callable) {
        $this->queue[] = $callable;
    }

    /**
     * @see LoopInterface::addTimer()
     * @return TimerInterface
     */
    public function addTimer($interval, $callable) {
        $timer = new Timer($interval, $callable);
        $this->addTimerInstance($timer);
        return $timer;
    }

    /**
     * This is mostly equivalent to calling $timer->cancel() directly, except
     * that this will also remove the timer from the loops list of timers
     */
    public function cancelTimer(TimerInterface $timer) {
        $timer->cancel();
        $root = $this;
        while($root->nextTimer && $root->nextTimer !== $timer)
            $root = $root->nextTimer;
        if($root->nextTimer === $timer) {
            $root->nextTimer = $root->nextTimer->nextTimer;
        }
    }

    /**
     * @see LoopInterface::addPeriodicTimer()
     * @return TimerInterface
     */
    public function addPeriodicTimer($interval, $callable) {
        $timer = new Timer($interval, $callable, true);
        $this->addTimerInstance($timer);
        return $timer;
    }

    /**
     * @see LoopInterface::addReadStream()
     */
    public function addReadStream($stream, $callback) {
        
    }
    
    /**
     * @see LoopInterface::addWriteStream()
     */
    public function addWriteStream($stream, $callback) {
        
    }
    
    public function removeReadStream($stream) {
        
    }
    
    public function removeWriteStream($stream) {
        
    }

    /**
     * Stop the event loop immediately
     * 
     * @see LoopInterface::stop()
     * @return null
     */
    public function stop() {
        $this->running = false;
    }

    /**
     * Run the loop. You must add something to the loop before this function
     * has any effect. For example using $loop->futureTick(), $loop->addTimer()
     * 
     * @see LoopInterface::run()
     */
    public function run() {
        $this->running = true;
        while($this->running) {
            /**
             * Used to determine if anything happened on this iteration. If nothing
             * happened - we'll usleep some to avoid using 100% cpu.
             */
            $somethingHappened = false;
            
            /**
             * Add any timers to the queue. As long as getInvocationTime() returns 
             * a time in the past - it will be added. If the timer isPeriodic(), it
             * will be reinserted after this iteration.
             */
            foreach($this->getTimersDue() as $timer) {
                $this->futureTick($timer->getCallback());
                if($timer->isPeriodic()) {
                    $timer->scheduleNextInvocation();
                    $this->addTimerInstance($timer);
                }
            }
            
            /**
             * Pull callables from the queue
             */
            foreach($this->getTicks() as $callable) {
                if(!$this->running) break;
                $somethingHappened = true;
                $res = $callable();
                if(is_a($res, \Generator::class)) {
                    $this->resolveGenerator($res);
                }
            }

            // Check any read and write streams
/*
            $readStreams = [];
            $writeStreams = [];
            foreach($this->readStreams as $rs) {
                $readStreams[] = $rs['stream'];
            }
            foreach($this->writeStreams as $ws) {
                $writeStreams[] = $ws['stream'];
            }
*/            
            /**
             * Determine if the loop has any purpose, any reason to keep going,
             * iteration by iteration.
             */
            if((0 == sizeof($this->queue) + sizeof($this->readStreams) + sizeof($this->writeStreams)) && $this->nextTimer === null)
                break;
                
            /**
             * Consider adding a usleep here, to save CPU cycles.
             */
            if(!$somethingHappened) {
                $time = microtime(true);
                $nextTime = $this->nextTimer ? $this->nextTimer->getInvocationTime() : $time + (static::DEFAULT_USLEEP / 1000000);
                $delay = $nextTime - $time;
                // When the delay is too short, we reduce the sleep time to 1 millisecond
                if($this->options['worker_pool']) {
                    // The purpose of this maximum delay is to make sure it doesn't take
                    // too much time before events are executed PROVIDED 
                    $delay = min($delay, 0.01);
                }

                if($delay > 0.025) {
                    // Don't sleep too much. Better safe than sorry
                    $sleepTime = ($delay * 1000000) - 25000;
                    if($sleepTime > 0)
                        usleep($sleepTime);
                    else
                        usleep(1000);
                } else {
                    if($this->options['ultraprecise']) {
                        if($delay > 0.005) {
                            usleep(4000);
                        } else if($delay > 0.002) {
                            usleep(1000);
                        }
                        // No delay, hard loop. Should last at most 1 millisecond.
                    } else {
                        if($delay > 0.05)
                            usleep(40000);
                        else if($delay > 0.02)
                            usleep(8000);
                        else if($delay > 0.005)
                            usleep(4000);
                        else
                            usleep(1000);
                    }
                }
            }
        }
        $this->running = false;
    }

    /**
     * Returns the ticks
     */
    protected function getTicks() {
        if(!isset($this->queue[0]))
            return;
        
        // I'm assuming that this is thread safe
        $lock = null;
        while(false == ($lock = fopen($this->lockFile, 'x'))) {
            usleep(mt_rand(1,10));
        }

        $queue = $this->queue;
        $this->queue = [];

        unlink($this->lockFile);
        fclose($lock);

        foreach($queue as &$callable)
            yield($callable);
    }

    /**
     * Inserts a timer instance at the right place in the queue.
     * 
     * @return null
     */
    protected function addTimerInstance(TimerInterface $timer) {
        $invocationTime = $timer->getInvocationTime();
        $root = $this;
        while($root->nextTimer && $root->nextTimer->getInvocationTime() < $invocationTime) {
            $root = $root->nextTimer;
        }
        // $timer should adopt the current $root->nextTimer
        $timer->nextTimer = $root->nextTimer;
        // $root->nextTimer is now $timer
        $root->nextTimer = $timer;
    }
    
    /**
     * Returns an iterable list of timers. Periodic timers are reinserted automatically.
     * 
     * @return \Generator<TimerInterface>|null
     */
    protected function getTimersDue() {
        $timersToReinsert = [];
        while($this->nextTimer && $this->nextTimer->getInvocationTime() <= microtime(true)) {
            $timer = $this->nextTimer;
            $this->nextTimer = $timer->nextTimer;
            if($timer->isPeriodic()) {
                $timersToReinsert[] = $timer;
            }
            yield($timer);
        }
        foreach($timersToReinsert as $timer) {
            $this->addTimerInstance($timer);
        }
    }

    /**
     * Generators are resolved by sending any yielded results back. If a yielded
     * result seems to be a promise (i.e. it has a 'then' method), we add a listener
     * to the promise. Once the promise resolves, we'll add to futureTick a callback
     * that resumes the generator with the promise result.
     */
    protected function resolveGenerator($generator) {
        $self = $this;
        while($generator->valid()) {
            $res = $generator->current();
            if(is_a($res, \Generator::class)) {
                $res = $this->resolveGenerator($res);
                throw new \Exception("NOT IMPLEMENTED, OR TESTED AT LEAST!");
            } else if(is_callable([$res, 'then'])) {
                // Handles most types of promises.
                $yes = function($result) use ($self, $generator) {
                    $self->futureTick(function() use($self, $result, $generator) {
                        $generator->send($result);
                        $self->resolveGenerator($generator);
                    });
                };
                $no = function($reason) use($self, $generator) {
                    $self->futureTick(function() use($self, $result, $generator) {
                        if(is_a($reason, \Throwable::class)) {
                            $generator->throw($reason);
                        } else {
                            $generator->send(null);
                        }
                        $self->resolveGenerator($generator);
                    }); 
                };
                $res->then($yes, $no);
                return;
            } else {
                $generator->send($res);
            }
        }
    }
}