<?php
namespace Fubber\Loop;

/**
 * Interface is heavily "inspired" by React PHP. See that for documentation.
 */
interface LoopInterface {
    public function addReadStream($stream, $callable);
    public function addWriteStream($stream, $callable);
    public function removeReadStream($stream);
    public function removeWriteStream($stream);
    public function addTimer($interval, $callable);
    public function addPeriodicTimer($interval, $callable);
    public function cancelTimer(TimerInterface $timer);
    public function futureTick($callable);
    public function run();
    public function stop();
}