<?php
namespace Fubber\Loop;

/**
 * Interface is heavily "inspired" by React PHP. See that for documentation.
 */
interface TimerInterface {
    /**
     * @return float
     */
    public function getInterval();
    
    /**
     * @return \Closure
     */
    public function getCallback();
    
    /**
     * @return bool
     */
    public function isPeriodic();
    
    /**
     * Missing from the React PHP interface. 
     * 
     * Returns a microtime value for when the event should be invoked next. If
     * the returned value is <= microtime(true) - enqueue this callback. The method
     * will internally update event time of the timer is periodic.
     * 
     * @param $dontUpdate If true, the internal invocation time will not be updated
     * @return float
     */
    public function getInvocationTime();
    
    /**
     * Missing from the React PHP interface.
     * 
     * Increases the next invocation time for this timer by the interval set.
     */
    public function scheduleNextInvocation();
    
    /**
     * Missing from the React PHP interface.
     * 
     * The timer will be marked as cancelled and removed from the event queue
     * 
     * @return null
     */
    public function cancel();
    
    /**
     * Missing from the React PHP interface
     * 
     * Returns true if the timer has been cancelled
     * 
     * @return bool
     */
    public function isCancelled();
}