<?php
namespace Fubber\Loop;

class Timer implements TimerInterface {
    protected $invocationTime;
    protected $interval;
    protected $callback;
    protected $periodic;
    protected $cancelled = false;
    
    public function __construct($interval, $callable, $periodic=false) {
        $this->interval = $interval;
        $this->callback = \Closure::fromCallable($callable);
        $this->periodic = $periodic;
        // Schedule first invocation time
        $this->invocationTime = microtime(true) + $interval;
    }
    
    public function getInterval() {
        return $this->interval;
    }
    
    public function getCallback() {
        return $this->callback;
    }
    
    public function isPeriodic() {
        return $this->periodic;
    }
    
    public function getInvocationTime() {
        return $this->invocationTime;
    }
    
    public function scheduleNextInvocation() {
        $this->invocationTime += $this->interval;
    }
    
    public function cancel() {
        $this->cancelled = true;
    }
    
    public function isCancelled() {
        return $this->cancelled;
    }
}