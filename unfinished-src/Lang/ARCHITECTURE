The Architecture of the FubberLang scripting language is inspired by PHP and Javascript in several ways:

- class VM represents the entire script file, and has a definition of every opcode that exists.
    - Has a $program[] array, where each item represents an opcode or a constant value.
    - Maintains a list of Thread instances. Each Thread represents a separate execution context, and threads don't share memory.
    
- class VM\Thread represents one execution of the script.
    - Maintains the programCounter
    - Holds a reference to the current Context, in which code is evaluated

- class VM\Context represents an execution context.
    - The context has a $parent property. If $parent is null, then we're running in the global scope.
    - The context has a $vars array property, which holds all variables declared in the current execution context. It initially contains $this, and any arguments passed to the function.
    - ArrayAccess interface provides access to all the variables that are defined and accessible according to Javascript rules:
        - Getting a value: we check the current context, then traverse parents and if no value is found - you receive a new TUndefined();
        - Setting a value: we check the current context, then traverse parents to find where the value was declared. If it is not declared, it is placed in the global context.
        - Deleting a value: we check the current context, then traverse parents to find where the value was declared. Unless we are in the global context, the variable is deleted.
        
    
    
    
NOTES ABOUT ECMASCRIPT:

https://www.ecma-international.org/ecma-262/5.1/#sec-4.2

- Primitive values: Undefined, Null, Boolean, Number, String, Object
- Objects may be callable, and is then called a Function
- An Object is a collection of properties.
- Each Object property may have attributes (Writable)
- The following objects must exist when execution starts, declared on 'this' in the topmost execution frame:
    - 'this', which is also called the "Global object"
    - 'Object' object.
    - 'Function' object
    - 'Array' object
    - 'String' object
    - 'Boolean' object
    - 'Number' object
    - 'Math' object
    - 'Date' object
    - 'RegExp' object
    - 'JSON' object
    - 'Error' object
    - 'EvalError' object
    - 'RangeError' object
    - 'ReferenceError' object
    - 'SyntaxError' object
    - 'TypeError' object
    - 'URIError' object
