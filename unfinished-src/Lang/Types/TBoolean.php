<?php
namespace Fubber\Lang\Types;

class TBoolean extends TPrimitive {

    public function __construct($value) {
        $this->value = !!$value;
    }
    
    public function isTrue() {
        return $this->value;
    }
    
    public function toNumber() {
        return new TNumber(NAN);
    }
    
    public function toString() {
        return new TString($this->value ? 'true' : 'false');
    }

    /**
     * See https://www.ecma-international.org/ecma-262/5.1/#sec-9.9
     */
    public function toObject() {
        throw new Fubber\Lang\Exceptions\TypeErrorException("Can't cast Undefined to Object");
    }

    public function __toString() {
        return 'TBoolean<'.($this->value ? 'true' : 'false').'>';
    }


}