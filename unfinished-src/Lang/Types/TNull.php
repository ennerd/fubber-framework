<?php
namespace Fubber\Lang\Types;

class TNull extends TPrimitive implements \JsonSerializable {

    public function __construct() {
        $this->value = null;
    }
    
    public function isTrue() {
        return false;
    }
    
    public function toObject() {
        throw new Fubber\Lang\Exceptions\TypeErrorException("Can't cast Undefined to Object");
    }

    public function toNumber() {
        return new TNumber(NAN);
    }
    
    public function toString() {
        return new TString('null');
    }

    public function __toString() {
        return 'TNull';
    }
    
    public function jsonSerialize() {
        return null;
    }

}