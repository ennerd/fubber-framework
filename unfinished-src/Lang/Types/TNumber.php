<?php
namespace Fubber\Lang\Types;

class TNumber extends TPrimitive {

    public function __construct($number) {
        $this->value = floatval($number);
    }

    public function toPropertyName() {
        if($this->isFinite()) return $this->value;
        else return $this->toString()->toPropertyName();
    }    

    public function isFinite() {
        return is_finite($this->value);
    }
    
    public function isNaN() {
        return $this->value === NAN;
    }
    
    public function isTrue() {
        return $this->value != 0;
    }
    
    public function toNumber() {
        return new static($this->value);
    }
    
    public function toObject() {
        die("CANT CAST NUMBER TO OBJECT YET");
    }
    
    public function toString() {
        if(is_finite($this->value))
            return new TString($this->value);
        else if(is_nan($this->value))
            return new TString('NaN');
        else if(is_infinite($this->value) && $this->value > 0)
            return new TString('Infinity');
        else
            return new TString('-Infinity');
        
    }
    
    /**
     * In chrome, properties added to a number is discarded
     */
    public function offsetSet($name, $value) {
        
    }

}