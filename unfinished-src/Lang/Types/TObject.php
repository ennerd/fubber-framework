<?php
namespace Fubber\Lang\Types;

class TObject extends TPrimitive implements \ArrayAccess {

    public $properties;
        
    public function __construct($properties=[]) {
        if(!is_array($properties)) {
            $string = strval($properties);
            $l = mb_strlen($string);
            $properties = [];
            for($i = 0; $i < $l; $i++)
                $properties[] = mb_substr($string, $i, 1);
        }
        $this->properties = $properties;
    }

    public function isTrue() {
        return true;
    }
    
    public function toNumber() {
        return new TNumber(NAN);
    }
    
    public function toString() {
        return new TString('[object Object]');
    }
    
    public function jsonSerialize() {
        return $this->properties;
    }

    public function toObject() {
        return new TObject($this->properties);
    }

    public function &offsetGet($name) {
        // NOTE: Should support getters
        if(!isset($this->properties[$name])) {
            $null = new TUndefined();
            return $null;
        }
        return $this->properties[$name];
    }
    
    /**
     * Set a property
     */
    public function offsetSet($name, $value) {
        // NOTE: Should support setters
        $this->properties[$name] = $value;
    }
    
    /**
     * Check if a property is declared
     */
    public function offsetExists($name) {
        return isset($this->properties[$name]);
    }
    
    /**
     * Remove a property
     */
    public function offsetUnset($name) {
        unset($this->properties[$name]);
        return true;
    }

}