<?php
namespace Fubber\Lang\Types;

class TString extends TPrimitive {

    public function __construct($string) {
        $this->value = $string.'';
    }
    
    

    public function isTrue() {
        return $this->value !== '';
    }

    public function toObject() {
        die("CANT CAST STRING TO OBJECT YET");
    }

    public function toNumber() {
        if(is_numeric($this->value))
            return new TNumber(floatval($this->value));
        else
            return new TNumber(NAN);
    }
    
    public function toString() {
        return new TString($this->value);
    }

    public function &offsetGet($name) {
        $null = new TUndefined();
        return $null;
    }

    /**
     * Can't set properties on primitive type TString
     */
    public function offsetSet($name, $value) {
    }
    
    
}