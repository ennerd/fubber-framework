<?php
namespace Fubber\Lang\Types;
use Fubber\Lang\Exceptions\TypeErrorException;

abstract class TPrimitive implements \ArrayAccess, \JsonSerializable {

    public function __toString() {
        $cn = explode('\\', static::class);
        $cn = array_pop($cn);
        return $cn.'<'.json_encode($this).'>';
    }
    
    abstract public function isTrue();
    abstract public function toNumber();
    abstract public function toString();
    abstract public function toObject();
    public function toPropertyName() {
        return $this->toString()->value;
    }
    public function jsonSerialize() {
        return $this->value;
    }
    public function toBoolean() {
        return new TBoolean($this->isTrue());
    }

    public function &offsetGet($name) {
        throw new TypeErrorException("Cannot read property '$name'");
    }

    public function offsetSet($name, $value) {
        throw new TypeErrorException("Cannot set property '$name'");
    }

    public function offsetExists($name) {
        throw new TypeErrorException("Cannot read property '$name'");
    }
    
    public function offsetUnset($name) {
        throw new TypeErrorException("Cannot convert undefined or null to object");
    }

    public static function fromVar($var) {
        if(is_string($var))
            return new TString($var);
        else if(is_int($var) || is_float($var))
            return new TNumber($var);
        else if($var === null)
            return new TNull();
        else if($var === false || $var === true)
            return new TBoolean($var);
        else
            throw new TypeErrorException('Unable to import type "'.gettype($var).'"');
    }

}