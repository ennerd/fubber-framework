<?php
namespace Fubber\Lang\Types;

class TUndefined extends TPrimitive {

    public function __construct() {
        $this->value = null;
    }
    
    public function isTrue() {
        return false;
    }
    
    public function toNumber() {
        return new TNumber(NAN);
    }
    
    public function toString() {
        return new TString('undefined');
    }
    
    public function toObject() {
        throw new Fubber\Lang\Exceptions\TypeErrorException("Can't cast Undefined to Object");
    }

    public function __toString() {
        return 'TUndefined';
    }


}