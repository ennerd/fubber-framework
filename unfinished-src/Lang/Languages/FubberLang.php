<?php
namespace Fubber\Lang\Languages;
use Fubber\Lang\Compiler\Lexer;

class FubberLang {
    
    public function compile($script) {
        $lexer = $this->getLexer();
    }
    
    public function getLexer() {
        return new Lexer($this);
    }
    
    public function getLexerOptions() {
        return [
            'whitespace' => false,
            ];
    }
    
    public function getTokenDefinitions() {
        return [
            // matching brackets
            Lexer::T_BRACKET => '(){}[]',
            Lexer::T_OPERATOR => [
                // Array of symbols that are treated as operators.
                [Lexer::A_NONE, 'new', 'var',], // PHP also has 'clone'
                [Lexer::A_LEFT, '['],
                [Lexer::A_RIGHT, '**'],
                [Lexer::A_RIGHT, '++', '--', '~', '(number)', '(string)', '(object)', '(bool)', '@',], // PHP also has '(array)', '(int)' 
                // [Lexer::A_NONE, 'instanceof'],
                [Lexer::A_RIGHT, '!',],
                [Lexer::A_LEFT, '*', '/', '%', ],
                [Lexer::A_LEFT, '+', '-', ], // PHP also has '.'.
                [Lexer::A_LEFT, '<<', '>>', ],
                [Lexer::A_NONE, '<', '<=', '>', '>=', ],
                [Lexer::A_NONE, '==', '!=', '===', '!==', '<>',], // PHP also has '<=>',
                [Lexer::A_LEFT, '&', ],
                [Lexer::A_LEFT, '^', ],
                [Lexer::A_LEFT, '|', ],
                [Lexer::A_LEFT, '&&', ],
                [Lexer::A_LEFT, '||', ],
                [Lexer::A_RIGHT, '??' ],
                [Lexer::A_LEFT, '?:', ],
                [Lexer::A_RIGHT, '=', '+=', '-=', '*=', '**=', '/=', '%=', '|=', '^=', '<<=', '>>=', ],
                [Lexer::A_LEFT, 'and' ],
                [Lexer::A_LEFT, 'xor', ],
                [Lexer::A_LEFT, 'or', ],
                [Lexer::A_LEFT, ',' ],
                ],
            
            // The characters that form a whitespace
            Lexer::T_WHITESPACE => " \t\n\r",
                
            Lexer::T_STRLIT => [
                ['open' => '"', 'close' => '"', 'escaped' => '\"'],
                ['open' => "'", 'close' => "'", 'escaped' => "\\'"],
                ],
    
            // Define how comments are declared
            Lexer::T_COMMENT => [
                ['//', "\n"],
                ['/*', '*/'],
                ],
            ];        
    }
}