<?php
namespace Fubber\Lang;
use Fubber\Lang\VM\Thread;
use Fubber\Lang\Types\TString;
use Fubber\Lang\Types\TNumber;
use Fubber\Lang\Types\TNull;
use Fubber\Lang\Types\TBoolean;
use Fubber\Lang\Types\TObject;
use Fubber\Lang\Exceptions\SelfTestException;
use Fubber\Lang\Exceptions\InvalidOpcodeException;

class VM {
    
    public $program = [];
    public $opcodes = [];
    
    public function __construct() {
        $this->program = [
            'noop',
            new TNumber(1), 
            new TNumber(2), 
            '+',
            new TNumber(3),
            '===',
            'assert',
            new TNumber(1),
            new TNumber(2),
            '-',
            new TNumber(-1),
            '===',
            'assert',
            new TNumber(3),
            new TNumber(2),
            '*',
            new TNumber(6),
            '===',
            'assert',
            new TNumber(9),
            new TNumber(2),
            '/',
            new TNumber(4.5),
            '===',
            'assert',
            new TBoolean(false),
            '!',
            'assert',
            new TNumber(0),
            'bool',
            '!',
            'assert',
            new TString(''),
            'bool',
            '!',
            'assert',
            new TBoolean(true),
            'assert',
            new TNumber(0.1),
            'bool',
            'assert',
            new TString('0'),
            'bool',
            'assert',
            new TString('1'),
            'bool',
            'assert',
            new TNull(),
            'bool',
            '!',
            'assert',
            new TNull(),
            'bool',
            new TBoolean(false),
            '===',
            'assert',
            new TBoolean(false),
            new TBoolean(true),
            '!==',
            'assert',
            new TString('1'),
            new TNumber(1),
            '!==',
            'assert',
            new TNumber(1),
            new TNumber(2),
            'enq',
            new TNumber(1),
            '===',
            'assert',
            'deq',
            new TNumber(2),
            '===',
            'assert',
            'true',
            'false',
            '!==',
            'true',
            '===',
            'assert',
            new TNumber(0),
            '++',
            'false',
            'enq',
            'bool',
            'deq',
            '!==',
            'if',
            new TNumber(1),
            'jump',
            'exit',
            new TNumber(123),
            new TNumber(NAN),
            'dup',
            'var',
            'store',
            'object',
            new TString('mitt_objekt'),
            'store',
            'pop',
            'pop',
            new TObject('mitt_object'),
            'dup',
            new TString('frode'),
            new TNumber(1000000),
            'store_prop',
            new TString('frode'),
            'load_prop',
            ];
            
        $this->program = [
            new TObject(),
            'dup',
            new TString('frode'),
            new TNumber(1000000),
            'store_prop',
            new TString('objektet'),
            'store',
            ];
        
        $this->opcodes = [
            // pop count, push count, callback 
            'noop' => [0, 0, 'noop: Does nothing.', function() {}],
            'pop' => [1, 0, 'pop: Discard topmost value', function() { $this->context->pop(); }],
            '+' => [2, 1, 'Does push(pop + pop)', function() {
                $this->context->push($this->context->popNumber() + $this->context->popNumber());
            }],
            '/' => [2, 1, 'Does t=pop, push(pop / t)', function() {
                $t = $this->context->popNumber();
                $this->context->push($this->context->popNumber() / $t);
            }],
            'dup' => [1, 2, 'Duplicates the topmost operand', function() {
                $t = $this->context->pop();
                $this->context->push($t);
                $this->context->push($t);
            }],
/*
            '++' => [1, 1, 'Does push(pop + 1)', function() {
                $this->context->push($this->context->popNumber() + 1);
            }], 
            '--' => [1, 1, 'Does push(pop + 1)', function() {
                $this->context->push($this->context->popNumber() - 1);
            }], 
            '-' => [2, 1, 'Does t=pop, push(pop - t)', function() {
                $this->context->push(-$this->context->popNumber() + $this->context->popNumber());
            }],
            '*' => [2, 1, '[number] [number] * [value] push(pop * pop)', function() {
                $this->context->push($this->context->popNumber() * $this->context->popNumber());
            }],
*/
            '&&' => [2, 1, '[boolean] [boolean] && [boolean] Does t=pop, push(pop && t)', function() {
                $a = $this->context->popBoolean();
                $b = $this->context->popBoolean();
                $this->context->push($a && $b);
            }],
            '||' => [2, 1, '[boolean] [boolean] || [boolean]: Does push(pop || pop)', function() {
                $a = $this->context->popBoolean();
                $b = $this->context->popBoolean();
                $this->context->push($a || $b);
            }],
            '===' => [2, 1, '[value] [value] === [boolean]: Does push(pop === pop)', function() {
                $this->context->push($this->context->pop()->value === $this->context->pop()->value);
            }],
            '!==' => [2, 1, '[value] [value] != [boolean]: Does push(pop !== pop)', function() {
                $a = $this->context->pop();
                $b = $this->context->pop();
                $this->context->push($a->value !== $b->value);
            }],
            'assert' => [2, 1, '[value] assert: Halts execution if operand is not boolean true', function() {
                if(!$this->context->popBoolean())
                    throw new AssertionException("Assertion failed");
            }],
            '!' => [1, 1, '[value] ! [!value]: Does push(!pop)', function() {
                $this->context->push(!$this->context->popBoolean());
            }],
            'bool' => [1, 1, '[value] bool [boolean]: Converts the topmost operand to boolean', function() {
                $this->context->push($this->context->pop()->isTrue());
            }],
            'number' => [1, 1, '[value] number [number]: Converts the topmost operand to a number', function() {
                $this->context->push($this->context->pop()->toNumber());
            }],
            'string' => [1, 1, '[value] string [string]: Converts the topmost operand to a string', function() {
                $this->context->push($this->context->pop()->toString());
            }],
            'enq' => [1, 0, '[value] enq: Pop to back of operand queue', function() {
                $this->context->enqueue($this->context->pop());
            }],
            'deq' => [0, 1, 'deq [value]: Push from front of operand queue', function() {
                $this->context->push($this->context->dequeue());
            }],
            'jump' => [1, 0, '[offset] jump: Jump by adding offset to operand counter', function() {
                $this->programCounter += intval($this->context->popNumber());
            }],
            'if' => [1, 0, '[boolean] if: Skip next two opcodes if false. See "noop".', function() {
                if(!$this->context->popBoolean())
                    $this->programCounter += 2;
            }],
            'exit' => [0, 0, 'exit: Quit the program', function() {
                echo "exit called\n";
            }],
            'var' => [2, 1, '[name] store: Declare a variable in current context.', function() {
                $name = $this->context->pop();
                if(is_a($name, TNumber::class) && $name->isFinite()) {
                    $this->context->offsetCreate($name->value);
                } else {
                    $this->context->offsetCreate($name->toString()->value);
                }
            }],
            'store' => [2, 1, '[value] [name] store [value]: Store a value. It goes to the topmost context unless it was created with \'var\' first.', function() {
                $name = $this->context->pop();
                $value = $this->context->pop();
                $this->context->push($value);
                
                $this->context[$name->toPropertyName()] = $value;
            }],
            'load' => [1, 1, '[name] load [value]: Retrieve a variable.', function() {
                $name = $this->context->pop();
                $this->context->push($this->context[$name->toPropertyName()]);
            }],
            'load_prop' => [2, 1, '[object] [name] load_prop [value]: Retrieve the value of [object][name]', function() {
                $name = $this->context->pop();
                $object = $this->context->pop();
                $this->context->push($object[$name->toPropertyName()]);
            }],
            'store_prop' => [3, 0, '[object] [name] [value] store_prop: Set property [name] to [value] on [object]', function() {
                $value = $this->context->pop();
                $property = $this->context->pop()->toPropertyName();
                $object = $this->context->pop();
                $object[$property] = $value;
            }],
            'object' => [1, 1, 'object [object]: Declare an empty object.', function() {
                /**
                 * Notes; the simplest object has no constructor and is not ready to become a function or instantiated - but it has the ability to.
                 */
                $this->context->push(new TObject());
            }],
            ];
        $this->macros = [
            'swap' => ['enq', 'enq', 'deq', 'deq'],
            '*' => ['enq', new TNumber(1), 'deq', '/', '/'],
            '++' => [new TNumber(1), '+'],
            '--' => [new TNumber(-1), '+'],
            '-' => ['neg', '+'],
            '!==' => ['===', '!'],
            'neg' => [new TNumber(-1), '*'],
            'false' => [new TBoolean(false)],
            'true' => [new TBoolean(true)],
            'null' => [new TNull()],
            ];

        $this->precompile();
    }

    public function run(array $args=[]) {
        foreach($args as &$arg) {
            $arg = TScalar::fromVar($arg);
        }
        return new Thread($this, $args);
    }

    protected function dumpProgram() {
        foreach($this->program as $index => $code)
            echo "$index: $code\n";
    }

    /**
     * Replaces macros.
     */
    protected function precompile() {
        $queue = $this->program;
        $newProgram = [];
        //echo '§§ '.implode(" ", $queue)."\n";
        while(!empty($queue)) {
            $code = array_shift($queue);
            if(is_string($code)) {
                if(isset($this->macros[$code])) {
                    $queue = array_merge($this->macros[$code], $queue);
                } else if(isset($this->opcodes[$code])) {
                    $newProgram[] = $code;
                } else {
                    throw new InvalidOpcodeException('Unknown opcode '.$code);
                }
            } else {
                $newProgram[] = clone $code;
            }
        }
        $this->program = $newProgram;
    }
}