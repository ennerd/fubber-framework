<?php
namespace Fubber\Lang;

interface LibraryInterface {
    
    public function __construct(VM $vm);
}