<?php
namespace Fubber\Lang\Traits;
use Fubber\Lang\LibraryInterface;

trait Program {
    protected $program = [];
    protected $autoLabelIndex = 0;
    protected function init_program() {
        $this['_program_p'] = 0;
        $this['_program_c'] = -1;
        $this['_program_cs'] = [];
    }
    
    /**
     * Loads library functions
     */
    public function addLibrary($libraryName) {
        $library = new $libraryName($this);
    }
    
    public function addInstructions(array $map) {
        $skipLabel = $this->getAutoLabel();
        $code = [
            ['fl_jump', $skipLabel],           // Jump to end of library
            ];
        foreach($map as $label => $callback) {
            $code[] = ['fl_label', $label];
            $code[] = ['native', \Closure::fromCallable($callback)];
            $code[] = ['return'];
        }
        $code[] = ['fl_label', $skipLabel];    // Define end of library label
        $this->appendInstructions($code);
        //$this->printState();die();
    }
    
    protected function getAutoLabel() {
        return '#'.$this->autoLabelIndex++;
    }
    
    protected function appendInstruction(array $instruction) {
        if(!isset($instruction[0]))
            $this->throwVMException('Invalid code token '.json_encode($code[$i]), static::PARSE_ERROR);
        if($instruction[0] == 'fl_label') {
            if(isset($this[$instruction[1]]))
                $this->throwVMException('Label already declared', static::PARSE_ERROR);
            // Store address on heap
            $this[$instruction[1]] = sizeof($this->program);
        } else {
            $this->program[] = $instruction;
        }
    }
    
    protected function appendInstructions(array $code) {
        foreach($code as $instruction)
            $this->appendInstruction($instruction);
    }
    
    public function step() {
        if(!isset($this->program[$this['_program_p']]))
            return false;
        $this['_program_c'] = $this['_program_p'];
        $this['_program_p'] = $this['_program_p'] + 1;
        $instruction = $this->program[$this['_program_c']];
        $this->evaluate($instruction);
        return true;
    }
    
    public function evaluate(array $instruction) {
        echo "- ".$this['_program_c'].": ".json_encode($instruction)."\n";
        $cmd = array_shift($instruction);
        
        if(method_exists($this, $i = 'i_'.$cmd)) {
            // Only core functions are handled here
            call_user_func_array([$this, $i], $instruction);
            echo "- _ops: ".json_encode($this['_ops'])."\n";
        } else {
            echo "NOT CORE FUNCTION $cmd OP: ".static::printIt($this['_ops'])."\n";
            // All arguments should be pushed as operands (and popped by the function)
            while($operand = array_shift($instruction)) {
                $this->i_fl_op($operand);
            }
            $this->i_fl_call($cmd);
            
            //$this->throwVMException("Unknown command '$cmd'", static::PARSE_ERROR);
        }
    }
    
    public function i_native(\Closure $closure, ...$args) {
        if(!is_object($closure) || get_class($closure) !== \Closure::class)
            $this->throwVMException('Expected closure, got '.\gettype($closure), static::TYPE_ERROR);
        $closure();
    }
    
    public function i_fl_jump($label) {
        $this['_program_p'] = $this[$this->assert_label($label)];
    }
    
    public function i_fl_call($label) {
        $this['_program_cs'][] = $this['_program_p'];
        $this->i_fl_jump($label);
    }
    
    public function &assert_int(&$value) {
        if(is_int($value)) return $value;
        $this->throwVMException('Expected integer', static::TYPE_ERROR);
    }
    
    public function &assert_num(&$value) {
        if(is_int($value) || is_float($value)) return $value;
        $this->throwVMException('Expected numeric', static::TYPE_ERROR);
    }
    
    public function &assert_float(&$value) {
        if(is_float($value)) return $value;
        $this->throwVMException('Expected float', static::TYPE_ERROR);
    }
    
    public function &assert_string(&$value) {
        if(is_string($value)) return $value;
    }
    
    public function &assert_code_address(&$value) {
        if(is_int($value) && isset($this->program[$value])) return $value;
        $this->throwVMException('Expected valid address', static::TYPE_ERROR);
    }
    
    public function &assert_label(&$value) {
        if(isset($this[$value])) return $value;
        $this->throwVMException('Expected label (got '.$this->printIt($value).')', static::TYPE_ERROR);
    }
    
/*
    public function i_fl_jump($nextPos) {
        if(!is_int($nextPos)) {
            if(!isset($this->labels[$nextPos]))
                $this->throwUnknownLabel($nextPos);
            $nextPos = $this->labels[$nextPos];
        }
        if(!isset($this->program[$nextPos]))
            $this->throwJumpedOutOfCode($nextPos);
        echo "Jumping to $nextPos\n";
        $this->programP = $nextPos;
    }
    
    public function i_fl_if_not() {
        $val = $this->op_pop_int();
        if($val !== 0) {
            $this->programP++;
        }
    }
    
    public function i_fl_program_p() {
        $this->i_fl_op($this->programP);
    }
    
    public function i_fl_stop() {
        
    }
*/
}