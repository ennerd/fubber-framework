<?php
namespace Fubber\Lang\Traits;

trait Operands {
    /**
     * Prepares the operand functionality
     */
    protected function init_operands() {
        $this['_ops'] = [];
    }
    
    /**
     * Push operand
     */
    public function i_fl_op($value) {
        echo "- adding operand '".json_encode($value)."'\n";
        $this['_ops'][] = $value;
    }
    
    /**
     * Duplicate the topmost operand
     */
    public function i_fl_op_dup() {
        $val = $this->i_fl_op_pop();
        $this->i_fl_op($val);
        $this->i_fl_op($val);
    }
    
    /**
     * Pop operand
     */
    public function i_fl_op_pop() {
        if(sizeof($this['_ops']) === 0) $this->throwVMException('No operand available', static::RUNTIME_ERROR);
        $res = array_pop($this['_ops']);
        echo "- popped operand '$res'\n";
        return $res;
    }
    
    /**
     * Push size of operand stack
     */
    public function i_fl_op_pos() {
        $this->i_fl_op(sizeof($this['_ops']));
    }
    

    public function pop_number() {
        $res = $this->i_fl_op_pop();
        if(!is_int($res) && !is_float($res))
            $this->throwVMException('Expected numeric operand', static::TYPE_ERROR);
        return $res;
    }
    
    public function pop_int() {
        $res = $this->i_fl_op_pop();
        if(!is_int($res))
            $this->throwVMException('Excpected integer operand', static::TYPE_ERROR);
        return $res;
    }
    
    public function pop_float() {
        $res = $this->i_fl_op_pop();
        if(!is_float($res))
            $this->throwVMException('Excpected float operand', static::TYPE_ERROR);
        return $res;
    }
    
    public function pop_string() {
        $res = $this->i_fl_op_pop();
        if(!is_string($res))
            $this->throwTypeError('Excpected string operand');
        return $res;
    }
    
}