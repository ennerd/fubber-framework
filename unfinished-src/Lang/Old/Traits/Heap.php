<?php
namespace Fubber\Lang\Traits;

trait Heap {
    public function i_fl_read($label) {
        $this->i_fl_op($this[$label]);
    }
    
    public function i_fl_write($label) {
        $this[$label] = $this->i_fl_op_pop();
    }
}