<?php
namespace Fubber\Lang\Traits;

trait Scopes {

    protected function init_scopes() {
        $this['_scopes'] = [];
    }

    /**
     * Open a new scope
     */
    public function i_fl_scope(array $locals) {
        $this['_scopes'][] = $locals;

        $this->i_fl_program_p();
        $this->stack[++$this->stackP] = [
            '_first_instruction' => $this->i_fl_op_pop()
            ];
    }
    /*
    public function i_fl_stack_clone() {
        $this->i_fl_program_p();
        $this->stack[$this->stackP + 1] = $this->stack[$this->stackP++];
        $this->stack[$this->stackP]['_first_instruction'] = $this->i_op_pop();
    }
    */
    /**
     * Goes back to previous scope
     */
    public function i_fl_descope() {
        array_pop($this['_scopes']);
    }
    
    /**
     * Return size of current scope
     */
    public function i_fl_scope_depth() {
        $this->i_fl_op(sizeof($this['_scopes']));
    }

    /**
     * Set a variable on the current scope
     */
    public function i_fl_set($symbol) {
        $this->stack[$this->stackP][$symbol] = $this->i_fl_op_pop();
    }
    
    /**
     * Get a local variable as as operand
     */
    public function i_fl_get($symbol) {
        if(!isset($this->stack[$this->stackP][$symbol]))
            $this->throwUndefinedLocalVariable($symbol);
        $this->i_fl_op($this->stack[$this->stackP][$symbol]);
    }
    
    protected function throwUndefinedLocalVariable($symbol) {
        throw new \Exception("The variable '$symbol' is not defined");
    }
    
}