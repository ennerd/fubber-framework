<?php
namespace Fubber\Lang;

class VM implements \ArrayAccess {
    const HEAP_ERROR = 1;
    const PARSE_ERROR = 2;
    const RUNTIME_ERROR = 3;
    const TYPE_ERROR = 4;
    
    /**
     * Heap is the RAM of the VM. This RAM is unconventional because it is a hash
     * map, instead of a sequence of bytes. This means that every item on the heap
     * consumes only one address, this includes strings. It also means that variable
     * names are first class citizens of the language.
     */
    protected $heap = array();
    public function offsetExists($offset) {
        return isset($this->heap[$offset]);
    }
    public function &offsetGet($offset) { 
        if(!isset($this->heap[$offset]))
            $this->throwVMException("Trying to read undeclared memory address '$offset'.", static::HEAP_ERROR);
        return $this->heap[$offset];
    }
    public function offsetSet($offset, $value) { 
        $this->heap[$offset] = $value;
    }
    public function offsetUnset($offset) { 
        if(!isset($this->heap[$offset]))
            $this->throwVMException("Trying to unset undeclared memory address '$offset'.", static::HEAP_ERROR);
        unset($this->heap[$offset]);
    }
    
    use Traits\Program;
    /**
     * CODE MEMORY
     * 
     * Code is stored separately from the heap and can't be modified during runtime.
     * The only language construct is labels - which are pre-parsed and stored on the
     * heap as addresses. Labels does not consume program space.
     * 
     * You can think of code memory as ROM.
     * 
     * Public API
     * 
     * ->addCode(array $code)                                                   Appends code to program memory. Labels are stored on heap as simple offsets.
     * ->step()                                                                 Execute one more instruction
     * ->evaluate(array $instruction)                                           Evaluate an arbitrary instruction
     * 
     * Instructions
     * 
     * fl_label <label>     Declares the address of the next statement as <label> on the heap.
     */

    use Traits\Operands;
    /**
     * OPERAND STACK
     * 
     * Many instructions require arguments and return values. Arguments are pushed
     * to the operand stack and popped when needed by the instruction. If the instruction 
     * returns a value, that value is pushed to the operand stack again.
     * 
     * Instructions
     * 
     * fl_op <value>        Push value
     * fl_op_dup            Duplicate the topmost operand
     * fl_op_pop            Remove topmost operand. (The return value is not accessible inside VM code.)
     * fl_op_pos            Put the size of the op stack on top of the op stack
     */

    use Traits\Scopes;
    /**
     * PROGRAM SCOPES
     * 
     * Provides a scope stack, used to define local namespaces.
     *
     */

    //use Traits\Stack;
    /**
     * SCOPE STACK
     * 
     * fl_stack             Push an empty scope on the scope stack
     * fl_unstack           Delete topmost scope stack
     * fl_stack_size        OP: Size of scope stack
     * fl_set symbol        OP-POP: Store to scope
     * fl_get symbol        OP: Read from scope
     */
     

    //use Traits\Debug;

    public function __construct() {
        $this->init_program();
        $this->init_operands();
        
        $this->addLibrary(Libraries\Arithmetic::class);
        // Testing script. Should be removed after implementation
        $this->appendInstructions([
            ['fl_label', 'start'],
            ['fl_op', 3],
            ['fl_op', 5],
            ['*'],
            ['fl_op', 15],
            ['/'],
            ['++'],
            ['fl_op_dup'],
            ['*'],
            ['--'],
            ['fl_op', 2],
            ['/'],
            ['--'],
            ['fl_abs'],
            ]);
    }
    
    public function throwVMException($message, $code=0) {
        throw new VMException($message, $code);
    }
    
    public static function printIt($data) {
        if(is_array($data)) {
            $res = [];
            $expectedInt = 0;
            foreach($data as $k => $v) {
                if($k === $expectedInt++)
                    $res[] = static::printIt($v);
                else
                    $res[] = "$k=".static::printIt($v);
            }
            if(sizeof($res) === 0)
                return '[]';
            return '['.implode(" ", $res).']';
        } else if(is_scalar($data)) {
            return json_encode($data);
        } else if(is_object($data)) {
            return '['.get_class($data).']';
        } else if(is_resource($data)) {
            return '[resource]';
        } else if($data === null) {
            return 'null';
        } else if(is_callable($data)) {
            return '[callable:'.json_encode($data).']';
        }
    }
    
    public function printState() {
        echo "CODE:\n";
        foreach($this->program as $k => $v) {
            echo str_pad(substr($k, 0, 5), 5)." = ".static::printIt($v)."\n";
        }
        echo "HEAP:\n";
        foreach($this->heap as $k => $v) {
            if(strlen($k) <= 30)
                $k = str_pad($k, 30);
            else {
                $k = substr($k, 0, 14).'~'.substr($k, -15);
            }
            echo $k." = ".static::printIt($v)."\n";
        }
    }
}