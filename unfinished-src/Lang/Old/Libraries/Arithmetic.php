<?php
namespace Fubber\Lang\Libraries;
use Fubber\Lang\LibraryInterface;
use Fubber\Lang\VM;

class Arithmetic implements LibraryInterface {
    
    public function __construct(VM $vm) {
        $this->vm = $vm;
        
        $this->vm->addInstructions([
            '+' => [$this, 'i_fl_add'],
            '*' => [$this, 'i_fl_mul'],
            '/' => [$this, 'i_fl_div'],
            '-' => [$this, 'i_fl_sub'],
            '--' => [$this, 'i_fl_dec'],
            '++' => [$this, 'i_fl_inc'],
            'fl_abs' => [$this, 'i_fl_abs'],
            'fl_floor' => [$this, 'i_fl_floor'],
            'fl_round' => [$this, 'i_fl_round'],
            'fl_ceil' => [$this, 'i_fl_ceil'],
            'fl_exp' => [$this, 'i_fl_exp'],
            ]);
    }
    
    public function i_fl_add() {
        $this->vm->i_fl_op($this->vm->pop_number() + $this->vm->pop_number());
    }
    
    public function i_fl_mul() {
        $this->vm->i_fl_op($this->vm->pop_number() * $this->vm->pop_number());
    }
    
    public function i_fl_div() {
        $this->vm->i_fl_op($this->vm->pop_number() / $this->vm->pop_number());
    }
    
    public function i_fl_sub() {
        $this->vm->i_fl_op($this->vm->pop_number() - $this->vm->pop_number());
    }
    
    public function i_fl_dec() {
        $this->vm->i_fl_op($this->vm->pop_number() - 1);
    }
    
    public function i_fl_inc() {
        $this->vm->i_fl_op($this->vm->pop_number() + 1);
    }
    
    public function i_fl_abs() {
        $this->vm->i_fl_op(abs($this->vm->pop_number()));
    }
    
    public function i_fl_floor() {
        $this->vm->i_fl_op(floor($this->vm->pop_number()));
    }

    public function i_fl_round() {
        $this->vm->i_fl_op(round($this->vm->pop_number()));
    }

    public function i_fl_ceil() {
        $this->vm->i_fl_op(ceil($this->vm->pop_number()));
    }

    public function i_fl_exp() {
        $this->vm->i_fl_op(exp($this->vm->pop_number()));
    }
}