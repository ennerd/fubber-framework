<?php
namespace Fubber\Lang\Compiler;

// See https://astexplorer.net/

class Node {
    const T_PROGRAM = 'T_PROGRAM';
    const T_EXPRESSION_STATEMENT = 'T_EXPRESSION_STATEMENT';
    const T_ASSIGNMENT_EXPRESSION = 'T_ASSIGNMENT_EXPRESSION';
    const T_IDENT = Lexer::T_IDENT;
    const T_STRLIT = Lexer::T_STRLIT;
    const T_NUMLIT = Lexer::T_NUMLIT;
    const T_OPERATOR = Lexer::T_OPERATOR;
    const T_WHITESPACE = Lexer::T_WHITESPACE;
    const T_COMMENT = Lexer::T_COMMENT;
    const T_BRACKET = Lexer::T_BRACKET;

    protected $children = [];
    protected $start;
    protected $end;

    public function __construct($type) {
        $this->type = $type;
    }
    
    public function addChild(Node $child) {
        $this->children[] = $child;
    }
}