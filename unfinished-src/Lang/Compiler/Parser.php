<?php
namespace Fubber\Lang\Compiler;

class Parser {
    protected $language;
    
    public function __construct($language) {
        $this->language = $language;
    }
    
    public function parse($script) {
        $lexer = $this->language->getLexer();
        $spec = $this->language->getTokenDefinitions();
        $operatorPrecedence = [];
        foreach($spec[Lexer::T_OPERATOR] as $precedence => $token) {
            for($i = sizeof($token)-1; $i > 0; $i--) {
                $operatorPrecedence[$token[$i]] = ['precedence' => $precedence, 'assoc' => $token[0]];
            }
        }

        // Shunting Yard algorithm.
        // Remember: All T_IDENT should have highest precedence, then comes $operatorPrecedence


        foreach($lexer->getTokens($script) as $token) {
            var_dump($token);
        }
    }
}