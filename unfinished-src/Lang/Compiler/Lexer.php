<?php
namespace Fubber\Lang\Compiler;

class Lexer {
    
    const T_IDENT = 'T_IDENT';              // var, function, return
    const T_STRLIT = 'T_STRLIT';            // "String", 'Some string'
    const T_NUMLIT = 'T_NUMLIT';            // 123, 123.22
    const T_OPERATOR = 'T_OPERATOR';        // +. - 
    const T_WHITESPACE = 'T_WHITESPACE';    //
    const T_COMMENT = 'T_COMMENT';          //
    const T_BRACKET = 'T_BRACKET';
    const A_NONE = 'A_NONE';                // Associativity none
    const A_LEFT = 'A_LEFT';                // Left associative
    const A_RIGHT = 'A_RIGHT';              // Right associative
    
    
    public function __construct($language) {
        $this->language = $language;
    }
    
    public function getTokens($source) {
        $options = $this->language->getLexerOptions() + [
            'whitespace' => true
            ];

        // Adds line number and byte offsets to the tokens
        $line = 0;
        $byte = 0;
        foreach($this->_getTokens($source) as $token) {
            $token[] = $line;
            $token[] = $byte;
            $line += sizeof(explode("\n", $token[1]))-1;
            $byte += strlen($token[1]);
            $token[] = $byte - $token[3];
            if($token[0]!==static::T_WHITESPACE || $options['whitespace'])
                yield($token);
        }
    }
    
    protected function _getTokens($source) {
        $line = 0;
        $spec = $this->language->getTokenDefinitions();
            
        $offset = 0;
        
        // Find all unique tokens
        $allTokens = [];
        if(isset($spec[static::T_OPERATOR])) {
            foreach($spec[static::T_OPERATOR] as $tokens) {
                for($i = 1; $i < sizeof($tokens); $i++) {
                    $allTokens[] = [$tokens[$i], mb_strlen($tokens[$i])];
                }
            }
        }

        $charAt = function($offset) use($source) {
            return mb_substr($source, $offset, 1);
        };
        $isWhitespace = function($char) {
            return trim($char, static::$languageSpec['overrides'][static::T_WHITESPACE])=='';
        };
        $isNumber = function($char) {
            return trim($char, '0123456789')=='';
        };
        $isIdentStart = function($char) {
            
        };
        $length = mb_strlen($source);

        $tokenLengths = [];
        foreach($allTokens as $token)
            $tokenLengths[$token[1]] = $token[1];
        rsort($tokenLengths);

        foreach($allTokens as &$token)
            $token = $token[0];

        // Everything that is not captured by other rules are considered T_IDENT
        $ident = '';

        while($offset < $length) {
            // This loop allows me to use break; to skip the rest of the checks
            while(true) {
                $char = $charAt($offset);
    
                // Check for T_WHITESPACE
                if(mb_strpos($spec[static::T_WHITESPACE], $char)!==false) {
                    if($ident !== '') {
                        yield([static::T_IDENT, $ident]);
                        $ident = '';
                    }
                    $result = $char;
                    while(++$offset<$length && mb_strpos($spec[static::T_WHITESPACE], $char = $charAt($offset))!==false)
                        $result .= $char;
                    yield([static::T_WHITESPACE, $result]);
                    break;
                }

                // Check for T_COMMENT
                foreach($spec[static::T_COMMENT] as $comment) {
                    if(mb_substr($source, $offset, mb_strlen($comment[0])) === $comment[0]) {
                        if($ident !== '') {
                            yield([static::T_IDENT, $ident]);
                            $ident = '';
                        }
                        $l = mb_strlen($comment[1]);
                        $offset += $l - 1;
                        $result = '';
                        while(++$offset<$length && mb_substr($source, $offset, $l)!==$comment[1])
                            $result .= $charAt($offset);
                        $offset += mb_strlen($comment[1]);
                        yield([static::T_COMMENT, $result]);
                        break 2;
                    }
                }
                
                // Check for T_OPERATOR
                foreach($tokenLengths as $tokenLength) {
                    $testFor = mb_substr($source, $offset, $tokenLength);
                    if(in_array($testFor, $allTokens)) {
                        if($ident !== '') {
                            yield([static::T_IDENT, $ident]);
                            $ident = '';
                        }
                        $offset += $tokenLength;
                        yield([static::T_OPERATOR, $testFor]);
                        break 2;
                    }
                }
                
                // Check for T_NUMLIT
                if($ident==='' && ctype_digit($char)) {
                    $result = $char;
                    // Allow numbers, until we get to the first .
                    while(++$offset<$length && ctype_digit($char = $charAt($offset))) {
                        $result .= $char;
                    }
                    if($char == '.' && ctype_digit($charAt($offset+1))) {
                        $result .= $char;
                        while(++$offset<$length && ctype_digit($char = $charAt($offset))!==false) {
                            $result .= $char;
                        }
                        yield([static::T_NUMLIT, $result, 'decimal']);
                        break;
                    } else {
                        yield([static::T_NUMLIT, $result]);
                        break;
                    }
                }
                
                // Check for T_BRACKET
                if(strpos($spec[static::T_BRACKET], $char)!==false) {
                    if($ident !== '') {
                        yield([static::T_IDENT, $ident]);
                        $ident = '';
                    }
                    $offset++;
                    yield([static::T_BRACKET, $char]);
                    break;
                }
                
                if($char != '.') {
                    $ident .= $char;
                    $offset++;
                    break;
                }
                
                die("Unable to parse");
            }
        }

    }
}