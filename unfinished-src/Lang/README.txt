Fubber Lang

A simple virtual machine, that allows you to write code that halts and resumes between requests. For example, 
you could script a chat-robot that maintains state across multiple requests.


Design goals

* Serializable
* Bare minimum core functionality
* Simple to extend
* Can be used to create a DSL
* Fast


Instruction Set

The native instruction set is kept small, with focus on enabling implementation
of the typical minimal feature set of languages:

* Conditional Jumps
* Call Stack
* Scopes
