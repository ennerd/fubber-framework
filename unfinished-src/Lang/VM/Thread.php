<?php
namespace Fubber\Lang\VM;
use Fubber\Lang\VM;
use Fubber\Lang\Types\TPrimitive;
use Fubber\Lang\Types\TObject;
use Fubber\Lang\Exceptions\InvalidOpcodeException;
use Closure;

class Thread {
    
    public $programCounter = 0;
    public $context = null;
    protected $vm = null;
    protected $opcodes = null;
    protected $root;

    public function __construct(VM $vm, array $root = null) {
        if(!$root) {
            $this->root = new TObject();
        } else {
            $this->root = new TObject($root);
        }
        $this->vm = $vm;
        $this->importOpcodes();
        $this->context = new Context($this->root);
    }
    
    public function getRoot() {
        return json_decode(json_encode($this->root), true);
    }
    
    public function tick() {
        $was = $this->programCounter;
        $opcode = $this->vm->program[$this->programCounter];
        $this->programCounter++;
        $this->eval($opcode);
        return isset($this->vm->program[$this->programCounter]);
    }
    
    protected function eval($opcode) {
        if(is_a($opcode, TPrimitive::class))
            $this->context->push($opcode);
        else if(!isset($this->opcodes[$opcode]))
            throw new InvalidOpcodeException('Unknown opcode "'.$opcode.'"');
        else {
            echo "OPCODE $opcode\n";
            $this->opcodes[$opcode][2]($this->context);
        }
    }
    
    protected function importOpcodes() {
        foreach($this->vm->opcodes as $code => $callback) {
            //var_dump($callback);
            $this->opcodes[$code] = [ $callback[0], $callback[1], Closure::fromCallable($callback[3])->bindTo($this, static::class) ];
        }
    }
}