<?php
namespace Fubber\Lang\VM;
use Fubber\Lang\Types\TPrimitive;
use Fubber\Lang\Types\TObject;
use Fubber\Lang\Types\TNumber;
use Fubber\Lang\Types\TString;
use Fubber\Lang\Types\TBoolean;
use Fubber\Lang\Types\TUndefined;
use Fubber\Lang\Exceptions\OutOfOperandsException;
use Fubber\Lang\Exceptions\TypeErrorException;


/**
 * Represents the local context and has a reference to the parent context, unless it is
 * the root context.
 */
class Context implements \ArrayAccess {
    protected $thread;
    protected $vars = [];
    protected $operands = [];
    protected $queue = [];
    protected $parent;
    
    public function __construct(TObject $theThis, Context $parent=null) {
        $this->parent = $parent;
        $this->vars['this'] = $theThis;
    }

    public function enqueue($val) {
        if(!is_a($val, TPrimitive::class))
            $val = TPrimitive::fromVar($val);
        $this->queue[] = $val;
    }
    
    public function dequeue() {
        return array_shift($this->queue);
    }
    
    public function push($val) {
        if(!is_a($val, TPrimitive::class))
            $val = TPrimitive::fromVar($val);
        echo "PUSHED $val\n";
        $this->operands[] = $val;
    }
    
    public function pop() {
        if(null === ($res = array_pop($this->operands)))
            throw new OutOfOperandsException();
        echo "POPPED $res\n";
        return $res;
    }
    
    public function popNumber() {
        $res = $this->pop();
        if(!is_a($res, TNumber::class))
            throw new TypeErrorException('Expected type operand of type TNumber, got '.$res);
        return $res->value;
    }
    
    public function popString() {
        $res = $this->pop();
        if(!is_a($res, TString::class))
            throw new TypeErrorException('Expected type operand of type TString, got '.$res);
        return $res->value;
    }
    
    public function popBoolean() {
        $res = $this->pop();
        if(!is_a($res, TBoolean::class))
            throw new TypeErrorException('Expected type operand of type TBoolean, got '.$res);
        return $res->value;
    }
    
    public function offsetCreate($name) {
        if(!isset($this->vars[$name]))
            $this->vars[$name] = new TUndefined();
    }
    
    public function &offsetGet($name) {
        if(!isset($this->vars[$name])) {
            if($this->parent)
                return $this->parent[$name];
            // Return value from the global object, since we have no parent
            return $this['this'][$name];
        }
        return $this->vars[$name];
    }
    
    /**
     * Set the variable in the scope where it was first declared (with offsetCreate). 
     * If it was never declared, it should be set in the topmost scope.
     */
    public function offsetSet($name, $value) {
        if(isset($this->vars[$name]))
            $this->vars[$name] = $value;
        else if($this->parent)
            return $this->parent[$name] = $value;
        // Set the value on the global object
        return $this['this'][$name] = $value;
    }
    
    /**
     * Check if variable is declared in scope or any of the parent scopes
     */
    public function offsetExists($name) {
        if(isset($this->vars[$name]))
            return true;
        else if($this->parent)
            return isset($this->parent[$name]);

        // Fallback to checking on the global object
        return isset($this['this'][$name]);
    }
    
    /**
     * Works like javascript delete. You can't delete variables from the topmost scope.
     * 
     * Todo: https://www.ecma-international.org/ecma-262/5.1/#sec-11.4.1
     */
    public function offsetUnset($name) {
        // NOTE: Not sure if javascript 
        if($this->parent) {
            unset($this->vars[$name]);
            return true;
        }
        return false;
    }
}