<?php $this->extends('page'); ?>
<h1><?=$statusCode; ?> <?=$reasonPhrase; ?></h1>
<p>Redirecting to <a href="<?=$targetUri; ?>"><?=$targetUri; ?></a></p>
<h2>Request Headers</h2>
<pre class='code'><?php
    print_r($requestHeaders);
?></pre>
<h2>Response Headers</h2>
<pre class='code'><?php
    print_r($responseHeaders);
?></pre>
<script type="module">
    setTimeout(() => {
        location.href = <?=json_encode($targetUri); ?>;
    }, 500);
</script>