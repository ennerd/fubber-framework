<!DOCTYPE html>
<html>
    <head>
        <title>{{$title}}</title>
        <script src="https://cdn.jsdelivr.net/npm/vue@3/dist/vue.global.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue-template-compiler@2/browser.js"></script>
    </head>
    <body>
        <div id="fubber-app">
            <header>
                <span class="logo">Fubber Framework</span> <em><?=htmlspecialchars($title ?? 'Title not set'); ?></em>
            </header>
            <main>
                @section('main')
                This is the 'main' section
                @show
            </main>
        </div>
        <style scoped>
            html, body {
                margin: 0;
                padding: 0;
                font-family: Tahoma, Arial, Helvetica, sans-serif;
                color: #555555;
                background-color: #FFFFFF;
            }
            header {
                padding: 20px 50px 15px 50px;
                color: #ffffff;
                border-bottom: 2px solid #3667A1;
                background-color: #4E95E8;
            }
            pre.code {
                border: 1px solid #bbbbbb;
                padding: 10px;
            }
            .logo {
                font-weight: bold;
                font-style: italic;
            }
            main {
                padding: 5px 50px;
            }
            a {
                color: #4E95E8;
            }
            a:hover {
                color: #56A3FF;
            }
        </style>
    </body>
</html>