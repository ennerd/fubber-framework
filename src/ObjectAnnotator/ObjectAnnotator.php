<?php
namespace Fubber\ObjectAnnotator;

use Fubber\LogicException;
use stdClass;
use WeakMap;

/**
 * Utility class utilizing a WeakMap for attaching information on arbitrary
 * objects.
 * 
 * Usage:
 * ```
 * $a = new ObjectAnnotator();
 * $a($user)->some_annotation = "Hey";
 * echo $a($user)->some_annotation; // Hey
 * ```
 * 
 * The annotations remain in memory until the target object is gone, or
 * until the ObjectAnnotator instance is destructed.
 * 
 * @template SourceClass
 * @template AnnotationClass
 * @package Fubber\ObjectAnnotator
 */
class ObjectAnnotator {

    /**
     * Stores annotation instances of T for objects.
     * 
     * @var WeakMap<SourceClass, AnnotationClass>
     */
    private WeakMap $map;

    /**
     * The class for annotation objects.
     * 
     * @var null|string
     */
    private readonly ?string $annotationClass;

    /**
     * @param class-string<AnnotationClass> $annotationClass 
     */
    public function __construct(string $annotationClass=stdClass::class) {
        $this->map = new WeakMap();
        $this->annotationClass = $annotationClass;
    }

    /**
     * Get an annotation class instance
     * 
     * @param SourceClass $target 
     * @throws LogicException 
     */
    public function get(object $target): object {
        if (!isset($this->map[$target])) {
            $this->map[$target] = new ($this->annotationClass)();
        }

        return $this->map[$target];
    }

    /** 
     * Cloning is used for stashing and restoring a state, so we must
     * ensure the WeakMap is also cloned.
     */
    public function __clone() {
        $this->map = clone $this->map;
    }

    /**
     * 
     * @param SourceClass $target 
     * @return AnnotationClass 
     * @throws LogicException 
     */
    public function __invoke(object $target): object {
        return $this->get($target);
    }
}