# Pure Objects, Algorithms and Separation of Concerns

A common design pattern in programming is called "separation of concerns"
(SoC). This can be quite difficult when working with algorithms and data
structures - it is tempting to attach properties to objects which is only
an implementation detail needed by some algorithm.

## An imaginary example

Let's pretend we're developing a chess engine. This chess engine has two
main object classes: `Board` and `Piece` representing a board and a chess
piece.

There are many different algorithms which may be relevant for a chess board,
and to allow each of these algorithms to work on chess chess boards, you could
end up adding properties to the `Board` class - such as `Board::$previousBoard`
and `Board::$nextBoards[]` and `Board::$value`. Each of these properties 
does not actually belong to the `Board` *class*. They are metadata which
concerns *only your algorithm*.

## The slightly better solution

A better implementation is to create a special `BoardContainer` object which
has a `BoardContainer::$board` property, and then has the other properties
`$previousBoardContainer` and `$nextBoardContainers[]`.

The problem with this solution, is that you don't your algorithm to expect
`BoardContainer` objects in your arguments, and you don't want to return
`BoardContainer` objects to the calling class.

## The best solution

Using `WeakMap` you can associate arbitrary data to objects, and when those
objects are no longer needed - the associated data is also garbage collected.

It is common to attach an array to objects via a `WeakMap` but then you will
lose out on IDE features such as type hinting and inspection.

Using `ObjectAnnotator`, you first declare a class for your additional
properties:

```php
class BoardAnnotations {
    public ?Board $previousBoard = null;
    /** @var Board[] */
    public array $nextBoards = [];
    public ?int $value = null;
}
```

Next, add the following property and method to your `ChessEngine`
class. The `board(Board $board)` function is really only there
because some language servers don't support generics. If you are
using PHPStorm you can just access `$this->boardInfo->get($board)`
directly.

```php 
class ChessEngine {
    /**
     * Remember to create this object in your constructor with
     * `new ObjectAnnotator(BoardAnnotations::class)`
     * 
     * @var ObjectAnnotator<BoardAnnotations>
     */
    protected ObjectAnnotator $boardInfo;

    /**
     * You could use `$this->boardInfo->get($board)` directly,
     * but some IDEs (such as VS Code with Intelephense) does
     * not currently support template generics.
     */
    protected function board(Board $board): BoardAnnotations {
        return $this->boardInfo->get($board);
    }

    /**
     * Example function
     */
    public function getBoardValueAndIncrement(Board $board): ?int {
        /**
         * You can modify the properties directly, and you get IDE
         * assistance with the property names. 
         */
        return $this->board($board)->value++;
    }

}
```
