<?php
namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\Traits\ExceptionTrait;
use JsonSerializable;

/**
*	@see \BadFunctionCallException
*/
class InvalidArgumentException extends \InvalidArgumentException implements JsonSerializable, IException {
    use ExceptionTrait;

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Invalid Argument";
    }
}
