<?php
namespace Fubber\Traits;

trait TDecorator {
    abstract protected function _decoratedObject();

    public function __call($name, array $args) {
        $source = $this->_decoratedObject();
        $closure = Closure::fromCallable([$source, $name])->bindTo($source);
        return call_user_func_array($closure, $args);
    }
    public function __get($name) {
        // No need to worry about visibility, since $this->source is not this class.
        return $this->_decoratedObject()->$name;
    }
    public function __set($name, $value) {
        // No need to worry about visibility, as long as everything for this class is declared
        $this->_decoratedObject()->$name = $value;
    }
    
}