<?php
namespace Fubber\Traits;

use Fubber\I18n\Translatable;
use Fubber\Kernel;
use Throwable;

/**
 * @mixin Throwable
 */
trait ExceptionTrait {

    protected string|Translatable $originalMessage;
    protected ?string $suggestion = "Review error logs and source code";
    protected ?int $httpCode = null;
    protected ?string $reasonPhrase = null;

    public function __construct(string $message='', int $code=0, ?Throwable $previous=null) {
        parent::__construct((string) $message, $code, $previous);
    }

    abstract public function getExceptionDescription(): string;

    /**
     * Returns an array with the HTTP status code and
     * the HTTP reason phrase
     *
     * @return array
     */
    abstract protected function getDefaultStatus(): array;

    /**
     * Return a new exception with a custom HTTP status code and
     * reason phrase.
     * 
     * @param mixed $code 
     * @param string $reasonPhrase 
     * @return static 
     */
    public final function withStatus($code, $reasonPhrase = ''): static {
        $c = clone $this;
        $c->httpCode = $code;
        $c->reasonPhrase = $reasonPhrase;
        return $c;
    }

    /**
     * Return a new exception with a suggestion for developers.
     * 
     * @param string $suggestion 
     * @return static 
     */
    public final function withSuggestion(string $suggestion): static {
        $c = clone $this;
        $c->suggestion = $suggestion;
        return $c;
    }

    public final function getStatusCode(): int {
        return $this->httpCode ?? $this->getDefaultStatus()[0];
    }

    public final function getReasonPhrase(): string {
        return $this->httpStatus ?? $this->getDefaultStatus()[1];
    }

    public final function getSuggestion(): ?string {
        return $this->suggestion;
    }

    public function jsonSerialize() {
        $result = [
            'error' => $this->getExceptionDescription(),
            'message' => $this->originalMessage,
            'code' => $this->getCode()
        ];
        if (Kernel::$instance->env->debug || (defined('DEBUG') && DEBUG)) {
            $result['debug'] = [
                'file' => $this->getFile(),
                'line' => $this->getLine(),
                'backtrace' => $this->getTrace(),
            ];
        }
        return $result;
    }

}