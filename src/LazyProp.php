<?php
namespace Fubber;

use Closure;
use ReflectionMethod;
use ReflectionProperty;
use User;
use WeakMap;

const LAZYPROPERTY_UNINITIALIZED = 'UNINITIALIZED PROPERTY';

/**
 * Trait to declare getters and setter properties on an class. Each getter
 * and setter method will be bound to the class and can use $this to access
 * the instance.
 * 
 * Methods named `'get'.ucfirst($propertyName)` and `'set'.ucfirst($propertyName)`
 * on the class will automatically be exposed as ordinary properties.
 * 
 * The main purpose of this is to "retrofit" logic on properties that initially avoid pre-loading expensive properties
 * when refactoring old code while maintaining backward compatability.
 * 
 * A better practice is to write getSomething() or setSomething()-methods
 * in the first place.
 */
trait LazyProp {

    /**
     * Holds getter methods for classes and child-classes.
     *
     * @var array<string, array<class-string, Closure>>
     */
    private static array $_getters = [];

    /**
     * Holds setter methods for classes and child-classes.
     *
     * @var array<string, array<class-string, Closure>>
     */
    private static array $_setters = [];

    /**
     * Holds property values whenever they are set.
     * 
     * @todo Consider dropping weakmaps in favor of a class property array (which probably is more expensive)
     *
     * @var array<string, WeakMap<static, mixed>>
     */
    private static array $_values = [];

    /**
     * Holds lazily evaluated properties for objects; when the property
     * has been evaluated - the result is persisted in $_values.
     * 
     * Note! The data structure was chosen to reduce garbage collection.
     *
     * @var array<string, WeakMap<static, Closure>>
     */
	private static array $_lazyProps = [];

    /**
     * Convenience method for declaring many getters and setters in one operation
     *
     * @param array<string, Closure> $getters Property names and their getter function
     * @param array<string, Closure> $setters Property names and their setter function
     * @param array<string> $properties Property names
     * @return void
     */
    public static function declareProperties(array $getters=[], array $setters=[], array $properties=[]): void {
        foreach ($getters as $name => $callback) {
            static::declareGetter($name, $callback);
        }
        foreach ($setters as $name => $callback) {
            static::declareSetter($name, $callback);
        }
        foreach ($properties as $name) {
            static::declareProperty($name);
        }
    }

    /**
     * Declare a getter method on the class. This getter method will apply
     * to all instances of the class.
     * 
     * @param string $propertyName The property name
     * @param Closure $callback 
     */
    public static function declareGetter(string $propertyName, Closure $callback): void {
        $className = \get_called_class();

        if (\method_exists($className, $getMethodName = 'set' . ucfirst($propertyName))) {
            throw new LogicException("Can't declare a setter method when a method $className::$getMethodName exists");
        }

        if (\property_exists($className, $propertyName)) {
            $rp = new ReflectionProperty($className, $propertyName);

            if ($rp->isPublic()) {
                $declaringClass = $rp->getDeclaringClass();

                throw (new LogicException("The property $className::\$$propertyName is already explicitly declared public as $declaringClass::\$$propertyName"))
                ->withSuggestion("Remove the explicit declaration, make it private/protected or avoid declaring a getter");
            }
        }

        if (isset(self::$_getters[$propertyName][$className])) {
            throw new LogicException("The getter $className::\$propertyName has already been declared (or accessed)");
        }

        if (!isset(self::$_values[$propertyName])) {
            self::$_values[$propertyName] = new WeakMap();
        }

        self::$_getters[$propertyName][$className] = $callback->bindTo(null, $className);
    }

    /**
     * Declare a getter method on the class. This getter method will apply
     * to all instances of the class.
     * 
     * @param string $propertyName The property name
     * @param Closure $callback 
     */
    public static function declareSetter(string $propertyName, Closure $callback): void {
        $className = \get_called_class();

        if (\method_exists($className, $setMethodName = 'set' . ucfirst($propertyName))) {
            throw new LogicException("Can't declare a setter method when a method $className::$setMethodName exists");
        }

        if (\property_exists($className, $propertyName)) {
            $rp = new ReflectionProperty($className, $propertyName);
            if ($rp->isPublic()) {
                $declaringClass = $rp->getDeclaringClass();

                throw (new LogicException("The property $className::\$$propertyName is already explicitly declared public as $declaringClass::\$$propertyName"))
                ->withSuggestion("Remove the explicit declaration, make it private/protected or avoid declaring a setter");
            }
        }

        if (isset(self::$_setters[$propertyName][$className])) {
            throw new LogicException("The setter $className::\$propertyName has already been declared (or accessed)");
        }

        if (!isset(self::$_values[$propertyName])) {
            self::$_values[$propertyName] = new WeakMap();
        }

        self::$_setters[$propertyName][$className] = $callback->bindTo(null, $className);
    }

    /**
     * Declare a property and an initial value
     *
     * @param string $name
     * @param mixed $initialValue=null
     * @return void
     */
    public static function declareProperty(string $name, mixed $initialValue=LAZYPROPERTY_UNINITIALIZED): void {
        $className = \get_called_class();

        if (\property_exists($className, $name)) {
            $rp = new ReflectionProperty($className, $name);
            if ($rp->isPublic()) {
                $declaringClass = $rp->getDeclaringClass();
                throw (new LogicException("The property $className::\$$name is already explicitly declared public as $declaringClass::\$$name"))
                ->withSuggestion("Remove the explicit declaration, make it private/protected or avoid declaring a setter");
            }
        }

        if (isset(self::$_getters[$name][$className])) {
            throw new LogicException("The property $className::\$name already has a getter");
        }
        if (isset(self::$_setters[$name][$className])) {
            throw new LogicException("The property $className::\$name already has a setter");
        }

        if (isset(self::$_getters[$name]) || isset(self::$_setters[$name])) {
            // somewhere there exists a getter or a setter for this property, so we must check if it is on a parent class
            $searchClass = $className;
            
            while ($searchClass = \get_parent_class($searchClass)) {
                if (isset(self::$_getters[$name][$searchClass]) || isset(self::$_setters[$name][$searchClass])) {
                    throw (new LogicException("The property $className::\$$name is already declared on ancester class $searchClass::\$$name"));
                }
            }
    
        }

        $setter = (function($value) use ($name) {
            self::$_values[$name]->offsetSet($this, $value);
        })->bindTo(null, $className);

        $getter = (function() use ($name, $className, $initialValue) {
            if (!self::$_values[$name]->offsetExists($this)) {
                if ($initialValue !== LAZYPROPERTY_UNINITIALIZED) {
                    return $initialValue;
                }
                throw new LogicException("Property $className::\$$name was declared via LazyProp::declareProperty() but it has no default value");
            }
            return self::$_values[$name]->offsetGet($this);
        })->bindTo(null, $className);

        self::$_getters[$name][$className] = $getter;
        self::$_setters[$name][$className] = $setter;

        if (!\array_key_exists($name, self::$_values)) {
            self::$_values[$name] = new WeakMap();
        }
    }


	/**
	*	Add a lazily evaluated property on a particular instance.
	*
	*	@param string $name
	*	@param Closure $callback
	*	@return $this For convenience
	*/
	public function lazy(string $name, callable $callback) {
        if (!isset(self::$_lazyProps[$name])) {
            self::$_lazyProps[$name] = new WeakMap();
            if (!isset(self::$_values[$name])) {
                self::$_values[$name] = new WeakMap();
            }
        }
        /*
        if (!($callback instanceof Closure)) {
            
            $callback = Closure::fromCallable($callback)->bindTo(null, \get_called_class());
        }
        */
        self::$_lazyProps[$name]->offsetSet($this, function() use ($callback) {
            if (!$this) {
                var_dump($this);die();
            }
            return $callback($this);
        });
        
		return $this;
	}

    /**
     * @inheritDoc
     * @param [type] $name
     * @return void
     */
	public function __get($name) {
        if (method_exists($this, $methodName = 'get' . ucfirst($name))) {
            return $this->$methodName();
        }

        $className = \get_called_class();

        if (!isset(self::$_values[$name])) {
            throw new CodingErrorException("Access to undeclared property $className::\$$name");
        }

        /**
         * Get the property value if it was attached directly to this instance
         */
        if (self::$_values[$name]->offsetExists($this)) {
            return self::$_values[$name]->offsetGet($this);
        }

        /**
         * Could the value be a lazily initialized value?
         */
        if (isset(self::$_lazyProps[$name])) {

            /**
             * Is there actually a lazy value here?
             */
            if (self::$_lazyProps[$name]->offsetExists($this)) {
                /**
                 * @var Closure
                 */
                $closure = self::$_lazyProps[$name]->offsetGet($this);
                self::$_lazyProps[$name]->offsetUnset($this);
                $result = $closure->call($this, $this);
                self::$_values[$name]->offsetSet($this, $result);
                return $result;
            }
        }

        if (!isset(self::$_getters[$name][$className])) {
            /**
             * Is the getter declared explicitly on the class or any parent class?
             */
            $searchClass = $className;
            while ($searchClass) {
                if (isset(self::$_getters[$name][$searchClass])) {
                    // this ensures that we'll find the getter immediately next time
                    self::$_getters[$name][$className] = self::$_getters[$name][$searchClass];
                    break;
                }
                $searchClass = \get_parent_class($searchClass);
            }
        }

        if (isset(self::$_getters[$name][$className])) {
            return self::$_getters[$name][$className]->call($this);
        }

        /**
         * The property getter does not exist, which is an error. No need to cache this error situation.
         */
        if (isset(self::$_setters[$name][$className])) {
            $exception = new CodingErrorException("A LazyProp setter is declared for $className::$name, but no getter method is declared");
        } else {
            $exception = new CodingErrorException("Unknown property '$name' in '$className'");
        }
		throw $exception
        ->withSuggestion("Declare the $className::$methodName() method, or use $className::declareGetter(".trim(var_export($name, true)).", function() { ... })");
	}
	
    /**
     * @inheritDoc
     *
     * @param string $name
     * @param mixed $value
     */
	public function __set($name, $value) {
        $className = \get_called_class();

        if (\method_exists($this, $methodName = 'set' . ucfirst($name))) {
            $this->$methodName($value);
            return;
        }

        if (!isset(self::$_values[$name])) {
            throw new CodingErrorException("Access to undeclared property $className::\$$name");
        }

        /**
         * Quickly overwrite the property value if it was attached directly to this instance
         */
        if (self::$_values[$name]->offsetExists($this)) {
            self::$_values[$name]->offsetSet($this, $value);
            return;
        }

        /**
         * Could the value be a lazily initialized value?
         */
        if (isset(self::$_lazyProps[$name])) {

            /**
             * Is there actually a lazy value here?
             */
            if (self::$_lazyProps[$name]->offsetExists($this)) {
                /**
                 * @var Closure
                 */
                $closure = self::$_lazyProps[$name]->offsetGet($this);
                self::$_lazyProps[$name]->offsetUnset($this);
                $result = $closure->call($this, $this);
                self::$_values[$name]->offsetSet($this, $result);
                return $result;
            }
        }

        if (!isset(self::$_setters[$name][$className])) {
            $searchClass = $className;
            /**
             * Is the getter declared explicitly on the class or any parent class?
             */
            while ($searchClass = \get_parent_class($searchClass)) {
                if (isset(self::$_setters[$name][$searchClass])) {
                    // this ensures that we'll find the setter immediately next time
                    self::$_setters[$name][$className] = self::$_setters[$name][$searchClass];
                    break;
                }
            }
        }

        if (isset(self::$_setters[$name][$className])) {
            self::$_setters[$name][$className]->call($this, $value);
            return;
        }

        /**
         * The property setter does not exist, which is an error. No need to cache this error situation.
         */
        if (isset(self::$_getters[$name][$className])) {
            $exception = new CodingErrorException("A LazyProp getter is declared for $className::$name, but no setter method is declared");
        } else {
            $exception = new CodingErrorException("Unknown property '$name' in '$className'");
        }
		throw $exception
        ->withSuggestion("Declare the $className::$methodName() method, or use $className::declareSetter(".trim(var_export($name, true)).", function($value) { ... })");
	}
	
	/**
	 * Handle special case of invokable property
	 */
	public function __call($name, $args) {
        $method = $this->__get($name);

        if (\is_callable($method)) {
            return $method(...$args);
        } else {
            throw (new CodingErrorException("Property '$name' is not callable in '".\get_called_class()."'"))
            ->withSuggestion("Fix the typo or declare the method");
        }
	}
}
