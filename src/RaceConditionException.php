<?php
namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when you are attempting to update a resource
 * that in the meantime have been updated by somebody else. For example
 * if you are updating a counter value, and somebody else already incremented
 * it.
 */
class RaceConditionException extends InternalErrorException {

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }
    
    public function getExceptionDescription(): string {
        return "Race Condition";
    }
}
