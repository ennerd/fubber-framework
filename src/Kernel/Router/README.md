# Fubber\Router

A router middleware which will use the `RequestInterface::getRequestTarget()` to find
a request handler that will process the requested route.