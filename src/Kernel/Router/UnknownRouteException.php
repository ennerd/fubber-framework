<?php
namespace Fubber\Kernel\Router;

use Fubber\CodingErrorException;

class UnknownRouteException extends CodingErrorException {
    
}