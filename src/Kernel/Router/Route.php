<?php
namespace Fubber\Kernel\Router;

use Closure;
use Fubber\LogicException;
use Psr\Http\Server\RequestHandlerInterface;

class Route {
    private readonly string $hash;
    public readonly ?string $name;
    public readonly array $method;
    public readonly string $path;
    public readonly int $priority;
    public readonly mixed $origin;

    private static array $instances = [];
    private static array $handlers = [];

    public function __construct(string|array $method, string $path, RequestHandlerInterface|Closure $handler, string $name=null, int $priority=0, mixed $origin=null) {

        $this->hash = $method.$path.($name??'NULL');

        if (isset(self::$handlers[$this->hash])) {
            throw new LogicException("Route with hash '" . $this->hash . "' already added");
        }
 
        $this->name = $name;
        if (\is_string($method)) {
            $this->method = explode("|", \strtoupper($method));
        } else {
            $this->method = \array_map(\strtoupper(...), $method);
        }
        $this->path = $path;
        $this->priority = $priority;
        $this->origin = $origin;
        self::$instances[$this->hash] = $this;
        self::$handlers[$this->hash] = $handler;
    }

    public function __get($name) {
        if ($name === 'handler') {
            return self::$handlers[$this->hash];
        }
        return null;
    }

    public static function __set_state($properties)
    {
        if (!isset(self::$instances[$properties['hash']])) {
            throw new RouteLoadFailedException();
        }
        return self::$instances[$properties['hash']];
    }

    public function __toString(): string {
        return 'Route['.implode("|", $this->method).' '.$this->path.']';
    }
}