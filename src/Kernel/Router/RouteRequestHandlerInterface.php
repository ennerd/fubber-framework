<?php
namespace Fubber\Kernel\Router;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface for running routes which don't have a RequestHandlerInterface 
 * handler.
 * 
 * @package Fubber\Router
 */
interface RouteRequestHandlerInterface {
    public function __invoke(ServerRequestInterface $request, Route $route): ResponseInterface;
}