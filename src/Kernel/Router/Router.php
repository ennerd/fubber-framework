<?php
namespace Fubber\Kernel\Router;

use Closure;
use CodingErrorException;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Fubber\CodingErrorException as FubberCodingErrorException;
use Fubber\I18n\Translatable;
use Fubber\InternalErrorException;
use Fubber\InvalidArgumentException;
use Fubber\Kernel;
use Fubber\Kernel\Container\Container;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Kernel\Environment\Environment;
use Fubber\Hooks\Trigger;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Hooks\Event;
use Fubber\Hooks\Handler;
use Fubber\Kernel\Http\RequestHandlers\ClosureRequestHandler;
use Fubber\Kernel\LogicException;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Router\Route;
use Fubber\Kernel\Router\RouteLoadFailedException;
use Fubber\Kernel\Router\UnknownRouteException;
use Fubber\NotFoundException;
use Fubber\Psr\HttpMessage\Response;
use Fubber\Psr\HttpMessage\Uri;
use Fubber\Redirect;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ReflectionException;

use function FastRoute\cachedDispatcher;
use function FastRoute\simpleDispatcher;

class Router extends KernelExtension implements RequestHandlerInterface {

    /**
     * When this event is dispatches, the application should register all routes
     * 
     * @var Trigger
     */
    public readonly Event $onCollectRoutes;

    /**
     * Trigger when routing is available (after all routes have been added)
     */
    public readonly Trigger $onReady;

    /**
     * All registered request handler routes
     * 
     * @var Route[]
     */
    protected array $routes = [];

    /**
     * Named routes
     * 
     * @var array<string, Route>
     */
    protected array $namedRoutes = [];

    /**
     * The compiled dispatcher for request handler routes.
     * 
     * @var null|Dispatcher
     */
    protected ?Dispatcher $handlerDispatcher = null;

    /**
     * Route runner function
     * 
     * @var null|RouteRequestHandlerInterface
     */
    protected ?Closure $routeRequestHandler;

    protected bool $isCollectingRoutes = false;

    protected ?array $preCollectedRoutes = [];

    public function getExtensionName(): string
    {
        return 'HTTP Router';
    }

    /**
     * @param Kernel $kernel 
     * @param null|RouteRequestHandlerInterface $routeRequestHandler A callback which receives a 
     */
    public function __construct(
        protected readonly Environment $env,
        protected readonly Container $container,
        protected readonly Instrumentation $dev,
    ) {
        $this->onCollectRoutes = new Event("Router expects to receive all routes");
        $this->onReady = new Trigger("Router is ready to resolve paths");
        $this->onIncorrectResponseType = new Handler("Invoked when a controller returns a value which is not an instance of `ResponseInterface`.");
    }

    #[PhaseHandler(Phase::Integrate, \PHP_INT_MAX)]
    private function doIntegrate(): void {
        $this->collectRoutes();
        $this->onReady->trigger();
    }

    private function collectRoutes(): void {
        $benchCollectingRoutes = $this->dev->benchmark("Collecting routes");

        $this->routes = [];
        $this->namedRoutes = [];
        $this->isCollectingRoutes = true;

        if ($this->preCollectedRoutes !== null) {
            $this->add(...$this->preCollectedRoutes);
            $this->preCollectedRoutes = null;
        }

        $this->onCollectRoutes->trigger($this);
        $this->isCollectingRoutes = false;
        \usort($this->routes, function(Route $a, Route $b) {
            return $a->priority - $b->priority;
        });
        $benchCollectingRoutes();
    }

    /**
     * Add a route to the router implementation.
     * 
     * @param Route ...$routes 
     * @throws CodingErrorException 
     */
    public function add(Route ...$routes): void {
        if (!$this->isCollectingRoutes) {
            if ($this->preCollectedRoutes !== null) {
                foreach ($routes as $route) {
                    $this->preCollectedRoutes[] = $route;
                }
                return;
            }
            throw Instrumentation::redirect(new LogicException("Can only add routes as a response to the `\$router->onCollectRoutes` event"), __FILE__);
        }
        
        $this->handlerDispatcher = null;
        foreach ($routes as $route) {
            $this->routes[] = $route;
            if ($route->name !== null) {
                if (isset($this->namedRoutes[$route->name])) {
                    throw new CodingErrorException("A route with the name `{$route->name}` already exists");
                }
                $this->namedRoutes[$route->name] = $route;
            }
        }
    }

    /**
     * Register a route for a GET request
     * 
     * @param string $path 
     * @param Closure|RequestHandlerInterface $handler 
     * @throws CodingErrorException 
     */
    public function get(string $path, Closure|RequestHandlerInterface $handler, string $name=null, int $priority=0): void {
        $this->add(new Route('GET', $path, $handler, $name, $priority));
    }

    /**
     * Register a route for a POST request
     * 
     * @param string $path 
     * @param Closure|RequestHandlerInterface $handler 
     * @throws InvalidArgumentException 
     */
    public function post(string $path, Closure|RequestHandlerInterface $handler, string $name=null, int $priority=0): void {
        $this->add(new Route('POST', $path, $handler, $name, $priority));
    }

    /**
     * Register a route for a PUT request
     * 
     * @param string $path 
     * @param Closure|RequestHandlerInterface $handler 
     * @throws InvalidArgumentException 
     */
    public function put(string $path, Closure|RequestHandlerInterface $handler, string $name=null, int $priority=0): void {
        $this->add(new Route('PUT', $path, $handler, $name, $priority));
    }

     /**
     * Register a route for a PATCH request
     * 
     * @param string $path 
     * @param Closure|RequestHandlerInterface $handler 
     * @throws InvalidArgumentException 
     */
    public function patch(string $path, Closure|RequestHandlerInterface $handler, string $name=null, int $priority=0): void {
        $this->add(new Route('PATCH', $path, $handler, $name, $priority));
    }

     /**
     * Register a route for a DELETE request
     * 
     * @param string $path 
     * @param Closure|RequestHandlerInterface $handler 
     * @throws InvalidArgumentException 
     */
    public function delete(string $path, Closure|RequestHandlerInterface $handler, string $name=null, int $priority=0): void {
        $this->add(new Route('DELETE', $path, $handler, $name, $priority));
    }

    /**
     * Register a route for a HEAD request
     * 
     * @param string $path 
     * @param Closure|RequestHandlerInterface $handler 
     * @throws InvalidArgumentException 
     */
    public function head(string $path, Closure|RequestHandlerInterface $handler, string $name=null, int $priority=0): void {
        $this->add(new Route('HEAD', $path, $handler, $name, $priority));
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $method = \strtoupper($request->getMethod());

        [ $targetUri ] = \explode("?", $request->getRequestTarget(), 2);
        $handlerRouteInfo = $this->getDispatcher()->dispatch($method, $targetUri);
        switch ($handlerRouteInfo[0]) {
            case Dispatcher::NOT_FOUND:

                if (\substr($targetUri, -1) !== '/') {
                    // Check if we should redirect
                    $handlerRouteInfo = $this->getDispatcher()->dispatch($method, $targetUri . '/');
                    if ($handlerRouteInfo[0] === Dispatcher::FOUND) {
                        $redirectTo = $this->env->baseUrl.$targetUri.'/';

                        if ($method === 'GET') {
                            return new Redirect($redirectTo, 301);
                        } else {
                            return new Redirect($redirectTo, 307);
                        }
                    }
                }

                throw new NotFoundException(new Translatable("The page {targetUri} was not found", [ 'targetUri' => $targetUri ]), 404);
            case Dispatcher::METHOD_NOT_ALLOWED:
                return new Response(
                    \json_encode(['valid_methods' => $handlerRouteInfo[1]]), 
                    [
                        'Content-Type' => 'application/json',
                        'Allow' => implode(", ", $handlerRouteInfo[1]),
                        'Cache-Control' => 'public, max-age=10',
                    ],
                    405
                );
            case Dispatcher::FOUND:
                /** @var Route */
                $route = $handlerRouteInfo[1];

                if (!empty($handlerRouteInfo[2])) {
                    foreach ($handlerRouteInfo[2] as $name => $value) {
                        $request = $request->withAttribute($name, $value);
                    }    
                }

                $request = $request->withAttribute(Route::class, $route);
        
                return $this->handleRoute($request, $route, $handlerRouteInfo[2]);
        }
    }

    /**
     * Invoke the Route::$handler with a Request instance
     * 
     * @param ServerRequestInterface $request 
     * @param Route $route 
     * @return ResponseInterface 
     * @throws NotFoundExceptionInterface 
     * @throws ContainerExceptionInterface 
     * @throws ReflectionException 
     * @throws LogicException 
     * @throws InternalErrorException 
     */
    protected function handleRoute(ServerRequestInterface $request, Route $route, array $routeVariables): ResponseInterface {
        if ($route->handler instanceof RequestHandlerInterface) {
            return $route->handler->handle($request);
        } elseif ($route->handler instanceof Closure) {
            return (new ClosureRequestHandler($this->container, $route->handler, $routeVariables))->handle($request);
            /*
            $extraObjects = [
                $request,
                $route,
            ];
            foreach ($request->getAttributes() as $key => $val) {
                if (\is_object($val)) {
                    $extraObjects[] = $val;
                }
            }
            $result = $this->container->invoke(
                $route->handler,
                namedArguments: $routeVariables,
                extraObjects: $extraObjects
            );
            if ($result instanceof ResponseInterface) {
                return $result;
            }

            $newResult = $this->onIncorrectResponseType->trigger($result, $request, $routeVariables);
            if ($newResult instanceof ResponseInterface) {
                return $newResult;
            }
            return $result;
            */
        } else {
            throw new InternalErrorException("Handler failed for `$route`");
        }
    }

    /**
     * Returns the request dispatcher for routes that end with a request handler.
     * 
     * @return Dispatcher 
     */
    protected function getDispatcher(): Dispatcher {
        if (!$this->onReady->wasTriggered()) {
            throw new LogicException("Can't provide routes before the `\$router->onReady` event");
        }
        if (!$this->handlerDispatcher) {
            $perf = $this->dev->benchmark('Getting router dispatcher');
            try {
                $this->handlerDispatcher = cachedDispatcher(function(RouteCollector $r) {
                    foreach ($this->routes as $route) {
                        $r->addRoute($route->method, $route->path, $route);
                    }
                }, [
                    'cacheFile' => $this->getCacheFilePath(),
                    'cacheDisabled' => !!\DEBUG,
                ]);
            } catch (RouteLoadFailedException $e) {
                unlink($this->getCacheFilePath());
                $this->handlerDispatcher = cachedDispatcher(function(RouteCollector $r) {
                    foreach ($this->routes as $route) {
                        $r->addRoute($route->method, $route->path, $route);
                    }
                }, [
                    'cacheFile' => $this->getCacheFilePath(),
                    'cacheDisabled' => !!\DEBUG,
                ]);
            } finally {
                $perf();
            }
        }
        return $this->handlerDispatcher;
    }

    protected function getCacheFilePath(): string {
        return $this->env->fileCacheRoot . '/router.dat';
    }

    protected function hasValidCache(): bool {
        return \file_exists($this->getCacheFilePath()) && \filemtime($this->getCacheFilePath()) > \time() - 5;
    }

    /**
     * Returns the middleware for routes that define a middleware.
     * 
     * @return Dispatcher 
     */
    protected function getMiddlewareDispatcher(): Dispatcher {
        if (!$this->middlewareDispatcher) {
            $this->middlewareDispatcher = simpleDispatcher(function(RouteCollector $r) {
                foreach ($this->middlewareRoutes as $route) {
                    $r->addRoute($route->method, $route->path, $route);
                }
            });
        }
        return $this->middlewareDispatcher;
    }

    public function url(string $name, int|float|string ...$args): UriInterface {
        if (!isset($this->namedRoutes[$name])) {
            throw new UnknownRouteException("Unknown named route `$name`");
        }

        $route = $this->namedRoutes[$name];
        $path = $route->path;
        if (!\str_contains($path, '{')) {
            return new Uri($this->env->baseUrl . '/' . \ltrim($path, '/'));
        } else {
            $offset = 0;
            $path = \preg_replace_callback('/{[a-zA-Z0-9]+[^}]*}/', function() use (&$offset, $args) {
                if (!\array_key_exists($offset, $args)) {
                    throw new FubberCodingErrorException("Too few arguments for the route");
                }
                return $args[$offset++];
            }, $path);
            return new Uri($this->env->baseUrl . '/' . \ltrim($path, '/'));
        }
    }
}