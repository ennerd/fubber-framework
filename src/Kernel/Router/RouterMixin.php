<?php
namespace Fubber\Kernel\Router;

use Closure;
use Fubber\CodingErrorException;
use Fubber\Kernel\Config\ConfigMixin;
use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Environment\EnvironmentMixin;
use Fubber\Kernel\Instrumentation\InstrumentationMixin;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Mixins\MixinConstructor;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

trait RouterMixin {
    use ServicesMixin;
    use EnvironmentMixin;
    use InstrumentationMixin;
    use ConfigMixin;

    public readonly Router $router;

    #[MixinConstructor(__TRAIT__, ServicesMixin::class, EnvironmentMixin::class, InstrumentationMixin::class, ConfigMixin::class)]
    private function constructRouter(): void {
        $this->container->addSingleton(
            Router::class,
            $this->router = new Router($this->env, $this->container, $this->instrumentation)
        );
        $this->addExtension($this->router);
    }

}