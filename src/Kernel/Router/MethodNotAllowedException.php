<?php
namespace Fubber\Kernel\Router;

use Fubber\BadRequestException;

class MethodNotAllowedException extends BadRequestException {
    public $allowedMethods;
    public ?int $httpCode = 405;
    public $httpStatus = 'Method not allowed';

	public function __construct($allowedMethods) {
	    $this->suggestion = 'Use one of the following methods to access this resource: '.implode(", ", $allowedMethods);
		$this->allowedMethods = $allowedMethods;
        parent::__construct("Method not allowed.", 405);
    }

    public function jsonSerialize() {
        return ['error' => 'HTTP method not supported exception', 'allowedMethods' => $this->allowedMethods, 'message' => $this->getMessage(), 'code' => $this->getCode()];
    }
}
