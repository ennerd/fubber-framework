<?php
namespace Fubber\Kernel\Router;

use Fubber\LogicException;

class DuplicateRouteException extends LogicException {}