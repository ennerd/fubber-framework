<?php
namespace Fubber\Kernel\I18n;

use Fubber\I18n\I18nController;
use Fubber\Kernel;
use Fubber\Kernel\Controllers\ControllersMixin;
use Fubber\Mixins\MixinConstructor;

/**
 * @mixin Kernel
 */
trait I18nMixin {
    use ControllersMixin;

    #[MixinConstructor(__TRAIT__, ControllersMixin::class)]
    private function constructI18n(): void {
        $this->controllers->add(new I18nController);
    }
}