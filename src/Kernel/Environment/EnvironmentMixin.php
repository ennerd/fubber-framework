<?php
namespace Fubber\Kernel\Environment;

use Fubber\Kernel\Microcache\MicrocacheMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Mixins\MixinConstructor;

trait EnvironmentMixin {
    use ExtensionsMixin;
    use ServicesMixin;
    use MicrocacheMixin;

    /**
     * Provides configuration information from the server environment.
     * 
     * @var Environment
     */
    public readonly Environment $env;

    #[MixinConstructor(__TRAIT__, ServicesMixin::class, MicrocacheMixin::class)]
    private function constructEnvironment(): void {
        $this->container->addSingleton(Environment::class, $this->env = new Environment($this->microcache->withNamespace(Environment::class)));        
        $this->addExtension($this->env);
    }
}