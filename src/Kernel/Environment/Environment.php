<?php
namespace Fubber\Kernel\Environment;

use Dotenv\Dotenv;
use Fubber\ConfigErrorException;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Microcache\MicrocacheInterface;

/**
 * This class defines the configurable environment variables for the framework.
 * 
 * Environment variables are generally used for configuration which may change
 * between installations of the same application. For defining services and
 * application internal wiring, the $APP_ROOT/config/ files should be used.
 * 
 * After environment variables have been imported, they are unset to avoid
 * leaking sensitive data.
 */
final class Environment extends KernelExtension {

    /**
     * The application HTTP host name
     * 
     * Default: getenv('HTTP_HOST') || $_SERVER['HTTP_HOST'] || '127.0.0.1'
     *
     * @var string
     */
    public readonly string $httpHost;

    /**
     * The path to the project root, generally where composer.json belongs.
     * 
     * Default: getenv('ROOT') || dirname for composer.json
     *
     * @var string
     */
    public readonly string $root;

    /**
     * The document root, where the web server will serve files from.
     * Typically this is the ROOT/html path, but it may also be identical
     * to the ROOT path itself.
     * 
     * Default: getenv('DOCUMENT_ROOT') || getenv('ROOT')
     *
     * @var string
     */
    public readonly string $baseRoot;

    /**
     * The public URL of the application. This url should match the base url.
     * 
     * Default: getenv('BASE_URL') || derived from $_SERVER['HTTPS'] and $_SERVER['HTTP_HOST'].
     *
     * @var string
     */
    public readonly string $baseUrl;

    /**
     * The path to the application source code, typically in ROOT/app.
     * 
     * Default: getenv('APP_ROOT') || $this->root . '/app'
     *
     * @var string
     */
    public readonly string $appRoot;

    /**
     * The path where uploaded files from users are placed. This might be
     * a placed outside of the web root depending on security requirements.
     * 
     * Default: getenv('UPLOAD_ROOT') || $this->root . '/files'
     *
     * @var string
     */
    public readonly string $uploadRoot;

    /**
     * The URL where uploaded files from users are web accessible. This
     * URL may be NULL if upload root is outside of the base root.
     * 
     * Default: getenv('UPLOAD_URL') || inferred based on $this->baseUrl, $this->baseRoot and $this->uploadRoot || NULL
     *
     * @var string
     */
    public ?string $uploadUrl;

    /**
     * The path where private generated files are stored. These files MUST NOT
     * be web accessible, either through server configuration or by being placed
     * outside of the base root.
     * 
     * Default: getenv('PRIV_ROOT') || throw new ConfigErrorException
     *
     * @var string
     */
    public readonly string $privRoot;

    /**
     * The path where application database migration files are stored.
     * 
     * Default: getenv('MIGRATIONS_ROOT') || $this->appRoot . '/migrations'
     *
     * @var string
     */
    public readonly string $migrationsRoot;

    /**
     * The path where application database seed files are stored.
     * 
     * Default: $this->appRoot . '/seeds'
     *
     * @var string
     */
    public readonly string $seedsRoot;

    /**
     * The path where application configuration files are stored.
     * 
     * Default: $this->appRoot . '/config'
     *
     * @var string
     */
    public readonly string $configRoot;

    /**
     * The path where file based caching can be done. This location is
     * not required to be globally shared between servers.
     * 
     * Default: $this->privRoot . '/cache'
     *
     * @var string
     */
    public readonly string $fileCacheRoot;

    /**
     * The path where template files for the application is stored.
     * 
     * Default: $this->appRoot . '/views'
     * 
     * @var string
     */
    public readonly string $viewsRoot;

    /**
     * An application wide secret seed string which is used to sign
     * URLs and other basic cryptographic stuff.
     *
     * @var string
     */
    public readonly string $salt;

    /**
     * The e-mail address which is used as a return address for e-mail
     * unless another e-mail address is specified via the API.
     * 
     * Default: getenv('EMAIL_ADDRESS') || 'no-reply@' . $_SERVER['HTTP_HOST']
     *
     * @var string
     */
    public readonly string $emailAddress;

    /**
     * The sender name for e-mails unless another sender name is specified
     * via the API.
     * 
     * Default: getenv('EMAIL_NAME') || 'no-reply@' . $_SERVER['HTTP_HOST']
     *
     * @var string
     */
    public readonly string $emailName;

    /**
     * The SMTP server host name to use when sending e-mail from the application.
     * 
     * Default: getenv('SMTP_HOST') || NULL (uses the PHP configuration)
     *
     * @var ?string
     */
    public readonly ?string $smtpHost;

    /**
     * The SMTP server port number to use when sending e-mail from the application
     *
     * Default: getenv('SMTP_PORT') || 25 (the default SMTP port according to the specification)
     * 
     * @var ?string
     */
    public readonly ?int $smtpPort;

    /**
     * The application default language
     * 
     * Default: getenv('DEFAULT_LANG') || 'en'
     *
     * @var string
     */
    public readonly string $lang;

    /**
     * Is the application running in development mode/debug mode and are we allowed to provide
     * additional error information in error pages. This configuration option also can trigger
     * stricter checking at the expense of performance.
     * 
     * @see \DEBUG
     *
     * @var boolean
     */
    public readonly bool $debug;

    /**
     * The primary time zone for the application. This affects what time is considered night-time
     * for CRON jobs and the default timezone for users.
     * 
     * Default: getenv('TZ') || \date_default_timezone_get();
     * 
     * @var string
     */
    public readonly string $timeZone;

    /**
     * Caches environment variables fetched from the .env file, $_SERVER and getenv()
     *
     * @var array
     */
    protected array $env;

    protected readonly MicrocacheInterface $cache;

    public function getExtensionName(): string
    {
        return 'Environment configuration variables';
    }

    public function __construct(MicrocacheInterface $cache) {
        $this->cache = $cache;

        if (isset($_ENV['ROOT'])) {
            $this->root = $_ENV['ROOT'];
        } elseif ($value = getenv('ROOT')) {
            $this->root = $value;
        } else {
            // value is determined by the path to composer.json
            $this->root = dirname(dirname(dirname((new \ReflectionClass('\Composer\Autoload\ClassLoader'))->getFileName())));
        }

        /**
         * Find the .env file primarily in the folder above the ./composer.json or in the
         * same folder and load it.
         * 
         * @todo Move this to a separate package.
         */
        $env = $this->cache->fetch('$kernel->env', function() {
            if (file_exists($file = dirname($this->root) . '/.env')) {
                $env = Dotenv::parse(file_get_contents($file));
            } elseif (file_exists($file = dirname($this->root) . '/.env')) {
                $env = Dotenv::parse(file_get_contents($file));
            }
            return $env;
        }, 5);

        foreach ($_SERVER as $k => $v) {
            if (!str_starts_with($k, 'HTTP_')) {
                $env[$k] = $v;
            }
        }

        $this->env = $env;

        if (!empty($env['HTTP_HOST'])) {
            // value is configured specifically
            $this->httpHost = $env['HTTP_HOST'];
        } elseif (!empty($_SERVER['HTTP_HOST'])) {
            // value is determined by the user supplied 'Host' header
            $this->httpHost = $_SERVER['HTTP_HOST'];
        } else {
            // fallback value
            $this->httpHost = '127.0.0.1';
        }

        if (!empty($env['DOCUMENT_ROOT'])) {
            // configuration value
            $this->baseRoot = $env['DOCUMENT_ROOT'];
        } elseif (!empty($_SERVER['DOCUMENT_ROOT'])) {
            $this->baseRoot = $_SERVER['DOCUMENT_ROOT'];
        } else {
            $this->baseRoot = $this->root;
        }

        if (!empty($env['BASE_URL'])) {
            $this->baseUrl = $env['BASE_URL'];
        } else {
            $this->baseUrl = 'http' .
                ((!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') ? 's' : '') .
                '://' .
                $this->httpHost;
        }

        if (!empty($env['APP_ROOT'])) {
            $this->appRoot = $env['APP_ROOT'];
        } else {
            $this->appRoot = $this->root . \DIRECTORY_SEPARATOR . 'app';
        }

        if (!empty($env['UPLOAD_ROOT'])) {
            $this->uploadRoot = $env['UPLOAD_ROOT'];
        } else {
            $this->uploadRoot = $this->root . \DIRECTORY_SEPARATOR . 'files';
        }

        if (!empty($env['UPLOAD_URL'])) {
            $this->uploadUrl = $env['UPLOAD_URL'];
        } else {
            // if the $this->uploadRoot is inside of $this->baseRoot, infer the value
            if (\str_starts_with($this->uploadRoot, $this->baseRoot)) {
                $length = \strlen($this->baseRoot);
                $this->uploadUrl = $this->baseUrl . \strtr(\substr($this->uploadRoot, $length), [ \DIRECTORY_SEPARATOR => '/']);
            } else {
                $this->uploadUrl = null;
            }
        }

        if (!empty($env['PRIV_ROOT'])) {
            $this->privRoot = $env['PRIV_ROOT'];
            if (\str_starts_with($this->privRoot, $this->baseRoot)) {
                throw (new ConfigErrorException("Environment variable 'PRIV_ROOT' is configured to a file path inside the web server root"))
                    ->withStatus(500, 'Internal Configuration Error')
                    ->withSuggestion("Set the PRIV_ROOT environment variable (you can puth PRIV_ROOT=/some/path in a .env file along with composer.json)");
            }
        } else {
            if (\PHP_SAPI === 'cli' && !empty($env['HOME'])) {
                $this->privRoot = $this->root . \DIRECTORY_SEPARATOR . 'var';
            } else {
                throw (new ConfigErrorException('Environment variable "PRIV_ROOT" is not configured'))
                    ->withStatus(500, 'Internal Configuration Error')
                    ->withSuggestion("Set the PRIV_ROOT environment variable (optionally by adding PRIV_ROOT=/some/path in an .env file along with composer.json)");
            }
        }


        if (!empty($env['MIGRATIONS_ROOT'])) {
            $this->migrationsRoot = $env['MIGRATIONS_ROOT'];
        } else {
            $this->migrationsRoot = $this->appRoot . \DIRECTORY_SEPARATOR . 'migrations';
        }

        if (!empty($env['SEEDS_ROOT'])) {
            $this->seedsRoot = $env['SEEDS_ROOT'];
        } else {
            $this->seedsRoot = $this->appRoot . \DIRECTORY_SEPARATOR . 'seeds';
        }

        if (!empty($env['CONFIG_ROOT'])) {
            $this->configRoot = $env['CONFIG_ROOT'];
        } else {
            $this->configRoot = $this->appRoot . \DIRECTORY_SEPARATOR . 'config';
        }

        if (!empty($env['FILE_CACHE_ROOT'])) {
            $this->fileCacheRoot = $env['FILE_CACHE_ROOT'];
        } else {
            $this->fileCacheRoot = $this->privRoot . \DIRECTORY_SEPARATOR . 'cache';
        }

        if (!empty($env['VIEWS_ROOT'])) {
            $this->viewsRoot = $env['VIEWS_ROOT'];
        } else {
            $this->viewsRoot = $this->appRoot . \DIRECTORY_SEPARATOR . 'views';
        }

        if (!empty($env['SALT'])) {
            $this->salt = $env['SALT'];
        } else {
            if (\PHP_SAPI === 'cli') {
                if (!\is_dir($varLocal = $this->privRoot . \DIRECTORY_SEPARATOR . 'local')) {
                    $m = \umask(0);
                    mkdir($varLocal, 0750, true);
                    \umask($m);
                }
                if (!\is_file($saltFile = $this->privRoot . \DIRECTORY_SEPARATOR . 'local' . \DIRECTORY_SEPARATOR . 'salt.json')) {
                    $salt = \base64_encode(\random_bytes(12));
                    $saltStructure = [
                        'about' => 'Automatically generated salt for Fubber Framework',
                        'generated' => gmdate('c'),
                        'salt' => $salt
                    ];
                   
                    \file_put_contents($saltFile, \json_encode($saltStructure, \JSON_PRETTY_PRINT));

                    $this->salt = $salt;
                } else {
                    $saltStructure = \json_decode(\file_get_contents($saltFile));
                    $this->salt = $saltStructure->salt;
                }
            } else {
                throw (new ConfigErrorException("Environment variable SALT is not configured"))
                ->withStatus(500, 'Internal Configuration Error')
                ->withSuggestion('A SALT is a secret string for the application which makes it possible to perform the HMAC algorithm. Add SALT=hard-to-guess-string to your .env file.');
            }
        }

        if (!empty($env['SMTP_HOST'])) {
            $this->smtpHost = $env['SMTP_HOST'];
        }

        if (!empty($env['SMTP_PORT'])) {
            $this->smtpPort = (int) $env['SMTP_PORT'];
        } else {
            $this->smtpPort = 25;
        }

        if (!empty($env['DEFAULT_LANG'])) {
            $this->lang = $env['DEFAULT_LANG']; 
        } else {
            $this->lang = 'en';
        }

        if (!empty($env['TZ'])) {
            $this->timeZone = $env['TZ'];
        } else {
            $this->timeZone = $this->cache->fetch('date_default_timezone_get()', function() {
                return \date_default_timezone_get();
            }, 10);
        }

        $this->debug = \defined('DEBUG') ? !!\DEBUG : false;
    }

    public function init(): void {
        $this->onReady->trigger();
    }

    /**
     * Get an environment variable.
     * 
     * This function should be thread safe, unless other parts of your application calls `getenv`
     * directly. Thread safety is achieved by caching the environment variable and avoiding future
     * calls to get the same environment variable, but there is no guarantee that it doesn't happen..
     *
     * @param string $key
     * @return string|null
     */
    public function get(string $key): ?string {
        if (!empty($this->env[$key])) {
            return $this->env[$key];
        }
        $value = \getenv($key);
        if ($value) {
            $this->env[$key] = $value;
        }
        return $this->env[$key] ?? null;
    }
}