<?php
namespace Fubber\Kernel\Microcache;

use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Microcache\MicrocacheFactory;
use Fubber\Microcache\MicrocacheInterface;
use Fubber\Mixins\MixinConstructor;

trait MicrocacheMixin {
    use ServicesMixin;

    public readonly MicrocacheInterface $microcache;

    #[MixinConstructor(__TRAIT__, ServicesMixin::class)]
    private function constructMicrocache(): void {

        $this->container->add(MicrocacheInterface::class, MicrocacheFactory::class, Scope::Singleton);

        $this->microcache = $this->container->get(MicrocacheInterface::class);
    }

}