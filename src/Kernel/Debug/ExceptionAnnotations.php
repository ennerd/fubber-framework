<?php
namespace Fubber\Kernel\Debug;

use Closure;
use Fubber\I18n\Translatable;
use Fubber\Kernel\State;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Allows attaching additional debug information to exceptions thrown,
 * even if those exceptions don't use the {@see Fubber\Traits\ExceptionTrait}.
 * 
 * To add debug information to exceptions, use 
 * 
 * @package Fubber\Kernel\ExceptionHandlers
 */
class ExceptionAnnotations {
    /**
     * Provides a suggestion for fixing the problem; for example a hint about
     * the incorrect usage or a concrete change.
     * 
     * @var string|Translatable|null
     */
    public string|Translatable|null $suggestion = null;

    /**
     * Provides a description of the exception class.
     * 
     * @var string|Translatable|null
     */
    public string|Translatable|null $description = null;

    /**
     * The HTTP status code which this exception defaults to sending to the client.
     * 
     * @var null|int
     */
    public ?int $httpStatusCode = null;

    /**
     * The HTTP reason phrase which this exception defaults to sending to the client.
     * 
     * @var null|string
     */
    public ?string $httpReasonPhrase = null;

    /**
     * The HTTP request that was being processed when the exception was thrown.
     * 
     * @var null|RequestInterface
     */
    public null|RequestInterface $request = null;

    /**
     * The State object that was being used when the exception was thrown.
     * 
     * @var null|State
     */
    public null|State $state = null;

    /**
     * The closure object which caused the exception, if available.
     * 
     * @var null|Closure
     */
    public null|Closure $closure = null;

    /**
     * The request handler instance which caused the exception, if available
     * 
     * @var null|RequestHandlerInterface
     */
    public null|RequestHandlerInterface $requestHandler = null;

    /**
     * Other information which may be relevant for debugging the exception.
     * 
     * @var array
     */
    public array $extraData = [];
}

