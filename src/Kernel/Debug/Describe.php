<?php
namespace Fubber\Kernel\Debug;

use Closure;
use Fubber\Kernel;
use InvalidArgumentException;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionIntersectionType;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionType;
use ReflectionUnionType;

/**
 * Utilities for generating a descriptive name from reflection and other debug 
 * information sources.
 * 
 * @package Fubber\Kernel\Debug
 */
class Describe {

    public static function auto(mixed $what): string {
        if (\is_int($what)) {
            return 'int(' . $what . ')';
        } elseif (\is_float($what)) {
            return 'float(' . $what . ')';
        } elseif (\is_string($what)) {
            if (\strlen($what) > 20) {
                $what = \substr($what, 0, 17) . '...';
            }
            return 'string('.\json_encode($what).')';
        } elseif (\is_bool($what)) {
            return 'bool('.\json_encode($what).')';
        } elseif (\is_array($what)) {
            try {
                $desc = \json_encode($what);
            } catch (\Throwable $e) {
                $desc = \count($what) . ' elems';
            }
            if (\strlen($desc) > 50) {
                $desc = \substr($desc, 0, 47) . '...';
            }
            return 'array('.$desc.')';
        } elseif ($what instanceof Closure) {
            return self::function(new ReflectionFunction($what));
        } elseif ($what instanceof ReflectionFunctionAbstract) {
            return 'function(' . self::function($what) . ')';
        } elseif ($what instanceof \Throwable) {
            return \get_class($what) . '(' . $what->getFile() . ':' . $what->getLine() . ')';
        } elseif (\is_object($what)) {
            return 'object='.\get_class($what);
        } else {
            return \get_debug_type($what);
        }
    }

    public static function autoArray(array $what): array {
        $result = [];
        foreach ($what as $element) {
            $result[] = self::auto($element);
        }
        return $result;
    }

    public static function exception(\Throwable $e): string {
        return 'exception(' . \get_class($e) . ', ' . \json_encode($e->getMessage()) . ')';
    }

    public static function stackTrace(array $trace=null): string {
        if ($trace === null) {
            $trace = \array_reverse(\debug_backtrace(0, 10));
            \array_pop($trace);
        } else {
            $trace = \array_reverse($trace);
        }
        $rootLength = \strlen(Kernel::getComposerPath()) + 1;
        return "TRACE:".\implode(" < ", \array_map(function(array $t) use ($rootLength) {
            return \substr($t['file'], $rootLength) . ':' . $t['line']."(" . implode(",", \array_map(self::auto(...), $t['args'])) . ")";
        }, $trace));
    }

    public static function function(ReflectionFunctionAbstract $reflectionFunction): string {
        if ($rc = $reflectionFunction->getClosureScopeClass()) {
            $signature = $rc->getName()."::";
        } elseif ($reflectionFunction->returnsReference()) {
            $signature = 'function &';
        } else {
            $signature = 'function ';
        }

        $name = $reflectionFunction->getName();
        if (\str_ends_with($name, '{closure}')) {
            $signature .= '{closure on line '.$reflectionFunction->getStartLine().'}';
        } else {
            $signature .= $name;
        }

        $signature .= "(" . self::parameterList($reflectionFunction->getParameters()) . ")";

        if ($reflectionFunction->hasReturnType()) {
            $signature .= ': ' . self::type($reflectionFunction->getReturnType());
        }

        return $signature;
    }

    public static function parameter(ReflectionParameter $reflectionParameter): string {
        $signature = '';
        if ($reflectionParameter->hasType()) {
            $signature = self::type($reflectionParameter->getType()) . ' ';
        }
        if (!$reflectionParameter->canBePassedByValue()) {
            $signature .= '&';
        }
        if ($reflectionParameter->isVariadic()) {
            $signature .= '...';
        }
        $signature .= '$' . $reflectionParameter->getName();

        if ($reflectionParameter->isDefaultValueAvailable()) {
            $signature .= '=';
            if ($reflectionParameter->isDefaultValueConstant()) {
                $signature .= $reflectionParameter->getDefaultValueConstantName();
            } else {
                $signature .= \var_export($reflectionParameter->getDefaultValue(), true);
            }
        }

        return $signature;
    }

    public static function type(ReflectionType $reflectionType): string {
        $signature = '';
        if ($reflectionType instanceof ReflectionNamedType) {
            $signature = $reflectionType->getName();
            if ($reflectionType->allowsNull()) {
                $signature = '?' . $reflectionType;
            }
            return $signature;
        } elseif ($reflectionType instanceof ReflectionUnionType) {
            $parts = [];
            foreach ($reflectionType->getTypes() as $subType) {
                $parts[] = self::type($subType);
            }
            return implode("|", $parts);
        } elseif ($reflectionType instanceof ReflectionIntersectionType) {
            $parts = [];
            foreach ($reflectionType->getTypes() as $subType) {
                $parts[] = self::type($subType);
            }
            return implode("^", $parts);
        } else {
            throw new InvalidArgumentException("Not supported to build signature from `".$reflectionType::class."`");
        }        
    }

    /**
     * Parameter list
     * @param ReflectionParameter[] $parameters 
     * @return string 
     */
    protected static function parameterList(array $parameters): string {
        $strings = [];
        foreach ($parameters as $parameter) {
            $strings[] = self::parameter($parameter);
        }
        return \implode(", ", $strings);
    }
}