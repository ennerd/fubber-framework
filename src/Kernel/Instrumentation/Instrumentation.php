<?php
namespace Fubber\Kernel\Instrumentation;

use Closure;
use Fubber\CodingErrorException;
use Fubber\I18n\Translatable;
use Fubber\IException;
use Fubber\Kernel;
use Fubber\Kernel\Debug\Describe;
use Fubber\Kernel\Debug\ExceptionAnnotations;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\State;
use Fubber\LogicException;
use Fubber\ObjectAnnotator\ObjectAnnotator;
use Fubber\PHP\ExceptionTool;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ReflectionClass;
use ReflectionProperty;
use Throwable;

/**
 * Class provides performance metrics and tools for improving developers
 * experience.
 * 
 * @package Fubber\Kernel
 */
final class Instrumentation extends KernelExtension {

    /**
     * Stores additional information about exceptions
     * 
     * @var null|ObjectAnnotator
     */
    private static ?ObjectAnnotator $exceptionInfo = null;

    /**
     * Holds ongoing time measurements
     * 
     * @var string[]
     */
    protected array $measurementStack = [];

    /**
     * The offset of the next available measurement stack index
     * 
     * @var int
     */
    protected int $measurementStackOffset = 0;

    protected array $measurements = [];
    protected int $measurementIndex = 0;

    public function getExtensionName(): string {
        return 'Developer Experience';
    }

    /**
     * Start a performance monitor for some section of code. The timer starts
     * running immediately when this function is called, and the returned
     * function completes the measurement.
     * 
     * @param string $name 
     * @param float $loggableTimeRequirement If the time spent is greater than this number of seconds, it will be logged
     * @return Closure 
     */
    public function benchmark(string|array $name, float $loggableTimeRequirement = 0): Closure {
        $t = \hrtime(true);
        $stackOffset = $this->measurementStackOffset++;
        $measurementIndex = $this->measurementIndex++;
        $this->measurementStack[$stackOffset] = $name;
        $this->measurements[$measurementIndex] = null;
        $done = false;
        return function() use ($name, $t, $stackOffset, $measurementIndex, &$done, $loggableTimeRequirement) {
            if ($done) {
                Kernel::error("Triggered the `{name}` benchmark twice", ['name' => $name]);
                return;
            }
            if ($stackOffset !== $this->measurementStackOffset - 1) {
                throw new CodingErrorException("Can't complete the `$name` measurement without completing `{$this->measurementStack[$this->measurementStackOffset-1]}` first");
            }
            $elapsed = (\hrtime(true) - $t) / 1000000;

            if ($elapsed > $loggableTimeRequirement * 1000) {
                $elapsedStr = \number_format($elapsed, 2);
                $starCount = \round($elapsed * \pow(4, $stackOffset));
                if ($starCount > 15) {
                    $starStr = '******************!';
                } else {
                    $starStr = \str_repeat("*", $starCount);
                }
                Kernel::debug("{indent}{name}{pad}{elapsed} ms {stars}", [ 
                    'name' => $name,
                    'elapsed' => $elapsedStr,
                    'indent' => \str_repeat("  ", $stackOffset),
                    'pad' => \str_repeat(" ", \max(1, 60 - ($stackOffset * 2) - \mb_strlen($name) - \strlen($elapsedStr))),
                    'stars' => $starStr,
                ]);
            }
            $this->measurements[$measurementIndex] = new class($elapsed, $stackOffset, $name) {
                public function __construct(
                    public readonly float $elapsed,
                    public readonly int $depth,
                    public readonly string $name
                ) {}

                public function __toString(): string {
                    return 
                        \str_repeat('  ', $this->depth) .
                        \str_pad(\number_format($this->elapsed, 2), 10, ' ', \STR_PAD_LEFT) .
                        '  ' .
                        
                        $this->name;
                }
            };
            --$this->measurementStackOffset;
        };
    }

    public function dumpPerformanceData(): void {
        echo "<pre>\n";
        echo implode("\n", $this->getPerformanceMeasurements())."\n";
        echo "</pre>\n";
        die();
    }

    public function getPerformanceMeasurements(): array {
        return $this->measurements;
    }

    /**
     * Log an exception. When exceptions are logged this way, the application generally
     * should resume without rethrowing the exception. You can optionally annotate
     * the exception object with additional data here {@see static::setExceptionInfo()}.
     * 
     * @param Throwable $exception 
     * @param string|Translatable|null $suggestion 
     * @param string|Translatable|null $description 
     * @param RequestInterface|null $request 
     * @param State|null $state 
     * @param array $extraData 
     * @param int|null $httpStatusCode 
     * @param string|null $httpReasonPhrase 
*/
    public function logException(
        Throwable $exception,
        string|Translatable $suggestion=null,
        string|Translatable $description=null,
        RequestInterface $request=null,
        State $state=null,
        Closure $closure=null,
        array $extraData=[],
        int $httpStatusCode=null,
        string $httpReasonPhrase=null
    ): void {
        self::setExceptionInfo(
            $exception,
            suggestion: $suggestion,
            description: $description,
            request: $request,
            state: $state,
            closure: $closure,
            extraData: $extraData,
            httpStatusCode: $httpStatusCode,
            httpReasonPhrase: $httpReasonPhrase
        );
        Kernel::debug("".$exception::class.": ".$exception->getMessage()." (code=".$exception->getCode().") in ".$exception->getFile().":".$exception->getLine()." with trace ".$exception->getTraceAsString());
    }

    /**
     * Utility function for adding additional help for any exception being thrown.
     * This additional help is meant to help the developer pinpoint and fix the
     * problem that is occurring.
     * 
     * @param Throwable $exception 
     * @param string|Translatable|null $suggestion 
     * @param string|Translatable|null $description 
     * @param RequestInterface|null $request 
     * @param State|null $state 
     * @param array $extraData 
     * @param int|null $httpStatusCode 
     * @param string|null $httpReasonPhrase 
     * @param int|string|null $redirect
     * @return Throwable 
     * @throws LogicException 
     */
    public static function setExceptionInfo(
        Throwable $exception,
        string|Translatable $suggestion=null,
        string|Translatable $description=null,
        RequestInterface $request=null,
        State $state=null,
        Closure $closure=null,
        array $extraData=[],
        int $httpStatusCode=null,
        string $httpReasonPhrase=null,
        RequestHandlerInterface $requestHandler=null,
        int|string $redirect=null,
    ): Throwable {
        if ($redirect !== null) {
            static::redirect($exception, $redirect);
        }
        $info = self::getExceptionAnnotations($exception);
        $info->suggestion = $suggestion ?? $info->suggestion;
        $info->description = $description ?? $info->description;
        $info->request = $request ?? $info->request;
        $info->state = $state ?? $info->state;
        $info->closure = $closure ?? $info->closure;
        $info->requestHandler = $requestHandler ?? $info->requestHandler;
        $info->extraData = $extraData + $info->extraData;
        $info->httpStatusCode = $httpStatusCode ?? $info->httpStatusCode;
        $info->httpReasonPhrase = $httpReasonPhrase ?? $info->httpReasonPhrase;
        return $exception;
    }

    /**
     * Redirect an exception to the caller of a particular method or class, or simply
     * remove a number of traces from the top of the call stack.
     * 
     * @param Throwable $exception 
     * @param int|string $removeFromTrace 
     * @return Throwable 
     */
    public static function redirect(Throwable $exception, int|string $removeFromTrace=1): Throwable {
        return (new ExceptionTool($exception))->redirect($removeFromTrace);
    }

    public static function getExceptionInfo(\Throwable $e): ExceptionAnnotations {
        $info = clone self::getExceptionAnnotations($e);
        try {
            if ($e instanceof IException) {
                if (!$info->httpStatusCode) {
                    $info->httpStatusCode = $e->getStatusCode();
                }
                if (!$info->httpReasonPhrase) {
                    $info->httpReasonPhrase = $e->getReasonPhrase();
                }
                if (!$info->description) {
                    $info->description = (string) $e->getExceptionDescription();
                }
                if (!$info->suggestion) {
                    $info->suggestion = (string) $e->getSuggestion();
                }
            }
            if ($info->httpStatusCode === null) {
                $info->httpStatusCode = 500;
            }
            if ($info->httpReasonPhrase === null) {
                $info->httpReasonPhrase = 'Internal Server Error';
            }
            if (!$info->description) {
                $info->description = self::getDocCommentFor($e);
            }
        } catch (\Throwable $e) {}
        return $info;
    }

    /**
     * Get a description of the particular exception instance thrown. This description
     * can be customized by the code that throws the exception by using
     * {@see self::setExceptionInfo()}.
     * 
     * @param Throwable $e 
     * @return Translatable|string 
     * @throws LogicException 
     */
    protected static function getExceptionDescription(\Throwable $e): string|Translatable {
        return Describe::exception($e);
        $info = self::getExceptionAnnotations($e);

        if ($info->description) {
            return $info->description;
        }
        if ($e instanceof IException) {
            return $e->getExceptionDescription();
        }
        $rc = new ReflectionClass($e);
        $docComment = $rc->getDocComment();
        if ($docComment !== false) {
            $docComment = \preg_replace('/^[ \t]*\/\*+[ \t]*|[ \t]*(\*\/$|\*[ \t])/m', '', $docComment);
            return $docComment;
        }
        return "No description for available for `{" . $e::class . "}`";
    }

    protected static function getExceptionSuggestion(\Throwable $e): Translatable|string|null {
        $info = self::getExceptionAnnotations($e);
        if ($info->suggestion !== null) {
            return $info->suggestion;
        }
        if ($e instanceof IException) {
            return $e->getExceptionDescription();
        }
        return null;
    }

    /**
     * Get additional information about an exception thrown, if available.
     * 
     * @param Throwable $e 
     * @return ExceptionAnnotations 
     * @throws LogicException 
     */
    protected static function getExceptionAnnotations(\Throwable $e): ExceptionAnnotations {
        if (self::$exceptionInfo === null) {
            self::$exceptionInfo = new ObjectAnnotator(ExceptionAnnotations::class);
        }
        return self::$exceptionInfo->get($e);
    }

    protected static function getDocCommentFor(object|string $object): ?string {
        $rc = new ReflectionClass($object);
        $docComment = $rc->getDocComment();
        if ($docComment !== false) {
            $docComment = \preg_replace('/^[ \t]*\/\*+[ \t]*|[ \t]*(\*\/$|\*[ \t])/m', '', $docComment);
            return $docComment;
        }
        return null;
    }

}