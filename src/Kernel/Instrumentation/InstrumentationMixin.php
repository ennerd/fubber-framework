<?php
namespace Fubber\Kernel\Instrumentation;

use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Mixins\MixinConstructor;

trait InstrumentationMixin {
    /**
     * Provides developer services
     * 
     * @var Instrumentation
     */
    public readonly Instrumentation $instrumentation;


    #[MixinConstructor(__TRAIT__)]
    private function constructInstrumentation(): void {
        $this->instrumentation = new Instrumentation();
        $this->addExtension($this->instrumentation);
    }
}

