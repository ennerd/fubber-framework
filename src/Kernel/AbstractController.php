<?php
namespace Fubber\Kernel;

use Fubber\ControllerInterface;
use Fubber\JsonView;
use Fubber\Kernel;
use Fubber\NotFoundException;
use Fubber\PlainView;
use Fubber\View;
use JsonSerializable;
use Psr\Http\Message\ResponseInterface;
use stdClass;

/**
 * Abstract Controller class for Fubber Framework. Override the following methods to
 * integrate with the application:
 * 
 * ```
 * public function load(Kernel $kernel): void;
 * public function init(Kernel $kernel): void;
 * ```
 * 
 * In the init function, add routes by calling {@see Kernel::$router}, attach to cron via {@see Kernel::$cron}
 * and so on.
 * 
 * @package Fubber\Kernel
 */
abstract class AbstractController implements ControllerInterface {

    public function load(Kernel $kernel): void { }

    public function init(Kernel $kernel): void { }

    /**
     * Create a View response
     * 
     * @param string $template 
     * @param array $vars 
     * @param array $headers 
     * @param int $statusCode 
     * @param string|null $reasonPhrase 
     * @return ResponseInterface 
     */
    public function view(string $template, array $vars=[], array $headers=[], int $statusCode=200, string $reasonPhrase=null): ResponseInterface {
        return new View($template, $vars, $headers, $statusCode, $reasonPhrase);
    }

    /**
     * Create a JSON view response
     * 
     * @param string|int|float|bool|array|stdClass|JsonSerializable $value 
     * @param array $headers 
     * @param int $statusCode 
     * @param string|null $reasonPhrase 
     * @return ResponseInterface 
     */
    public function json(string|int|float|bool|array|stdClass|JsonSerializable $value, array $headers=[], int $statusCode=200, string $reasonPhrase=null): ResponseInterface {
        return new JsonView($value, $headers, $statusCode, $reasonPhrase);
    }

    /**
     * Create a raw response (plaintext or custom)
     * 
     * @param string $body 
     * @param array $headers 
     * @param int $statusCode 
     * @param string|null $reasonPhrase 
     * @return ResponseInterface 
     */
    protected function plain(string $body, array $headers=[], int $statusCode=200, string $reasonPhrase=null): ResponseInterface {
        return new PlainView($body, $headers, $statusCode, $reasonPhrase);
    }

    protected function error404(string $message): ResponseInterface {
        // It should be possible to continue searching for routes using this method maybe?
        throw new NotFoundException("Page not found");
    }

}