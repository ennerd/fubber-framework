<?php
namespace Fubber\Kernel;

use Fubber\ConfigErrorException;
use Fubber\DS\Vector\Strings;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Filesystem\Filesystem;
use Fubber\Hooks\Filter;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Microcache\MicrocacheInterface;
use Fubber\Util\Vector;
use InvalidArgumentException;

abstract class AbstractPathRegistry extends KernelExtension {

    /**
     * A filter for adding additional paths to the registry
     */
    public readonly Filter $filter;

    protected MicrocacheInterface $microcache;
    protected Filesystem $filesystem;
    /**
     * 
     * @var Vector<string>
     */
    private Vector $paths;

    public function __construct(MicrocacheInterface $microcache, Filesystem $filesystem) {
        $this->microcache = $microcache;
        $this->filesystem = $filesystem;        
        $this->paths = new Vector();
        $this->filter = new Filter("Add or remove paths");
    }

    public function all(): array {
        $result = [];
        foreach ($this->paths as $path) {
            $result[] = $path;
        }
        return $this->filter->filter($result);
    }

    public function add(string $path): void {
        if ($this->paths->contains($path)) {
            throw new InvalidArgumentException("The path `$path` is already registered");
        }
        $this->paths->append($path);
    }

    public function findFiles(string $filename): iterable {
        return $this->microcache->fetch('findFiles:'.$filename, function() use ($filename): array {
            $result = [];
            foreach ($this->all() as $path) {
                if ($this->filesystem->is_file($candidate = $this->filesystem->buildPath($path, $filename))) {
                    $result[] = $candidate;
                }
            }
            return $result;
        }, 15);
    }

    public function findDirs(string $dirname): iterable {
        return $this->microcache->fetch('findDirs:'.$dirname, function() use ($dirname): array {
            $result = [];
            foreach ($this->all() as $path) {
                if ($this->filesystem->is_dir($candidate = $this->filesystem->buildPath($path, $dirname))) {
                    $result[] = $candidate;
                }
            }
            return $result;
        }, 15);
    }

    public function find(string|array $filename, bool $required=true, array|string $extensions=null): ?string {
        return $this->microcache->fetch('find:'.$filename, function() use ($filename, $required, $extensions): ?string {
            foreach ($this->all() as $path) {
                foreach ($extensions ? (array) $extensions :  [''] as $ext) {
                    $candidate = $this->filesystem->buildPath($path, $filename . $ext);
                    if ($this->filesystem->file_exists($candidate)) {
                        return $candidate;
                    }
                }
            }
            if (!$required) {
                return null;
            }
            throw Instrumentation::setExceptionInfo(
                new ConfigErrorException($this->getExtensionName()." could not find file `$filename` in any of the paths."),
                suggestion: "Create the file `$filename`",
                extraData: [
                    'configPaths' => \iterator_to_array($this->paths),
                ]
            );
        }, 15);
    }

}