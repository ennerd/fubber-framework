<?php
namespace Fubber\Kernel\Views;

use Fubber\Kernel\AbstractPathRegistry;

class Views extends AbstractPathRegistry {

    public function getExtensionName(): string {
        return 'View Paths';
    }

}