<?php
namespace Fubber\Kernel\Views;

use Fubber\DS\Vector\Strings;
use Fubber\Kernel;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Environment\EnvironmentMixin;
use Fubber\Kernel\Filesystem\FilesystemMixin;
use Fubber\Kernel\Microcache\MicrocacheMixin;
use Fubber\Mixins\MixinConstructor;

trait ViewsMixin {
    use MicrocacheMixin;
    use FilesystemMixin;
    use ServicesMixin;
    use EnvironmentMixin;

    public readonly Views $views;

    #[MixinConstructor(__TRAIT__, MicrocacheMixin::class, FilesystemMixin::class, ServicesMixin::class, EnvironmentMixin::class)]
    private function constructViews(): void {
        $this->container->addSingleton(Views::class, $this->views = new Views($this->microcache->withNamespace(Config::class), $this->filesystem));
        $this->addExtension($this->views);
        $this->views->add($this->env->viewsRoot);
        $this->views->add(Kernel::getFrameworkPath() . '/assets');
    }

    public function addViewPath(string $path): void {
        $this->views->add($path);
    }

    public function getViewPaths(): array {
        return $this->views->all();
    }

    public function getViewPath(string $filename): string {
        return $this->views->find($filename);
    }

}