<?php
namespace Fubber\Kernel\RequestRunners;

use Fubber\LogicException;
use Fubber\Psr\HttpMessage\ServerRequest;
use Fubber\Psr\HttpMessage\Stream;
use Fubber\Psr\HttpMessage\UploadedFile;
use Fubber\Psr\HttpMessage\Uri;
use Fubber\RequestRunnerInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class WebServerRunner implements RequestRunnerInterface {

    public function run(RequestHandlerInterface $handler): void
    {
        if (empty($_SERVER['REQUEST_METHOD'])) {
            throw new LogicException("WebServerDispatcher can only run requests via a web server such as `nginx` or `apache2`.");
        }

        $request = $this->createServerRequest();    
        $response = $handler->handle($request);
        $this->sendResponse($response);
    }

    public function stop(): void {
        // function has no purpose in a SAPI environment; there are only one request
    }

    protected function sendResponse(ResponseInterface $response): void {
        \header('HTTP/'.$response->getProtocolVersion().' '.$response->getStatusCode().' '.$response->getReasonPhrase());
        foreach ($response->getHeaders() as $name => $values) {
            $first = true;
            foreach ($values as $value) {
                \header(\sprintf('%s: %s', $name, $value), $first);
                $first = false;
            }
        }
        $body = $response->getBody();
        while (!$body->eof()) {
            $chunk = $body->read(65536);
            echo $chunk;
        }
        $body->close();
    }

    protected function createServerRequest(): ServerRequestInterface {
        $requestUrl = 'http';
        if (
            (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) === 'https') ||
            (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
        ) {
            $requestUrl .= 's';
        }
        $requestUrl .= '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $uri = new Uri($requestUrl);
        $body = new Stream(\fopen("php://input", "r"));
        if (\function_exists('getallheaders')) {
            $headers = \getallheaders();
        } else {
            $headers = [];
            foreach ($_SERVER as $k => $v) {
                if (\str_starts_with($k, 'HTTP_')) {
                    $headers[\str_replace(' ', '-', \ucwords(\strtolower(\str_replace('_', ' ', \substr($k, 5)))))] = $v;
                }
            }
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_FILES)) {
            $files = self::createUploadedFileFromSpec($_FILES);
        } else {
            $files = [];
        }

        $request = new ServerRequest(
            $_SERVER['REQUEST_METHOD'],
            $uri,
            $body,
            $headers,
            $_GET ?? null,
            [
                'QUERY_STRING' => $_SERVER['QUERY_STRING'] ?? null,
                'HOSTNAME' => $_SERVER['HOSTNAME'] ?? null,
                'HOME' => $_SERVER['HOME'] ?? null,
                'PATH' => $_SERVER['PATH'] ?? null,
                'PWD' => $_SERVER['PWD'] ?? null,
                'USER' => $_SERVER['USER'] ?? null,
                'SERVER_NAME' => $_SERVER['SERVER_NAME'] ?? null,
                'SERVER_PORT' => $_SERVER['SERVER_PORT'] ?? null,
                'SERVER_ADDR' => $_SERVER['SERVER_ADDR'] ?? null,
                'REMOTE_PORT' => $_SERVER['REMOTE_PORT'] ?? null,
                'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'] ?? null,
                'SERVER_SOFTWARE' => $_SERVER['SERVER_SOFTWARE'] ?? null,
                'GATEWAY_INTERFACE' => $_SERVER['GATEWAY_INTERFACE'] ?? null,
                'SERVER_PROTOCOL' => $_SERVER['SERVER_PROTOCOL'] ?? null,
                'FCGI_ROLE' => $_SERVER['FCGI_ROLE'] ?? null,
                'DOCUMENT_ROOT' => $_SERVER['DOCUMENT_ROOT'] ?? null,
                'CONTENT_LENGTH' => $_SERVER['CONTENT_LENGTH'] ?? null,
                'SCRIPT_FILENAME' => $_SERVER['SCRIPT_FILENAME'] ?? null,
                'REQUEST_TIME_FLOAT' => $_SERVER['REQUEST_TIME_FLOAT'] ?? microtime(true),
                'REQUEST_TIME' => $_SERVER['REQUEST_TIME'] ?? time(),
            ],
            $_COOKIE ?? null,
            $files
        );      
        return $request;
    }

    /**
     * {@see https://github.com/guzzle/psr7/blob/master/src/ServerRequest.php}
     * 
     * Return an UploadedFile instance array.
     *
     * @param array $files An array which respect $_FILES structure
     *
     * @throws InvalidArgumentException for unrecognized values
     */
    private static function normalizeFiles(array $files): array
    {
        $normalized = [];

        foreach ($files as $key => $value) {
            if ($value instanceof UploadedFileInterface) {
                $normalized[$key] = $value;
            } elseif (is_array($value) && isset($value['tmp_name'])) {
                $normalized[$key] = self::createUploadedFileFromSpec($value);
            } elseif (is_array($value)) {
                $normalized[$key] = self::normalizeFiles($value);
                continue;
            } else {
                throw new InvalidArgumentException('Invalid value in files specification');
            }
        }

        return $normalized;
    }

    /**
     * {@see https://github.com/guzzle/psr7/blob/master/src/ServerRequest.php}
     * 
     * Create and return an UploadedFile instance from a $_FILES specification.
     *
     * If the specification represents an array of values, this method will
     * delegate to normalizeNestedFileSpec() and return that return value.
     *
     * @param array $value $_FILES struct
     *
     * @return UploadedFileInterface|UploadedFileInterface[]
     */
    private static function createUploadedFileFromSpec(array $value)
    {
        if (is_array($value['tmp_name'])) {
            return self::normalizeNestedFileSpec($value);
        }

        return new UploadedFile(
            $value['tmp_name'],
            $value['name'],
            $value['type'],
            (int) $value['size'],
            (int) $value['error']
        );
    }

    /**
     * Normalize an array of file specifications.
     *
     * Loops through all nested files and returns a normalized array of
     * UploadedFileInterface instances.
     *
     * @return UploadedFileInterface[]
     */
    private static function normalizeNestedFileSpec(array $files = []): array
    {
        $normalizedFiles = [];

        foreach (array_keys($files['tmp_name']) as $key) {
            $spec = [
                'tmp_name' => $files['tmp_name'][$key],
                'size'     => $files['size'][$key],
                'error'    => $files['error'][$key],
                'name'     => $files['name'][$key],
                'type'     => $files['type'][$key],
            ];
            $normalizedFiles[$key] = self::createUploadedFileFromSpec($spec);
        }

        return $normalizedFiles;
    }
}