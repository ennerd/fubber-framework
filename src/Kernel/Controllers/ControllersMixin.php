<?php
namespace Fubber\Kernel\Controllers;

use Fubber\Kernel\Bundles\BundlesMixin;
use Fubber\Kernel\Config\ConfigMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Cron\CronMixin;
use Fubber\Kernel\EventLoop\EventLoopMixin;
use Fubber\Kernel\Http\HttpMixin;
use Fubber\Kernel\Instrumentation\InstrumentationMixin;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Phases\PhasesMixin;
use Fubber\Kernel\Router\RouterMixin;
use Fubber\Mixins\MixinConstructor;

trait ControllersMixin {
    use ServicesMixin;
    use InstrumentationMixin;
    use CronMixin;
    use BundlesMixin;
    use ConfigMixin;
    use PhasesMixin;
    use RouterMixin;
    use HttpMixin;
    use EventLoopMixin;

    public readonly Controllers $controllers;

    #[MixinConstructor(__TRAIT__, ConfigMixin::class, ServicesMixin::class, InstrumentationMixin::class, CronMixin::class, BundlesMixin::class, PhasesMixin::class, RouterMixin::class, HttpMixin::class, EventLoopMixin::class)]
    private function constructControllers(): void {
        $this->controllers = new Controllers($this, $this->container, $this->instrumentation, $this->cron, $this->config, $this->router, $this->http);
        $this->addExtension($this->controllers);
        $this->container->addSingleton(Controllers::class, $this->controllers);
    }

    public function addController(string $className): void {
        $this->controllers->add($className);
    }

}