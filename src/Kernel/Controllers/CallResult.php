<?php
namespace Fubber\Kernel\Controllers;

use Fubber\ControllerInterface;

final class CallResult {
    public function __construct(
        public string|ControllerInterface $controller,
        public string $methodName,
        public mixed $result
    ) {}
}