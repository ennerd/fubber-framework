<?php
namespace Fubber\Kernel\Controllers;

use CC\Insights\InsightsController;
use Closure;
use Fubber\CodingErrorException;
use Fubber\ControllerInterface;
use Fubber\Hooks;
use Fubber\Kernel;
use Fubber\Kernel\Config\Config;
use Fubber\Kernel\Container\Container;
use Fubber\Kernel\Controllers\CallResult;
use Fubber\Kernel\Cron\Cron;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Hooks\PerItemTriggers;
use Fubber\Hooks\Trigger;
use Fubber\Kernel\AbstractController;
use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Http\ExceptionHandlers\ExceptionHandlers;
use Fubber\Kernel\Http\Http;
use Fubber\Kernel\LogicException;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Router\Route;
use Fubber\Kernel\Router\Router;
use Fubber\PHP\ClosureTool;
use InvalidArgumentException;
use IteratorAggregate;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ReflectionMethod;
use Traversable;
use WeakMap;

/**
 * Manages the root controller classes for Fubber\Kernel.
 * 
 * @package Fubber\Kernel
 */
final class Controllers extends KernelExtension implements IteratorAggregate {

    /**
     * Controller state
     */
    public const STATE_ADDED = 0;
    public const STATE_LOADED = 1;
    public const STATE_INITIALIZED = 2;

    /**
     * Triggers for each controller after their `load()` method was called.
     * 
     * @var PerItemTriggers
     */
    public readonly PerItemTriggers $onControllerLoaded;

    /**
     * Triggers for each controller after their `init()` method was called.
     * 
     * @var PerItemTriggers
     */
    public readonly PerItemTriggers $onControllerInitialized;

    /**
     * Last opportunity to add controllers via {@see self::add()}
     * 
     * @var Trigger
     */
    public readonly Trigger $onAllControllersLoaded;

    /**
     * Triggered when the `init()` method of all controllers have been called.
     * 
     * @var Trigger
     */
    public readonly Trigger $onAllControllersInitialized;

    /**
     * Controller class names for static controllers.
     * 
     * @var array<string|ControllerInterface>
     */
    private array $controllers = [];

    /**
     * Used to quickly check if a controller instance has been added and record
     * its current state {@see self::STATE_*}.
     * 
     * @var array<string, int>
     */
    private array $seenClasses = [];

    /**
     * Used to quickly check if a controller is being added twice and also
     * records the controllers current state {@see self::STATE_*}.
     * 
     * @var WeakMap<ControllerInterface, int>
     */
    private WeakMap $seenInstances;

    private bool $didScan = false;
    private array $collectedRoutes = [];
    private array $collectedExceptionHandlers = [];


    public function getExtensionName(): string
    {
        return 'Controller manager';
    }

    public function __construct(
        protected readonly Kernel $kernel,
        protected readonly Container $container,
        protected readonly Instrumentation $dev,
        protected readonly Cron $cron,
        protected readonly Config $config,
        protected readonly Router $router,
        protected readonly Http $http,
    ) {
        $this->onControllerLoaded = new PerItemTriggers('A controller was loaded');
        $this->onControllerInitialized = new PerItemTriggers('A controller was initialized');
        $this->onAllControllersLoaded = new Trigger('All controllers have been loaded. No more controllers can be loaded.');
        $this->onAllControllersInitialized = new Trigger('All controllers have been initialized. No more controllers can be added.');
        $this->seenInstances = new WeakMap();

        $this->router->onCollectRoutes->listen($this->provideRoutes(...));
        $this->http->exceptionHandlers->onCollect->listen($this->provideExceptionHandlers(...));
    }

    private function provideExceptionHandlers(ExceptionHandlers $handlers): void {
        if (!$this->onAllControllersInitialized->wasTriggered()) {
            throw new LogicException("Controllers haven't initialized");
        }
        if (!$this->didScan) {
            $this->scanRoutesAndExceptionHandlers();
        }
        foreach ($this->collectedExceptionHandlers as $exceptionClass => $handler) {
            $handlers[$exceptionClass] = $handler;
        }
    }

    private function provideRoutes(Router $router): void {
        if (!$this->onAllControllersInitialized->wasTriggered()) {
            throw new LogicException("Controllers haven't initialized");
        }
        if (!$this->didScan) {
            $this->scanRoutesAndExceptionHandlers();
        }
        $router->add(...$this->collectedRoutes);
    }

    private function scanRoutesAndExceptionHandlers(): void {
        if ($this->didScan) {
            throw new LogicException("Already scanned");
        }
        /** @var Route[] */
        $routes = [];
        $exceptionHandlers = [];

        $parseRoutes = function(array $routeDefinitions, int $priority, mixed $origin) use (&$routes, &$parseRoutes, &$exceptionHandlers) {
            foreach ($routeDefinitions as $k => $v) {
                if (\is_int($k)) {
                    if ($v instanceof Route) {
                        $routes[] = $v;
                    } elseif (\is_array($v)) {
                        $parseRoutes($v, $k, $origin);
                    }
                } elseif (\is_string($k)) {
                    $parts = \explode(" ", $k, 3);
                    if (isset($parts[2])) {
                        $name = $parts[0];
                        $methods = $parts[1];
                        $path = $parts[2];
                    } elseif (isset($parts[1])) {
                        $name = null;
                        $methods = $parts[0];
                        $path = $parts[1];
                    } elseif (\class_exists($parts[0])) {
                        $exceptionHandlers[$parts[0]] = Closure::fromCallable($v);
                        continue;
                    } else {
                        throw new CodingErrorException("Unable to parse route `$k`");
                    }
                    if (!\is_callable($v) && !($v instanceof RequestHandlerInterface) && !($v instanceof MiddlewareInterface)) {
                        throw new CodingErrorException("Handler for `$path` is not callable (got `".\json_encode($v)."`)");
                    }
                    $routes[] = new Route($methods, $path, \is_callable($v) ? Closure::fromCallable($v) : $v, $name, $priority ?? 0, $origin);
                } else {
                    throw new CodingErrorException("Unable to parse route `$k` => `$v`");
                }
            }
        };
        foreach ($this->call('routes', $this->kernel) as $result) {
            $parseRoutes($result->result, 0, $result->controller);
        }
        $this->collectedRoutes = $routes;
        $this->collectedExceptionHandlers = $exceptionHandlers;
        $this->didScan = true;
    }

    /**
     * Call a method on all registered controllers.
     * 
     * @param string $method 
     * @param array $args 
     * @return CallResult[] 
     */
    public function call(string $methodName, mixed ...$args): array {
        if (!$this->onAllControllersInitialized->wasTriggered()) {
            throw new CodingErrorException("Can't use `call()` before all controllers have been initialized");
        }
        $results = [];
        $callbackArray = [ null, $methodName ];
        foreach ($this->controllers as $controller) {
            $callbackArray[0] = $controller;
            if (\is_callable($callbackArray)) {
                $results[] = new CallResult($controller, $methodName, $callbackArray(...$args));
            }
        }
        return $results;
    }

    /**
     * Add a new controller
     * 
     * @param string ...$className 
     * @throws CodingErrorException
     */
    public function add(string|ControllerInterface ...$controllers): void {
        if ($this->onAllControllersLoaded->wasTriggered()) {
            throw new CodingErrorException("No more controllers can be added.");
        }

        foreach ($controllers as $controller) {
            // Kernel::debug("Adding controller {controllerClass}", ['controllerClass' => \is_string($controller) ? $controller : $controller::class]);
            if (\is_string($controller) && \class_exists($controller)) {
                // Determine if we need to instantiate the controller or if
                // it is a "RootController".
                if (isset($this->seenClasses[$controller])) {
                    throw new LogicException("Controller `$controller` is already added");
                }
                $this->seenClasses[$controller] = self::STATE_ADDED;
                if (\is_a($controller, ControllerInterface::class, true)) {
                    $controllerInstance = $this->container->construct($controller);
                    $this->seenInstances[$controllerInstance] = self::STATE_ADDED;
                    $this->container->addSingleton($controller, $controllerInstance);
                    $controller = $controllerInstance;
                }
            } elseif ($controller instanceof ControllerInterface) {
                if (isset($this->seenInstances[$controller])) {
                    throw new LogicException("Controller instance of class `".$controller::class."` is already added");
                }
                $this->seenInstances[$controller] = self::STATE_ADDED;
            } else {
                throw new InvalidArgumentException("Not a controller: " . $controller);
            }
            $this->controllers[] = $controller;
            $this->doControllerLoad($controller);
        }
    }

    #[PhaseHandler(Phase::Controllers, \PHP_INT_MIN)]
    private function onControllersPhaseStart(): void {
        /**
         * Load controllers from configuration file `controllers.php`
         */
        $controllers = $this->config->load('controllers');
        $this->add(...$controllers);

        $this->onAllControllersLoaded->trigger();
        Hooks::dispatch('fubber.kernel.controllers.loaded', $controllers);
    }

    /**
     * Call initialize on all controllers that have been added so far
     */
    #[PhaseHandler(Phase::Controllers)]
    private function onControllersPhase(): void {
        foreach ($this->controllers as $controller) {
            $this->doControllerInit($controller);
        }
        $this->onAllControllersInitialized->trigger($this);
    }

    private function doControllerLoad(string|ControllerInterface $controller): void {
        // Kernel::debug("{controllerClass}::load(...)", ['controllerClass' => \is_string($controller) ? $controller : $controller::class]);

        $perf = $this->dev->benchmark((\is_string($controller) ? $controller : $controller::class) . '::load()');
        try {
            if ($controller instanceof ControllerInterface) {
                if ($this->seenInstances[$controller] !== self::STATE_ADDED) {
                    throw new LogicException("Controller `".$controller."` not in ADDED state");
                }

                $this->seenInstances[$controller] = self::STATE_LOADED;

                $loadMethod = $controller->load(...);
            } else {
                if ($this->seenClasses[$controller] !== self::STATE_ADDED) {
                    throw new LogicException("Controller `".$controller::class."` not in ADDED state");
                }
    
                $loadMethod = null;
                if (\method_exists($controller, 'load')) {
                    $rm = new ReflectionMethod($controller, 'load');
                    if (!$rm->isStatic()) {
                        Kernel::notice("Method `{className}::{methodName}` is not declared as static", [ 'className' => $controller, 'methodName' => 'init']);
                    } elseif (!$rm->isPublic()) {
                        Kernel::notice("Method `{className}::{methodName}` is not declared as public", [ 'className' => $controller, 'methodName' => 'init']);
                    } else {
                        $loadMethod = $rm->getClosure(null);
                    }
                }

                $this->seenClasses[$controller] = self::STATE_LOADED;
            }    

            $result = $loadMethod ? $this->container->invoke($loadMethod) : null;
            $this->onControllerLoaded->triggerFor($controller, new CallResult($controller, 'load', $result));

        } finally {
            $perf();
        }
    }

    private function doControllerInit(string|ControllerInterface $controller): void {
        // Kernel::debug("{controllerClass}::init(...)", ['controllerClass' => \is_string($controller) ? $controller : $controller::class]);

        $perf = $this->dev->benchmark((\is_string($controller) ? $controller : $controller::class) . '::init()');
        try {
            if ($controller instanceof ControllerInterface) {
                if ($this->seenInstances[$controller] !== self::STATE_LOADED) {
                    throw new LogicException("Controller `".$controller::class."` not in LOADED state (was ".$this->seenInstances[$controller].")");
                }
    
                $result = $this->container->invoke($controller->init(...));
                $this->seenInstances[$controller] = self::STATE_INITIALIZED;
                $this->onControllerInitialized->triggerFor($controller, new CallResult($controller, 'load', $result));
            } else {
                if ($this->seenClasses[$controller] !== self::STATE_LOADED) {
                    throw new LogicException("Controller `".$controller."` not in LOADED state (was ".$this->seenClasses[$controller].")");
                }
    
                $result = null;
                if (\method_exists($controller, 'init')) {
                    $rm = new ReflectionMethod($controller, 'init');
                    if (!$rm->isStatic()) {
                        Kernel::notice("Method `{className}::{methodName}` is not declared as static", [ 'className' => $controller, 'methodName' => 'init']);
                    } elseif (!$rm->isPublic()) {
                        Kernel::notice("Method `{className}::{methodName}` is not declared as public", [ 'className' => $controller, 'methodName' => 'init']);
                    } else {
                        $closure = Closure::fromCallable([$controller, 'init']);
                        $result = $this->container->invoke($closure);
                    }
                }
                $this->seenClasses[$controller] = self::STATE_INITIALIZED;
                $this->onControllerInitialized->triggerFor($controller, new CallResult($controller, 'load', $result));
            }
        } finally {
            $perf();
        }
    }

   /**
     * @return Traversable<string|ControllerInterface>
     */
    public function getIterator(): Traversable
    {
        foreach ($this->controllerClasses as $className) {
            yield $className;
        }
        foreach ($this->controllerInstances as $controller) {
            yield $controller;
        }
    }

}