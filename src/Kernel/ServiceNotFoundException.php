<?php
namespace Fubber\Kernel;

use Fubber\I18n\Translatable;
use Fubber\NotFoundException;
use Psr\Container\NotFoundExceptionInterface;

class ServiceNotFoundException extends NotFoundException implements NotFoundExceptionInterface {

    public function getExceptionDescription(): string {
        return "Thrown when the service container don't provide a particular resource";
    }

}