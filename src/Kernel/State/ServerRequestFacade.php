<?php
namespace Fubber\Kernel\State;

use Fubber\LogicException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\StreamInterface;
use WeakMap;

/**
 * Provides a facade which will return the most recent ServerRequestInterface
 * object being used in the middleware stack.
 * 
 * @package Fubber\Kernel\State
 */
class ServerRequestFacade implements ServerRequestInterface {

    protected int $depth = 0;
    protected array $requests;

    public function __construct(ServerRequestInterface $originalRequest) {
        $this->requests = [ $originalRequest ];
    }

    public function push(ServerRequestInterface $request): void {
        $this->requests[++$this->depth] = $request;
    }

    public function pop(): ServerRequestInterface {
        if ($this->depth === 0) {
            throw new LogicException("Can't pop the root request object");
        }
        return $this->requests[$this->depth--];
    }

    /**
     * Get the proxied request instance
     * 
     * @return ServerRequestInterface 
     */
    protected function getRequest(): ServerRequestInterface {
        return $this->requests[$this->depth];
    }

    public function getServerParams() {
        return $this->getRequest()->getServerParams();
    }

    public function getCookieParams() {
        return $this->getRequest()->getCookieParams();
    }

    public function withCookieParams(array $cookies) {
        return $this->getRequest()->withCookieParams($cookies);
    }

    public function getQueryParams() {
        return $this->getRequest()->getQueryParams();
    }

    public function withQueryParams(array $query) {
        return $this->getRequest()->withQueryParams($query);
    }

    public function getUploadedFiles() {
        return $this->getRequest()->getUploadedFiles();
    }

    public function withUploadedFiles(array $uploadedFiles) {
        return $this->getRequest()->withUploadedFiles($uploadedFiles);
    }

    public function getParsedBody() {
        return $this->getRequest()->getParsedBody();
    }

    public function withParsedBody($data) {
        return $this->getRequest()->withParsedBody($data);
    }

    public function getAttributes() {
        return $this->getRequest()->getAttributes();
    }

    public function getAttribute($name, $default = null) {
        return $this->getRequest()->getAttribute($name, $default);
    }

    public function withAttribute($name, $value) {
        return $this->getRequest()->withAttribute($name, $value);
    }

    public function withoutAttribute($name) {
        return $this->getRequest()->withoutAttribute($name);
    }

    public function getRequestTarget() {
        return $this->getRequest()->getRequestTarget();
    }

    public function withRequestTarget($requestTarget) {
        return $this->getRequest()->withRequestTarget($requestTarget);
    }

    public function getMethod() {
        return $this->getRequest()->getMethod();
    }

    public function withMethod($method) {
        return $this->getRequest()->withMethod($method);
    }

    public function getUri() {
        return $this->getRequest()->getUri();
    }

    public function withUri(UriInterface $uri, $preserveHost = false) {
        return $this->getRequest()->withUri($uri, $preserveHost);
    }

    public function getProtocolVersion() {
        return $this->getRequest()->getProtocolVersion();
    }

    public function withProtocolVersion($version) {
        return $this->getRequest()->withProtocolVersion($version);
    }

    public function getHeaders() {
        return $this->getRequest()->getHeaders();
    }

    public function hasHeader($name) {
        return $this->getRequest()->hasHeader($name);
    }

    public function getHeader($name) {
        return $this->getRequest()->getHeader($name);
    }

    public function getHeaderLine($name) {
        return $this->getRequest()->getHeaderLine($name);
    }

    public function withHeader($name, $value) {
        return $this->getRequest()->withHeader($name, $value);
    }

    public function withAddedHeader($name, $value) {
        return $this->getRequest()->withAddedHeader($name, $value);
    }

    public function withoutHeader($name) {
        return $this->getRequest()->withoutHeader($name);
    }

    public function getBody() {
        return $this->getRequest()->getBody();
    }

    public function withBody(StreamInterface $body) {
        return $this->getRequest()->withBody($body);
    }
}