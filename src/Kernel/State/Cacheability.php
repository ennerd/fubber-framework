<?php
namespace Fubber\Kernel\State;

use DateTimeImmutable;
use DateTimeInterface;
use Fubber\CodingErrorException;
use Fubber\Kernel\State;
use Fubber\LogicException;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Manages the cache headers that eventually will end up with the
 * response.
 * 
 * @package State
 */
final class Cacheability {

    /**
     * The response can be cached in a shared cache.
     * 
     * The shared cache is located between the client and the server
     * and can store responses that can be shared among users. And 
     * shared caches can be further sub-classified into proxy caches
     * and managed caches.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching
     */
    const CACHE_PUBLIC = 'public';

    /**
     * The response can be shared only in a private cache.
     * 
     * A private cache is a cache tied to a specific client — 
     * typically a browser cache. Since the stored response
     * is not shared with other clients, a private cache can
     * store a personalized response for that user.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching
     */
    const CACHE_PRIVATE = 'private';

    /**
     * The response *can* be cached, but the browser MUST revalidate
     * the request with the origin to check if it has been modified
     * (revalidation).
     * 
     * Revalidation is possible if the response has a `last-modified`
     * date or an `ETag` key.
     * 
     * The response body will not be downloaded if the cache or the
     * client still has a copy, after 
     */
    const CACHE_NOCACHE = 'no-cache';
    const CACHE_NOSTORE = 'no-store';

    /**
     * Constant indicates for a directive that it can be added
     * or removed at any time without constituting a loosening
     * of caching constraints.
     */
    protected const DIRECTIVE_NORMAL = 0;

    /**
     * Constant indicates for a directive that once it is
     * enabled, it can't be disabled.
     */
    protected const DIRECTIVE_ENABLE_LOCKED = 1;

    /**
     * Constant indicates for a directive that once it is
     * disabled, it can't be enabled.
     */
    protected const DIRECTIVE_DISABLE_LOCKED = 2;

    protected State $state;

    /**
     * The response cacheability (public, private, no-cache)
     * 
     * @var string
     */
    protected string $level = self::CACHE_PUBLIC;

    /**
     * The response cache max age (default 1 year)
     * 
     * @var int
     */
    protected ?int $maxAgeValue = null;

    /**
     * The response cache max age for shared caches (default is not set)
     */
    protected ?int $sMaxAgeValue = null;

    /**
     * If the default caching restriction was changed
     * 
     * @var bool
     */
    protected bool $changed = false;

    /**
     * True if no changes are allowed to the cacheability state
     * 
     * @var bool
     */
    protected bool $locked = false;

    /**
     * Extensions to the Cache-Control header
     * 
     * @var array<string, string>
     */
    protected array $directives = [];
    
    /**
     * The last modified timestamp, if provided.
     * 
     * @var null|int
     */
    protected ?int $lastModifiedTimestamp = null;

    /**
     * The eTag for the response, if provided.
     * 
     * @var null|string
     */
    protected ?string $eTag = null;

    /**
     * The `ETag` we're using is "weak", which means that the response
     * was built based on the same data - but it might not be identical
     * to an earlier response. The `ETag` automatically is marked as
     * weak if the `ETag` was built incrementally.
     * 
     * @var bool
     */
    protected bool $eTagIsWeak = false;

    public function __construct() {}

    /**
     * Apply the cacheability settings to a `ResponseInterface` object.
     * 
     * @param ResponseInterface $response 
     * @return ResponseInterface 
     * @throws CodingErrorException 
     * @throws LogicException 
     * @throws InvalidArgumentException 
     */
    public function apply(ResponseInterface $response): ResponseInterface {

        /**
         * Applying the cacheability requires us to import any settings from the response,
         * so to avoid mutating this instance we'll work on a clone.
         */
        $self = clone $this;

        if ($response->getHeaderLine("Set-Cookie")) {
            /**
             * Security measure; since a cookie is being set we must avoid caching this 
             * response in a shared cache.
             */
            $self->privateCache();
            $response = $response->withAddedHeader('X-Fubber-Cache-Control', 'Made private because of Set-Cookie header');
        }

        $currentCacheControl = \strtolower($response->getHeaderLine('Cache-Control'));

        if (!empty($currentCacheControl)) {
            /**
             * Parse and apply the existing cache control directives
             */
            $count = \preg_match_all('/([a-z-]+)(=(\d+))?/m', $currentCacheControl, $matches);
        
            if ($count > 0) {
                $directives = [];

                foreach ($matches[0] as $i => $void) {
                    $directives[$matches[1][$i]] = $matches[3][$i] === '' ? $matches[1][$i] : intval($matches[3][$i]);
                }

                $self->setMaxAge(
                    \array_key_exists('max-age', $directives) ? intval($directives['max-age']) : null,
                    \array_key_exists('s-maxage', $directives) ? intval($directives['s-maxage']) : null,
                );

                /**
                 * Inherit any cacheability directives.
                 */
                if (!empty($directives['no-store'])) {
                    $self->noStore();
                } elseif (!empty($directives['no-cache'])) {
                    $self->noCache();
                } elseif (!empty($directives['private'])) {
                    $self->privateCache();
                } elseif (!empty($directives['public'])) {
                    $self->publicCache();
                }

                /**
                 * Import the currently understood directives without overriding any values that
                 * have already been locked.
                 */
                foreach ([
                    'must-revalidate' => self::DIRECTIVE_ENABLE_LOCKED,
                    'proxy-revalidate' => self::DIRECTIVE_ENABLE_LOCKED,
                    'must-understand' => self::DIRECTIVE_ENABLE_LOCKED,
                    'no-transform' => self::DIRECTIVE_ENABLE_LOCKED,
                    'immutable' => self::DIRECTIVE_DISABLE_LOCKED,
                    'stale-while-revalidate' => self::DIRECTIVE_DISABLE_LOCKED,
                    'stale-if-error' => self::DIRECTIVE_DISABLE_LOCKED,
                ] as $name => $type) {
                    if (!empty($directives[$name])) {
                        $self->setCacheControlDirective($name, $directives[$name], true, $type, false);
                    }
                }
            }
        }

        if ($eTag = $response->getHeaderLine('ETag')) {
            if ($self->eTag === null) {
                $self->eTag = $eTag;
            } else {
                $self->eTag($eTag, true, true);
            }
        }

        if ($lastModified = $response->getHeaderLine('Last-Modified')) {
            if ($self->lastModifiedTimestamp === null) {
                $self->lastModifiedTimestamp = \strtotime($lastModified);
            } else {
                $self->lastModified(\strtotime($lastModified), true);
            }
        }
        
        /**
         * Add all headers according to the current state
         */
        foreach ($self->getHeaders() as $name => $values) {
            $response = $response->withHeader($name, $values);
        }

        return $response;
    }

    /**
     * Set the `Last-Modified` timestamp. This timestamp is useful whenever the content being
     * served has a single timestamp; for example when serving a file using its `filemtime()`.
     * 
     * The `Last-Modified` timestamp makes it possible for clients and shared caches to serve
     * the response body EVEN IF the content was previously marked as `no-cache` or `private`,
     * or contains the `must-revalidate` directive.
     * 
     * The `Last-Modified` header will be removed from the reponse if the directive `no-store`
     * is being served.
     * 
     * The `Last-Modified` timestamp can be used by a proxy server or by the web client to 
     * check if it is okay to serve a stale response.
     * 
     * If you are generating a response based on data from a database, you can use the `$incremental`
     * parameter to indicate that you are recording the timestamp for multiple assets and that
     * we should use the "youngest" timestamp in the final response.
     * 
     * The `ETag` header is often easier to apply since it can be generated as a hash of the
     * response body and thus will be guaranteed to be correct. The `Last-Modified` timestamp
     * is helpful if the content being served is too big to efficiently generate an ETag hash.
     * 
     * @param int|DateTimeInterface $timestamp 
     * @param bool $incremental 
     * @throws LogicException 
     */
    public function lastModified(int|DateTimeInterface $timestamp, bool $incremental=false): void {
        if ($incremental === false && $this->lastModifiedTimestamp !== null) {
            throw (new LogicException("The response already has a `Last-Modified` timestamp."))
            ->withSuggestion('When adding `Last-Modified` to a response you must ensure that ALL entities annotate their last modification time. Otherwise it is better to not add the `Last-Modified` header. Set the `$incremental` parameter to `true` if you understand the implications.');
        }

        if ($timestamp instanceof DateTimeInterface) {
            $timestamp = $timestamp->getTimestamp();
        }

        if ($this->lastModifiedTimestamp === null || $this->lastModifiedTimestamp < $timestamp) {
            $this->timestamp = $timestamp;
        }
    }

    /**
     * Set the `ETag` content hash. This tag is used by caches to determine if they can avoid
     * downloading the entire response body and can save outgoing bandwidth.
     * 
     * The ETag must be based on all the variable components that are used to build your response
     * body.
     * 
     * The `ETag` hash makes it possible for clients and shared caches to serve an existing copy
     * of the response body and can make your website faster. Browsers can use this tag to determine
     * if they have to parse the javascript or CSS file again, while shared caches simply avoid
     * downloading the body after have parsed the headers.
     * 
     * If your response body is large, it might not be practical to calculate the `ETag`, and
     * in that case you should consider using the `Last-Modified` tag instead.
     * 
     * If you are generating a response based on data from a database, you can use the `$incremental`
     * parameter to indicate that you are recording the hash for multiple assets and that the hash
     * should be built we should use the "youngest" timestamp in the final response. This will mark
     * your `ETag` header as "weak" which means that the response might not be 100% byte-for-byte
     * identical to an earlier response - but that the response is still considered valid.
     * 
     * The `ETag` header is often easier to apply since it can be generated as a hash of the
     * response body and thus will be guaranteed to be correct. The `Last-Modified` timestamp
     * is helpful if the content being served is too big to efficiently generate an ETag hash.
     * 
     * @param string $tag 
     * @param bool $incremental 
     * @throws LogicException 
     */
    public function eTag(string $tag, bool $incremental=false, bool $noRehashing=false): void {
       
        if ($this->eTag !== null) {
            if (!$incremental) {
                throw (new LogicException("The response already has an `ETag` header."))
                ->withSuggestion('When addiong `ETag` to a response you must ensure that ALL entities used to build the response contribute to the `ETag` header. One way to do this is by adding the `ETag` header using the full body of the final response. If you wish to incrementally build the `ETag` header, set the `$incremental` parameter to `true`.');
            }

            $this->eTagIsWeak = true;

            if ($noRehashing) {
                $this->eTag .= '=' . $tag;
            } else {
                $oldHash = \explode("=", $this->eTag);

                $this->eTag = $this->base64Hash($tag . $oldHash[0]) . (isset($old[1]) ? '=' . $old[1] : '');
            }

        } else {
            if ($noRehashing) {
                if (\str_contains($tag, '"') || (\function_exists('ctype_graph') && !\ctype_graph($tag))) {
                    throw (new LogicException("The `ETag` must contain only ASCII characters."))
                    ->withSuggestion('Remove any non-ASCII characters from the `ETag` value, or set the `$noRehashing` parameter to `false`.');
                }
                $this->eTag = '=' . $tag;
            } else {
                $this->eTag = $this->base64Hash($tag);
            }
        }
    }

    /**
     * Builds a base64 encoded md5 hash.
     * 
     * @param string $content 
     * @return string 
     */
    protected function base64Hash(string $content): string {
        return \rtrim(\base64_encode(\md5($content, true)), '=');
    }

    /**
     * Performs hashing of some content, and retains the previous content
     * @param string $content 
     * @param string|null $previousHash 
     * @return string 
     */
    protected function incrementalHash(string $content, string $previousHash=null): string {
        return $this->base64Hash($content . ($previousHash ?? ''));
    }

    /**
     * Get the cache headers
     * 
     * @return array<string, string[]> 
     */
    public function getHeaders(): iterable {

        $extraDirectives = '';

        if ($this->maxAgeValue !== null && $this->level !== self::CACHE_NOSTORE) {
            $extraDirectives .= ', max-age'.$this->maxAgeValue;
        }

        if ($this->sMaxAgeValue !== null && $this->level === self::CACHE_PUBLIC) {
            $extraDirectives .= ', s-maxage='.$this->sMaxAgeValue;
        }

        foreach ($this->directives as $key => $value) {
            if ($value !== '') {
                $extraDirectives .= ', '.$value;
            }
        }

        switch ($this->level) {
            case self::CACHE_PUBLIC:
                yield 'Cache-Control' => 'public, max-age='.$this->maxAgeValue.$extraDirectives;
                break;
            case self::CACHE_PRIVATE:
                yield 'Cache-Control' => 'private, max-age='.$this->maxAgeValue.$extraDirectives;
                break;
            case self::CACHE_NOCACHE:
                yield 'Cache-Control' => 'no-cache, max-age=0'.$extraDirectives;
                break;
            case self::CACHE_NOSTORE:
                yield 'Cache-Control' => 'no-store, max-age=0'.$extraDirectives;
                yield 'Pragma' => 'no-cache';
                break;
        }

        if ($this->lastModifiedTimestamp !== null) {
            yield 'Last-Modified' => [ \gmdate('D, d M Y H:i:s', $this->lastModifiedTimestamp) . ' GMT' ];
            yield 'Date' => [ \gmdate('D, d M Y H:i:s') . ' GMT' ];
        }

        if ($this->eTag !== null) {
            if ($this->eTagIsWeak) {
                yield 'ETag' => 'W/"' . $this->eTag . '"';
            }
            yield 'ETag' => '"' . $this->eTag . '"';
        }

        
    }

    /**
     * True if the response may be cached publicly.
     * 
     * @return bool 
     */
    public function isPubliclyCacheable(): bool {
        return $this->level === self::CACHE_PUBLIC;
    }

    public function isLocked(): bool {
        return $this->locked;
    }

    /**
     * Get the cache time-to-live in seconds
     * 
     * @return int 
     */
    public function getMaxAge(): ?int {
        return $this->maxAgeValue;
    }

    /**
     * Declare that you would allow public caching for the response
     * up to `$ttl` seconds. Optionally restrict the time shared caches
     * are allowed to cache the result.
     * 
     * @param int|null $browserTTL 
     * @throws CodingErrorException 
     */
    public function publicCache(int $browserTTL=null, int $sharedTTL=null): void {
        $this->assertNotLocked();
        $this->changed = true;
        if ($browserTTL !== null) {
            $this->setMaxAge($browserTTL, $sharedTTL);
        }        
    }

    /**
     * Declare that you would allow no more than in-browser caching
     * and limit time-to-live to `$ttl` seconds. Shared caches aren't
     * allowed to cache the response.
     * 
     * @param int|null $browserTTL 
     * @throws CodingErrorException 
     */
    public function privateCache(int $browserTTL=null): void {
        $this->assertNotLocked();
        $this->changed = true;
        $this->setMaxAge($browserTTL, 0);
        if ($this->level === self::CACHE_PUBLIC) {
            $this->level = self::CACHE_PRIVATE;
        }
    }

    /**
     * Declare that you won't allow any caching of this response except in the
     * brower, and the browser must revalidate the request before using the
     * cache again. Automatically sets TTL=0.
     * 
     * @throws CodingErrorException 
     */
    public function noCache(): void {
        // for debugging; echo "<pre>"; \debug_print_backtrace();die();

        $this->assertNotLocked();
        $this->changed = true;
        $this->setMaxAge(0, 0);
        if (
            $this->level === self::CACHE_PUBLIC ||
            $this->level === self::CACHE_PRIVATE
        ) {
            $this->level = self::CACHE_NOCACHE;
        }
    }

    /**
     * Declare that the browser can't cache this response at all.
     * 
     * @throws CodingErrorException 
     */
    public function noStore(): void {
        $this->assertNotLocked();
        $this->changed = true;
        $this->setMaxAge(0, 0);
        if (
            $this->level === self::CACHE_PUBLIC ||
            $this->level === self::CACHE_PRIVATE ||
            $this->level === self::CACHE_NOCACHE
        ) {
            $this->level = self::CACHE_NOSTORE;
        }
    }

    /**
     * Add the `stale-if-error` directive extension to the Cache-Control HTTP header, which
     * allows a frontend cache to use serve stale cached responses in case of an error.
     * 
     * @see https://www.rfc-editor.org/rfc/rfc5861
     * 
     * @throws CodingErrorException 
     */
    public function staleIfError(bool $enable=true): void {
        $this->setCacheControlDirective('stale-if-error', 'stale-if-error', $enable, self::DIRECTIVE_DISABLE_LOCKED, true);
    }


    /**
     * Add the `stale-while-revalidate` directive extension to the Cache-Control HTTP header,
     * which allows a frontend cache to serve stale cached responses if it is currently
     * being updated.
     * 
     * @see https://www.rfc-editor.org/rfc/rfc5861
     * 
     * @param bool $enable 
     * @throws CodingErrorException 
     */
    public function staleWhileRevalidate(bool $enable=true): void {
        $this->setCacheControlDirective('stale-while-revalidate', 'stale-while-revalidate', $enable, self::DIRECTIVE_DISABLE_LOCKED, true);
    }

    /**
     * Add the `no-transform` directive to the Cache-Control HTTP header,
     * which prevents a frontend cache from transforming the response. An
     * example of such a transformation could be applying additional compression
     * to an image file to conserve bandwidth for the client.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control#directives
     * 
     * @param bool $enable 
     * @throws CodingErrorException 
     */
    public function noTransform(bool $enable=true): void {
        $this->setCacheControlDirective('no-transform', 'no-transform', $enable, self::DIRECTIVE_ENABLE_LOCKED, false);
    }

    /**
     * Add the `immutable` directive to the Cache-Control HTTP header. This indicates
     * to the cache that the response will not be updated while it's fresh.
     * 
     * Use this directive especially if the request path or query string  contains a
     * version number or a file hash, for example if you are serving transpiled files
     * where the filename contains a hash of the file contents, or javascript files 
     * number embedded.
     * 
     * @param bool $enable 
     * @throws CodingErrorException 
     */
    public function immutable(bool $enable=true): void {
        $this->setCacheControlDirective('immutable', 'immutable', $enable, self::DIRECTIVE_DISABLE_LOCKED, false);
    }

    /**
     * Add the `must-revalidate` directive to indicate that the response can be stored 
     * in caches and can be reused while fresh. If the response becomes stale, it must
     * be validated with the origin server before reuse.
     * 
     * @param bool $enable 
     * @throws CodingErrorException 
     */
    public function mustRevalidate(bool $enable=true): void {
        $this->setCacheControlDirective('must-revalidate', 'must-revalidate', $enable, self::DIRECTIVE_ENABLE_LOCKED, false);
    }

    /**
     * Add the `proxy-revalidate` directive to indicate that the response can be stored
     * in a proxy cache and can be reused while fresh. If the response becomes stale,
     * the proxy must validate it with the origin server before reuse.
     * 
     * The response must be revalidatable (for example by containing an `ETag` header)
     * and 
     */

    /**
     * The `must-understand` response directive indicates that a cache should store the
     * response only if it understands the requirements for caching based on status code.
     * 
     * This directive implies `no-store` for security reasons.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control#directives
     * 
     * @param bool $enable 
     * @throws CodingErrorException 
     */
    public function mustUnderstand(bool $enable=true): void {
        $this->setCacheControlDirective('must-understand', 'must-understand', $enable, self::DIRECTIVE_ENABLE_LOCKED, false);
        if ($enable) {
            // `must-understand` should be coupled with no-store for fallback behavior.
            $this->noStore();
        }
    }

    /** 
     * Prevent further changes to the cache headers.
     */
    public function lock(): void {
        $this->locked = true;
    }

    /**
     * Check that the cache hasn't been locked for changes.
     * 
     * @throws CodingErrorException 
     */
    protected function assertNotLocked(): void {
        if($this->locked) {
            throw new \Fubber\CodingErrorException("Cache has been locked. Don't use uncacheable data.");
        }
    }

    /**
     * Helper function for updating the `max-age` and `s-maxage` directives. The function
     * will only reduce the value, and never set it to a negative value.
     * 
     * @param int|null $maxAge 
     * @param int|null $sMaxAge 
     */
    protected function setMaxAge(int $maxAge=null, int $sMaxAge=null): void {
        if ($maxAge !== null) {
            $this->maxAgeValue = $this->maxAgeValue !== null ? \max(0, \min($this->maxAgeValue, $maxAge)) : $maxAge;
        }
        if ($sMaxAge !== null) {
            $this->sMaxAgeValue = $this->sMaxAgeValue !== null ? \max(0, \min($this->sMaxAgeValue ?? 31536000, $sMaxAge)) : $sMaxAge;
        }
    }

    /**
     * Helper function for setting extensions to the Cache-Control HTTP header.
     * 
     * @param string $key 
     * @param string $value 
     * @param bool $enable 
     * @param int $directiveType
     * @throws CodingErrorException 
     */
    protected function setCacheControlDirective(string $key, string $value, bool $enable, int $directiveType=self::DIRECTIVE_NORMAL, bool $throws=true): void {
        $this->assertNotLocked();

        if (
            $directiveType === self::DIRECTIVE_ENABLE_LOCKED &&
            $enable === false &&
            !empty($this->directives[$key] ?? null)
        ) {
            if (!$throws) {
                return;
            }
            // If this directive has been enabled, it can't be disabled
            throw new LogicException("Can't disable the `$key` Cache-Control HTTP header directive after it has been disabled");
        } elseif (
            $directiveType === self::DIRECTIVE_DISABLE_LOCKED &&
            $enable === true &&
            ($this->directives[$key] ?? null) === ''
        ) {
            if (!$throws) {
                return;
            }
            // If this directive has been disabled, it can't be enabled
            throw new LogicException("Can't enable the `$key` Cache-Control HTTP header directive after it has been disabled");
        }

        if (($this->directives[$key] ?? null) !== $value) {
            $this->changed = true;
        } else {
            return;
        }

        if ($enable) {
            $this->directives[$key] = $value;
        } else {
            $this->directives[$key] = '';
        }
    }
}