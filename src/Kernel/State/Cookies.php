<?php
namespace Fubber\Kernel\State;

use Fubber\CodingErrorException;
use Fubber\Kernel\State;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class collects information about cookies for the final response which
 * will be sent to the browser.
 * 
 * @package State
 */
final class Cookies {

    protected State $state;
    protected array $cookies = [];

    public function __construct(State $state) {
        $this->state = $state;
    }

    /**
     * @internal
     * @param ResponseInterface $response 
     * @return ResponseInterface 
     * @throws InvalidArgumentException 
     */
    public function apply(ResponseInterface $response): ResponseInterface {
        if (!empty($this->cookies)) {
            $response = $response->withHeader('Set-Cookie', $this->buildCookieHeaders());
        }

        return $response;
    }

    public function get(string $name): ?array {
        return $this->state->request->getCookieParams()[$name] ?? null;
    }

    /**
     * Set a cookie in the response
     * 
     * @param string $name The name of the cookie
     * @param string $value The value of the cookie
     * @param int $expire The time to live for the cookie in seconds. A value of 0 means this is a session-cookie.
     * @param string $path The cookie path value.
     * @param string $domain The cookie domain value.
     * @param bool $secure Should this cookie only be sent over encrypted requests?
     * @param bool $httpOnly Should this cookie not be available for javascript on the client side?
     * @throws CodingErrorException 
     */
    public function set(string $name, string $value="", int $expire=0, string $path="", string $domain="", bool $secure=false, bool $httpOnly=false): void {
        $this->state->cacheability->privateCache();
        $this->cookies[$name] = [$value, $expire, $path, $domain, $secure, $httpOnly];
    }

    protected function buildCookieHeaders(): array {
        $headers = [];
        foreach($this->cookies as $name => $spec) {
            $str = $name."=".$spec[0];
            if($spec[1])
                $str .= "; expires=".gmdate('D, d M Y H:i:s \G\M\T', $spec[1]);
            if($spec[3])
                $str .= "; domain=".$spec[3];
            if($spec[2])
                $str .= "; path=".$spec[2];
            if($spec[4]) {
                $str .= "; secure; SameSite=None";
            }
            if($spec[5])
                $str .= "; httponly";

            $headers[] = $str;
        }
        return $headers;
    }

}