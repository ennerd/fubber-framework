<?php
namespace Fubber\Kernel\State;

use Fubber\CodingErrorException;
use Fubber\Kernel\State;
use Psr\Http\Message\ResponseInterface;

/**
 * Class is a backward compatability layer for the {State::$response} property.
 * 
 * @package State
 */
class ResponseFacade {

    protected State $state;
    protected array $cookies = [];

    /**
     * Headers which will be added to ResponseInterface objects when applied to
     * it.
     * 
     * @var array<string, string[]>
     */
    protected array $headers = [];

    /**
     * Record the casing of a header name
     * 
     * @var array<string, string>
     */
    protected array $headerCases = [];

    /**
     * Headers which will be unset when applied to a ResponseInterface object
     * 
     * @var array<string, string>
     */
    protected array $headersToUnset = [];

    public function __construct(State $state) {
        $this->state = $state;
    }

    public function apply(ResponseInterface $response): ResponseInterface {
        if (
            $this->state->cacheability->isPubliclyCacheable() &&
            $this->state->cacheability->isLocked() &&
            !empty($this->cookies)
        ) {
            throw new \Fubber\CodingErrorException("Refusing to set cookie on forced publicly cacheable response.");
        }

        if (!empty($this->cookies)) {
            $response = $response->withHeader('Set-Cookie', $this->buildCookieHeaders());
        }

        foreach ($this->headersToUnset as $lName => $name) {
            $response = $response->withoutHeader($name);
        }

        foreach ($this->headers as $lName => $values) {
            $name = $this->headerCases[$lName];
            $response = $response->withAddedHeader($name, $values);
        }

        return $response;
    }

    public function addHeader(string $name, string|array $values): static {
        $lName = \strtolower($name);
        $this->headerCases[$lName] = $name;
        foreach ((array) $values as $value) {
            $this->headers[$lName][] = (string) $value;
        }
        return $this;
    }

    public function setHeader(string $name, string|array $values): static {
        $lName = \strtolower($name);
        $this->headersToUnset[$lName] = $name;
        unset($this->headers[$lName], $this->headerCases[$lName]);
        $this->headerCases[$lName] = $name;
        foreach ((array) $values as $value) {
            $this->headers[$lName][] = (string) $value;
        }
        return $this;
    }

    public function unsetHeader(string $name): static {
        $lName = \strtolower($name);
        unset($this->headers[$lName], $this->headerCases[$lName]);
        $this->headersToUnset[$lName] = $name;
        return $this;
    }

    public function getHeaders(): array {
        return $this->headers;
    }

    /**
     * Alias for {@see Cacheability::noCache()} for backward compatability.
     * 
     * @throws CodingErrorException 
     */
    public function noCache(): static {
        $this->state->cacheability->noCache();
        return $this;
    }

    public function privateCache(int $ttl=null): static {
        $this->state->cacheability->privateCache($ttl);
        return $this;
    }

    public function publicCache(int $ttl=null, int $proxyTTL=null): static {
        $this->state->cacheability->publicCache($ttl, $proxyTTL);
        return $this;
    }

    public function lockCache(): static {
        $this->state->cacheability->lock();
        return $this;
    }

    public function setCookie(string $name, string $value="", int $expire=0, string $path="", string $domain="", bool $secure=false, bool $httpOnly=false): static {
        //echo "<pre>";\debug_print_backtrace();die();
        $this->privateCache();
        $this->cookies[$name] = [$value, $expire, $path, $domain, $secure, $httpOnly];
        return $this;
    }

    protected function buildCookieHeaders(): array {
        $headers = [];
        foreach($this->cookies as $name => $spec) {
            $str = $name."=".$spec[0];
            if($spec[1])
                $str .= "; expires=".gmdate('D, d M Y H:i:s \G\M\T', $spec[1]);
            if($spec[3])
                $str .= "; domain=".$spec[3];
            if($spec[2])
                $str .= "; path=".$spec[2];
            if($spec[4]) {
                $str .= "; secure; SameSite=None";
            }
            if($spec[5])
                $str .= "; httponly";

            $headers[] = $str;
        }
        return $headers;
    }

}