<?php
namespace Fubber\Kernel\State;

use ArrayAccess;
use Closure;
use Countable;
use Fubber\CodingErrorException;
use Fubber\Kernel\State;
use IteratorAggregate;
use Traversable;

/**
 * Provides mapping from State::$get to State::$request->getQueryParams()
 * 
 * @package Fubber\Kernel\State
 */
final class CallbackArrayAccess implements Countable, ArrayAccess, IteratorAggregate {

    public function __construct(
        protected readonly Closure $callback,
        protected readonly ?Closure $setter=null,
        protected readonly ?Closure $unsetter=null
    ) {}

    protected function getData(): array {
        $src = ($this->callback)();
        if ($src === null) {
            return [];
        }
        return (array) $src;
    }

    public function asArray(): array {
        return $this->getData();
    }

    public function asQueryString(): string {
        return \http_build_query($this->getData());
    }

    public function count(): int
    {
        return \count($this->getData());
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->getData()[$offset] ?? null;
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        if ($this->setter !== null) {
            ($this->setter)($offset, $value);
            return;
        } else {
            throw new CodingErrorException("Can't set key `$offset` for immutable array");
        }
    }

    public function offsetExists(mixed $offset): bool
    {
        return \array_key_exists($offset, $this->getData());
    }

    public function offsetUnset(mixed $offset): void
    {
        if ($this->unsetter !== null) {
            ($this->unsetter)($offset);
            return;
        } else {
            throw new CodingErrorException("Can't unset key `$offset` for immutable array");
        }
        $this->assertNotImmutable();
    }

    public function getIterator(): Traversable
    {
        foreach ($this->getData() as $k => $v) {
            yield $k => $v;
        }
    }

    protected function assertNotImmutable(): void {
        // allways immutable
        throw new CodingErrorException("Immutable data modified");
    }

    public function __debugInfo()
    {
        return [ 'callbackArrayAccess' => $this->getData() ];
    }
}