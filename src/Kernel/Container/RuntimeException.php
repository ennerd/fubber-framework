<?php
namespace Fubber\Kernel\Container;

use Psr\Container\ContainerExceptionInterface;

class RuntimeException extends \Fubber\RuntimeException implements ContainerExceptionInterface {}