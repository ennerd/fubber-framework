<?php
namespace Fubber\Kernel\Container;

enum Scope: string {

    /**
     * `Scope::Singleton` creates an instance which is reused for the
     * entire run-time of the application.
     * 
     * This is suitable for values that are identical for every request
     * and invocation - for example configuration data, and is not 
     * suitable for objects that have state.
     */
    case Singleton = 'Singleton';

    /**
     * `Scope::Request` provides a fresh instance for every HTTP request.
     * 
     * This is useful for services that contain request-specific information,
     * such as a database connection or a user session.
     */
    case Request = 'Request';

    /**
     * `Scope::Fresh` returns a new (fresh) instance for every request.
     * 
     * This is suitable for services such as an HTTP client which generally
     * are used once and then disposed of when the job is done.
     */
    case Fresh = 'Fresh';

}