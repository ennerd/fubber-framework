<?php
namespace Fubber\Kernel\Container;

use Closure;

/**
 * A factory returns zero or more Closures or class names. The dependency
 * injector will determine if it is able to use the Closure or class.
 * 
 * 
 * @package Fubber\Kernel
 */
interface FactoryInterface {

    /**
     * Return any number of Closures which will be auto-injected with their dependencies
     * if those dependencies are available.
     * 
     * @return iterable<Closure>
     */
    public static function getMakerFunctions(): iterable;

}