<?php
namespace Fubber\Kernel\Container;

use Fubber\CodingErrorException;
use Fubber\I18n\Translatable;
use Psr\Container\NotFoundExceptionInterface;

class NotFoundException extends ContainerException implements NotFoundExceptionInterface {

    public function getExceptionDescription(): string {
        return "A service id was not found in the container";
    }

}