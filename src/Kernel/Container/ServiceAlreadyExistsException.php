<?php
namespace Fubber\Kernel\Container;

class ServiceAlreadyExistsException extends RuntimeException {}