# Fubber\Kernel\Container

Inversion of Control is provided by the container; a class should not be
concerned with how to connect to a database and create a PDO object, or how
to cache data. Instead, various services are registered in a container - and
the container then provides values/objects as needed.

Your application can obtain values in one of two ways:

 1. A direct request to `$container->get($id)` where `$id` is the service
    identifier. The service identifier is generally an interface name or a
    class name.

 2. Through dependency injection; the container will inject instances of
    objects according to parameter type declarations (of either constructors
    or functions).


## Main Concepts

 1. A "service" is anything which can be requested from the container; for
    example a database connection, a caching API or a configuration file.

 2. A "consumer" is application code that requires one or more services.

 3. A "manifest" contains a payload (either a factory function or a value)
    along with parameters that define how and when new instances are created.

 4. A "namespace" is a string value which can be used to isolate instances
    of a single service between multiple consumers.

 5. A "factory" is a function for creating new instances of a service.


## Quick Start

 1. Create your Container instance:

 ```
 $container = new Fubber\Kernel\Container();
 ```

 2. Add services to the container:

 ```
$container->add(Psr\Log\LoggerInterface::class, new Factory

));


## Fetching a service

In the simplest form, you can use the Container as a plain PSR-11 Container
and just fetch services via a service ID. We recommend registering services
via an `interface` name, such as `Psr\Container\ContainerInterface`.

This way you don't bind your code to a particular implementation.

`$kernel->get(\PDO::class)` will return a PDO database connection for you.





Services can have certain requirements which must be satisfied before the
service can be retrieved. There are two main types of requirements:

 1. Namespace string. If a service implements the `NamespacedInterface`, it
    requires that you provide a unique string to indicate a namespace for your
    component.
 2. Instance Scope. An instance such as a database connection should not be
    shared between users, while an instance such as a cache backend may be
    shared.

## Adding more containers

Additional containers can be added via the `addContainer()` function. The `$offset`
argument indicates at which priority the container belongs. Offset `0` means that
the added container will be queried first, while `\PHP_INT_MAX` will append the
container.

## Removing a container

Removing containers is not supported; as soon as the application enters the
`Fubber\Kernel\Phases\Phase::RUNNING` phase, the environment should not change.

