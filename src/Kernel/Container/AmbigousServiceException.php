<?php
namespace Fubber\Kernel\Container;

use Fubber\I18n\Translatable;
use LogicException;

class AmbiguousServiceException extends LogicException {
    
    public function getExceptionDescription(): string {
        return "The service container id provided is ambiguous with multiple classes implementing it";
    }

}