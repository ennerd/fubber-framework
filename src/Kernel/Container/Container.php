<?php
namespace Fubber\Kernel\Container;

use Closure;
use Fubber\Hooks\Event;
use Fubber\Kernel;
use Fubber\PHP\ReflectionTypeTool;
use Fubber\Kernel\Debug\Describe;
use Fubber\Kernel\EventLoop\EventLoopInterface;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\ReflectionUtilities;
use Fubber\Promise\Promise;
use Fubber\Promise\PromiseInterface;
use Fubber\PHP\ClosureTool;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionType;
use ReflectionUnionType;
use Throwable;
use WeakMap;

class Container extends KernelExtension implements ContainerInterface {

    protected readonly object $globalObject;

    /**
     * Constructor functions
     * 
     * @var array<string, Closure[]>
     */
    private array $makers = [];

    /**
     * Scopes per constructor function
     * 
     * @var array<string, Scope>
     */
    private array $scopes = [];

    /**
     * @var array<string, string>
     */
    private array $aliases = [];

    /**
     * Caches instances returned based on their scope
     * 
     * @var WeakMap<object, mixed>
     */
    protected readonly WeakMap $cache;

    /**
     * The PID number that the cache is associated with, to ensure new
     * connections have to be created after a fork.
     * 
     * @var int
     */
    protected int $cachePid;

    protected readonly Closure $contextFinder;

    protected readonly Instrumentation $instrumentation;

    protected readonly EventLoopInterface $loop;

    /**
     * If something depends on a service becoming available, it can wait
     * for that to happen by calling `$this->waitFor($id)`. Listeners
     * are added here as deferred promises.
     * 
     * @var array<string, PromiseInterface>
     */
    protected array $awaitedServices = [];

    /**
     * Holds logged information about an ongoing dependency injection process.
     * 
     * @var array
     */
    protected ?array $log = null;

    public readonly Event $onServiceAdded;

    /**
     * The `$globalObject` is any PHP object which will exist until
     * the application terminates.
     * 
     * @param Closure $contextIdentifier `function(): object {}` A closure which returns an object which is unique per request, and will not change if a new instance of ServerRequest is made
     */
    public function __construct(Closure $contextIdentifier, Instrumentation $instrumentation) {
        $this->cache = new WeakMap();
        $this->cachePid = \getmypid();

        $ct = new ClosureTool(static function(): object { return new \stdClass; });
        $ct->assertValidSignature($contextIdentifier, __FILE__);
        if ($contextIdentifier() !== $contextIdentifier()) {
            throw new InvalidArgumentException("Argument \$contextFinder must return the same exact object when the context hasn't changed");
        }

        $this->contextFinder = $contextIdentifier;
        $this->instrumentation = $instrumentation;
        $this->onServiceAdded = new Event("Service added");
    }

    /**
     * The name of this extension.
     * 
     * @return string 
     */
    public function getExtensionName(): string {
        return "Service Container";
    }

    /**
     * @param string $id The service identifier
     * @return mixed Entry.
     */
    public function get(string $id) {
        if (!$this->has($id)) {
            throw new NotFoundException("The service `$id` is not registered.");
        }

        switch ($this->scopes[$id]) {
            case Scope::Singleton:
                $token = $this;
                break;
            case Scope::Request:
                $token = ($this->contextFinder)();
                if ($token === null) {
                    // falling back to global context; assuming there is no current context
                    $token = $this;
                }
                //var_dump($token);
                break;
            case Scope::Fresh:
                $token = null;
        }

        if ($token !== null) {
            if ($this->cachePid !== ($pid = \getmypid())) {
                // if the process has forked, we'll recreate all services we are caching
                $this->cache = new WeakMap();
                $this->cachePid = $pid;
            }
            if (isset($this->cache[$token][$id])) {
                return $this->cache[$token][$id];
            }
        }

        //$bench = $this->instrumentation->benchmark('Container get `'.$id.'`');
        try {
            while (!isset($this->makers[$id]) && isset($this->aliases[$id])) {
                $id = $this->aliases[$id];
            }

            foreach ($this->makers[$id] as $num => $maker) {
                $maker = $this->mayInvoke($maker);
                if ($maker !== null) {
                    goto found_maker;
                }
            }
            throw new ContainerException("Unable to find a suitable factory function for service `$id`");

            found_maker:
            $service = $maker();

            if ($token !== null) {
                $cache = $this->cache[$token] ?? [];
                $cache[$id] = $service;
                $this->cache[$token] = $cache;
            }

            return $service;
        } finally {
            //$bench();
        }
    }

    public function has(string $id): bool {
        $result = !empty($this->makers[$id]);
        if (!$result) {
            Kernel::notice("Container was queried for missing service `{id}` from " . Describe::stackTrace(), [ 'id' => $id]);
        }
        return $result;
    }

    /**
     * Asynchronously get a service
     * 
     * @param string $id 
     * @return PromiseInterface 
     */
    public function getAsync(string ...$serviceIds): PromiseInterface {
        $promises = [];
        foreach ($serviceIds as $id) {
            if (!isset($this->awaitedServices[$id])) {
                $this->awaitedServices[$id] = new PromiseInterface(function($yes, $no) use ($id) {
                    if ($this->has($id)) {
                        $yes($this->get($id));
                    } else {
                        $this->onServiceAdded->listen(function(string $id) use ($yes) {
                            $yes($this->get($id));
                        });
                    }
                });
            }
            $promises[] = $this->awaitedServices[$id];
        }
        if (count($promises) === 0) {
            return $promises[0];
        }
        return Promise::all(...$promises);
    }

    public function add(string $id, Closure|Factory|string $maker, Scope $scope): void {
        static $depth = 0;
        ++$depth;
        try {
            if (\is_string($maker)) {
                foreach ($maker::getMakerFunctions() as $makerFunction) {
                    $this->add($id, $makerFunction, $scope);
                }
                return;
            } elseif ($maker instanceof Factory) {
                foreach ($maker as $makerFunction) {
                    $this->add($id, $makerFunction, $scope);
                }
                return;
            }
            if (isset($this->scopes[$id]) && $this->scopes[$id] !== $scope) {
                throw new InvalidArgumentException("Service is already provided with scope `" . $scope->name ."`. Can't have different scopes for the same service.");
            } else {
                $this->scopes[$id] = $scope;
            }
            $this->makers[$id][] = $maker;
        } finally {
            if (--$depth) {
                $this->onServiceAdded->trigger($id);
            }
        }
    }

    /**
     * Add amy specific value to the container, such as an object instance
     * or even an array or a string. This instance will then be injected by
     * using the id.
     * 
     * @param string $id 
     * @param mixed $value 
     * @throws InvalidArgumentException 
     * @throws Throwable 
     */
    public function addSingleton(string $id, mixed $value): void {
        if (!empty($this->makers[$id])) {
            throw new InvalidArgumentException("Service `$id` is already registered");
        }
        $this->add($id, function() use ($value) {
            return $value;
        }, Scope::Singleton);
    }

    /**
     * Constructs an instance of the given class with dependencies resolved from the container.
     *
     * @param string $className The fully qualified name of the class to instantiate.
     * @param array|null $namedArguments An array of named arguments to pass to the constructor.
     * @param array|null $extraObjects Additional objects for dependency injection resolution.
     * @return object The constructed instance.
     * @throws InvalidArgumentException if the class cannot be instantiated.
     * @throws ReflectionException if a reflection error occurs.
     */
    public function construct(string $className, ?array $namedArguments = [], ?array $extraObjects = []): object {
        try {
            $reflectionClass = new ReflectionClass($className);

            // Check if the class can be instantiated
            if (!$reflectionClass->isInstantiable()) {
                throw new InvalidArgumentException("Class `$className` cannot be instantiated.");
            }

            // Get the constructor of the class
            $constructor = $reflectionClass->getConstructor();

            // If there is no constructor, simply instantiate the class
            if ($constructor === null) {
                return $reflectionClass->newInstance();
            }

            // Use existing `mapParameters` to resolve constructor dependencies with additional arguments
            $dependencies = $this->mapParameters($constructor, $namedArguments, ...$extraObjects);

            // Instantiate the class with resolved dependencies
            return $reflectionClass->newInstanceArgs($dependencies);

        } catch (ReflectionException $e) {
            throw new InvalidArgumentException("Failed to construct `$className`: " . $e->getMessage(), 0, $e);
        }
    }
    
    public function invoke(Closure $callable, ?array $namedArguments=[], ?array $extraObjects=[]): mixed {
        $log = $this->log;
        try {
            $this->log = [];
            $rf = new ReflectionFunction($callable);
            if ($rf->getNumberOfParameters() === 0) {
                return $callable();
            }
            $parameters = $this->mapParameters($rf, $namedArguments, ...$extraObjects);
            return $callable(...$parameters);
        } catch (DependencyInjectionError $e) {
            $this->showDependencyInjectionError($e, $this->log);
        } finally {
            $this->log = $log;
        }
    }

    /**
     * Checks if we are able to invoke a callable. The returned Closure caches all the information
     * needed to actually invoke the callable.
     * 
     * @param Closure $callable 
     * @param null|array $namedArguments 
     * @param null|array $extraObjects 
     * @return null|Closure 
     */
    public function mayInvoke(Closure $callable, ?array $namedArguments=[], ?array $extraObjects=[]): ?Closure {
        $bench = $this->instrumentation->benchmark("Container::mayInvoke()");
        try {
            $rf = new ReflectionFunction($callable);
            if ($rf->getNumberOfParameters() === 0) {
                return $callable;
            }
            $oldLog = $this->log;
            $this->log = [];
            $parameters = $this->mapParameters($rf, $namedArguments, ...$extraObjects);
            $log = $this->log;
            $this->log = $oldLog;
            return function() use ($callable, $parameters, $log) {
                try {
                    return $callable(...$parameters);
                } catch (DependencyInjectionError $e) {
                    $this->showDependencyInjectionError($e, $log);
                }
            };
/*
        } catch (\Throwable $e) {
            var_
            return null;
*/
        } finally {
            $bench();
        }
    }

    public function addAliases(string $id, string ...$aliases): void {
        if (!$this->has($id)) {
            throw new NotFoundException("The service `$id` does not exist");
        }
        foreach ($aliases as $alias) {
            if ($this->has($alias)) {
                throw new ServiceAlreadyExistsException("The service `$alias` already exists");
            }
        }
        foreach ($aliases as $alias) {
            $this->aliases[$alias] = $id;
        }
    }

    /**
     * @return ReflectionType[]
     */
    protected function getMissingReflectionTypes(ReflectionFunctionAbstract $rm, array $namedArguments=[], object ...$extraObjects): array {
        $missingReflectionTypes = [];
        foreach ($rm->getParameters() as $parameter) {
            /*
            if ($parameter->isVariadic()) {
                // Currently not attempting to inject variadic
                continue;
            }
            */
            $paramDescription = Describe::parameter($parameter);
            if (isset($namedArguments[$parameter->getName()])) {
                continue;
            }

            if (isset($namedArguments[$parameter->getPosition()])) {
                continue;
            }
            
            if ($parameter->hasType()) {
                $type = $parameter->getType();
                if ($type instanceof ReflectionNamedType && $type->getName() !== 'mixed') {
                    foreach ($extraObjects as $eo) {
                        if (ReflectionTypeTool::checkType($type, $eo)) {
                            continue 2;
                        }
                    }
                }
                $typeName = $this->getIdFromReflectionType($type);
                if ($typeName) {
                    continue;
                }
            }
            if ($parameter->isDefaultValueAvailable()) {
                continue;
            }
            if ($parameter->isVariadic()) {
                // variadic parameters are not required, so stop
                break;
            }
            $missingReflectionTypes[$parameter->getName()] = $type;
        }
        return $missingReflectionTypes;
    }

    protected function mapParameters(ReflectionFunctionAbstract $rm, array $namedArguments=[], object ...$extraObjects): mixed {
        $desc = [];
        // the following code presumes that validation above was perfect
        $params = [];
        $this->log[] = 'Mapping parameters for '.Describe::function($rm);
        foreach ($rm->getParameters() as $parameter) {
            /*
            if ($parameter->isVariadic()) {
                // Currently not attempting to inject variadic
                continue;
            }
            */
            $paramDescription = Describe::parameter($parameter);
            if (isset($namedArguments[$parameter->getName()])) {
                $this->log[] = ' - Added parameter ' . $paramDescription . ' from $namedArguments[' . $parameter->getName() . ']';
                $params[] = $namedArguments[$parameter->getName()];
                continue;
            } else {
                $this->log[] = ' - Parameter ' . $paramDescription . ' was not passed via $namedArguments by name';
            }

            if (isset($namedArguments[$parameter->getPosition()])) {
                $this->log[] = ' - Added parameter ' . $paramDescription . ' from $namedArguments[' . $parameter->getPosition() . ']';
                $params[] = $namedArguments[$parameter->getPosition()];
                continue;
            } else {
                $this->log[] = ' - Parameter ' . $paramDescription . ' was not passed via $namedArguments by offset';
            }
            
            if ($parameter->hasType()) {
                $type = $parameter->getType();
                if ($type instanceof ReflectionNamedType && $type->getName() !== 'mixed') {
                    foreach ($extraObjects as $eo) {
                        if (ReflectionTypeTool::checkType($type, $eo)) {
                            $this->log[] = ' - Added parameter ' . $paramDescription . ' from $extraObjects after checking the type';
                            $params[] = $eo;
                            continue 2;
                        }
                    }
                }
                $this->log[] = ' - Parameter ' . $paramDescription . ' was not passed via $extraObjects';
                $typeName = $this->getIdFromReflectionType($type);
                if ($typeName) {
                    $this->log[] = ' - Added parameter ' . $paramDescription . ' from $this->get(' . $typeName . ')';
                    $params[] = $this->get($typeName);
                    continue;
                } else {
                    $this->log[] = ' - No service available in container for parameter ' . $paramDescription;
                }
            } else {
                $this->log[] = ' - Parameter ' . $paramDescription . ' does not have a type';
            }
            if ($parameter->isDefaultValueAvailable()) {
                $this->log[] = ' - Added parameter ' . $paramDescription . ' using the default parameter value';
                $params[] = $parameter->getDefaultValue();
                continue;
            }
            if ($parameter->isVariadic()) {
                // variadic parameters are not required, so stop
                break;
            }
            $paramName = Describe::parameter($parameter);
            $methodName = Describe::function($rm);

            Kernel::error("Parameter `{paramName}` has no candidate services trying to perform dependency injection on `{methodName}`", [ 'paramName' => $paramName, 'methodName' => $methodName], $this->log);
            throw new ServiceRequirementException("Parameter `$paramName` has no candidate services trying to perform dependency injection on `$methodName`");
        }
        return $params;
    }

    /**
     * Get a service based on a ReflectionType instead of by an id string
     */
    protected function getIdFromReflectionType(ReflectionType $type): ?string {
        if ($type instanceof ReflectionNamedType) {
            if ($this->has($type->getName())) {
                return $type->getName();
            }
            return null;
        } elseif ($type instanceof ReflectionUnionType) {
            foreach ($type->getTypes() as $unionType) {
                if ($this->has($unionType->getName())) {
                    return $unionType->getName();
                }
            }
            return null;
        } else {
            throw new ServiceRequirementException("Unsupported reflection with `" . $type::class . "`");
        }
    }

    protected static function getDescriptiveFunctionName(ReflectionFunctionAbstract $rf) {
        $name = $rf->getName();
        $class = $rf->getClosureScopeClass();
        if ($class !== null) {
            $name = $class->getName() . '::' . $name;
        }
        return $name;
    }

    protected static function getDescriptiveParameterName(ReflectionParameter $rp) {
        
        $rf = $rp->getDeclaringFunction();
        $name = self::getDescriptiveFunctionName($rf);
        $params = [];
        foreach ($rf->getParameters() as $frp) {
            $parameterString = ReflectionUtilities::getSignatureFromReflectionParameter($frp);
            if ($frp->getPosition() === $rp->getPosition() && $rf->getNumberOfParameters() > 1) {
                $parameterString = '==' . $parameterString . '==';
            }
            $params[] = $parameterString;
        }
        $name .= '(' . implode(", ", $params) . ')';
        return $name;
    }

    protected static function getDescriptiveType(ReflectionType $rt): string {
        return ReflectionUtilities::getSignatureFromReflectionType($rt);
    }

    private function showDependencyInjectionError(Throwable $e, array $log): void {
        echo "<pre>";
        echo $e->getMessage()."\n\n";
        echo \implode("\n", $log)."\n";
        die();
    }
}
