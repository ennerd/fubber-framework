<?php
namespace Fubber\Kernel\Container;

use Fubber\I18n\Translatable;
use Fubber\Kernel\Container\ContainerException;

class ServiceRequirementException extends ContainerException {

    public function getExceptionDescription(): string {
        return "The service has dependencies or requirements that could not be met";
    }
    
}