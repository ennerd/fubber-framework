<?php
namespace Fubber\Kernel\Container;

/**
 * Exception is thrown by dependency injection receivers if they received
 * incorrect services. This makes it easier to debug dependency injection.
 * 
 * @package Fubber\Kernel\Container
 */
class DependencyInjectionError extends RuntimeException {

}