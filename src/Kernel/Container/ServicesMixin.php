<?php
namespace Fubber\Kernel\Container;

use Closure;
use Fiber;
use Fubber\ConfigErrorException;
use Fubber\InternalErrorException;
use Fubber\InvalidArgumentException;
use Fubber\Kernel;
use Fubber\Kernel\Container\InvalidArgumentException as ContainerInvalidArgumentException;
use Fubber\Kernel\EventLoop\EventLoopMixin;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Kernel\Http\Http;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Kernel\Instrumentation\InstrumentationMixin;
use Fubber\Kernel\Phases\PhasesMixin;
use Fubber\Kernel\UnknownServiceException;
use Fubber\LogicException;
use Fubber\Mixins\MixinConstructor;
use InvalidArgumentException as GlobalInvalidArgumentException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionException;
use Throwable;
use WeakMap;

use function Fubber\kernel;

/**
 * @mixin Kernel
 */
trait ServicesMixin {
    use ExtensionsMixin;
    use InstrumentationMixin;
    use EventLoopMixin;

    public readonly Container $container;
    private readonly WeakMap $serviceCacheOwners;

    #[MixinConstructor(__TRAIT__, ExtensionsMixin::class, InstrumentationMixin::class, EventLoopMixin::class)]
    private function constructServices(): void {

        $this->container = new Container(function(): object {
            /**
             * Each HTTP request should run inside a separate Fiber so that
             * we can ensure that each request has distinct database connections.
             * When outside of a HTTP request, we'll be using the kernel instance
             * for caching.
             */
            return Fiber::getCurrent() ?? $this;
        }, $this->instrumentation, $this->eventLoop);

        $this->addExtension($this->container);
        $this->serviceCacheOwners = new WeakMap();
        $this->container->addSingleton(Kernel::class, $this);
        $this->container->addSingleton(Container::class, $this->container);
        $this->container->addAliases(Container::class, ContainerInterface::class);
        $this->onExtensionAdded->listenFor(Http::class, function(Http $instance) {
            die("ServicesMixin received an Http instance. Add middleware.");
        });
        
    }

    /**
     * Invoke a closure using dependency injection.
     * 
     * @param Closure $callable 
     * @param array $namedArguments 
     * @return mixed 
     * @throws LogicException 
     * @throws Throwable 
     */
    public function invoke(Closure $callable, array &$namedArguments=[]): mixed {
        try {
            return $this->container->invoke($callable, $namedArguments);
        } catch (ContainerExceptionInterface $e) {
            throw Instrumentation::setExceptionInfo(
                $e,
                closure: $callable
            );
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    /**
     * Get a service implementation. If a ServerRequestInterface is provided, the instance may be cached for that
     * object. If a $consumingClassOrObject is provided, the instance may be namespaced for that object.
     * 
     * {@see ContainerInterface::get()}
     * @param string $id 
     * @param null|ServerRequestInterface $request The request object
     * @param string|object|null $receiver The object or class that will be receiving this service instance
     * @return mixed 
     * @throws InternalErrorException 
     */
    public function get(
        string $id
    ) {
        return $this->container->get($id);
    }

    /**
     * Check if a particular service is available
     *
     * @param string $id
     * @return boolean
     */
    public function has(string $id): bool {
        return $this->container->has($id);
    }
}