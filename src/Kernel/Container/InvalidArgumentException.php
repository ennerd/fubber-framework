<?php
namespace Fubber\Kernel\Container;

use Psr\Container\ContainerExceptionInterface;

class InvalidArgumentException extends \Fubber\InvalidArgumentException implements ContainerExceptionInterface {}