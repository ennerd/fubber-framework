<?php
namespace Fubber\Kernel\Container;

use Fubber\I18n\Translatable;

class ContainerException extends RuntimeException {

    public function getExceptionDescription(): string {
        return "A generic exception related to the service container";
    }

}