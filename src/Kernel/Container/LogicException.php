<?php
namespace Fubber\Kernel\Container;

use Psr\Container\ContainerExceptionInterface;

class LogicException extends \Fubber\LogicException implements ContainerExceptionInterface {
    
}