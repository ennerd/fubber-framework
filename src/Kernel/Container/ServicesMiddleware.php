<?php
namespace Fubber\Kernel\Container;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use stdClass;

/**
 * Injects an stdClass object into the request. This object can be referenced by
 * a WeakMap and should provide a persistent way to attach metadata to a ServerRequestInterface
 * object even if that object is replaced with new instances through any of the 
 * ServerRequestInterface::with... methods.
 * 
 * @package Fubber\Kernel
 */
class ContainerMiddleware implements MiddlewareInterface {
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        return $handler->handle($request->withAttribute('_fubberServiceCacheToken', new stdClass));
    }
}