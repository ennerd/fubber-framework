<?php
namespace Fubber\Kernel\Container;

/**
 * Indicates that the service supports namespacing.
 * 
 * @package Fubber\Kernel\Container
 */
interface NamespacedInterface {

    /**
     * Get a new instance where the API is restricted to operating
     * on a subset of the data.
     * 
     * @param string $namespace 
     * @return static 
     */
    public function withNamespace(string $namespace): static;

    /**
     * Get the currently used namespace if any.
     * 
     * @return null|string 
     */
    public function getNamespace(): ?string;
}