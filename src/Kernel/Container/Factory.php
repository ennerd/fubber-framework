<?php
namespace Fubber\Kernel\Container;

use Closure;
use IteratorAggregate;
use Traversable;

final class Factory implements IteratorAggregate {

    protected array $makerFunctions;

    public function __construct(Closure ...$makerFunctions) {
        $this->makerFunctions = $makerFunctions;
    }

    public function getIterator(): Traversable {
        yield from $this->makerFunctions;
    }

}