<?php
namespace Fubber\Kernel;

use Closure;
use Fubber\CodingErrorException;
use Fubber\FubberAPI;
use Fubber\Hooks;
use Fubber\Http\ServerRequest;
use Fubber\I18n\I18n;
use Fubber\I18n\II18n;
use Fubber\JsonView;
use Fubber\Hooks\Event;
use Fubber\Kernel;
use Fubber\Kernel\State\Cacheability;
use Fubber\Kernel\State\Cookies;
use Fubber\Kernel\State\ServerRequestFacade;
use Fubber\Kernel\State\ResponseFacade;
use Fubber\LogicException;
use Fubber\PlainView;
use Fubber\Redirect;
use Fubber\Session;
use Fubber\Session\ISession;
use Fubber\StaticConstructor\StaticConstructor;
use Fubber\View;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\{ServerRequestInterface, ResponseInterface};
use stdClass;

use function Fubber\kernel;

/**
 * The State class collects state information pertaining to the request that is
 * currently being processed. The information collected is made available to the
 * template system and other components that are invoked late in the request-response
 * cycle. It is also used to update the final ResponseInterface object which will
 * be sent to the client.
 * 
 * @property bool $html Is the response going to be an HTML response?
 * @property bool $json Is the reponse going to be a JSON response?
 * @property-read II18n $locale The locale for the current request
 * @property-read ISession $session The current request's session
 */
class State implements ContainerInterface {
    use FubberAPI;

    public static Event $onConstructed;
    protected static ?State $activeState = null;

    #[StaticConstructor]
    public static function staticConstructor(): void {
        self::$onConstructed = new Event();
    }

    public array $caughtExceptions = [];

    /**
     * Register response transformations for the ResponseInterface object which
     * will be sent to the browser.
     * 
     * @var ResponseFacade
     */
    public readonly ResponseFacade $response;

    /**
     * Provides access to the active ServerRequestInterface instance.
     * 
     * @var ServerRequestFacade
     */
    public readonly ServerRequestFacade $request;

    public readonly Cacheability $cacheability;

    public readonly Kernel $kernel;

    protected array $services = [];

    /**
     * Provides array access to ServerRequestInterface::getQueryParams().
     */
    public ?array $_get = null;

    /**
     * Provides array access to ServerRequestInterface::getQueryParams().
     */
    public ?array $_post = null;

    /**
     * Work with cookies for the request and response.
     * 
     * @var Cookies
     */
    public readonly Cookies $cookies;

    /**
     * Name of headers to unset when ResponseState is applied to a Response
     * 
     * @var string[]
     */
    protected array $headersToUnset = [];

    /**
     * Header values that will be added to the final response
     * 
     * @var array<string, string[]>
     */
    protected array $headers = [];
    

    /**
     * The response content type expected; 'application/json' means that we should
     * respond with 'application/json'
     *
     * @var string|null
     */
    protected ?string $type = null;

    protected array $lazyProperties = [];
    protected array $addedProperties = [];

    /**
     * Use this to cache stuff  that pertains to the current request
     */
    public array $cache = [];

    private bool $isConstructing = true;

    public function __construct(Kernel $kernel, ServerRequestInterface $request) {
        $this->kernel = $kernel;

        $this->request = new ServerRequestFacade($request);
        $this->response = new ResponseFacade($this);

        $this->cacheability = new Cacheability($this);
        
        self::$onConstructed->trigger($this);

        // @deprecated convention
        Hooks::dispatch(__METHOD__, $this);
        Hooks::dispatch('State', $this);
        $this->isConstructing = false;
    }

    public function get(string $id) {
        return $this->services[$id] ?? null;
    }

    public function has(string $id): bool {
        return \array_key_exists($id, $this->services);
    }

    public function add(string $id, object $instance): void {
        if (\array_key_exists($id, $this->services)) {
            throw new LogicException("Service `id` has already been added to the State object");
        }
        $this->services[$id] = $instance;
    }

    public function invoke(Closure $callback): mixed {
        return kernel()->container->invoke($callback, extraObjects: $this->services);
    }


    public function lazy(string $propertyName, Closure $valueFunction): void {
        if (isset($this->lazyProperties[$propertyName])) {
            throw new CodingErrorException("State::\$$propertyName is already declared");
        }
        $this->lazyProperties[$propertyName] = $valueFunction;
    }

    public function __set($name, $value) {
        if (method_exists($this, $methodName = 'get' . ucfirst($name))) {
            $this->$methodName($value);
        } elseif ($this->isConstructing) {
            // Can't modify properties during construction, because
            // it's not clear which order such properties are set.
            if ($this->__isset($name)) {
                throw new LogicException("State property '$name' already declared");
            }
            $this->addedProperties[$name] = $value;
        } else {
            if (!$this->__isset($name)) {
                throw new LogicException("Can't set state properties ('$name') that haven't been declared via the State::\$onConstructed hook");
            }
            if (!\array_key_exists($name, $this->addedProperties)) {
                throw new LogicException("Can't modify the '$name' property (it's a dynamic property)");
            }

            if (is_numeric($this->addedProperties[$name]) && is_numeric($value)) {
                $this->addedProperties[$name] = $value;
            } elseif (\get_debug_type($this->addedProperties[$name]) == \get_debug_type($value)) {
                $this->addedProperties[$name] = $value;
            } else {
                throw new LogicException("Can't change type of the '$name' property (expecting " . get_debug_type($this->addedProperties[$name]));
            }
        }
    }

    public function __isset($name)
    {
        return $name === 'get' || $name === 'post' || array_key_exists($name, $this->addedProperties) || array_key_exists($name, $this->lazyProperties) || \method_exists($this, 'get' . \ucfirst($name));
    }

    public function &__get($name)
    {
        if ($name === 'get') {
            if ($this->_get === null) {
                Kernel::debug("SETTING _GET", [], $this->request->getQueryParams());
                $this->_get = $this->request->getQueryParams();
            } 
            return $this->_get;
        } elseif ($name === 'post') {
            if ($this->_post === null) {
                Kernel::debug("SETTING _POST", [], $this->request->getParsedBody() ?? []);
                $this->_post = $this->request->getParsedBody();
            }
            return $this->_post;
        } elseif (isset($this->lazyProperties[$name])) {
            $value = ($this->lazyProperties[$name])($this);
            return $value;
        } elseif (\method_exists($this, $methodName = 'get' . \ucfirst($name))) {
            $value = $this->$methodName();
            return $value;
        } elseif (isset($this->addedProperties[$name])) {
            return $this->addedProperties[$name];
        } else {
            throw new CodingErrorException("Property `$name` is not declared");
        }
    }

    public function __call($name, $arguments)
    {
        if (isset($this->lazyProperties[$name])) {
            return ($this->lazyProperties[$name])($this, ...$arguments);
        }
        throw new CodingErrorException("Method `$name` is not declared");
    }

    /**
     * @internal
     * @param ResponseInterface $response 
     * @return ResponseInterface 
     * @throws InvalidArgumentException 
     * @throws LogicException 
     * @throws CodingErrorException 
     */
    public function apply(ResponseInterface $response): ResponseInterface {
        if ($this->type !== null) {
            $response = $response->withHeader("Content-Type", $this->type);
        }
        $response = $this->response->apply($response);
        $response = $this->cacheability->apply($response);

        return $response;
    }

    /**
     * Return a view for a plain string
     */
    public function plain($output, array $headers=[], $statusCode=200, $reasonPhrase="Ok"): PlainView {
        extract(Hooks::filter(__METHOD__, ['output' => $output, 'headers' => $headers, 'statusCode' => $statusCode, 'reasonPhrase' => $reasonPhrase], $this), \EXTR_IF_EXISTS);
        return Hooks::filter(__METHOD__.'()', new \Fubber\PlainView($output, $headers, $statusCode, $reasonPhrase), $this);
    }

    /**
     * Return a view for JSON data
     */
    public function json($output, array $headers=[], $statusCode=200, $reasonPhrase="Ok"): JsonView {
        extract(Hooks::filter(__METHOD__, ['output' => $output, 'headers' => $headers, 'statusCode' => $statusCode, 'reasonPhrase' => $reasonPhrase], $this), \EXTR_IF_EXISTS);
        return Hooks::filter(__METHOD__.'()', new \Fubber\JsonView($output, $headers, $statusCode, $reasonPhrase), $this);
    }

    /**
     * Return a normal templated view
     */
    public function view($template, array $vars=[], array $headers=[], $statusCode=200, $reasonPhrase="Ok"): View {
        extract(Hooks::filter(__METHOD__, ['template' => $template, 'vars' => $vars, 'headers' => $headers, 'statusCode' => $statusCode, 'reasonPhrase' => $reasonPhrase], $this), \EXTR_IF_EXISTS);
        if(!isset($vars['state'])) {
            $vars['state'] = $this;
        }
        $view = new \Fubber\View($template, $vars, $headers, $statusCode, $reasonPhrase);
        return Hooks::filter(__METHOD__.'()', $view, $this);
    }

    /**
     * Return a redirect view
     */
    public function redirect($targetUri, int $statusCode=\Fubber\Redirect::CONTINUE_AT, $reasonPhrase="Redirect", array $headers=[]): Redirect {
        extract(Hooks::filter(__METHOD__, ['targetUri' => $targetUri, 'statusCode' => $statusCode, 'reasonPhrase' => $reasonPhrase, 'headers' => $headers], $this), \EXTR_IF_EXISTS);
        $redirect = new \Fubber\Redirect($targetUri, $statusCode, $reasonPhrase);
        return Hooks::filter(__METHOD__, $redirect, $this);
    }
    
    /**
     * Prevent cloning of the State object; the user has only one state
     *
     * @return void
     */
    public final function __clone() {
        throw new \Fubber\CodingErrorException("State must never be cloned.");
    }
    
    /**
     * Get the session object
     *
     * @return Session
     */
    public function getSession(): ISession {
        if (!isset($this->cache[ISession::class])) {
            $this->cache[ISession::class] = Session::getCurrent($this);
        }
        return $this->cache[ISession::class];
    }
    
    /**
     * Get the ServerRequest object
     *
     * @return ServerRequest
     */
    public function getRequest(): ServerRequestInterface {
        return $this->request;
    }
    
    /**
     * Get the locale for the current request
     *
     * @return II18n
     */
    public function getLocale(): II18n {
        if (!isset($this->cache[II18n::class])) {
            $this->cache[II18n::class] = I18n::fromAcceptLanguageHeader($this->request->getHeaderLine('Accept-Language'));
        }
        return $this->cache[II18n::class];
    }

    public function getResponse(): ResponseFacade {
        return $this->response;
    }

    public function setHtml(bool $value) {
        if ($value) {
            $this->type = "text/html";
        } else {
            $this->type = null;
        }
    }

    public function getHtml(): bool {
        return $this->type === 'text/html';
    }

    public function setJson(bool $value) {
        if ($value) {
            $this->type = 'application/json';
        } else {
            $this->type = null;
        }
    }

    public function getJson(): bool {
        return $this->type === 'application/json';
    }
}
