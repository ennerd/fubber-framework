<?php
namespace Fubber\Kernel\Extensions;

use Fubber\InvalidArgumentException;
use Fubber\Kernel;
use Fubber\Hooks\Event;
use Fubber\Hooks\Trigger;
use Fubber\Kernel\Extensions\KernelExtension as Extension;
use Fubber\Hooks\PerItemTriggers;
use Fubber\Mixins\MixinConstructor;
use WeakMap;

/**
 * @mixin Kernel
 */
trait ExtensionsMixin {

    private array $extensions = [];
    private WeakMap $extensionMap;


    /**
     * Event is triggered whenever a kernel module is added.
     * 
     * @var PerItemTriggers
     */
    public readonly PerItemTriggers $onExtensionAdded;

    #[MixinConstructor(__TRAIT__)]
    private function constructModules(): void {
        $this->extensionMap = new WeakMap();
        $this->onExtensionAdded = new PerItemTriggers("Triggered for each extension added to the Kernel");
    }

    protected function addExtension(Extension $extension): void {
        if ($this->hasExtension($extension)) {
            throw new InvalidArgumentException("Extension `".$extension->getExtensionName()."` already added");
        }
        if (!empty($extension->extensionState[Kernel::class])) {
            throw new InvalidArgumentException("Extension `".$extension->getExtensionName()."` is added to a kernel already");
        }
        $extension->extensionState[Kernel::class] = $this;
        $this->extensionMap[$extension] = true;
        $this->extensions[] = $extension;
        $this->onExtensionAdded->triggerFor($extension);
    }

    public function getExtensions(): array {
        return $this->extensions;
    }

    public function hasExtension(Extension $module): bool {
        return isset($this->extensionMap[$module]);
    }

}