<?php
namespace Fubber\Kernel\Extensions;

use Fubber\Kernel;
use Fubber\Kernel\ExtensionBase;

abstract class KernelExtension extends ExtensionBase {

    /**
     * Return the module instance name
     * 
     * @return string 
     */
    abstract public function getExtensionName(): string;

    public function __destruct() {
        if (!isset($this->extensionState[Kernel::class])) {
            Kernel::debug("`".static::class."` was not added to kernel\n");
        }
    }
    
}