<?php
namespace Fubber\Kernel\Phases;

use Fubber\Hooks;
use Fubber\Kernel;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Hooks\StateMachine;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Kernel\Instrumentation\InstrumentationMixin;
use Fubber\Mixins\MixinConstructor;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionMethod;

/**
 * @mixin Kernel
 * @mixin Modules
 */
trait PhasesMixin {
    use ExtensionsMixin;
    use InstrumentationMixin;
    //use ServicesMixin;

    /**
     * Controls the valid states for the kernel.
     * 
     * @see Phase::PHASE_*
     * @var StateMachine<string>
     */
    public readonly StateMachine $phases;

    private array $phaseListeners = [];

    #[MixinConstructor(__TRAIT__, ExtensionsMixin::class, InstrumentationMixin::class)] //, ServicesMixin::class)]
    public function constructPhases(): void {
        $this->phases = new StateMachine([
            [ Phase::Constructing, Phase::System, Phase::Failed ],
            [ Phase::System, Phase::Bundles, Phase::Failed ],
            [ Phase::Bundles, Phase::Services, Phase::Failed ],
            [ Phase::Services, Phase::Controllers, Phase::Failed ],
            [ Phase::Controllers, Phase::Integrate, Phase::Failed ],
            [ Phase::Integrate, Phase::Ready, Phase::Failed ],
            [ Phase::Ready, Phase::Terminated, Phase::Failed ],
            [ Phase::Failed, Phase::Terminated ],
            [ Phase::Terminated ],
        ], "Kernel phase manager");

        $this->onExtensionAdded->listen(function(KernelExtension $module) {
            $this->scanPhasesAttributes($module);
        });

        $this->scanPhasesAttributes($this);
        /**
         * Trigger legacy hooks
         */
        $this->phases->listen(function(Phase $fromState, Phase $toState) {
            Hooks::dispatch('Kernel.Phase.'.$fromState->name.'.Done', $this);
            Hooks::dispatch('Kernel.Phase.'.$toState->name.'.Starting', $this);
        });

        $this->onConstructorDone->listen($this->runPhases(...));
    }

    private function scanPhasesAttributes(object $target): void {
        Kernel::debug("Scanning attributes of {target}", ['target' => $target::class]);
        $rc = new ReflectionClass($target);
        foreach ($rc->getMethods(ReflectionMethod::IS_PRIVATE | ReflectionMethod::IS_PROTECTED | ReflectionMethod::IS_PUBLIC) as $rm) {
            foreach ($rm->getAttributes(PhaseHandler::class, ReflectionAttribute::IS_INSTANCEOF) as $ra) {
                /** @var PhaseHandler */
                $attribute = $ra->newInstance();
                Kernel::debug("- found attribute {target}::{method} with priority {priority}", [
                    'target' => $target::class,
                    'method' => $rm->getName(),
                    'priority' => $attribute->ordering,
                ]);
                $attribute->setMethod($rm->getClosure($target));
                $phase = $attribute->phase;

                if (empty($this->phaseListeners[$phase->name])) {
                    //Kernel::debug("- adding event listener for {phase}.entered", ['phase' => $phase->name]);
                    $this->phases->onEnteredState($phase, function() use ($phase) {
                        $listeners = &$this->phaseListeners[$phase->name];
                        \usort($listeners, function(PhaseHandler $a, PhaseHandler $b) {
                            return $a->ordering <=> $b->ordering;
                        });

                        //Kernel::debug('Bunch of {count} listeners for phase {phase}', ['phase' => $phase->name, 'count' => count($listeners)], $listeners);

                        /** @var PhaseHandler */
                        foreach ($listeners as $listener) {
                            $this->invoke($listener->method);
                        }
                    });
                }
                $this->phaseListeners[$phase->name][] = $attribute;
            }
        }
    }

    private function runPhases(): void {
        foreach ([
            Phase::System,
            Phase::Bundles,
            Phase::Services,
            Phase::Controllers,
            Phase::Integrate,
            Phase::Ready,
        ] as $nextPhase) {
            try {
                $perf = $this->instrumentation->benchmark('Phase '.$nextPhase->name);
                $this->phases->trigger($nextPhase);
                $perf();
            } catch (\Throwable $e) {
                throw $e;
            }
        }
    }

}