<?php
namespace Fubber\Kernel\Phases;

enum Phase {

    /**
     * This phase is never entered. It is active immediately when the Kernel object is
     * instantiated.
     * 
     * First the Mixins trait will find all methods of the Kernel object with the Fubber\Mixins\MixinConstructor
     * property. These methods are added to the Kernel object via traits or by extending the Kernel and will be
     * run in the order allowed by their dependencies.
     * 
     * Default mixins in the order they are run:
     * 
     *  1.  ExtensionsMixin
     * 
     *      $kernel->addExtension(KernelExtension $extension)
     *      $kernel->onExtensionAdded
     * 
     *      ExtensionsMixin provides the "extension" API for the kernel. An extension is an object which extends
     *      Fubber\Kernel\Extensions\KernelExtension class and is added to the kernel by the $kernel->addExtension()
     *      method. By default, Extensions have no particular capabilities. To give extensions capabilities, listen
     *      to the Kernel::$onExtensionAdded trigger.
     * 
     *  2.  ServicesMixin
     * 
     *      Services: Container, ContainerInterface, Kernel
     * 
     *      $kernel->container;
     *      $kernel->addService(string $id, mixed $factory, Scope $scope=null, array $aliases=[]): void;
     *      $kernel->invoke(Closure $callable, array &$namedArguments=[]): mixed;
     *      $kernel->public function construct(string $className, array &$namedArguments=[]): object;
     *      $kernel->get(string $id, ?string $namespace=null);
     *      $kernel->has(string $id);
     * 
     *      ServicesMixin adds the Kernel::$container property, where services can be registered for dependency injection.
     * 
     *  3.  EventLoopMixin
     * 
     *      $kernel->defer(Closure $callable, ...$args): void;
     *      $kernel->runEvents();
     * 
     *  4.  MicrocacheMixin
     * 
     *      Services: Fubber\Microcache\MicrocacheInterface
     * 
     *      $kernel->microcache;
     * 
     *  5.  FilesystemMixin
     * 
     *      Services: Fubber\Kernel\Filesystem
     *
     *      $kernel->filesystem
     * 
     *  6.  TemplateEngineMixin
     * 
     *      Services: Fubber\Kernel\TemplateEngine
     * 
     *      $kernel->template
     * 
     *  7.  InstrumentationMixin
     * 
     *      Services: Fubber\Kernel\Instrumentation
     * 
     *      $kernel->instrumentation;
     * 
     *  8.  EnvironmentMixin
     * 
     *      Services: Fubber\Kernel\Environment
     * 
     *      $kernel->env
     * 
     *  9.  ConfigMixin
     * 
     *      Services: Fubber\Kernel\Config\Config
     * 
     *      $kernel->config;
     *      $kernel->addConfigPath(string $path): void;
     *      $kernel->getConfigPaths(): array;
     *      $kernel->getConfigPath(string $filename): string;
     *      $kernel->loadConfig(string $filename, mixed $default=null): mixed;
     * 
     *  10. ViewsMixin
     * 
     *      Services: Fubber\Kernel\Views\Views
     * 
     *      $kernel->views
     *      $kernel->addViewPath(string $path);
     *      $kernel->getViewPaths(): array;
     *      $kernel->getViewPath(string $filename): string;
     * 
     * 11. PhasesMixin
     * 
     *      Hooks: Kernel.Phase.*.Done, Kernel.Phase.*.Starting
     *      $kernel->phases
     *      
     * 12.  RouterMixin
     * 
     *      Services: Fubber\Kernel\Router\Router
     *      $kernel->router
     * 
     * 13.  HttpMixin
     * 
     *      $kernel->http
     * 
     * 14.  LoadConfigMixin
     * 
     *      Loads `services.php` and `class_aliases.php` from config files.
     * 
     * 15.  BundlesMixin
     * 
     *       -  Scans for bundles in `fubber-framework/bundles/`, `app/modules/`,
     *      `   app/extra/` and `app/bundles/`.
     *       -  Adds their `views/`, `config/` and `src/` paths.
     *       -  Includes their `./init.php` file if it exists when the bundles
     *          phase is done.
     * 
     * 16.  KeyValueStoreMixin
     * 
     *      Services: Fubber\KeyValueStore\KeyValueStoreInterface
     *      $kernel->keyValueStore
     * 
     * 17.  CronMixin
     * 
     *      Services: Fubber\Kernel\Cron\Cron
     *      $kernel->cron
     * 
     * 18.  ControllersMixin
     * 
     *      Services: Fubber\Kernel\Controllers\Controllers
     *      $kernel->controllers
     *      $kernel->addController(string $className): void;
     */
    case Constructing;

    /**
     * Immediately after the Kernel has been constructed, this phase is responsible for
     * loading configuration from the config/ folders.
     */
    case System;

    /**
     * When configuration has been loaded, the "bundles" will be loaded. A "Bundle" is
     * a directory with views/, config/ and src/ folders and they co-exist with the main
     * application.
     */
    case Bundles;

    case Services;
    case Controllers;
    case Integrate;
    case Ready;
    case Terminated;
    case Failed;

}