<?php
namespace Fubber\Kernel\Phases;

use Attribute;
use Closure;
use Fubber\PHP\ClosureTool;
use JsonSerializable;

/**
 * Declare a handler function for one of the life cycle phases:
 * 
 * 1. Constructing
 * 2. System
 * 3. Modules
 * 4. Services
 * 5. Controllers
 * 6. Integrate
 * 7. Ready
 * 8. Terminated
 * 9. Failed
 * 
 * @package Fubber\Kernel\Mixins\Phases
 */
#[Attribute(Attribute::TARGET_METHOD)]
final class PhaseHandler implements JsonSerializable {

    public readonly Closure $method;

    public function __construct(public readonly Phase $phase, public readonly int $ordering=0) {}

    public function jsonSerialize(): mixed {
        return [ 'phase' => $this->phase->name, 'priority' => $this->ordering, 'method' => new ClosureTool($this->method) ];
    }

    public function setMethod(Closure $method): void {
        $this->method = $method;
    }

}