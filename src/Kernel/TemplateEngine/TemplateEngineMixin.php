<?php
namespace Fubber\Kernel\TemplateEngine;

use Fubber\Kernel\Microcache\MicrocacheMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Mixins\MixinConstructor;
use Fubber\Template\TplEngine;

trait TemplateEngineMixin {
    use ExtensionsMixin;
    use MicrocacheMixin;
    use ServicesMixin;

    public readonly TemplateEngine $template;

    #[MixinConstructor(__TRAIT__, ExtensionsMixin::class, MicrocacheMixin::class, ServicesMixin::class)]
    private function constructTemplateEngine(): void {
        $this->template = new TemplateEngine($this, $this->microcache->withNamespace(TemplateEngine::class));
        $this->addExtension($this->template);
        $this->container->addSingleton(TemplateEngine::class, $this->template);
    }
}