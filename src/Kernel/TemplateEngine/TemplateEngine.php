<?php
namespace Fubber\Kernel\TemplateEngine;

use ArrayAccess;
use Fubber\CodingErrorException;
use Fubber\ConfigErrorException;
use Fubber\Hooks\Filter;
use Fubber\InvalidArgumentException;
use Fubber\Kernel;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Microcache\MicrocacheInterface;
use Fubber\TemplateEngineInterface;
use Fubber\TemplateInterface;

/**
 * This is a special template engine implementation which allows adding multiple template
 * engines and the resolver will find the best template engine for each template file.
 * 
 * @package Fubber\Kernel
 */
final class TemplateEngine extends KernelExtension implements ArrayAccess, TemplateEngineInterface {

    protected readonly Kernel $kernel;
    protected readonly MicrocacheInterface $microcache;
    protected array $templateEngines = [];
    protected array $extensionOrder = [];

    public Filter $variablesFilter;

    public function __construct(Kernel $kernel, MicrocacheInterface $microcache) {
        $this->kernel = $kernel;
        $this->microcache = $microcache;
        $this->variablesFilter = new Filter("Filter for adding or modifying variables for templates");
    }

    public function getExtensionName(): string {
        return "Template Engine";
    }

    public function offsetExists(mixed $offset): bool {
        return !!($this->templateEngines[$offset] ?? false);
    }

    public function offsetGet(mixed $offset): ?TemplateEngineInterface {
        return $this->templateEngines[$offset] ?? null;
    }

    public function offsetSet(mixed $offset, mixed $value): void {
        if (!($value instanceof TemplateEngineInterface)) {
            throw new InvalidArgumentException("Expecting an object implementing `Fubber\TemplateEngineInterface`, got `".\get_debug_type($value)."`");
        }
        $this->addEngine($value);
    }

    public function offsetUnset(mixed $offset): void {
        throw new CodingErrorException("Can't remove template engines after they have been added");
    }

    public function addEngine(TemplateEngineInterface $templateEngine): void {
        foreach ($templateEngine->getExtensions() as $extension) {
            if (\array_key_exists($extension, $this->templateEngines)) {
                throw new CodingErrorException("Template engine for extension `$extension` is already installed");
            }
            $this->templateEngines[$extension] = $templateEngine;
        }
        $this->updateExtensionsOrdering();
    }

    public function render(string $view, array $vars, string $pathHint=null): TemplateInterface {
        try {
            /** @var string[] */
            $possibleFilePaths = [];

            /** @var ?TemplateEngineInterface */
            $engine = null;

            if ($pathHint !== null) {
                $possibleFilePaths[] = $pathHint;
                foreach ($this->extensionOrder as $extension) {
                    if (\str_ends_with($pathHint, $extension)) {
                        $engine = $this->templateEngines[$extension];
                        break;
                    }
                }
            }

            if ($engine === null) {

                [ $extension, $pathHint ] = $this->microcache->fetch($view, function() use ($view, &$possibleFilePaths) {
                    if (empty($this->templateEngines)) {
                        throw new ConfigErrorException("No template engines registered");
                    }
                    $filename = null;
                    foreach ($this->kernel->getViewPaths() as $path) {
                        foreach ($this->extensionOrder as $extension) {
                            $filename = $path . \DIRECTORY_SEPARATOR . $view . '.' . \ltrim($extension, '.');
                            $possibleFilePaths[] = $filename;
                            if (\is_file($filename)) {
                                break 2;
                            }
                        }
                        $filename = null;
                    }
                    if ($filename === null) {
                        throw new CodingErrorException("Could not find a template file or template engine to render `$view`");
                    } else {
                        return [ $extension, $filename ];
                    }
                }, 60);

                $engine = $this->templateEngines[$extension];
            }

            if ($engine === null) {
                throw new CodingErrorException("Unable to select a template engine to render `$view`");
            }

            $vars = $this->variablesFilter->filter($vars);

            return $engine->render($view, $vars, $pathHint);

        } catch (\Throwable $e) {
            throw Instrumentation::setExceptionInfo($e,
                extraData: [
                    'template' => $view,
                    'vars' => $vars,
                    'expectedFilePaths' => $possibleFilePaths,
                    'engine' => $engine ? $engine::class : 'N/A',
                ]
            );
        }
    }

    public function getExtensions(): array {
        return $this->extensionOrder;
    }

    protected function updateExtensionsOrdering(): void {
        $extensions = \array_keys($this->templateEngines);
        \usort($extensions, function($a, $b) {
            return -(\strlen($a) <=> \strlen($b));
        });
        $this->extensionOrder = $extensions;
    }

}