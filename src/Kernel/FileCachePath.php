<?php
namespace Fubber\Kernel;

use Fubber\InvalidArgumentException;

class FileCachePath {

    /**
     * The namespace relative to the root
     * 
     * @var string[]
     */
    protected array $namespace;

    /**
     * The absolute root directory where the namespaces get stored
     * 
     * @var string
     */
    protected readonly string $root;

    public function __construct(
        string $root,
        protected readonly DependencyInjector $di,
        protected readonly Filesystem $fileSystem
    ) {
        $realRoot = $fileSystem->realpath($root);
        if ($realRoot === false) {
            throw new InvalidArgumentException("Root path `$root` must exist");
        }
        $this->namespace = [];
    }

    protected function getMaxKeyLength(): int {
        return 100;
    }

    public function useNamespace(string|object $namespace): static {
        $this->namespace = $this->di->identifyNamespace($namespace, $this->namespace);
    }

    public function getPath(): string {
        return $this->fileSystem->buildPath($this->root, ...$this->namespace);
    }

    public function __toString() {
        return $this->getPath();
    }
}
