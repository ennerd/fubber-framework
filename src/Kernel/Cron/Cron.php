<?php
namespace Fubber\Kernel\Cron;

use Fubber\AccessDeniedException;
use Fubber\CodingErrorException;
use Fubber\Kernel;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Hooks\Event;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Router\Router;
use Fubber\Kernel\State;
use Fubber\Kernel\UnknownServiceException;
use Fubber\KeyValueStore\KeyValueStoreInterface;
use Fubber\Util\Url;
use InvalidArgumentException;

/**
 * Manages scheduled background jobs.
 * 
 * @package Fubber\Kernel
 */
final class Cron extends KernelExtension {

    /**
     * Triggered during the first night of every year
     * 
     * @var Event
     */
    public Event $everyYear;

    /**
     * Triggered during the first night of every month
     * 
     * @var Event
     */
    public Event $everyMonth;

    /**
     * Triggered during the first night of every week
     * 
     * @var Event
     */
    public Event $everyWeek;

    /**
     * Triggered every night
     * 
     * @var Event
     */
    public Event $everyNight;

    /**
     * Triggered every hour
     * 
     * @var Event
     */
    public Event $everyHour;

    /**
     * Triggered every minute
     * 
     * @var Event
     */
    public Event $everyMinute;

    /**
     * Triggered on every opportuniy
     * 
     * @var Event
     */
    public Event $everyRun;

    protected KeyValueStoreInterface $kv;

    protected Router $router;

    public function getExtensionName(): string {
        return 'CRON manager';
    }

    public function __construct(Router $router, KeyValueStoreInterface $keyValueStore, Kernel $kernel) {
        $this->router = $router;
        $this->kv = $keyValueStore;

        $this->everyYear = new Event('CRON yearly job event');
        $this->everyMonth = new Event('CRON monthly job event');
        $this->everyWeek = new Event('CRON weekly job event');
        $this->everyNight = new Event('CRON nightly job event');
        $this->everyHour = new Event('CRON hourly job event');
        $this->everyMinute = new Event('CRON every minute job event');
        $this->everyRun = new Event('CRON every invocation job event');

        $this->router->onCollectRoutes->listen(function() {
            $this->router->get('/fubber/cron/start', $this->start(...), self::class.'::start');
            $this->router->get('/fubber/cron/run', $this->run(...), self::class.'::run');    
            $this->router->get('/fubber/cron/', function() {
                return "Hei";
            });
        });
        if (\mt_rand(0, 200) === 0) {
            $kernel->phases->onEnteredState(Phase::Ready, function() {
                if ($this->getLastRunTime() < time() - 30) {
                    $this->start();
                }
            });
        }
    }

    /**
     * Start cron-jobs in the background
     */
    public function start(int $delay=0): bool {
        if ($this->isRunning()) {
            return false;
        }
        if ($this->getLastRunTime() > \time() - 3) {
            return false;
        }
        $delay = \max(0, min(60, $delay));
        $url = new Url($this->router->url(self::class.'::run'));

        if (\file_exists($php = \PHP_BINDIR . \DIRECTORY_SEPARATOR . 'php') || file_exists($php = \PHP_BINDIR . \DIRECTORY_SEPARATOR . 'php.exe')) {
            $cmd = 'echo '.escapeshellarg('<?php sleep('.\var_export($delay, true).'); file_get_contents('.\var_export($finalUrl = (string) $url->withQuery('expires='.(\time() + 30 + $delay))->quickSign(), true).');').' | '.$php.' > /dev/null &';
            \shell_exec($cmd);
        } elseif (\file_exists($wget = '/usr/bin/wget')) {
            $cmd = $wget . ' -bq -O /dev/null '.\escapeshellarg($url->withQuery('delay='.$delay.'&expires='.(\time() + 30 + $delay))->quickSign()).' > /dev/null';
            \shell_exec($cmd);
        } elseif (\file_exists($bash = '/bin/bash') && \file_exists($sleep = '/bin/sleep') && (\file_exists($curl = '/usr/bin/curl') || \file_exists($curl = '/usr/local/bin/curl'))) {
            // 'disown' is a bash builtin, and this should prevent zombie processes
            $cmd = "$sleep $delay && $curl -vs ".\escapeshellarg($url->withQuery('expires='.(time() + 30 + $delay))->quickSign());
            $cmd = 'exec '.$bash.' -c '.\escapeshellarg($cmd).' > /dev/null 2>&1 & disown';
            \shell_exec($cmd);
        } else {
            // fallback; run cron in our own namespace
            \file_get_contents($url->withQuery('delay='.$delay.'&expires=' . (time() + 30 + $delay))->quickSign(), false, \stream_context_create([
                'http' => [
                    'timeout' => 0.5,
                ]
            ]));
        }
        return true;
    }

    /**
     * Get the time stamp for the last cron run
     * 
     * @return null|int 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException  
     * @throws CodingErrorException 
     */
    public function getLastRunTime(): int {
        return $this->kv->get('last-run') ?? 0;
    }

    public function isRunning(): bool {
        $lastRun = $this->kv->get('running', $success);
        if (!$success) {
            return false;
        }
        if ($lastRun < time() - 3600) {
            // assuming failure if more than 1 hour since last run
            return false;
        }
        return true;
    }

    /**
     * Run all cron job handlers
     */
    public function run(State $state, int $delay=0): bool {
        $state->json = true;
        $request = $state->getRequest();
        $state->response->noCache();
        $url = new Url($request->getUri());
        $expires = $request->getQueryParams()['expires'] ?? null;
        if ($expires !== null && $expires < \time()) {
            throw new AccessDeniedException("URL signature has expired");
        }
        if (!$url->quickCheck()) {
            //throw new AccessDeniedException("Invalid signature");
        }
        if ($this->isRunning()) {
            //return false;
        }

        $now = \time();

        $lastRun = $this->kv->get('last-run');
        if ($lastRun > ($now - 30) + $delay) {
            // Don't run it if it last run less that 30 seconds ago
            return false;
        }

        $now = \time();
        $this->kv->set('last-run', $now);

        \register_shutdown_function(function() {
            try {
                $this->kv->delete('running');
            } catch (\Throwable $e) {}
        });
        $this->kv->set('running', $now);

        $this->everyRun->trigger();

        $minute = \gmdate('Y-m-d H:i', $now).':00';
        if ($this->kv->get('run-minute') !== $minute) {
            $this->kv->set('run-minute', $minute);
            $this->everyMinute->trigger();
            $this->kv->set('running', \time());
        }

        $hour = \gmdate('Y-m-d H', $now).':00:00';
        if ($this->kv->get('run-hour') !== $hour) {
            $this->kv->set('run-hour', $hour);
            $this->everyHour->trigger();
            $this->kv->set('running', \time());
        }

        if (\in_array(\date('H', $now), ['2', '3', '4'])) {
            // night-time

            $night = \gmdate('Y-m-d', $now) . ' 00:00:00';
            if ($this->kv->get('run-night') !== $night) {
                $this->kv->set('run-night', $night);
                $this->everyNight->trigger();
                $this->kv->set('running', \time());
            }

            $week = \gmdate('Y.W', $now);
            if ($this->kv->get('run-week') !== $week) {
                $this->kv->set('run-week', $week);
                $this->everyWeek->trigger();
                $this->kv->set('running', \time());
            }

            $month = \gmdate('Y-m', $now).'-01 00:00:00';
            if ($this->kv->get('run-month') !== $month) {
                $this->kv->set('run-month', $month);
                $this->everyMonth->trigger();
                $this->kv->set('running', \time());
            }

            $year = \gmdate('Y', $now).'-01-01 00:00:00';
            if ($this->kv->get('run-year') !== $year) {
                $this->kv->set('run-year', $year);
                $this->everyYear->trigger();
                $this->kv->set('running', \time());
            }
        }

        return true;
    }
}