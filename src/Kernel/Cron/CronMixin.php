<?php
namespace Fubber\Kernel\Cron;

use Fubber\Hooks;
use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\KeyValueStore\KeyValueStoreMixin;
use Fubber\Kernel\Router\RouterMixin;
use Fubber\Mixins\MixinConstructor;

trait CronMixin {
    use RouterMixin;
    use KeyValueStoreMixin;
    use ServicesMixin;

    public readonly Cron $cron;

    #[MixinConstructor(__TRAIT__, RouterMixin::class, KeyValueStoreMixin::class, ServicesMixin::class)]
    private function constructCron(): void {
        $this->cron = new Cron($this->router, $this->keyValueStore, $this);
        $this->container->addSingleton(Cron::class, $this->cron);
        $this->addExtension($this->cron);

        $this->cron->everyRun->listen(function() {
            $listeners = Hooks::getListeners('Kernel.Cron');
            foreach ($listeners as $listener) {
                if (!\is_callable($listener['callback'])) {
                    throw new \CodingErrorException("Listener `" . \json_encode($listener['callback']) . "` is not callable");
                }
                try {
                    $listener['callback']($this);
                } catch (\Throwable $e) {
                    $this->handleFatalException($e);
                }
            }
        });

    }
}