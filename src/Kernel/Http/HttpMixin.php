<?php
namespace Fubber\Kernel\Http;

use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Environment\EnvironmentMixin;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Kernel\Instrumentation\InstrumentationMixin;
use Fubber\Kernel\Router\RouterMixin;
use Fubber\Kernel\TemplateEngine\TemplateEngineMixin;
use Fubber\Mixins\MixinConstructor;

trait HttpMixin {
    use ServicesMixin;
    use RouterMixin;
    use InstrumentationMixin;
    use EnvironmentMixin;
    use ExtensionsMixin;
    use TemplateEngineMixin;

    public readonly Http $http;

    #[MixinConstructor(__TRAIT__, ServicesMixin::class, RouterMixin::class, InstrumentationMixin::class, EnvironmentMixin::class, TemplateEngineMixin::class)]
    private function constructHttp(): void {
        $this->http = new Http($this->container, $this->router, $this->instrumentation, $this->env, $this->template, $this);
        $this->addExtension($this->http);
    }
}