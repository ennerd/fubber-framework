<?php
namespace Fubber\Kernel\Http\RequestHandlers;

use Closure;
use Fubber\JsonView;
use Fubber\Kernel;
use Fubber\Kernel\Container\Container;
use JsonSerializable;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Creates a request handler from a Closure using dependency injection from the
 * ServerRequestInterface::getParameters() and any additional parameters provided
 * via the constructor.
 * 
 * @package Fubber\Kernel
 */
class ClosureRequestHandler implements RequestHandlerInterface {

    protected readonly Container $container;
    public readonly Closure $closure;
    public readonly array $extraParameters;
    public readonly array $extraObjects;

    public function __construct(Container $container, Closure $closure, array $extraParameters=[], array $extraObjects=[]) 
    {
        $this->container = $container;
        $this->closure = $closure;
        $this->extraParameters = $extraParameters;
        $this->extraObjects = $extraObjects;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $extraObjects = $this->extraObjects;
        foreach ($request->getAttributes() as $eo) {
            if (\is_object($eo)) {
                $extraObjects[] = $eo;
            }
        }

        try {
            $extraParams = $request->getAttributes();
            foreach ($this->extraParameters as $key => $value) {
                $extraParams[$key] = $value;
            }
            foreach ($request->getQueryParams() as $key => $value) {
                if (!\is_numeric($key) && !isset($extraParams[$key])) {
                    $extraParams[$key] = $value;
                }
            }
            $result = $this->container->invoke(
                $this->closure,
                $extraParams,
                $extraObjects
            );    
        } catch (\TypeError $e) {
            throw $e;
            echo "<pre>";
            echo $e->getMessage()."\n";
            $args = $request->getAttributes() + $this->extraParameters + $request->getQueryParams();
            var_dump($request->getQueryParams());die("FRODE");
            var_dump($extraObjects);
            die("TypeError");
        }

        if ($result instanceof JsonSerializable || \is_scalar($result) || \is_array($result) || $result === null) {
            return new JsonView($result);
        } else {
            return $result;    
        }        
    }
}