<?php
namespace Fubber\Kernel\Http\ExceptionHandlers;

use ArrayAccess;
use Closure;
use CodingErrorException;
use Fubber\Hooks\Event;
use Fubber\Hooks\Trigger;
use Fubber\Kernel;
use Fubber\Kernel\Http\Http;
use Fubber\Kernel\LogicException;
use Fubber\Kernel\Phases\Phase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

class ExceptionHandlers implements ArrayAccess {

    protected Http $http;
    protected array $exceptionHandlers = [];

    public readonly Event $onCollect;
    public readonly Trigger $onReady;

    protected bool $isCollecting;

    public function __construct(Kernel $kernel, Http $http) {
        $this->http = $http;
        $this->onCollect = new Event("Add exception handlers");
        $this->onReady = new Trigger("Exception handlers registered");
        $kernel->phases->onExitingState(Phase::Integrate, $this->collectExceptionHandlers(...));
    }

    public function getExtensionName(): string {
        return "ExceptionHandler collection";
    }

    protected function collectExceptionHandlers(): void {
        $this->isCollecting = true;
        $this->onCollect->trigger($this);
        $this->isCollecting = false;
        $this->onReady->trigger();
    }

    public function offsetExists(mixed $offset): bool
    {
        return \array_key_exists($offset, $this->exceptionHandlers);
    }

    /**
     * @param string $offset 
     * @return RequestHandlerInterface 
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->exceptionHandlers[$offset] ?? null;   
    }

    /**
     * @param string $offset 
     * @param RequestHandlerInterface $value 
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if (!$this->isCollecting) {
            throw new LogicException("Can only add exception handlers as a response to ExceptionHandlers::\$onCollect events.");
        }
        if (!($value instanceof RequestHandlerInterface) && !($value instanceof Closure)) {
            throw new CodingErrorException("Exception handlers must implement a RequestHandlerInterface or be a Closure");
        }
        $this->exceptionHandlers[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        throw new LogicException("To maintain consistency, ExceptionHandlers can't be removed when they have been added");
    }

    public function getHandlerFor(Throwable $exception): Closure|RequestHandlerInterface|null {
        $className = $exception::class;

        if (isset($this[$className])) {
            return $this[$className];
        }

        while ($className = \get_parent_class($className)) {
            if (isset($this[$className])) {
                return $this[$className];
            }
        }

        foreach (\class_implements($exception::class) as $interface) {
            if (isset($this[$interface])) {
                return $this[$interface];
            }
        }

        return null;
    }

    public function handle(ServerRequestInterface $request, \Throwable $exception): ResponseInterface {
        $handler = $this->getHandlerFor($exception);

        if ($handler !== null) {
            return $this->http->runController($request, $handler, extraObjects: [ $exception ]);
        } else {
            throw $exception;
        }

    }

}