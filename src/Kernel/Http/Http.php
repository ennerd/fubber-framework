<?php
namespace Fubber\Kernel\Http;

use Closure;
use Fubber\BodyParsingMiddleware\BodyParsingMiddleware;
use Fubber\Hooks\Event;
use Fubber\Kernel;
use Fubber\Kernel\Container\Container;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Environment\Environment;
use Fubber\Kernel\Http\ExceptionHandlers\ExceptionHandlers;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Kernel\Http\Middleware\ContentLengthMiddleware;
use Fubber\Kernel\Http\Middleware\DebugMiddleware;
use Fubber\Kernel\Http\Middleware\ExceptionHandlerMiddleware;
use Fubber\Kernel\Http\Middleware\StackedMiddleware;
use Fubber\Kernel\Http\RequestHandlers\ClosureRequestHandler;
use Fubber\Kernel\Router\Router;
use Fubber\Kernel\State;
use Fubber\Kernel\TemplateEngine\TemplateEngine;
use Fubber\LogicException;
use Fubber\NotFoundException;
use Fubber\OptimizeMiddleware\OptimizeMiddleware;
use Fubber\PlainView;
use Fubber\Template;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Debug\ExceptionHandler;
use Throwable;

/**
 * Encapsulates the ServerRequestInterface -> ResponseInterface request cycle for Fubber Framework
 * 
 * @package Fubber\Kernel
 */
final class Http extends KernelExtension implements RequestHandlerInterface {

    protected readonly Container $container;

    protected readonly Instrumentation $instrumentation;

    /**
     * Manage the middleware stack
     * 
     * @var StackedMiddleware
     */
    public readonly StackedMiddleware $middleware;

    /**
     * Request handler responsible for finding the correct
     * request handler based on the request path and method.
     * 
     * @var Router
     */
    public readonly Router $router;

    public readonly TemplateEngine $templateEngine;

    protected readonly Kernel $kernel;

    protected ?State $state = null;

    /**
     * Exception handlers 
     * 
     * @var ExceptionHandlers
     */
    public readonly ExceptionHandlers $exceptionHandlers;

    public function getExtensionName(): string {
        return 'HTTP module';
    }
    
    public function __construct(Container $container, Router $router, Instrumentation $instrumentation, Environment $env, TemplateEngine $templateEngine, Kernel $kernel) {
        $this->container = $container;
        $this->router = $router;
        $this->instrumentation = $instrumentation;
        $this->kernel = $kernel;
        $this->templateEngine = $templateEngine;
        $this->templateEngine->variablesFilter->listen($this->templateVariablesFilter(...));

        $container->addSingleton(static::class, $this);

        $perf = $this->instrumentation->benchmark('Making $kernel->http->middleware');
        $this->middleware = new StackedMiddleware();
        $container->addSingleton($this->middleware::class, $this->middleware);
        $perf();

        $perf = $this->instrumentation->benchmark('Making $kernel->http->exceptionHandlers');
        $this->exceptionHandlers = new ExceptionHandlers($kernel, $this);
        $container->addSingleton($this->exceptionHandlers::class, $this->exceptionHandlers);
        $perf();


        $middlewares = [
            BodyParsingMiddleware::class => new BodyParsingMiddleware(),
            ContentLengthMiddleware::class => new ContentLengthMiddleware(),
        ];
        if (\str_starts_with($env->uploadRoot, $env->baseRoot)) {
            $webPath = \substr($env->uploadRoot, \strlen($env->baseRoot)) . '/asset-cache';
            //$middlewares[OptimizeMiddleware::class] = new OptimizeMiddleware($env->baseRoot, $webPath);
        }

        foreach ($middlewares as $id => $instance) {
            $this->middleware->add($instance);
            $container->addSingleton($id, $instance);
        }

        if ($env->debug) {
            $debugMiddleware = new DebugMiddleware();
            $this->middleware->add($debugMiddleware);
            $container->addSingleton($debugMiddleware::class, $debugMiddleware);
        }
        $exceptionHandler = new ExceptionHandlerMiddleware($kernel);
        $container->addSingleton(ExceptionHandlerMiddleware::class, $exceptionHandler);

        $this->middleware->add($exceptionHandler);
    }

    public function serveFile(string $path, array $headers = []): ResponseInterface {
        if (!\file_exists($path)) {
            throw new NotFoundException("File `$path` not found");
        }
        $ext = \pathinfo($path, \PATHINFO_EXTENSION);
        switch ($ext) {
            case 'js':
                $headers['Content-Type'] = 'text/javascript';
                break;
            case 'css':
                $headers['Content-Type'] = 'text/css';
                break;
            case 'html':
                $headers['Content-Type'] = 'text/html; charset=utf-8';
                break;
            default:
                $headers['Content-Type'] = \mime_content_type($path);
                break;
        }
        $headers['Cache-Control'] = 'private,max-age=1';
        return new PlainView(\file_get_contents($path), $headers);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface {
        if ($this->state !== null) {
            throw new LogicException("Already handling a request, make sure State is switched when handling multiple concurrent requests");
        }

        try {
            $bench = $this->instrumentation->benchmark("Processing request " . $request->getRequestTarget());
            $this->state = new State($this->kernel, $request);
            $request = $request->withAttribute('request', $request);
            $request = $request->withAttribute('state', $this->state);
            $this->middleware->onServerRequestChanged->listen($onServerRequestChangedListener = $this->state->request->push(...));
            $response = $this->middleware->process(
                $request,
                $this->router
            );
            return $this->state->apply($response);
        } finally {
            $this->middleware->onServerRequestChanged->off($onServerRequestChangedListener);
            $this->state = null;
            $bench();
        }
    }

    /**
     * Returns the State object for the request that is currently being processed.
     * 
     * @return null|State 
     */
    public function getCurrentState(): ?State {
        return $this->state;
    }

    /**
     * Respond to a ServerRequest with a controller (either a RequestHandlerInterface or a Closure).
     * Uses dependency injection to provide the parameters requested.
     * 
     * @param ServerRequestInterface $request 
     * @param RequestHandlerInterface|Closure $closure 
     * @param array $paramValues 
     * @param array $extraObjects 
     * @return ResponseInterface 
     */
    public function runController(ServerRequestInterface $request, RequestHandlerInterface|Closure $closure, array $paramValues=[], array $extraObjects=[]): ResponseInterface
    {
        try {
            if ($closure instanceof RequestHandlerInterface) {
                return $closure->handle($request);
            } elseif ($closure instanceof Closure) {
                return (new ClosureRequestHandler($this->container, $closure, $paramValues, $extraObjects))->handle($request);
            } else {
                throw new LogicException("Unable to run this controller");
            }
        } catch (Throwable $e) {
            throw Instrumentation::setExceptionInfo(
                $e,
                request: $request,
                closure: $closure instanceof Closure ? $closure : null,
                extraData: [
                    'paramValues' => $paramValues,
                    'extraObjects' => $extraObjects,
                ]
            );
        }
    }

    protected function templateVariablesFilter(array $vars): array {
        if (!isset($vars['state'])) {
            $vars['state'] = $this->state;
        }
        return $vars;
    }
}