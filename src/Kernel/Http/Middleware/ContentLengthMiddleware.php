<?php
namespace Fubber\Kernel\Http\Middleware;

/*
 * Slim Framework (https://slimframework.com)
 *
 * @license https://github.com/slimphp/Slim/blob/4.x/LICENSE.md (MIT License)
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ContentLengthMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        if ($response->hasHeader("Content-Length")) {
            // Content-Length already present
            return $response;
        }

        if ($response->hasHeader("Transfer-Encoding")) {
            // Don't touch
            return $response;
        }

        // Add Content-Length header if not already added
        $size = $response->getBody()->getSize();
        if (null !== $size && !$response->hasHeader('Content-Length')) {
            $response = $response->withHeader('Content-Length', $size);
        }

        return $response;
    }
}