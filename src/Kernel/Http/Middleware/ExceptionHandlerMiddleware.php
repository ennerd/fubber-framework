<?php
namespace Fubber\Kernel\Http\Middleware;

use Fubber\IException;
use Fubber\Kernel;
use Fubber\ExceptionTemplate;
use Fubber\Psr\HttpMessage\Response;
use Fubber\PHP\ClosureTool;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Fubber\Psr\HttpMessage\Stream;
use Throwable;

/**
 * Middleware which handles exceptions thrown by the application.
 * 
 * @package Fubber\Kernel\Middleware
 */
class ExceptionHandlerMiddleware implements MiddlewareInterface {

    protected Kernel $kernel;
    private ?\Whoops\Run $whoops = null;
    private int $requestId = 0;
    protected array $active = [];

    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $requestId = $this->requestId++;
        $this->active[$requestId] = $request;
        try {
            \ob_start();
            $result = $handler->handle($request);
            $contents = \ob_get_contents();
            if ($contents !== '') {
                \error_log("STDOUT: ".$contents);
            }
            ob_end_clean();
            return $result;
        } catch (\Throwable $e) {
            $contents = \ob_get_contents();
            try {
                return $this->createExceptionResponse($request, $e, $contents);
            } catch (\Throwable $e2) {
                return $this->createExceptionResponse($request, $e2, $contents);
            }
        } finally {
            unset($this->active[$requestId]);
            \ob_end_clean();
        }
    }

    public function createExceptionResponse(ServerRequestInterface $request, Throwable $e, string $bufferedOutput): ResponseInterface {
        
        try {
            return $this->kernel->http->exceptionHandlers->handle($request, $e);
        } catch (\Throwable $error) {
            $e = $error;
        }
        if (\defined('DEBUG') && DEBUG) {
            $exceptionTemplate = new ExceptionTemplate($e);
            return new Response(
                Stream::cast($exceptionTemplate),
                [
                    'Content-Type' => 'text/html; charset=utf-8',
                    'Cache-Control' => 'no-store, no-cache, max-age=0, s-maxage=0',
                ],
                500,
                'Unhandled '.$e::class.' exception',
            );
        } else {
            /**
             * @todo Use a template here; perhaps create an `InternalResponse extends ResponseInterface` class which parses the template
             */
            return new Response(
                <<<HTML
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <title>Internal Server Error</title>
                        </head>
                        <body>
                            <h1>Error</h1><p>Unhandled Internal Server Error</p>
                            <p><em>Fubber Framework</em></p>
                        </body>
                    </html>
                    HTML,
                [
                    'Content-Type' => 'text/html; charset=utf-8',
                    'Cache-Control' => 'no-store, no-cache, max-age=0, s-maxage=0',
                ],
                500,
                'Internal Server Error',
            );
        }
    }

    public function __debugInfo()
    {
        return [
            'active' => $this->active,
        ];
    }
}