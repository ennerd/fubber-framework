<?php
namespace Fubber\Kernel\Http\Middleware;

use Fubber\Hooks\Event;
use Fubber\Kernel\Http\Middleware\StackedMiddleware\StackedMiddlewareRequestHandler;
use Fubber\LogicException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Manage the stack of MiddlewareInterfaces for Fubber Framework
 * 
 * @package Fubber\Kernel
 */
class StackedMiddleware implements MiddlewareInterface {

    /**
     * Since ServerRequestInterface objects are immutable, we try to detect if the server
     * request was changed into a new instance. This event allows you to subscribe to
     * such events.
     * 
     * @var Event
     */
    public readonly Event $onServerRequestChanged;

    /**
     * Middleware chain
     * 
     * @var MiddlewareInterface[]
     */
    protected array $middlewareChain = [];

    public function __construct()
    {
        $this->onServerRequestChanged = new Event("ServerRequestInterface object was mutated");
    }

    /**
     * @inheritDoc
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return (new StackedMiddlewareRequestHandler($this->middlewareChain, $handler, $this->onServerRequestChanged))->handle($request);
    }

    /**
     * Add middleware to the end of the chain of middleware handlers which process
     * each request.
     * 
     * @param MiddlewareInterface $middleware 
     */
    public function add(MiddlewareInterface $middleware): void
    {
        $this->middlewareChain[] = $middleware;
    }

    /**
     * Insert middleware at a particular offset or before a particular middleware instance or class
     * 
     * @param MiddlewareInterface|class-string|int $otherMiddleware Integer offset, class name or middleware instance to insert
     * @param MiddlewareInterface $newMiddleware 
     * @throws LogicException 
     */
    public function insert(MiddlewareInterface|string|int $otherMiddleware, MiddlewareInterface $newMiddleware): void
    {
        if (\is_int($otherMiddleware)) {
            if (!isset($this->middlewareChain[$otherMiddleware])) {
                throw new LogicException("No middleware at the given offset");
            }
            $newMiddlewareChain = [];
            for ($i = 0; $i < $otherMiddleware && isset($this->middlewareChain[$i]); $i++) {
                $newMiddlewareChain[] = $this->middlewareChain[$i];
            }
            $newMiddlewareChain[] = $newMiddleware;
            for ($i = $otherMiddleware; isset($this->middlewareChain[$i]); $i++) {
                $newMiddlewareChain[] = $this->middlewareChain[$i];
            }
            $this->middlewareChain = $newMiddlewareChain;
        } elseif (\is_string($otherMiddleware)) {
            foreach ($this->middlewareChain as $i => $existingMiddleware) {
                if ($otherMiddleware === \get_class($existingMiddleware)) {
                    $this->insert($i, $newMiddleware);
                    return;
                }
            }
            throw new LogicException("No middleware having the class '$otherMiddleware' was found in the middleware chain");
        } else {
            foreach ($this->middlewareChain as $i => $existingMiddleware) {
                if ($otherMiddleware === $existingMiddleware) {
                    $this->insert($i, $newMiddleware);
                    return;
                }
            }
            throw new LogicException("The middleware instance provided does not exist in the chain");
        }
    }

}