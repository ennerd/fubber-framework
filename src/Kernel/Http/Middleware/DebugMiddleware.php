<?php
namespace Fubber\Kernel\Http\Middleware;

use Fubber\Kernel;
use Fubber\Kernel\SimpleTemplate\SimpleTemplate;
use Fubber\Psr\HttpMessage\Stream;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DebugMiddleware implements MiddlewareInterface {

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $time = \hrtime(true);
        $response = $handler->handle($request);
        $response = $this->addLog($response, 'Total handling time: '.((\hrtime(true) - $time) / 1000000) . ' ms');
        $response = $this->addLog($response, 'Peak memory usage: '.(\number_format(\memory_get_peak_usage() / (1024 * 1024), 1)).' MB');
        $response = $this->addLog($response, 'Included file count: '.\count(\get_included_files()));
        //$response = $this->processRedirects($request, $response);


        //$response = $response->withAddedHeader('x-debug', 'time='.(\hrtime(true) - $t).' peak-mem='.);

        return $response;
    }

    protected function addLog(ResponseInterface $response, string $message): ResponseInterface {
        Kernel::debug($message);
        return $response->withAddedHeader('X-DebugMiddleware', $message);
    }

    protected function processRedirects(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        if (\str_starts_with((string) $response->getStatusCode(), '3')) {
            $originalHeaders = \json_encode($response->getHeaders(), \JSON_PRETTY_PRINT);
            return $response
                ->withStatus(200)
                ->withoutHeader('Location')
                ->withoutHeader('Content-Length')
                ->withHeader('Cache-Control', 'no-store')
                ->withBody(
                    SimpleTemplate::parse('middleware/debug/redirect', [
                        'title' => "fun stuff",
                        'statusCode' => $response->getStatusCode(),
                        'reasonPhrase' => $response->getReasonPhrase(),
                        'targetUri' => $response->getHeaderLine('Location'),
                        'responseHeaders' => $response->getHeaders(),
                        'requestHeaders' => $request->getHeaders(),
                    ])
                );
        } else {
            return $response;
        }
        

    }
}
/*
Stream::cast(<<<HTML
                    <h1>DebugMiddleware</h1>
                    <p>DEBUG is ON</p>
                    <h2>{$response->getStatusCode()} {$response->getReasonPhrase()}</h2>
                    <p>Redirecting to <a href="{$response->getHeaderLine('Location')}">{$response->getHeaderLine('Location')}</a></p>
                    <h2>Previous response headers</h2>
                    <pre>{$originalHeaders}</pre>
                    HTML)
*/