<?php
namespace Fubber\Kernel\Http\Middleware;

use Fubber\CodingErrorException;
use Fubber\Kernel\State\Cacheability;
use Fubber\LogicException;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Middleware adds a 'cacheability' property containing a `Cacheability` instance
 * which can be used to declare the cacheability of a response. 
 * 
 * @package State
 */
class CacheabilityMiddleware implements MiddlewareInterface {

    public readonly bool $restrictSetCookie;

    /**
     * @param bool $restrictSetCookie Set to `false` if middleware should allow caching of responses with `Set-Cookie` headers
     */
    public function __construct(
        bool $restrictSetCookie=true,
    ) {
        $this->restrictSetCookie = $restrictSetCookie;
    }

    /**
     * @inheritDoc
     * @param ServerRequestInterface $request 
     * @param RequestHandlerInterface $handler 
     * @return ResponseInterface 
     * @throws CodingErrorException 
     * @throws LogicException 
     * @throws InvalidArgumentException 
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $cacheability = new Cacheability();
        $request = $request->withAttribute('cacheability', $cacheability);
        $response = $handler->handle($request);

        if ($this->restrictSetCookie && !empty($response->getHeader('Set-Cookie'))) {
            /**
             * Safety measure: don't allow public caching of Set-Cookie headers
             */
            $cacheability->privateCache();
        }

        foreach ($cacheability->getHeaders() as $name => $values) {
            $response = $response->withHeader($name, $values);
        }

        return $response;
    }
}