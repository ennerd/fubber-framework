<?php
namespace Fubber\Kernel\Http\Middleware\StackedMiddleware;

use Fubber\Hooks\Event;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use WeakMap;

class StackedMiddlewareRequestHandler implements RequestHandlerInterface {

    /**
     * The chain of middlewares to run
     * 
     * @var MiddlewareInterface[]
     */
    private array $chain;

    /**
     * The next middleware to run in the chain
     * 
     * @var int
     */
    private int $offset = 0;

    /**
     * The final request handler of the chain
     * 
     * @var RequestHandlerInterface
     */
    private RequestHandlerInterface $handler;

    /**
     * Event emitter used whenever a new instance of the original ServerRequestInterface object
     * is detected.
     */
    private Event $onServerRequestChanged;

    private ServerRequestInterface $request;

    public function __construct(array $chain, RequestHandlerInterface $handler, Event $onServerRequestChanged) {
        $this->chain = $chain;
        $this->handler = $handler;
        $this->onServerRequestChanged = $onServerRequestChanged;
        $this->seenRequests = new WeakMap();
    }

    /**
     * This handler is invoked by every middleware in the chain to move up the middleware stack
     * 
     * @param ServerRequestInterface $request 
     * @return ResponseInterface 
     * @throws Throwable 
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($this->offset === 0) {
            // We're starting the middleware stack
            $this->request = $request;
        } elseif ($request !== $this->request) {
            // We have a new ServerRequestInterface object
            $this->onServerRequestChanged->trigger($request, $this->request);
            $this->request = $request;
        }

        if (isset($this->chain[1 + $this->offset])) {
            // there are more middlewares after this
            return $this->chain[$this->offset++]->process($request, $this);
        } else {
            // this is the last middleware, so we provide the final handler
            return $this->chain[$this->offset++]->process($request, $this->handler);
        }
    }

}