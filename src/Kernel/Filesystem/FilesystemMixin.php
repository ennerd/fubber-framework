<?php
namespace Fubber\Kernel\Filesystem;

use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Microcache\MicrocacheMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Mixins\MixinConstructor;

trait FilesystemMixin {
    use MicrocacheMixin;
    use ServicesMixin;

    public readonly Filesystem $filesystem;

    #[MixinConstructor(__TRAIT__, MicrocacheMixin::class, ServicesMixin::class)]
    private function constructFilesystem(): void {
        $this->container->addSingleton(Filesystem::class, $this->filesystem = new Filesystem($this->microcache->withNamespace(Filesystem::class)));
        $this->addExtension($this->filesystem);
    }
}