<?php
namespace Fubber\Kernel\Filesystem;

use FilesystemIterator;
use Fubber\InternalErrorException;
use Fubber\InvalidArgumentException;
use Fubber\Kernel;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\LogicException;
use Fubber\Microcache\MicrocacheInterface;
use RecursiveDirectoryIterator;
use Throwable;
use Transliterator;

class Filesystem extends KernelExtension {

    /**
     * Sanitizes filename components so that the file names are valid UTF-8 on Windows,
     * Linux and Mac operating systems. These filenames can safely be urlencoded and used
     * in URLs with a standard web server implementation.
     * 
     * This sanitizing mode is lossy, the original filename is lost - but the filename will
     * look OK in a directory listing.
     * 
     * `一個好的文件名.pdf` becomes `
     */
    public const SANITIZE_DEFAULT = 1;

    /**
     * Sanitizes filename components into ASCII characters which is portable to most
     * operating systems.
     */
    public const SANITIZE_LATIN = 2;

    /**
     * Sanitizes filename components into a subset of ASCII characters according to the
     * "POSIX portable filename" with a couple of extensions.
     * 
     * @see https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_282
     * 
     * This sanitizing mode is very lossy for internationalized filenames; 一個好的文件名
     */
    public const SANITIZE_POSIX = 3;

    /**
     * Matches all forbidden characters and character sequences across all systems:
     * 
     * - Control characters \x00-\x19 (all forbidden in Windows, \x00 forbidden in Linux)
     * - Printable characters <>:"/|\?* (all forbidden in Windows, some forbidden in Linux)
     * - Trailing . is forbidden in Windows
     * - Leading . makes hidden files in Linux
     * - Trailing whitespace is forbidden in Windows
     * - Leading whitespace is annoying in any platform
     * - Filenames CON, PRN, AUX, NUL, COM[1-9], LPT[1-9] are forbidden in windows (even with extensions)
     * - Filename ~ is annoying, since ~ as a path component in Linux is a reference to $HOME
     * - Filenames . and .. are implicitly forbidden because they have a trailing .
     */
    const REGEX_FORBIDDEN_SEQUENCES = '/[\x00-\x19<>:"\/|\\\\?*]+|\.+$|^\.+|\s+$|^\s+/mu';

    const REGEX_RESERVED_FILENAMES = '/^(CON|PRN|AUX|NUL|COM[1-9]|LPT[1-9])(\..+)?$|^\.\.?$|^~$/mui';

    /**
     * Matches characters that are not POSIX portable (and trailing dots)
     */
    const REGEX_NONPOSIX_SEQUENCES = '/[^a-zA-Z0-9._\-]+|\.+$/u';

    protected string $prefix;
    protected readonly bool $restrictedToPrefix;
    protected ?Transliterator $transliterator;

    public function getExtensionName(): string {
        return 'Cached filesystem operations';
    }

    /**
     * 
     * @param MicrocacheInterface $microcache 
     * @param int $statCacheTime How long to cache file metadata
     * @param int $dirCacheTime How long to cache directory operations (such as glob())
     * @param int $contentCacheTime How long to cache file contents
     * @param string|null $prefix An optional path prefix to use for all non-absolute file operations
     */
    public function __construct(
        protected readonly MicrocacheInterface $microcache,
        protected readonly int $statCacheTime=15,
        protected readonly int $dirCacheTime=15,
        protected readonly int $contentCacheTime=120,
        string $prefix = null,
        bool $restrictedToPrefix = false
    ) {
        if ($prefix === null) {
            $prefix = Kernel::getComposerPath();
        }
        $this->prefix = \strtr(\realpath($prefix), ['\\' => '/']);
        $this->restrictedToPrefix = $restrictedToPrefix;
        // PHP variables [a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]
        // File system [^\x21\x24\x25\2b\x2d\2e\x30-\x39\x40-\x5b\x5d\x5f\x61-\x7a\x7e-\xff]
        // [^\x21\x24\x25\2b\x2d\2e\x30-\x39\x40-\x5b\x5d\x5f\x61-\x7a\x7e-\xff] Remove; [:Nonspacing Mark:] Remove; 
        // Any-ASCII; [:Private Use:] Remove; [:Control:] Remove; [:Format:] Remove; [:Nonspacing Mark:] Remove; [:Punctuation:] remove
        // Good so far: Any-Latin; Lower; NFKD; [:Control:] Remove; [:Nonspacing Mark:] Remove
        // [^\x20-\xff] Remove
        $this->transliterator = \class_exists(Transliterator::class) ? Transliterator::create('Any-Latin; Any-Publishing; NFKD; [:Control:] Remove; Lower') : null;
        if ($this->transliterator === null) {
            throw new InternalErrorException("Error creating transliterator");
        }
    }

    public function traverse(string|array $path): iterable {
        $path = $this->buildPath($path);
        foreach ($this->glob($path.'/*', \GLOB_NOSORT) as $sub) {
            $bn = \basename($sub);
            if ($bn == '..' || $bn == '.') {
                continue;
            }
            if ($this->is_dir($sub)) {
                yield from $this->traverse($sub);
            }
            yield $sub;
        }
    }

    public function rmdir(string|array $path): bool {
        $path = $this->buildPath($path);
        if (!$this->is_dir($path)) {
            throw new LogicException("Path `$path` is not a directory");
        }
        foreach ($this->traverse($path) as $file) {
            if ($this->is_file($file)) {
                $this->unlink($file);
            } else {
                \rmdir($file);
            }
        }
        \rmdir($path);
        return \is_dir($path);
    }

    public function unlink(string|array ...$path): bool {
        return \unlink($this->buildPath($path));
    }

    public function withPathPrefix(string|array $path, bool $restricted=null): static {

        $path = $this->buildPath($path);

        return new static(
            $this->microcache,
            $this->statCacheTime,
            $this->dirCacheTime,
            $this->contentCacheTime,
            $this->buildPath($path),
            $restricted || $this->restrictedToPrefix
        );
    }

    public function relativePath(string|array $startPath, string|array ...$components): string {
        $startPath = $this->buildPath($startPath);
        $path = $this->buildPath($components);
        if (\str_starts_with($path, $startPath)) {
            if ($path === $startPath) {
                return '';
            }
            $l = \strlen($startPath);
            if ($path[$l] === '/') {
                $relativePath = \substr($path, $l+1);
                return $relativePath;
            }
            die("almost seemed right");
            // path starts wrong
        }
        die("STARTS WRONG");
    }

    /**
     * Normalizes a path provided in terms of strings and arrays recursively, while
     * resolving relative path components like .. and .
     * 
     * @param (string|array)[] $components 
     * @return string 
     * @throws InvalidArgumentException 
     */
    public function buildPath(string|array ...$components): string {

        $flatten = function(array $components) use (&$flatten): string {
            $parts = [];
            foreach ($components as $component) {
                if (\is_array($component)) {
                    $parts[] = $flatten($component);
                } else {
                    $parts[] = $component;
                }
            }
            return \implode("/", $parts);
        };
        $result = $flatten($components);
        if ($result === '') {
            return $this->prefix;
        } else {
            $result = \str_replace('\\', '/', $result);
            if ($result[0] !== '/') {
                $result = $this->prefix . '/' . $result;
            }
        }

        $prev = $result;
        while ($prev !== ($result = \preg_replace('/[\\\\\/]+(?=[\\\\\/])|[\\\\\/][^\\\\\/]+\/\.\.(?=[\\\\\/]|$)|[\\\\\/]\.(?=[\\\\\/]|$)|[\\\\\/]$/xm', '', $result))) {
            $prev = $result;
        }

        if ($this->restrictedToPrefix && !\str_starts_with($result, $this->prefix)) {
            throw new InvalidArgumentException('Path is restricted to prefix `'.$result[0].'`');
        }

        return $result;
    }

    public function dirname(string|array $path): string {

        $result = $this->buildPath($path, '..');
        die("fs dirname from $path to $result");
    }

    /**
     * Reads entire file into a string
     * 
     * @param string $path 
     * @param bool $use_include_path 
     * @param mixed $context 
     * @param int $offset 
     * @param null|int $length 
     * @return string|false 
     * @throws Throwable 
     */
    public function file_get_contents(string|array $path, bool $use_include_path=false, mixed $context=null, int $offset=0, ?int $length=null): string|false {
        $path = $this->buildPath($path);
        if ($use_include_path !== false || $context !== null || $this->filesize($path) > 128 * 1024) {
            return \file_get_contents($path, $use_include_path, $context, $offset, $length);
        }
        $mTime = $this->filemtime($path);
        $filesize = $this->filesize($path);
        $contents = $this->microcache->fetch('file_get_contents:' . $path . ':' . $mTime . ':' . $filesize, function() use ($path) {
            return \file_get_contents($path);
        }, $this->contentCacheTime);

        if ($offset === 0 && $length === null) {
            return $contents;
        }

        return \substr($contents, $offset, $length);
    }

    /**
     * Gives information about a file
     * 
     * @param string $path 
     * @return array|false 
     * @throws Throwable 
     */
    public function stat(string|array $path): array|false {
        $path = $this->buildPath($path);
        return $this->microcache->fetch('stat:' . $path, function() use ($path) {
            return \stat($path);
        }, $this->statCacheTime);
    }

    /**
     * Gets file group
     * 
     * @param string $filename 
     * @return int|false 
     * @throws Throwable 
     */
    public function filegroup(string|array $filename): int|false {
        $stat = $this->stat($filename);
        return $stat['gid'] ?? false;
    }

    /**
     * Gets file inode
     * 
     * @param string $filename 
     * @return int|false 
     * @throws Throwable 
     */
    public function fileinode(string|array $filename): int|false {
        return $this->stat($filename)['ino'] ?? false;
    }

    /**
     * Gets file owner
     * 
     * @param string $filename 
     * @return int|false 
     * @throws Throwable 
     */
    public function fileowner(string|array $filename): int|false {
        return $this->stat($filename)['uid'] ?? false;
    }

    /**
     * Gives information about a file or symbolic link
     * 
     * @param string $path 
     * @return array|false 
     * @throws Throwable 
     */
    public function lstat(string|array $path): array|false {
        $path = $this->buildPath($path);
        return $this->microcache->fetch('lstat:' . $path, function() use ($path) {
            return \stat($path);
        }, $this->statCacheTime);
    }

    /**
     * Gets file modification time
     * 
     * @param string $path 
     * @return int|false 
     * @throws Throwable 
     */
    public function filemtime(string|array $path): int|false {
        $path = $this->buildPath($path);
        $stat = $this->stat($path);
        if ($stat === false) {
            return false;
        }
        return $stat['mtime'] ?? false;
    }

    /**
     * Gets file creation time
     * 
     * @param string $path 
     * @return int|false 
     * @throws Throwable 
     */
    public function filectime(string|array $path): int|false {
        return $this->stat($path)['ctime'] ?? false;
    }

    /**
     * Gets last access time of a file
     * 
     * @param string $path 
     * @param int|null $maxCacheTime If a shorter cache life time is required, provide an alternative time to live here.
     * @return int|false 
     * @throws Throwable 
     */
    public function fileatime(string|array $path, int $maxCacheTime=null): int|false {
        $path = $this->buildPath($path);
        if ($maxCacheTime !== null) {
            return $this->microcache->fetch('fileatime:' . $path . ':' . $maxCacheTime, function() use ($path) {
                return \fileatime($path);
            }, $maxCacheTime);
        }
        $stat = $this->stat($path);
        if ($stat === false) {
            return false;
        }
        return $stat['atime'] ?? false;
    }

    /**
     * Get the size of a file
     * 
     * @param string $path 
     * @return int|false 
     * @throws Throwable 
     */
    public function filesize(string|array $path): int|false {
        return $this->stat($path)['size'] ?? false;
    }

    /**
     * Find pathnames matching a pattern
     * 
     * @param string $pattern The pattern. No tilde expansion or parameter substitution is done.
     * @param \GLOB_* $flags 
     * @return array|false 
     * @throws Throwable 
     */
    public function glob(string|array $pattern, int $flags=0): array|false {
        $pattern = $this->buildPath($pattern);
        return $this->microcache->fetch('glob:' . $pattern . ':' . $flags, function() use ($pattern, $flags) {
            return \glob($pattern, $flags);
        }, $this->dirCacheTime);
    }

    /**
     * Checks whether a file or directory exists
     * 
     * @param string $path 
     * @return bool 
     * @throws Throwable 
     */
    public function file_exists(string|array $path): bool {
        $path = $this->buildPath($path);
        return $this->microcache->fetch('file_exists:' . $path, function() use ($path) {
            return \file_exists($path);
        }, $this->statCacheTime);
    }

    /**
     * Tells whether the filename is a directory
     * 
     * @param string $path 
     * @return bool 
     * @throws Throwable 
     */
    public function is_dir(string|array $path): bool {
        $path = $this->buildPath($path);
        return $this->microcache->fetch('is_dir:' . $path, function() use ($path) {
            return \is_dir($path);
        }, $this->statCacheTime);
    }

    /**
     * Tells whether the filename is a regular file
     * 
     * @param string $path 
     * @return bool 
     * @throws Throwable 
     */
    public function is_file(string|array $path): bool {
        $path = $this->buildPath($path);
        return $this->microcache->fetch('is_file:' . $path, function() use ($path) {
            return \is_file($path);
        }, $this->statCacheTime);
    }

    /**
     * Returns canonicalized absolute pathname
     * 
     * @param string $path 
     * @return string|false 
     * @throws Throwable 
     */
    public function realpath(string|array $path): string|false {
        $path = $this->buildPath($path);
        return $this->microcache->fetch('realpath:' . $path, function() use ($path) {
            return \realpath($path);
        }, $this->statCacheTime);
    }

    /**
     * Filters a filename to make it usable on Windows, Linux and Mac, while allowing unicode.
     * 
     * @param string $filename 
     * @param self::SANITIZE_* $mode Sanitize mode for path components {@see self::SANITIZE_*}
     * @return string 
     */
    /**
     * 
     * @param string $filename The filename to sanitize
     * @param self::SANITIZE_* $mode How clean filenames are required?
     * @param string|null $whitespace String to replace whitespace sequences with. If `null` the value depends on context and mode.
     * @param string|null $droppedCharacter String to replace dropped character sequences with.
     * @return string 
     */
    public function sanitizeFilename(string $filename, int $mode=self::SANITIZE_DEFAULT, string $whitespace=' ', string $droppedCharacter='_'): string {
        static $pass = 0, $transliterators = [];
        if ($transliterators === []) {
            $transliterators = [
                self::SANITIZE_DEFAULT => Transliterator::create('Any-Publishing; NFKD'),
                self::SANITIZE_LATIN => Transliterator::create('Any-Publishing; Any-Latin; NFKD'),
                self::SANITIZE_POSIX => Transliterator::create('Any-Publishing; Any-Latin; de-ASCII; NFKD'),
            ];
        }
        ++$pass;
        try {
            // Fix `$droppedCharacter` parameter
            if (trim($droppedCharacter) === '') {
                $droppedCharacter = '_';
            }
            
            // We dislike leading and trailing whitespace, even if some versions of it is allowed
            $result = \trim($filename);

            // Repeating whitespace and leading whitespace is replaced with `$whitespace`
            if ($whitespace === null) {
                $result = \preg_replace('/\s+/u', ' ', $result);
            } else {
                $result = \preg_replace('/\s+/u', $whitespace, $result);
            }

            $result = $transliterators[$mode]->transliterate($result);

            if ($mode = self::SANITIZE_POSIX) {
                $result = \preg_replace_callback(self::REGEX_NONPOSIX_SEQUENCES, function(array $matches) use ($whitespace, $droppedCharacter) {
                    if (\trim($matches[0]) === '') {
                        // This is whitespace
                        return $whitespace;
                    }
                    if (\trim($matches[0], '.') === '') {
                        return \str_repeat($droppedCharacter, \mb_strlen($matches[0]));
                    }
                    return $droppedCharacter;
                }, $result);

                // Final check with defaults in case of bad $whitespace or $droppedCharacter settings
                $result = \preg_replace(self::REGEX_NONPOSIX_SEQUENCES, '_', $result);
            }

            /**
             * Some filenames are reserved, so we'll prefix them with '_' to maintain portability.
             */
            $result = \preg_replace_callback(self::REGEX_RESERVED_FILENAMES, function(array $matches) use ($droppedCharacter): string {
                if (\trim($matches[0], '.') === '') {
                    return \str_repeat($droppedCharacter, \mb_strlen($matches[0]));
                }

                return '_'.$matches[0];
            }, $result);

            /**
             * Remove any characters that are forbidden
             */
            $result = \preg_replace(self::REGEX_FORBIDDEN_SEQUENCES, '', $result);

            if ($pass === 1) {
                // Run one additional pass
                $result2 = $this->sanitizeFilename($result, $mode, $whitespace, $droppedCharacter);
                if ($result2 !== $result) {
                    $result3 = $this->sanitizeFilename($result2, $mode, $whitespace, $droppedCharacter);
                    if ($result2 !== $result3) {
                        throw new InvalidArgumentException("Problem sanitizing the filename with the current `\$whitespace` and `$droppedCharacter` settings.");
                    }
                }
                return $result2;
            }
            return $result;
        } finally {
            --$pass;
        }
    }

    /**
     * Checks if a path is an absolute path
     * 
     * @param string $path 
     * @return bool 
     */
    public static function isAbsolutePath(string $path): bool {
        if ($path === '') {
            throw new InvalidArgumentException("Empty path `` provided");
        }
        if ($path[0] === '\\' || $path[0] === '/') {
            // absolute path on Linux and Windows (on the current volume character, e.g. C: or D:)
            return true;
        }
        if ($path[1] ?? null === ':') {
            // absolute path on Windows
            return true;
        }
        return false;
    }

    public static function removeTrailingPathSeparator(string $path): string {
        return \rtrim($path, '\\/');
    }
}