<?php
namespace Fubber\Kernel\EventLoop;

use Closure;
use Fubber\Kernel;
use Fubber\Kernel\Debug\Describe;
use Fubber\Kernel\Instrumentation\Instrumentation;
use SplMinHeap;

class PhpEventLoop implements EventLoopInterface {

    protected int $deferredStart = 0;
    protected int $deferredEnd = 0;
    protected array $deferredFuncs = [];
    protected array $deferredArgs = [];

    protected int $microtaskStart = 0;
    protected int $microtaskEnd = 0;
    protected array $microtaskFuncs = [];
    protected array $microtaskArgs = [];

    protected array $readStreams = [];
    protected array $readListeners = [];

    protected array $writeStreams = [];
    protected array $writeListeners = [];

    protected SplMinHeap $timers;

    public function __construct() {
        $this->timers = new class() extends SplMinHeap {
            /**
             * @param Timer $a 
             * @param Timer $b 
             */
            protected function compare(mixed $value1, mixed $value2): int {
                return $value1->timestamp <=> $value2->timestamp;
            }
            public function top(): Timer {
                /** @var Timer */
                $result = parent::top();
                return $result;
            }
            public function extract(): Timer {
                /** @var Timer */
                $result = parent::extract();
                return $result;
            }
        };
    }

    public function defer(Closure $callable, mixed ...$args): void {
        $this->deferredFuncs[$this->deferredEnd] = $callable;
        $this->deferredArgs[$this->deferredEnd] = $args;
        ++$this->deferredEnd;
    }

    public function queueMicrotask(Closure $callable, mixed ...$args): void {
        $this->microtaskFuncs[$this->microtaskEnd] = $callable;
        $this->microtaskArgs[$this->microtaskEnd] = $args;
        ++$this->microtaskEnd;
    }

    public function readable(mixed $resource, Closure $callable): void {
        $id = (int) $resource;
        $this->readStreams[$id] = $resource;
        $this->readListeners[$id][] = $callable;
    }

    public function writable(mixed $resource, Closure $callable): void {
        $id = (int) $resource;
        $this->writeStreams[$id] = $resource;
        $this->writeListeners[$id] = $callable;
    }

    /**
     * @param float $seconds Number of seconds to wait before running the handler function
     * @param Closure $handler The handler function
     */
    public function setTimeout(float $seconds, Closure $handler): void {
        $time = \hrtime(true) / 1000000000 + $seconds;
        $this->timers->insert(new Timer($time, $handler));
    }

    /**
     * Run any pending events. If no events are pending, sleep for up to
     * `$maxSleep` seconds.
     * 
     * @param float $maxSleep The maximum number of seconds to sleep.
     */
    public function runEvents(float $maxSleep=0): void {

        if ($maxSleep > 0) {
            $maxSleep = \min($this->getMaxSleepTime(), $maxSleep);
        }

        if (count($this->writeStreams) > 0 || count($this->readStreams) > 0) {
            $reads = \array_values($this->readStreams);
            $writes = \array_values($this->writeStreams);
            $excepts = [];
            $result = \stream_select($reads, $writes, $excepts, (int) $maxSleep, (int) (($maxSleep - (int) $maxSleep) * 1000000));
            if (\is_int($result) && $result > 0) {
                foreach ($reads as $readableStream) {
                    $id = (int) $readableStream;
                    foreach ($this->readListeners as $readListener) {
                        $this->queueMicrotask($readListener, $readableStream);
                    }
                    unset($this->readStreams[$id], $this->readListeners[$id]);
                }
                foreach ($writes as $writableStream) {
                    $id = (int) $writableStream;
                    foreach ($this->writeListeners as $writeListener) {
                        $this->queueMicrotask($writeListener, $writableStream);
                    }
                    unset($this->writeStreams[$id], $this->writeListeners[$id]);
                }
            }
        } elseif ($maxSleep > 0) {
            \usleep($maxSleep * 1_000_000);
        }

        $deferredEnd = $this->deferredEnd;
        do {
            while ($this->microtaskStart < $this->microtaskEnd) {
                $func = $this->microtaskFuncs[$this->microtaskStart];
                $args = $this->microtaskArgs[$this->microtaskStart];
                unset($this->microtaskFuncs[$this->microtaskStart], $this->microtaskArgs[$this->microtaskStart]);
                ++$this->microtaskStart;
                $this->invoke($func, ...$args);
            }

            if ($this->deferredStart < $deferredEnd) {
                $func = $this->deferredFuncs[$this->deferredStart];
                $args = $this->deferredArgs[$this->deferredStart];
                unset($this->deferredFuncs[$this->deferredStart], $this->deferredArgs[$this->deferredStart]);
                ++$this->deferredStart;
                Kernel::debug("deferring " . Describe::auto($func));
                $this->invoke($func, ...$args);
            }
        } while ($this->deferredStart < $deferredEnd);
    }

    private function invoke(Closure $func, mixed ...$args): void {
        try {
            $func(...$args);
        } catch (\Throwable $e) {
            Instrumentation::setExceptionInfo(
                $e,
                closure: $func,
                extraData: [
                    'args' => $args,
                ]
            );
            Kernel::error("{exceptionClass} {message} in {file}:{line}", [
                'exceptionClass' => $e::class,
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }
    }
    
    private function getMaxSleepTime(): float {
        $now = \hrtime(true) / 1000000000;
        if (
            $this->deferredStart < $this->deferredEnd ||
            $this->microtaskStart < $this->microtaskEnd
        ) {
            return 0;
        }
        if ($this->timers->valid()) {
            return \max(0, $this->timers->top()->timestamp - $now);
        }
        return 0.25;
    }
}