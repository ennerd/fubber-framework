<?php
namespace Fubber\Kernel\EventLoop;

use Closure;

interface EventLoopInterface {

    /**
     * Schedule a callback to be invoked on the next available time.
     * 
     * @param callable $callable 
     * @param array $args 
     */
    public function defer(Closure $callable, mixed ...$args): void;

    /**
     * Schedule a function to be invoked immediately before the next
     * deferred function.
     * 
     * @param callable $callabke 
     * @param array $args 
     */
    public function queueMicrotask(Closure $callable, mixed ...$args): void;

    /**
     * Invoke the callable when a stream resource becomes readable
     * 
     * @param mixed $resource 
     * @param callable $callable 
     */
    public function readable(mixed $resource, Closure $callable): void;

    /**
     * Invoke the callable when a stream resource becomes writable
     * 
     * @param mixed $resource 
     * @param callable $callable 
     */
    public function writable(mixed $resource, Closure $callable): void;

    /**
     * Run the callable after a number of seconds
     * 
     * @param float $seconds 
     * @param callable $handler 
     */
    public function setTimeout(float $seconds, Closure $handler): void;


    /** 
     * Run all the deferred callbacks that currently have been enqueued.
     * If new callbacks are added to the queue, they must NOT be run.
     */
    public function runEvents(): void;

}