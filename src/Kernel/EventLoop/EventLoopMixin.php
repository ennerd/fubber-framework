<?php
namespace Fubber\Kernel\EventLoop;

use Kernel;
use Closure;
use Fubber\DS\Vector;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Mixins\MixinConstructor;

/**
 * @mixin Kernel
 */
trait EventLoopMixin {

    public readonly EventLoopInterface $eventLoop;

    #[MixinConstructor(__TRAIT__)]
    private function constructEventLoop(): void {
        $this->eventLoop = Factory::getEventLoop();
    }

    public function defer(Closure $callable, ...$args): void {
        $this->eventLoop->defer($callable, ...$args);
    }

    public function queueMicrotask(Closure $callable, ...$args): void {
        $this->eventLoop->queueMicrotask($callable, ...$args);
    }

    public function runEvents(): void {
        $this->eventLoop->runEvents();
   }
}