<?php
namespace Fubber\Kernel\EventLoop;

use Closure;

final class Timer {
    public function __construct(
        public readonly float $timestamp,
        public readonly Closure $callable
    ) {}
}