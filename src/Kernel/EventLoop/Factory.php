<?php
namespace Fubber\Kernel\EventLoop;

use Fubber\Kernel\Container\FactoryInterface;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\Scope;

class Factory {

    public static function getEventLoop(): EventLoopInterface {
        return new PhpEventLoop();
    }

}