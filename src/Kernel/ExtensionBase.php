<?php
namespace Fubber\Kernel;

use Fubber\InternalErrorException;
use Fubber\Kernel;
use Fubber\Kernel\Instrumentation\Instrumentation;

abstract class ExtensionBase {

    /**
     * Array is readable by the Kernel and by the extension.
     * 
     * @var array
     */
    protected array $extensionState = [];

    /**
     * Functions accessible by Extension classes and the Kernel.
     */
    protected final function includePHPFile(string $path): mixed {
        if ($this instanceof Kernel) {
            try {
                return include($path);
            } catch (\Throwable $e) {
                throw Instrumentation::setExceptionInfo(
                    $e,
                    redirect: 1
                );
            }
        } else {
            throw new InternalErrorException("includePHPFile can only be called on the Kernel class.");
        }
    }

}