<?php
namespace Fubber\Kernel;

use Fubber\ConfigErrorException;
use Fubber\InvalidArgumentException;
use Fubber\Kernel;
use Fubber\Kernel\Config\ConfigMixin;
use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Mixins\MixinConstructor;
use Throwable;

/**
 * Loads service configuration from the application and default services
 */
trait LoadConfigMixin {
    use ServicesMixin;
    use ConfigMixin;

    #[MixinConstructor(__TRAIT__, ServicesMixin::class, ConfigMixin::class)]
    private function constructLoadConfig(): void {
        $this->loadServicesConfig();
        $this->loadClassAliasesConfig();
    }

    /**
     * Loads the `services` config file from the application main config
     * directory, and the fills in any missing configuration values from
     * `config/services.php` in the framework root.
     * 
     * @throws Throwable 
     * @throws InvalidArgumentException 
     * @throws ConfigErrorException 
     */
    private function loadServicesConfig() {
        $factories = $this->config->load('services', false, []);
        $factories += require(static::getFrameworkPath().'/config/services.php');
        foreach ($factories as $serviceId => $method) {
            $this->container->add($serviceId, $method, Scope::Request);
        }
    }

    /**
     * Loads the `class_alises` config file from the application main config
     * directory, and the fills in any missing configuration values from
     * `config/class_aliases.php` in the framework root.
     * 
     * @throws Throwable 
     * @throws InvalidArgumentException 
     */
    private function loadClassAliasesConfig() {
        $classAliases = $this->config->load('class_aliases', false, []);
        $classAliases += require(Kernel::getFrameworkPath().'/config/class_aliases.php');

        foreach ($classAliases as $alias => $original) {
            if (!\class_exists($alias, false)) {
                \class_alias($original, $alias, true);
            }
        }
    }
}