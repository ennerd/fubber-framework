# Kernel Bootstrap Process

During a normal bootstrap, the Kernel goes through the following phases:

1. CONSTRUCTING
2. SYSTEM
3. MODULES
4. SERVICES
5. CONTROLLERS
6. INTEGRATE
7. READY
8. TERMINATED

If any of the previous phases fail with an error or exception, the FAILED phase is entered followed by the TERMINATED phase.

## CONSTRUCTING phase

See `Kernel::__constructor()`

During the CONSTRUCTING phase, the Kernel will instantiate the essential properties of the Kernel object:

 * The dispatchers `Kernel::$on*`.

 * The `Kernel::$phases` state machine.

    * Configuring the exception handler for the state machine.

    * Parse PHP attributes `#[OnPhaseEntered($phase)]`, `#[OnPhaseEntered($phase)]` and `#[OnPhaseChange]` of the Kernel instance and add listeners to the `Kernel::$phases` state machine. This approach intends to structure the integration of various aspects of the Kernel into separate `trait` classes.

 Next, a set of core services is added to the service container registry, making them available externally:

    * Kernel (making the Kernel available for dependency injection)

    * Dev (provides benchmarking and extra information for exceptions thrown)

    * Microcache (improves performance by caching small amounts of data in memory on the server)

    * FileSystem (a thin caching filesystem layer)

    * Environment (environment variables)

    * Config (loading of config files)

    * DependencyInjector (function and class instantiator)

    * Router (for routing strings to functions)

    * ExceptionHandlers (for routing exceptions to functions)

# SYSTEM phase

During the SYSTEM phase the Kernel reads configuration files from disk

$array ...$value