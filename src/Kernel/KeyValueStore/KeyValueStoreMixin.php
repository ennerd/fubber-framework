<?php
namespace Fubber\Kernel\KeyValueStore;

use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\LoadConfigMixin;
use Fubber\KeyValueStore\KeyValueStoreFactory;
use Fubber\KeyValueStore\KeyValueStoreInterface;
use Fubber\Mixins\MixinConstructor;

trait KeyValueStoreMixin {
    use ServicesMixin;
    use LoadConfigMixin;

    public readonly KeyValueStoreInterface $keyValueStore;

    #[MixinConstructor(__TRAIT__, ServicesMixin::class, LoadConfigMixin::class)]
    private function constructKeyValueStore(): void {
        $this->container->add(
            KeyValueStoreInterface::class,
            KeyValueStoreFactory::class,
            Scope::Singleton
        );
        $this->keyValueStore = $this->get(KeyValueStoreInterface::class);
    }

}