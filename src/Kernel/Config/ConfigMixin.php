<?php
namespace Fubber\Kernel\Config;

use Fubber\Kernel\Environment\EnvironmentMixin;
use Fubber\Kernel\Filesystem\FilesystemMixin;
use Fubber\Kernel\Microcache\MicrocacheMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Mixins\MixinConstructor;

trait ConfigMixin {
    use ExtensionsMixin;
    use MicrocacheMixin;
    use FilesystemMixin;
    use ServicesMixin;
    use EnvironmentMixin;

    public readonly Config $config;

    #[MixinConstructor(__TRAIT__, ExtensionsMixin::class, MicrocacheMixin::class, FilesystemMixin::class, ServicesMixin::class, EnvironmentMixin::class)]
    private function constructConfig(): void {
        $this->container->addSingleton(Config::class, $this->config = new Config($this->microcache->withNamespace(Config::class), $this->filesystem));
        $this->addExtension($this->config);
        $this->config->add($this->env->configRoot);
    }

    public function addConfigPath(string $path): void {
        $this->config->add($path);
    }

    public function getConfigPaths(): array {
        return $this->config->all();
    }

    /**
     * Resolve the path to a configuration file
     * 
     * @param string $filename The filename of the configuration file we're searching for.
     * @return string The full path to the configuration file.
     */
    public function getConfigPath(string $filename): string {
        return $this->config->find($filename);
    }

    public function loadConfig(string $filename, mixed $default=null): mixed {
        return $this->config->load($filename);
    }
}