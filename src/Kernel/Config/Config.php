<?php
namespace Fubber\Kernel\Config;

use CodingErrorException;
use Fubber\ConfigErrorException;
use Fubber\DS\Vector\Strings;
use Fubber\InvalidArgumentException;
use Fubber\Kernel\AbstractPathRegistry;
use Fubber\Kernel\Extensions\KernelExtension;
use Fubber\Kernel\Filesystem\Filesystem;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Microcache\MicrocacheInterface;
use Throwable;

class Config extends AbstractPathRegistry {

    /**
     * Counts how many times a config value has been loaded
     * 
     * @var array
     */
    protected array $loadedConfigPaths = [];

    protected function getFileExtensions(): array {
        return ['php'];
    }

    public function getExtensionName(): string {
        return 'Config Paths';
    }


    /**
     * Load configuration from a PHP file
     * 
     * @param string $name The configuration name you wish to load, without extension
     * @param mixed $default The default value to return
     * @param bool $required 
     * @return mixed The returned variable from the config file
     * @throws Throwable 
     * @throws InvalidArgumentException 
     */
    public function load(string $name, bool $required=true, mixed $default=null): mixed {
        if ($name === '') {
            throw new InvalidArgumentException("Config `$name` is illegal");
        }
        if (\str_ends_with($name, '.php')) {
            throw Instrumentation::redirect(new CodingErrorException("Config name must not end with an extension"));
        }
        $path = $this->find($name, $required, '.php');
        if (!$required && $path === null) {
            return $default;
        }
        if (\array_key_exists($path, $this->loadedConfigPaths)) {
            return $this->loadedConfigPaths[$path];
            throw Instrumentation::redirect(new InvalidArgumentException("Refusing to load the config `$path` twice"));
        }
        try {
            $result = require($path);
            $this->loadedConfigPaths[$path] = $result;
            return $result;
        } catch (\Throwable $e) {
            throw Instrumentation::setExceptionInfo($e, 'Check the config file', extraData: [
                'name' => $name,
                'path' => $path,
            ], redirect: 1);
        }
    }

}