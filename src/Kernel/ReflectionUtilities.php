<?php
namespace Fubber\Kernel;

use Closure;
use Fubber\Kernel\Debug\Describe;
use ReflectionClass;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionType;
use ReflectionUnionType;

class ReflectionUtilities {
    
    /**
     * Scan a function signature and get service identifiers required
     * 
     * @param Closure $closure 
     * @return iterable<string> 
     */
    public static function getDependenciesFromFunction(Closure $closure): iterable {
        return self::getDependenciesFromReflectionFunction(new ReflectionFunction($closure));
    }

    public static function getDependenciesFromReflectionFunction(ReflectionFunctionAbstract $reflectionFunction): iterable {
        $dependencies = [];
        foreach ($reflectionFunction->getParameters() as $parameter) {
            if ($parameter->allowsNull() || $parameter->isDefaultValueAvailable()) {
                // this parameter is not required, so it is not a dependency
            } elseif (!$parameter->hasType()) {
                // this parameter does not declare a type
            } else {
                foreach (self::getDependenciesFromReflectionType($parameter->getType()) as $dependency) {
                    if (\is_array($dependency)) {
                        $dependencies[] = $dependency;
                    } else {
                        $dependencies[$dependency] = $dependency;
                    }
                }
            }
        }
        return $dependencies;
    }

    public static function getDependenciesFromClass(string $className): iterable {
        return self::getDependenciesFromReflectionClass(new ReflectionClass($className));
    }

    public static function getDependenciesFromReflectionClass(ReflectionClass $reflectionClass): iterable {
        $reflectionFunction = $reflectionClass->getConstructor();
        if ($reflectionFunction === null) {
            return [];
        }
        return self::getDependenciesFromReflectionFunction($reflectionFunction);
    }

    public static function getDependenciesFromReflectionType(ReflectionType $type): iterable {
        if ($type->allowsNull()) {
            return;
        }
        if ($type instanceof ReflectionNamedType) {
            if ($type->isBuiltin()) {
                return;
            }
            yield $type->getName();
        } elseif ($type instanceof ReflectionIntersectionType) {
            foreach ($type->getTypes() as $subType) {
                yield from self::getDependenciesFromReflectionType($subType);
            }
        } elseif ($type instanceof ReflectionUnionType) {
            $alternatives = [];
            foreach ($type->getTypes() as $subType) {
                $alternatives[] = $subType->getName();
            }
            yield $alternatives;
        }
    }

    public static function getSignatureFromReflectionFunction(ReflectionFunctionAbstract $reflectionFunction, bool $asClosure=true): string {

        if ($asClosure) {
            if ($reflectionFunction->returnsReference()) {
                $signature = 'function &';
            } else {
                $signature = 'function';
            }
        } else {
            if ($reflectionFunction->returnsReference()) {
                $signature = 'function &' . $reflectionFunction->getName();
            } else {
                $signature = 'function ' . $reflectionFunction->getName();
            }
        }

        $signature .= self::getArgumentListFromReflectionParameters(...$reflectionFunction->getParameters());

        if ($reflectionFunction->hasReturnType()) {
            $signature .= ': ' . self::getSignatureFromReflectionType($reflectionFunction->getReturnType());
        }

        return $signature;
    }

    public static function getArgumentListFromReflectionParameters(ReflectionParameter ...$reflectionParameters): string {
        if (empty($reflectionParameters)) {
            return '()';
        }
        $args = [];
        foreach ($reflectionParameters as $reflectionParameter) {
            $args[] = self::getSignatureFromReflectionParameter($reflectionParameter);
        }
        return \implode(", ", $args);
    }

    public static function getSignatureFromReflectionParameter(ReflectionParameter $reflectionParameter): string {
        return Describe::parameter($reflectionParameter);
    }

    public static function getSignatureFromReflectionType(ReflectionType $type): string {
        return Describe::type($type);
    }
}