<?php
namespace Fubber\Kernel\Bundles;

use ArrayAccess;
use Composer\Autoload\ClassLoader;
use Countable;
use Fubber\CodingErrorException;
use Fubber\Kernel;
use Fubber\Kernel\InvalidArgumentException;
use Fubber\Kernel\AbstractPathRegistry;
use Fubber\Kernel\Config\Config;
use Fubber\Kernel\Controllers\Controllers;
use Fubber\Kernel\Environment\Environment;
use Fubber\Kernel\EventLoop\EventLoopInterface;
use Fubber\Kernel\Filesystem\Filesystem;
use Fubber\Kernel\LogicException;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Views\Views;
use Fubber\Microcache\MicrocacheInterface;
use IteratorAggregate;
use Traversable;

class Bundles extends AbstractPathRegistry implements IteratorAggregate, Countable, ArrayAccess {

    protected ClassLoader $composer;
    protected array $initFunctions = [];

    public function __construct(
        protected Kernel $kernel,
        protected Environment $env,
        protected Config $config,
        protected Filesystem $filesystem,
        protected MicrocacheInterface $microcache,
        protected Views $views,
        protected EventLoopInterface $loop
    ) {
        parent::__construct($microcache, $filesystem);
        $this->composer = require(Kernel::getComposerPath() . '/vendor/autoload.php');
    }

    public function offsetExists(mixed $offset): bool {
        return isset($this->manifests[$offset]);
    }

    public function offsetGet(mixed $offset): Manifest {
        return $this->manifests[$offset] ?? null;
    }

    public function offsetSet(mixed $offset, mixed $value): void {
        throw new LogicException("Can't manipulate bundles this way");
    }

    public function offsetUnset(mixed $offset): void {
        throw new LogicException("Can't manipulate bundles this way");
    }

    public function count(): int {
        return \count($this->manifests);
    }

    public function getIterator(): Traversable {
        yield from $this->manifests;
    }

    /**
     * Root path for all added bundles
     * 
     * @var Manifest[]
     */
    private array $manifests = [];

    protected function getFileExtensions(): ?array {
        return null;
    }

    public function getExtensionName(): string {
        return "Bundles Registry";
    }

    public function add(string $path): void {
        Kernel::debug("BUNDLE AT ".$path);
        if (!$this->filesystem->is_dir($path)) {
            throw new InvalidArgumentException("The path `$path` is not a directory");
        }
        parent::add($path);
        if ($this->filesystem->is_file([$path, 'manifest.php'])) {
            $manifest = $this->loadManifest($path);
        } else {
            $manifest = $this->makeManifest($path);
        }

        $this->processManifest($manifest);
    }

    protected function processManifest(Manifest $manifest): void {
        if (isset($this->manifests[$manifest->name])) {
            throw new LogicException("Manifest with name `" . $manifest->name . "` is already added");
        }

        $this->manifests[$manifest->name] = $manifest;

        /**
         * Add `src/` to autoloader.
         */
        $srcPath = $this->filesystem->buildPath($manifest->path, 'src');
        if ($this->filesystem->is_dir($srcPath)) {
            $this->composer->addPsr4($manifest->namespace, $srcPath);
        } elseif ($manifest->namespace !== '') {
            throw new LogicException("Manifest `" . $manifest->name ."` declares a namespace, but has no `src/` directory");
        }

    }

    protected function loadManifest(string $path): Manifest {
        /** @var Manifest */
        $manifest = require($manifestPath = $this->filesystem->buildPath($path, 'manifest.php'));
        if (!($manifest instanceof Manifest)) {
            throw new CodingErrorException("Manifest file at `$path` is expected to return an instance of `" . Manifest::class . "`");
        }
        return $manifest;
    }

    /**
     * Makes a manifest for legacy bundles
     */
    protected function makeManifest(string $path): Manifest {
        $manifest = new Manifest(path: $path, name: \basename($path), namespace: '', init: function() use ($path) {
            $initFile = $this->filesystem->buildPath($path, 'init.php');
            if ($this->filesystem->is_file($initFile)) {
                $this->kernel->includePHPFile($initFile);
            }
        });
        return $manifest;
    }

    protected function runAll(string $manifestPropertyName, object ...$injectableObjects): void {
        /** @var Manifest[] */
        $deferred = [];
        foreach ($this->manifests as $manifest) {
            if ($manifest->$manifestPropertyName) {
                $doIt = $this->kernel->container->mayInvoke($manifest->$manifestPropertyName, extraObjects: $injectableObjects);
                if ($doIt) {
                    $this->loop->queueMicrotask($doIt);
                } else {
                    $deferred[] = $manifest;
                }
            }
        }
        if (!empty($deferred)) {
            throw new LogicException("Unable to run `$manifestPropertyName` for manifest `".$deferred[0]->name."`");
        }
    }

    protected function runInitFunctions(): void {
        $this->runAll('init');
        $initFunctions = $this->initFunctions;
        $this->initFunctions = [];
        foreach ($initFunctions as $initFunction) {
            $doIt = $this->kernel->container->mayInvoke($initFunction);
            if ($doIt) {
                $this->loop->queueMicrotask($doIt);
            } else {
                $this->initFunctions[] = $initFunction;
            }
        }
        if (!empty($this->initFunctions)) {
            die("Init functions I can't run");
        }
    }


    #[PhaseHandler(Phase::Bundles)]
    private function onBundlesPhase(): void {
        // scan for bundles in the automatic locations
        $bundles = $this->findAutoBundlePaths();
        foreach ($bundles as $path) {
            $this->add($path);
        }

        // extend the paths
        $this->config->filter->listen(function(array $configPaths) {
            foreach ($this->findDirs('config') as $extraConfigDir) {
                $configPaths[] = $extraConfigDir;
            }
            return $configPaths;
        });

        $this->views->filter->listen(function(array $viewPaths) {
            foreach ($this->findDirs('views') as $extraViewPath) {
                $viewPaths[] = $extraViewPath;
            }
            return $viewPaths;
        });

        $this->runInitFunctions();
    }

    private function findAutoBundlePaths(): iterable {
        return $this->microcache->fetch(
            'findAutoBundlePaths',
            function() {
                $paths = [];
                $scanDirFunction = function(string ...$path) use (&$paths) {
                    foreach ($this->filesystem->glob([ ...$path, '*'], \GLOB_ONLYDIR) as $subPath) {
                        $paths[] = $subPath;
                    }
                };
                $locations = [
                    $this->filesystem->buildPath(Kernel::getFrameworkPath(), 'bundles'),
                    $this->filesystem->buildPath($this->env->appRoot, 'modules'),
                    $this->filesystem->buildPath($this->env->appRoot, 'extra'),
                    $this->filesystem->buildPath($this->env->appRoot, 'bundles')
                ];
                foreach ($locations as $location) {
                    if ($this->filesystem->is_dir($location)) {
                        $scanDirFunction($location);
                    }
                }
                return $paths;
            },
            DEBUG ? 1 : 60
        );
    }
}