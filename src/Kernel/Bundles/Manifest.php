<?php
namespace Fubber\Kernel\Bundles;

use Closure;
use Fubber\ControllerInterface;

/**
 * Manifest for bundles 
 * 
 * @package Fubber\Kernel
 */
final class Manifest {

    /**
     * Configure the bundle
     * 
     * @param string $path The root path for the bundle
     * @param string $name A globally unique name for this bundle
     * @param null|string $namespace A namespace prefix to use for any files in the `src/` path of this bundle
     * @param Closure $init A function which will be invoked with dependency injection as soon as all dependencies are available
     */
    public function __construct(
        public readonly string $path,
        public readonly string $name,
        public readonly string $namespace='',
        public readonly ?Closure $init=null,
    ) {}

}