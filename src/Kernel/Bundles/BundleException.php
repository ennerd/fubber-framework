<?php
namespace Fubber\Kernel\Bundles;

use Fubber\RuntimeException;

class BundleException extends RuntimeException {
    
}