<?php
namespace Fubber\Kernel\Bundles;

use Fubber\Kernel;
use Fubber\Kernel\Config\ConfigMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\Controllers\ControllersMixin;
use Fubber\Kernel\Environment\EnvironmentMixin;
use Fubber\Kernel\EventLoop\EventLoopMixin;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Kernel\Filesystem\FilesystemMixin;
use Fubber\Kernel\Microcache\MicrocacheMixin;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Phases\PhasesMixin;
use Fubber\Kernel\Views\ViewsMixin;
use Fubber\Mixins\MixinConstructor;
use Fubber\Mixins\Mixins;

/**
 * @mixin Kernel
 */
trait BundlesMixin {
    use ExtensionsMixin;
    use ServicesMixin;
    use FilesystemMixin;
    use ViewsMixin;
    use ConfigMixin;
    use EnvironmentMixin;
    use MicrocacheMixin;
    use PhasesMixin;
    use EventLoopMixin;

    public readonly Bundles $bundles;

    #[MixinConstructor(__TRAIT__, ExtensionsMixin::class, ServicesMixin::class, FilesystemMixin::class, ViewsMixin::class, ConfigMixin::class, EnvironmentMixin::class, EventLoopMixin::class)]
    private function constructBundles(): void {
        $this->bundles = new Bundles(
            $this,
            $this->env,
            $this->config,
            $this->filesystem,
            $this->microcache->withNamespace(__TRAIT__),
            $this->views,
            $this->eventLoop
        );
        $this->addExtension($this->bundles);
        $this->container->addSingleton(Bundles::class, $this->bundles);
    }
}