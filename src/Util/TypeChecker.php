<?php
namespace Fubber\Util;

use ArrayAccess;
use Closure;
use LogicException;
use RuntimeException;
use TypeError;

/**
 * Declare an array shape for validation.
 * 
 * Example: <br />
 * ```
 *  $shape = new ArrayShape([
 *      'name' => 'string',
 *      'number' => 'int',
 *      'boolean' => 'bool',
 *      'optional' => '?string',
 *      'an_object' => 'SomeInterface|SomeClass',
 *      'enum' => '1|2|3|4',
 *      'number_range' => '0..255',
 *      'nested_shape' => [
 *          'property' => 'bool|true',
 *          'key' => '"
 *      ]
 *  ]);
 */
class TypeChecker {

    const RE = <<<'RE'
        /(?<var>[a-zA-Z_\x7f-\xff](\-?[a-zA-Z0-9_\x7f-\xff])*)|
         (?<str>"(\\\\.|[^"])*"|\'(\\\\.|[^"])*\')|
         (?<float>(0|[1-9][0-9]*)\.[0-9]+)|
         (?<int>0|[1-9][0-9]*)|
         (?<token>[:?|<>{},*\[\]])|
         (?<ws>\s+)|
         (?<unknown>.)
        /mx
        RE;

    protected Closure $validator;

    protected array $shape;
    protected array $tokens;
    protected int $offset;

    /**
     * @param string The type annotation according to PHPDoc, such as 'array{0: string, 1: int}'
     */
    public function __construct(string $type) {
        $this->offset = 0;
        $this->tokens = self::tokenize($type);
        $this->validator = $this->_parseType([]);
    }

    public function __invoke($value) {
        ($this->validator)($value);
    }

    private function _parseType(array $path): Closure {

        if ($this->_is('?')) {
            $this->_take('?');
            $validator = $this->_parseRequiredType($path);
            return function($value) use ($validator) {
                if ($value !== null) {
                    $validator($value);
                }
            };
        }

        return $this->_parseRequiredType($path);
    }

    private function _parseRequiredType(array $path): Closure {

        $buildName = function() use ($path, &$name) {
            return $name . (!empty($path) ? ' in ' . \implode('.', $path) : '');
        };

        $name = null;

        if ($this->_is('var')) {
            $name = $this->_take('var', 'identifier');

            switch ($name) {
                case 'string': $validator = \is_string(...); break;
                case 'int': $validator = \is_int(...); break;
                case 'float': $validator = function($value) { return \is_float($value) || \is_int($value); }; break;
                case 'bool': $validator = \is_bool(...); break;
                case 'class-string': $validator = \class_exists(...); break;
                case 'resource': $validator = \is_resource(...); break;
                case 'callable': $validator = \is_callable(...); break;
                case 'true': $validator = function($value) { return $value === true; }; break;
                case 'false': $validator = function($value) { return $value === false; }; break;
                case 'null': $validator = \is_null(...); break;
                case 'object': $validator = \is_object(...); break;
                case 'array': $validator = \is_array(...); break;
                case 'never': $validator = function($value) { return false; }; break;
                default:
                    $validator = function($value) use ($name) {
                        return $value instanceof $name;
                    };
                    break;
            }

            $validator = function($value) use ($validator, $buildName) {
                if (!$validator($value)) {
                    throw new TypeError('Unexpected ' . \get_debug_type($value) . ', expecting ' . $buildName());
                }
            };
        } elseif ($this->_is('int') || $this->_is('float')) {
            $num = (int) $this->_take('int');
            $name = $num;
            $validator = function($value) use ($num, $buildName) {
                if ($value !== $num) {
                    throw new TypeError('Unexpected ' . \get_debug_type($value) . ', expecting ' . $buildName());
                }
                return $value === $num;
            };
        } elseif ($this->_is('str')) {
            $str = $this->_take('str');
            $name = $str;
            $str = self::parseEscapeSequences(\substr($str, 1, -1), $str[0]);
            $validator = function($value) use ($str, $buildName) {
                if ($value !== $str) {
                    throw new TypeError('Unexpected ' . \get_debug_type($value) . ', expecting ' . $buildName());
                }
            };
        } else {
            throw new LogicException("Parse Error: Unexpected `" . $this->tokens[$this->offset][0] . "`");
        }

        if ($this->_is('<')) {
            if (
                $name === 'array' ||
                $name === 'iterable' ||
                $name instanceof \Traversable
            ) {
                $startOffset = $this->offset;
                $this->_take('<');
                $type0 = $this->_parseType($path + [ &$name ]);
                if ($this->_is(',')) {
                    $type1 = $this->_parseType($path + [ &$name, '.0' ]);
                    $validator = function($value) use ($validator, $type0, $type1) {
                        $validator($value);
                        foreach ($value as $key => $value) {
                            $type0($key);
                            $type1($value);
                        }
                    };
                } else {
                    $validator = function($value) use ($validator, $type0) {
                        $validator($value);
                        foreach ($value as $value) {
                            $type0($value);
                        }
                    };
                }
                $this->_take('>');
                for ($i = $startOffset; $i < $this->offset; $i++) {
                    $name .= $this->tokens[$i][0];
                }
            } else {
                throw new LogicException("Can't use <key, type> validators for `$name` types");
            }
        } elseif ($this->_is('{')) {
            if (
                $name === 'array' ||
                $name === 'object' ||
                $name instanceof ArrayAccess
            ) {
                $startOffset = $this->offset;
                $this->_take('{');
                $requiredKeys = [];
                $optionalKeys = [];
                $remainingKeys = null;
                while (!$this->_is('}')) {
                    $optional = false;
                    if ($this->_is('?')) {
                        $optional = true;
                        $this->_take('?');
                    }
                    if ($this->_is('*') && !$optional) {
                        $key = $this->_take('*');
                        $this->_take(':');
                        $remainingKeys = $this->_parseType($path + [ &$name, '["*"]']);
                        goto next;
                    } elseif ($this->_is('int')) {
                        $key = (int) $this->_take('int');
                    } elseif ($this->_is('var')) {
                        $key = $this->_take('var');
                    } elseif ($this->_is('str')) {
                        $key = $this->_take('str');
                        $key = self::parseEscapeSequences(\substr($key, 1, -1), $key[0]);
                    } else {
                        throw new LogicException('Unexpected ' . $this->tokens[$this->offset][0] . ' in type declaration');
                    }
                    $this->_take(':');
                    if ($optional) {
                        $optionalKeys[$key] = $this->_parseType($path + [ &$name, '.'. $key . '?']);
                    } else {
                        $requiredKeys[$key] = $this->_parseType($path + [ &$name, '.' . $key]);
                    }
                    next:
                    if ($this->_is(',')) {
                        $this->_take(',');
                    }
                }
                $validator = function($value) use ($buildName, $name, $validator, $requiredKeys, $optionalKeys, $remainingKeys, $path) {
                    $validator($value);
                    $handledKeys = [];
                    if ($name === 'object') {
                        foreach ($requiredKeys as $key => $keyValidator) {
                            if (!\property_exists($value, $key)) {
                                throw new TypeError('Object property `' . $key . "` must be declared in " . $buildName());
                            }
                            $keyValidator($value->$key);
                            $handledKeys[] = $key;
                        }
                        foreach ($optionalKeys as $key => $keyValidator) {
                            if (\property_exists($value, $key)) {
                                $keyValidator($value->$key ?? null);                                
                            }
                            $handledKeys[] = $key;
                        }
                    } elseif ($name === 'array' || $value instanceof ArrayAccess) {
                        foreach ($requiredKeys as $key => $keyValidator) {
                            if (!\array_key_exists($key, $value)) {
                                throw new TypeError('Array key `' . $key . "` must be declared in " . $buildName());
                            }
                            $keyValidator($value[$key]);
                        }
                        foreach ($optionalKeys as $key => $keyValidator) {
                            if (\array_key_exists($key, $value)) {
                                $keyValidator($value[$key]);
                            }
                            $handledKeys[] = $key;
                        }
                    }
                    if ($remainingKeys !== null) {
                        foreach ($value as $key => $value) {
                            if (\in_array($key, $handledKeys)) {
                                continue;
                            }
                            $remainingKeys($value);
                        }
                    }
                };
                $this->_take('}');
                for ($i = $startOffset; $i < $this->offset; $i++) {
                    $name .= $this->tokens[$i][0];
                }
            } else {
                throw new LogicException("Can't use {key: type, ...} validators for `$name` types");
            }
        }

        while ($this->_is('[')) {
            $path = [ \implode('.', $path), '[]' ];
            $this->_take('[');
            $this->_take(']');
            $offset = $this->offset;
            $validator = function($value) use ($validator, $offset, $path, $name) {
                if (!\is_array($value) ) {
                    throw new TypeError("Expecting array in " . \implode('.', $path + [ $name ]));
                }
                foreach ($value as $sub) {
                    $validator($sub);
                }
            };
        }

        if ($this->_is('|')) {
            $this->_take('|');
            \array_pop($path);
            $nextValidator = $this->_parseRequiredType($path);
            $offset = $this->offset;
            $validator = function($value) use ($validator, $nextValidator, $path) {
                try {
                    $validator($value);
                } catch (TypeError $e) {
                    try {
                        $nextValidator($value);
                    } catch (TypeError $e) {
                        throw new TypeError("Unexpected `" . \get_debug_type($value). "` when parsing " . implode('.', $path));
                    }
                }
            };
        }

        return $validator;
    }

    private function _textTokens(int $firstToken, int $length): array {
        $result = [];
        for ($i = $firstToken; $i < $firstToken + $length; $i++) {
            $result[] = $this->tokens[$i][0];
        }
        return $result;
    }

    private function _is(string $what): bool {
        return isset($this->tokens[$this->offset]) && ($this->tokens[$this->offset][0] === $what || $this->tokens[$this->offset][1] === $what);
    }

    private function _take(string $what, string $expectedName=null) {
        if (!$this->_is($what)) {
            if ($this->offset > 0) {
                throw new LogicException('Parse Error: Expected ' . ($expectedName ?? '`' . $what . '`') . ' following `' . \implode('', $this->_textTokens(0, $this->offset)) . '`');
            } else {
                throw new LogicException('Parse Error: Expected ' . ($expectedName ?? '`' . $what . '`') . ' found `' . \implode('', $this->_textTokens($this->offset, 1)) . '`');
            }
        }
        return $this->tokens[$this->offset++][0];
    }

    private function _eof(): bool {
        return $this->offset >= count($this->tokens);
    }

    private static function tokenize(string $type): array {
        $count = \preg_match_all(self::RE, $type, $matches, \PREG_PATTERN_ORDER | \PREG_UNMATCHED_AS_NULL);
        $tokens = [];
        foreach ($matches[0] as $offset => $value) {
            foreach ($matches as $type => $subs) {
                if (\is_int($type)) {
                    continue;
                }
                if ($value === $subs[$offset]) {
                    if ($type === 'ws') {
                        continue;
                    }
                    if ($type === 'unknown') {
                        throw new LogicException('Parse Error: Unexpected `' . $value . '` following `' . \implode('', \array_slice($matches[0], 0, $offset)) . '`');
                    }
                    $tokens[] = [ $value, $type ];
                    continue 2;
                }
            }
        }
        return $tokens;
    }

    /**
     * {@see https://github.com/nikic/PHP-Parser/blob/4.x/lib/PhpParser/Node/Scalar/String_.php}
     * 
     * @internal
     *
     * Parses escape sequences in strings (all string types apart from single quoted).
     *
     * @param string      $str   String without quotes
     * @param null|string $quote Quote type
     *
     * @return string String with escape sequences parsed
     */
    private static function parseEscapeSequences(string $str, $quote) {
        if (null !== $quote) {
            $str = \str_replace('\\' . $quote, $quote, $str);
        }

        $replacements = [
            '\\' => '\\',
            '$'  =>  '$',
            'n'  => "\n",
            'r'  => "\r",
            't'  => "\t",
            'f'  => "\f",
            'v'  => "\v",
            'e'  => "\x1B",
        ];

        $codePointToUtf8 = function(int $num): string {
            if ($num <= 0x7F) {
                return \chr($num);
            }
            if ($num <= 0x7FF) {
                return \chr(($num>>6) + 0xC0) . \chr(($num&0x3F) + 0x80);
            }
            if ($num <= 0xFFFF) {
                return \chr(($num>>12) + 0xE0) . \chr((($num>>6)&0x3F) + 0x80) . \chr(($num&0x3F) + 0x80);
            }
            if ($num <= 0x1FFFFF) {
                return \chr(($num>>18) + 0xF0) . \chr((($num>>12)&0x3F) + 0x80)
                     . \chr((($num>>6)&0x3F) + 0x80) . \chr(($num&0x3F) + 0x80);
            }
            throw new RuntimeException('Invalid UTF-8 codepoint escape sequence: Codepoint too large');
        };

        return \preg_replace_callback(
            '~\\\\([\\\\$nrtfve]|[xX][0-9a-fA-F]{1,2}|[0-7]{1,3}|u\{([0-9a-fA-F]+)\})~',
            function($matches) use ($codePointToUtf8, $replacements) {
                $str = $matches[1];

                if (isset($replacements[$str])) {
                    return $replacements[$str];
                } elseif ('x' === $str[0] || 'X' === $str[0]) {
                    return \chr(\hexdec(\substr($str, 1)));
                } elseif ('u' === $str[0]) {
                    return $codePointToUtf8(\hexdec($matches[2]));
                } else {
                    return \chr(octdec($str));
                }
            },
            $str
        );
    }
}

$ts = new TypeChecker('array{"frode": string[], *: int}');
$ts(["test", 12.23, "frode" => 123]);
