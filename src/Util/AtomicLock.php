<?php
namespace Fubber\Util;

/**
 * Thread safe locking without hardware support
 * 
 * @package Fubber\Util
 */
class AtomicLock {

    /**
     * 
     * @var int[]
     */
    private array $slots;
    private int $slotCount = 5;

    public function __construct() {
        $this->slots = array_fill(0, $this->slotCount, 0);
    }

    public function acquire(): void {
        $id = \mt_rand(1, \PHP_INT_MAX - 1);
        $sequence = self::getSequence();
        while (!$this->tryAcquire($sequence, $id)) {
            \usleep(\mt_rand(0, 1000));
        }
    }

    public function tryAcquire(array $sequence, int $id): bool {
        /**
         * The below algorithm may suffer from a race condition,
         * in which we determine that we have the greatest id
         * and overwrite the slow. There is a chance that another
         * thread with a higher id will be overwritten, but by
         * performing the writes in a random sequence, it is much
         * less likely that we will overwrite every slot for that
         * other thread.
         */
        foreach ($sequence as $offset) {
            if ($id > $this->slots[$offset]) {
                $this->slots[$offset] = $id;
            } else {
                // We found a stronger thread id, so we give up
                $this->clearSlots($sequence, $id);
                $this->wait();
                return false;
            }
        }

        /**
         * Check in the same order that there is no greater thread id
         * in any of the slots. This check is meant to further mitigate
         * the low probability race condition in the above foreach.
         */
        foreach ($sequence as $offset) {
            if ($id < $this->slots[$offset]) {
                // We give up, another higher powered thread found
                $this->clearSlots($sequence, $id);
                $this->wait();
                return false;
            }
        }

        /**
         * A small check, just in case another thread has claimed the
         * lock for some unforeseen race condition.
         */
        foreach ($this->slots as $value) {
            if ($value === PHP_INT_MAX) {
                $this->clearSlots($sequence, $id);
                $this->wait();
                return false;
            }
        }

        /**
         * We have the lock, so ensure that no other thread attempts
         * can ever succeed in getting a lock. We do this by writing
         * PHP_INT_MAX to every slot. Since other threads attempt in
         * different order
         * We have the lock, so we'll spin write with the greatest
         * possible id ensuring all other threads fail and that
         * any new attempts also will fail until the lock is released.
         */
        foreach ($this->slots as &$value) {
            $value = PHP_INT_MAX;
        }
        foreach ($this->slots as &$value) {
            if ($value !== PHP_INT_MAX) {
                $this->clearSlots($sequence, PHP_INT_MAX);
                return false;
            }
        }

        return true;
    }

    private function wait(): void {
        \usleep(\mt_rand(0, 1000));
    }

    private function clearSlots(array $sequence, int $id) {
        foreach ($sequence as $offset) {
            if ($this->slots[$offset] <= $id) {
                $this->slots[$offset] = 0;
            }
        }
    }

    /**
     * Generate a unique sequence of slots we'll try to collect.
     * The unique order helps greatly increase the probability
     * of detecting a conflict before the reverse pass.
     * 
     * @return array 
     */
    private function getSequence(): array {
        $sequence = range(0, $this->slotCount - 1);
        shuffle($sequence);
        return $sequence;
    }
}