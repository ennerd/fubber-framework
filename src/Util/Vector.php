<?php
namespace Fubber\Util;

use ArrayAccess;
use Closure;
use Countable;
use Fubber\DS\Traits\TypeGuardTrait;
use IteratorAggregate;
use JsonSerializable;
use RangeException;
use Traversable;
use TypeError;

/**
 * @template T Type
 * @package Fubber\Util
 */
class Vector implements IteratorAggregate, ArrayAccess, Countable, JsonSerializable {

    private array $items;
    private int $lowOffset = 0;
    private int $highOffset = 0;

    /**
     * @param T ...$items 
     */
    public function __construct(mixed ...$items) {
        $this->items = $items;
    }

    /**
     * Check that all items are contained in the vector.
     * 
     * @param T ...$items 
     * @return bool 
     */
    public function contains(mixed ...$items): bool {
        foreach ($items as $item) {
            if (!\in_array($item, $this->items, true)) {
                return false;
            }
        }
        return true;
    }

    public function jsonSerialize(): mixed {
        return \array_values($this->items);
    }

    public function __debugInfo() {
        return \array_values($this->items);
    }

    private function getLowOffset(): int {
        return $this->lowOffset;
    }

    private function getHighOffset(): int {
        return $this->highOffset;
    }

    public final function count(): int {
        return $this->highOffset - $this->lowOffset;
    }

    public function push(mixed ...$values): void {
        $this->append(...$values);
    }

    public function append(mixed ...$values): void {
        foreach ($values as $value) {
            $this[] = $value;
        }
    }

    public function unshift(mixed ...$values): void {
        $this->prepend(...$values);
    }

    public function prepend(mixed ...$values): void {
        foreach ($values as $value) {
            $this[-1] = $value;
        }
    }

    public function shift(): mixed {
        if ($this->count() === 0) {
            return null;
        }
        $result = $this[0];
        unset($this[0]);
        return $result;
    }

    public function pop(): mixed {
        if ($this->count() === 0) {
            return null;
        }
        $result = $this[$this->count() - 1];
        unset($this[$this->count() - 1]);
        return $result;
    }

    public final function getIterator(): Traversable {
        if ($this->lowOffset === $this->highOffset) {
            return [];
        }
        $items = $this->items;
        $to = $this->highOffset;
        $index = 0;
        for ($i = $this->lowOffset; $i < $to; $i++) {
            yield $index++ => $items[$i];
        }
    }

    /**
     * Get an element by offset
     * 
     * @param int $offset 
     * @return ?T 
     * @throws TypeError 
     */
    public function offsetGet(mixed $offset): mixed {
        if (!\is_int($offset)) {
            throw new TypeError("Offset must be of type integer");
        }

        if ($offset >= 0 && $offset < $this->count()) {
            return $this->items[$this->lowOffset + $offset];
        }
        return null;
    }

    /**
     * Does the offset exist in the vector?
     * 
     * @param int $offset 
     * @return bool 
     * @throws TypeError 
     */
    public function offsetExists(mixed $offset): bool {
        if (!\is_int($offset)) {
            throw new TypeError("Offset must be of type integer");
        }

        return $offset >= 0 && $offset < $this->count();
    }

    /**
     * Prepend, append or overwrite an item in the vector
     *
     * @param int $offset 
     * @param T $value 
     * @throws TypeError 
     * @throws RangeException 
     */
    public function offsetSet(mixed $offset, mixed $value): void {
        if (!\is_int($offset) && $offset !== null) {
            throw new TypeError("Offset must be of type integer, got `$offset`");
        }

        if ($offset === null) {
            $offset = $this->highOffset;
        } else {
            $offset = (int) $offset;
        }
        if ($offset === $this->highOffset || $this->offsetExists($offset)) {
            // overwrite or append
            $this->items[$this->highOffset++] = $value;
        } elseif ($offset === -1) {
            // prepend
            $this->items[--$this->lowOffset] = $value;
        } else {
            throw new RangeException("Offset $offset is not within the vector");
        }
    }

    /**
     * Remove an element in the vector, shortening the vector by
     * one element.
     * 
     * @param int $offset 
     * @throws TypeError 
     * @throws RangeException 
     */
    public function offsetUnset(mixed $offset): void {
        if (!\is_int($offset)) {
            throw new TypeError("Offset must be of type integer");
        }

        if ($offset === 0 && $this->highOffset > $this->lowOffset) {
            // remove from beginning
            unset($this->items[$this->lowOffset++]);
        } elseif ($offset === $this->count() - 1) {
            // remove from end
            unset($this->items[--$this->highOffset]);
        } elseif ($offset > 0 && $offset < $this->count()) {
            for ($i = $this->lowOffset + $offset + 1; $i < $this->highOffset; $i++) {
                $this->items[$i - 1] = $this->items[$i];
            }
            unset($this->items[--$this->highOffset]);
        } else {
            throw new RangeException("Offset $offset is not within the vector");
        }
    } 
}