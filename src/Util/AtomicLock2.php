<?php
namespace Fubber\Util;

/**
 * Thread safe locking without hardware support
 * 
 * @package Fubber\Util
 */
class AtomicLock2 {

    /**
     * 
     * @var int[]
     */
    private array $slots;
    private int $slotCount = 20;

    public function __construct() {
        $this->slots = array_fill(0, $this->slotCount, 0);
    }

    public function acquire(): void {
        $id = \mt_rand(1, \PHP_INT_MAX - 1);
        $sequence = self::getSequence();
        while (!$this->tryAcquire($sequence, $id)) {
            \usleep(\mt_rand(0, 1000));
        }
    }

    public function tryAcquire(array $sequence, int $id): bool {
        /**
         * Write our $id to each of the slots, and stop trying if we discover
         * a higher id
         */
        foreach ($sequence as $offset) {            
            if ($id > $this->slots[$offset]) {
                /**
                 * Possible race condition; a higher id can already have written,
                 * and we'll overwrite that higher id. Since each thread follows
                 * a unique sequence, it is very unlikely that the thread that
                 * caused this race condition will follow the exact same sequence
                 * so we'll get another opportunity to find that other thread.
                 */
                $this->slots[$offset] = $id;
            } else {
                /**
                 * As soon as we discover a higher id, we'll back off and let that
                 * thread have the lock.
                 */
                $this->clearSlots($sequence, $id);
                $this->wait();
                return false;
            }
        }

        /**
         * Most other threads will be gone by now, but in the slim
         * chance that more than one thread reaches this point, we
         * must confirm.
         */
        do {
            $updated = false;
            foreach ($sequence as $offset) {
                $value = &$this->slots[$offset];
                if ($value < $id) {
                    $updated = true;
                    $value = $id;                    
                } elseif ($value !== $id) {
                    // A higher thread id found, so we back off
                    $this->clearSlots($sequence, $id);
                    $this->wait();
                    return false;
                }
            }
        } while ($updated);

        return true;
    }

    private function wait(): void {
        \usleep(\mt_rand(0, 1000));
    }

    private function clearSlots(array $sequence, int $id) {
        foreach ($sequence as $offset) {
            $value = &$this->slots[$offset];
            if ($value === $id) {
                $value = 0;
            }
        }
    }

    /**
     * Generate a unique sequence of slots we'll try to collect.
     * The unique order helps greatly increase the probability
     * of detecting a conflict before the reverse pass.
     * 
     * @return array 
     */
    private function getSequence(): array {
        $sequence = range(0, $this->slotCount - 1);
        shuffle($sequence);
        return $sequence;
    }
}