<?php
namespace Fubber\Util;

use Closure;
use RuntimeException;

/**
 * Thread safe locking without hardware support
 * 
 * @package Fubber\Util
 */
class AtomicLock3 {

    /**
     * These slots store the id of the lock owner, or 0 if the slot is available.
     * 
     * @var int[]
     */
    private array $slots;
    private int $slotCount = 20;

    public function __construct() {
        $this->slots = array_fill(0, $this->slotCount, 0);
    }

    /**
     * Returns null if a lock was not acquired, and a closure
     * which will release the lock when invoked or garbage
     * collected.
     * 
     * @return null|Closure 
     * @throws RuntimeException 
     */
    public function acquire(): ?Closure {
        $myId = \mt_rand(1, \PHP_INT_MAX - 1);
        $sequence = self::getSequence();
        $lockId = $this->tryAcquire($sequence, $myId);
        if ($lockId === $myId) {
            return $this->makeReleaseFunction($myId);
        }
        return null;
    }

    public function tryAcquire(array $sequence, int $id): int {
        // Counts the number of times only our own ID was encountered
        $hits = 0;
        $i = 0;
        for ($i = 0; $hits < $this->slotCount; $i++) {
            $offset = $sequence[$i % $this->slotCount];
            $value = &$this->slots[$offset];
            $diff = $value - $id;
            if ($diff === 0) {
                ++$hits;
            } elseif ($diff > 0) {
                // A higher id was discovered, so we'll not be getting a lock
                $this->clearSlots($sequence, $id);
                return $id + $diff;
            } else {
                // A lower id was found, so we must start filling from here
                $hits = 0;
                $value = $id;
            }
        }

        // At this point, all slots must contain either our own id or a higher id
        foreach ($this->slots as &$value) {
            $diff = $value - $id;
            if ($diff > 0) {
                // A higher id thread has entered the race, back off
                $this->clearSlots($sequence, $id);
                return $value;
            } elseif ($diff < 0) {
                // A lower id thread somehow has begun writing, which should be virtually
                // impossible.
                throw new RuntimeException("A lower id thread was able to insert itself");
            }
        }

        // Here we are certain we have the lock

        return $id;
    }

    /**
     * Creates a self executing function which will be invoked when garbage
     * collected, unless it has already been invoked.
     * 
     * @param int $lockId 
     * @return Closure 
     */
    private function makeReleaseFunction(int $lockId): Closure {
        $innerFunc = function() use ($lockId) {
            foreach ($this->slots as &$value) {
                if ($value === $lockId) {
                    $value = 0;
                }
            }
        };

        $releaseObject = new class($innerFunc) {
            private bool $didRelease = false;
            public function __construct(private Closure $releaseFunc) {}
            public function __invoke() {
                if (!$this->didRelease) {
                    $this->didRelease = true;
                    ($this->releaseFunc)();
                }
            }
            public function __destruct() {
                $this->__invoke();
            }
        };

        return static function() use ($releaseObject) {
            $releaseObject();
        };
    }

    private function wait(): void {
        \usleep(\mt_rand(0, 1000));
    }

    private function clearSlots(array $sequence, int $id) {
        foreach ($sequence as $offset) {
            $value = &$this->slots[$offset];
            if ($value === $id) {
                $value = 0;
            }
        }
    }

    /**
     * Generate a unique sequence of slots we'll try to collect.
     * The unique order helps greatly increase the probability
     * of detecting a conflict before the reverse pass.
     * 
     * @return array 
     */
    private function getSequence(): array {
        $sequence = range(0, $this->slotCount - 1);
        shuffle($sequence);
        return $sequence;
    }
}
