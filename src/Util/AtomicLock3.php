<?php
namespace Fubber\Util;

use Closure;

/**
 * Frode Børlis locking algorithm. This algorithm enables locking with extremely
 * small probability of collisions. Only threads that are unable to acquire a
 * lock will sleep, making the algorithm fast.
 * 
 * @package Fubber\Util
 */
class AtomicLock3 {

    /**
     * These slots store the id of the lock owner, or 0 if the slot is available.
     * 
     * @var int[]
     */
    private array $slots;
    private int $slotCount;
    private int $threadCount;

    public function __construct(int $threadCount = 64, int $slotCount = 20) {
        $this->threadCount = $threadCount;
        $this->slotCount = $slotCount;
        $this->slots = array_fill(0, $this->slotCount, 0);
    }

    public function acquire(int $threadId): Closure {
        $sequence = self::getSequence();
        while (true) {
            $myId = $this->generateId($threadId);
            $lockId = $this->tryAcquire($sequence, $myId);
            if ($lockId === $myId) {
                return $this->makeReleaseFunction($myId);
            }
        }
    }

    public function tryAcquire(array $sequence, int $id): int {
        /**
         * Each thread will write its id in every slot according
         * to a random sequence. If it encounters a slot with a higher
         * value, it leaves the race and must try again. The race
         * is completed when the thread has traversed all slots for
         * the second time without any other threads overwriting the
         * slot. If locked, this method will always encounter a slot
         * having PHP_INT_MAX which is greater than any possible $id
         * the thread may have and abort.
         */
        $hits = 0;
        $i = 0;
        for ($i = 0; $hits < $this->slotCount; $i++) {
            $offset = $sequence[$i % $this->slotCount];
            $value = &$this->slots[$offset];
            $diff = $value - $id;
            if ($diff === 0) {
                ++$hits;
            } elseif ($diff > 0) {
                // A higher id was discovered, so we'll not be getting a lock
                $this->clearSlots($sequence, $id);
                return $id + $diff;
            } else {
                /**
                 * At this point we must start over increasing the $hits counter.
                 * There is a chance that a race condition occurred, in which
                 * another thread also overwrites $value - but the $hits counter
                 * ensures that we will confirm that our value is still in this
                 * slot before calling it a success.
                 * 
                 * Further, the probability that the other thread writes in exactly
                 * the same order is extremely small `1 / ($this->slotCount)!`, so
                 * we are very likely to avoid a race condition with this particular
                 * other thread for the next slot.
                 */
                $hits = 0;
                $value = $id;
            }
        }

        /**
         * Very few threads should reach this final stage. Here the thread will
         * attempt to write PHP_INT_MAX in every slot, but if any of the slots
         * already contains PHP_INT_MAX, the thread will abort. If multiple threads
         * reach this stage, chances are quite high that both threads will abort
         * and must start over again with a new race.
         */
        $slotsToRestore = [];
        foreach ($sequence as $i => $offset) {
            $value = &$this->slots[$offset];
            if ($value === PHP_INT_MAX) {
                /**
                 * This slot has been taken
                 */
                $this->clearSlots($sequence, $id);

                /**
                 * Ensure we don't leave PHP_INT_MAX in any slots
                 */
                for ($j = 0; $j < $i; $j++) {
                    $this->slots[$sequence[$j]] = 0;
                }
                                
                $this->wait();
                return PHP_INT_MAX;
            } else {
                $slotsToRestore[] = $offset;
                $value = PHP_INT_MAX;
            }
        }

        return $id;
    }

    /**
     * Creates a self executing function which will be invoked when garbage
     * collected, unless it has already been invoked.
     * 
     * @param int $lockId 
     * @return Closure 
     */
    private function makeReleaseFunction(int $lockId): Closure {
        $innerFunc = function() use ($lockId) {
            foreach ($this->slots as &$value) {
                if ($value === PHP_INT_MAX) {
                    $value = 0;
                }
            }
        };

        $releaseObject = new class($innerFunc) {
            private bool $didRelease = false;
            public function __construct(private Closure $releaseFunc) {}
            public function __invoke() {
                if (!$this->didRelease) {
                    $this->didRelease = true;
                    ($this->releaseFunc)();
                }
            }
            public function __destruct() {
                $this->__invoke();
            }
        };

        return static function() use ($releaseObject) {
            $releaseObject();
        };
    }

    private function wait(): void {
        \usleep(\mt_rand(0, 1000));
    }

    private function generateId(int $threadId): int {
        $base_id = \mt_rand(0, \PHP_INT_MAX) % $this->threadCount;
        return $base_id * $this->threadCount + $threadId;
    }

    private function clearSlots(array $sequence, int $id) {
        foreach ($sequence as $offset) {
            $value = &$this->slots[$offset];
            if ($value === $id) {
                $value = 0;
            }
        }
    }

    /**
     * Generate a unique sequence of slots we'll try to collect.
     * The unique order helps greatly increase the probability
     * of detecting a conflict before the reverse pass.
     * 
     * @return array 
     */
    private function getSequence(): array {
        $sequence = range(0, $this->slotCount - 1);
        shuffle($sequence);
        return $sequence;
    }
}