<?php
namespace Fubber\Util;

use ReflectionAttribute;
use ReflectionClass;
use ReflectionMethod;

class AttributeTool {

    protected readonly ReflectionClass $reflectionClass;
    /** @var ReflectionMethod[] */
    protected readonly array $methods;

    public function __construct(
        protected readonly object $source
    ) {
        $this->reflectionClass = new ReflectionClass($source);
        $this->methods = $this->reflectionClass->getMethods();
    }

    /**
     * Finds all methods that has an attribute attached. If an `$attributeName`
     * is provided, then only methods that have an attribute with a matching name
     * will be returned.
     * 
     * @param string|null $attributeName 
     * @return iterable 
     */
    public function getMethodsWithAttributes(string $attributeName=null): iterable {
        foreach ($this->methods as $rm) {
            $attributes = $rm->getAttributes($attributeName);
            if (empty($attributes)) {
                continue;
            }
            yield $rm->getName() => $attributes;
        }
    }
}