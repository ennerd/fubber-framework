<?php
namespace Fubber\Util;

use Fubber\Service;
use PDO;

trait OptionsTrait {
    // You must declare the following properties on your object
    // protected static $_optionsTable = 'user_options'
    // protected static $_optionsFK = 'user_id';
    private $optionsCache = [];

    public function getOption($name) {
        static $stmt;

        if (isset($this->optionsCache[$name]) && $this->optionsCache[$name][0] < microtime(true)) {
            return unserialize($this->optionsCache[1]);
        }

        if(!$stmt) {
            $db = Service::use(PDO::class);
            $stmt = $db->prepare('SELECT value FROM '.static::$_optionsTable.' WHERE '.static::$_optionsFK.'=? AND name=? LIMIT 1');
        }

        if($stmt->execute([$this->id, $name])) {
            $res = $stmt->fetchColumn();
            if($res) {
                $this->optionsCache[$name] = [microtime(true) + 0.2, $res];
                return unserialize($res);
            }
        }
        $this->optionsCache[$name] = [microtime(true) + 0.1, null];
        return null;
    }

    public function setOption($name, $value) {
        unset($this->optionsCache[$name]);
        $value = serialize($value);
        $db = Service::use(PDO::class);
//        $db = \Nerd\Glue::get(\PDO::class);
        $stmt = $db->prepare('INSERT INTO '.static::$_optionsTable.' ('.static::$_optionsFK.', name, value) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value=?');
        $result = $stmt->execute([$this->id, $name, $value, $value]);
        if ($result) {
            $this->optionsCache[$name] = [microtime(true) + 0.2, $value];
        }
        return $result;
    }

    public function deleteOption($name) {
        $this->optionsCache[$name] = [microtime(true) + 0.1, null];
        $db = Service::use(PDO::class);
//        $db = \Nerd\Glue::get(\PDO::class);
        $stmt = $db->prepare('DELETE FROM '.static::$_optionsTable.' WHERE '.static::$_optionsFK.'=? AND name=?');
        return $stmt->execute([$this->id, $name]);
    }
}
