<?php
namespace Fubber\Util;

use Closure;
use Traversable;

/**
 * A set of utility functions which expect a generator function to process data.
 * 
 * @package Fubber\Util
 */
class GeneratorTool {

    /**
     * Filter, reduce or grow an array by providing a generator for each `$key => value`-pair
     * in a source iterable.
     * 
     * Example: Remove all elements with a value < 2
     * 
     * ```php
     * $result = GeneratorTool::process(
     *     function($iterable) {
     *         foreach ($iterable as $key => $value) {
     *             if ($value < 2) {
     *                 continue;
     *             }
     *             yield $value;
     *         }
     *     },
     *     [ 0, 1, 2, 3, 4, 5 ]
     * );
     * // result [ 2, 3, 4, 5 ]
     * ```
     * 
     * Example: Recursively make all values lowercase
     * 
     * ```php
     * $result = GeneratorTool::process(
     *     function($iterable, $recurseFunction) {
     *         foreach ($iterable as $key => $value) {
     *             if (\is_iterable($value)) {
     *                 $value = $recurseFunction($value);
     *             } elseif (\is_string($value)) {
     *                 $value = \strtolower($value);
     *             }
     *             yield $key => $value;
     *         }
     *     },
     *     [ 'text' => 'Lowercase This', 'recursively' => [ 'And THIS', 'AND THAT' ]]
     * );
     * // result [ 'text' => 'lowercase this', 'recursively' => [ 'and this', 'and that' ]]
     * ```
     * 
     * Example: Repeat all values 
     * 
     * @param Closure $generatorFunction `function(mixed $value, string|int $key, Callable $recurseFunction): iterable`
     * @param array $source 
     * @return array 
     */
    public static function process(Closure $generatorFunction, iterable $source): Traversable {
        $recurseFunction = function(iterable $source) use ($generatorFunction) {
            return self::process($generatorFunction, $source);
        };
        yield from $generatorFunction((function(iterable $source): iterable {
            yield from $source;
        })($source), $recurseFunction);
    }

    public static function processToArray(Closure $generatorFunction, iterable $source): array {
        $result = self::process($generatorFunction, $source);
        return \iterator_to_array($result);
    }

    /**
     * Run the generator once for every element in the source iterable. The generator yields
     * the new values using `yield $key => $value` or `yield $value`.
     * 
     * @param Closure $callback 
     * @param iterable $source 
     * @param mixed $initial 
     * @return iterable 
     */
    public static function reduce(Closure $callback, iterable $source): iterable {
        foreach ($source as $key => $value) {
            yield from $callback($value, $key);
        }
    }

    /**
     * See {self::reduce()}. Returns an array instead of an iterable.
     * 
     * @param Closure $callback 
     * @param iterable $source 
     * @param mixed $initial 
     * @return array 
     */
    public static function reduceToArray(Closure $callback, iterable $source): array {
        return \iterator_to_array(self::reduce($callback, $source));
    }

}
