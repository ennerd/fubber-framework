<?php
namespace Fubber\Util;

use ArrayAccess;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use JsonSerializable;
use OutOfBoundsException;
use ReflectionClass;
use ReflectionProperty;
use Serializable;
use Throwable;
use Traversable;
use TypeError;

/**
 * A helper class for accepting structured arrays and converting them to objects. Extend
 * this class and declare typed properties.
 * 
 * Properties can be declared `public`, `protected` and `private`. Only properties that
 * are declared `public` are accessible via the `ArrayAccess` interface.
 * 
 * Static properties are ignored. Example properties with descriptions:
 * 
 * ```
 * public bool $required_bool;      // This property is required because the type does not allow `null`
 * public ?bool $optional_bool;
 * public array $any_array;         // There will be no type checking of this array
 * public AnotherArrayStruct $sub;  // Accepts array, AnotherArrayStruct, Traversable or JsonSerializable.
 * public bool|int $boolOrInt;      // Union types are supported.
 * ```
 * 
 */
abstract class ArrayStruct implements ArrayAccess, IteratorAggregate, Serializable, Countable, JsonSerializable {

    /**
     * Caches property maps for each array struct type that exists.
     */
    private static array $propertyMaps = [];

    /**
     * Holds attributes for {@see self::setAttribute()} and {@see self::getAttribute()}
     */
    private array $_attributes_ = [];

    /**
     * Create a StructArray from an array, or clone an existing DataObject
     * 
     * @param array|Traversable|JsonSerializable $object 
     * @param bool $ignoreUnknownKeys If false, any keys in the array which don't map to a property will be ignored.
     * @return static 
     */
    public static function createFrom(array|Traversable|JsonSerializable $object, bool $ignoreUnknownKeys=false) {
        if ($object instanceof static) {
            return clone $object;
        }
        return new static($object, $ignoreUnknownKeys);
    }

    /**
     * Constructor
     * 
     * @param array|Traversable|JsonSerializable $value 
     * @param bool $ignoreUnknownKeys 
     * @throws InvalidArgumentException 
     * @throws TypeError 
     * @throws Throwable 
     */
    public final function __construct(array|Traversable|JsonSerializable $value=[], bool $ignoreUnknownKeys=false) {
        if (\is_array($value) || $value instanceof Traversable) {
            $src = $value;
        } elseif ($value instanceof JsonSerializable) {
            $src = $value->jsonSerialize();
        } else {
            throw new InvalidArgumentException("Expected `" . static::class . "` or array or Traversable but received `" . \get_debug_type($value) . "`");
        }
        $types = static::getProperties();
        foreach ($src as $key => $subVal) {

            if (!isset($types[$key])) {
                if ($ignoreUnknownKeys) {
                    continue;
                }
                throw new InvalidArgumentException("Unknown property `" . $key . "`");
            }

            try {
                /**
                 * Type checking is done by PHP, and if it fails we'll see if it is a nested ArrayStruct
                 */
                $this->$key = $subVal;
                unset($types[$key]);
            } catch (\TypeError $e) {                                
                if (
                    (\is_array($subVal) || $subVal instanceof Traversable | $subVal instanceof JsonSerializable) && 
                    ($to = $types[$key]->getType())
                ) {
                    // This might be a nested ArrayStruct
                    $types = \explode("|", (string) $to);
                    foreach ($types as $prop) {
                        if (\is_subclass_of($prop, self::class)) {
                            try {
                                $val = $prop::createFrom($subVal, $ignoreUnknownKeys);
                                $this->$key = $val;
                                unset($types[$key]);
                                continue 2;
                            } catch (\TypeError $se) {
                                // It wasn't an ArrayStruct
                            }
                        }
                    }
                }
                throw $e;
            }
        }

        /**
         * Handle any uninitialized properties that are required
         */
        foreach ($types as $key => $prop) {
            if (!$prop->isInitialized($this)) {
                if ($type = $prop->getType()) {
                    if ($type->allowsNull()) {
                        $this->$key = null;
                        unset($types[$key]);
                        continue;
                    }
                }
                throw new InvalidArgumentException("Required property `" . $key . "` not set");
            }
        }

        /**
         * Perform any custom validation
         */
        $this->validate();
    }

    /** 
     * Override this method to implement validation. Throw `DomainException`, `UnexpectedValueException`,
     * `ValueError` or similar to describe the validation error.
     * 
     * @throws Throwable Throws exception if any of the values are invalid.
     * @override
     */
    public function validate(): void {}

    /**
     * Get an attribute value
     * 
     * @param string $key The attribute name
     * @return mixed The attribute value or null if the attribute does not exist
     */
    public function getAttribute(string $key) {
        return $this->_attributes_[$key] ?? null;
    }

    /**
     * Set an attribute value
     * 
     * @param string $key The attribute name
     * @param mixed $value 
     * @return $this
     */
    public function setAttribute(string $key, mixed $value) {
        $this->_attributes_[$key] = $value;
        return $this;
    }

    /**
     * Does an attribute exist?
     * 
     * @param string $key The attribute name
     * @return bool True if the attribute exists
     */
    public function hasAttribute(string $key): bool {
        return isset($this->_attributes_[$key]) || \array_key_exists($key, $this->_attributes_);
    }

    /**
     * Get all attributes that are declared as an array.
     * 
     * @return array<string, mixed>
     */
    public function getAttributes(): array {
        return $this->_attributes_;
    }

    /**
     * How many public members does this object have
     * 
     * @return int
     */
    public final function count(): int {
        $count = 0;
        foreach (static::getProperties() as $property) {
            if ($property->isPublic()) {
                ++$count;
            }
        }
        return $count;
    }

    /**
     * Does the member name exist?
     * 
     * @param string $offset
     * @return bool
     */
    public final function offsetExists(mixed $offset): bool {
        $properties = static::getProperties();
        return isset($properties[$offset]) && $properties[$offset]->isPublic();
    }

    /**
     * Get the value of a member
     * 
     * @param string $offset 
     * @return mixed 
     * @throws OutOfBoundsException 
     */
    public final function offsetGet(mixed $offset): mixed {
        $this->assertOffsetIsPublic($offset);
        return $this->offsetExists($offset) ? $this->$offset : null;
    }

    /**
     * Set the value of a member
     * 
     * @param string $offset
     * @param mixed $value
     * @throws OutOfBoundsException if the offset is not defined
     * @throws TypeError if the value is not allowed for the type of this property
     * @throws Throwable if the value is of the correct type, but has an illegal value
     */
    public final function offsetSet(mixed $offset, mixed $value): void {
        $this->assertOffsetIsPublic($offset);

        $oldValue = $this->$offset;
        $this->$offset = $value;
        try {
            $this->validate();
        } catch (\Throwable $e) {
            $this->$offset = $oldValue;
            throw $e;
        }
    }

    /**
     * Set a value of a member to null.
     * 
     * @throws OutOfBoundsException if the offset is not defined
     * @throws TypeError if the member does not allow null values
     */
    public final function offsetUnset(mixed $offset): void {
        $this->assertOffsetIsPublic($offset);
        $this->$offset = null;
        $this->validate();
    }

    /**
     * Ensure that any ArrayStruct members are also cloned. This does
     * not clone ArrayStruct members that are stored inside array members.
     */
    public final function __clone() {
        foreach (static::getProperties() as $name => $p) {
            if ($this->$name instanceof ArrayStruct) {
                $this->$name = clone $this->$name;
            }
        }
    }

    /**
     * Do not allow adding properties that have not been declared explicitly.
     * To store extra information the {@see self::setAttribute()} method should
     * be used.
     * 
     * @param mixed $name 
     * @param mixed $value 
     * @return never 
     * @throws OutOfBoundsException 
     */
    public final function __set($name, $value) {
        throw new OutOfBoundsException("The property `$name` is not declared");
    }

    /**
     * Handle serialization properly, including attributes.
     */
    public final function serialize(): ?string {
        $result = [];
        foreach (static::getProperties() as $name => $prop) {
            $result[$name] = $this->$name;
        }
        $result["\0attrs"] = $this->_attributes_;
        return \serialize($result);
    }

    /**
     * Handle deserialization of the attributes properly.
     */
    public final function unserialize(string $data): void {
        foreach (\unserialize($data) as $key => $value) {
            if ($key === "\0attrs") {
                $this->_attributes_ = $value;
            } else {
                $this->$key = $value;
            }
        }
        $this->validate();
    }

    /**
     * Check that an offset exists
     * 
     * @param string $offset 
     * @throws OutOfBoundsException 
     */
    private function assertOffsetIsPublic(string $offset): void {
        if (!$this->offsetExists($offset)) {
            throw new OutOfBoundsException("Property `$offset` is undefined");
        }
    }

    /**
     * Get the declared properties of the class.
     * 
     * @return array<string, ReflectionProperty>
     */
    protected static function getProperties(): array {
        if (isset(self::$propertyMaps[static::class])) {
            return self::$propertyMaps[static::class];
        }
        $result = [];
        foreach ((new ReflectionClass(static::class))->getProperties() as $prop) {
            if ($prop->isStatic()) {
                continue;
            }
            $result[$prop->getName()] = $prop;
        }
        return self::$propertyMaps[static::class] = $result;
    }

    /**
     * Check if the value is of the specified type.
     * 
     * @param mixed $value The value to check
     * @param null|string $type The type string
     * @param bool $strict If false, type checking will be relaxed to behave like typed arguments
     * @return bool 
     */
    protected static function isOfType(mixed $value, string $type='mixed', bool $strict=false) {
        if (\strpos($type, '|') !== false) {
            foreach (\explode('|', $type) as $subType) {
                if (static::isOfType($value, $subType, $strict)) {
                    return true;
                }
            }    
            return false;
        }

        if ($value === null && \substr($type, 0, 1) === '?') {
            return true;
        }

        if ($strict) {
            $lcType = \strtolower($type);
            $types = [ $type, $lcType ];
            if (\in_array(\gettype($value), $types)) {
                return true;
            }
            if (\function_exists('get_debug_type') && \in_array(\get_debug_type($value), $types)) {
                return true;
            }
            switch ($lcType) {
                case 'string': return \is_string($value);
                case 'int': return \is_int($value);
                case 'float': return \is_float($value) || \is_int($value);
                case 'bool': return \is_bool($value);
                case 'array': return \is_array($value);
                case 'iterable': return \is_iterable($value);
                case 'callable': return \is_callable($value);
                case 'true': return $value === true;
                case 'false': return $value === false;
            }
        }

        /**
         * Sanity check is important in case of eval being used below.
         */
        if (!\preg_match('/^\??[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $type)) {
            throw new InvalidArgumentException('Expecting a valid PHP type string, got `' . $type . '`');
        }

        try {
            eval('(function(' . $type . ' $var){})($value);');
        } catch (\TypeError $e) {
            return false;
        }
        return true;
    }

}