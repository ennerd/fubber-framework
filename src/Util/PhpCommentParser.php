<?php
namespace Fubber\Util;

/**
 * Extremely simple doc block parser
 */
class PhpCommentParser {

    public function __construct(array $options=[]) {
    }

    public function parse(string $string) {

        $res = (object) [
            'title' => null,
            'description' => null,
            'tags' => null,
        ];

        foreach (explode("\n", $string) as $line) {
            $line = trim($line, "\n\r\t */");
            if ($line === '' && $res->description === null) {
                continue;
            }
            if ($res->title === null) {
                $res->title = $line;
            } elseif ($line === '' || $line[0] !== '@') {
                $res->description .= $line."\n";
            }
        }
        $res->tags = iterator_to_array(static::getTokensFromString($string));
        $res->description = trim($res->description);
        return $res;
    }

    /**
     *
     */
    protected static function getTokensFromString(string $string): \Generator {

        \preg_match_all(
'/('.
    // match {@inline(tags) with stuff}
    '{(@[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff\.\-^\b]*)(\([^\)]*\))?([^}]*)?}'.
    '|'.
    // match @non-inline(tags) with stuff until end of line
    '((?<!{)@[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff\.\-^\b]*)(\([^\)]*\))?(^}[^\n@]*)?'.
')/m',
            $string,
            $matches
        );
        foreach ($matches[0] as $i => $match) {
            $tag = [
                'full' => $match,
            ];
            if ($matches[2][$i]) {
                $tag['type'] = 'inline-tag';
                $offset = 2;
            } else {
                $tag['type'] = 'normal';
                $offset = 5;
            }
            $tag['name'] = $matches[$offset][$i];

            if ($matches[$offset+1][$i] !== "") {
                $argString = '['.trim(substr($matches[$offset+1][$i], 1, -1)).']';
                $tag['args'] = \json_decode($argString, true);
                if ($tag['args'] === null) {
                    throw new \Fubber\RuntimeException("Unable to parse '$argString'. Remember to quote object keys, and don't use parenthesis inside your argument list.");
                }
/*
//                var_dump(json_decode($argString));die();
                // arguments
                $tag['args'] = array_map(function($s) {
                    return trim($s);
                }, explode(",", substr($matches[$offset+1][$i], 1, -1)));
*/
            } else {
                $tag['args'] = [];
            }
            if ($matches[$offset+2][$i] !== '') {
                \preg_match_all('|[\x21-\xFF]+|', $matches[$offset+2][$i], $tag['extra']);
            }
            yield (object) $tag;
        }
    }

}
