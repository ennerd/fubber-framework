<?php
namespace Fubber\Util;

use Closure;
use RuntimeException;

/**
 * Frodes Lock implementation
 * 
 * @package Fubber\Util
 */
class AtomicLock {

    /**
     * With 128 slots there are about 3.85e215 different sequences that we can
     * write in.
     */
    private const SLOT_COUNT = 128;

    /**
     * These slots store the id of the lock owner, or 0 if the slot is available.
     * 
     * @var int[]
     */
    private array $slots;

    public function __construct() {
        $this->slots = array_fill(0, self::SLOT_COUNT, 0);
    }

    

    /**
     * Returns null if a lock was not acquired, and a closure
     * which will release the lock when invoked or garbage
     * collected.
     * 
     * @return null|Closure 
     * @throws RuntimeException 
     */
    public function acquire(): ?Closure {
        if ($this->tryAcquire()) {
            return $this->makeReleaseFunction($myId);
        }
        return null;
    }

    private function tryAcquire(): bool {
        $lockId = \mt_rand(0, \PHP_INT_MAX);

        $sequence = $this->getSequence();
        for ($i = 0, $hits = 0; $hits < self::SLOT_COUNT; $i++) {
            $offset = $sequence[$i % self::SLOT_COUNT];
            $value = &$this->slots[$offset];
            $diff = $value - $lockId;
            if ($diff > 0) {
                $this->clearSlots($lockId);
                return false;
            } elseif ($diff < 0) {
                $value = $lockId;
                $hits = 0;
            } else {
                ++$hits;
            }
        }

        /**
         * We won the race, but there is a slim chance a contender has entered
         * with an even higher lock id - or that there is a condenter having the
         * same lock id, so let's rule that situation out. By using a unique order,
         * there is virtually zero probability that we don't notice the other thread.
         */
        foreach ($sequence as $offset) {
            $value = &$this->slots[$offset];
            if ($value < PHP_INT_MAX) {
                // we got the slot
                $value = PHP_INT_MAX;
            } else {
                // another thread took this slot before us
                $this->clearSlots($lockId);
                return false;
            }
        }

        /**
         * Now we are definitely the owner of the lock, but
         * in case another thread gave up in the second phase
         * and wrote zeros in any slots, we must spin write 
         * until all slots are PHP_INT_MAX.
         */
        do {
            $didChange = false;
            foreach ($this->slots as &$value) {
                if ($value !== $lockId) {
                    $didChange = true;
                }
                $value = PHP_INT_MAX;
            }
        } while ($didChange);

        return true;
    }

    /**
     * Creates a self executing function which will be invoked when garbage
     * collected, unless it has already been invoked.
     * 
     * @param int $lockId 
     * @return Closure 
     */
    private function makeReleaseFunction(int $lockId): Closure {
        $releaseObject = new class() {
            private bool $didRelease = false;
            public function __construct() {}
            public function __invoke() {
                if (!$this->didRelease) {
                    $this->didRelease = true;
                    ($this->releaseFunc)();
                }
            }
            public function __destruct() {
                $this->__invoke();
            }
        };

        return static function() use ($releaseObject) {
            $releaseObject();
        };
    }

    private function clearSlots(int $lockId): void {
        foreach ($this->slots as &$value) {
            if ($value === $lockId) {
                $value = 0;
            }
        }
    }

    private function releaseLock(): void {
        $this->clearSlots(PHP_INT_MAX);
    }

    /**
     * Generate a unique sequence of slots for the commit phase.
     * 
     * @return array 
     */
    private function getSequence(): array {
        $sequence = range(0, $this->slotCount - 1);
        shuffle($sequence);
        return $sequence;
    }
}
