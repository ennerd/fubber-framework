<?php
namespace Fubber\Util;

trait ProxyTrait {
    abstract protected function getProxiedObject();
    
    public function __set($name, $value) {
        $this->getProxiedObject->$name = $value;
    }
    
    public function __get($name) {
        return $this->getProxiedObject()->$name;
    }
    
    public function __isset($name) {
        return isset($this->getProxiedObject()->$name);
    }
    
    public function __unset($name) {
        unset($this->getProxiedObject()->$name);
    }
    
    public function __call($name, $arguments) {
        return call_user_func_array([$this->getProxiedObject(), $name], $arguments);
    }
    
    public static function __callStatic($name, $arguments) {
        return call_user_func_array([get_class($this->getProxiedObject()), $name], $arguments);
    }
}