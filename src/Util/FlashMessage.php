<?php
namespace Fubber\Util;

use Fubber\Kernel\State;

class FlashMessage {
    
    const SUCCESS = 'success';
    const POSITIVE = 'positive';
    const ERROR = 'error';
    const NEGATIVE = 'negative';
    const INFO = 'info';
    const WARNING = 'warning';
    const FLOATING = 'floating';
    
    public $message, $classNames;
    
    public function __construct(State $state, $message, $classNames=self::INFO) {
        $this->message = $message;
        $this->classNames = $classNames;
        $hash = md5(serialize([$message,$classNames]));
        
        $session = $state->session;
        $current = $session->flashMessages;
        if(!is_array($current)) $current = [];
        $current[$hash] = $this;
        if(sizeof($current)>0)
            $session->flashMessages = $current;
    }
    
    public static function getMessages(State $state) {
        $session = $state->session;
        $current = $session->flashMessages;
        if($current && sizeof($current) > 0) {
            $state->response->privateCache(4);
            $session->flashMessages = [];
            if(!$current) return [];
            return array_reverse($current);
        }
        return [];
    }
    
    public static function getMessagesArray(State $state) {
        $messages = static::getMessages($state);
        $result = [];
        foreach($messages as $m) {
            $result[] = [
                "message" => $m->message,
                "classNames" => $m->classNames
                ];
        }
        return $result;
    }
}
