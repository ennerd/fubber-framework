<?php
namespace Fubber\Util;

use Fubber\CodingErrorException;

/**
* This class provides validation functionality on objects and arrays.
*
* Example:
*
* <code>
* <?php
* $errors = new Errors($array);			// $array is a set of key-value pairs. However, the argument is optional
*
* $errors->password('pass');			// Requires that the 'pass' property is at minimum 8 characters long and contains one number and one character
* $errors->required('username');			// The 'username' property is required
* $errors->minLen('username', 3');		// The 'username' property must be at least 3 characters
* $errors->email('email');			// The 'email' property must be a proper e-mail address
*
* if($errors->pass != $errors->passConfirm)
*	$errors->addError('passConfirm', 'Passwords do not match');
*
* var_dump($errors->isInvalid());			// Prints NULL or an array of errors, where the key is the property name
* ?>
* </code>
*/
class Errors {
	protected $_errors = array();
	protected $_val = NULL;

	/**
	*	@param array|object|null $val Pre-initialize the values.
	*/
	public function __construct($val = null) {
		if(!$val)
			$this->_val = array();
		else
			$this->_val = $val;
	}

	public function __get($fieldname) {
		if(is_array($this->_val)) {
			if(!isset($this->_val[$fieldname])) return null;
			return $this->_val[$fieldname];
		}
		else if(is_object($this->_val)) {
			if(!isset($this->_val->$fieldname)) return null;
			return $this->_val->$fieldname;
		} else
			return NULL;
	}

	public function __isset($fieldname) {
		if(is_array($this->_val)) {
			return isset($this->_val[$fieldname]);
		} else if(is_object($this->_val)) {
			return !!($this->_val->$fieldname);
		} else {
			return false;
		}
	}

	/**
	*	@param string $fieldname
	*	@return bool
	*/
	public function hasError($fieldname) {
		return isset($this->_errors[$fieldname]);
	}

	/**
	*	Add a custom validation error
	*
	*	@param string $fieldname
	*	@param string $error
	*	@return $this
	*/
	public function addError($fieldname, $error) {
		$this->_errors[$fieldname] = $error;
		return $this;
	}
	
	public function addErrors($errors) {
		if($errors)
			foreach($errors as $field => $value)
				$this->addError($field, $value);
		return $this;
	}
	
	public function removeError($fieldname) {
		unset($this->_errors[$fieldname]);
		return $this;
	}
	
	public function removeErrors(array $fieldnames) {
		if($fieldnames)
			foreach($fieldnames as $fieldname)
				$this->removeError($fieldname);
		return $this;
	}

	/**
	*	Check if there are any validation errors
	*
	*	@return array|false
	*/
	public function isInvalid(): ?array {
		if(sizeof($this->_errors)===0) return null;
		return $this->_errors;
	}
	
    public function trimmed($fieldName) {
        if (empty($this->getValue($fieldName))) return $this;

		$value = $this->getValue($fieldName);

        if (trim($value) != $value) {
            return $this->addError($fieldName, "Remove leading or trailing spaces");
        }

        return $this;
    }

    /**
     *  Field must be a valid phone number
     */
    public function phone($fieldname) {
        if (empty($this->getValue($fieldname))) {
            return $this;
        }
        $value = $this->getValue($fieldname);

        if (trim($value) !== $value) {
            return $this->addError($fieldname, 'Remove leading or trailing whitespace');
        }

        if (trim($value, '0123456789()+- ')) {
            return $this->addError($fieldname, 'Only use digits, spaces and parenthesis in phone numbers');
        }

        if ($value[0]==='+' && trim(substr($value, 1), '0123456789()- ')) {
            return $this->addError($fieldname, 'Invalid phone number');
        }

        if (
            strpos($value, '--') !== false ||
            strpos($value, '  ') !== false
        ) {
            return $this->addError($fieldname, 'Invalid phone number');
        }

        foreach (['(', ')', '+'] as $char) {
            if (substr_count($value, $char) > 1) {
                return $this->addError($fieldname, 'Invalid phone number');
            }
        }

        $l = strpos($value, '(');
        $r = strpos($value, ')');
        if ($l !== $r) {
            return $this->addError($fieldname, 'Invalid phone number');
        } elseif ($l && $l > $r) {
            return $this->addError($fieldname, 'Invalid phone number');
        }

        return $this;
    }

	/**
	 * Class name validator. Checks that the specified class is defined and that it extends
	 * a specified parent class.
	 */
	public function className($fieldname, $className) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);		
		if(!is_string($value))
			return $this->addError($fieldname, "Not a valid class name");
		if(!class_exists($value))
			return $this->addError($fieldname, "Not found");
		if(!is_a($value, $className, true))
			return $this->addError($fieldname, "Not found");
		return $this;
	}

    public function json($fieldname) {
        $value = $this->getValue($fieldname);
        if (empty($value)) return $this;

        try {
            json_decode($value, null, 512, JSON_THROW_ON_ERROR);
        } catch (\Throwable $e) {
            return $this->addError($fieldname, "Invalid JSON");
        }

        return $this;
    }

	/**
	 * Slug validator. Makes sure that the string can be safely used as part of
	 * a URL.
	 */
	public function slug($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		$legalChars = 'abcdefghijklmnopqrstuvwxyz0123456789-_';
		if(trim($value) != $value)
			return $this->addError($fieldname, "A slug can't contain spaces");
		if(trim($value, '-_') != $value)
			return $this->addError($fieldname, "A slug can't begin or end with a dash or an underscore");
			
		for($i = strlen($value) -1; $i >= 0; $i--) {
			if(strpos($legalChars, $value[$i])===false)
				return $this->addError($fieldname, 'Illegal character used in slug');
		}
		
		return $this;
	}

	/**
	 * Identifier validator. Makes sure that the string can be safely used as a variable name
	 * in PHP or javascript.
	 * 
	 * a URL.
	 */
	public function identifier($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		$legalStartChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_';
		$legalChars = $legalStartChars.'0123456789';
		if(trim($value[0], $legalStartChars)!='')
			return $this->addError($fieldname, "A variable name can't start with this character.");
		if(trim($value, $legalChars)!='')
			return $this->addError($fieldname, "Only english characters, digits and _ is allowed.");

		return $this;
	}

	/**
	*	Password validator for field
	*
	*	@param string $fieldname
	*	@return $this
	*/
	public function password($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);

		if (strlen($value) < 8) return $this->addError($fieldname, 'Passwords must be 8 characters or more');
		if (!preg_match('#[0-9]+#', $value)) return $this->addError($fieldname, 'Password must contain a number');
		if (!preg_match('#[a-zA-Z]+#', $value)) return $this->addError($fieldname, 'Password must contain at least one character');
		return $this;
	}

	/**
	*	Minimum length validator for field
	*
	*	@param string $fieldname
	*	@param int $minLen
	*	@return $this
	*/
	public function minLen($fieldname, $minLen) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if(mb_strlen($value)<$minLen) $this->addError($fieldname, 'Minimum length is '.$minLen.' characters');
		return $this;
	}

	/**
	*	Minimum value validator for field
	*
	*	@param string $fieldname
	*	@param mixed $minVal
	*	@return $this
	*/
	public function minVal($fieldname, $minVal) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if($value < $minVal) $this->addError($fieldname, 'Minimum value is '.$minVal);
		return $this;
	}

	/**
	*	Maximum value validator for field
	*
	*	@param string $fieldname
	*	@param mixed $maxVal
	*	@return $this
	*/
	public function maxVal($fieldname, $maxVal) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if($value > $maxVal) $this->addError($fieldname, 'Maximum value is '.$maxVal);
		return $this;
	}

	/**
	*	Maximum length validator for field
	*
	*	@param string $fieldname
	*	@param int $maxLen
	*	@return $this
	*/
	public function maxLen($fieldname, $maxLen) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if(strlen($value)>$maxLen) $this->addError($fieldname, 'Maximum length is '.$maxLen.' characters');
		return $this;
	}

	/**
	*	Field must not be empty
	*
	*	@param string $fieldname
	*	@return $this
	*/
	public function required($fieldname) {
		$value = $this->getValue($fieldname);
		if(trim($value)=='') $this->addError($fieldname, 'Required');
		return $this;
	}

	/**
	*	Field must not be NULL
	*
	*	@param string $fieldname
	*	@return $this
	*/
	public function notNull($fieldname) {
		if($this->getValue($fieldname) === NULL)
			$this->addError($fieldname, 'Value must not be NULL');
		return $this;
	}

	/**
	*	Field must be a valid e-mail address
	*
	*	@param string $fieldname
	*	@return $this
	*/
	public function email($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if(!filter_var($value, FILTER_VALIDATE_EMAIL))
			$this->addError($fieldname, 'Invalid e-mail address');
		return $this;
	}

	/**
	*	Field must be one of the supplied values
	*
	*	@param string $fieldname
	*	@param array $values
	*	@return $this
	*/
	public function oneOf($fieldname, array $values) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if(!in_array($value, $values))
			$this->addError($fieldname, 'Illegal value');
		return $this;
	}
	
	public function url($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if(filter_var($value, FILTER_VALIDATE_URL) === false)
			$this->addError($fieldname, 'Invalid url');
		return $this;
	}
	
	public function integer($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if($value === "0")
			return $this;
		if(!is_numeric($value) || intval($value) != $value)
			$this->addError($fieldname, 'Not an integer');
		return $this;
	}
	
	public function float($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if($value === "0")
			return $this;
		if(!is_numeric($value) || floatval($value) != $value)
			$this->addError($fieldname, 'Not an decimal number');
		return $this;
	}

	public function date($fieldname) {
        if (empty($this->getValue($fieldname))) return $this;

        $value = $this->getValue($fieldname);

        if (trim($value, '0123456789-') !== '') {
            return $this->addError($fieldname, 'Illegal characters in date');
        }

        if (date('Y-m-d', strtotime($value)) !== $value) {
            return $this->addError($fieldname, 'Invalid date format. Use YYYY-MM-DD');
        }

        return $this;
    }


	public function datetime($fieldname) {
		if(empty($this->getValue($fieldname))) return $this;
		$value = $this->getValue($fieldname);
		if(trim($value, '0123456789- :')!=='') {
			$this->addError($fieldname, 'Illegal characters in date');
		} else {
			if(date('Y-m-d H:i:s', strtotime($value)) != $value)
				$this->addError($fieldname, 'Date must be in YYYY-MM-DD HH:II:SS format');
		}
		return $this;
	}
	
	public function foreignKey(string $fieldname, string $className, $key='id') {
        $value = $this->getValue($fieldname);
        if ($value === null) {
            return $this;
        }
        if (!is_a($className, \Fubber\Table\IBasicTable::class, true)) {
            throw new CodingErrorException(static::class."::foreignKey() can only test foreign key on classes that implement 'Fubber\Table\IBasicTable'. '$className' received.");
        }
		$count = $className::allUnsafe()->where($key,'=',$this->getValue($fieldname))->count();
		if($count < 1) {
            if ($value === '') {
                // Special case informative for developer
                $this->addError($fieldname, "Value '' not found");
            } else {
    			$this->addError($fieldname, 'Value not found');
            }
        }
		return $this;
	}
	
	/**
	 * Checks if the field is unique in the table - ignoring the row with primary key = $exceptPrimaryKey
	 */
	public function unique(string $fieldname, string $className, $exceptPrimaryKey=null) {
		if(empty($this->getValue($fieldname))) return $this;
        $pk = $className::getPrimaryKeyNames();
		if(isset($pk[1])) {
			throw new CodingErrorException("Can't use complex primary keys on unique validator yet.");
		}
        $pk = $pk[0];


		$row = $className::allUnsafe()->where($fieldname,'=',$this->getValue($fieldname))->one();
		if(!$row) {
			return $this;
        }

		if ($exceptPrimaryKey === null) {
			$this->addError($fieldname, 'Already exists');
			return $this;
		} elseif (gettype($row->$pk) !== gettype($exceptPrimaryKey)) {
			$this->addError($fieldname, 'Coding error: primary key is of type '.gettype($row->$pk).' but testing for type primary key of type '.gettype($exceptPrimaryKey));
			return $this;
		}
	
		if($row->$pk === $exceptPrimaryKey) {
			return $this;
        }
		$this->addError($fieldname, 'Already exists');
		return $this;
	}
	
	/**
	 * Checks if the field is unique in the table - ignoring the row with primary key = $exceptPrimaryKey
	 */
	public function groupedUnique($fieldname, $className, $groupColumn, $groupValue, $exceptPrimaryKey) {
		if(empty($this->getValue($fieldname))) return $this;
		$pk = $className::getPrimaryKeyNames();
		if(isset($pk[1])) {
			throw new CodingErrorException("Can't use complex primary keys on unique validator yet.");
		}
        $pk = $pk[0];

		$row = $className::allUnsafe()->where($groupColumn,'=',$groupValue)->where($fieldname,'=',$this->getValue($fieldname));
		if($exceptPrimaryKey)
			$row->where($pk,'<>',$exceptPrimaryKey);
		$row = $row->one();
		if(!$row)
			return $this;
		$this->addError($fieldname, 'Already exists');
		return $this;
	}
	
	protected function getValue($name) {
		$parts = explode("|", $name);
		$name = $parts[0];
		$root = $this->$name;
		for($i = 1; $i < sizeof($parts); $i++) {
			$name = $parts[$i];
			if(is_array($root)) {
				if(!isset($root[$name]))
					return null;
				else
					$root = $root[$name];
			} else if(is_object($root)) {
				if(!isset($root->$name))
					return null;
				else
					$root = $root->$name;
			}
		}
		return $root;
	}
}
