<?php
namespace Fubber\Util;

use RuntimeException;
use SplFileInfo;

final class Path extends SplFileInfo {

    public function __construct(string $path) {
        // Normalize the path to remove any inconsistencies
        parent::__construct(static::normalize($path));
        $this->setInfoClass(static::class);
    }

    // Normalize path separators to DIRECTORY_SEPARATOR
    private static function normalize(string $path): string {
        return preg_replace('#[/\\\\]+#', DIRECTORY_SEPARATOR, $path);
    }

    private static function resolvePath(string $path): string {
        $parts = [];
        $isAbsolute = self::isAbsolutePath($path);
    
        foreach (explode(DIRECTORY_SEPARATOR, $path) as $part) {
            if ($part === '' || $part === '.') {
                continue;
            }
            if ($part === '..') {
                if (!empty($parts) && end($parts) !== '..') {
                    array_pop($parts);
                } elseif (!$isAbsolute) {
                    // Only add '..' if we're not at root
                    $parts[] = '..';
                }
                // If we're at root and see '..', ignore it
            } else {
                $parts[] = $part;
            }
        }
    
        $resolved = implode(DIRECTORY_SEPARATOR, $parts);
        return $isAbsolute 
            ? DIRECTORY_SEPARATOR . $resolved 
            : $resolved;
    }
        
    // Check if the path is absolute
    private static function isAbsolutePath(string $path): bool {
        return preg_match('#^(?:[A-Z]:|\\\\{2}[^\\\\/]+\\\\[^\\\\/]+|/).#i', $path) === 1;
    }

    public function getContents(): string {
        $result = \file_get_contents($this);
        if ($result === false) {
            throw new RuntimeException("Unable to read file");
        }
        return $result;
    }

    public function isRelative(): bool {
        return !static::isAbsolutePath($this->getPathname());
    }
    
    // Add method to get absolute path (different from realpath())
    public function absolute(): Path {
        if ($this->isRelative()) {
            return new static(getcwd() . DIRECTORY_SEPARATOR . $this->getPathname());
        }
        return $this;
    }
    
    public function navigate(string $relativePath): static {
        return static::join($this, $relativePath);
    }
    
    public function dirname(int $levels=1): Path {
        return new static(dirname($this->__toString(), $levels));
    }

    public function exists(): bool {
        return file_exists($this);
    }

    public function realpath(): ?Path {
        $rp = $this->getRealPath();
        if ($rp === false) {
            return null;
        }
        return new static($rp);
    }

    // Joins paths like path.join in Node.js
    public static function join(...$segments): Path {
        $finalPath = '';
        foreach ($segments as $segment) {
            if (substr($segment, 0, 1) === DIRECTORY_SEPARATOR) {
                // If segment is an absolute path, reset the finalPath
                $finalPath = $segment;
            } else {
                // Concatenate maintaining the DIRECTORY_SEPARATOR
                $finalPath = rtrim($finalPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim($segment, DIRECTORY_SEPARATOR);
            }
        }
        // Normalize and resolve any navigational cues in the final path
        $finalPath = static::resolvePath($finalPath);
        return new static($finalPath);
    }

    // Resolves paths to an absolute path like path.resolve in Node.js
    public static function resolve(...$segments): Path {
        $resolvedPath = '';
        foreach ($segments as $segment) {
            if (static::isAbsolutePath($segment)) {
                // Reset the path if the segment is an absolute path
                $resolvedPath = $segment;
            } else {
                // Otherwise append the segment
                $resolvedPath .= DIRECTORY_SEPARATOR . $segment;
            }
        }
        // Normalize and resolve the final path to make sure it's absolute
        $resolvedPath = static::resolvePath($resolvedPath);
        return new static($resolvedPath);
    }

    public static function from(self|string $path): static {
        if ($path instanceof static) {
            return $path;
        }
        return new static($path);
    }
}
