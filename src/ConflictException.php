<?php
namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when conflicting settings or
 * parameters are detected.
 */
class ConflictException extends RuntimeException {

    public function getDefaultStatus(): array {
        return [400, "Conflict"];
    }


    public function getExceptionDescription(): string {
        return "Conflict";
    }
}
