<?php
namespace Fubber\AssetManager;

use Composer\Semver\VersionParser;
use Fubber\AssetManager\LogicException as AssetManagerLogicException;
use Fubber\AssetManager\Resolver\AssetAnnotations;
use Fubber\AssetManager\Resolver\DependencyAnnotations;
use Fubber\LogicException;
use Fubber\ObjectAnnotator\ObjectAnnotator;
use IteratorAggregate;
use Throwable;
use Traversable;
use WeakMap;

/**
 * Logic for resolving dependencies between assets and asset-dependencies.
 * 
 * @internal
 * 
 * @package Fubber\AssetManager
 */
class Resolver implements IteratorAggregate {

    private static ?VersionParser $versionParser = null;

    /**
     * Holds a bucket of assets
     *
     * @var RepositoryInterface
     */
    protected RepositoryInterface $repository;

    /**
     * Registry of assets which must be included
     *
     * @var array<string, Asset[]>
     */
    protected array $candidates = [];

    /**
     * Registry of assets where the version number is finalized and
     * locked.
     * 
     * @var array<string, Asset>
     */
    protected array $locked = [];

    /**
     * Extra asset bundles that are not part of the core bundle, but which may
     * be referenced by dependencies
     * 
     * @var array<string, Asset>
     */
    protected array $extraAssets = [];

    /**
     * Annotates which assets depend on a particular asset
     * 
     * @var WeakMap<Asset, Asset[]>
     */
    protected WeakMap $dependants;

    /**
     * Internal information about assets used for resolving and error messages
     * 
     * @var ObjectAnnotator<AssetAnnotations>
     */
    protected ObjectAnnotator $assetAnnotations;

    /**
     * Internal information about dependencies used for resolving and for error messages
     * 
     * @var ObjectAnnotator<DependencyAnnotations>
     */
    protected ObjectAnnotator $dependencyAnnotations;

    /**
     * Given a set of assets, this class can remove all unused assets
     * and select the most recent version of assets when multiple 
     * versions of that asset exists.
     *
     * @param Asset[] $registry
     */
    public function __construct(RepositoryInterface $repository) {
        $this->repository = $repository;
        $this->dependants = new WeakMap();
        $this->assetAnnotations = new ObjectAnnotator(AssetAnnotations::class);
        $this->dependencyAnnotations = new ObjectAnnotator(DependencyAnnotations::class);
    }

    public function __clone() {
        $this->dependants = new WeakMap();
        $this->assetAnnotations = new ObjectAnnotator(AssetAnnotations::class);
        $this->dependencyAnnotations = new ObjectAnnotator(DependencyAnnotations::class);
        $this->candidates = [];
        $this->locked = [];
    }

    /**
     * Add dependencies to the resolver.
     *
     * @param Dependency ...$dependencies A specification of what is being required
     * @return void
     * @throws ResolutionFailedException
     */
    public function addDependency(Dependency ...$dependencies): void {
        $state = $this->getState();
        try {
            foreach ($dependencies as $dependency) {
                $this->_addDependency($dependency, null);
            }
        } catch (ResolutionFailedException $e) {
            $this->restoreState($state);
            throw $e;
        }
    }

    public function _addDependency(Dependency $dependency, ?Asset $dependant): void {

        // Record the previous state, so we can roll back if the dependency can't be satisified
        $state = $this->getState();

        try {
            /**
             * Make sure this dependency exists in any version in the repository
             */
            if (!$this->repository->has($dependency->identifier)) {
                throw new ResolutionFailedException('the asset is not available in any versions', $dependency, $dependant);
            }

            /**
             * If this dependency is locked, make sure the locked version is compatible
             */
            if (isset($this->locked[$dependency->identifier])) {
                if ($this->locked[$dependency->identifier]->satisfiesConstraint($dependency->constraint)) {
                    // This dependency can be used here as well
                    if ($dependant === null) {
                        $this->assetInfo($this->locked[$dependency->identifier])->isRootDependency = true;
                    } else {
                        $this->assetInfo($this->locked[$dependency->identifier])->dependants[] = $dependant;
                    }
                    return;
                } else {
                    // This dependency can't be satisfied
                    $lockedAsset = $this->locked[$dependency->identifier];
                    if (!empty($this->assetInfo($lockedAsset)->isRootDependency)) {
                        $reason = 'version '.$lockedAsset->version.' is an application dependency';
                    } else {
                        $reason = $lockedAsset . ' is required by ' . implode(", ", $this->assetInfo($lockedAsset)->dependants);
                    }
                    throw new ResolutionFailedException($reason, $dependency, $dependant);
                }
            }

            /**
             * The dependency is not locked, so remove all incompatible versions and lock it if only a single
             * version remains
             * 
             * @var Asset[]
             */
            $candidates = [];
            foreach ($this->candidates[$dependency->identifier] ?? $this->repository->query($dependency->identifier) as $asset) {
                if ($asset->satisfiesConstraint($dependency->constraint)) {
                    if ($dependant === null) {
                        $this->assetInfo($asset)->isRootDependency = true;
                    }
                    if ($dependant !== null) {
                        $this->assetInfo($asset)->dependants[] = $dependant;
                    }
                    $candidates[] = $asset;
                }
            }

            /**
             * Check if we must lock the candidate
             */
            switch (count($candidates)) {
                case 0: // No candidates remain
                    /** @var Asset[] */
                    $versions = \iterator_to_array($this->repository->query($dependency->identifier));
                    $availableVersions = implode(", ", \array_map(function(Asset $asset) {
                        return $asset->version;
                    }, $versions));
                    throw new ResolutionFailedException('only ' . $availableVersions . ' available', $dependency, $dependant);

                case 1: // Only a single candidate remains, so we will lock it down then resolve its dependencies
                    /**
                     * Lock to this candidate
                     */
                    try {
                        $this->lockAsset($candidates[0]);
                    } catch (ResolutionFailedException $e) {
                        throw new ResolutionFailedException($candidates[0]."'s dependency on ".$e->dependency." failed", $dependency, $dependant, $e);
                    }
                    return;

                default: // Multiple candidates remain, no need to make it more complex
                    // we have multiple candidates remaining
                    $this->candidates[$dependency->identifier] = $candidates;
                    return;
            }
        } catch (\Throwable $e) {
            // restore our changes
            $this->restoreState($state);
            throw $e;
        }
    }

    /**
     * Yield all the asset objects that must be used to satisfy the
     * dependencies.
     *
     * @return Traversable<Asset>
     */
    public function getIterator(): Traversable {
        $state = $this->getState();
        try {
            /**
             * Lock down the best of the available versions. If it fails, try the next
             * best version and so on.
             */
            while ($this->candidates !== []) {
                $lockedOne = null;
                foreach ($this->candidates as $identifier => $candidates) {
                    $sorted = $this->candidates[$identifier] = Utils::sortAssets($candidates);
                    $errors = [];
                    foreach ($sorted as $asset) {
                        try {
                            $this->lockAsset($asset);
                            continue 2;
                        } catch (ResolutionFailedException $e) {
                            $errors[] = $e;
                        }
                    }
                    if ($errors !== []) {
                        throw $errors[0];
                    }
                }
            }

            /**
             * Emit all assets with their dependencies first
             */
            $queue = \array_values($this->locked);
            foreach ($this->extraAssets as $asset) {
                $queue[] = $asset;
            }

            /**
             * @var array<string, array<string, Asset>>
             */
            $sent = [];

            $max = 100000;
            while ($queue !== []) {
                if ($max-- === 0) {
                    throw new AssetManagerLogicException("Unable to resolve queue, seems like infinite loop");
                    die("Max limit");
                }
                $asset = \array_shift($queue);
                foreach ($asset->dependencies as $dependency) {
                    if (empty($sent[$dependency->identifier])) {
                        $queue[] = $asset;
                        continue 2;
                    }
                    $foundOne = false;
                    foreach ($sent[$dependency->identifier] as $void => $candidate) {
                        if ($candidate->satisfies($dependency)) {
                            $asset = $asset->withAddedDependencies($candidate->getDependencyObject());
                            $foundOne = true;
                            break;
                        }
                    }
                    if (!$foundOne) {
                        $queue[] = $asset;
                        continue 2;
                    }
                }
                $sent[$asset->identifier][$asset->version] = $asset;
                yield $asset->name => $asset;
            }
        } finally {
            $this->restoreState($state);
        }
    }

    /**
     * Make an asset permanent
     * 
     * @param Asset $asset 
     * @throws LogicException 
     * @throws Throwable 
     */
    protected function lockAsset(Asset $asset): void {
        if (isset($this->locked[$asset->identifier])) {
            throw new LogicException("Can't lock asset $asset, this identifier has already been locked with ".$this->locked[$asset->identifier]);
        }
        $this->locked[$asset->identifier] = $asset;
        unset($this->candidates[$asset->identifier]);

        /**
         * Apply all dependencies
         */
        /** @var Dependency */
        foreach ($asset->dependencies as $subDependency) {
            $this->dependencyInfo($subDependency)->source = $asset;
            try {
                $this->_addDependency($subDependency, $asset);
            } catch (ResolutionFailedException $e) {
                if ($this->repository->allowsInternalDependencies()) {
                    /**
                     * Add internal bundles for assets where we can't solve the dependency
                     */
                    $resolver = new static($this->repository);
                    $resolver->addDependency($subDependency);
                    foreach ($resolver as $key => $ea) {
                        $this->extraAssets[$key] = $ea;
                    }
                } else {
                    throw $e;
                }
            }
        }
    }

    protected function getState(): array {
        return [
            'assetAnnotations' => clone $this->assetAnnotations,
            'dependencyAnnotations' => clone $this->dependencyAnnotations,
            'candidates' => $this->candidates,
            'locked' => $this->locked,
            'extraAssets' => $this->extraAssets,
        ];
    }

    protected function restoreState(array $state): void {
        foreach ($state as $key => $value) {
            $this->$key = $value;
        }
    }
    /**
     * Annotations about a particular asset
     * 
     * @param Asset $object 
     * @return AssetAnnotations 
     * @throws LogicException 
     */
    protected function assetInfo(Asset $object): AssetAnnotations {
        /** @var AssetAnnotations */
        $result = ($this->assetAnnotations)($object);
        return $result;
    }

    /**
     * Annotations about a particular dependency
     * 
     * @param Dependency $object 
     * @return DependencyAnnotations 
     * @throws LogicException 
     */
    protected function dependencyInfo(Dependency $object): DependencyAnnotations {
        /** @var DependencyAnnotations */
        $result = ($this->dependencyAnnotations)($object);
        return $result;
    }

    /**
     * Get a `composer/semver` VersionParser instance.
     * 
     * @internal Used in AssetManager
     * @return VersionParser 
     */
    public static final function getVersionParser(): VersionParser {
        if (self::$versionParser === null) {
            self::$versionParser = new VersionParser();
        }
        return self::$versionParser;
    }
}