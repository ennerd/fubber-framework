# NPM node_modules package repository

This implements parsing of packages-lock.json files and extraction and resolution of dependencies between packages to bundle exactly
the javascript libraries you need for any particular page.

