<?php
namespace Fubber\AssetManager;

use Fubber\LogicException as FubberLogicException;

class LogicException extends FubberLogicException {
}