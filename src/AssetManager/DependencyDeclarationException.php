<?php
namespace Fubber\AssetManager;

use Fubber\I18n\Translatable;
use Fubber\LogicException;

/**
 * Special exception which is used to record the location where a dependency was created.
 * This exception is used whenever a dependency can't be met, to find the location where
 * the dependency was declared.
 * 
 * @internal
 * @package Fubber\AssetManager
 */
class DependencyDeclarationException extends LogicException {

    public function __construct(
        public readonly string $identifier, 
        public readonly string $constraint
    ) {
        parent::__construct(new Translatable("Dependency request for {identifier} {constraint}", [
            'identifier' => $identifier,
            'constraint' => $constraint,
        ]));
    }
}