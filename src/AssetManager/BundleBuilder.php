<?php
namespace Fubber\AssetManager;

use IteratorAggregate;
use Traversable;

/**
 * Utility class for allowing various parts of the application to report their dependencies
 * during the request life cycle, and finally a shared "super-bundle" can be generated.
 * 
 * @package Fubber\AssetManager
 */
class BundleBuilder implements IteratorAggregate {

    /**
     * The Bucket where all available assets and their dependencies should be registered.
     * 
     * @var RepositoryInterface
     */
    protected RepositoryInterface $repository;

    /**
     * All dependencies that have been declared
     * 
     * @var Dependency[]
     */
    protected array $dependencies = [];

    /**
     * @param RepositoryInterface $repository 
     */
    public function __construct(RepositoryInterface $repository) {
        $this->repository = $repository;
    }

    /**
     * Include a dependency in the asset bundle.
     * 
     * @param string $identifier An asset identifier, such as 'jquery'
     * @param string $version An asset version constraint, for example '^1.23' or '>=1.2.3 <2.0'
     */
    public function add(string $identifier, string $version='*'): void {
        $dependency = new Dependency($identifier, $version);
        $this->dependencies[] = $dependency;
    }

    public function getBundle(): Bundle {
        return new Bundle($this->repository, ...$this->dependencies);
    }


    /**
     * Generate the list of dependencies that are in use
     *
     * @return Traversable<Asset>
     */
    public function getIterator(): Traversable {
        return $this->getBundle();
    }
}