<?php
namespace Fubber\AssetManager;

use Fubber\CodingErrorException;

/**
 * Exception thrown whenever a dependency can't be met.
 * 
 * @package Fubber\AssetManager
 */
class UnsatisfiableDependencyException extends CodingErrorException {
    
}