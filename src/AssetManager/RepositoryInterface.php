<?php
namespace Fubber\AssetManager;

use ArrayAccess;
use Traversable;

/**
 * Represents a collection of assets
 * 
 * @package Fubber\AssetManager
 */
interface RepositoryInterface extends Traversable, ArrayAccess {

    /**
     * Query the repository for assets that satisfy the given constraint. A common
     * query looks something like `$repository->query('jquery-ui', '^1.13.0')`.
     * 
     * @param string $identifier An asset identifier, for example 'jquery-ui'
     * @param string $constraint A semver constraint (as interpreted by `composer/semver`)
     * @return Traversable<Asset> All assets that match the query, ordered by the newest version first
     */
    public function query(string $identifier, string $constraint='*'): Traversable;

    /**
     * Query the repository to see if it has any versions of an asset at all. The purpose of this
     * function is for optimizations; a repository might be powered by an API and this function
     * could respond faster by filtering only on package identifier.
     * 
     * @param string $identifier 
     * @return bool 
     */
    public function has(string $identifier): bool;

    /**
     * Add another dependency to an existing asset. For example; if you have a package named
     * 'datepicker', and you wish to ensure that your CSS file is added 
     */
    public function addDependency(Dependency $from, Dependency $dependsOn): void;

    /**
     * True if assets can use internal dependencies if it isn't possible to satisfy dependencies
     * in combination with previous dependencies.
     * 
     * @return bool 
     */
    public function allowsInternalDependencies(): bool;
}