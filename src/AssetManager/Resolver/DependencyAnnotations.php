<?php
namespace Fubber\AssetManager\Resolver;

use Fubber\AssetManager\Asset;

/**
 * Object holds annotations which are associated with a Dependency by the
 * Resolver class.
 * 
 * @internal
 * @package Fubber\AssetManager
 */
class DependencyAnnotations {
    /**
     * The asset which has this dependency
     * 
     * @var null|Asset
     */
    public ?Asset $source = null;
}