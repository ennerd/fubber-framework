<?php
namespace Fubber\AssetManager\Resolver;

use Fubber\AssetManager\Bundle;

/**
 * Object holds annotations which are associated with an Asset by the
 * Resolver class.
 * 
 * @internal
 * @package Fubber\AssetManager
 */
class AssetAnnotations {
    public array $dependants = [];
    public bool $isRootDependency = false;

    /**
     * Holds bundled assets for cases where a dependency can't be met
     * 
     * @var ?Bundle
     */
    public ?Bundle $internalBundle = null;
}