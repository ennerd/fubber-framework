<?php
namespace Fubber\AssetManager;

use Composer\Semver\Constraint\Constraint;
use Composer\Semver\Constraint\ConstraintInterface;

/**
 * Represents a single asset with a version number.
 * 
 * @package Fubber\AssetManager
 */
class Asset {

    /**
     * Convenient helper method for building a new instance.
     * 
     * @param string $identifier 
     * @param string $version 
     * @param mixed $payload 
     * @param Dependency ...$dependencies 
     * @return static 
     */
    public static function create(string $identifier, string $version, mixed $payload=null, Dependency ...$dependencies): static {
        return new Asset($identifier, $version, null, ...$dependencies);
    }

    /**
     * The canonical name of this asset, containing the identifier and version string.
     *
     * @var string
     */
    public readonly string $name;

    /**
     * A string identifying this asset so that it can be referenced by
     * other packages
     *
     * @var string
     */
    public readonly string $identifier;

    /**
     * A semantic version string as interpreted by the `composer/semver` package.
     * 
     * @link https://getcomposer.org/doc/articles/versions.md#composer-versions-vs-vcs-versions For more information about version strings
     * 
     * @var string
     */
    public readonly string $version;

    /**
     * The package payload - extra information which can be used to implement all the dependencies,
     * such as a URL, file path, callback or an object.
     *
     * @var mixed
     */
    public readonly mixed $payload;

    /**
     * Array of dependencies by this asset
     *
     * @var Dependency[]
     */
    public readonly array $dependencies;

    /**
     * A constraint instance which represents this asset's particular version.
     *
     * @var ConstraintInterface|null
     */
    public readonly ?ConstraintInterface $constraint;

    /**
     * Class represents an asset with an identifier, a path and nested dependencies
     *
     * @param string $identifier A string representing the package
     * @param string $version A semver string representing the version of the particular asset
     * @param mixed $package A representation of the asset, for example a path or an object
     * @param Dependency ...$dependencies Array of dependencies where key is the identifier and value is a version constraint string
     */
    public function __construct(
        string $identifier,
        string $version,
        mixed $payload,
        Dependency ...$dependencies,
    ) {
        $this->identifier = $identifier;
        $this->version = Utils::getVersionParser()->normalize($version);
        $this->payload = $payload;

        $this->name = $this->identifier . '@' . $this->version;
        $this->constraint = new Constraint("==", $this->version);

        foreach ($dependencies as $dependency) {
            if (!($dependency instanceof Dependency)) {
                throw new InvalidArgumentException("Argument \$dependencies must be an array of Dependency objects");
            }
        }

        $this->dependencies = $dependencies;
    }

    /**
     * Create a Dependency object which will resolve exactly to this Asset instance.
     * 
     * @internal
     * @return Dependency
     */
    public function getDependencyObject(): Dependency {
        return new Dependency($this->identifier, $this->constraint);
    }

    /**
     * Return a new instance with a different set of dependencies
     * 
     * @param Dependency ...$dependencies 
     * @return static 
     */
    public function withDependencies(Dependency ...$dependencies): static {
        return new static($this->identifier, $this->version, $this->payload, ...$dependencies);
    }

    /**
     * Return a new instance with additional dependencies
     * 
     * @param Dependency ...$dependencies 
     * @return static 
     */
    public function withAddedDependencies(Dependency ...$dependencies): static {
        $newDependencies = [];
        foreach ($dependencies as $dependency) {
            $newDependencies[$dependency->identifier] = $dependency;
        }
        foreach ($this->dependencies as $dependency) {
            if (!isset($newDependencies[$dependency->identifier])) {
                $newDependencies[$dependency->identifier] = $dependency;
            }
        }
        $newDependencies = \array_values($newDependencies);
        return new static($this->identifier, $this->version, $this->payload, ...$newDependencies);
    }

    /**
     * Return a new instance with a different payload.
     * 
     * @param mixed $payload 
     * @return static 
     */
    public function withPayload(mixed $payload): static {
        return new static($this->identifier, $this->version, $payload, ...$this->dependencies);
    }

    /**
     * Return a new instance with a different version number.
     * 
     * @param string $version 
     * @return static 
     */
    public function withVersion(string $version): static {
        return new static($this->identifier, $version, $this->payload, ...$this->dependencies);
    }

    /**
     * Does this asset satisfy the given dependency
     * 
     * @param Dependency $dependency 
     * @return bool 
     */
    public function satisfies(Dependency $dependency): bool {
        if ($this->identifier !== $dependency->identifier) {
            return false;
        }
        return $this->constraint->matches($dependency->constraint);
    }

    /**
     * Does this asset satisfy the given constraint
     *
     * @internal Should be no need to work directly with constraints.
     * @param ConstraintInterface $constraint
     * @return boolean
     */
    public function satisfiesConstraint(ConstraintInterface $constraint): bool {
        return $this->constraint->matches($constraint);
    }

    public function __toString() {
        return $this->name;
    }    
}