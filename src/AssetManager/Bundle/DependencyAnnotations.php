<?php
namespace Fubber\AssetManager\Bundle;

/**
 * Object holds annotations which are associated with a Dependency by the
 * Resolver class.
 * 
 * @internal
 * @package Fubber\AssetManager
 */
class DependencyAnnotations {

    /**
     * The file which requested the annotation
     * 
     * @var null|string
     */
    public ?string $file;

    /**
     * The line where the annotation was requested from
     * 
     * @var null|string
     */
    public ?int $line;

}