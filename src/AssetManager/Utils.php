<?php
namespace Fubber\AssetManager;

use Composer\Semver\Comparator;
use Composer\Semver\Constraint\ConstraintInterface;
use Composer\Semver\VersionParser;
use RuntimeException;
use UnexpectedValueException;

/**
 * Provides a set of utility functions for the AssetManager package
 * 
 * @package Fubber\AssetManager
 */
final class Utils {

    /**
     * Parse a string describing version constraints into a ConstraintInterface object
     * for further processing.
     * 
     * @param string $constraint A string such as '>=1.0 <2.0' or '^1.23.0'
     * @return ConstraintInterface 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    public static function createConstraint(string $constraint): ConstraintInterface {
        return self::getVersionParser()->parseConstraints($constraint);
    }

    /**
     * 
     * @return VersionParser 
     */
    public static function getVersionParser(): VersionParser {
        static $versionParser = null;
        if ($versionParser === null) {
            $versionParser = new VersionParser();
        }
        return $versionParser;
    }

    /**
     * Sort assets in descending version order
     * 
     * @param Asset[] $assets 
     * @return Asset[] 
     */
    public static function sortAssets(array $assets): array {
        \usort($assets, function(Asset $a, Asset $b) {
            if ($a->version === $b->version) {
                return 0;
            }
            if (Comparator::lessThan($a->version, $b->version)) {
                return 1;
            } else {
                return -1;
            }
        });
        return $assets;
    }

    /**
     * Return a new Asset instance with additional dependencies if the `$from` dependency matches the
     * Asset.
     * 
     * @param Asset $asset 
     * @param Dependency $from 
     * @param Dependency ...$to 
     * @return Asset 
     */
    public static function insertDependencies(Asset $asset, Dependency $from, Dependency ...$to): Asset {
        if (!$asset->satisfies($from)) {
            return $asset;
        }
        if ($asset->constraint->matches($from->constraint)) {
            return $asset->withAddedDependencies(...$to);
        }
    }
}