<?php
namespace Fubber\AssetManager;

use Composer\Semver\Comparator;
use IteratorAggregate;
use Traversable;

eclass MultiRepository implements RepositoryInterface, IteratorAggregate {

    /**
     * Repositories in prioritized order
     * 
     * @var RepositoryInterface[]
     */
    protected array $repositories;

    /**
     * Keeps track of all added dependencies per identifier in the shape `["identifier" => [ [ $fromDependency, $toDependency ], ... ] ]
     * 
     * @var array<string, array<Dependency[]>>
     */
    protected array $addedDependencies = [];

    public function __construct(RepositoryInterface ...$repositories) {
        $this->repositories = $repositories;
    }

    public function has(string $identifier): bool {
        foreach ($this->repositories as $repository) {
            if ($repository->has($identifier)) {
                return true;
            }
        }
        return false;
    }

    public function query(string $identifier, string $constraint = '*'): Traversable {
        $assets = [];
        foreach ($this->repositories as $repository) {
            if ($repository->has($identifier)) {
                foreach ($repository->query($identifier, $constraint) as $asset) {
                    if (!isset($assets[$asset->name])) {
                        continue;
                    }
                    if (isset($this->addedDependencies[$identifier])) {
                        foreach ($this->addedDependencies[$identifier] as $pair) {
                            $asset = Utils::insertDependencies($asset, $pair[0], $pair[1]);
                        }
                    }
                    $assets[$asset->name] = $asset;
                }
            }
        }
        yield from Utils::sortAssets($assets);
    }

    /**
     * Additional dependencies which will be added to the result assets
     * who match the `$from` dependency.
     *
     * @param Dependency $from 
     * @param Dependency $dependsOn 
     */
    public function addDependency(Dependency $from, Dependency $dependsOn): void {
        $this->addedDependencies[$from->identifier][] = [ $from, $dependsOn ];
    }    
}
