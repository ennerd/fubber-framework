<?php
namespace Fubber\AssetManager;

use ArrayAccess;
use ArrayObject;
use Fubber\I18n\Translatable;
use Fubber\ObjectAnnotator\ObjectAnnotator;
use Iterator;
use IteratorAggregate;
use Throwable;
use Traversable;

/**
 * Represents a subset of assets
 * 
 * @package Fubber\AssetManager
 */
class Bundle extends ArrayObject {

    /**
     * All dependencies used to create this bundle
     * 
     * @var Dependency[]
     */
    public readonly array $dependencies;

    /**
     * The repository used to create this bundle
     * 
     * @var RepositoryInterface
     */
    private readonly RepositoryInterface $repository;

    /**
     * Map asset from identifier to all possible versions
     * 
     * @var array<string, Asset[]>
     */
    private array $assetMap = [];

    /**
     * Construct a bundle of assets by providing Dependency objects
     * 
     * @param RepositoryInterface $repository 
     * @param Dependency ...$dependencies 
     * @throws ResolutionFailedException 
     * @throws LogicException
     * @throws Throwable 
     */
    public function __construct(RepositoryInterface $repository, Dependency ...$dependencies) {
        $this->repository = $repository;
        $this->dependencies = $dependencies;
        $assets = [];
        $resolver = new Resolver($repository);
        try {
            $resolver->addDependency(...$dependencies);
            foreach ($resolver as $key => $asset) {
                $this->assetMap[$asset->identifier][] = $asset;
                $assets[$key] = $asset;
            }
            parent::__construct($assets, ArrayObject::STD_PROP_LIST);
        } catch (ResolutionFailedException $e) {
            $message = "Unable to satisfy ";
            if (!$e->dependant) {
                $message .= ' the root dependency';
            } else {
                $message .= $e->dependant->name."'s dependency";
            }

            $message .= ' on '.$e->dependency->name;

            if (defined('DEBUG') && DEBUG) {
                foreach ($e->dependency->getTrace() as $trace) {
                    if (empty($trace['class']) || !\str_starts_with($trace['class'], __NAMESPACE__)) {
                        $message .= ' declared in '.$trace['file'].':'.$trace['line'];
                        break;
                    }
                }
            }
            throw new UnsatisfiableDependencyException($message, 0, $e);
        }
    }

    /**
     * Get asset keys which are required by an asset
     * 
     * @param Asset $asset 
     * @return string[] 
     * @throws LogicException 
     */
    public function getReferencesFor(Asset $asset): array {
        if (!isset($this[$asset->name])) {
            throw new LogicException("Asset $asset is not contained in this bundle");
        }
        $result = [];
        foreach ($asset->dependencies as $dependency) {
            foreach ($this->assetMap[$dependency->identifier] as $candidate)  {
                if ($candidate->satisfiesConstraint($dependency->constraint)) {
                    $result[] = $candidate->name;
                    continue 2;
                }
            }
            throw new LogicException("Dependency $dependency for asset $asset is not contained in this bundle");
        }
        return $result;
    }

    public function getAssetForDependency(Dependency $dependency): Asset {
        foreach ($this->assetMap[$dependency->identifier] as $asset) {
            if ($asset->satisfiesConstraint($dependency->constraint)) {
                return $asset;
            }
        }
        throw new LogicException("The dependency $dependency has no candidates in this bundle");
    }

    /**
     * Combine multiple bundles together into a new bundle and resolve dependencies again.
     * 
     * @param Bundle ...$bundles 
     * @return static 
     */
    public function combineWith(Bundle ...$bundles): static {
        $dependencies = [];
        foreach ($bundles as $bundle) {
            foreach ($bundle->getDependencies() as $dependency) {
                $dependencies[] = $dependency;
            }
        }
        return $this->withDependencies(...$dependencies);
    }

    /**
     * Create a larger bundle with new dependencies
     * 
     * @param Dependency ...$dependencies 
     * @return static 
     */
    public function withDependencies(Dependency ...$dependencies): static {
        return new static($this->repository, ...$this->dependencies, ...$dependencies);
    }

    /**
     * Get the dependencies needed to create this object.
     * 
     * @return array 
     */
    public function getDependencies(): array {
        $result = [];
        foreach ($this->assets as $asset) {
            $result[] = $asset->getDependencyObject();
        }
        return $result;
    }

    /**
     * Just for type hinting 
     * 
     * @return Traversable<string, Asset> 
     */
    public function getIterator(): Iterator {
        return parent::getIterator();
    }

    public function __toString(): string {
        return "bundle [".implode(", ", $this->dependencies)."]";
    }

}