<?php
namespace Fubber\AssetManager;

use ArrayObject;
use IteratorAggregate;
use RuntimeException;
use Traversable;
use UnexpectedValueException;

/**
 * A repository implementation which is built programmatically.
 * 
 * @package Fubber\AssetManager
 */
class Bucket implements RepositoryInterface, IteratorAggregate {

    public readonly bool $allowsInternalDependencies;

    public function __construct(bool $allowInternalDependencies=false) {
        $this->allowsInternalDependencies = $allowInternalDependencies;
    }

    /**
     * Contains all available assets
     *
     * @var array<string, Asset[]>
     */
    protected array $registry = [];

    /**
     * Keeps track of all added dependencies per identifier in the shape `["identifier" => [ [ $fromDependency, $toDependency ], ... ] ]
     * 
     * @var array<string, array<Dependency[]>>
     */
    protected array $addedDependencies = [];

    /**
     * @inheritdoc
     * @param string $identifier 
     * @param string $constraint 
     * @return Traversable 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    public function query(string $identifier, string $constraint = '*'): Traversable {
        if (!isset($this->registry[$identifier])) {
            return;
        }
        $constraintObject = Utils::createConstraint($constraint);
        $assets = [];
        foreach ($this->registry[$identifier] as $asset) {
            if (!$asset->satisfiesConstraint($constraintObject)) {
                continue;
            }
            if (isset($this->addedDependencies[$identifier])) {
                foreach ($this->addedDependencies[$identifier] as $pair) {
                    $asset = Utils::insertDependencies($asset, $pair[0], $pair[1]);
                }
            }
            $assets[] = $asset;
        }
        yield from Utils::sortAssets($assets);
    }

    /**
     * @inheritdoc
     * @param string $identifier 
     * @return bool 
     */
    public function has(string $identifier): bool {
        return !empty($this->registry[$identifier]);
    }

    /**
     * Declare the ability to provide an asset with a version and recursive dependencies.
     * 
     * @param Asset $asset
     * @throws InvalidArgumentException
     */
    public function add(Asset ...$assets): void {
        foreach ($assets as $asset) {
            if (isset($this->registry[$asset->identifier]) && isset($this->registry[$asset->identifier][$asset->version])) {
                throw new InvalidArgumentException("An identical asset is already registered");
            }
        }
        foreach ($assets as $asset) {
            $this->registry[$asset->identifier][$asset->version] = $asset;
//            $this->registry[$asset->identifier][$asset->version] = $asset;
        }
    }

    /**
     * Additional dependencies which will be added to the result assets
     * who match the `$from` dependency.
     *
     * @param Dependency $from 
     * @param Dependency $dependsOn 
     */
    public function addDependency(Dependency $from, Dependency $dependsOn): void {
        $this->addedDependencies[$from->identifier][] = [ $from, $dependsOn ];
    }

    public function offsetExists(mixed $offset): bool {
        $parts = explode('@', $offset, 2);
        if (isset($parts[1])) {
            return isset($this->registry[$parts[0]][$parts[1]]);
        } else {
            return isset($this->registry[$parts[0]]);
        }
    }

    public function offsetGet(mixed $offset): Asset|array|null {
        $parts = explode('@', $offset);
        if (isset($parts[1])) {
            return $this->registry[$parts[0]][$parts[1]] ?? null;
        } else {
            return $this->registry[$parts[0]] ?? null;
        }
    }

    public function offsetUnset(mixed $offset): void {
        throw new LogicException("Can't unset an asset");
    }

    public function offsetSet(mixed $offset, mixed $value): void {
        throw new LogicException("Can't add assets this way");
    }

    public function getIterator(): Traversable {
        foreach ($this->registry as $identifier => $assets) {
            foreach ($assets as $asset) {
                if (isset($this->addedDependencies[$identifier])) {
                    /**
                     * Two pass optimization to avoid O(n^2) performance.
                     */
                    $additions = [];
                    foreach ($this->addedDependencies[$identifier] as $pair) {
                        $additions[$pair[0]->name][] = $pair[1];
                    }
                    foreach ($this->addedDependencies[$identifier] as $pair) {
                        $dependencies = $additions[$pair[0]->name];
                        $asset = Utils::insertDependencies($asset, $pair[0], ...$dependencies);
                    }
                }
                yield $asset->name => $asset;
            }
        }
    }

    public final function allowsInternalDependencies(): bool {
        return $this->allowsInternalDependencies;
    }
}