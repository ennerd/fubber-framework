<?php
namespace Fubber\AssetManager;

use Fubber\InvalidArgumentException as FubberInvalidArgumentException;

class InvalidArgumentException extends FubberInvalidArgumentException {
    
}