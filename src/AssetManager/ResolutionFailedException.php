<?php
namespace Fubber\AssetManager;

use Fubber\I18n\Translatable;
use Fubber\LogicException;
use Throwable;

/**
 * Internal exception thrown whenever a dependency can't be satisfied. 
 * 
 * @internal
 * 
 * @package Fubber\AssetManager
 */
class ResolutionFailedException extends LogicException {

    /**
     * Exception when resolution fails
     *
     * @param Dependency $dependency
     * @param Asset|null $dependant
     */
    public function __construct(string $reason, public readonly Dependency $dependency, public readonly ?Asset $dependant, ?Throwable $previous=null) {
        parent::__construct(new Translatable("Can't provide {dependency}, reason: {reason}", [
            'dependency' => $dependency,
            'dependant' => $dependant ?? 'application',
            'reason' => $reason,
        ]), 0, $previous);
        return;
        if ($dependant !== null) {
            parent::__construct(new Translatable("Asset {dependant} requires {dependency} which could not be satisfied", [
                'dependant' => $dependant,
                'dependency' => $dependency,
            ]));
        } else {
            parent::__construct(new Translatable("Dependency on {dependency} could not be satisfied", [
                'dependency' => $dependency,
            ]));
        }
    }

}