<?php
namespace Fubber\AssetManager;

use Composer\Semver\Constraint\ConstraintInterface;

/**
 * Represents a dependency on a particular asset with version constraints.
 * 
 * @package Fubber\AssetManager
 */
class Dependency {

    /**
     * The asset identifier that we're depending on
     * 
     * @var string
     */
    public readonly string $identifier;

    /**
     * The constraints declared for this dependency
     * 
     * @var ConstraintInterface
     */
    public readonly ConstraintInterface $constraint;

    /**
     * The canonical name of this dependency
     * 
     * @var string
     */
    public readonly string $name;

    /**
     * If the constant DEBUG is true, this contains the debug_backtrace when the dependency was constructed.
     * 
     * @var array
     */
    private readonly array $trace;

    /**
     * @param string $identifier 
     * @param ConstraintInterface $constraint 
     * @param null|Asset $dependant 
     */
    public function __construct(
        string $identifier,
        string|ConstraintInterface $constraint='*'
    ) {
        $this->identifier = $identifier;
        $this->constraint = is_string($constraint) ? Utils::createConstraint($constraint) : $constraint;
        $this->name = $this->identifier . ' ' . $this->constraint;

        if (defined('DEBUG') && DEBUG) {
            $this->trace = \debug_backtrace();
        }
    }

    /**
     * Provide the stack trace when this object was constructed. Only available
     * when the constant DEBUG is true.
     * 
     * @return array 
     * @throws LogicException 
     */
    public function getTrace(): array {
        if (!defined('DEBUG') || !DEBUG) {
            throw new LogicException("Unable to provide a backtrace unless the constant DEBUG is true");
        }
        return $this->trace;
    }

    public function __toString() {
        return $this->name;
    }
}