# Resolving a Dependency Graph with Version Constraints

When an application becomes complex, you will eventually end up with a bunch of "assets"
and dependencies - similar to how a composer package might depend on a set of other
packages.

With javascript files, it is quite common to just load every javascript file on every
page view - so that it is always available. This means that the `jquery-datepicker`
javascript file will be loaded on every page view. With client side caching, this
may not be a problem - but for a large application there might only be a tiny chance
that most of your users will ever need that datepicker library.

You *could* begin migrating to the `npm` eco-system, but it may end up quite a complex
undertaking. Another alternative is to have your template parts record their dependencies,
and just before you send the HTML to the browser - you use the dependency resolver to
figure out which assets are *actually needed* for this particular page view.

## Forms can have a Dependency Graph

If you're building an application with forms that people needs to complete; many forms
require you to fill out personal details, and other forms don't require that.

With a dependency graph resolver, you could treat your `personal details` form as one
asset, and then your `driving licence application` form as a second asset - and finally
a `medical certificate` as a third form.

Now, the `driving licence application` form would depend on both `personal details` and
`medical certificate`. The `medical certificate` also would depend on `personal details`.

Querying a dependency resolver, your query for `driving licence application` would
immediately return the following "assets":

 * `personal details`
 * `medical certificate`
 * `driving licence application`

Next, you could check your database to see if the user has already filled out any of
these parts and only present him with whatever he needs to fill out.

## A complete example

```php
// Bucket is a basic implementation of a repository
$assets = new Bucket();

// Add all the assets that exists
$assets->add(
    new Asset('personal details', '1.0.0', PartialForms\PersonalDetails::class),
    new Asset('medical certificate', '1.0.0', PartialForms\MedicalCertificate::class,
        new Dependency('personal details', '^1.0.0')
    ),
    new Asset('driving licence application', '1.0.0', PartialForms\DrivingLicenceApplication::class,
        new Dependency('personal details', '^1.0.0'),
        new Dependency('medical certificate', '1.0.0')
    )
);

// Pick whatever you want from the repository by creating a "bundle"
$bundle = new Bundle($assets, new Dependency('driving licence application')));

// This is a made up class which combines muliple partial forms
$form = new CompoundForm();

// The bundle can be iterated (or you can access the `$bundle->assets` array)
foreach ($bundle as $asset) {
    echo "Asset {$asset->identifier} version {$asset->version}:<br>";
    $className = $asset->payload;
    $form->add(new $className);
}
$form->generate();
```
