<?php
namespace Fubber\KeyValueStore;

use Fubber\Kernel\Container\FactoryInterface;
use Traversable;

class KeyValueStoreFactory implements FactoryInterface {

    public function __construct() {}

    public static function getMakerFunctions(): iterable {
        yield from DbKeyValueStore::getMakerFunctions();
    }
}