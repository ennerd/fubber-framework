<?php
namespace Fubber\KeyValueStore;

use Closure;
use Fubber\{CodingErrorException, Db\IDb, Kernel, LogicException, RaceConditionException, UnknownServiceException};
use Fubber\Kernel\Container\FactoryInterface;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\Scope;
use Fubber\KeyValueStore\KeyValueStoreInterface;
use InvalidArgumentException;
use PDOException;
use Traversable;

/**
 * Simple Key Value store, creates a database table and allow you to set any variable on it that will
 * persist.
 */
class DbKeyValueStore implements KeyValueStoreInterface, FactoryInterface {
    

    /**
     * Provide a database table name to use. This table will automatically be created if it doesn't exist.
     * 
     * @param string $tableName 
     */
    public function __construct(protected readonly IDb $db, protected readonly string $tableName='ff_vars') {}

    public static function getMakerFunctions(): iterable {
        yield function(IDb $db) {
            return new self($db);
        };
    }

    protected ?string $namespace=null;

    public function withNamespace(string $namespace): static {
        $c = clone $this;
        if ($c->namespace === null) {
            $c->namespace = '/' . $namespace;
        } else {
            $c->namespace .= '/' . $namespace;
        }
        return $c;
    }

    public function getNamespace(): ?string {
        return $this->namespace;
    }

    public static function isAvailable(): bool {
        return Kernel::instance()->has(IDb::class);
    }

    protected function getMaxKeyLength(): int {
        return 50;
    }


    /**
     * Get a value from the key-value store
     * 
     * @param string $name 
     * @param bool $success True if a value was fetched from the database
     * @return mixed 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     */
    public function get(string $name, bool &$success=null): mixed {
        try {
            $value = $this->db()->queryField('SELECT value FROM '.$this->tableName.' WHERE name=?', [ $this->getKey($name) ]);
            if($value) {
                $success = true;
                if (\is_numeric($value)) {
                    if (\intval($value) == \floatval($value)) {
                        return (int) $value;
                    }
                    return (float) $value;
                }
                return unserialize($value);
            } else {
                $success = false;
            }
        } catch (\PDOException $e) {}
        return null;
    }
    
    /**
     * Save a value to the key-value store
     * 
     * @param mixed $name 
     * @param mixed $value 
     * @return mixed 
     * @throws PDOException 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     */
    public function set(string $name, mixed $value): mixed {
        try {
            if (\is_int($value) || \is_float($value)) {
                $valueS = (string) $value;
            } else {
                $valueS = serialize($value);
            }
            return $this->db()->exec($sql = 'INSERT INTO '.$this->tableName.' (name, value) VALUES (?,?) ON DUPLICATE KEY UPDATE value=?', [
                $this->getKey($name),
                $valueS,
                $valueS,
                ]);
        } catch (\PDOException $e) {
            if($e->getCode() == '42S02') {
                $this->createTable();
                return $this->set($name, $value);
            }
            throw $e;
        }
    }

    /**
     * Delete a stored value.
     * 
     * @param mixed $name 
     * @return bool 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     */
    public function delete($name): bool {
        try {
            return !!$this->db()->exec('DELETE FROM '.$this->tableName.' WHERE name=?', [
                $this->getKey($name),
            ]);
        } catch (\PDOException $e) {
            return false;
        }
    }
    
    /**
     * Increment a saved value from the database atomically.
     * 
     * @param string $name 
     * @param float|int $incrementBy 
     * @return mixed 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     * @throws PDOException 
     * @throws RaceConditionException 
     */
    public function inc(string $name, float $incrementBy=1): float {
        if ($incrementBy === 0.0) {
            $result = $this->get($name, $success);
            if (!$success) {
                return 0;
            }
            return (float) $result;
        }

        try {
            $this->db()->exec('INSERT INTO '.$this->tableName.' (name, value) VALUES (?, ?) IN DUPLICATE KEY UPDATE value=value+?', [
                $this->getKey($name),
                $incrementBy,
                $incrementBy
            ]);
        } catch (\PDOException $e) {
            if ($e->getCode() === '42S02') {
                $this->createTable();
                return $this->inc($name, $incrementBy);
            }
            throw $e;
        }
    }

    protected function db(): IDb {
        return Kernel::instance()->get(IDb::class);
    }
    
    protected function createTable() {
        $this->db()->exec('CREATE TABLE '.$this->tableName.' (name VARCHAR(50) PRIMARY KEY, value BLOB) CHARACTER SET utf8');
    }

    protected function getKey(string $key): string {
        if ($this->namespace === null) {
            return $key;
        } else {
            $ns = $this->namespace;
            if (\strlen($ns) > 30) {
                $ns = \substr($this->namespace, 0, 20) . \substr(\md5($this->namespace), 0, 10);
            }
            return $ns . '/' . $key;
        }
    }
}
