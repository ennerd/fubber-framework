<?php
namespace Fubber\KeyValueStore;

use Closure;
use DateInterval;
use Fubber\Kernel\Container\NamespacedInterface;
use Throwable;
use Traversable;

/**
 * This interface provides a backend for the simplest, busiest and fastest
 * caching scenarios and is intended for short time server-local caching of
 * data which rarely changes.
 * 
 * The cache is designed especially for caching the results of slightly
 * expensive processing jobs where the source data rarely changes - for
 * example routing tables or dependency injection information.
 * 
 * DO NOT use this interface for caching large amounts of data. This 
 * interface is different from more common cache interfaces PRECICELY
 * because the implementation is given the privilege of adhering to very
 * relaxed concurrency and concistency requirements.
 * 
 * NO CONSISTENCY guarantees between processes and concurrent requests are
 * provided, which means that this type of caching is designed for caching
 * data which rarely changes and is slightly costly to generate.
 * 
 * Implementations SHOULD NOT access the network for caching.
 * 
 * @package Fubber
 */
interface KeyValueStoreInterface extends NamespacedInterface {

    /**
     * Returns `true` if the this implementation is supported by the current
     * PHP and server environment.
     * 
     * @return bool 
     */
    public static function isAvailable(): bool;

    /**
     * Get a value from the key-value store
     * 
     * @param string $name 
     * @param bool $success True if a value was fetched from the database
     * @return mixed 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     */
    public function get(string $name, bool &$success=null): mixed;
    
    /**
     * Save a value to the key-value store
     * 
     * @param mixed $name 
     * @param mixed $value 
     * @return mixed 
     * @throws PDOException 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     */
    public function set(string $name, mixed $value): mixed;

    /**
     * Delete a stored value.
     * 
     * @param mixed $name 
     * @return bool 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     */
    public function delete($name): bool;

    /**
     * Increment a saved value from the database atomically.
     * 
     * @param string $name 
     * @param float|int $incrementBy 
     * @return mixed 
     * @throws InvalidArgumentException 
     * @throws UnknownServiceException 
     * @throws CodingErrorException 
     * @throws PDOException 
     * @throws RaceConditionException 
     */
    public function inc(string $name, float $incrementBy=1): float;
}