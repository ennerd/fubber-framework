<?php
namespace Fubber\Mixins;

use Attribute;
use Closure;

/**
 * Designate a method as an "aspect constructor" which will be invoked
 * first thing inside the Kernel's constructor.
 * 
 * @package Fubber\Kernel
 */
#[Attribute(Attribute::TARGET_METHOD)]
class MixinConstructor {

    /**
     * @var string[]
     */
    public readonly array $dependencies;
    public readonly Closure $method;

    /**
     * Declare the name of this kernel aspect, and a list of other aspects that
     * must have run before this aspect is initialized.
     * 
     * @param string $name Generally the trait name
     * @param string[] $dependencies 
     */
    public function __construct(public readonly string $name, string ...$dependencies) {
        $this->dependencies = $dependencies;
    }

    public function setMethod(Closure $closure): void {
        $this->method = $closure;
    }
}