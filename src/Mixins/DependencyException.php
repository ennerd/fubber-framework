<?php
namespace Fubber\Mixins;

use RuntimeException;

class DependencyException extends RuntimeException {
    
}