<?php
namespace Fubber\Mixins;

use Fubber\DS\Vector;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;

trait Mixins {

    /** 
     * This method MUST be invoked by the class that uses this trait.
     */
    protected final function Mixins(): void {
        /**
         * Queue of mixins which need initialization
         * 
         * @var MixinConstructor[]
         */
        $queue = [];
        /** 
         * Mixins that completed initialization
         * 
         * @var array<string, MixinConstructor>
         */
        $completed = [];

        $rc = new ReflectionClass($this);
        foreach ($rc->getMethods(ReflectionMethod::IS_PRIVATE | ReflectionMethod::IS_PROTECTED | ReflectionMethod::IS_PUBLIC) as $rm) {
            foreach ($rm->getAttributes(MixinConstructor::class) as $ra) {
                /** @var MixinConstructor */
                $attribute = $ra->newInstance();
                $attribute->setMethod($rm->getClosure($this));
                $queue[$attribute->name] = $attribute;
            }
        }

        while ($queue !== []) {
            $next = \array_shift($queue);
            foreach ($next->dependencies as $dependency) {
                if (!isset($completed[$dependency])) {
                    if (!isset($queue[$dependency])) {
                        if (!\trait_exists($dependency)) {
                            throw new DependencyException("Mixin `".$next->name."` has a dependency `".$dependency."` which does not exist");
                        }
                        throw new DependencyException("Mixin `".$next->name."` depends on mixins `".implode("`, `", $next->dependencies)."` and `".$dependency." is neither queued nor completed");
                    }
                    $queue[$next->name] = $next;
                    continue 2;
                }
            }
            //header("X-Mixin: ". $next->name, false);
            $completed[$next->name] = $next;
            ($next->method)();
        }
    }
}