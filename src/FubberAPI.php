<?php
namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\I18n\TranslatableInterface;
use ReflectionClass;

trait FubberAPI {

    protected static function state(): State {
        return State::getCurrent();
    }

    protected static function trans(string $message, array $variables=[]): TranslatableInterface {
        $file = \debug_backtrace(false, 1)[0]['file'];
        return new Translatable($message, $variables, $file);
    }

}