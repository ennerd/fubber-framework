<?php
namespace Fubber\Interfaces;

interface IDecorator {
    public function __call($name, array $args);
    public function __get($name);
    public function __set($name, $value);
}