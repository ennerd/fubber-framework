<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\Traits\ExceptionTrait;
use JsonSerializable;

/**
*	@see \BadFunctionCallException
*/
class BadFunctionCallException extends \BadFunctionCallException implements JsonSerializable, IException {
    use ExceptionTrait;

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Bad Function Call";
    }
}
