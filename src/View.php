<?php
namespace Fubber;

use Fubber\Psr\HttpMessage\Response;

/**
 * A buffered response that should be sent via another response back to the browser.
 */
class View extends Response {

    protected $template;
    protected $vars = [];

    public function __construct(string $template, array $vars=[], array $headers=[], int $statusCode=200, string $reasonPhrase=null) {
        Hooks::dispatch('fubber.view.created', $template, $vars, $headers, $statusCode, $reasonPhrase);

        $this->vars = $vars;

        if(!is_object($template)) {
            $template = Kernel::instance()->template->render($template, $vars);
        }

        $contentType = null;
        foreach ($headers as $k => $v) {
            if (\strtolower($k) === 'content-type') {
                $contentType = $v;
                break;
            }
        }
        if ($contentType === null) {
            $headers['Content-Type'] = 'text/html; charset=UTF-8';
        }

        parent::__construct((string) $template, $headers, $statusCode, $reasonPhrase);
    }

    public function __toString(): string {
        return (string) $this->getBody();
    }
}
