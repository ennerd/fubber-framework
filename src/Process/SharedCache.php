<?php
namespace Fubber\Process;

/**
 * Rock solid caching which can be used even if threading.
 * 
 * Limits memory usage by purging items whenever they expire.
 */
class SharedCache {
    /**
     * Set to false if not threaded
     */
    protected static $useLocking = null;
    
    /**
     * All cached items are stored here: $key => $node,
     */
    protected static $cache = [];
    
    /**
     * Associative array of expiration time => $keys[].
     */
    protected static $expirations = [];
    protected static $perhapsEmptyExpirations = [];
    
    /**
     * Time of the last maintain-cycle
     */
    protected static $lastCycle = null;
    
    // Key of the most recently used items
    protected static $leastRecentKey = null;
    protected static $mostRecentKey = null;

    protected static $microLock = 0;
    protected static $maintainSkipper = 0;
    
    public static function get($key) {
        //echo "get()\n";
        // this check is much faster than raising an error
        if(!isset(static::$cache[$key]))
            return null;
            
        // must ignore errors here, since the item may have been removed by another thread
        $item = @static::$cache[$key];
        if(!$item) {
            return null;
        }
        
        if($item['e'] < time()) {
            // Item has expired. It will be purged at some point.
            return null;
        }
        
        // We must update LRU
        static::block();
        static::makeRecent($key);
        static::$microLock--;
            
        return $item['v'];
    }
    
    /**
     * Method blocks until it is allowed to update the cache.
     * 
     * @return void
     */
    public static function set($key, $value, $expiresIn=60, $_microLockOffset=0) {
        //echo "set()\n";
        $e = time() + floor($expiresIn);
        static::block();

        // Removes planned expirations for the item
        if(isset(static::$cache[$key])) {
            static::removeLinks($key);
            static::$cache[$key]['e'] = $e;
            static::$cache[$key]['v'] = $value;
            static::makeRecent($key);
            static::$expirations[$e][$key] = true;
        } else {
            static::$cache[$key] = [
                'e' => $e,
                'v' => $value,
                'p' => null,
                'n' => null,
                ];
            static::makeRecent($key);
            //echo "EXPSET $e $key\n";
            static::$expirations[$e][$key] = true;
        }
        static::$microLock--;
        return true;
    }
    
    public static function increment($key, $offset=1, $initialValue=0, $expiresIn = 60) {
        //echo "increment()\n";
        $e = time() + $expiresIn;
        static::block();

        if(!isset(static::$cache[$key])) {
            $res = static::set($key, $initialValue, $expiresIn, 1);
            static::$microLock--;
            return $initialValue;
        } else {
            static::removeLinks($key);
            static::$cache[$key]['v']++;
            static::$cache[$key]['e'] = $e;
            static::makeRecent($key);
            static::$expirations[$e][$key] = true;
            $v = static::$cache[$key]['v'];
            static::$microLock--;
            return $v;
        }
        
    }
    
    public static function decrement($key, $value=1, $initialValue=0, $expiresIn=60) {
        return static::increment($key, -$value, $initialValue, $expiresIn);
    }
    
    /**
     * Does the following:
     * - removes all items that have expired
     * - update LRU
     * - while available PHP memory < 10 MB, purge least recently used
     */
    protected static function _maintain() {
        $t = time();
        if(static::$lastCycle === null) {
            foreach(static::$expirations as $ct => &$keys) {
                if($ct < $t) {
                    foreach($keys as $key => $null) {
                        static::removeLinks($key);
                        unset(static::$cache[$key]);
                    }
                    unset(static::$expirations[$ct]);
                    unset(static::$perhapsEmptyExpirations[$ct]);
                }
            }
        } else for($ct = static::$lastCycle; $ct < $t; $ct++) {
            //echo "_maintain: $ct\n";
            if(isset(static::$expirations[$ct])) {
                foreach(static::$expirations[$ct] as $key => $null) {
                    static::removeLinks($key);
                    unset(static::$cache[$key]);
                }
                unset(static::$expirations[$ct]);
                unset(static::$perhapsEmptyExpirations[$ct]);
            }
        }
        foreach(static::$perhapsEmptyExpirations as $ts => $null) {
            if(empty(static::$expirations[$ts]))
                unset(static::$expirations[$ts]);
        }
        static::$perhapsEmptyExpirations = [];

        

        static::$lastCycle = $t;
    }
    
    protected static function makeRecent($key) {
        //echo "makeRecent()\n";
        // Is already most recent?
        if(static::$mostRecentKey === $key) return;

        $item = &static::$cache[$key];
        if($item['p']) {
            static::$cache[$item['p']]['n'] = $item['n'];
            static::$cache[$item['n']]['p'] = $item['p'];
        }
        static::$cache[static::$mostRecentKey]['n'] = $key;
        static::$mostRecentKey = $key;
        if(static::$leastRecentKey === null)
            static::$leastRecentKey = $key;
    }
    
    protected static function removeLinks($key) {
        //echo "removeLinks()\n";
        $item = &static::$cache[$key];

        if(static::$mostRecentKey === $key)
            static::$mostRecentKey = $item['p'];
        if(static::$leastRecentKey === $key)
            static::$leastRecentKey = $item['n'];

        // Update next/previous items
        if($item['n'] !== null) {
            static::$cache[$item['n']]['p'] = $item['p'];
            $item['p'] = null;
        }
        if($item['p'] !== null) {
            static::$cache[$item['p']]['n'] = $item['n'];
            $item['n'] = null;
        }
        //echo "EXPUNSET2: ".$item['e']." $key\n";
        unset(static::$expirations[$item['e']][$key]);
        static::$perhapsEmptyExpirations[$item['e']] = true;
    }
    
    /**
     * Loop forever, until we are able to achieve a microlock. Unlocking is
     * done by static::$microLock--;
     */
    protected static $blockMethod;
    protected static function block() {
        static $blockMethod;
        if($blockMethod)
            return $blockMethod();

        if(!class_exists(\Threaded::class, false)) {
            // No locking will be used
            $blockMethod = function() {
                static::$microLock++;
                if(++static::$maintainSkipper > 1000 && static::$lastCycle < time() - 5) {
                    static::$maintainSkipper = 0;
                    static::_maintain();
                }
            };
            return;
        }
        throw new \Exception("Threaded use is not currently supported. MUST implement thread safe locking. Perhaps pht\AtomicInteger can be used? Perhaps Threads::synchronized?");
    }
    
    public static function status() {
        echo "status()\n";
        print_r(static::$cache);
        echo "LR: ".static::$leastRecentKey."\n";
        echo "MR: ".static::$mostRecentKey."\n";
        echo "Expirations: ";
        print_r(static::$expirations);
    }
}