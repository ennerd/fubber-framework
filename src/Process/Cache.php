<?php
namespace Fubber\Process;

/**
 * This class provides thread safe fast in-process caching. It should be used
 * instead of class internal caching using globals or class properties, unless 
 * you are certain that threading will not break.
 * 
 * The caching class implements least recently used purging and removes items from
 * memory once they expires based on a highly optimized maintenance algorithm that
 * is invoked on 1 in 1000 write operations, but no more than once every 5 seconds.
 * 
 * The class does not serialize content, and can be used to cache for example 
 * resources, iterators, closures and generators.
 */
class Cache {
    protected $key;
    
    public function __construct($key) {
        $this->key = $key;
    }
    
    public function get($key) {
        return SharedCache::get($this->key.'\n'.$key);
    }
    
    public function set($key, $value, $expiresIn=3600) {
        return SharedCache::set($this->key.'\n'.$key, $value, $expiresIn);
    }
    
    public function increment($key, $offset=1, $expiresIn=3600) {
        return SharedCache::increment($this->key.'\n'.$key, $offset, $expiresIn);
    }

    public function decrement($key, $offset=1, $expiresIn=3600) {
        return SharedCache::increment($this->key.'\n'.$key, $offset, $expiresIn);
    }
}