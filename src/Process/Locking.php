<?php
namespace Fubber\Process;

/**
 * Class helps with atomick locking and unlocking
 */
class Locking {
    protected static $locks = [];
    protected static $locknum = 0;
    // Used to lock short code segments
    protected static $tmpfile;
    
    /**
     * Returns true if we were able to atomically get a lock on $name
     */
    public static function lock($name) {
        if(static::$locks[$name]) return false;
        if(static::atomStart($blocking)) {
            static::$locks[$name] = true;
            static::atomEnd();
            return $name;
        }
        return null;
    }
    
    public static function lockBlocking($name) {
        if(static::$locks[$name]) return false;
        if(static::atomStart(true)) {
            static::$locks[$name] = true;
            static::atomEnd();
            return $name;
        }
        return null;
    }
    
    /**
     * Releases the lock. There is no checking. The lock can be released by anyone.
     */
    public static function release($name) {
        unset(static::$locks[$name]);
        return true;
    }
    
    /**
     * This function returns true if we were able to reach a global lock. All
     * other threads should receive false while this lock is in effect.
     */
    protected static function atomStart($blocking=false) {
        if(!static::$tmpfile)
            static::$tmpfile = tmpfile();
        if($blocking)
            return flock(static::$tmpfile, LOCK_EX);
        else
            return flock(static::$tmpfile, LOCK_EX|LOCK_NB);
    }
    
    /**
     * This function MUST ALWAYS be called, whenever atomStart() returned true
     */
    protected static function atomEnd() {
        return flock(static::$tmpfile, LOCK_UN);
    }
}