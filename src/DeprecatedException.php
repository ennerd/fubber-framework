<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception that is thrown when a requested method or operation is deprecated.
 */
class DeprecatedException extends NotSupportedException {

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Deprecated";
    }
}
