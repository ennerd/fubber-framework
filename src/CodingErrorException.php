<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when we detect that a library is being used
 * incorrectly or not as intended. Not to be confused with the
 * DeprecatedException
 *
 * @see DeprecatedException
 * @see NotSupportedException
 */
class CodingErrorException extends LogicException {
    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Programming Error Detected";
    }
}
