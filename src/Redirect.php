<?php
namespace Fubber;

use Fubber\Psr\HttpMessage\Response;
use Psr\Http\Message\UriInterface;

class Redirect extends Response {
    /**
     * Cacheable redirect, but changes the protocol to GET if it is not already. The old url is not valid, and link value should be sent to the
     * new location.
     */
    const MOVED_PERMANENTLY = 301;

    /**
     * Temporary redirect, changes the protocol to GET. The content the visitor wants lives in a different location. Link value remains here, though.
     */
    const CONTINUE_AT = 302;

    /**
     * Temporary redirect. Keeps protocol, so it allows POST-data to follow. The content you want lives at a different location, but it will come back here.
     */
    const TEMPORARY_REDIRECT = 307;

    /**
     * Cacheable redirect. Keeps protocol, so it allows POST-data to follow. The content you are redirected will eventually be placed here again. Users
     * should keep requesting this url and continue to be redirected.
     */
    const PERMANENT_REDIRECT = 307;

    public function __construct(UriInterface|string $targetUri, int $statusCode=null, string $reasonPhrase="Redirect", array $headers=[]) {
        if ($statusCode === null) {
            $statusCode = self::CONTINUE_AT;
        }
        $targetUri = (string) $targetUri;

        if($targetUri[0]==='/' && $targetUri[1]!=='/') {
            $targetUri = rtrim(Kernel::$instance->env->baseUrl, '/') . $targetUri;
        }
        parent::__construct(
            "",
            $headers + [
                "Location" => $targetUri,
                "Cache-Control" => "no-cache",
            ],
            $statusCode,
            $reasonPhrase
        );
    }

}
