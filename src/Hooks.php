<?php
namespace Fubber;

class Hooks {
    const MAX_DEPTH = 200;

	private static $listeners = array();
	private static $depth = 0; // record the depth of recursive hooks, to avoid infinite loops
	private static $index = 0;

	public static function getListeners($name) {
		if(!isset(static::$listeners[$name]))
			return [];
		return static::$listeners[$name];
	}
	
	public static function hasListeners($name) {
		return !empty(static::$listeners[$name]);
	}

	public static function dispatch($name, &$p1=null, &$p2=null, &$p3=null, &$p4=null, &$p5=null, &$p6=null, &$p7=null, &$p8=null, &$p9=null, &$p10=null, &$p11=null, &$p12=null)
	{
		if(static::$depth++ > self::MAX_DEPTH) {
            static::stackOverflow($name);
        }

		if (!isset(static::$listeners[$name])) {
			static::$depth--;
			return [];
		}

		$results = array();
		foreach(static::$listeners[$name] as $listener) {
			$results[] = call_user_func_array($listener['callback'], [&$p1, &$p2, &$p3, &$p4, &$p5, &$p6, &$p7, &$p8, &$p9, &$p10, &$p11, &$p12]);
		}
		static::$depth--;
		return $results;
	}
	
	/**
	 * Not tested
	 */
	public static function filter($name, $value, $p1=null, $p2=null, $p3=null, $p4=null, $p5=null, $p6=null, $p7=null, $p8=null, $p9=null, $p10=null, $p11=null, $p12=null) {
		if(static::$depth++ > self::MAX_DEPTH) {
            static::stackOverflow($name);
        }

		if (!isset(static::$listeners[$name])) {
			static::$depth--;
			return $value;
		}

		foreach(static::$listeners[$name] as $listener) {
			$value = call_user_func_array($listener['callback'], [$value, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10, $p11, $p12]);
		}
		
        static::$depth--;
		return $value;
	}


	/**
	*	Will stop after the first "truthy" response
	*/
	public static function dispatchToFirst($name, &$p1=null, &$p2=null, &$p3=null, &$p4=null, &$p5=null, &$p6=null, &$p7=null, &$p8=null, &$p9=null, &$p10=null, &$p11=null, &$p12=null)
	{
		if(!isset(static::$listeners[$name]))
			return null;

		if(static::$depth++ > self::MAX_DEPTH) {
            static::stackOverflow($name);
        }

		foreach(static::$listeners[$name] as $listener)
		{
		    $result = call_user_func_array($listener['callback'], [&$p1, &$p2, &$p3, &$p4, &$p5, &$p6, &$p7, &$p8, &$p9, &$p10, &$p11, &$p12]);

			if($result || is_array($result))
			{
				static::$depth--;
				return $result;
			}
		}
		static::$depth--;
		return null;
	}

	/**
	 * Listen on a specified hook.
	 * 
	 * @param string $name Name or identifier of the hook.
	 * @param mixed $callback Callback
	 * @param int $weight
	 * @return mixed Handle of the listener. Can be used to cancel (unlisten).
	 */
	public static function listen($name, $callback, $weight=0)
	{
        //\header("X-Hook-Listen: ".$name, false);
		if(!isset(static::$listeners[$name]))
			static::$listeners[$name] = array();

		static::$listeners[$name][static::$index] = ['callback' => $callback, 'weight' => $weight];

		usort(static::$listeners[$name], function($a, $b) {
			return $a['weight'] < $b['weight'] ? -1 : ($a['weight'] == $b['weight'] ? 0 : 1);
		});
		
        return $name.":".static::$index++;
	}
	
	public static function unlisten($handle)
	{
	    list($name, $index) = explode(":", $handle);
	    unset(static::$listeners[$name][$index]);
	}

    protected static function stackOverflow($hookName) {
        throw new CodingErrorException('Fubber\Hooks stack overflow. Probably recursive hook (name='.$hookName.').');
    }
}
