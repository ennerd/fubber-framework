<?php
namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when an internal error condition occurs, which
 * is not related to how APIs are being used.
 */
class InternalErrorException extends Exception {

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Internal Error";
    }
}
