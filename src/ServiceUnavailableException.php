<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The HyperText Transfer Protocol (HTTP) 503 Service Unavailable server error response code indicates that the server is not ready to handle the request.
 * The response should contain a Retry-After header, if the resource is expected to become available soon.
 */
class ServiceUnavailableException extends RuntimeException {

    public function getDefaultStatus(): array {
        return [503, "Service Unavailable"];
    }

    public function getExceptionDescription(): string {
        return "Service Unavailable";
    }
}
