<?php
namespace Fubber\Session;

use Fubber\Caching\ICache;
use Fubber\Kernel;
use Fubber\Kernel\State;

class CacheSession implements ISession {
    public readonly string $id;
    protected ICache $_cache;
    protected State $_state;
    protected ?array $_data=null;

    private const DEFAULT_TTL = 3600;
    
    public static function getCurrent(State $state): ISession {
        if (isset($state->cache['SessionId']) ) {
            $sessionId = $state->cache['SessionId'];
        } elseif (isset($state->request->getCookieParams()['SID']) && $state->request->getCookieParams()['SID']) {
            $sessionId = $state->request->getCookieParams()['SID'];
        } elseif (isset($state->request->getQueryParams()['SID']) && $state->request->getQueryParams()['SID']) {
            $sessionId = $state->request->getQueryParams()['SID'];
        } else {
            $sessionId = static::createSessionId();
        }      
        $state->cache['SessionId'] = $sessionId;
        
        if(isset($state->cache[$sessionId])) {
            return $state->cache[$sessionId];
        }
        
        return $state->cache[$sessionId] = new static($state, $sessionId);
    }
    
    /**
     * Creates a cryptographically strong random 128 bit unique id, represented
     * as a 23 character long string
     */
    protected static function createSessionId() {
        $str = openssl_random_pseudo_bytes(16);
        return str_replace("%","", rawurlencode(rtrim(base64_encode($str), "=")));
    }
    
    public function __construct(State $state, string $sessionId) {
        $this->_cache = Kernel::$instance->getCache("session");
        $this->_state = $state;
        $this->id = $sessionId;

        /**
         * This page uses the session, so we must avoid public caching
         */
        $state->response->privateCache();

         // \Fubber\Kernel::init()->config['base_url']);
        //$state->response->setCookie('SID', $sessionId, 0, !empty($urlP['path']) ? $urlP['path'] : '/', $urlP['host'], $urlP['scheme']=='https', true);
    }

    public function getId(): string {
        return $this->id;
    }
    
    public function destroy() {
        $this->_state->response->noCache();
        $this->deleteSessionData();
    }

    /**
     * Load session data from the backend and check if it has expired
     *
     * @param boolean $force Ensure that we actually reload the data
     * @return void
     */
    protected function loadSessionData(bool $force=false): void {
        if ($force || $this->_data === null || $this->_data['@expires'] < time()) {
            $data = $this->_cache->get($this->id);

            if (!is_array($data) || (isset($data['@expires']) && $data['@expires'] < time())) {
                $this->_data = [
                    '@expires' => time() + self::DEFAULT_TTL,
                ];
            } else {
                $this->_data = $data;
            }
        }

        $ttl = $this->_data['@expires'] - time();
        if ($ttl < self::DEFAULT_TTL / 2) {
            $this->_data['@expires'] = time() + self::DEFAULT_TTL;
            $this->saveSessionData();
        }
    }

    protected function saveSessionData(): void {
        $this->addCookieHeader();
        if ($this->_data !== null) {
            $ttl = $this->_data['@expires'] - time();
            $this->_cache->set($this->id, $this->_data, $ttl);
        }
    }

    protected function addCookieHeader(): void {
        $urlP = parse_url(Kernel::init()->env->baseUrl);
        $this->_state->response->setCookie('SID', $this->id, 0, !empty($urlP['path']) ? $urlP['path'] : '/', $urlP['host'], false, true);
        $this->_state->cacheability->privateCache();
    }

    protected function deleteSessionData(): void {
        $this->_cache->set($this->id, null, 0);
        $this->_data = null;
    }
    
    public function __get($key) {
        $this->loadSessionData();

        if (!isset($this->_data[$key])) {
            return null;
        }

        return $this->_data[$key];
    }
    
    public function __set($key, $value) {
        // Make sure we have the most recent
        $this->loadSessionData(true);
        $this->_data[$key] = $value;
        $this->_data['@expires'] = time() + self::DEFAULT_TTL;
        $this->saveSessionData();
    }
    
    public function __unset($key) {
        // Make sure we have the most recent
        $this->loadSessionData(true);
        unset($this->_data[$key]);
        $this->saveSessionData();
    }

    public function __isset($key) {
        $this->loadSessionData();
        return isset($this->_data[$key]);
    }
}
