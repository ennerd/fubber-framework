<?php
namespace Fubber\Session;

use Fubber\Kernel\State;

/**
 *  Sessions are retrieved by Fubber\Service::use(ISession::class)
 */
interface ISession {

    /**
     * Return an ISession instance.
     * 
     * @return ISession
     */
    public static function getCurrent(State $state): self;

    /**
     * Get the session ID.
     * 
     * @return string 
     */
    public function getId(): string;

    /**
     * Destroy the current session and all data, if any.
     */
    public function destroy();

    /**
     * Set a session value. The value must be serializable. Setting a value
     * will immediately trigger a save operation to the backend.
     * 
     * @param $key
     * @param $value
     */
    public function __set($key, $value);

    /**
     * Get a session value. Getting a value will immediately trigger a load
     * operation from the backend.
     * 
     * @param mixed $key 
     * @return mixed 
     */
    public function __get($key);

    /**
     * Unset a session value
     */
}
