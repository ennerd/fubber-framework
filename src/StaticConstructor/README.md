# Fubber\StaticConstructor

Declare static constructors on your classes via PHP Attributes, and have them
automatically run when the class is loaded via the autoloader.

It is important to run `Fubber\StaticConstructor\StaticConstructorProvider::setup()`
as soon as possible in your bootstrap code.

## Declaring a static constructor

```php
use Fubber\StaticConstructor\Constructor;

class SomeClass {

    #[Constructor]
    public static myConstructor() {
        // this function will run immediately after the class has been loaded
    }

    #[Constructor]
    public static otherConstructor() {
        // a class can declare multiple constructors
    }

}
```



## Composer

This package is not designed especially for Compser, but it runs very nicely 
with it.
