<?php
namespace Fubber\StaticConstructor;

use Attribute;

/**
 * Annotate methods that they should be invoked immediately after they have been
 * loaded.
 * 
 * @package Fubber\StaticConstructor
 */
#[Attribute(Attribute::TARGET_METHOD)]
class StaticConstructor {}