<?php
namespace Fubber\StaticConstructor;

use Closure;
use Fubber\I18n\Translatable;
use Fubber\Kernel;
use LogicException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use TypeError;

/**
 * This class integrates with PHP to run static constructors whenever a
 * new class is being loaded.
 */
final class StaticConstructorProvider {

    /**
     * Enable the StaticConstructorProvider and run any constructors which was not
     * previously run.
     * 
     * @throws LogicException 
     * @throws TypeError 
     * @throws LogicException 
     */
    public static function setup(): void {
        if (self::$instance) {
            return;
        }
        self::$instance = new self();
        self::$instance->register();
        self::$instance->detectUnhandledClasses();
    }

    public static function disable(): void {
        if (!self::$instance) {
            return;
        }
        self::$instance->unregister();
    }

    public static function enable(): void {
        self::setup();
        self::$instance->register();
    }

    /**
     * Singleton instance
     * 
     * @var null|StaticConstructorProvider
     */
    private static ?StaticConstructorProvider $instance = null;


    /**
     * List of classes which have been fully processed.
     * 
     * @var array<class-string, bool>
     */
    private array $processedClasses = [];

    /**
     * Counts how many times the autoloader is being invoked without
     * completing. This can occur if a loaded class causes other
     * classes to get autoloaded.
     * 
     * @var int
     */
    private int $isProcessing = 0;

    /**
     * If we detect that our autoloader somehow is not the first autoloader 
     * anymore, we'll re-register it to make it first.
     * 
     * @var bool
     */
    private bool $needsReRegistering = false;

    /**
     * The closure which was registered with \spl_autoload_register(...)
     * 
     * @var null|Closure
     */
    private ?Closure $registeredClosure = null;

    /**
     * Disables instantiating this class externally.
     */
    private function __construct() {}

    /**
     * Register the autoloader
     */
    private function register(): void {
        if ($this->registeredClosure) {
            return;
        }
        \spl_autoload_register($this->registeredClosure = $this->autoloader(...), true, true);
    }

    /**
     * Unregister the autoloader
     */
    private function unregister(): void {
        if (!$this->registeredClosure) {
            return;
        }
        \spl_autoload_unregister($this->registeredClosure);
    }

    /**
     * This autoload handler will run all other autoload handlers, and
     * if any of those autoloaders successfully loads the class, it will
     * use reflection to find any static constructors which needs to be
     * invoked.
     *
     * @param string $className
     * @return void
     */
    private function autoloader(string $className): void {
        ++$this->isProcessing;


        $this->runOtherAutoloaders($className);

        /**
         * Are there other classes that have somehow been loaded
         * and we don't know about?
         */
        if (--$this->isProcessing > 0) {
            return;
        }
        $declaredClasses = \get_declared_classes();
        if (\count($declaredClasses) !== \count($this->processedClasses)) {
            foreach ($declaredClasses as $existingClass) {
                if (!isset($this->processedClasses[$existingClass])) {
                    $this->processClass($existingClass);
                }
            }
        }

        if ($this->needsReRegistering) {
            $this->unregister();
            $this->detectUnhandledClasses();
            $this->register();
        }
    }

    private function runOtherAutoloaders(string $className) {
        foreach (\spl_autoload_functions() as $number => $existingAutoloader) {
            if ($existingAutoloader === $this->registeredClosure) {
                if ($number !== 0) {
                    $this->needsReRegistering = true;
                }
                // don't recursively invoke ourselves
                continue;
            }
            try {
                $existingAutoloader($className);
            } catch (\Throwable $e) {
                // ignoring exceptions just in case
            }

            if (\class_exists($className, false)) {
                $this->processClass($className);
                return;
            }
        }
    }

    /**
     * Search for all loaded classes which haven't had their static
     * autoloader run and then run their constructor methods.
     * 
     * @return bool True if unhandled classes were found
     */
    private function detectUnhandledClasses(): void {
        foreach (\get_declared_classes() as $className) {
            if (!isset($this->processedClasses[$className])) {
                $this->processClass($className);
            }
        }
    }

    private function processClass(string $className): void {
        $this->processedClasses[$className] = true;

        try {
            $rc = new ReflectionClass($className);
            foreach ($rc->getMethods(ReflectionMethod::IS_STATIC) as $rm) {
                if (!empty($rm->getAttributes(StaticConstructor::class))) {
                    $rm->setAccessible(true);
                    $rm->invoke(null);
                }
            }

        } catch (ReflectionException $e) {
            return;
        }
    }

}