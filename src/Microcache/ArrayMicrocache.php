<?php
namespace Fubber\Microcache;

use Closure;
use Fubber\Kernel\Container\FactoryInterface;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\ManifestInterface;
use Fubber\Kernel\Container\Scope;
use Fubber\Microcache\MicrocacheInterface;
use Traversable;

class ArrayMicrocache implements MicrocacheInterface, FactoryInterface {

    public static function getMakerFunctions(): iterable {
        yield function(): static {
            return new static();
        };
    }

    /**
     * Holds the cache values of entries
     * 
     * @var array<string, int|float|string|bool|null>
     */
    private array $values = [];

    /**
     * Holds the expiration time of entries
     * 
     * @var array<string, float>
     */
    private array $expirations = [];

    /**
     * Holds the timestamp for the last unix timestamp that was flushed
     * 
     * @var int
     */
    private int $flushedUntil = \PHP_INT_MAX;

    /**
     * Holds the key names of entries which expire on any 
     * given second.
     * 
     * @var array<int, string[]>
     */
    private array $expirationQueue = [];

    private ?string $namespace = null;

    public function withNamespace(string $namespace): static {
        $child = clone $this;
        $child->expirations = &$this->expirations;
        $child->flushedUntil = &$this->flushedUntil;
        $child->expirationQueue = &$this->expirationQueue;
        if ($child->namespace === null) {
            $child->namespace = $namespace;
        } else {
            $child->namespace .= '/' . $namespace;
        }
        return $child;
    }

    public function getNamespace(): ?string {
        return $this->namespace;
    }

    public function fetch(string $key, Closure $generatorFunction, float $ttl): int|float|string|bool|array|null {
        $realKey = $this->namespace . $key;
        if (!isset($this->values[$realKey]) || $this->expirations[$realKey] < \microtime(true)) {
            $this->flushExpiredEntries();
            $this->values[$realKey] = $generatorFunction();
            $this->expirations[$realKey] = \microtime(true) + $ttl;
            $this->expirationQueue[1 + ((int) $this->expirations[$realKey])][] = $realKey;
        }
        return $this->values[$realKey];
    }

    protected function flushExpiredEntries(): void {
        if ($this->flushedUntil === \PHP_INT_MAX && $this->expirationQueue !== []) {
            /**
             * Find the "oldest" expiration key
             */
            foreach ($this->expirationQueue as $timestamp => $entries) {
                if ($timestamp < $this->flushedUntil) {
                    $this->flushedUntil = $timestamp - 1;
                }
            }
            if ($this->flushedUntil === \PHP_INT_MAX) {
                // the cache is empty
                return;
            }
        }

        $until = \time();
        $microtime = \microtime(true);
        for ($timestamp = $this->flushedUntil; $timestamp < $until; $timestamp++) {
            if (\array_key_exists($timestamp, $this->expirationQueue)) {
                foreach ($this->expirationQueue[$timestamp] as $key) {
                    if (\array_key_exists($key, $this->expirations)) {
                        continue;
                    }
                    // don't flush entries which have already been updated somehow
                    if ($this->expirations[$key] <= $microtime) {
                        unset($this->expirations[$key], $this->values[$key]);
                    }
                }
                unset($this->expirationQueue[$timestamp]);
            }
            $this->flushedUntil = $timestamp;
        }
    }   
}