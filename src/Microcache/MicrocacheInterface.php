<?php
namespace Fubber\Microcache;

use Closure;
use DateInterval;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\NamespacedInterface;
use Throwable;
use Traversable;

/**
 * This interface provides a backend for the simplest, busiest and fastest
 * caching scenarios and is intended for short time server-local caching of
 * data which rarely changes.
 * 
 * The cache is designed especially for caching the results of slightly
 * expensive processing jobs where the source data rarely changes - for
 * example routing tables or dependency injection information.
 * 
 * DO NOT use this interface for caching large amounts of data. This 
 * interface is different from more common cache interfaces PRECICELY
 * because the implementation is given the privilege of adhering to very
 * relaxed concurrency and concistency requirements.
 * 
 * NO CONSISTENCY guarantees between processes and concurrent requests are
 * provided, which means that this type of caching is designed for caching
 * data which rarely changes and is slightly costly to generate.
 * 
 * Implementations SHOULD NOT access the network for caching.
 * 
 * @package Fubber
 */
interface MicrocacheInterface {

    /**
     * Fetch a value from the cache. If the value is not available, the `$generatorFunction`
     * is invoked for fetching the value.
     * 
     * @param string $key 
     * @param Closure $generatorFunction 
     * @param null|int|DateInterval $ttl 
     * @throws Throwable
     * @return int|float|string|bool|array|null
     */
    public function fetch(string $key, Closure $generatorFunction, float $ttl): int|float|string|bool|array|null;
}