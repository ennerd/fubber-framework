<?php
namespace Fubber\Microcache;

use Fubber\Kernel\Container\FactoryInterface;
use Fubber\Microcache\ApcuMicrocache;
use Fubber\Microcache\ArrayMicrocache;
use Traversable;

class MicrocacheFactory implements FactoryInterface {

    public static function getMakerFunctions(): iterable {
        yield from ApcuMicrocache::getMakerFunctions();
        yield from ArrayMicrocache::getMakerFunctions();
    }
    
}