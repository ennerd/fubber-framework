<?php
namespace Fubber\Microcache;

use Closure;
use Fubber\Kernel\Container\FactoryInterface;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\ManifestInterface;
use Fubber\Kernel\Container\Scope;
use Fubber\Microcache\MicrocacheInterface;
use Fubber\Traits\NamespaceableTrait;
use Traversable;

/**
 * Microcache implementation powered by the `apcu` PHP extension.
 * 
 * @package Fubber\MicroCache
 */
class ApcuMicrocache implements MicrocacheInterface, FactoryInterface {

    protected ?string $namespace = null;

    public static function getMakerFunctions(): iterable {
        if (\function_exists('apcu_fetch')) {
            yield function(): static {
                return new static();
            };
        }
    }

    public function withNamespace(string $namespace): static {
        $c = clone $this;
        if ($c->namespace === null) {
            $c->namespace = $namespace;
        } else {
            $c->namespace .= '/' . $namespace;
        }
        return $c;
    }

    public function getNamespace(): ?string {
        return $this->namespace;
    }

    public function fetch(string $key, Closure $generatorFunction, float $ttl): int|float|string|bool|array|null {
        $realKey = $this->getKey($key);

        $entry = \apcu_fetch($realKey, $success);
        if (!$success || $entry->expiration < \microtime(true)) {
            $value = $generatorFunction();
            $entry = $this->createEntryObject($value, $ttl);
            \apcu_store($realKey, $entry, \ceil($ttl));
        }

        return $entry->value;
    }

    public static function isAvailable(): bool {
        return \function_exists('apcu_fetch');
    }

    public static function createInstance(): static {
        return new static();
    }

    /**
     * Creates a cache entry instance object
     * 
     * @internal
     * @param int|float|string|bool|null $value 
     * @param float $ttl 
     * @return object 
     */
    protected function createEntryObject(int|float|string|bool|array|null $value, float $ttl): object {
        return (object) [ 'value' => $value, 'expiration' => \microtime(true) + $ttl ];
    }

    protected function getKey(string $key): string {
        if ($this->namespace === null) {
            return $key;
        }
        return $this->namespace . '/' . $key;
    }
    
}