<?php
namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when a request is not authenticated, when authentication
 * is required.
 *
 * @see UnauthorizedException
 * @see NotAuthenticatedException
 * @see AccessDeniedException
 */
class NotAuthenticatedException extends AccessDeniedException {

    public function getDefaultStatus(): array {
        return [403, "Not Authenticated"];
    }

    public function getExceptionDescription(): string {
        return "Access Denied";
    }
}
