<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\Traits\ExceptionTrait;

/**
 * Exception to be used whenever a user tries to perform an illegal operation, for example
 * if a signature or CSRF token is invalid. The UnauthorizedException should be used when
 * the user IS authenticated, but does not have authorization. The NotAuthenticatedException
 * should be used when the user needs to log in.
 *
 * @see UnauthorizedException
 * @see NotAuthenticatedException
 */
class AccessDeniedException extends RuntimeException {

    public function getDefaultStatus(): array {
        return [403, "Access Denied"];
    }


    public function getExceptionDescription(): string {
        return "Access Denied";
    }
}
