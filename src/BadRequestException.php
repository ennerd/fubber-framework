<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when a request is malformed. For example, the ValidationException extends
 * BadRequestException.
 */
class BadRequestException extends RuntimeException {

    public function getDefaultStatus(): array {
        return [400, "Bad Request"];
    }

    public function getExceptionDescription(): string {
        return "Bad Request";
    }
}
