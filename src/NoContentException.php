<?php
namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception if the user is for when there is no content available.
 */
class NoContentException extends RuntimeException {

    public function getDefaultStatus(): array {
        return [204, "No Content"];
    }


    public function getExceptionDescription(): string {
        return "No Content";
    }
}
