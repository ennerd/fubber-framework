<?php
namespace Fubber;

/**
 * Represents a template with variables, ready to be rendered.
 * 
 * @package Fubber
 */
interface TemplateInterface {
    /**
     * Return the body of the message. This method MUST NOT throw exceptions,
     * and SHOULD return a descriptive error message preferably using the
     * `Fubber\ExceptionTemplate` class
     *
     * return string
     */
    public function __toString();
}