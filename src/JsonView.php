<?php
namespace Fubber;

use Fubber\Psr\HttpMessage\Response;

class JsonView extends Response {
    public function __construct($value, array $headers=[], $statusCode=200, $reasonPhrase="Ok") {
        if(!isset($headers['Content-Type'])) {
            $headers['Content-Type'] = 'application/json';
        }

        parent::__construct(\json_encode($value), $headers, $statusCode, $reasonPhrase);
    }

    public function __toString(): string {
        return (string) $this->getBody();
    }
}
