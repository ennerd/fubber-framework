<?php
namespace Fubber;

interface IValidatable {
    public function isInvalid();
}