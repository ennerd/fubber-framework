<?php
namespace Fubber\BodyParsingMiddleware;

/*
 * Slim Framework (https://slimframework.com)
 *
 * @license https://github.com/slimphp/Slim/blob/4.x/LICENSE.md (MIT License)
 */

use Closure;
use Fubber\PHP\ClosureTool;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

class BodyParsingMiddleware implements MiddlewareInterface {
    /**
     * @var callable[]
     */
    protected $bodyParsers;

    /**
     * @param callable[] $bodyParsers list of body parsers as an associative array of mediaType => callable
     */
    public function __construct(array $bodyParsers = [])
    {
        $this->registerDefaultBodyParsers();

        foreach ($bodyParsers as $mimeType => $parser) {
            $this->registerBodyParser($mimeType, $parser);
        }
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $parsedBody = $request->getParsedBody();
        if (null === $parsedBody || empty($parsedBody)) {
            $parsedBody = $this->parseBody($request);
            if ($parsedBody === null) {
                $parsedBody = [];
            }
            $request = $request->withParsedBody($parsedBody);
        }

        return $handler->handle($request);
    }

    /**
     * @param string   $mimeType a HTTP media type (excluding content-type params)
     * @param callable $callable a callable that returns parsed contents for media type
     */
    public function registerBodyParser(string $mimeType, Closure $callable): self
    {
        $this->bodyParsers[$mimeType] = $callable;

        return $this;
    }

    /**
     * @param string $mimeType a HTTP media type (excluding content-type params)
     */
    public function hasBodyParser(string $mimeType): bool
    {
        return isset($this->bodyParsers[$mimeType]);
    }

    /**
     * 
     * @param string $mimeType 
     * @return callable 
     * @throws RuntimeException 
     */
    public function getBodyParser(string $mimeType): callable
    {
        $mimeType = self::parseMimeType($mimeType);

        if (!isset($this->bodyParsers[$mimeType])) {
            throw new RuntimeException('No parser for type `'.$mimeType.'`');
        }

        return $this->bodyParsers[$mimeType];
    }

    protected function registerDefaultBodyParsers(): void
    {
        $this->registerBodyParser('application/json', static function ($input) {
            $result = \json_decode($input, true);

            if (!\is_array($result)) {
                return null;
            }

            return $result;
        });

        $this->registerBodyParser('application/x-www-form-urlencoded', static function ($input) {
            \parse_str($input, $data);

            return $data;
        });

        $this->registerBodyParser('application/xml', static function ($input) {
            $backup = self::disableXmlEntityLoader(true);
            $backup_errors = \libxml_use_internal_errors(true);
            $result = \simplexml_load_string($input);

            self::disableXmlEntityLoader($backup);
            \libxml_clear_errors();
            \libxml_use_internal_errors($backup_errors);

            if (false === $result) {
                return null;
            }

            return $result;
        });

        $this->registerBodyParser('text/xml', $this->getBodyParser('application/xml'));
    }

    /**
     * @return array<mixed>|object|null
     */
    protected function parseBody(ServerRequestInterface $request): array|object|null
    {
        $contentTypeHeader = $request->getHeader('Content-Type');

        if (empty($contentTypeHeader)) {
            return null;
        }

        $mimeType = self::parseMimeType($contentTypeHeader[0]);

        if ($mimeType === '') {
            return null;
        }

        // Check if this specific media type has a parser registered first
        if (!isset($this->bodyParsers[$mimeType])) {
            // If not, look for a media type with a structured syntax suffix (RFC 6839)
            $parts = \explode('+', $mimeType);
            if (\count($parts) >= 2) {
                $mimeType = 'application/'.$parts[\count($parts) - 1];
            }
        }

        if (isset($this->bodyParsers[$mimeType])) {
            $body = (string) $request->getBody();
            $parsed = $this->bodyParsers[$mimeType]($body);

            if (null !== $parsed && !\is_object($parsed) && !\is_array($parsed)) {
                throw new RuntimeException('Request body media type parser return value must be an array, an object, or null');
            }

            return $parsed;
        } else {
            die("NO BODY PARSER FOR $mimeType");
        }

        return null;
    }

    public static function parseMimeType(string $contentTypeHeader): string {
        if (empty($contentTypeHeader)) {
            return '';
        }
        [ $mimeType ] = \explode(";", $contentTypeHeader, 2);
        return $mimeType;
    }

    protected static function disableXmlEntityLoader(bool $disable): bool
    {
        if (\LIBXML_VERSION >= 20900) {
            // libxml >= 2.9.0 disables entity loading by default, so it is
            // safe to skip the real call (deprecated in PHP 8).
            return true;
        }

        // @codeCoverageIgnoreStart
        return \libxml_disable_entity_loader($disable);
        // @codeCoverageIgnoreEnd
    }
}