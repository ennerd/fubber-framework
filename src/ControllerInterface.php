<?php
namespace Fubber;

/**
 * Common interface for dynamic controller instances
 * 
 * @package Fubber
 */
interface ControllerInterface {

    /**
     * Invoked when the controller is added to the Kernel
     * 
     * @param Kernel $kernel 
     */
    public function load(Kernel $kernel): void;

    /**
     * Invoked when all controllers have been added to the 
     * Kernel
     * 
     * @param Kernel $kernel 
     */
    public function init(Kernel $kernel): void;
    
}