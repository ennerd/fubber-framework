<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when we detect missing or invalid
 * configuration.
 */
class ConfigErrorException extends LogicException {

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Configuration problem on server";
    }
}
