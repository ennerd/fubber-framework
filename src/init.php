<?php
namespace {
    use Fubber\Hooks;
    use Fubber\StaticConstructor\StaticConstructorProvider;

    if (!\defined('DEBUG')) {
        define('DEBUG', !!getenv('DEBUG'));
    }
    
    StaticConstructorProvider::setup();    
}
namespace Fubber {

    use Fubber\I18n\Translatable;
    use Fubber\I18n\TranslatableConjunction;
    use Fubber\I18n\TranslatableInterface;
    use Fubber\Kernel\Config\Config;
    use Fubber\Kernel\Environment\Environment;
    use Fubber\Kernel\State;
    use JsonSerializable;
    use Psr\Http\Message\ServerRequestInterface;

    function kernel(): Kernel {
        return Kernel::instance();
    }

    function trans(string $message, array|JsonSerializable $variables=[]): Translatable {
        $file = \debug_backtrace(false, 1)[0]['file'];
        return new Translatable($message, $variables, $file);
    }
    /**
     * Translate multiple sub-sentences and combine them
     * 
     * @param array $sentenceParts 
     * @param string $conjunction "and", "or", "but", "but not"
     * @param array|JsonSerializable $variables 
     */
    function transConjunction(array $wordsPhrasesClauses, string $none, string $conjunction=' and ', array|JsonSerializable $variables): TranslatableConjunction {
        $file = \debug_backtrace(false, 1)[0]['file'];
        $result = new TranslatableConjunction($conjunction, $none, $variables, $file);
        $result->add(...$wordsPhrasesClauses);
        return $result;
    }

    function route(string $name, int|float|string ...$args): string {
        return Kernel::instance()->router->url($name, ...$args);
    }

    function state(): ?State {
        return kernel()->http->getCurrentState();
    }

    function request(): ServerRequestInterface {
        return state()->request;
    }

    function config(): Config {
        return kernel()->config;
    }

    function environment(): Environment {
        return kernel()->env;
    }
}

