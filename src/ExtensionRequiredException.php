<?php
namespace Fubber;

use Fubber\I18n\Translatable;
use JsonSerializable;

/**
 *  The exception when a PHP extension is required but not available.
 */
class ExtensionRequiredException extends BadFunctionCallException implements JsonSerializable {

    public static function assertInstalled(string $extensionName): void {
        if (!\extension_loaded($extensionName)) {
            throw new static($extensionName);
        }
    }

    public function __construct(string $extensionName) {
        parent::__construct("The `$extensionName` extension is required");
    }

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }
    
    public function getExceptionDescription(): string {
        return "Extension Required";
    }

}