<?php
namespace Fubber;

use Closure;
use Composer\Autoload\ClassLoader;
use Fubber\Caching\ICache;
use Fubber\Caching\NamespacedCache;
use Fubber\Db\IDb;
use Fubber\Kernel\RequestRunners\WebServerRunner;
use Fubber\ExceptionTemplate;
use Fubber\Kernel\Controllers\ControllersMixin;
use Fubber\Kernel\EventLoop\EventLoopMixin;
use Fubber\Hooks\Dispatcher;
use Fubber\Hooks\Event;
use Fubber\Hooks\PerItemTriggers;
use Fubber\Kernel\Extensions\ExtensionsMixin;
use Fubber\Hooks\Trigger;
use Fubber\Kernel\Http\HttpMixin;
use Fubber\Kernel\Phases\Phase;
use Fubber\Kernel\Phases\PhaseHandler;
use Fubber\Kernel\Phases\PhasesMixin;
use Fubber\Logging\ILogger;
use Fubber\Kernel\Router\RouterMixin;
use Fubber\Kernel\Container\ServicesMixin;
use Fubber\Kernel\ExtensionBase;
use Fubber\Kernel\I18n\I18nMixin;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\Kernel\TemplateEngine\TemplateEngineMixin;
use Fubber\Mixins\Mixins;
use Fubber\StaticConstructor\StaticConstructor;
use Fubber\Table\TableController;
use Fubber\Util\GeneratorTool;
use PDO;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use ReflectionClass;
use Throwable;

/**
 * The `Fubber\Kernel` class coordinates loading of controllers and handling requests
 * issued from the web or via the CLI interface.
 * 
 * The bootstrap is trigged by the first call to the method `Fubber\Kernel::init()` and
 * it performs the following steps:
 * 
 * 1. Look for a `.env` file in the folder above the `composer.json` first, and then look
 *    for the file in the same folder as `composer.json`. The environment variables from
 *    that file is imported.
 * 
 * 2. Phase::PHASE_CONSTRUCTING begins by instantiating the Kernel class.
 * 
 * 3. Kernel::prepareConfig() is invoked. Here the application wide configuration is made
 *    available via environment variables through various means. Generally such configuration
 *    variables are set via environment variables (optionally via the .env file).
 * 
 * 4. Modules are initialized. A module is similar to a bundle in Symfony, and it can contain
 *    controllers, models, views and other assets. Modules can be added to the project in
 *    two ways:
 * 
 *    a. Creating a directory under `APP_ROOT/modules/`.
 * 
 *    b. Adding a directory path to the `$GLOBALS['FubberModules']` array. This is intended
 *       for distributing modules using Composer.
 * 
 *    When modules are initialized the following occurs:
 * 
 *    1. If the module directory DOES NOT contain a composer.json, and a folder named `srC/`
 *       exists, it is added to the Composer autoloader with an empty namespace prefix.
 * 
 *    2. If a folder named `config/` exists, that config path is added to the list of config
 *       file locations. This means that a module can bring fallback configuration files for
 *       the application.
 * 
 *    3. If a folder named `views/` exists, that path is added to the list of view file
 *       locations. This means that a module can bring default template files for the application.
 * 
 *    4. if a file named `init.php` exists, it is required.
 * 
 * 5. Services are initialized. A service is an object or function which provides functionality
 *    for the entire project - for example it can be a database connection, an HTTP client or
 *    a logger. Services are defined one of two ways; as class aliases and as services provided
 *    by the `\Fubber\Services\Service` container. Services can be registered in two ways.
 * 
 *    1. If a file `CONFIG_ROOT/class_aliases.php` is found, it is included and the returned array
 *       defines an associative array of $aliasName => $implementationName. Most services provided
 *       by Fubber Framework are accessible via a class in the `Fubber\` namespace.
 * 
 *       For example the default cache implementation is declared as an alias from `\Fubber\cache::class` to
 *       `\Fubber\Caching\DbCache::class` - which means you can create a cache instance by calling
 *       `\Fubber\Cache::create()`. If you change the default cache backend by changing
 * 
 *       This approach is also useful to replace implementations such as the `Fubber\Table` API.
 *
 *    2. If a file `CONFIG_ROOT/services.php` is found, it is included and the returned should contains
 *       an associative array mapping the service id to a service factory function.
 * 
 *    3. Services can also be added via the `Fubber\Service::register($serviceId, $factoryFunction)`
 *       static method.
 * 
 * 6. A file stream wrapper for the fubbercache:// URL scheme is registered. Using this it is possible
 *    to direct any file based caching backend to the Fubber\Cache implementation being used.
 * 
 * 7. Root controllers are loaded. These controllers are implemented via static methods and are therefore
 *    singleton controllers. The `CONFIG/controllers.php` file must return an array of root controllers.
 *    Controllers may extend Fubber\RootController, but it is not required.
 * 
 *    Root controllers can add other root controllers when their `RootController::load($kernel)` method 
 *    is called, by invoking `$kernel->addController($className)`.
 * 
 * 7. The `init` methods of all root controllers is invoked. During this phase, dynamic controllers extending
 *    the `Fubber\DynamicController` class should be instantiated. The contoller instances must call 
 *    `$this->mount()` to become enabled.
 * 
 * 8. 
 *    
 */
final class Kernel extends ExtensionBase implements ContainerInterface {
    use Mixins;
    use PhasesMixin;
    use ExtensionsMixin;
    use EventLoopMixin;
    use ServicesMixin;
    use RouterMixin;
    use HttpMixin;
    use ControllersMixin;
    use TemplateEngineMixin;
    use I18nMixin;

    /**
     * Triggers as the first event in the Kernel's constructor. The listeners
     * receive a completely empty Kernel instance.
     */
    public static PerItemTriggers $onConstructing;

    public readonly Trigger $onConstructorDone;

    /**
     * The Kernel is a singleton and it can be accessed from here from
     * code where we are certain that the Kernel has initialized. This method
     * for accessing the kernel is deprecated and the {@see self::instance()}
     * method is preferred.
     * 
     * @internal Use {@see self::instance()}
     * @var self
     */
    public static ?self $instance = null;

    /**
     * Static constructor for the Kernel class
     */
    #[StaticConstructor]
    private static function staticSetup(): void {
        \mb_internal_encoding('UTF-8');
        self::$onConstructing = new PerItemTriggers("Invoked immediately after an instance of `".static::class."` is constructed, before any properties have been initialized");
    }

    /**
     * Get the file system path to the installation of Fubber Framework.
     * 
     * @return string 
     */
    public static function getFrameworkPath(): string {
        return \dirname(__DIR__);
    }

    /**
     * Get the dir where the `composer.json` file and the `vendor/` directory
     * currently being used.
     * 
     * @return string 
     */
    public static function getComposerPath(): string {
        return dirname((new ReflectionClass(ClassLoader::class))->getFileName(), 3);
    }

    /**
     * Get the current kernel instance. This will load the kernel if it hasn't been loaded
     * earlier.
     * 
     * @deprecated Use Kernel::instance() instead.
     * @return Kernel 
     */
    public static function init(): self {
        return self::instance();
    }

    /**
     * Get the current kernel instance. This will load the kernel if it hasn't been loaded
     * earlier.
     */
    public static function instance(): self {
        if (self::$instance) {
            return self::$instance;
        }

        try {
            new static();
            return self::$instance;
        } catch (\Throwable $e) {
            self::handleFatalException($e);
        }
    }

    /**
     * Serve a request and exit; to be used from bootstrap.php in a standard web
     * server environment. Creates a RequestInterface object and invokes the Kernel
     * as a request handler.
     */
    public static function run() {
        $kernel = static::instance();

        if (isset($_SERVER['REQUEST_METHOD'])) {
            // We're running via a SAPI
            $requestDispatcher = new WebServerRunner();
        } else {
            throw new LogicException("This SAPI is not supported");
        }

        $requestDispatcher->run($kernel->http);
    }

    /**
     * Constructor for the Kernel object. This object is constructed once by
     * calling {@see Fubber\Kernel::init()}
     */
    protected function __construct() {
        $t = \hrtime(true);
        static::$instance = $this;
        $this->onConstructorDone = new Trigger("Kernel constructor done");
        $this->Mixins();
        Dispatcher::configure($this->queueMicrotask(...), $this->runEvents(...), function(Throwable $exception, ?Closure $listener, ?Dispatcher $event) {
            Instrumentation::setExceptionInfo($exception, closure: $listener, description: $event?->getDescription());
            $this->handleFatalException($exception);
        });
        
        self::$onConstructing->triggerFor($this);
        $this->onConstructorDone->trigger();
        self::debug("Total constructor time {time} ms", ['time' => (\hrtime(true) - $t) / 1000000]);
    }    

    #[PhaseHandler(Phase::System)]
    private function addExtraControllers(): void {
        $this->controllers->add(TableController::class);
    }

    #[PhaseHandler(Phase::Ready)]
    public function doReadyPhaseHooks(): void {
        $perf = $this->instrumentation->benchmark('Legacy kernel ready hooks');
        Hooks::dispatch('Kernel.Ready', $controllers);
        $perf();
    }

    /**
     * Shortcut for fetching the default cache implementation.
     *
     * @return ICache
     */
    public function getCache(string $namespace=null): ICache {
        if ($namespace === null) {
            return $this->get(ICache::class);
        }
        return new NamespacedCache($namespace);
    }

    /**
     * Shortcut for fetching the default DB implementation.
     *
     * @return IDb
     */
    public function getDB(): IDb {
        return $this->get(IDb::class);
    }

    /**
     * Shortcut for fetching the default PDO database connection instance.
     */
    public function getPDO(): PDO {
        return $this->get(PDO::class);
    }

    /**
     * Shortcut for fetching the default logger implementation.
     *
     * @param string $logDomain
     * @return ILogger
     */
    public function getLogger(string $logDomain): ILogger {
        return Logger::create($logDomain);
    }

    public static final function debugMode(): bool {
        return !!(\defined('DEBUG') && \DEBUG || \getenv('DEBUG'));
    }

    public static final function handleFatalException(Throwable $e): never {
        if (isset($_SERVER["REQUEST_URI"]) && !\headers_sent()) {
            header("HTTP/1.1 500 Internal Server Error");
            header("Cache-Control: no-store, no-cache, max-age=0, s-maxage=0");
            header("Content-Type: text/html");
        }
        $template = new ExceptionTemplate($e, pageTitle: "Fatal error");
        echo $template;
        die();
    }

    /**
     * Write a DEBUG message to the error log
     * 
     * @param string $message 
     * @param array $vars 
     * @param array $details 
     */
    public static final function debug(string $message, array $vars=[], array $details=[]): void {
        self::log('DEBUG', $message, $vars, $details);
    }

    /**
     * Write a NOTICE to the error log
     * 
     * @param string $message 
     * @param array $vars 
     * @param array $details 
     */
    public static final function notice(string $message, array $vars=[], array $details=[]): void {
        self::log('NOTICE', $message, $vars, $details);
    }

    /**
     * Write an ERROR message to the error log
     * 
     * @param string $message 
     * @param array $vars 
     * @param array $details 
     */
    public static final function error(string $message, array $vars=[], array $details=[]): void {
        self::log('ERROR', $message, $vars, $details);
    }

    private static function log(string $destination, string $message, array $vars=[], array|object $details=null): void {
        static $fp = null, $lastTime = null;
        if ($lastTime === null) {
            $lastTime = \microtime(true);
        }
        if ($fp === null || ($lastTime - \microtime(true)) > 1) {
            $path = self::getComposerPath() . '/fubber-log.txt';
            if (\mt_rand(0, 50) === 0) {
                $pathOld = self::getComposerPath() . '/fubber-log.old';
                if (\filesize($path) > 1000000) {
                    if (\file_exists($pathOld)) {
                        \unlink($pathOld);
                    }
                    \rename($path, $pathOld);
                }
            }
            $fp = \fopen($path, 'ab');
        }
        $pid = \getmypid();

        $bt = \debug_backtrace(0, 3)[2] ?? [];;
        $className = $bt['class'] ?? 'N/A';
        $split = \strrpos($className, '\\');
        if ($split) {
            $className = substr($className, $split + 1);
        }

        $microtime = \microtime(true);
        [$unixTimestamp, $decimalTime] = \explode(".", \number_format($microtime, 6, '.', ''));

        $translates = GeneratorTool::reduceToArray(function(string $value, string $key) {
            $key = \trim($key);
            yield '{' . $key . '}' => $value;
            yield ':' . $key => $value;
        }, $vars);

        $message = \strtr(\rtrim($message), $translates);

        if ($details !== null) {
            $message .= " " . \rtrim(\json_encode($details, \JSON_PRETTY_PRINT), "}\n");
        }

        fwrite($fp, \gmdate('Y-m-d H:i:s', (int) $unixTimestamp) . '.' . $decimalTime . "\t" . \PHP_SAPI . "\t" . $pid . "\t" . $destination."\t" . '['.\preg_replace('/[^a-zA-Z0-9\\\\\\/@\\-][\w\W]*/', '', $className).']'."\t".$message . "\n");
        if ($destination === 'ERROR') {
            \error_log(\preg_replace('/[^a-zA-Z0-9\\\\\\/@\\-][\w\W]*/', '', $className) . "\t" . $message);
        }
    }

    /**
     * Prevent gigantic stack traces
     */
    public function __debugInfo() {
        return [ "fubber" => "kernel" ];
    }
}
