<?php
namespace Fubber\Caching;

class NullCache implements ICache {
	/**
	*	Get a key
	*
	*	@param $key
	*	@return mixed|null
	*/
	public function get($key) {
		return null;
	}

	/**
     *  Set a key
     *
     *  @param $key
     *  @param $val
     *  @param $ttl
     *  @return mixed
     */
	public function set($key, $val=null, $ttl=null) {
        return $val;
	}
	
	/**
	 * Create an instance of a class that implements ICache
	 * 
	 * @return ICache
	 */
	public static function create(): ICache {
	    return new static();
	}
}
