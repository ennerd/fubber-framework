<?php
namespace Fubber\Caching;

use Fubber\CodingErrorException;
use Fubber\Kernel;

class NamespacedCache implements ICache {

    /**
     * A string prefix for all cache keys
     *
     * @var string
     */
    protected string $namespace;

    protected ICache $backend;

    public function __construct(string $namespace) {
        $this->namespace = $namespace;
        $this->backend = Kernel::$instance->get(ICache::class);
    }

    public function get($key) {
        return $this->backend->get($this->buildKey($key));
    }

    public function set($key, $val = null, $ttl = null) {
        return $this->backend->set($this->buildKey($key), $val, $ttl);
    }

    public static function create(): ICache {
        throw (new CodingErrorException("NamespacedCache can't be created this way; needs a namespace"))
        ->withSuggestion("Use Fubber\\Kernel::init()->getCache(\$namespace) to get a namespaced cache");
    }

    /**
     * Build a namespaced cache key by concatenating the namespace and the key
     *
     * @param string $key
     * @return void
     */
    protected function buildKey(string $key) {
        return $this->namespace.':'.$key;
    }
}