<?php
namespace Fubber\Caching;

/**
*	Cache provider interface.
*/
interface ICache {
	/**
     * Get a key
     *
     * @param string $key
     * @return mixed|null
     */
	public function get($key);

	/**
     * Set a key
     *
     * Tip! It is convenient to set the value in your return
     * statement:
     *
     * <code>
     * return Fubber\Cache::create()->set('some-key', $returnValue, 60);
     * </code>
     *
     * @param string $key The identifier for your value
     * @param mixed $val The value
     * @param integer $ttl How long should it be cached?
     *
     * @return mixed Returns the value
     */
	public function set($key, $val=null, $ttl=null);

	/**
	 * Create an instance of a class that implements ICache
	 *
	 * @return ICache
	 */
	public static function create(): self;
}
