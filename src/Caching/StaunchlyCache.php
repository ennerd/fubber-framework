<?php
namespace Fubber\Caching;
use Fubber\Caching\ICache;

/**
 * A caching backend that will try to find a caching backend that would work without configuration.
 * It is intended to be used as a cache engine in development. In production, it is more efficient
 * to configure a backend in your class_aliases.php file.
 */
class StaunchlyCache implements ICache {
	protected $_db;

	protected $cacheProvider;

	public static function create(): ICache {
		static $instance = null;
		if($instance) return $instance;

		return new self();
	}

	protected function createRealBackend() {
		if($this->cacheProvider) {
			return $this->cacheProvider;
        }

		try {
			return $this->cacheProvider = MemcachedCache::create();
		} catch (\Exception $e) {}

		try {
			return $this->cacheProvider = DbCache::create();
		} catch (\Exception $e) {}

		try {
			return $this->cacheProvider = TempFileCache::create();
		} catch (\Exception $e) {}

        return $this->cacheProvider = NullCache::create();
	}

    /**
     *  @param IDb
     */
	public function __construct() {
		$this->createRealBackend();
	}

	/**
     *  @see ICache::get()
     */
    public function get($name) {
        return $this->cacheProvider->get($name);
    }

	/**
     *  @see ICache::set()
     */
	public function set($name, $val=null, $ttl=null) {
        return $this->cacheProvider->set($name, $val, $ttl);
	}
}
