<?php
namespace Fubber\Caching;

use Fubber\Service;

class MemcachedCache implements ICache {
    protected $memcached;
    public function __construct(\Memcached $memcached) {
        $this->memcached = $memcached;
    }
    
	/**
	*	Get a key
	*
	*	@param $key
	*	@return mixed|null
	*/
	public function get($key) {
	    $res = $this->memcached->get($key);

	    if(!$res && $this->memcached->getResultCode()==\Memcached::RES_NOTFOUND) {
	        return null;
	    }

	    return $res;
	}

	/**
     *  Set a key
     *
     *  @param $key
     *  @param $val
     *  @param $ttl
     *  @return $this
     */
	public function set($key, $val=null, $ttl=null) {
		//echo "SET: $key\n";
        if ($ttl === null) {
            $this->memcached->delete($key);
            return $val;
        }
	    if($ttl > 0) {
	        $ttl += time();
        }
	    $this->memcached->set($key, $val, $ttl);
	    return $val;
	}

	/**
	 * Create an instance of a class that implements ICache
	 *
	 * @return ICache
	 */
	public static function create(): ICache {
	    static $instance = null;
	    if($instance) return $instance;

	    $memcached = Service::get(\Memcached::class);

	    return $instance = new static($memcached);
	}
}
