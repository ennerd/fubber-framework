<?php
namespace Fubber\Caching;

class TempFileCache implements ICache {
	public function __construct($tempRoot) {
		$this->tempRoot = $tempRoot;
		if(!is_dir($this->tempRoot)) {
			$umask = umask(0);
			mkdir($this->tempRoot, 0700);
			umask($umask);
		}
		$this->maybeDoPurge();
	}

	protected function maybeDoPurge() {
		if(mt_rand(0,5000) > 0) {
			return;
		}
		$files = glob($this->tempRoot.'/*.tmp'); /* */
		shuffle($files);
		$now = time();
		$since = $now - 43200; // don't look at files newer than 12 hours
		$limit = 200;
		foreach($files as $file) {
			if($limit-- <= 0) return;
			if(filemtime($file) > $since)
				continue;
			$cache = unserialize(file_get_contents($file));
			if($cache['e'] < $now) {
				unlink($file);
			}
		}
	}

	protected function getPathForKey($key) {
		return $this->tempRoot.'/'.md5($key).'.tmp';
	}

	/**
     *  Get a key
     *
     *  @param $key
     *  @return mixed|null
     */
	public function get($key) {
		$path = $this->getPathForKey($key);
		if(!file_exists($path)) {
			return null;
		}
		$res = unserialize(file_get_contents($path));
		if($res && $res['e'] < time()) {
			unlink($path);
			return null;
		}

		return $res['v'];
	}

	/**
     *  Set a key.
     *
     *  @param $key
     *  @param $val
     *  @param $ttl
     *  @return mixed
     */
	public function set($key, $val=null, $ttl=null) {
		$path = $this->getPathForKey($key);

        if ($ttl === null) {
            unlink($path);
        } else {
    		$time = time();
            if($ttl < $time) {
                $ttl += $time;
            }

            $val = [
                'v' => $val,
                'e' => $ttl,
            ];
    		file_put_contents($path, serialize($val));
        }
		return $val;
	}

	/**
	 * Create an instance of a class that implements ICache
	 *
	 * @return ICache
	 */
	public static function create(): ICache {
	    static $instance = null;
	    if($instance) return $instance;

	    return $instance = new static(sys_get_temp_dir().'/'.md5(__FILE__));
	}
}
