<?php
namespace Fubber\Caching;

use Fubber\Service;
use Fubber\Caching\ICache;
use Fubber\Db\IDb;

/**
*	Cache that uses the database as a backend. Should perform quite well for a small site.
*/
class DbCache implements ICache {
	protected $_db;

	public static function create(): ICache {
        // static caching is bad, because we might run multiple concurrent requests in one PHP invocation with libraries such as Swoole
        return new static(Service::use(IDb::class));

		static $instance = null;
		if($instance) return $instance;
//		return $instance = new self(\Nerd\Glue::get(\Fubber\Db\IDb::class));
	}

	/**
	*	@param IDb
	*/
	public function __construct(\Fubber\Db\IDb $db) {
		$this->_db = $db;
		if(mt_rand(0, 5000)===0) {
			$this->_db->exec('DELETE FROM ff_cache WHERE expires < NOW()');
		}
	}

	/**
	*	@see ICache::get()
	*/
	public function get($name) {
		try {
			$res = $this->_db->queryField("SELECT val FROM ff_cache WHERE id=? AND expires>NOW()", array($name));
			if($res) return unserialize($res);
		} catch(\PDOException $e) {}

		return null;
	}

	/**
	*	@see ICache::set()
	*/
	public function set($name, $val=null, $ttl=null) {
		if($ttl===NULL) {
            $this->_db->exec('DELETE FROM ff_cache WHERE id=?', [$name]);
            return $val;
        }
		try {
			$val = serialize($val);
			$expires = date('Y-m-d H:i:s', time()+$ttl);
			$this->_db->exec('INSERT INTO ff_cache (id,val,expires) VALUES (?,?,?) ON DUPLICATE KEY UPDATE val=?, expires=?', array(
				$name, $val, $expires,
				$val, $expires
			));
            return $val;
		} catch (\PDOException $e) {
			if($e->getCode()=='22001')
				$this->_db->exec('DROP TABLE ff_cache');
			$this->_db->exec("CREATE TABLE ff_cache (id VARCHAR(100), val MEDIUMBLOB, expires DATETIME, PRIMARY KEY(id)) CHARACTER SET utf8 COLLATE utf8_bin");
			return $this->set($name, $val, $ttl);
		}
	}
}
