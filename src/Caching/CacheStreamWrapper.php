<?php
namespace Fubber\Caching;

use Fubber\Cache;

/**
 * A stream wrapper with the following characteristics
 * 
 * [name]://[host]/[path]
 * 
 * name:    The name used with stream_wrapper_register
 * host:    The time to live, in seconds for files written. Ignored for files read.
 * path:    The cache item key
 */

class CacheStreamWrapper {
    protected $path;
    protected $mode;
    protected $options;
    protected $position;
    protected $val;
    protected $dirty = false;
    protected $allowRead = false;
    protected $allowWrite = false;
    
    protected $cache;
    
    public function __construct() {
        $this->cache = Cache::create();
    }

    /**
     * Facade functions. Override these to use different caching system
     */
    protected function cacheGet($key) {
        $res = $this->cache->get($key);
        return $res;
    }
    protected function cacheSet($key, $val, $ttl) {
        $this->cache->set($key, $val, $ttl);
    }
    protected function cacheDel($key) {
        $this->cache->set($key);
    }
    
    protected function fetch() {
        $p = parse_url($this->path);
        $this->file = $this->cacheGet($p['path']);
    }
    
    protected static function make_stat_array($data) {
        $uid = posix_getuid();
        $gid = posix_getgid();
        return [
            0 => 0,
            "dev" => 0,
            1 => 0,
            "ino" => 0,
            2 => 0,
            "mode" => 0,
            3 => 0,
            "nlink" => 0,
            4 => $uid,
            "uid" => $uid,
            5 => $gid,
            "gid" => $gid,
            6 => 0,
            "rdef" => 0,
            7 => $data['s'],
            "size" => $data['s'],
            8 => 0,
            "atime" => 0,
            9 => $data['m'],
            "mtime" => $data['m'],
            10 => $data['m'],
            "ctime" => $data['m'],
            11 => -1,
            "blksize" => -1,
            12 => -1,
            "blocks" => -1,
            ];
    }
    
    public function stream_set_option($option, $arg1, $arg2) {
        return false;
    }

    public function stream_stat() {
        return static::make_stat_array($this->file);
    }
    
    /**
     * Retrieve information about a file. This goes directly to the cache for information.
     * 
     * @param string $path
     * @param int $flags
     * @return array
     */
    public function url_stat($path, $flags) {
        $path = substr($path, 14);
               
        $val = $this->cacheGet($path);
        if(!$val) {
            if(!($flags & STREAM_URL_STAT_QUIET)) {
                trigger_error("File $path not found $flags ".STREAM_URL_STAT_QUIET, E_USER_WARNING);
            }
            return false;
        } 
        
        return static::make_stat_array($val);
    }

    /**
     * Initialize everything
     * 
     * @param string Path being queried
     * @param int File opening mode
     * @param int Options (STREAM_USE_PATH | STREAM_REPORT_ERRORS)
     * @param string Opened path
     * @return boolean
     */
    public function stream_open($path, $mode, $options, &$opened_path) {
        $this->path = $path;
        $this->mode = $mode;
        $p = parse_url($this->path);
        $ttl = intval($p['host']);
        
        $reportErrors = (($options & STREAM_REPORT_ERRORS) > 0);
        
        $mode = rtrim($mode, "bt"); // We ignore these flags, and always use mode binary
        switch($mode) {
            case 'r' : // Open read only, file pointer at beginning of file
                $this->fetch();
                $this->allowRead = true;
                $this->position = 0;
                break;
            case 'r+' : // Open read write
                $this->fetch();
                $this->allowRead = true;
                $this->allowWrite = true;
                $this->position = 0;
                break;
            case 'w' : // Write only, truncate
                $this->file = ['s' => 0, 'm' => time(), 'd' => '', ];
                $this->allowWrite = true;
                $this->position = 0;
                $this->dirty = true;
                break;
            case 'w+' : // Read and write, truncate
                $this->file = ['s' => 0, 'm' => time(), 'd' => '', ];
                $this->allowWrite = true;
                $this->allowRead = true;
                $this->position = 0;
                $this->dirty = true;
                break;
            // Append modes are not supported
            case 'a' :
            case 'a+' :
                if($reportErrors) {
                    trigger_error("The Fubber Framework Cache Stream Wrapper does not support append mode", E_USER_NOTICE);
                }
                return false;
            
            case 'x+' : // Same as x, plus allow read
                $this->allowRead = true;
            case 'x' : // Fail if the value already exists, open for writing only
                $this->fetch();
                if($this->file) {
                    if($reportErrors) {
                        trigger_error("File already exists", E_USER_NOTICE);
                    }
                    return false;
                }
                $this->file = ['s' => 0, 'm' => time(), 'd' => '', ];
                $this->allowWrite = true;
                $this->position = 0;
                $this->dirty = true;
                break;

            // Create without truncate is not supported
            case 'c' :
            case 'c+' :
                if($reportErrors) {
                    trigger_error("The Fubber Framework Cache Stream Wrapper does not support writing to existing files. Must always overwrite.", E_USER_NOTICE);
                }
                return false;
            default :
                if($reportErrors) {
                    trigger_error("The Fubber Framework Cache Stream Wrapper does not support the mode '$mode'.", E_USER_NOTICE);
                }
                return false;
        }
        return true;
    }
    
    /**
     * Return some bytes and update position
     * 
     * @param int Number of bytes to read
     * @return string
     */
    public function stream_read($count) {
        $ret = substr($this->file['d'], $this->position, $count);
        $this->position += strlen($ret);
        return $ret;
    }
    
    /**
     * Overwrite data at the current position and update position.
     * 
     * @return int Number of bytes written
     */
    public function stream_write($data) {
        $this->dirty = true;
        $len = strlen($data);
        $left = substr($this->file['d'], 0, $this->position);
        $right = substr($this->file['d'], $this->position + $len);
        $this->file['d'] = $left.$data.$right;
        $this->position += $len;
        return $len;
    }
    
    public function stream_truncate($new_size) {
        $this->dirty = true;
        $this->file['d'] = substr($this->file['d'], 0, $new_size);
    }
    
    public function unlink($path) {
        $p = parse_url($path);
        $current = $this->cacheGet($p['path']);
        if(!$current) return false;
        $this->cacheDel($p['path']);
        return true;
    }
    
    /**
     * Get the current offset
     * 
     * @return int
     */
    public function stream_tell() {
        //echo "stream_tell()\n";
        return $this->position;
    }
    
    public function stream_eof() {
        return $this->position >= strlen($this->val);
    }
    
    /**
     * Seek to an arbitrary offset in the stream
     * 
     * @param int $offset Position to seek to
     * @param int $whence One of SEEK_SET, SEEK_CUR, SEEK_END
     * @return boolean
     */
    public function stream_seek($offset, $whence) {
        //echo "stream_seek($offset, $whence)\n";
        switch ($whence) {
            case SEEK_SET:
                if ($offset < strlen($this->file['d']) && $offset >= 0) {
                     $this->position = $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            case SEEK_CUR:
                if ($offset >= 0) {
                     $this->position += $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            case SEEK_END:
                $strlen = strlen($this->file['d']);
                if ($strlend + $offset >= 0) {
                     $this->position = $strlen + $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            default:
                return false;
        }
    }
    
    public function stream_flush() {
        if(!$this->dirty) return true;
        $p = parse_url($this->path);
        
        $ttl = intval($p['host']);
        $key = $p['path'];
        $this->file['s'] = strlen($this->file['d']);
        $this->file['m'] = time();
        return $this->cacheSet($p['path'], $this->file, $ttl);
    }
    
    /**
     * Set some metadata about the stream
     */
    public function stream_metadata($path, $option, $var) {
        die("Not supported yet");
        if($option == STREAM_META_TOUCH) {
            $url = parse_url($path);
            $varname = $url["host"];
            if(!isset($GLOBALS[$varname])) {
                $GLOBALS[$varname] = '';
            }
            return true;
        }
        return false;
    }
}
