<?php
namespace Fubber;

/**
 * A root controller is PHP class which bootstraps the application. Which root controllers
 * get loaded is determined by the `$config/controllers.php` file, which is a PHP file that
 * returns an array of class names.
 * 
 * Root controllers are not instantiated and they are responsible for the core architecture
 * and development environment of your application. They integrate with the framework via
 * static methods as documented in this class.
 * 
 * A root controller does not have to extend this class (but it doesn't hurt); the purpose 
 * of this class is to document root controllers.
 */
class RootController {

    /**
     * The load method of controller classes is invoked immediately after the
     * controller has been added to the application. The load method should not make
     * assumptions about the existence of particular configuration variables or if other
     * controllers have been loaded.
     * 
     * Controllers can add other controllers via the {@see Kernel::addController} method.
     * 
     * The first root controllers are loaded based on the array of class names which is
     * returned from the `CONFIG/controllers.php` config file.
     *
     * @param Kernel $kernel
     * @return void
     */
    public static function load(Kernel $kernel): void {}

    /**
     * When all controllers have loaded, this method is invoked. 
     *
     * @param Kernel $kernel
     * @return void
     */
    public static function init(Kernel $kernel): void {}

    /**
     * Undocumented function
     *
     * @param Kernel $kernel
     * @return array
     */
    public static function routes(Kernel $kernel): array {
        return [];
    }
}