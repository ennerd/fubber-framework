<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\Traits\ExceptionTrait;
use RuntimeException;

/**
 * The exception that is thrown when a resource cannot be found.
 */
class NotFoundException extends RuntimeException implements IException {
    use ExceptionTrait;

    protected function getDefaultStatus(): array { 
        return [404, 'Not found'];
    }

    public function getExceptionDescription(): string {
        return "The resource requested was not found";
    }

}
