<?php
namespace Fubber\LRU;

use Closure;

/**
 * @template T
 * @package Fubber\LRU
 * @extends LRUCacheInterface<T>
 */
class LRUMemoryCache implements LRUCacheInterface {
    protected int $capacity;
    protected ?Closure $typeCheckFunction;
    protected array $cache = [];

    /**
     * @param int $capacity 
     * @param Closure(T):void|null $typeCheckFunction 
     */
    public function __construct(int $capacity, Closure $typeCheckFunction=null) {
        $this->capacity = $capacity;
        $this->typeCheckFunction = $typeCheckFunction;
    }

    /**
     * @param string $key 
     * @param Closure():T $generatorFunction 
     * @return T|null 
     */
    public function fetch(string $key, Closure $generatorFunction) {
        if (isset($this->cache[$key])) {
            // refresh the value so that it's not evicted by adding it to the end of the array
            $value = $this->cache[$key];
            unset($this->cache[$key]);
            $this->cache[$key] = $value;
        } else {
            $value = $generatorFunction();
            // type check
            if ($this->typeCheckFunction !== null) {
                ($this->typeCheckFunction)($value);
            }
            $this->cache[$key] = $value;
            // evict values if we are at capacity
            while (count($this->cache) > $this->capacity) {
                \array_shift($this->cache);
            }
        }
        return $value;
    }
}
