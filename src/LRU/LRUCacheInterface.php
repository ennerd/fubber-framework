<?php
namespace Fubber\LRU;

use Closure;

/**
 * @template T
 * @package Fubber\LRU
 */
interface LRUCacheInterface {
    /**
     * @param string $key 
     * @param Closure():T $generatorFunction 
     * @param float $ttl 
     * @return T 
     */
    public function fetch(string $key, Closure $generatorFunction);
}
