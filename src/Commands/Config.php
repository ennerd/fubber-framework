<?php
namespace Fubber\Commands;

class Config extends Command {

	public function __construct() {
		parent::__construct();
		$this->setHelp("Prints the current configuration");
	}

	public function invoke() {
		var_export(\Fubber\Kernel::init()->env);
	}

}
