<?php
namespace Fubber\Commands;
use Doctrine\Common\Inflector\Inflector;

class CreateTableController extends Command {

	public function __construct() {
		parent::__construct();
		$this->argument()
			->referToAs("class")
			->require()
			->describedAs("The Table class name");
			
		$this->argument()
			->referToAs("view-prefix")
			->require()
			->describedAs("The view prefix, for example 'user' will create /user/, /user/create/, /user/1/ and so on.");
			
		$this->option('namespace')
			->aka('t')
			->describedAs('Optional namespace to use for the generated class');
			
		$this->setHelp("Usage: fubber CreateTableController <class>\nGenerates a CRUD-controller based on a Table class.");
	}
	
	public function invoke() {
		$kernel = \Fubber\Kernel::init();
		
		$className = $this[1];
		$prefix = $this[2];
		
		if(!class_exists($className))
			die("Error: Class $className not found\n");
			
		if(!is_subclass_of($className, 'Fubber\Table'))
			die("Error: Class $className must extend Fubber\Table\n");
			
		$ref = new \ReflectionClass($className);
		
		$res = '<?php
';
		if($this['namespace']) {
			$res .= "namespace ".$this['namespace'].";\n";
		}
		
		$res .= "use Fubber\Util\Errors;\n\n";

		$res .= "class $className"."Controller {\n";
		$res .= "
	/**
	* The page that lists all objects
	*
	* @route GET /$prefix/
	*/
	public static function index(\$state) {
		\$rows = $className::all();
		return new \Fubber\View(".var_export($prefix.'/index', true).", ['rows' => \$rows]);
	}
	
	/**
	* The page where you create new objects. Accepts the continue_to get parameter,
	* in which case it will add the '$className=\$id' get parameter.
	*
	* @route GET|POST /$prefix/create/
	*/
	public static function create(\$state) {
		\$$prefix = new $className();
		\$form = new $className"."Form(\$$prefix);
		if(\$form->success) {
			new \Fubber\FlashMessage('Created');
			if(isset(\$state->request->get['continue_to']))
				\$continueTo = \Fubber\Util\Url::create(\$state->request->get['continue_to'])->setParam(".var_export($className, true).", \$$prefix"."->id);
			else
				\$continueTo = ".var_export("/$prefix/", true).".\$$prefix"."->id.'/';
				
			return new \Fubber\Redirect(\$continueTo);
		}
		return new \Fubber\View(".var_export($prefix.'/create', true).", ['form' => \$form, ".var_export($prefix, true)." => \$$prefix]);
	}
	
	/**
	* The page where you view an object
	*
	* @route GET /$prefix/{id:\d+}/
	*/
	public static function retrieve(\$state, \$id) {
		\$$prefix = $className::load(\$id);
		return new \Fubber\View(".var_export($prefix.'/retrieve', true).", [".var_export($prefix, true)." => \$$prefix]);
	}
	
	/**
	* The page where you edit an object
	*
	* @route GET|POST /$prefix/{id:\d+}/update/
	*/
	public static function update(\$state, \$id) {
		\$$prefix = $className::load(\$id);
		\$form = new $prefix"."Form(\$$prefix);
		if(\$form->success) {
			new \Fubber\FlashMessage('Saved');
			if(isset(\$state->request->get['continue_to']))
				\$continueTo = \Fubber\Util\Url::create(\$state->request->get['continue_to'])->setParam(".var_export($className, true).", \$$prefix"."->id);
			else
				\$continueTo = ".var_export("/$prefix/", true).".\$$prefix"."->id.'/';
				
			return new \Fubber\Redirect(\$continueTo);
		}
		return new \Fubber\View(".var_export($prefix.'/update', true).", ['form' => \$form, ".var_export($prefix, true)." => \$$prefix]);
	}
	
	/**
	* The page where you delete an object
	*
	* @route GET /$prefix/{id:\d+}/delete/
	*/
	public static function delete(\$state, \$id) {
		\$$prefix = $className::load(\$id);
		if(isset(\$state->request->get['confirmed']) && \$state->request->get['confirmed']=='true') {
			if(isset(\$state->request->get['continue_to']))
				\$continueTo = \Fubber\Util\Url::create(\$state->request->get['continue_to'])->setParam(".var_export($className, true).", \$$prefix"."->id);
			else
				\$continueTo = null;

			if(\$$prefix"."->delete()) {
				new \Fubber\FlashMessage(".var_export("You have deleted the $prefix", true).", \Fubber\FlashMessage::SUCCESS);
				if(!\$continueTo)
					\$continueTo = ".var_export("/$prefix/", true).";
			} else {
				new \Fubber\FlashMessage(".var_export("Failed deleting $prefix", true).", \Fubber\FlashMessage::FAILURE);
				if(!\$continueTo)
					\$continueTo = ".var_export("/$prefix/", true).".'/'.\$$prefix"."->id.'/';
			}
			return new \Fubber\Redirect(\$continueTo);
		}
		return new \Fubber\View(".var_export($prefix.'/delete', true).", [".var_export($prefix, true)." => \$$prefix, 'confirmUrl' => \Fubber\Util\Url::current()->setParam('confirmed','true')]);
	}
}";
		echo $res;
		

	}

}
