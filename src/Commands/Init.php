<?php
namespace Fubber\Commands;

class Init {
    public static function command($cmd) {
        if(is_dir(getcwd().'/app'))
            die("There is a folder named 'app' here already.\n");
        
        $root = getcwd();
            
        echo "Configuring a Fubber Framework application. Please answer the following questions:\n\n";
        
        /**
         * DATABASE SETUP
         */
        $infoJson = false;
        if(file_exists($root.'/../info.json')) {
            echo "Found $root/../info.json: ";
            $infoJson = json_decode(file_get_contents($root.'/../info.json'), true);
            if(!isset($infoJson['mysql'])) {
                echo "Not using. Missing MySQL configuration.\n";
                $infoJson = false;
            } else {
                echo "Using this configuration!\n";
            }
        }

        if(!$infoJson) {
            $dbHost = 'localhost';
            $dbName = null;
            $dbUser = null;
            $dbPass = null;
            
            $dbConfig = null;
            
            // See if there is info.json file somewhere near
            while(true) {
                $dbHost = self::prompt('Database host', $dbHost);
                $dbName = self::prompt('Database name', $dbName);
                $dbUser = self::prompt('Database username', $dbUser);
                $dbPass = self::prompt('Database password', $dbPass);
                
                try {
                    $dbConfig = [];
                    $db = new \PDO(
                        $dbConfig['dsn'] = 'mysql:host='.$dbHost.';dbname='.$dbName, 
                        $dbConfig['user'] = $dbUser,
                        $dbConfig['password'] = $dbPass
                        );
                    echo "Database connection successful.\n";
                    break;
                } catch (\PDOException $e) {
                    echo "Unable to connect to the database using the following configuration:\n";
                    var_export($dbConfig);
                    echo "\n";
                }
            }
        }
        
        $fubberConfig = [
            'base_url' => null,
            'salt' => md5(mt_rand(0,9999).microtime(true)),
            'email_address' => 'no-reply@example.com',
            'email_name' => 'Your Service',
            'userfiles' => $root.'/files',
            'userfiles_url' => null,
            'smtp' => [
                'host' => 'localhost',
                'port' => '25',
                ],
            'debug' => false,
            'factories' => [
                Fubber\Templating\ITemplate::class => [Fubber\Blade\Template::class, 'create'],
                ],
            ];
            
        $fubberConfig['base_url'] = rtrim(self::prompt('Base URL for your site (http://your.domain.com)'), '/');
        $fubberConfig['email_address'] = self::prompt("E-mail address for outgoing e-mail");
        $fubberConfig['email_name'] = self::prompt("Name for sender of outgoing e-mail");
        $fubberConfig['userfiles'] = self::prompt("Path for publicly accessible files", $fubberConfig['userfiles']);
        $fubberConfig['userfiles_url'] = self::prompt("URL for publicly accessible files", $fubberConfig['base_url'].'/files');
        $fubberConfig['privfiles'] = self::prompt("Path for private persistent files", dirname($root).'/'.basename($root).'-priv');
        $fubberConfig['smtp']['host'] = self::prompt("SMTP host", $fubberConfig['smtp']['host']);
        $fubberConfig['smtp']['port'] = self::prompt("SMTP port", $fubberConfig['smtp']['port']);
            
        if(self::prompt('Create '.$root.'/app/ and configure your application?', ['y' => 'Yes', 'n' => 'No']) == 'n' )
            die();
            
        mkdir($root.'/app');
        mkdir($root.'/app/assets');
        mkdir($root.'/app/config');
        mkdir($root.'/app/src');
        mkdir($root.'/app/migrations');
        mkdir($root.'/app/views');
        
        file_put_contents($root.'/fubber-config.php', '<?php return '.var_export($fubberConfig, true));
        file_put_contents($root.'/index.php', '<?php
require("vendor/autoload.php");
Fubber\Kernel::run();');
        file_put_contents($root.'/phinx.php', '<?php
$reflect = new \ReflectionClass("\Fubber\Kernel");
$path = dirname($reflect->getFileName());
$config = require($path."/phinx.php");

return $config;');
        file_put_contents($root.'/.htaccess', '<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
');

        echo 'You should add 
    "autoload": {
        "psr-4": {
            "": "app/src/"
        }
    }
to your composer.json file.

If you want to use Fubber Extras, run the following command:

git clone http://bitbucket.org/ennerd/fubber-extras.git '.$root.'/app/extra
';
        die();
    }
    
    public static function prompt($question, $answers=null) {
        while(true) {
            if(is_array($answers)) {
                $default = false;
                $alts = [];
                foreach($answers as $k => $v) {
                    if($k == strtoupper($k)) {
                        if($default) throw new \Fubber\CodingErrorException("Can't have multiple default answers in prompt.");
                        $default = $k;
                    }
                    $alts[] = $k;
                }
                $res = readline($question.' ['.implode($alts).']: ');
                if(trim($res)=='' && $default) {
                    return $res;
                }
                
                if(isset($answers[$res]))
                    return $res;
                
                echo "Invalid choice. Valid alternatives are:\n";
                foreach($answers as $k => $v) {
                    echo " $k $v\n";
                }
            } else if(is_string($answers)) {
                $res = readline($question.' ['.$answers.']: ');
                if(trim($res)=='')
                    return $answers;
                return $res;
            } else {
                $res = readline($question.': ');
                if(trim($res)!='')
                    return $res;
                echo "Invalid choice.\n";
            }
        }
    }
}