<?php
namespace Fubber\Commands;

use PDO;
use Fubber\Service;
use Doctrine\Common\Inflector\Inflector;

class CreateTableClass extends Command {

	public function __construct() {
		parent::__construct();
		$this->argument()
			->referToAs("table")
			->require()
			->describedAs("The database table name");

		$this->option('namespace')
			->aka('t')
			->describedAs('Optional namespace to use for the generated class');

		$this->setHelp("Usage: fubber CreateTableClass <table> <class>\nGenerates a proper Fubber\Table class based on the data model.");
	}

	protected static function tableToClass($tableName) {
		if(trim($tableName, 'abcdefghijklmnopqrstuvwxyz_')!=='') die("Table names must be in the form 'table_name' and can't contain numbers.\n");

		$parts = explode("_", $tableName);
		$finalParts = [];
		foreach($parts as $part) {
			$finalParts[] = ucfirst(Inflector::singularize($part));
		}
		return implode("", $finalParts);
	}

	public function invoke() {
		$kernel = \Fubber\Kernel::init();
        $db = Service::use(PDO::class);
//      $db = \Nerd\Glue::get(\PDO::class);

		// First, we must check that the table name is in the correct form
		$tableName = $this[1];

		$className = self::tableToClass($tableName);


		$cols = $db->query('DESC '.$this[1])->fetchAll(\PDO::FETCH_ASSOC);

		$constraints = $db->query("SELECT
				table_name AS 'from_table',
			    column_name AS 'from_column',  
			    referenced_table_name AS 'to_table',
			    referenced_column_name AS 'to_column'
			FROM
			    information_schema.key_column_usage
			WHERE
			    referenced_table_name IS NOT NULL")->fetchAll(\PDO::FETCH_ASSOC);

		$colNames = [];
		$colProps = [];
		$validators = [];
		$primaryKeys = [];
		foreach($cols as $col) {
			if($col['Key']=='PRI') {
				$primaryKeys[] = var_export($col['Field'], true);
			}
			$colNames[] = var_export($col['Field'], true);
			$colProps[] = '	public $'.$col['Field'].';';
			if($col['Null']=='NO' && $col['Extra']!=='auto_increment')
				$validators[] = '		$errors->notNull('.var_export($col['Field'], true).');';
			if($col['Type']=='datetime') {
				$validators[] = '		$errors->datetime('.var_export($col['Field'], true).');';
			} else if(substr($col['Type'], 0, 4)=='int(') {
				$len = intval(substr($col['Type'], 4));
				$validators[] = '		$errors->integer('.var_export($col['Field'], true).');';
				$validators[] = '		$errors->maxLen('.var_export($col['Field'], true).', '.$len.');';
			} else if(substr($col['Type'], 0, 8)=='varchar(') {
				$len = intval(substr($col['Type'], 8));
				$validators[] = '		$errors->maxLen('.var_export($col['Field'], true).', '.$len.');';
			}
			$validators[] = '';
		}
		
		$res = <<<EOT
<?php

EOT;

		if($this['namespace']) {
			$res .= "namespace ".$this['namespace'].";\n";
		}
		
		$res .= "use Fubber\Util\Errors;
		
class {$className} extends \Fubber\Table {
	public static \$_table = ".var_export($this[1], true).";
	public static \$_cols = [".implode(", ", $colNames)."];
	public static \$_primaryKey = [".implode(", ", $primaryKeys)."];
	
".implode("\n", $colProps)."

	public function isInvalid() {
		\$errors = new Errors(\$this);
		
".implode("\n", $validators)."
		return \$errors->isInvalid();
	}
";

		// Auto generate getter methods for relationships
		
		// one-to-many, getWhatevers()
		// from_table = other_table
		// from_column = !primary_key
		
		foreach($constraints as $cons) {
//			var_dump($cons);
			$toClass = self::tableToClass($cons['to_table']);
			$fromClass = self::tableToClass($cons['from_table']);

			if($cons['from_table'] == $tableName) {
				
				$getMethod = 'get'.$toClass;
				$allMethod = 'allBy'.$toClass;

				if(strpos($cons['from_column'], $cons['to_table'])!==0) {
					// Must end with $cons['to_column']
					if(substr($cons['from_column'], -(strlen($cons["to_column"])+1)) == "_".$cons["to_column"]) {
						$getMethod = 'get'.Inflector::classify(substr($cons['from_column'], 0, -(strlen($cons["to_column"])+1)));
						$allMethod = 'allBy'.Inflector::classify(substr($cons['from_column'], 0, -(strlen($cons["to_column"])+1)));
					}
				}
				
				$res .= "
	public function $getMethod() {
		static \$cache;
		if(\$cache) return \$cache;
		return \$cache = $toClass::load(\$this->".$cons['from_column'].");
	}
	
	public static function $allMethod($toClass \$o) {
		return self::all()->where(".var_export($cons['from_column'], true).", '=', \$o->".$cons['to_column'].");
	}
	";
			}
			
			if($cons['to_table'] == $tableName) {
				
				$res .= "
	public function get".Inflector::pluralize($fromClass)."() {
		return $fromClass::all()->where(".var_export($cons['from_column'], true).", '=', \$this->".$cons['to_column'].");	
	}";
			}
		}
		
		$res .= "
}";

		echo $res;

	}

}
