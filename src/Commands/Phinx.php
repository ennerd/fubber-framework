<?php
namespace Fubber\Commands;

class Phinx {
    /**
     * Provides the fubber phinx command
     */
    public static function command($cmd) {
        echo "Creating phinx.php config file: ";
        static::setup();
        echo "DONE\n";
        die();
    }
    
    public static function setup() {
		$kernel = \Fubber\Kernel::init();
		$file = $kernel->env->root.'/phinx.php';
		if(file_exists($file)) {
		    die("FAILED: $file exists\n");
        }
		file_put_contents($file, '<?php
$reflect = new \ReflectionClass("\Fubber\Kernel");
$path = dirname($reflect->getFileName());
$config = require($path."/phinx.php");

return $config;');
        
    }
}