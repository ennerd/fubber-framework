<?php
namespace Fubber\Commands;
use Fubber\Hooks;

class Command extends \Commando\Command {

	public final static function run() {
		$cmd = new self();
		$cmd->invoke();
	}
	
	public static function catchall($cmd) {
		switch($cmd[0]) {
			case "init" :
				return Init::command($cmd);
			case "phinx":
				return Phinx::command($cmd);
			case "db":
				return Db::command($cmd);
			default:
				return null;
		}
	}
	
	public static function commands($cmd) {
		return [
			"init" => "Setup Fubber Framework app",
			"phinx" => "Configure phinx migrations",
			"db" => "Manage database model",
			];
	}

	public function invoke() {
		$this->printHelp();
	}

	public function __construct() {
		parent::__construct();
		if(!$this[0] || !Hooks::dispatchToFirst("Fubber.Cmd.".$this[0], $this)) {
			
			if($res = Hooks::dispatchToFirst("Fubber.Cmd.*", $this)) {
				return;
			}
			
			$commandsLists = Hooks::dispatch("Fubber.Cmd.GetCommands");
			$commands = [];
			foreach($commandsLists as $commandList) {
				foreach($commandList as $command => $desc)
					$commands[$command] = $desc;
			}
			ksort($commands);
			$help = "fubber

Use the fubber command to perform various development related tasks. Available commands:
";
			foreach($commands as $command => $desc) {
				$help .= "\n    fubber ".str_pad($command, 15).$desc;
			}

			$this->setHelp($help);
			$this->option()
				->require()
				->describedAs("Provide a sub command.");
		}
	}

}
