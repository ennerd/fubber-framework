<?php
namespace Fubber\Commands;
use Doctrine\Common\Inflector\Inflector;

class CreateTableForm extends Command {

	public function __construct() {
		parent::__construct();
		$this->argument()
			->referToAs("class")
			->require()
			->describedAs("The Table class name");
			
		$this->option('namespace')
			->aka('t')
			->describedAs('Optional namespace to use for the generated class');
			
		$this->setHelp("Usage: fubber CreateTableForm <class>\nGenerates a proper Form based on a Table class.");
	}
	
	public function invoke() {
		$kernel = \Fubber\Kernel::init();
		
		$className = $this[1];
		if(!class_exists($className))
			die("Error: Class $className not found\n");
			
		if(!is_subclass_of($className, 'Fubber\Table'))
			die("Error: Class $className must extend Fubber\Table\n");
			
		$ref = new \ReflectionClass($className);
		$cols = $className::$_cols;
		$pk = $className::$_primaryKey;
		if(!$pk) $pk = [current($cols)];
		$consInitializers = [];
		$acceptInitializers = [];
		foreach($cols as $col) {
			if(!in_array($col, $pk)) {
				$consInitializers[] = "			".var_export($col, true)." => \$object->$col,";
				$acceptInitializers[] = "			\$object->$col = \$this->$col;";
			}
		}
		
		$res = '<?php
';
		if($this['namespace']) {
			$res .= "namespace ".$this['namespace'].";\n";
		}
		
		$res .= "use Fubber\Util\Errors;\n\n";

		$res .= "class $className"."Form extends \Fubber\Forms\Form {\n";
		$res .= "	protected \$_object;\n\n";
		$res .= "	public function __construct($className \$object) {
		/**
		* First we'll add the properties that this form will manage, by calling
		* the parent constructor.
		*/
		parent::__construct(".var_export($className."Form", true).", [
".implode("\n", $consInitializers)."
		]);
		\$this->_object = \$object;
		
		if(\$this->accept()) {
			/**
			* The form has data, and we'll populate our object with this new data.
			*/
".implode("\n", $acceptInitializers)."

			/**
			* The isInvalid method will call the objects' isInvalid method
			*/
			\$this->errors = \$this->isInvalid();
			if(!\$this->errors) {
				/**
				* Everything appears to be fine and dandy, so we'll save the object
				*/
				\$this->success = \$object->save();
			}
		}
	}
	
	public function isInvalid() {
		if(!\$this->hasData) return false;
		return \$this->_object->isInvalid();
	}
}";
		echo $res;
		

	}

}
