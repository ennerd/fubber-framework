<?php
namespace Fubber\PHP;

use JsonSerializable;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionClassConstant;
use ReflectionEnum;
use ReflectionException;
use ReflectionExtension;
use ReflectionFiber;
use ReflectionFunctionAbstract;
use ReflectionGenerator;
use ReflectionParameter;
use ReflectionProperty;
use ReflectionReference;
use ReflectionType;
use ReflectionZendExtension;
use Reflector;
use Stringable;

abstract class AbstractTool implements JsonSerializable, Stringable {

    /**
     * Get the underlying reflection object relevant for the resource
     */
    abstract public function reflect(): object;

    /**
     * Get a descriptive name for the underlying resource, which will help developers
     * understand what the resource is.
     * 
     * @return string 
     */
    abstract public function getDescriptiveName(): string;

    public function __toString(): string {
        return $this->getDescriptiveName();
    }

    public function jsonSerialize(): mixed {
        return (string) $this;
    }

    public function getLocation(): ?array {
        $r = $this->reflect();
        if ($r instanceof ReflectionClassConstant) {
            $r = $r->getDeclaringClass();
        } elseif ($r instanceof ReflectionGenerator) {
            $r = $r->getFunction();
        } elseif ($r instanceof ReflectionType) {
            // Types are location independent
            return null;
        } elseif ($r instanceof ReflectionAttribute) {
            // Reflection attributes unfortunatley don't record their location
            return null;
        } elseif ($r instanceof ReflectionFiber) {
            return [ $r->getExecutingFile(), $r->getExecutingLine() ];
        }
        if (
            $r instanceof ReflectionFunctionAbstract ||
            $r instanceof ReflectionClass
        ) {
            if (!($filename = $r->getFileName())) {
                return null;
            }

            return [ $r->getFileName(), $r->getStartLine() ?: null ];
        }
        return null;
    }

}