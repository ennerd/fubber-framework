<?php
namespace Fubber\PHP;

use Closure;
use Fubber\InvalidArgumentException;
use Fubber\Kernel\Container\LogicException;
use Fubber\PHP\ReflectionTypeTool as PHPReflectionTypeTool;
use Fubber\Reflection\ReflectionTypeTool as ReflectionReflectionTypeTool;
use ReflectionFunction;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionType;
use ReflectionUnionType;
use stdClass;
use Stringable;
use Traversable;

class ReflectionTypeTool extends AbstractTool {

    /**
     * Create a ReflectionTypeTool instance from a closure return type
     * 
     * @param Closure $exampleClosure 
     * @return static 
     * @throws FubberInvalidArgumentException 
     */
    public static function fromClosureReturnType(Closure $exampleClosure): static {
        $rf = new ReflectionFunction($exampleClosure);
        if (!$rf->hasReturnType()) {
            throw new InvalidArgumentException("Closure must have a return type");
        }
        return new static($rf->getReturnType());
    }

    public static function checkType(ReflectionType $type, mixed $value): bool {
        if ($type instanceof ReflectionNamedType) {

            if ($value === null && $type->allowsNull()) {
                return true;
            }
            if ($type->isBuiltin()) {
                if (\get_debug_type($value) === $type->getName()) {
                    return true;
                }
                switch ($type->getName()) {
                    case 'array': $f = function(array $v) {}; break;
                    case 'callable': $f = function(callable $v) {}; break;
                    case 'bool': $f = function(bool $v) {}; break;
                    case 'float': $f = function(float $v) {}; break;
                    case 'int': $f = function(int $v) {}; break;
                    case 'string': $f = function(string $v) {}; break;
                    case 'object': $f = function(object $v) {}; break;
                    case 'iterable': $f = function(iterable $v) {}; break;
                    case 'mixed': return true;
                    default:
                        throw new LogicException("Unknown built-in type `" . $type->getName() ."`");
                }

                try {
                    $f($value);
                    return true;
                } catch (\Throwable $e) {
                    return false;
                }
            }
            if ($value instanceof ($type->getName())) {
                return true;
            }

            return false;
        } elseif ($type instanceof ReflectionUnionType) {
            foreach ($type->getTypes() as $unionType) {
                if (self::checkType($unionType, $value)) {
                    return true;
                }
            }
            return false;
        } elseif ($type instanceof ReflectionIntersectionType) {
            foreach ($type->getTypes() as $intersectionType) {
                if (!self::checkType($intersectionType, $value)) {
                    return false;
                }
            }
            return true;
        } else {
            throw new InvalidArgumentException("ReflectionType `".$type::class."` is not supported");
        }
    }

    public static function typeToString(ReflectionType $type): string {
        if ($type instanceof ReflectionNamedType) {
            return ($type->allowsNull() && $type->getName() !== 'null' ? '?' : '') . $type->getName();
        } elseif ($type instanceof ReflectionUnionType) {
            return implode("|", \array_map(self::typeToString(...), $type->getTypes()));
        } elseif ($type instanceof ReflectionIntersectionType) {
            return implode("^", \array_map(self::typeToString(...), $type->getTypes()));
        } else {
            return 'mixed';
        }
    }

    private readonly ReflectionType $reflection;

    public function __construct(ReflectionType $reflection) {
        $this->reflection = $reflection;
    }

    public function reflect(): ReflectionType {
        return $this->reflection;
    }

    public function getDescriptiveName(): string {
        return self::typeToString($this->reflection);        
    }

    public function isNumeric(): bool {
        if (!($this->reflection instanceof ReflectionNamedType)) {
            return false;
        }
        return $this->reflection->getName() === 'int' || $this->reflection->getName() === 'float';
    }

    /**
     * Check if `$type` is a superset of our type.
     * 
     * @param ReflectionTypeTool|ReflectionType $type 
     * @return null|string 
     */
    public function isSubsetOf(ReflectionTypeTool|ReflectionType $type): ?string {
        if ($type instanceof ReflectionType) {
            $type = new ReflectionTypeTool($type);
        }
        return $type->isSupersetOf($this->reflect());
    }

    /**
     * Check if `$type` is a subset of our type.
     * 
     * @param ReflectionType|ReflectionTypeTool $type 
     * @return ?string If disjunct, a short reason why the type is incompatible
     */
    public function isSupersetOf(ReflectionType|ReflectionTypeTool $type): ?string {

        // Conventient to allow passing a ReflectionTypeTool, but we work with ReflectionType
        if ($type instanceof ReflectionTypeTool) {
            $type = $type->reflect();
        }

        $thisType = $this->reflect();

        // Quick check if exactly the same type, assuming that's common
        if (
            $type instanceof ReflectionNamedType &&
            $thisType instanceof ReflectionNamedType &&
            (string) $type === (string) $this->reflect()
        ) {
            return null;
        }

        // Handle union and intersection types
        if ($type instanceof ReflectionUnionType) {
            // TypeA|TypeB: Each possible return type must be acceptable
            foreach ($type->getTypes() as $subType) {
                if ($reason = $this->isSupersetOf($subType)) {
                    // this type does not overlap me
                    return 'may return '.$subType;
                }
            }
            return null;
        } elseif ($type instanceof ReflectionIntersectionType) {
            // TypeA&TypeB: At least one of the possible return types must be acceptable
            foreach ($type->getTypes() as $subType) {
                if (null === $this->isSupersetOf($subType)) {
                    // one of their types overlap me
                    return null;
                }
            }
            return 'incompatible intersection type';
        }

        /** @var ReflectionNamedType $type */

        $thisType = $this->reflect();

        // Special case; could they return null?
        if (!$thisType->allowsNull() && $type->allowsNull()) {
            return 'allows null';
        }

        $checker = function(ReflectionNamedType $ourType, ReflectionNamedType $type): ?string {
            if ($ourType->isBuiltin()) {
                if ($type->isBuiltin()) {
                    // Handle special cases
                    switch ($ourType->getName()) {
                        case 'int':
                        case 'float':
                            if (\in_array($type->getName(), [ 'int', 'float' ])) {
                                return null;
                            }
                            return 'not int|float';
                    }
                    if ($type->getName() !== $ourType->getName()) {
                        return 'not ' . $ourType->getName();
                    }
                    return null;
                } else {
                    // they aren't returning a built-in, so check for special cases
                    $thatClass = $type->getName();

                    switch ($ourType->getName()) {
                        case 'string':
                            if ($thatClass instanceof Stringable) {
                                return null;
                            } else {
                                return $thatClass . ' is not Stringable';
                            }
                        case 'object':
                            return null;
                        case 'iterable':
                            if ($thatClass instanceof Traversable) {
                                return null;
                            } else {
                                return $thatClass . ' is not Traversable';
                            }
                        case 'mixed':
                            // anything goes
                            return null;
                    }

                    // Their class is not of our built in type
                    return $thatClass . ' is not ' . $ourType->getName();
                }
            } else {
                // We aren't a builtin type
                $ourClass = $ourType->getName();
                $thatClass = $type->getName();
                if (\is_a($thatClass, $ourClass, true)) {
                    return null;
                }
                return $thatClass . ' is not ' . $ourClass;
            }
        };

        if ($thisType instanceof ReflectionNamedType) {
            return $checker($thisType, $type);
        } elseif ($thisType instanceof ReflectionUnionType) {
            // their type must be one of our types
            foreach ($thisType->getTypes() as $subType) {
                if (null === $checker($subType, $type)) {
                    return null;
                }
            }
            return $type->getName() . ' is not compatible with ' . $this->getDescriptiveName();
        } elseif ($thisType instanceof ReflectionIntersectionType) {
            foreach ($thisType->getTypes() as $subType) {
                if (null !== ($reason = $checker())) {
                    return $reason;
                }
            }
            return null;
        }
    }

    public function getType(): ReflectionType {
        return $this->reflect();
    }

    public function isLegalValue(mixed $value): bool {
        return self::checkType($this->reflection, $value);
    }

}