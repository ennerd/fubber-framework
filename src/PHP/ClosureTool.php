<?php
namespace Fubber\PHP;

use Closure;
use Fubber\TypeError;
use InvalidArgumentException;
use JsonSerializable;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionIntersectionType;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionUnionType;
use Throwable;

class ClosureTool extends AbstractTool {


    protected Closure $closure;
    protected ?ReflectionFunction $reflection = null;

    public function __construct(Closure $closure) {
        $this->closure = $closure;
    }

    public function get(): Closure {
        return $this->closure;
    }

    public function reflect(): ReflectionFunctionAbstract {
        if ($this->reflection === null) {
            $this->reflection = new ReflectionFunction($this->closure);
        }
        return $this->reflection;
    }

    public function getReturnType(): ?ReflectionTypeTool {
        if (!$this->reflect()->hasReturnType()) {
            return null;
        }
        return new ReflectionTypeTool($this->reflect()->getReturnType());
    }

    public function getParameters(): array {
        $result = [];
        foreach ($this->reflect()->getParameters() as $parameter) {
            $result[] = new ReflectionParameterTool($parameter);
        }
        return $result;
    }

    /**
     * Returns a closure description, suitable for explaining arguments and return type requirements.
     * 
     * @return string 
     */
    public function getSignature(): string {
        $name = 'function('. $this->getParametersSignature() . ')';
        if ($this->reflect()->hasReturnType()) {
            return $name . ': ' . $this->getReturnTypeSignature();
        }
        return $name;
    }

    /**
     * Return a string for identifying a closure, for example in the form `ClassName::methodName()`.
     * 
     * @return string 
     */
    public function getDescriptiveName(): string {
        $rm = $this->reflect();
        $name = $rm->getName();
        if (\str_ends_with($name, '{closure}')) {
            $name = $rm->getClosureScopeClass()->getName() . '::{closure}';
            $name = 'function';
        } else {
            $scopeClass = $rm->getClosureScopeClass();
            if ($scopeClass) {
                $name = $scopeClass->getName().'::'.$name;
            }    
        }
        $name .= '(' . $this->getParametersSignature() . ')';

        if ($rm->hasReturnType()) {
            $name .= ': ' . $this->getReturnType();
        }

        return $name;
    }

    /**
     * Returns a string like `int $arg2=null, string $arg2="example"` corresponding to the
     * arguments that this closure expects.
     * 
     * @return string 
     */
    public function getParametersSignature(): string {
        return \implode(", ", $this->getParameters());
    }

    public function getReturnTypeSignature(): string {
        return $this->getReturnType() ?? '';
    }

    /**
     * Was the exception thrown by this closure?
     * 
     * @param Throwable $exception 
     * @return bool 
     */
    public function didThrow(Throwable $exception): bool {
        $r = $this->reflect();

        if ($r->getFileName() !== $exception->getFile()) {
            return false;
        }

        return $r->getStartLine() <= $exception->getLine() && $r->getEndLine() >= $exception->getLine();
    }

    /**
     * Check that a closure has the correct signature by comparing the signature to our correct
     * signature. Note that if your signature declares parameters that are not optional (without a default
     * value), then the closure is required to take those arguments.
     * 
     * Example:
     * 
     * ```
     * // fails unless `$someClosure` matches at least `function(int $a): string`
     * 
     * (new ClosureTool(function(int $a, int $b=0): string {}))->validateSignature($someClosure);
     * ```
     * 
     * @param Closure $correctSignature 
     * @param Closure $closure 
     * @param int|string|null $redirectException {@see ExceptionTool::redirect()}
     * @throws InvalidArgumentException
     */
    public function assertValidSignature(Closure $closure, int|string $redirectException=null): void {
        $thisRF = $this->reflect();
        $closureTool = new self($closure);
        $location = $closureTool->getLocation();
        try {

            // check return type first
            $closureRF = $closureTool->reflect();

            if ($thisRF->hasReturnType()) {
                // We have a return type

                if (!$closureRF->hasReturnType()) {
                    throw new InvalidArgumentException("Closure `$closureTool` does not declare a return type");
                }

                $closureReturnType = $closureRF->getReturnType();

                $thisReturnTool = new ReflectionTypeTool($thisRF->getReturnType());

                if ($reason = $thisReturnTool->isSupersetOf($closureReturnType)) {
                    throw new InvalidArgumentException("Closure `$closureTool` has an incompatible return type ($reason)");
                }

                // return type is OK
            }

            // check parameters;
            if ($closureRF->getNumberOfRequiredParameters() > $this->reflection->getNumberOfRequiredParameters()) {
                throw new InvalidArgumentException("Closure `$closureTool` expects more parameters than `$this`");
            }
            $thisParameters = $this->reflection->getParameters();
            $thisParameterIsVariadic = null;
            $closureParameters = $closureRF->getParameters();
            $closureParameterIsVariadic = null;
            for ($i = 0; isset($closureParameters[$i]) || isset($thisParameters[$i]); $i++) {
                if ($closureParameterIsVariadic !== null) {
                    $closureParam = $closureParameters[$closureParameterIsVariadic];
                } elseif (isset($closureParameters[$i])) {
                    $closureParam = $closureParameters[$i];
                    if ($closureParam->isVariadic()) {
                        $closureParameterIsVariadic = $i;
                    }
                } else {
                    $closureParam = null;
                }

                if ($thisParameterIsVariadic !== null) {
                    $thisParam = $thisParameters[$thisParameterIsVariadic];
                } elseif (isset($thisParameters[$i])) {
                    $thisParam = $thisParameters[$i];
                    if ($thisParam->isVariadic()) {
                        $thisParameterIsVariadic = $i;
                    }
                } else {
                    $thisParam = null;
                }

                if ($closureParam === null) {
                    if ($thisParam->isVariadic()) {
                        // variadic parameters are not required
                        return;
                    }
                    if (!$thisParam->isDefaultValueAvailable()) {
                        throw new InvalidArgumentException("Closure `$closureTool` must accept parameter `" . (new ReflectionParameterTool($thisParam)) ."` as in `" . $this->getSignature() . "`");
                    }
                }

                if ($thisParam === null) {
                    // we won't be passing this argument
                    if (!$closureParam->isDefaultValueAvailable()) {
                        // they require a parameter we won't provide
                        throw new InvalidArgumentException("Closure `$closureTool` expects parameter `" . (new ReflectionParameterTool($closureParam)) . "`");
                    }
                }

                // both have parameter
                if (!$closureParam->hasType()) {
                    // they didn't bother declaring type, but it is compatible
                    continue;
                } elseif (!$thisParam->hasType()) {
                    throw new InvalidArgumentException("Closure `$closureTool` parameter `" . (new ReflectionParameterTool($closureParam)) . "` is incompatible with `$thisParam`");
                }

                if ($thisParam->allowsNull() && !$closureParam->allowsNull()) {
                    throw new InvalidArgumentException("Closure `$closureTool` parameter `" . (new ReflectionParameterTool($closureParam)) . "` doesn't allow null");
                }

                $thisTool = new ReflectionTypeTool($thisParam->getType());

                if ($reason = $thisTool->isSubsetOf($closureParam->getType())) {
                    throw new InvalidArgumentException("Closure `$closureTool` parameter `" . (new ReflectionParameterTool($closureParam)) . "` may not accept parameters of type `" . new ReflectionParameterTool($thisParam) . "` ($reason)");
                }
            }
        } catch (InvalidArgumentException $e) {
            $rt = new ExceptionTool($e);
            $rt->addNote("Expected signature", "`" . $this->getSignature() . "`");
            $rt->redirect(__DIR__);
            if ($redirectException) {
                $rt->redirect($redirectException);
            }
            if ($location !== null) {
                $rt->addNote("Closure location", \implode(":", $location));
            }
            throw $e;
        }
    }

    public function jsonSerialize(): mixed {
        return (string) $this;
    }

    public function __toString() {
        return $this->getDescriptiveName();

        $r = $this->reflect();
        return (string) $r;

        $name = $r->getName();

        if ($closureClass = $r->getClosureScopeClass()) {
            $name = $closureClass . '::' . $name;
        } elseif ($closureThis = $r->getClosureThis()) {
            $name = $closureThis::class . '' . $name;
        }
    
        return 'Closure[' . $name . ']';
    }
}