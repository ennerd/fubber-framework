<?php
namespace Fubber\PHP;

use ReflectionParameter;

class ReflectionParameterTool extends AbstractTool {

    private ReflectionParameter $reflection;

    public function __construct(ReflectionParameter $parameter) {
        $this->reflection = $parameter;
    }

    public function reflect(): ReflectionParameter {
        return $this->reflection;
    }

    public function getType(): ?ReflectionTypeTool {
        if (!$this->reflection->hasType()) {
            return null;
        }
        return new ReflectionTypeTool($this->reflect()->getType());
    }

    public function getDescriptiveName(): string {

        $argName = ($this->reflection->isVariadic() ? '...$' : '$') . $this->reflection->getName();

        if (null !== ($t = $this->getType())) {
            return $t . ' ' .$argName;
        }

        return $argName;
    }
}