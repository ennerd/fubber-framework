# Fubber\Reflection

Utility classes for providing better error messages and type checking.

## Examples

Validate a closure:

```php
function myFunction(Closure $callback): void {
    $validator = new ClosureTool(function(int $factor1, int $factor2): string {});
    $validator->validateSignature($callback);
}

myFunction( function() {} ); // throws InvalidArgumentException('')

Describe a function signature:

```php
echo (new ClosureTool(function(int $argument): void {}))->getSignature();
```