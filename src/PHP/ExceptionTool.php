<?php
namespace Fubber\PHP;

use ReflectionClass;
use ReflectionObject;
use ReflectionProperty;
use Throwable;

class ExceptionTool extends AbstractTool {

    protected Throwable $exception;
    protected ReflectionObject $reflection;

    public function __construct(Throwable $exception) {
        $this->exception = $exception;
        $this->reflection = new ReflectionObject($exception);
    }

    public function addNote(string $label, string $message): Throwable {
        // formatted as markdown for readability everywhere and for renderability in HTML
        $this->setMessage($this->exception->getMessage() . "\n * " . $label . ": " . $message);
        return $this->exception;
    }

    public function setMessage(string $message): Throwable {
        $rp = $this->reflection->getProperty('message');
        $rp->setValue($this->exception, $message);
        return $this->exception;
    }

    public function setCode(int $code): Throwable {
        $rp = $this->reflection->getProperty('code');
        $rp->setValue($this->exception, $code);
        return $this->exception;
    }

    public function setLine(int $line): Throwable {
        $rp = $this->reflection->getProperty('line');
        $rp->setValue($this->exception, $line);
        return $this->exception;
    }

    public function setFile(string $file): Throwable {
        $rp = $this->reflection->getProperty('file');
        $rp->setValue($this->exception, $file);
        return $this->exception;
    }

    /**
     * Changes the exception source so that it appears to have been thrown
     * further up the stack.
     * 
     * If given a string, removes all stack traces originating within the
     * path.
     * 
     * ```php
     * throw (new ExceptionTool(new \Exception()))->redirect(__DIR__);
     * ```
     * 
     * If given a number $n, removes $n steps of the backtrace.
     * 
     * 
     * @param string|int $removeFromTrace 
     * @return Throwable Returns the exception for convenience. 
     */
    public function redirect(string|int $removeFromTrace=1): Throwable {
        if (\is_int($removeFromTrace)) {
            for ($i = 0; $i < $removeFromTrace; $i++) {
                static::trimExceptionTrace($this->exception);
            }
        } else {
            while (\str_contains($this->exception->getFile(), $removeFromTrace)) {
                static::trimExceptionTrace($this->exception);
            }
        }
        return $this->exception;
    }

    /**
     * Rewrite the exception so that the location that caused the exception is the previous
     * stack trace element.
     * 
     * @param Throwable $e 
     * @return Throwable 
     */
    public static function trimExceptionTrace(Throwable $e): void {
        $rc = new ReflectionObject($e);

        $findProperty = function(ReflectionClass $rc, string $name) use (&$findProperty): ?ReflectionProperty {
            if ($rc->hasProperty($name)) {
                return $rc->getProperty($name);
            }
            if ($parent = $rc->getParentClass()) {
                return $findProperty($parent, $name);
            }
            return null;
        };

        /** @var ?ReflectionProperty */
        $rTrace = $findProperty($rc, 'trace');
        /** @var ?ReflectionProperty */
        $rFile = $findProperty($rc, 'file');
        /** @var ?ReflectionProperty */
        $rLine = $findProperty($rc, 'line');

        $trace = $rTrace->getValue($e);

        $top = \array_shift($trace);
        $rTrace->setValue($e, $trace);
        $rFile->setValue($e, $top['file']);
        $rLine->setValue($e, $top['line']);
    }

    public function reflect(): ReflectionObject {
        return $this->reflection;
    }

    public function getDescriptiveName(): string {
        return $this->reflection->getName();
    }
}