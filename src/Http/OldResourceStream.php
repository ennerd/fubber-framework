<?php
declare(strict_types=1);

namespace Fubber\Http;

use Psr\Http\Message\StreamInterface;

class ResourceStream extends Stream {

    protected $stream;

    public function __construct($resource) {
        $this->stream = $resource;
    }

    /**
     * @see StreamInterface::close();
     */
    public function close() {
        fclose($this->stream);
    }

    /**
     * @see StreamInterface::detach();
     */
    public function detach() {
        $result = $this->stream;
        $this->stream = null;
        return $result;
    }

    /**
     * @see StreamInterface::getSize();
     */
    public function getSize(): ?int {
die("getSize");
        return $this->size;
    }

    /**
     * @see StreamInterface::tell()
     */
    public function tell(): int {
        if (!$this->stream) {
            throw new \RuntimeException("Stream detached");
        }
        return ftell($this->stream);
    }

    /**
     * @see StreamInterface::close();
     */
    public function seek($offset, $whence = SEEK_SET) {
die("seek");
    }

    /**
     * @see StreamInterface::write();
     */
    public function write($string): int {
        if (!$this->isReadable()) {
            throw new \RuntimeException("Stream is not writable");
        }
        return fwrite($this->stream, $string);
    }

    /**
     * @see StreamInterface::close();
     */
    public function read($length): string {
        return fread($this->stream, $length);
        throw new \RuntimeException("Stream is not readable");
    }

    /**
     * @see StreamInterface::close();
     */
    public function getContents(): string {
        if (!$this->isReadable()) {
            throw new \RuntimeException("Stream is not readable");
        }
        return stream_get_contents($this->stream);
    }

    /**
     * @see StreamInterface::close();
     */
    public function getMetadata($key = null) {
        $metadata = \stream_get_meta_data($this->stream);
        if ($key !== null) {
            return array_key_exists($key, $metadata) ? $metadata[$key] : null;
        }
        return $metadata;
    }
}
