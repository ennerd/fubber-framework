<?php
namespace Fubber\Http;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Represents an entire HTTP request, including cookies and whatnot.
 */
class ServerRequest implements ServerRequestInterface {
    use MessageTrait;

    protected
        $_uri,
        // headers comes from MessageTrait
        $_body,
        $_serverParams,
        $_method, 
        $_path,
        $_scheme,
        $_httpVersion,
        $_parsedBody,
        $_files = [],
        $_attributes = [];

    /**
     * Old param list
     * @param string $method        GET/POST/PUT/HEAD/DELETE
     * @param string $path           /some/url
     * @param array $get            [field=value, field2=value]
     * @param array $post           [field=value, field2=value]
     * @param array $files          Same as $_FILES
     * @param array $headers        ['Accept-Language' => 'no-nb']
     * @param resource $body        file descriptor that represents the request body
     * @param string $httpVersion   1.1
     * @param bool $https           false
     */
//    public function __construct($method, $path, $get, $post, $files, $headers, $body, $httpVersion = '1.1', $https = false) {

    /**
     * @param $uri
     * @param $headers
     * @param $body
     * @param $method        GET|POST|PUT|HEAD|DELETE|PATCH|OPTIONS
     * @param $scheme
     * @param $httpVersion
     */
    public function __construct(
        UriInterface $uri,
        array $headers,
        StreamInterface $body,
        array $serverParams = [],
        string $method = 'GET',
        $httpVersion = '1.1',
        $injects = []
    ) {
        $this->_uri = $uri;
        $this->setHeaders($headers);
        $this->setBody($body);
        $this->_serverParams = $serverParams;
        $this->_method = $method;
        $this->_body = $body;
        $this->_httpVersion = $httpVersion;
        if (isset($injects['parsedBody'])) {
            $this->_parsedBody = $injects['parsedBody'];
        }
        if (isset($injects['files'])) {
            $this->_files = $injects['files'];
        }
        if (isset($injects['attributes'])) {
            $this->_attributes = $injects['attributes'];
        }
    }

    /**
     * @see ServerRequestInterface::getServerParams()
     */
    public function getServerParams(): array {
        return $this->_serverParams;
    }

    public function getCookieParams(): array {
        $cookies = [];
        $cookieString = $this->getHeader('Cookie');
        if(isset($cookieString[0])) {
            $cookieString = $cookieString[0];
            $cookies = explode(";", $cookieString);
            foreach($cookies as $cookie) {
                $cookie = trim($cookie);
                list($name, $value) = explode("=", $cookie);
                $cookies[$name] = $value;
            }
        }
        return $cookies;
    }

    public function withCookieParams(array $cookies) {

        $cookieStrings = [];
        foreach ($this->getCookieParams() as $name => $value) {
            $cookieStrings[] = "$name=$value";
        }
        return $this->withHeader('Cookie', $cookieStrings);

    }

    public function getQueryParams(): array {
        if (function_exists('mb_parse_str')) {
            mb_parse_str($this->_uri->getQuery(), $get);
        } else {
            parse_str($this->_uri->getQuery(), $get);
        }
        return $get;
    }

    public function withQueryParams(array $query): self {
        $c = clone $this;
        $c->_uri = $c->_uri->withQuery(http_build_query($query));
        return $c;
    }

    public function getUploadedFiles(): array {
        return $this->_files;
    }

    public function withUploadedFiles(array $files) {
        $c = clone $this;
        $c->_files = $files;
        return $c;
    }

    public function getParsedBody() {
        if ($this->_parsedBody) {
            return $this->_parsedBody;
        }
        $body = $this->getBody()->getContents();
        if ($body === '') {
            return null;
        }
        $contentType = $this->getHeader('Content-Type');
        if (!isset($contentType[0])) {
            return $this->_parsedBody = [];
        }
        $contentType = $contentType[0];
        $parts = explode(";", $contentType);
        switch (strtolower($parts[0])) {
            case 'application/x-www-form-urlencoded' :
                if (function_exists('mb_parse_str')) {
                    mb_parse_str($body, $result);
                } else {
                    parse_str($body, $result);
                }
                return $this->_parsedBody = $result;
            case 'multipart/form-data' :
                throw new \BadMethodCallException("Parsing of multipart/form-data not implemented yet");
            case 'application/json' :
                $result = json_decode($body, true);
                if (\json_last_error() !== \JSON_ERROR_NONE) {
                    return $this->_parsedBody = [];
                }
                return $this->_parsedBody = $result;
            default :
                throw new \BadMethodCallException("Unable to parse requests with Content-Type: ".json_encode($contentType));
        }
        return $this->_parsedBody = [];
    }

    public function withParsedBody($data) {
        $c = clone $this;
        $c->_parsedBody = $data;
        return $c;
    }

    public function getAttributes(): array {
        return $this->_attributes;
    }

    public function getAttribute($name, $default = null) {
        return $this->_attributes[$name] ?? $default;
    }

    public function withAttribute($name, $value) {
        $c = clone $this;
        $c->_attributes[$name] = $value;
        return $c;
    }

    public function withoutAttribute($name) {
        $c = clone $this;
        unset($c->_attributes[$name]);
        return $c;
    }

    /**
     * @see RequestInterface::getRequestTarget()
     */
    public function getRequestTarget() {
        $query = $this->_uri->getQuery();
        return $this->_uri->getPath() . ($query !== '' ? '?' . $query : '');
    }

    /**
     * @see RequestInterface::withRequestTarget()
     */
    public function withRequestTarget($requestTarget) {
        $c = clone $this;
        $parts = explode("?", $requestTarget);
        $c->_uri = $c->_uri->withPath($parts[0]);
        if (isset($parts[1])) {
            $c->_uri = $c->_uri->withQuery($parts[1]);
        }
        return $c;
    }

    /**
     * @see RequestInterface::getMethod()
     */
    public function getMethod() {
        return $this->_method;
    }

    /**
     * @see RequestInterface::withMethod()
     */
    public function withMethod($method) {
        $c = clone $this;
        $c->_method = $method;
        return $c;
    }

    /**
     * @see RequestInterface::getUri()
     */
    public function getUri(): UriInterface {
        return $this->_uri;
    }

    /**
     * @see RequestInterface::withUri()
     */
    public function withUri(UriInterface $uri, $preserveHost = false) {
        $c = clone $this;
        if ($preserveHost && $this->_uri->getHost()) {
            $c->_uri = $uri->withHost($this->_uri->getHost());
        } else {
            $c->_uri = $uri;
        }
        return $c;
    }

    public function getUrl(): UriInterface {
        return $this->_uri;
    }
/*
    public function getUrl() {
        $urlString = $this->_scheme.'://';
        $host = $this->getHeader('host');
        if(isset($host[0]))
            $urlString .= $host[0];
        $urlString .= $this->_path;
        if($this->get) {
            $urlString .= '?'.http_build_query($this->get);
        }
        return new \Fubber\Util\Url($urlString);
    }
*/
    public function getPath() {
        return $this->getUri()->getPath();
    }

    public function __clone() {
        $this->_uri = clone $this->_uri;
    }

    public function __get(string $name) {
        switch ($name) {
            case 'get' :
                return $this->getQueryParams();
            case 'post' :
                return $this->getParsedBody();
            case 'cookies' :
                return $this->getCookieParams();
            default :
                die ("Fubber\\Http\\ServerRequest::\$$name does not exist");
                throw new \Exception("Property ServerRequest::$name does not exist");
        }
    }
}
