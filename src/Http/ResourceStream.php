<?php
declare(strict_types=1);

namespace Fubber\Http;

use Throwable;
use RuntimeException;
use Psr\Http\Message\StreamInterface;
use function fread, fwrite, fclose, ftell, fstat, stream_get_meta_data, stream_get_contents,
    array_key_exists;

class ResourceStream extends Stream {

    protected $stream;
    protected $closed = false;

    public function __construct($resource) {
        $this->stream = $resource;
    }

    /**
     * @see parent::close();
     */
    public function read($length): string {
        $this->assertAttached();
        $this->assertNotClosed();
        $result = fread($this->stream, $length);
        if ($result === false) {
            throw new RuntimeException("An error occurred when calling fread()");
        }
        return $result;
    }

    /**
     * @see parent::write();
     */
    public function write($string): int {
        $this->assertReadable();
        $result = fwrite($this->stream, $string);
        if ($result === false) {
            throw new RuntimeException("An error occurred when calling fwrite()");
        }
        return $result;
    }

    /**
     * @see parent::close();
     */
    public function close() {
        try {
            // This trick avoids the PHP error handler having to start up
            $this->assertNotClosed();
            fclose($this->stream);
            $this->closed = true;
        } catch (Throwable $e) {}
    }

    /**
     * @see parent::detach();
     */
    public function detach() {
        $result = $this->stream;
        $this->stream = null;
        return $result;
    }

    /**
     * @see parent::getSize();
     */
    public function getSize(): ?int {
        try {
            $this->assertAttached();
            $this->assertNotClosed();
            $stat = fstat($this->stream);
            if (!$stat) {
                return null;
            }
            return $stat['size'];
        } catch (Throwable $e) {
            return null;
        }
    }

    /**
     * @see parent::tell()
     */
    public function tell(): int {
        $this->assertAttached();
        $this->assertNotClosed();
        $result = ftell($this->stream);
        if ($result === false) {
            throw new RuntimeException("An error occurred when calling ftell()");
        }
        return $result;
    }

    /**
     * @see parent::seek();
     */
    public function seek($offset, $whence = SEEK_SET) {
        $this->assertAttached();
        $this->assertNotClosed();
        if (-1 === fseek($this->stream, $offset, $whence)) {
            throw new RuntimeException("An error occurred when calling fseek()");
        }
    }

    /**
     * @see parent::seek();
     */
    public function getContents(): string {
        if (!$this->isReadable()) {
            throw new RuntimeException("Stream is not readable");
        }
        return stream_get_contents($this->stream);
    }

    /**
     * @see parent::close();
     */
    public function getMetadata($key = null) {
        $metadata = stream_get_meta_data($this->stream);
        if ($key !== null) {
            return array_key_exists($key, $metadata) ? $metadata[$key] : null;
        }
        return $metadata;
    }

    protected function assertAttached() {
        if (!$this->stream) {
            throw new RuntimeException("Underlying stream resource is detached");
        }
    }

    protected function assertReadable() {
        if (!$this->isReadable()) {
            throw new RuntimeException("Stream is not readable");
        }
    }

    protected function assertWritable() {
        if (!$this->isWritable()) {
            throw new RuntimeException("Stream is not writable");
        }
    }

    protected function assertNotClosed() {
        if ($this->closed) {
            throw new RuntimeException("Stream is closed");
        }
    }
}
