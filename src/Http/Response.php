<?php
namespace Fubber\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Fubber Framework Response class. Encapsulates a Http response that will be
 * sent to the browser.
 */
abstract class Response {
    use MessageTrait;

    const CACHE_PUBLIC = 'public';
    const CACHE_PRIVATE = 'private';
    const CACHE_NO = 'no-cache';

    protected $_writable = TRUE;
    protected $_statusCode = 200;
    protected $_reasonPhrase = 'OK';
    protected $_cookies = [];
    protected $_cacheLevel = self::CACHE_PUBLIC;
    protected $_cacheTtl = 31536000; // One year
    protected $_cacheSetExplicitly = false; // true if a user has explicitly called caching functions.
    protected $_cacheLocked = false; // True if no further changes to cache should be allowed

    public function getCacheLevel(): string {
        return $this->_cacheLevel;
    }

    public function getCacheTTL(): int {
        return $this->_cacheTtl;
    }

    /**
     * This response may not be cached
     */
    public function noCache() {
        if($this->_cacheLocked) {
            throw new \Fubber\CodingErrorException("Cache has been locked. Don't use uncacheable data.");
            return $this;
        }
        $this->_cacheSetExplicitly = true;
        $this->_cacheLevel = self::CACHE_NO;
        $this->_cacheTtl = 0;
        $this->updateCacheHeader();
        return $this;
    }

    /**
     * This response may be privately cached (i.e. in the users browser)
     */
    public function publicCache($ttl = 31536000) {
        if($this->_cacheLocked) {
            throw new \Fubber\CodingErrorException("Cache has been locked. Don't use uncacheable data.");
            return $this;
        }
        $this->_cacheSetExplicitly = true;
        if($this->_cacheTtl > $ttl)
            $this->_cacheTtl = $ttl;
        $this->updateCacheHeader();
        return $this;
    }

    /**
     * This response may not be publicly cached (browser cache, reverse proxy etc)
     */
    public function privateCache($ttl = 86400) {
        if($this->_cacheLocked) {
            throw new \Fubber\CodingErrorException("Cache has been locked. Don't use uncacheable data.");
            return $this;
        }
        $this->_cacheSetExplicitly = true;
        if($this->_cacheTtl > $ttl)
            $this->_cacheTtl = $ttl;
        if($this->_cacheLevel == self::CACHE_PUBLIC)
            $this->_cacheLevel = self::CACHE_PRIVATE;
        $this->updateCacheHeader();
        return $this;
    }

    public function lockCache() {
        $this->_cacheLocked = true;
        $this->removeHeader('Set-Cookie');
        return $this;
    }

    public function explicitCaching() {
        return $this->_cacheSetExplicitly;
    }

    protected function updateCacheHeader() {
        switch($this->_cacheLevel) {
            case self::CACHE_NO :
                $this->setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
                $this->setHeader('Pragma', 'no-cache');
                $this->setHeader('Expires', '0');
                break;
            case self::CACHE_PRIVATE :
                $this->setHeader('Cache-Control', 'private, max-age='.$this->_cacheTtl);
                break;
            case self::CACHE_PUBLIC :
                $this->setHeader('Cache-Control', 'public, max-age='.$this->_cacheTtl);
                break;
        }
    }

    public function setCookie($name, $value="", $expire=0, $path="", $domain="", $secure=false, $httpOnly=false) {
        if($this->_cacheLevel == static::CACHE_PUBLIC && $this->_cacheLocked) {
            throw new \Fubber\CodingErrorException("Refusing to set cookie on forced publicly cacheable response.");
        }
        $this->privateCache();
        $this->_cookies[$name] = [$value, $expire, $path, $domain, $secure, $httpOnly];
        $this->setHeader("Set-Cookie", $this->_buildCookieHeaders());
    }

    protected function _buildCookieHeaders() {
        $headers = [];
        foreach($this->_cookies as $name => $spec) {
            $str = $name."=".$spec[0];
            if($spec[1])
                $str .= "; expires=".gmdate('D, d M Y H:i:s \G\M\T', $spec[1]);
            if($spec[3])
                $str .= "; domain=".$spec[3];
            if($spec[2])
                $str .= "; path=".$spec[2];
            if($spec[4]) {
                $str .= "; secure; SameSite=None";
            }
            if($spec[5])
                $str .= "; httponly";

            $headers[] = $str;
        }
        return $headers;
    }

    public function isWritable() {
        return $this->_writable;
    }

    public function writeHead(int $status = 200, array $headers = [], $reasonPhrase = '') {
        if(!$this->_writable) {
            throw new Exception("Not writable");
        }
        $this->setStatus($status, $reasonPhrase);

        foreach($headers as $k => $v) {
            $this->setHeader($k, $v);
        }

        return $this;
    }

    /**
     * Finish building this response. Make ready for sending to browser.
     */
    public function end($data = null) {
        \Fubber\Hooks::dispatch('fubber.response.end', $this, $data);
        $this->setBody($data);
        $this->_writable = FALSE;
        return $this;
    }

    public function setStatus(int $code, $reasonPhrase = '') {
        if (trim($reasonPhrase, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,. !?_\\') !== '') {
            throw new \Fubber\CodingErrorException("Invalid characters in reason phrase. Only alphanumeric, space and punctuation allowed.");
        }
        $this->_statusCode = $code;
        $this->_reasonPhrase = $reasonPhrase;
        return $this;
    }

    public function getStatusCode() {
        return $this->_statusCode;
    }

    public function getReasonPhrase() {
        return $this->_reasonPhrase;
    }

    public function withStatus($code, $reasonPhrase = '') {
        $c = clone $this;
        $c->_statusCode = $code;
        $c->_reasonPhrase = $reasonPhrase;
        return $c;
    }

    public function __toString() {
        return (string) $this->getBody();
        $body = $this->getBody();

        if(is_scalar($body)) {
            return $body;
        }
        // NOTE: Could support more types, for example files etc.
        return $body->__toString();
    }
}
