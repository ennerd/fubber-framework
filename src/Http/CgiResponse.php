<?php
namespace Fubber\Http;

/**
 * This Response class is used when running inside a web server, either mod_php,
 * or as CGI.
 */
class CgiResponse extends Response {
    public function end($contents = NULL) {

        header($str = "HTTP/".trim($this->getProtocolVersion())." ".trim($this->getStatusCode())." ".str_replace("\n", "-", trim($this->getReasonPhrase())));

        // Handling this here allows Content-Length header to be generated without being removed by apache.
        /*
        if(!empty($_SERVER["HTTP_ACCEPT_ENCODING"]) && strpos($_SERVER["HTTP_ACCEPT_ENCODING"], 'gzip') !== false) {
            $this->addHeader('Content-Encoding', 'gzip');
            $contents = gzencode($contents, 9, FORCE_GZIP);
        }
        */

        // Add content length header
        $cl = $this->getHeader('Content-Length');
        if(empty($cl)) {
            $this->addHeader('Content-Length', strlen($contents));
        }

        $headers = $this->getHeaders();

        foreach($headers as $k => $vals) {
            $first = true;
            foreach($vals as $val) {
                if($first)
                    header($k.": ".$val, true);
                else
                    header($k.": ".$val, false);
                $first = false;
            }
        }
        echo $contents;
        ignore_user_abort(true);
        if(function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        } else {
            while (@ob_end_flush());
            flush();
        }

        $this->_writable = false;
        return $this;
    }
}
