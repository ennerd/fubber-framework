<?php
namespace Fubber\Http;

use Psr\Http\Message\StreamInterface;

/**
 * Functionality that both Requests and Responses share (i.e. they have
 * headers, protocol version and a request body)
 */
trait MessageTrait {
    protected
        $_headers = [],
        $_headersCase = [],
        $_protocolVersion = '1.1',
        $_body;
    
    /**
     * @see MessageInterface::getProtocolVersion()
     */
    public function getProtocolVersion() {
        return $this->_protocolVersion;
    }

    /**
     * @see MessageInterface::withProtocolVersion()
     */
    public function withProtocolVersion($version) {
        $c = clone $this;
        $c->_protocolVersion = $version;
        return $c;
    }

    /**
     * @see MessageInterface::getHeaders()
     */
    public function getHeaders() {
        $result = [];
        foreach($this->_headers as $name => $values) {
            $result[$this->_headersCase[$name]] = $values;
        }
        return $result;
    }

    /**
     * @see MessageInterface::hasHeader()
     */
    public function hasHeader($name) {
        return isset($this->_headers[strtolower($name)]);
    }

    /**
     * @see MessageInterface::getHeader()
     */
    public function getHeader($name) {
        return $this->_headers[strtolower($name)] ?? [];
    }

    /**
     * @see MessageInterface::getHeaders()
     */
    public function getHeaderLine($name) {
        $name = strtolower($name);
        return isset($this->_headers[$name]) ? implode(", ", $this->_headers[$name]) : "";
    }

    /**
     * @see MessageInterface::withHeader()
     */
    public function withHeader($name, $value) {
        $c = clone($this);
        $lName = strtolower($name);
        if (!is_array($value)) {
            $value = [ $value ];
        }
        $this->_headers[$lName] = $value;
        $this->_headersCase[$lName] = $name;
    }

    /**
     * @see MessageInterface::withAddedHeader()
     */
    public function withAddedHeader($name, $value) {
        $lName = strtolower($name);

        $currentValues = $this->_headers[$lName] ?? [];

        if (is_array($value)) {
            foreach ($value as $v) {
                $currentValues[] = $v;
            }
        } else {
            $currentValues[] = $value;
        }

        return $this->withHeader($name, $currentValues);
    }

    /**
     * @see MessageInterface::withoutHeader()
     */
    public function withoutHeader($name) {
        $c = clone $this;
        $lName = strtolower($name);
        unset($c->_headers[$lName]);
        unset($c->_headersCase[$lName]);
        return $c;
    }

    public function setProtocolVersion($version) {
        $this->_protocolVersion = $version;
        return $this;
    }

    public function setHeaders($headers) {
        foreach($headers as $k => $v) {
            $this->setHeader($k, $v);
        }
    }
    
    public function setHeader($name, $value) {
        if(!is_array($value))
            $value = [$value];
        $lc = strtolower($name);
        $this->_headers[$lc] = $value;
        $this->_headersCase[$lc] = $name;
        return $this;
    }

    public function addHeader($name, $value) {
        if(!is_array($value))
            $value = [$value];
        $lc = strtolower($name);
        if(!isset($this->_headers[$lc]))
            $this->_headers[$lc] = [];
        foreach($value as $k => $v) {
            $this->_headers[$lc][] = $v;
        }
        $this->_headersCase[$lc] = $name;
        return $this;
    }

    public function removeHeader($name) {
        $lc = strtolower($name);
        unset($this->_headers[$lc]);
        unset($this->_headersCase[$lc]);
        return $this;
    }

    public function withBody(\Psr\Http\Message\StreamInterface $body) {
        $c = clone $this;
        $c->_body = $body;
        return $c;
    }

    public function getBody() {
        return $this->_body;
    }

    public function setBody($body) {
        $this->_body = $body;
    }
}
