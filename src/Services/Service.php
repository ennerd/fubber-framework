<?php
declare(strict_types=1);

namespace Fubber\Services;

use Fubber\ConfigErrorException;
use Fubber\Hooks;
use Fubber\Exception;
use Fubber\Kernel;
use Fubber\NotSupportedException;

/**
 * The default service factory for Fubber Framework. Could be powered by
 * a PSR-11 Container.
 */
class Service implements IService {

    /*
    public static function create(): IService {
        static $instance;
        if (!$instance) {
            $instance = new static();
        }
        return $instance;
    }
    */

    /**
     * @see IService::register()
     */
    /*
    public static function register(string $serviceId, callable $factory) {
        // Filter to wrap or modify factory methods for services
        extract(Hooks::filter(__METHOD__, ['serviceId' => $serviceId, 'factory' => $factory]), \EXTR_IF_EXISTS);
        static::create()->registerService($serviceId, $factory);
    }
    */
    /**
     * @deprecated Use Kernel::init()->get() instead
     * @see IService::use()
     */
    public static function use(string $serviceId) {
        // Filter to modify the service instance that is returned from filters
        return Hooks::filter(__METHOD__.'()', Kernel::init()->get($serviceId));
    }

    protected function registerService(string $serviceId, callable $factory) {
        if (isset($this->serviceMap[$serviceId])) {
            throw new ConfigErrorException("A service with the id '$serviceId' has already been registered.");
        }
        $this->serviceMap[$serviceId] = $factory;
    }

    public function get($id) {
        return Kernel::init()->get($id);

        if (!isset($this->serviceMap[$id])) {
            throw new class("Service '$id' not found") extends NotSupportedException implements \Psr\Container\NotFoundExceptionInterface {};
        }
        try {
            $service = $this->serviceMap[$id];
            return $service();
        } catch (\Throwable $e) {
            throw new class("Error occurred while retrieving the '$id' service", 131, $e) extends Exception implements \Psr\Container\ContainerExceptionInterface {};
        }
    }

    public function has(string $id): bool {
        return Kernel::init()->has($id);
        
        return array_key_exists($id, $this->serviceMap);
    }
}
