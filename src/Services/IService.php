<?php
declare(strict_types=1);

namespace Fubber\Services;

use Psr\Container\ContainerInterface;

/**
 * The default service container for Fubber Framework. Any PSR-11 compliant
 * container can be used, as long as this API is implemented.
 */
interface IService extends ContainerInterface {

    /**
     * Retrieve an instance of the IService provider
     */
    //public static function create(): IService;

    /**
     * Convenience function for receiving a service instance
     *
     * Retrieve a service according to a Service Id. The Service Id is normally
     * the interface name or the class name in cases like PDO.
     *
     * For example:
     * <code>
     * $db = \Fubber\Service::get(\PDO::class);
     * </code>
     *
     * @param $serviceId The service id
     * @return mixed
     */
    public static function use(string $serviceId);

    /**
     * Register a service factory. Note; if the callable accept arguments,
     * these arguments must be provided when calling IService::get().
     *
     * We don't do auto injection or caching of instances - so you should
     * implement that in your factory function if this is required.
     *
     * @param $serviceId The service id
     * @param $factory The service factory.
     */
    //public static function register(string $serviceId, callable $factory);

}
