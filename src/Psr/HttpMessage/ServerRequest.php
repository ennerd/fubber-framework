<?php
namespace Fubber\Psr\HttpMessage;

use Charm\Http\Message\ServerRequest as CharmServerRequest;
use Fubber\CodingErrorException;

class ServerRequest extends CharmServerRequest {
    public function __get(string $name) {
        throw new CodingErrorException("Unknown property ServerRequestInterface::`$name`, update your code.");
    }
    public function __set(string $name, $value) {
        throw new CodingErrorException("Unknown property ServerRequestInterface::`$name`, update your code.");
    }
}