<?php
namespace Fubber\Psr\HttpMessage;

use Charm\Http\Message\Response as CharmResponse;
use Fubber\CodingErrorException;

class Response extends CharmResponse {
    public function __get(string $name) {
        throw new CodingErrorException("Unknown property ResponseInterface::`$name`, update your code.");
    }
    public function __set(string $name, $value) {
        throw new CodingErrorException("Unknown property ResponseInterface::`$name`, update your code.");
    }
}