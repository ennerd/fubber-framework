<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * Exception to be used whenever user provided data does not validate. 
 * Accepts the validation errors as an associative array.
*/
class ValidationException extends RuntimeException {

	public readonly array $errors;

    public function getDefaultStatus(): array {
        return [400, "Invalid Data Received"];
    }

	/**
	*	@param array $errors
	*	@param int $code
	*/
	public function __construct(array $errors, $code=0) {
		$this->errors = $errors;
		$errStr = '';
		foreach($errors as $k => $v) {
			$errStr .= $k.": ".$v."<br>\n";
        }
		$errStr = trim($errStr);
		parent::__construct("Validation Errors", $code);
	}

    public function getExceptionDescription(): string {
        return "Validation Errors";
    }

    public function jsonSerialize() {
        $result = parent::jsonSerialize();
        $result['validationErrors'] = $this->errors;
        return $result;
    }
}
