<?php
namespace Fubber\Meta;

/**
 * Defines various contexts where a semantic type can describe itself.
 */
enum UsageContext {
    /**
     * When the row, value or type is being used as a table header or in
     * tabular data such as a JSON structure.
     */
    case TableHeader;

    /**
     * When the row, value or type is being used in a form, together with
     * other values.
     */
    case FormLabel;

    /**
     * When the row, value or type is being referenced externally. This is
     * mostly applicable to rows being referenced as a foreign key.
     */
    case Reference;
}