<?php
namespace Fubber\Meta;

use Fubber\I18n\Translatable;

/**
 * Describe a data collection
 * 
 * @package Fubber\Meta
 */
interface CollectionMetadataInterface {
   
    /**
     * The name of the collection in singular and plural form
     * 
     * @return Translatable|string 
     */
    public function name(): Translatable|string;

    /**
     * Describes each of the columns
     * 
     * @return array<string, ColumnMetadataInterface>
     */
    public function columns(): array;
}