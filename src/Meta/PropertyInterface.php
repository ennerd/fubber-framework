<?php
namespace Fubber\Meta;

interface PropertyInterface {

    public function getName(): string;
    public function getType(): RootTypeInterface;
    public function getValidator(): ValidatorInterface;

}