<?php
namespace Fubber\Meta;

interface MetaRowInterface {
    public function metaTable(): MetaTableInterface;
}