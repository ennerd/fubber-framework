<?php
namespace Fubber\Meta;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\BackedType;
use Fubber\Meta\Abstract\BooleanBackedType;
use Fubber\Meta\Abstract\EnumIntegerBackedType;
use Fubber\Meta\Abstract\EnumStringBackedType;
use Fubber\Meta\Abstract\FloatBackedType;
use Fubber\Meta\Abstract\IntegerBackedType;
use Fubber\Meta\Abstract\NumberBackedType;
use Fubber\Meta\Abstract\ObjectBackedType;
use Fubber\Meta\Abstract\StringableBackedType;
use Fubber\Meta\Abstract\StringBackedType;

interface TypeInterface extends RootTypeInterface {

    /**
     * If this semantic type extends another semantic type, get that
     * parent type. Generally, all types must should extend directly or
     * indirectly one of the primitive types (Number, String, Enumerable).
     */
    public function getParentType(): RootTypeInterface;

}