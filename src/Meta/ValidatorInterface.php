<?php
namespace Fubber\Meta;

use Fubber\I18n\Translatable;
use JsonSerializable;

interface ValidatorInterface extends JsonSerializable {
    public function isInvalid(mixed $value): ?Translatable;
}