<?php
namespace Fubber\Meta\Validators;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaValidator;
use Fubber\Meta\ValidatorInterface;

use function Fubber\trans;

class Either extends MetaValidator {

    /**
     * @var ValidatorInterface[]
     */
    private array $validators;

    public function __construct(ValidatorInterface ...$validators) {
        $this->validators = $validators;
    }

    public function isInvalid(mixed $value): ?Translatable {
        $reasons = [];
        foreach ($this->validators as $validator) {
            $reason = $validator->isInvalid($value);
            if ($reason === null) {
                return null;
            }
            $reasons[] = $reason;
        }
        $comma = trans(", ");
        $finally = trans(" and ");
        
    }

}