<?php
namespace Fubber\Meta\Validators;

use Countable;
use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaValidator;

use function Fubber\trans;

class MinLength extends MetaValidator {

    public function __construct(
        public readonly int $minLength
    ) {}

    public function isInvalid(mixed $value): ?Translatable {
        $length = \mb_strlen($value);
        if ($length >= $this->minLength) {
            return null;
        }
        return trans("Minimum length is {minLength}", $this);
    }
}