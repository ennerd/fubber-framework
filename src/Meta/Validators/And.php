<?php
namespace Fubber\Meta\Validators;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaValidator;
use Fubber\Meta\ValidatorInterface;

class Every extends MetaValidator {

    /**
     * @var ValidatorInterface[]
     */
    private array $validators;

    public function __construct(ValidatorInterface ...$validators) {
        $this->validators = $validators;
    }

    public function isInvalid(mixed $value): ?Translatable {
        foreach ($this->validators as $validator) {
            if ($reason = $validator->isInvalid($value)) {
                return $reason;
            }
        }
        return null;
    }

}