<?php
namespace Fubber\Meta\Validators;

use Countable;
use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaValidator;

use function Fubber\trans;

class MaxLength extends MetaValidator {

    public function __construct(
        public readonly int $maxLength
    ) {}

    public function isInvalid(mixed $value): ?Translatable {
        $length = \mb_strlen($value);
        if ($length <= $this->maxLength) {
            return null;
        }
        return trans("Maximum length is {maxLength}", $this);
    }
}