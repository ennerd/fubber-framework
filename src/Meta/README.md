Structure:

1. Type is an object that describes a "thing".
2. Collection is a "type" holding zero or more Atoms. (example; a table in a database)
3. Atom is a "type" having zero or more Properties. (example; a row in a table)
4. Property is a named value associated with an atom (example; a column in a row)

A "type" is an object describing a "thing" (collections, atoms and atom-properties)