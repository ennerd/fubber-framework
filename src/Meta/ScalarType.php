<?php
namespace Fubber\Meta;

/**
 * The native PHP type
 */
enum ScalarType: string {
    const Float = 'float';
    const Integer = 'int';
    const String = 'string';
    const Object = 'object';
    const Array = 'array';
    const Resource = 'resource';
    const NULL = 'null';
} 