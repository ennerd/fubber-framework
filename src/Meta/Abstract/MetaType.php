<?php
namespace Fubber\Meta\Abstract;

use Error;
use Fubber\Meta\RootTypeInterface;
use Fubber\I18n\Translatable;
use Fubber\Meta\TypeInterface;
use Fubber\Meta\UsageContext;

abstract class MetaType implements TypeInterface {

    private static array $_instances = [];
    private readonly RootTypeInterface $parentType;

    /**
     * Create the singleton instance of this type
     * 
     * @return void 
     */
    abstract protected static function construct(): self;

    public final static function instance(): static {
        return self::$_instances[static::class] = self::$_instances[static::class] ?? static::construct();
    }

    protected final function __construct(RootTypeInterface $parentType) {
        $this->parentType = $parentType;
    }

    public function getParentType(): RootTypeInterface {
        return $this->parentType;
    }

    public function __call(string $name, array $arguments) {
        $parent = $this->getParentType();
        if (\method_exists($parent, $name)) {
            return $parent->$name(...$arguments);
        }
        throw new Error("Call to undefined method " . static::class . "::$name()");
    }
}