<?php
namespace Fubber\Meta\Abstract;

interface EnumIntegerBackedType extends IntegerBackedType, EnumBackedType {

    /**
     * @return iterable<int> 
     */
    public function getLegalValues(): iterable;
}