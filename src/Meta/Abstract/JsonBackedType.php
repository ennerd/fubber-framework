<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface JsonBackedType extends BackedType {

    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): mixed;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(mixed $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): mixed;

    public function isInvalid(mixed $value): ?Translatable;
}