<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface ObjectBackedType extends BackedType {
    
    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): object;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(object $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): object;

    public function isInvalid(object $value): ?Translatable;
}