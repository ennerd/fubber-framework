<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface FloatBackedType extends BackedType {

    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): float;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(float $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): float;

    public function isInvalid(float $value): ?Translatable;
}