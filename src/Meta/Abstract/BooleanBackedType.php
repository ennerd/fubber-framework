<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface BooleanBackedType extends BackedType {

    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): bool;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(bool $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): bool;

    public function isInvalid(bool $value): ?Translatable;
}