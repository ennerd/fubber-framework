<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface StringBackedType extends BackedType {
    
    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): string;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(string $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): string;

    public function isInvalid(string $value): ?Translatable;
}