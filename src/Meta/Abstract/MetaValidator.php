<?php
namespace Fubber\Meta\Abstract;

use Fubber\Meta\ValidatorInterface;

abstract class MetaValidator implements ValidatorInterface {

    public function jsonSerialize(): mixed {
        return ['Validator' => static::class] + (array) $this;
    }

}