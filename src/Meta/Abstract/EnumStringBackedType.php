<?php
namespace Fubber\Meta\Abstract;

interface EnumStringBackedType extends StringBackedType, EnumBackedType {
    /**
     * @return iterable<string> 
     */
    public function getLegalValues(): iterable;
}