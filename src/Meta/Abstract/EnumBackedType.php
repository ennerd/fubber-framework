<?php
namespace Fubber\Meta\Abstract;

interface EnumBackedType extends BackedType {
    public function getLegalValues(): iterable;
}