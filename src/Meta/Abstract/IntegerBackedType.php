<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface IntegerBackedType extends BackedType {

    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): int;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(int $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): int;

    public function isInvalid(int $value): ?Translatable;
}