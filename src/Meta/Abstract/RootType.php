<?php
namespace Fubber\Meta\Abstract;

use Fubber\Meta\RootTypeInterface;
use Fubber\Meta\TypeInterface;
use Fubber\Meta\UsageContext;

abstract class RootType implements RootTypeInterface {

    private static ?self $instance = null;

    protected final function __construct() {}

    public final static function instance(): static {
        if (!self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public final function getParent(): ?TypeInterface {
        return null;
    }
    
}