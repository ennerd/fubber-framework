<?php
namespace Fubber\Meta\Abstract;

/**
 * @mixin BooleanBackedType|FloatBackedType|IntegerBackedType|NumberBackedType|ObjectBackedType|StringBackedType
 * @package Fubber\Meta\Abstract
 */
interface BackedType {
    public function fromString(string $value): mixed;
}