<?php
namespace Fubber\Meta\Abstract;

use Fubber\I18n\Translatable;

interface NumberBackedType extends BackedType {

    /**
     * Parse a JSON string into the native value
     */
    public function fromJSON(mixed $json): int|float;

    /**
     * Encode the value as a JSON string
     */
    public function toJSON(int|float $value): mixed;

    /**
     * Parse a string (typically from a form field as POST or GET data)
     */
    public function fromString(string $value): int|float;

    public function isInvalid(int|float $value): ?Translatable;
}