<?php
namespace Fubber\Meta;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;

class Atom implements AtomInterface {

    /**
     * @var array<string, PropertyInterface>
     */
    private array $properties = [];

    public function __construct(string $type, PropertyInterface ...$property) {
        
    }

    public function getProperty(string $propertyName): ?PropertyInterface {
        return $this->properties[$propertyName] ?? null;
    }

    public function getProperties(): array {
        return $this->properties;
    }

    public function getParentType(): RootTypeInterface {

    }

    public static function instance(): static { }

    public function getNoun(): Translatable { }


}