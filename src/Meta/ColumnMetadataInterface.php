<?php
namespace Fubber\Meta;

use Fubber\I18n\Translatable;

interface ColumnMetadataInterface {

    /**
     * The name of the column
     * 
     * @return Translatable 
     */
    public function name(): Translatable;

    /**
     * The caption when the column is shown in a form or in a table
     */
    public function caption(SemanticContextInterface $context): Translatable;

    /**
     * Get the semantic type of the column; this contains informatino
     * about what the meaning of the value and how to represent it in
     * different contexts; in HTML, in JSON, as a PHP primitive and
     * stored in the database.
     * 
     * @return Fubber\Meta\SemanticTypeInterface 
     */
    public function type(): TypeInterface;


}