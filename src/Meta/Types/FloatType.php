<?php
namespace Fubber\Meta\Types;

use Fubber\Meta\UsageContext;
use Fubber\Meta\TypeInterface;
use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\FloatBackedType;
use Fubber\Meta\Abstract\RootType;
use Fubber\Meta\U;
use InvalidArgumentException;

final class FloatType extends RootType implements FloatBackedType {

    public function fromJSON(mixed $json): float {
        if (!\is_numeric($json)) {
            throw new InvalidArgumentException("Expecting a numeric value");
        }
        return \floatval($json);
    }

    public function toJSON(float $value): mixed {
        return $value;
    }

    public function fromString(string $value): float {
        if (!\is_numeric($value)) {
            throw new InvalidArgumentException("Expecting a numeric value");
        }
        return \floatval($value);
    }

    public function isInvalid(float $value): ?Translatable {
        return null;
    }

    public function getNoun(): Translatable {
        return U::trans("number|-|numbers");
    }

}