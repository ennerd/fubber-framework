<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use InvalidArgumentException;

use function Fubber\trans;

class CountryType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Invalid country value");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): self {
        return new self(IdentifierType::instance());
    }

    public function getParentType(): IdentifierType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans("country code|-|country codes");
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            return new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (!\preg_match('/[A-Z]{3}/', $value)) {
            return trans("Country codes are 3 uppercase characters long according to ISO-3166");
        }
        return $this->getParentType()->isInvalid($value);
    }

}