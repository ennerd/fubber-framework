<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\RootTypeInterface;
use InvalidArgumentException;

use function Fubber\trans;

class IdentifierType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!(\is_string($json) || \is_numeric($json))) {
            throw new InvalidArgumentException("Expecting a number or string");
        }
        return $this->fromString($json);    
    }

    public function toJSON(string $value): mixed {
        return $value;
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;        
    }

    public function isInvalid(string $value): ?Translatable {
        if (\preg_match('/\s/', $value)) {
            return trans("A key can't contain whitespace");
        }
        return null;
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getParentType(): StringType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans("ID|-|IDs");
    }

}