<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use InvalidArgumentException;

use function Fubber\trans;

class PhoneNumberType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expecting a string");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getNoun(): Translatable {
        return trans("phone number|-|phone numbers");
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }

        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (\preg_match('/^((\+|00)?[1-9][0-9]{0,2}+)?\ ?(((?<=[0-9])([ -]|\ ?(~|ext\.?|extension)\ ?)(?=[0-9]))|[0-9]|\ ?\([0-9]+\)\ ?)+$/', $value) !== 1) {
            return trans("Invalid telephone number");
        }
        return null;
    }

}