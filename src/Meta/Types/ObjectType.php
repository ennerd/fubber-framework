<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\ObjectBackedType;
use Fubber\Meta\Abstract\RootType;
use Fubber\Meta\U;
use InvalidArgumentException;
use JsonSerializable;
use stdClass;

use function Fubber\trans;

final class ObjectType extends RootType implements ObjectBackedType {

    public function fromJSON(mixed $json): object {
        if (!\is_object($json)) {
            throw new InvalidArgumentException("Expecting an object");
        }
        if ($json::class !== stdClass::class && !($json instanceof JsonSerializable)) {
            throw new InvalidArgumentException("Object is not json serializable");
        }
        return $json;
    }

    public function toJSON(object $value): mixed {
        return \json_encode($value);
    }

    public function fromString(string $value): object {
        return \json_decode($value);
    }

    public function isInvalid(object $value): ?Translatable {
        if ($value::class !== stdClass::class && !($value instanceof JsonSerializable)) {
            return trans("Object is not json serializable");
        }
        return null;
    }

    public function getNoun(): Translatable {
        return U::trans("object|-|objects");
    }

}