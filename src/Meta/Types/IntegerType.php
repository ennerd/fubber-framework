<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\IntegerBackedType;
use Fubber\Meta\Abstract\MetaType;
use InvalidArgumentException;

use function Fubber\trans;

final class IntegerType extends MetaType implements IntegerBackedType {

    public function fromJSON(mixed $json): int {
        if (\is_int($json)) {
            return $json;
        }
        if (\is_numeric($json) && $json == (int) $json) {
            return (int) $json;
        }
        throw new InvalidArgumentException("Expecting an integer value");
    }

    public function toJSON(int $value): mixed {
        return $value;
    }

    public function fromString(string $value): int {
        if (!\is_numeric($value) || $value != (int) $value) {
            throw new InvalidArgumentException("Expecting an integer value");
        }
        return \intval($value);
    }

    public function isInvalid(int $value): ?Translatable {
        return null;
    }

    protected static function construct(): self {
        return new static(FloatType::instance());
    }

    public function getNoun(): Translatable {
        return trans("integer|-|integers");
    }
}