<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\Abstract\MetaType;
use InvalidArgumentException;

use function Fubber\trans;

final class NameType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expecting a string");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    public function isInvalid(string $value): ?Translatable {
        if (\preg_match('/^\s|\s$|\s\s|(?=[^ ])\s|[^\p{L}]/', $value)) {
            return trans("The name contains illegal spaces or numbers");
        }
        return null;
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    protected static function construct(): self {
        return new static(StringType::instance());
    }

    public function getNoun(): Translatable {
        return trans("name|-|names");
    }
}