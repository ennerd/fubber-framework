<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use InvalidArgumentException;

use function Fubber\trans;

/**
 * See Well-Known Text (WKT) Format
 * 
 * @package Fubber\Meta
 */
class LatitudeType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_numeric($json)) {
            throw new InvalidArgumentException("Expecting a numeric value");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return \floatval($this->fromString($value));
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getParentType(): StringType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans('latitude|-|latitudes');
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (!\is_numeric($value)) {
            return trans("Latitude is a numeric value");
        }
        $value = (float) $value;
        if ($value > 90) {
            return trans("Maximum latitude is 90");
        }
        if ($value < -90) {
            return trans("Minimum latitude is -90");
        }
        return null;
    }

}