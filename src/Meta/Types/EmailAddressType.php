<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use InvalidArgumentException;

use function Fubber\trans;

class EmailAddressType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expecting an e-mail address string");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): EmailAddressType {
        return new self(StringType::instance());
    }

    public function getNoun(): Translatable {
        return trans("e-mail address|-|e-mail addresses");
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }

        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (!\filter_var($value, \FILTER_VALIDATE_EMAIL)) {
            return trans("Invalid e-mail address");
        }
        return null;
    }

}