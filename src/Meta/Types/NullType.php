<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\JsonBackedType;
use Fubber\Meta\Abstract\RootType;
use InvalidArgumentException;

use function Fubber\trans;

class NullType extends RootType implements JsonBackedType {

    public function fromJSON(mixed $json): mixed {
        if ($json !== null) {
            throw new InvalidArgumentException("Expecting `null` value");
        }
        return null;
    }

    public function toJSON(mixed $value): mixed {
        if (\is_string($value)) {
            $value = $this->fromString($value);
        } elseif ($value !== null) {
            throw new InvalidArgumentException("Expecting `null` value");
        }
        return null;
    }

    public function fromString(string $value): mixed {
        if ($value !== '' && $value !== 'null') {
            throw new InvalidArgumentException("Only empty strings or \"null\" represent `null`");
        }
        return null;
    }

    public function isInvalid(mixed $value): ?Translatable {
        if ($value !== null) {
            return trans("Value is not `null`");
        }
        return null;
    }

    public function getNoun(): Translatable {
        return trans("no value|-|no values");
    }

}