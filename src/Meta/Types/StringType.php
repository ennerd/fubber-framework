<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\RootType;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\U;

use function Fubber\trans;

final class StringType extends RootType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new \InvalidArgumentException("Expecting a string");
        }
        return $json;
    }

    public function toJSON(string $value): mixed {
        return $value;
    }

    public function fromString(string $value): string {
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        return null;
    }

    public function getNoun(): Translatable {
        return trans("string|-|strings");
    }

}