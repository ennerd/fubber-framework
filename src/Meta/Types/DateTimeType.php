<?php
namespace Fubber\Meta\Types;

use DateTimeImmutable;
use DateTimeZone;
use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\Abstract\MetaType;
use InvalidArgumentException;

use function Fubber\trans;

class DateTimeType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expecting a string in ISO 8601 format");
        }
        $datetime = DateTimeImmutable::createFromFormat('c', $json);
        return $datetime->setTimezone(self::UTC())->format('Y-m-d H:i:s');
    }

    public function toJSON(string $value): mixed {
        $datetimeString = $this->fromString($value, self::UTC());
        $datetimeObject = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $datetimeString, self::UTC());
        return $datetimeObject->format('c');
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getNoun(): Translatable {
        return trans("date and time|-|date times");
    }

    public function getParentType(): StringType {
        return parent::getParentType();
    }

    public function fromString(string $value, string|DateTimeZone $timezone=null): string {
        if (\is_string($timezone)) {
            $timezone = new DateTimeZone($timezone);
        }

        $dt = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value, $timezone);
        if ($dt === false) {
            throw new InvalidArgumentException("Invalid datetime format");
        }

        return $dt->setTimezone(self::UTC())->format('Y-m-d H:i:s');
    }

    public function isInvalid(string $value): ?Translatable {
        $dt = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        if ($dt === false) {
            return null;
        }
        return trans("Invalid datetime format");
    }

    public static function UTC(): DateTimeZone {
        return new DateTimeZone('UTC');
    }

}