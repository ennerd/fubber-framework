<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\RootTypeInterface;
use InvalidArgumentException;

use function Fubber\trans;

class SlugType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expecting a string");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getParentType(): StringType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans('slug|-|slugs');
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (\preg_match('/^\w/u', $value) !== 1) {
            return trans("The slug does not start with a character or number");
        }
        if (\preg_match('/\w$/u', $value) !== 1) {
            return trans("The slug does not end with a character or number");
        }
        if (\preg_match('/^((?<=\p{L})[\w\-](?=\p{L})|\w)+$/u', $value) !== 1) {
            return trans("The slug does not only contain alphanumeric characters, dashes and underscores");
        }
        return null;
    }

}