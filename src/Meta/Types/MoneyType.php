<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\RootTypeInterface;
use InvalidArgumentException;

use function Fubber\trans;

class MoneyType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (\is_string($json) && !$this->isInvalid($json)) {
            return $json;
        } elseif (\is_numeric($json)) {
            return (string) $json;
        }
        throw new InvalidArgumentException("Invalid value");
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getParentType(): StringType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans("money|-|money");
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (!\preg_match('/^-?(0|[1-9][0-9]*)(\.[0-9]{1,4)?$', $value)) {
            return trans("Number must contain digits and optionally up to 4 digits after a decimal symbol '.'");
        }
        return null;
    }


}