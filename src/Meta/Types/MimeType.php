<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use InvalidArgumentException;

use function Fubber\trans;

class MimeType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expecting mime type as a string");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): self {
        return new self(StringType::instance());
    }

    public function getNoun(): Translatable {
        return trans('MIME type|-|MIME types');
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (\preg_match('/^(application|audio|font|image|model|text|video|multipart|x-((?<=\w)[\-+.](?=\w)|\w)+)\/((?<=\w)[\-+.](?=\w)|\w)+$/u', $value) !== 1) {
            return trans("Invalid MIME media type");
        }
        return null;
    }

}
//