<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Util\Url;
use InvalidArgumentException;
use Stringable;

use function Fubber\trans;

class UrlType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        if (!\is_string($json)) {
            throw new InvalidArgumentException("Expected a string");
        }
        return $this->fromString($json);
    }

    public function toJSON(string $value): mixed {
        return $this->fromString($value);
    }

    protected static function construct(): self {
        return new self(ObjectType::instance());
    }

    public function getParentType(): ObjectType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans('URL|-|URLs');
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function isInvalid(string $value): ?Translatable {
        if (!\filter_var($value, \FILTER_VALIDATE_URL)) {
            return trans("Invalid URL address");
        }
        return null;
    }

}