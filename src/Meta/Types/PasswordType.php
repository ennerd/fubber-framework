<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\RootTypeInterface;
use InvalidArgumentException;

use function Fubber\trans;

final class PasswordType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        return $this->getParentType()->fromJSON($json);
    }

    public function toJSON(string $value): mixed {
        return $this->getParentType()->toJSON($value);
    }

    public function isInvalid(string $value): ?Translatable {
        return null;
    }

    public function fromString(string $value): string {
        if ($reason = $this->isInvalid($value)) {
            throw new InvalidArgumentException($reason);
        }
        return $value;
    }

    public function getParentType(): StringType {
        return parent::getParentType();
    }

    protected static function construct(): self {
        return new static(StringType::instance());
    }

    public function getNoun(): Translatable {
        return trans("password|-|passwords");
    }
}