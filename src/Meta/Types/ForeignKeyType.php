<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\StringBackedType;
use Fubber\Meta\Abstract\MetaType;
use Fubber\Meta\RootTypeInterface;
use InvalidArgumentException;

use function Fubber\trans;

class ForeignKeyType extends MetaType implements StringBackedType {

    public function fromJSON(mixed $json): string {
        return $this->getParentType()->fromJSON($json);
    }

    public function toJSON(string $value): mixed {
        return $this->getParentType()->toJSON($value);
    }

    protected static function construct(): self { 
        return new self(IdentifierType::instance());
    }

    public function getParentType(): IdentifierType {
        return parent::getParentType();
    }

    public function getNoun(): Translatable {
        return trans("foreign key|-|foreign keys");
    }

    public function fromString(string $value): string {
        return $this->getParentType()->fromString($value);
    }

    public function isInvalid(string $value): ?Translatable {
        return $this->getParentType()->isInvalid($value);
    }
}