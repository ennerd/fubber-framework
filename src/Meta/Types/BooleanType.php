<?php
namespace Fubber\Meta\Types;

use Fubber\I18n\Translatable;
use Fubber\Meta\Abstract\BooleanBackedType;
use Fubber\Meta\Abstract\EnumBackedType;
use Fubber\Meta\Abstract\RootType;

use function Fubber\trans;

final class BooleanType extends RootType implements BooleanBackedType, EnumBackedType {

    public function fromJSON(mixed $json): bool {
        return (bool) $json;
    }

    public function toJSON(bool $value): mixed {
        return $value;
    }

    public function getLegalValues(): iterable {
        return [ true, false ];
    }

    public function fromString(string $value): bool {
        if ($value === '' || $value === '0') {
            return false;
        }
        switch (\strtolower(\trim($value))) {
            case '':
            case 'off':
            case 'no':
                return false;
        }
        return true;
    }

    public function isInvalid(bool $value): ?Translatable {
        return null;
    }

    public function getNoun(): Translatable {
        return trans("boolean|-|booleans");
    }
}