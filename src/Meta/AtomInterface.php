<?php
namespace Fubber\Meta;

use DateTime;

/**
 * An atom is generally an object representing a row in a database,
 * for example a "user" or a "department" or a "purchase". An atom
 * has properties.
 * 
 * 
 * @package Fubber\Meta
 */
interface AtomInterface extends TypeInterface {

    public function getProperty(string $propertyName): ?PropertyInterface;

    /**
     * Get all declared properties for this atom
     * 
     * @return array<string, PropertyInterface>
     */
    public function getProperties(): array;

}