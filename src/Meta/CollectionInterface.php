<?php
namespace Fubber\Meta;

/**
 * Interface for objects that describe a collection, typically a database
 * table.
 * 
 * @package Fubber\Meta
 */
interface CollectionInterface extends AtomInterface {

    /**
     * Describe this collection in a given context
     * 
     * @return TypeInterface 
     */
    public function describe(): TypeInterface;

    /**
     * Describe the members of this collection
     * 
     * @return TypeInterface 
     */
    public function describeMembers(): TypeInterface;

}