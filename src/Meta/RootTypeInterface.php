<?php
namespace Fubber\Meta;

use Fubber\I18n\Translatable;
use ReflectionType;

interface RootTypeInterface {

    /**
     * Get the singleton instance of this type
     * 
     * @return static
     */
    public static function instance(): static;

    /**
     * The noun that describes the semantic type (when we put 'a' or 'an' in front)
     * 
     * Examples: "person", "human", "account", "elephant"
     * 
     * @return Translatable 
     */
    public function getNoun(): Translatable;
}