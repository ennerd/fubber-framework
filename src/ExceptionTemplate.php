<?php
namespace Fubber;

use Fubber\Kernel;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\TemplateInterface;
use Fubber\PHP\ClosureTool;
use Psr\Http\Message\RequestInterface;
use Throwable;
use UnitEnum;
use Whoops\Handler\Handler;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PlainTextHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\XmlResponseHandler;
use Whoops\Util\TemplateHelper;

/**
 * @package Fubber
 */
final class ExceptionTemplate implements TemplateInterface {

    public function __construct(
        public readonly Throwable $exception,
        public readonly ?RequestInterface $request=null,
        public readonly ?string $pageTitle=null
    ) {
    }

    public function getContentType(): string {
        $accept = $this->request?->getHeader('Accept') ?? $_SERVER['HTTP_ACCEPT'] ?? null;
        
        if ($accept === null) {
            if ($this->request?->getHeader('User-Agent') ?? $_SERVER['HTTP_USER_AGENT'] ?? null) {
                $accept = 'text/html';
            }
        }

        if ($accept === null) {
            return 'text/plain';
        } elseif (\str_contains($accept, 'html')) {
            return 'text/html';
        } elseif (\str_contains($accept, 'json')) {
            return 'application/json';
        } elseif (\str_contains($accept, 'xml')) {
            return 'application/xml';
        } else {
            return 'text/plain';
        }
    }

    public function render(): string {
        \error_log($this->exception::class.': '.$this->exception->getMessage().' in '.$this->exception->getFile().':'.$this->exception->getLine());
        try {
            $whoops = new \Whoops\Run();
            $whoops->allowQuit(false);
            $whoops->writeToOutput(false);
            $whoops->sendHttpCode(false);        

            /*
            if ($bufferedOutput !== '') {
                $prettyPageHandler->addDataTable("Direct Output", [
                    'output' => $bufferedOutput,
                ]);
            }
            */

            $debugInfo = Instrumentation::getExceptionInfo($this->exception);

            $request = $debugInfo->request ?? $debugInfo->state?->request;
            $state = $debugInfo->state;

            $closureInfo = "Not available";

            if ($debugInfo->closure) {
                $ct = new ClosureTool($debugInfo->closure);
                $closureInfo = [
                    'Name' => $ct->getDescriptiveName(),
                    'Direct Cause?' => $ct->didThrow($this->exception) ? 'Yes, exception was thrown by the closure' : 'No',
                    'Doc Comment' => \preg_replace('/^[ \t]*\/\*+[ \t]*|[ \t]*(\*\/$|\*[ \t])/m', '', $ct->reflect()->getDocComment()),
                ];
            }

            switch ($this->getContentType()) {
                case 'text/html':
                    $fubberVars = [
                        'panels' => [],
                    ];

                    $prettyPageHandler = new class($fubberVars) extends PrettyPageHandler {

                        public function __construct(private array &$fubberVars) {
                            parent::__construct();
                            $this->templateHelper = new class($fubberVars, $this->templateHelper) extends TemplateHelper {
                                public function __construct(private array &$fubberVars, TemplateHelper $previous) {
                                    parent::__construct();
                                    $this->setCloner($previous->getCloner());
                                }

                                public function getVariable($variableName, $defaultValue = null) {
                                    if ($variableName === 'fubber') {
                                        return $this->fubberVars ?? $defaultValue;
                                    }
                                    return parent::getVariable($variableName, $defaultValue);
                                }

                                public function getVariables() {
                                    $result = parent::getVariables();
                                    $result['fubber'] = &$this->fubberVars;
                                    return $result;
                                }
                            };
                        }


                    };
                    $prettyPageHandler->setApplicationRootPath(Kernel::getComposerPath());
                    $prettyPageHandler->setPageTitle($this->pageTitle ? $this->pageTitle . ' (' .$this->exception::class . ': ' . $this->exception->getMessage() . ')' : $this->exception::class.' was not handled');
                    $prettyPageHandler->addResourcePath(Kernel::getFrameworkPath() . '/assets/Whoops');
                    $prettyPageHandler->addCustomCss('PrettyPageHandler.css');
                    $prettyPageHandler->setEditor(PrettyPageHandler::EDITOR_VSCODE);
                    $whoops->pushHandler($prettyPageHandler);

                    $phase = Kernel::instance()->phases->getCurrentState();
                    if ($phase instanceof UnitEnum) {
                        $phase = $phase->name;
                    }
                    $details = [ 'Kernel Phase' => $phase ?? 'N/A' ] + [
                        'Suggestion' => (string) $debugInfo->suggestion,
                        'Description' => (string) $debugInfo->description,
                        'Suggested HTTP Status' => $debugInfo->httpStatusCode . ' ' . $debugInfo->httpReasonPhrase,
                        'Closure Info' => $closureInfo,
                        'Request' => $request ? [
                            'method' => $request->getMethod(),
                            'target' => $request->getRequestTarget(),
                            'headers' => $request->getHeaders(),
                            'uri' => $request->getUri(),
                        ] : 'N/A',
                        'Session ID' => $state ? $state->session->getId() : 'N/A',
                    ] + $debugInfo->extraData;

                    $detailContents = '';
                    foreach ($details as $key => $value) {
                        if (!empty($value)) {
                            $detailContents .= <<<HTML
                            <div class="data-table">
                                <label>$key</label>
                            HTML;

                            if (\is_array($value) || \is_object($value)) { 
                                $detailContents .= '<pre>'.\json_encode($value, \JSON_PRETTY_PRINT).'</pre>';
                            } else {
                                $detailContents .= '<div>'.\htmlspecialchars($value).'</div>';
                            }
                            $detailContents .= '</div>';
                        }
                    }

                    $fubberVars['panels'][] = <<<HTML
                        <div class="details">
                            <h2 class="details-heading">Fubber Framework</h2>
                            <div class="data-table-container">
                                {$detailContents}
                            </div>
                        </div>
                    HTML;
                    //$prettyPageHandler->addDataTable("Exception Details", $details);

                    $whoops->appendHandler(new class() extends Handler {
                        public function handle() {
                            $exception = $this->getException();
                            \error_log(\get_class($exception).': '.$exception->getMessage()." in \n file ".$exception->getFile().":".$exception->getLine());
                            return Handler::DONE;
                        }
                    });
                    break;
                case 'application/json':
                    $whoops->pushHandler($handler = new JsonResponseHandler());
                    $handler->addTraceToOutput(true);
                    break;
                case 'application/xml':
                    $whoops->pushHandler($handler = new XmlResponseHandler());
                    $handler->addTraceToOutput(true);
                    break;
                case 'text/plain':
                default:
                    $whoops->pushHandler($handler = new PlainTextHandler());
                    $handler->addPreviousToOutput(true);
                    $handler->addTraceToOutput(true);
                    break;
            }

            $result = $whoops->handleException($this->exception);

            return $result;
        } catch (Throwable $exception) {
            return self::fallbackRender($this->exception);
        }
    }

    /**
     * Renders an exception with very simple HTML code and with no dependencies.
     * 
     * @param Throwable $exception 
     * @return string 
     */
    private static function fallbackRender(Throwable $exception, int $depth=0): string {
        $indenter = function(string $string): string {
            return \preg_replace('/^|(?<=\n)/m', '  ', $string);
        };
        $info = Instrumentation::getExceptionInfo($exception);

        $className = $exception::class;
        $message = $exception->getMessage();
        $file = $exception->getFile();
        $line = $exception->getLine();
        $traceAsString = $indenter("--- TRACE ---\n".trim($exception->getTraceAsString())."\n-------------\n");
        $result = <<<HTML
            {$className}: {$message} in {$file}:{$line}
            {$traceAsString}
            HTML;

        $result = \trim($result)."\n\n";

        if ($info->description) {
            $result .= "Error class description:\n".$indenter($info->description)."\n\n";
        }
        if (!empty($info->extraData)) {
            try {
                $result .= "Extra debug information:\n".$indenter(\json_encode($info->extraData, \JSON_PRETTY_PRINT | \JSON_THROW_ON_ERROR))."\n\n";
            } catch (Throwable $e) {}
        }
        if ($info->suggestion) {
            $result .= "Suggestion:\n".$indenter($info->suggestion)."\n\n";
        }

        $result = \trim($result)."\n</pre>\n\n";

        $result = $result . ( $exception->getPrevious() ? $indenter("Preceding exception:\n".self::fallbackRender($exception->getPrevious(), $depth + 1)) : "" );
        $result = \trim($result);

        if ($depth > 0) {
            return $result."\n\n";
        }
        return <<<HTML
            <pre style='background-color: white; color: black; font-family: monospace; border: 2px solid #444; padding: 5px; margin: 0px; width: initial; height: initial; z-index: 32767; overflow: auto; min-width: 640px; min-height: 480px;'>
            $result
            </pre>
            HTML;
    }

    public function __toString() {
        return $this->render();
    }
}