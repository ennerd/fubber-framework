<?php
namespace Fubber\Parsing;

use Generator;

abstract class AbstractLexer {

    private readonly Generator $tokens;
    private array $buffer = [];
    private int $bufferIndex = 0;

    public final function tokenize(string $source): Tokens {
        return new Tokens($this->generateTokens($source));
    }

    abstract protected function generateTokens(string $source): Generator;
}