<?php
namespace Fubber\Parsing;

use Generator;
use Throwable;

final class Tokens {
    
    private array $buffer = [];
    private int $bufferIndex = 0;
    private int $offset = 0;

    public function __construct(private readonly Generator $tokens) {}

    public final function getOffset(): int {
        return $this->offset;
    }

    /**
     * Get all remaining tokens in an array.
     * 
     * @return array 
     */
    public final function all(): array {
        $result = [];
        while ($this->peek()) {
            $result[] = $this->eat();
        }
        for ($i = \count($result) - 1; $i >= 0; $i--) {
            $this->uneat($result[$i]);
        }
        return $result;
    }

    public final function eat(string $type=null, string $customError=null): ?Token {
        if ($this->bufferIndex > 0) {
            $token = $this->buffer[--$this->bufferIndex];
        } elseif ($this->tokens->valid()) {
            $token = $this->tokens->current();
            $this->tokens->next();
        } else {
            $token = null;
        }

        if ($token === null) {
            if ($type !== null) {
                throw new SyntaxErrorException("Expecting `$type` at offset " . $this->offset . " but found end-of-file");
            } else {
                throw new SyntaxErrorException("Expected end-of-file at offset " . $this->offset);
            }
        } elseif ($type !== null && $token->type !== $type) {
            if ($customError !== null) {
                throw new SyntaxErrorException("$customError at offset ".$this->offset);
            } else {
                throw new SyntaxErrorException("Expected `$type` at offset " . $this->offset . ", got " . $token->type);
            }
        }

        return $token;
    }

    public final function uneat(Token $token): void {
        $this->buffer[$this->bufferIndex++] = $token;
        $this->offset = $token->offset;
    }

    public final function peek(int $distance=1): ?Token {
        $buffer = [];
        try {
            while ($distance-- > 0) {
                $buffer[] = $token = $this->eat();
            }
            return $token;
        } catch (Throwable $e) {
            return null;
        } finally {
            while ($buffer !== []) {
                $this->uneat(\array_pop($buffer));
            }
        }
    }
}