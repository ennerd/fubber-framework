<?php
namespace Fubber\Parsing;

use Closure;

abstract class AbstractParser {

    public function __construct(private readonly AbstractLexer $lexer) {}

    public function parse(string $source): mixed {
        return $this->start($this->lexer->tokenize($source));
    }

    abstract protected function start(Tokens $tokens): mixed;
}