<?php
namespace Fubber\Parsing;

class Token {
    public function __construct(
        public readonly string $type,
        public readonly string $contents,
        public readonly string $whitespace,
        public readonly int $offset,
        public readonly int $length,
    ) {}

    public function asMarkdown(): string {
        return $this->contents . $this->whitespace;
    }

    public function __toString() {
        return $this->contents . $this->whitespace;
    }

    public function __debugInfo() {
        return [
            'type' => $this->type,
            'offset' => $this->offset,
            'contents' => \strtr($this->contents, [ "\n" => "\\n", "\t" => "\\t"]),
            'whitespace' => \strtr($this->whitespace, [ "\n" => "\\n", "\t" => "\\t"]),
        ];
    }
}