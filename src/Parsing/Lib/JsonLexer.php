<?php
namespace Fubber\Parsing\Lib;

use Fubber\Parsing\AbstractLexer;
use Generator;

class JsonLexer extends AbstractLexer {

    const RE = <<<'RE'
        /
        (?<punc>[{}[\]:])|
        (?<string>"(\\\\|\\"|[^"])*+")|
        (?<whitespace>\s+)|
        (?<literal>[^{}[\]:"\s]++)
        /xu
        RE;

    protected function generateTokens(string $source): Generator {

    }

}