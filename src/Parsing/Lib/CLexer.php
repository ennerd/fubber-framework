<?php
namespace Fubber\Parsing\Lib;

use Fubber\Parsing\AbstractLexer;
use Generator;

class JsonLexer extends AbstractLexer {

    const RE = <<<'RE'
        /
        (?<struct>[{}[\]();])|
        (?<string>("(\\\\|\\"|[^"])*+")|('(\\\\|\\'|[^'])*+')|(`(\\\\|\\`|[^`])*+`))|
        (?<number>[1-9][0-9_](\.[0-9_]+)?|0[xX][0-9aAbBcCdDeEfF_]+|0[bB_][01_]+) |
        (?<operators>\+\+?|->|<-|--?|!==?|={1,3}|[><]=?|!|[.+\-*^&%\|]=|\.|\?\??|::?|&&?)|
        (?<whitespace>\s+)|
        (?<comment_single>(\#|\/\/)[ \t\S]*) |
        (?<comment_multi>\/\*[\s\S]*?\*\/) |
        (?<literal>|[^.;:(){}[\]<>"'+\-*\/%&=!?\s]++)
        /xu
        RE;

    protected function generateTokens(string $source): Generator {

    }

}