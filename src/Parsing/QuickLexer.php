<?php
namespace Fubber\Parsing;

use Generator;

final class QuickLexer extends AbstractLexer {

    private readonly string $regex;

    public function __construct(string $regex) {
        $this->regex = $regex;
    }

    protected function generateTokens(string $source): Generator {
        $offset = 0;
        \preg_match_all($this->regex, $source, $matches, \PREG_PATTERN_ORDER | \PREG_UNMATCHED_AS_NULL);
        // prepare space for all results to ensure ordering is maintained
        $keys = \array_filter(\array_keys($matches), \is_string(...));
        foreach ($matches[0] as $offset => $value) {
            foreach ($keys as $key) {
                if ($matches[$key][$offset] !== null) {
                    $length = \strlen($value);
                    yield new Token($key, $value, '', $offset, $length);
                    $offset += $length;
                    break;
                }
            }
        }
    }
    
}