<?php
namespace Fubber;

use Closure;
use Fubber\Kernel\Debug\Describe;
use Fubber\Kernel\Instrumentation\Instrumentation;
use Fubber\PHP\ReflectionTypeTool;
use ReflectionFunction;

class ClosureReturnTypeException extends RuntimeException {

    public function __construct(Closure $closure, string $expectedType, mixed $receivedValue) {
        parent::__construct("Expecting return type `" . $expectedType . "`, received `".\get_debug_type($receivedValue)."` from closure `" . Describe::function(new ReflectionFunction($closure)) . "`; expecting `$expectedType`");
        Instrumentation::setExceptionInfo(
            $this,
            closure: $closure,
            extraData: [
                'returnValue' => $receivedValue
            ]
        );
    }

}