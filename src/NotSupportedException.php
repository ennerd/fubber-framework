<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception that is thrown when an invoked method is not
 * supported. In some cases it might be more appropriate to use
 * InvalidStateException - if the method is supported, but can't
 * be invoked at this time.
 *
 * @see DeprecatedException
 * @see CodingErrorException
 */
class NotSupportedException extends CodingErrorException {

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "Not Supported";
    }
}
