<?php
$kernel = \Fubber\Kernel::init();

if ($dbUrl = $kernel->env->get('DATABASE_URL')) {
    $url = parse_url($dbUrl);
    switch ($url['scheme']) {
        case 'mysql':
            $paths = explode("/", $url['path']);
            $e = [
                'adapter' => 'mysql',
                'host' => $url['host'],
                'name' => $paths[1],
                'user' => $url['user'],
                'pass' => $url['pass'],
                'port' => isset($url['port']) ? $url['port'] : 3306,
                'charset' => 'utf8',
            ];
            break;
        default:
            throw new \Fubber\ConfigErrorException('Fubber Framework Phinx Config does not support database "'.$url['scheme'].'".');
    }
}
$migrationPaths = [
    $kernel->env->migrationsRoot,
    ];
$seedPaths = [
    $kernel->env->seedsRoot,
    ];

if (!is_dir($kernel->env->migrationsRoot)) {
    mkdir($kernel->env->migrationsRoot);
}
if (!is_dir($kernel->env->seedsRoot)) {
    mkdir($kernel->env->seedsRoot);
}

foreach ($kernel->bundles->findDirs('migrations') as $migrationDir) {
    $migrationPaths[] = $migrationDir;
}
foreach ($kernel->bundles->findDirs('seeds') as $seedDir) {
    $seedPaths[] = $seedDir;
}

return [
    'paths' => [
        'migrations' => $migrationPaths,
        'seeds' => $seedPaths,
    ],
    'environments' => [
        'default_migration_table' => 'phinxLog',
        'default_database' => 'current',
        "current" => $e
    ]
];
