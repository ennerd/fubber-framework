<?php
namespace Fubber;

use Fubber\Psr\HttpMessage\Response;

/**
 * A buffered response that should be sent via another response back to the browser.
 */
class PlainView extends Response {

    protected $rendered;
    protected $_body;

    public function __construct($output, array $headers=[], $statusCode=200, $reasonPhrase="Ok") {
        Hooks::dispatch('fubber.view.created', $template, $vars, $headers, $statusCode, $reasonPhrase);

        parent::__construct((string) $output, $headers, $statusCode, $reasonPhrase);
        Hooks::dispatch('fubber.view.output', $output);
    }

    public function __toString(): string {
        return (string) $this->getBody();
    }

}
