<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when a forbidden operation is attempted. It is not
 * for when the request is not authenticated or when users don't have 
 * access - use UnauthorizedException for that. This is for example for
 * when a user attempts to delete himself.
 */
class ForbiddenException extends AccessDeniedException {

    public function getDefaultStatus(): array {
        return [403, "Forbidden"];
    }


    public function getExceptionDescription(): string {
        return "Forbidden";
    }
}
