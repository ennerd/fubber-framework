<?php
namespace Fubber\NPM;

use Charm\Http\Message\Response;
use Fubber\PlainView;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RequestHandler implements RequestHandlerInterface {

    protected NPMRepository $repository;

    public function __construct(NPMRepository $repository) {
        $this->repository = $repository;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface {
        return new Response("This is the body", [
            'Content-Type' => 'text/plain',
        ]);
    }

    public function __debugInfo() {
        return [ 'repository' => $this->repository->path ];
    }

}