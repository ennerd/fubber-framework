<?php
namespace Fubber\NPM;

use Fubber\AssetManager\Asset;
use Fubber\AssetManager\Bucket;
use Fubber\AssetManager\Dependency;
use Fubber\AssetManager\InvalidArgumentException;
use stdClass;

/**
 * Represents `node_modules` repository and allows bundling packages
 * and resolving dependencies from assets within the repository.
 * 
 * @package Fubber\NPM
 */
class NPMRepository extends Bucket {

    public readonly stdClass $package;

    public function __construct(public readonly string $path) {
        $path = realpath($path);
        if (!\file_exists($packageLockPath = $path . \DIRECTORY_SEPARATOR . 'package-lock.json')) {
            throw new InvalidArgumentException("The packages haven't been installed with `npm` ($path/package-lock.json not found)");
        }
        $this->package = \json_decode(file_get_contents($packageLockPath));
        if (empty($this->package->lockfileVersion) || $this->package->lockfileVersion < 2) {
            throw new InvalidArgumentException("Lock file is from `npm <7.0` which aren't supported.");
        }
        foreach ($this->package->packages as $key => $subPackage) {
            $path = $key;
            $identifier = $subPackage->name ?? \substr($key, \strrpos($key, 'node_modules/') + 13);
            $version = self::filterVersion($subPackage->version);
            $dependencies = [];
            if (!empty($subPackage->dependencies)) {
                foreach ($subPackage->dependencies as $depId => $depVer) {
                    $depVer = self::filterVersion($depVer);
                    $dependencies[] = new Dependency($depId, $depVer);
                }
            }
            if (\str_contains($version, ':')) {
                var_dump($identifier, $version);die();
            }
            $package = new Package($this, $key);
            $this->add(new Asset(
                $identifier,
                $version,
                $package,
                ...$dependencies
            ));
        }
        parent::__construct(true);
    }

    protected static function filterVersion(string $version): string {
        if (\str_contains($version, ':')) {
            return '*';
        }
        if (\str_contains($version, '-')) {
            return self::filterVersion(explode('-', $version, 2)[0]);
        }
        return $version;
    }

    protected static function filterConstraint(string $constraint): string {
        $constraint = self::filterVersion($constraint);
        if ($constraint !== ltrim($constraint, '0123456789')) {
            return self::filterVersion('^'.$constraint);
        }

    }
}