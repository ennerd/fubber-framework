<?php
namespace Fubber\NPM;

use stdClass;

class Package {
    const SOURCE_NOT_LOADED = 0;
    const SOURCE_PACKAGEJSON = 1;
    const MODULE_UNKNOWN = 0;
    const MODULE_AMD = 1; // asynchronous module definition: requirejs. Expects a define() function to exists.
    const MODULE_UMD = 2; // creates a global object in the browser, but can also be used in the backend
    const MODULE_CJS = 4; // commonjs: expects `module.exports` to be available and all of it runs in a closure. the require() function is synchronous
    const MODULE_ESM = 8; // ES Module: modern browsers support it with `import React from 'react'` style imports.
    const MODULE_BROWSER_GLOBAL = 16; // Old style, defines a browser global

    private int $_source = self::SOURCE_NOT_LOADED;
    private readonly string $_path;
    private ?stdClass $_data = null;
    private stdClass|bool|null $_bower = null;

    public function __construct(
        public readonly NPMRepository $_repo,
        public readonly string $_key,
    ) {
        $this->_path = realpath($this->_repo->path . \DIRECTORY_SEPARATOR . $this->_key);
    }

    /**
     * Get the absolute path to a file inside this package.
     * 
     * @param string $file 
     * @return string 
     */
    public function getPath(string $file=null): string {
        if ($file === null) {
            return $this->_path;
        }
        return $this->_path . \DIRECTORY_SEPARATOR . $file;
    }

    public function getModuleFile(): string {
        $main = $this->_data->module ?? $this->_data->main ?? 'index';

        $ext = \pathinfo($main, \PATHINFO_EXTENSION);
        if (!$ext) {
            foreach ( ['.min.mjs', '.mjs'] as $mjsExt ) {

            }
        }
        throw new \Exception();
    }

    /**
     * Expose the keys of the package.json
     * 
     * @param string $key 
     * @return mixed 
     * @throws LogicException 
     */
    public function __get(string $key) {
        $this->load();

        return $this->_data->$key ?? null;
    }

    public function __isset(string $key) {
        $this->load();

        return isset($this->_data->$key);
    }

    protected function load(): void {
        if ($this->_source === self::SOURCE_NOT_LOADED) {
            /**
             * @todo Consider supporting bower.json or other files which may provide better targeting
             */
            if (\is_file($filename = $this->_path . \DIRECTORY_SEPARATOR . 'package.json')) {
                // package.json is a little more difficult to be sure about
                $this->_data = \json_decode(\file_get_contents($filename));
                $this->_source = self::SOURCE_PACKAGEJSON;
            } else {
                throw new LogicException("Expected to find `bower.json` or `package.json` in `{$this->_path}`");
            }
        }
    }

    protected function loadBower(): bool {
        if ($this->_bower === null) {
            $filename = $this->_path . \DIRECTORY_SEPARATOR . 'bower.json';
            if (!\is_file($filename)) {
                $this->_bower = false;
            } else {
                $this->_bower = \json_decode(\file_get_contents($filename));
            }
        }
        return !!$this->_bower;
    }

    /**
     * Returns the file path to a CJS script file, script which can be loaded via a traditional `<script>` tag in browsers, and the script
     * usually declares a global or adds itself to another library that it depends on (often jQuery).
     * 
     * This requires the package to have an UMD or AMD module.
     * 
     * @return string 
     */
    public function getGlobalScriptFile(): ?string {
        $this->load();

        // package.json conventions it seems?
        if (!empty($this->_data->jsdelivr)) {
            return $this->_path . \DIRECTORY_SEPARATOR . $this->_data->jsdelivr;
        } elseif (!empty($this->_data->unpkg)) {
            return $this->_path . \DIRECTORY_SEPARATOR . $this->_data->unpkg;
        } elseif (!empty($this->_data->browser)) {
            return $this->_path . \DIRECTORY_SEPARATOR . $this->_data->browser;
        }

        $main = $this->_data->main ?? 'index.js';

        // if the main file has no extension, check directly to see for a .min.js variant
        $ext = \pathinfo($main, \PATHINFO_EXTENSION);
        if (empty($ext)) {
            $main .= '.js';
        }

        // is there a minified version available, that's a good hint
        $minifiedPath = $this->_path . \DIRECTORY_SEPARATOR . \preg_replace('/\.[jJ][sS]$/', '.min.js', $main, 1, $count);
        if ($count > 0 && \file_exists($minifiedPath)) {
            return $minifiedPath;
        }

        if (\file_exists($this->_path . \DIRECTORY_SEPARATOR . $main)) {
    
    
            // if a style sheet is defined, we'll assume this is meant for the browser
            if (!empty($this->_data->style)) {
                return $this->_path . \DIRECTORY_SEPARATOR . $main;
            }

            // if jquery is among the dependencies, we'll assume this is meant for the browser (as a jQuery module)
            if (!empty($this->_data->dependencies->jquery) || !empty($this->_data->peerDependencies->jquery)) {
                return $this->_path . \DIRECTORY_SEPARATOR . $main;
            }


        }

        /**
         * By now we should have solved it, but if not - look for bower.json, it might contain the stuff
         * we need.
         */
        if ($this->loadBower()) {
            if (isset($this->_bower->main)) {
                if (\is_string($this->_bower->main)) {
                    $path = $this->_bower->main;
                } elseif (\is_array($this->_bower->main)) {
                    foreach ($this->_bower->main as $file) {
                        if (\str_ends_with($file, '.js')) {
                            $path = $file;
                            break;
                        }
                    }
                }
            }
        }

        return null;
    }

    public function __debugInfo() {
        $key = $this->_key;
        return [
            'key' => $this->_key,
            'lockFileInfo' => $this->_repo->package->packages->$key,
        ];
    }
}