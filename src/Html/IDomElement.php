<?php
namespace Fubber\Html;

interface IDomElement {
    public function getClone();
    public function setParent(IDomElement $parent);
    public function getParent();
    public function map(callable $filter);
    public function __toString();
}