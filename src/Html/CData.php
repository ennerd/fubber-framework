<?php
namespace Fubber\Html;

class CData implements IDomElement {
    protected $_text;
    protected $_parent;
    
    public function __construct(string $text) {
        /*
        if($text instanceof CData)
            $text = $text->__toString();
        if(!is_string($text)) {
            throw new Exception("CData requires a string, but got '".get_debug_type($text)."'");
        }
        */
        $this->_text = $text;
    }
    
    public function setParent(IDomElement $parent) {
        $this->_parent = $parent;
        return $this;
    }
    
    public function getParent() {
        return $this->_parent;
    }
    
    public function getClone() {
        return clone $this;
    }
    
    public function map(callable $filter) {
        $filter($this);
    }
    
    public function __toString() {
        return htmlspecialchars($this->_text);
    }
}