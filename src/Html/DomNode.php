<?php
namespace Fubber\Html;
use Fubber\Parsing\StringParser;

class DomNode implements IDomElement {
    protected $_root;
    protected $_children;
    protected $_parent;
    
    /**
     * Parses arbitrary XML into a DomNode
     */
    public static function parse($source, $skipWhitespace=false) {
        $parser = new StringParser($source, $skipWhitespace);

        // Consumes only whitespace
        $consumeWhitespace = function() use ($parser) {
            $token = $parser->consume();
            if($parser->type == StringParser::CONSUMED_WHITESPACE)
                return $token;
            $parser->push($token);
            return '';
        };
        
        // Consumes only words
        $consumeWord = function() use ($parser) {
            $token = $parser->consume();
            if($parser->type != StringParser::CONSUMED_WORD) {
                $parser->push($token);
                throw new Exception("Expected identifier", Exception::PARSE_ERROR);
            }
            return $token;
        };
        
        // Consume only symbols
        $consumeSymbol = function() use ($parser) {
            $token = $parser->consume();
            if($parser->type != StringParser::CONSUMED_SYMBOLS) {
                $parser->push($token);
                throw new Exception("Expected symbol", Exception::PARSE_ERROR);
            }
            return $token;
        };
        
        $consumeQuoted = function() use ($parser) {
            $token = $parser->consume();
            if($parser->type != StringParser::CONSUMED_QUOTED) {
                $parser->push($token);
                throw new Exception("Expected quoted string", Exception::PARSE_ERROR);
            }
            return $token;
        };
        
        $consumeAttribute = function() use ($parser, $consumeWhitespace, $consumeWord, $consumeQuoted, $consumeSymbol) {
            $consumeWhitespace();
            try {
                $attributeName = $consumeWord();
            } catch (Exception $e) {
                return null;
            }
            try {
                $consumeWhitespace();
                $operator = $consumeSymbol();
                $consumeWhitespace();
            } catch (Exception $e) {
                return [$attributeName,true];
            }
            $value = $consumeQuoted();
            return [$attributeName, StringParser::unquote($value)];
        };
        
        // Consumes only complete tags
        $consumeTag = function() use ($parser, $consumeWhitespace, $consumeWord, $consumeAttribute, $consumeSymbol) {
            $tag = null;
            $token = $parser->consume();
            if($token == '<') {
                $consumeWhitespace();
                $tagName = $consumeWord();
                $tag = new Tag($tagName);
                while($attribute = $consumeAttribute()) {
                    // Get all attributes
                    $tag->addAttribute($attribute[0], $attribute[1]);
                }
                $consumeWhitespace();
                $symbol = $consumeSymbol();
                if(substr($symbol, 0, 1) == '>') {
                    $parser->push(substr($symbol, 1));
                    return $tag;
                } else if(substr($symbol, 0, 2) == '/>') {
                    $parser->push(substr($symbol, 2));
                    $tag->selfClosing = true;
                    return $tag;
                } else {
                    throw new Exception("Expected closing bracket for tag $tagName", Exception::PARSE_ERROR);
                }
            } else if($token == '</') {
                $tagName = '/'.$consumeWord();
                $consumeWhitespace();
                $symbol = $consumeSymbol();
                $rest = substr($symbol, 1);
                $parser->push($rest);
                if($symbol[0] != '>') {
                    $parser->push($symbol[0]);
                    throw new Exception("Expected closing bracket", Exception::PARSE_ERROR);
                }
                return new Tag($tagName);
            } else {
                throw new Exception("Expected tag", Exception::PARSE_ERROR);
            }
        };
        
        $consumeCData = function() use ($parser) {
            $buffer = '';
            while($token = $parser->consume()) {
                $p = strpos($token, '<');
                if($p !== false) {
                    $buffer .= substr($token, 0, $p);
                    $parser->push(substr($token, $p));
                    break;
                }
                $buffer .= $token;
            }
            if(strlen($buffer)>0)
                return new CData($buffer);
            throw new Exception("Expected CData", Exception::PARSE_ERROR);
        };
        
        $consumeAny = function() use ($consumeCData, $consumeTag) {
            try {
                return $consumeCData();
            } catch (Exception $e) {
            }
            try {
                return $consumeTag();
            } catch (Exception $e) {
            }
            throw new Exception("Expected something", Exception::PARSE_ERROR);
        };
        
        $root = new DomNode(new Tag('document'));
    
        $failSafe = 10000;
        while(true) {
            if($failSafe-- == 0) throw new Exception("Parser went bananas!");
            // Ignore whitespace
            try {
                $tag = $consumeAny();
                if(get_class($tag)=='Fubber\Html\CData') {
                    $root->append($tag);
                } else if(get_class($tag)=='Fubber\Html\Tag') {
                    $tagName = $tag->getName();
                    if($tagName[0]!='/') {
                        $domNode = new DomNode($tag);
                        $root->append($domNode);
                        $root = $domNode;
                    } else {
                        // We're closing a tag
                        $root = $root->getParent();
                    }
                }
            } catch (Exception $e) {
                break;
            }
        }
        return $root;
    }
    
    public function __construct(Tag $root, array $children=[]) {
        foreach($children as $k => $child)
            if(is_string($child))
                $children[$k] = new CData($child);
        $this->_root = $root;
        $this->_children = $children;
    }
    
    public function attr($name, $setTo=null) {
        return $this->getTag()->attr($name, $setTo);
    }
    
    public function replaceWith(DomNode $node) {
        $this->setTag($node->getTag());
        $this->setChildren($node->getChildren());
    }
    
    public function getName() {
        return $this->getTag()->getName();
    }
    
    public function getAttributes() {
        return $this->getTag()->getAttributes();
    }
    
    public function setChildren(array $children) {
        $this->_children = $children;
        return $this;
    }
    
    public function getChildren() {
        return $this->_children;
    }
    
    public function setTag(Tag $tag) {
        $this->_root = $tag;
        return $this;
    }
    
    public function getTag() {
        return $this->_root;
    }
    
    public function getParent() {
        return $this->_parent;
    }
    
    public function setParent(IDomElement $parent) {
        $this->_parent = $parent;
        return $this;
    }
    
    public function append(IDomElement $content) {
        $content->setParent($this);
        $this->_root->selfClosing = false;
        $this->_children[] = $content;
        return $this;
    }
    
    public function getClone() {
        $res = clone $this;
        foreach($res->_children as $k => $v)
            $res->_children[$k] = $v->getClone();
        return $res;
    }
    
    public function __get($name) {
        return $this->getTag()->attr($name);
    }
    
    public function __set($name, $value) {
        return $this->getTag()->attr($name, $value);
    }
    
    /**
     *  Build a new DOM by iterating recursively through each tag
     */
    public function map(callable $filter) {
        // We don't want to iterate any new children, if the filter inserts new children. That's why we
        // fetch children before applying the filter.
        $children = $this->getChildren();
        $filter($this);
        $count = 10;
        foreach($children as $child) {
            if($child instanceof self)
                $child->map($filter);
        }
    }
    
    public function __toString() {
        if(sizeof($this->_children)==0) {
            $this->_root->selfClosing = true;
            return $this->_root->__toString();
        } else {
            $this->_root->selfClosing = false;
            $res = $this->_root->__toString();
            foreach($this->_children as $child) {
                $res .= $child->__toString();
            }
            $res .= $this->_root->getClosing()->__toString();
            return $res;
        }
    }
}