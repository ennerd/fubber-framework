<?php
namespace Fubber\Html;

class Exception extends \Fubber\Exception {
    const PARSE_ERROR = 1;
}