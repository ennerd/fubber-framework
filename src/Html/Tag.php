<?php
namespace Fubber\Html;

class Tag {
    protected $_tag;
    protected $_attrs;
    public $selfClosing = false;
    
    public function __construct($tag, array $attrs = [], $selfClosing = false) {
        $this->_tag = $tag;
        $this->_attrs = $attrs;
        $this->selfClosing = $selfClosing;
    }
    
    public function getName() {
        return $this->_tag;
    }
    
    public function getAttributes() {
        return $this->_attrs;
    }

    public function attr($name, $setTo=null) {
        if($setTo!==null) {
            $this->_attrs[$name] = $setTo;
        }
        if(isset($this->_attrs[$name]))
            return $this->_attrs[$name];
        return null;
}
    
    public function addAttribute($name, $value) {
        $this->_attrs[$name] = $value;
        return $this;
    }
    
    public function removeAttribute($name) {
        unset($this->_attrs[$name]);
        return $this;
    }
    
    public function getClosing() {
        return new self('/'.$this->_tag);
    }
    
    public function __toString() {
        $res = '<'.$this->_tag;
        $pairs = [];
        foreach($this->_attrs as $key => $val) {
            $pairs[] = $key.'="'.htmlspecialchars($val).'"';
        }
        
        if(sizeof($pairs)>0)
            $res .= ' '.implode(" ", $pairs);
        
        $res .= ($this->selfClosing?' />':'>');
        
        return $res;
    }
}