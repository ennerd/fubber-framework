<?php
namespace Fubber\ImportMaps;

use Fubber\Util\Path;
use JsonSerializable;
use RuntimeException;

class ImportMapGenerator implements JsonSerializable {
    private string $httpRoot;
    private Path $appRoot;
    private array $packageJsonFiles = [];
    private array $processedPackages = [];
    private array $imports = [];

    public function __construct(string $httpRoot, Path|string $appRoot) {
        $this->httpRoot = rtrim($httpRoot, '/');
        $this->appRoot = Path::from($appRoot);
    }

    public function addPackageJson(string|Path ...$path): void {
        if (is_array($path)) {
            foreach ($path as $p) {
                $this->addPackageJson($p);
            }
            return;
        }
        $packageJsonPath = Path::from($path);
        
        // Convert to absolute path if relative
        if ($packageJsonPath->isRelative()) {
            $packageJsonPath = $packageJsonPath->absolute();
        }
        
        if (!$packageJsonPath->exists()) {
            throw new RuntimeException("Package.json not found at: {$packageJsonPath}");
        }
        
        $this->packageJsonFiles[] = $packageJsonPath;
    }

    private function processPackageJson(Path $packageJsonPath): void {
        $content = json_decode($packageJsonPath->getContents(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new RuntimeException("Invalid JSON in {$packageJsonPath}: " . json_last_error_msg());
        }

        // Find the nearest node_modules directory
        $nodeModules = $this->findNearestNodeModules($packageJsonPath->dirname());
        if ($nodeModules === null) {
            throw new RuntimeException("Could not find node_modules directory for {$packageJsonPath}");
        }

        // Process dependencies
        foreach (['dependencies', 'peerDependencies'] as $depType) {
            if (isset($content[$depType]) && is_array($content[$depType])) {
                foreach ($content[$depType] as $packageName => $version) {
                    $this->processPackage($packageName, $nodeModules);
                }
            }
        }
    }

    private function findNearestNodeModules(Path $startDir): ?Path {
        $current = $startDir;
        while (true) {
            $nodeModules = $current->navigate('node_modules');
            if ($nodeModules->exists()) {
                return $nodeModules;
            }
            
            // Stop if we've reached the root
            $parent = $current->dirname();
            if ($parent->__toString() === $current->__toString()) {
                return null;
            }
            $current = $parent;
        }
    }

    private function processPackage(string $packageName, Path $nodeModules): void {
        if (isset($this->processedPackages[$packageName])) {
            return;
        }

        $packagePath = $nodeModules->navigate($packageName);
        $packageJsonPath = $packagePath->navigate('package.json');

        if (!$packageJsonPath->exists()) {
            return;
        }

        $this->processedPackages[$packageName] = true;
        $packageJson = json_decode($packageJsonPath->getContents(), true);

        // Process package exports
        if (isset($packageJson['exports'])) {
            $this->processExports($packageName, $packageJson['exports'], $packagePath);
        } else {
            // Traditional entry point
            $entryPoint = $packageJson['module'] ?? $packageJson['main'] ?? null;
            if ($entryPoint) {
                $this->addImport($packageName, $packagePath->navigate($entryPoint));
            }
        }

        // Process submodules
        $this->processSubmodules($packageName, $packagePath);

        // Process nested dependencies
        if (isset($packageJson['dependencies'])) {
            foreach ($packageJson['dependencies'] as $depName => $version) {
                $this->processPackage($depName, $nodeModules);
            }
        }
    }

    private function processExports(string $packageName, $exports, Path $packagePath): void {
        if (is_string($exports)) {
            $this->addImport($packageName, $packagePath->navigate($exports));
            return;
        }

        if (is_array($exports)) {
            // Main export
            if (isset($exports['.'])) {
                $mainExport = $exports['.'];
                if (is_string($mainExport)) {
                    $this->addImport($packageName, $packagePath->navigate($mainExport));
                } elseif (is_array($mainExport)) {
                    $entryPoint = $mainExport['import'] ?? $mainExport['default'] ?? null;
                    if ($entryPoint) {
                        $this->addImport($packageName, $packagePath->navigate($entryPoint));
                    }
                }
            }

            // Subpath exports
            foreach ($exports as $key => $value) {
                if ($key === '.' || str_starts_with($key, './node_modules')) {
                    continue;
                }

                $subpath = ltrim($key, './');
                if (is_string($value)) {
                    $this->addImport(
                        "{$packageName}/{$subpath}", 
                        $packagePath->navigate($value)
                    );
                } elseif (is_array($value) && isset($value['import'])) {
                    $this->addImport(
                        "{$packageName}/{$subpath}", 
                        $packagePath->navigate($value['import'])
                    );
                }
            }
        }
    }

    private function processSubmodules(string $packageName, Path $packagePath): void {
        // We'll use PHP's RecursiveDirectoryIterator but filter appropriately
        $iterator = new \RecursiveDirectoryIterator(
            $packagePath->__toString(),
            \FilesystemIterator::SKIP_DOTS
        );
        $iterator = new \RecursiveIteratorIterator($iterator);

        foreach ($iterator as $file) {
            $path = Path::from($file->getPathname());
            
            // Skip non-JS files and test/spec files
            if (!preg_match('/\.(js|mjs)$/', $path->__toString()) ||
                preg_match('/(test|spec|mock|__tests__|dist\/cjs)/', $path->__toString())) {
                continue;
            }

            // Create the import path (e.g., "lodash/map")
            $relativePath = str_replace($packagePath->__toString() . DIRECTORY_SEPARATOR, '', $path->__toString());
            $importPath = preg_replace('/\.(js|mjs)$/', '', $relativePath);
            
            // Only add direct submodules
            if (substr_count($importPath, DIRECTORY_SEPARATOR) === 0) {
                $this->addImport("{$packageName}/{$importPath}", $path);
            }
        }
    }

    private function addImport(string $importName, Path $targetPath): void {
        // Convert the filesystem path to a URL path
        $urlPath = str_replace(
            $this->appRoot->__toString(),
            $this->httpRoot,
            $targetPath->__toString()
        );
        
        // Ensure forward slashes for URLs
        $urlPath = str_replace(DIRECTORY_SEPARATOR, '/', $urlPath);
        
        $this->imports[$importName] = $urlPath;
    }

    public function generate(): void {
        foreach ($this->packageJsonFiles as $packageJsonPath) {
            $this->processPackageJson($packageJsonPath);
        }
    }

    public function jsonSerialize(): mixed {
        if (empty($this->imports)) {
            $this->generate();
        }
        
        return [
            'imports' => $this->imports
        ];
    }
}