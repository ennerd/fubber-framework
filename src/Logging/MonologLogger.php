<?php
namespace Fubber\Logging;

use Psr\Log\LogLevel;

class MonologLogger extends \Monolog\Logger implements ILogger {

    public static function create(string $logDomain): ILogger {
        return new static($logDomain);
    }

    public function __construct(string $name, array $handlers = [], array $processors = []) {
        $handlers[] = new EventLogHandler();
        $processors[] = [ static::class, 'fubber_style_tags' ];
        parent::__construct($name, $handlers, $processors);
    }
    
    public static function fubber_style_tags(array $record) {
        /**
         * This allows Fubber framework style :tags when logging, in addition to
         * Monolog style {tags}.
         */
        preg_match_all("/:([^\s]+)/", $record['message'], $matches);
        $replaces = [];
        foreach($matches[1] as $k => $match) {
            $replaces[$matches[0][$k]] = '{'.$match.'}';
        }
        $record['message'] = strtr($record['message'], $replaces);
        return $record;
    }
}
