<?php
namespace Fubber\Logging;
use Monolog\Logger;

class EventLogHandler extends \Monolog\Handler\AbstractProcessingHandler {
    
    public function __construct($level=Logger::DEBUG, bool $bubble=true) {
        parent::__construct($level, $bubble);
    }
    
    protected function write(array $record): void {
        /**
         * Fubber framework tries to use :keyword instead of {keyword} in string templates - so
         * to maintain consistency, we'll do that here. This would normally be done by a processor. 
         * 
         * To maintain compatability with other processors from the Monolog universe, we'll
         * let monolog only see handlebar-style template strings.
         */
        preg_match_all("/{(.*?)}/", $record['message'], $matches);
        $replaces = [];
        foreach($matches[1] as $k => $match) {
            $replaces[$matches[0][$k]] = ':'.$match;
        }
        $record['message'] = strtr($record['message'], $replaces);

        $log = new EventLog();
        $log->message = substr($record['message'], 0, 255);
        $log->context = serialize($record['context']);
        $log->level = $record['level'];
        $log->channel = $record['channel'];
        $log->microtime = $record['datetime']->format('U.u');
        $log->extra = serialize($record['extra']);
        $log->save();
    }
}
