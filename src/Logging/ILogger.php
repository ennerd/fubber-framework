<?php
declare(strict_types=1);

namespace Fubber\Logging;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

interface ILogger extends LoggerInterface {
    /**
     * Create a logging instance. The `$logDomain` variable
     * is used to differentiate between various services.
     *
     * @param $logDomain Name of log
     * @return self
     */
    public static function create(string $logDomain): self;

    
}
