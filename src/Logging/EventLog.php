<?php
namespace Fubber\Logging;
use Fubber\Db\IDb;
use Fubber\Table\Attributes\Column;
use Fubber\Table\Attributes\TableBackend;
use Fubber\Table\Backends\DbTable;
use Fubber\Table\Table;

/**
*	Logs event from monolog
*/
#[TableBackend("ff_event_log")]
class EventLog extends DbTable {

    #[Column]
    public $id, $message, $level, $context, $channel, $microtime, $extra;
    
    public static function updateSchema(IDb $database) {
        $database->exec('CREATE TABLE IF NOT EXISTS '.static::$_table.' (
            id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
            message VARCHAR(255),
            level INT(11),
            context BLOB,
            channel VARCHAR(100),
            microtime DOUBLE,
            extra BLOB, INDEX(channel, microtime))');
    }
}
