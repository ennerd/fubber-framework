<?php
namespace Fubber\ML;

use ArrayAccess;
use Countable;
use Fubber\LogicException;
use Fubber\RuntimeException;
use IteratorAggregate;
use Traversable;

/**
 * Represents a mathematical vector and provides various vector operations.
 * 
 * This class implements Countable, ArrayAccess, and IteratorAggregate interfaces
 * for enhanced usability and integration with PHP's built-in functions.
 */
class Vector implements Countable, ArrayAccess, IteratorAggregate {
    /** @var int The number of elements in the vector */
    private int $length;

    /** @var float[] The array holding the vector's elements */
    private array $data;

    /**
     * Constructs a new Vector instance.
     *
     * @param int|array $init Either the length of the vector (to create a zero-filled vector)
     *                        or an array of initial values.
     */
    public function __construct(int|array $init) {
        if (is_array($init)) {
            $this->data = $init;
            $this->length = count($this->data);
        } else {
            $this->length = $init;
            $this->data = \array_fill(0, $init, 0);
        }
    }

    /**
     * Allows the vector to be used in a foreach loop.
     *
     * @return Traversable
     */
    public function getIterator(): Traversable {
        yield from $this->data;
    }

    /**
     * Returns the number of elements in the vector.
     *
     * @return int
     */
    public function count(): int {
        return count($this->data);
    }

    /**
     * Retrieves the value at the specified index.
     *
     * @param mixed $index The index of the element to retrieve.
     * @return float The value at the specified index.
     */
    public function offsetGet(mixed $index): float {
        return $this->data[intval($index)];
    }

    /**
     * Sets the value at the specified index.
     *
     * @param mixed $offset The index at which to set the value.
     * @param mixed $value The value to set.
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->data[intval($offset)] = $value;
    }

    /**
     * Throws an exception as vector elements cannot be unset.
     *
     * @param mixed $offset The index to unset.
     * @throws LogicException Always thrown as unsetting is not allowed.
     */
    public function offsetUnset(mixed $offset): void
    {
        throw new LogicException("Can't unset");
    }

    /**
     * Checks if an offset exists in the vector.
     *
     * @param mixed $offset The index to check.
     * @return bool True if the offset exists, false otherwise.
     */
    public function offsetExists(mixed $offset): bool
    {
        return !($offset < 0 || $offset >= $this->length);
    }

    /**
     * Appends another vector to this vector.
     *
     * @param Vector $other The vector to append.
     * @return Vector A new Vector instance with the appended elements.
     */
    public function append(Vector $other): Vector {
        return new Vector([...$this->data, ...$other->data]);
    }

    /**
     * Creates a new vector from a slice of this vector.
     *
     * @param int $offset The starting index of the slice.
     * @param int|null $length The length of the slice (or null to slice to the end).
     * @return Vector A new Vector instance containing the sliced elements.
     */
    public function slice(int $offset, ?int $length=null): Vector {
        return new Vector(\array_slice($this->data, $offset, $length));
    }

    /**
     * Adds this vector to another vector.
     *
     * @param Vector $other The vector to add.
     * @return Vector A new Vector instance representing the sum.
     * @throws RuntimeException If the vectors have different sizes.
     */
    public function add(Vector $other): Vector {
        $this->assertSameSize($other);

        $data = [];
        for ($i = 0; $i < $this->length; $i++) {
            $data[] = $this->data[$i] + $other->data[$i];
        }

        return new Vector($data);
    }

    /**
     * Subtracts another vector from this vector.
     *
     * @param Vector $other The vector to subtract.
     * @return Vector A new Vector instance representing the difference.
     * @throws RuntimeException If the vectors have different sizes.
     */
    public function subtract(Vector $other): Vector {
        $this->assertSameSize($other);

        $data = [];
        for ($i = 0; $i < $this->length; $i++) {
            $data[] = $this->data[$i] - $other->data[$i];
        }

        return new Vector($data);
    }

    /**
     * Calculates the dot product of this vector with another vector.
     *
     * @param Vector $other The vector to calculate the dot product with.
     * @return float The dot product.
     * @throws RuntimeException If the vectors have different sizes.
     */
    public function dotProduct(Vector $other): float {
        $this->assertSameSize($other);

        $result = 0;
        for ($i = 0; $i < $this->length; $i++) {
            $result += $this->data[$i] * $other->data[$i];
        }

        return $result;
    }

    /**
     * Multiplies this vector by a scalar value.
     *
     * @param float $scalar The scalar value to multiply by.
     * @return Vector A new Vector instance representing the result.
     */
    public function scalarMultiply(float $scalar): Vector {
        $result = [];
        for ($i = 0; $i < $this->length; $i++) {
            $result[] = $this->data[$i] * $scalar;
        }

        return new Vector($result);
    }

    /**
     * Calculates the magnitude (length) of this vector.
     *
     * @return float The magnitude of the vector.
     */
    public function magnitude(): float {
        return \sqrt($this->dotProduct($this));
    }

    /**
     * Normalizes this vector (creates a unit vector).
     *
     * @return Vector A new Vector instance representing the normalized vector.
     * @throws RuntimeException If the vector is a zero vector.
     */
    public function normalize(): Vector {
        $magnitude = $this->magnitude();
        if ($magnitude === 0) {
            throw new RuntimeException("Cannot normalize a zero vector");
        }

        return $this->scalarMultiply(1 / $magnitude);
    }

    /**
     * Returns a string representation of the vector.
     *
     * @return string The string representation of the vector.
     */
    public function __toString() {
        return '[' . implode(', ', $this->data) . ']';
    }

    /**
     * Provides debugging information for the vector.
     *
     * @return array The vector's data array.
     */
    public function __debugInfo()
    {
        return $this->data;
    }

    /**
     * Asserts that this vector and another vector have the same size.
     *
     * @param Vector $other The other vector to compare size with.
     * @throws RuntimeException If the vectors have different sizes.
     */
    private function assertSameSize(Vector $other): void {
        if ($other->length !== $this->length) {
            throw new RuntimeException("Vectors must have the same dimensions");
        }
    }
}