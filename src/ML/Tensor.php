<?php
namespace Fubber\ML;

use ArrayAccess;
use FFI;
use FFI\CData;
use FFI\ParserException;
use InvalidArgumentException;
use LogicException;
use RuntimeException;

/**
 * Implementation of Tensors for PHP. The implementation is focused on a tile
 * based memory layout for the tensors. 
 * 
 * @package Fubber\ML
 */
class Tensor implements ArrayAccess {

    /**
     * Number of bytes that are stored in a single cache line. This size is used
     * to determine how many float values are stored - which in turn determines
     * the size of each tile that a tensor is made of.
     */
    private const CACHE_LINE_SIZE = 64;

    public const FP16 = 2; // effectively mma32x32 operations
    public const FP32 = 4; // effectively mma16x16 operations
    public const FP64 = 8; // effectively mma8x8 operations

    /**
     * The shape as seen by developers working with this tensor
     * 
     * @var int[] 
     */
    private array $shape;

    /**
     * The spacing between each value in the memory layout
     * 
     * @var array
     */
    private array $stride;

    /**
     * If the underlying data structure is row major (C layout) or
     * column-major (F layout)
     * 
     * @var bool
     */
    private bool $isRowMajor;

    /**
     * The underlying memory
     * 
     * @var float[]
     */
    private CData $data;

    /**
     * The actual length of the $data array
     * 
     * @var int
     */
    private int $dataLength;

    /**
     * True if the underlying data is shared between Tensor instances.
     * 
     * @var bool
     */
    private bool $isShared = false;

    /**
     * The offset into the underlying $data array
     * 
     * @var int
     */
    private int $dataOffset = 0;

    private int $type;

    private bool $isMutable = false;

    /**
     * Constructs a new Tensor instance with padded block storage.
     *
     * @param array $shape The logical shape of the tensor
     * @param array|null $data Initial data (optional)
     */
    private function __construct() {
        // Use static::create* functions
    }

    public static function createEmpty(array $shape, int $type=self::FP32): Tensor {
        $t = new Tensor();
        $t->type = $type;
        $t->shape = $shape;
        $paddedShape = self::calculatePaddedShape(self::CACHE_LINE_SIZE / $type, $shape);
        $t->stride = self::calculateStride($paddedShape, true);
        $t->dataLength = array_product($paddedShape);
        $t->data = self::createTypedArray($type, $t->dataLength);
        $t->isRowMajor = true;
        return $t;
    }

    /**
     * Create a mutable tensor which enables writing to individual values
     * of the tensor without creating expensive copies for every modification.
     * 
     * @return Tensor 
     */
    public function variable(): Tensor {
        $t = $this->clone();
        $t->detach();
        $t->isMutable = true;
        return $t;    
    }

    /**
     * Checks if the offset exists in the first dimension of
     * the tensor.
     * 
     * @param int|array $offset 
     * @return bool 
     */
    public function offsetExists(mixed $offset): bool {
        if (is_int($offset)) {
            return !($offset < 0 || $offset >= $this->shape[0]);
        } elseif (is_array($offset)) {
            $offsetDimensionality = count($offset);
            if ($offsetDimensionality <= 0 || $offsetDimensionality > count($this->shape)) {
                return false;
            }
            foreach ($offset as $i => $o) {
                if ($o < 0 || $o >= $this->shape[$i]) {
                    return false;
                }
            }
            return true;
        } else {
            throw new LogicException("Offset must be specified as int or array of integers");
        }
    }

    /**
     * For tensor where count($shape) > 1, $value must be a Tensor
     * with a compatible shape to array_slice($shape, 1), otherwise
     * set the value.
     * 
     * @param mixed $offset The index (or array of indices) to set the value at.
     * @param mixed $value The value to set (either a scalar or another tensor).
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void {
        if (!$this->isMutable) {
            throw new LogicException("Tensor is not mutable");
        }

        // Handle single-dimension offset (int) or multi-dimension offset (array)
        $this->assertValidOffset($offset);

        if (is_int($offset)) {
            // Handle scalar or sub-tensor assignment for 1D tensors or the first dimension of an nD tensor
            if (count($this->shape) > 1) {
                if (!$value instanceof self) {
                    throw new \InvalidArgumentException("Value must be a Tensor for multi-dimensional assignment.");
                }

                // Check if the value tensor has compatible shape to slice of the current tensor
                if ($value->shape !== array_slice($this->shape, 1)) {
                    throw new \InvalidArgumentException("The value Tensor must have a compatible shape.");
                }

                // Calculate the offset in the original tensor's data
                $newOffset = $this->dataOffset + $offset * $this->stride[0];

                // Write the smaller tensor's data into this tensor's data array
                $this->writeTensorData($value, $newOffset);
            } else {
                // Calculate the flat index using stride and write the value
                $flatIndex = $this->dataOffset + $offset * $this->stride[0];
                $this->data[$flatIndex] = $value;
            }
        } elseif (is_array($offset)) {
            // Multi-dimensional offset handling
            $address = $this->dataOffset;
            foreach ($offset as $i => $o) {
                $address += $this->stride[$i] * $o;
            }

            if ($value instanceof self) {
                // Ensure the value tensor's shape matches the slice shape of the target tensor
                if ($value->shape !== array_slice($this->shape, count($offset))) {
                    throw new \InvalidArgumentException("The value Tensor must have a compatible shape.");
                }

                // Write the smaller tensor's data into the parent tensor
                $this->writeTensorData($value, $address);
            } else {
                // Scalar assignment for the exact location within the multi-dimensional tensor
                if (!is_numeric($value)) {
                    throw new \InvalidArgumentException("Scalar value must be numeric.");
                }

                $this->data[$address] = $value;
            }
        }
    }

    /**
     * For tensor where count($this->shape) > 1, return a new Tensor
     * with a lower dimensionality, using the same underlying 
     * data. Otherwise, return the value at the offset.
     * 
     * @param int|array $offset 
     * @return Tensor|float 
     */
    public function offsetGet(mixed $offset): Tensor|float {
        $this->assertValidOffset($offset);

        if (is_int($offset)) {
            if (count($this->shape) > 1) {
                $t = $this->clone();
                $t->shape = array_slice($this->shape, 1);
                $t->stride = array_slice($this->stride, 1);
                // Use the stride that accounts for padding
                $t->dataOffset = $this->dataOffset + $offset * $this->stride[0];
                return $t;
            } else {
                // For 1D access, calculate the flat index based on the stride
                $flatIndex = $this->dataOffset + $offset * $this->stride[0];
                return $this->data[$flatIndex];
            }
        } elseif (is_array($offset)) {
            // Multi-dimensional access
            $address = $this->dataOffset;
            foreach ($offset as $i => $o) {
                $address += $this->stride[$i] * $o;  // Adjust this based on the padded stride
            }
            if (count($offset) === count($this->shape)) {
                // If full-dimensional access, return the scalar
                return $this->data[$address];
            }
            // Otherwise, return a new sub-tensor
            $t = $this->clone();
            $t->shape = array_slice($this->shape, count($offset));
            $t->stride = array_slice($this->stride, count($offset));
            $t->dataOffset = $address;
            return $t;
        }
    }
    
    public function offsetUnset(mixed $offset): void {
        throw new LogicException("Can't unset offset in Tensor");;
    }

    /**
     * Writes the data of a smaller tensor into this tensor's data array at a specific offset.
     * 
     * @param Tensor $tensor The smaller tensor whose data will be copied.
     * @param int $offset The offset in the current tensor's data array where the writing begins.
     * @return void
     */
    private function writeTensorData(Tensor $tensor, int $offset): void {
        // Traverse the smaller tensor and copy its values into the correct place in the larger tensor
        $smallDataOffset = 0;
        foreach ($tensor->traverseValues($tensor->isRowMajor) as $value) {
            $this->data[$offset + $smallDataOffset++] = $value;
        }
    }

    /**
     * Transpose the tensor. Both the original and transposed tensor 
     * will share the same underlying data, but the logical access 
     * pattern will change.
     * 
     * @return Tensor A new Tensor with transposed shape and stride.
     */
    public function transpose(): Tensor {
        // Clone the tensor to create a new instance with the same underlying data
        $t = $this->clone();

        // Swap the shape to reflect the transposed dimensions
        $t->shape = array_reverse($this->shape);

        // Swap the strides to match the transposed shape
        $t->stride = array_reverse($this->stride);

        // Return the new tensor with transposed shape and stride
        return $t;
    }

    /**
     * Creates a new underlying data structure for the tensor and resets
     * the strides and dataOffset, optimizing for speed by directly 
     * copying data.
     * 
     * This function can be optimized in the future to allocate less data
     * and update the data offset and strides accordingly.
     */
    private function detach(): void {
        if (!$this->isShared) {
            return;
        }
        // Create a new tensor of the same shape as the current tensor
        $data = self::createTypedArray($this->type, $this->dataLength);
        FFI::memcpy($data, $this->data, FFI::sizeof($data));
        $this->data = $data;
        $this->isShared = false;
    }


    /**
     * Internal function for cloning the Tensor without copying
     * the underlying data (relying in PHP built in copy-on-write
     * mechanism for arrays)
     * 
     * @return Tensor 
     */
    public function clone(): Tensor {
        $this->isShared = true;
        $t = new Tensor();
        $t->data = $this->data;
        $t->dataLength = $this->dataLength;
        $t->dataOffset = $this->dataOffset;
        $t->isMutable = $this->isMutable;
        $t->isRowMajor = $this->isRowMajor;
        $t->isShared = true;
        $t->shape = $this->shape;
        $t->size = $this->size;
        $t->stride = $this->stride;
        $t->type = $this->type;
        return $t;
    }

    public function __clone() {
        throw new RuntimeException("Can't clone a tensor this way. Use the Tensor::clone() method.");
    }

    public function traverseValues(bool $rowMajor): \Generator {
        foreach ($this->traverseOffsets($rowMajor) as $offset) {
            yield $this->data[$offset];
        }
    }

    public function traverseOffsets(bool $rowMajor): \Generator {
        $dimCount = count($this->shape);  // Number of dimensions
        $currentIndex = array_fill(0, $dimCount, 0);  // Initialize the index for each dimension
        $flatIndex = $this->dataOffset;  // Start at the data offset
        
        // Determine the order of dimension traversal based on row-major or column-major
        $dimsOrder = $rowMajor ? range($dimCount - 1, 0, -1) : range(0, $dimCount - 1);
    
        while (true) {
            // Yield the current value, skipping padded regions
            yield $flatIndex;
    
            // Move to the next element in the specified order (row-major or column-major)
            for ($dim = 0; $dim < $dimCount; $dim++) {
                $dimIdx = $dimsOrder[$dim];  // Get the dimension index for the current step
    
                $currentIndex[$dimIdx]++;
                $flatIndex += $this->stride[$dimIdx];  // Move the flat index based on stride
    
                // Check if we've reached the end of the current dimension
                if ($currentIndex[$dimIdx] < $this->shape[$dimIdx]) {
                    break;  // Continue within the current dimension
                }
    
                // Reset the current dimension and move up to the next dimension
                if ($dim < $dimCount - 1) {
                    $flatIndex -= $currentIndex[$dimIdx] * $this->stride[$dimIdx];
                    $currentIndex[$dimIdx] = 0;  // Reset this dimension index and continue to the next one
                } else {
                    // If the first dimension overflows, we're done
                    return;
                }
            }
        }
    }

    private function assertValidOffset(mixed $offset): void {
        if (!$this->offsetExists($offset)) {
            throw new LogicException("Invalid offset " . \json_encode($offset) . " for Tensor with shape " . \json_encode($this->shape));
        }
    }

    /**
     * Calculates the stride of a tensor based on its shape and layout (row-major or column-major).
     * 
     * @param array $shape The shape of the tensor.
     * @param bool $isRowMajor Whether the layout is row-major or column-major.
     * @return array The stride for each dimension.
     */
    private static function calculateStride(array $shape, bool $isRowMajor): array {
        $stride = [];
        $n = count($shape);
        $currentStride = 1;

        if ($isRowMajor) {
            // Row-major: Strides decrease as dimensions increase
            for ($i = $n - 1; $i >= 0; $i--) {
                $stride[$i] = $currentStride;
                $currentStride *= $shape[$i];
            }
        } else {
            // Column-major: Strides increase as dimensions increase
            for ($i = 0; $i < $n; $i++) {
                $stride[$i] = $currentStride;
                $currentStride *= $shape[$i];
            }
        }

        return $stride;
    }

    /**
     * Matrix Multiplicate and Accumulate
     * @param float[] $a 
     * @param float[] $b 
     * @param &float[] $c 
     */
    private static function mma(
        int $type,
        CData $aData,
        CData $bData,
        CData $cData,
        int $aOffset,
        int $bOffset,
        int $cOffset,
        int $aStride = 1,
        int $bStride = 1,
        int $cStride = 1,
        int $maxLength = \PHP_INT_MAX
    ): void {
        $length = min($maxLength, self::CACHE_LINE_SIZE / $type);
        for ($i = 0; $i < $length; $i++) {
            $cData[$cOffset + $i * $cStride] += $aData[$aOffset + $i * $aStride] * $bData[$bOffset + $i * $bStride];
        }
    }
    
    /**
     * Calculate the padded shape for alignment purposes.
     * 
     * @param int $size The padding size.
     * @param array $shape The logical shape.
     * @return array The padded shape.
     */
    private static function calculatePaddedShape(int $size, array $shape): array {
        return array_map(fn($dim) => ceil($dim / $size) * $size, $shape);
    }

}
