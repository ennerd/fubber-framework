<?php
namespace Fubber\ML;

use ArrayAccess;
use Closure;
use Traversable;

interface MatrixInterface extends Traversable {
    /**
     * Basic matrix addition operation. When adding a matrix with a scalar,
     * the scalar is added to each element of the matrix.
     * 
     * Given matrix A and B: C[i,j] = A[i,j] + B[i,j]
     * Given matrix A and scalar s: C[i,j] = A[i,j] + s
     * 
     * Used in: Weight updates during training, bias addition in neural networks
     */
    public function add(MatrixInterface|float|int $other): MatrixInterface;

    /**
     * Matrix subtraction. When subtracting a scalar, the scalar is subtracted 
     * from each element of the matrix.
     * 
     * Given matrix A and B: C[i,j] = A[i,j] - B[i,j]
     * Given matrix A and scalar s: C[i,j] = A[i,j] - s
     * 
     * Used in: Computing errors/residuals, gradient descent updates
     */
    public function sub(MatrixInterface|float|int $other): MatrixInterface;

    /**
     * Element-wise multiplication (Hadamard product). When multiplying by a scalar,
     * each element is multiplied by that scalar.
     * 
     * Given matrix A and B: C[i,j] = A[i,j] * B[i,j]
     * Given matrix A and scalar s: C[i,j] = A[i,j] * s
     * 
     * Used in: Applying learning rates, element-wise gradient operations
     */
    public function mul(MatrixInterface|float|int $other): MatrixInterface;

    /**
     * Matrix multiplication (dot product). Key operation in neural networks.
     * For matrices A(m×n) and B(n×p), produces matrix C(m×p).
     * 
     * C[i,j] = sum(A[i,k] * B[k,j]) for k = 1 to n
     * 
     * Used in: Forward propagation (weights × inputs), backward propagation
     */
    public function matMul(MatrixInterface $other): MatrixInterface;

    /**
     * Transposes the matrix by flipping it over its diagonal.
     * Converts a matrix of shape (m×n) to shape (n×m).
     * 
     * A^T[i,j] = A[j,i]
     * 
     * Used in: Backpropagation when propagating gradients backwards
     */
    public function transpose(): MatrixInterface;

    /**
     * Applies a custom function to each element of the matrix.
     * The function receives the element value and its position (row, col).
     * 
     * Used in: Custom activation functions, data preprocessing
     */
    public function map(Closure $mapFunction): MatrixInterface;

    /**
     * Reduces the entire matrix to a single value using the provided function.
     * Function should take two arguments and return a single value.
     * 
     * Used in: Computing total loss, finding max/min values
     */
    public function reduce(Closure $reduceFunction): mixed;

    /**
     * Reduces each row to a single value, creating a column vector.
     * 
     * Used in: Computing row-wise statistics, feature aggregation
     */
    public function reduceRows(Closure $reduceFunction): MatrixInterface;

    /**
     * Reduces each column to a single value, creating a row vector.
     * 
     * Used in: Computing column-wise statistics, neuron outputs
     */
    public function reduceColumns(Closure $reduceFunction): MatrixInterface;

    /**
     * Computes e^x for each element x in the matrix.
     * Key component in softmax and sigmoid functions.
     * 
     * f(x) = e^x
     * 
     * Used in: Activation functions, probability computations
     */
    public function exp(): MatrixInterface;

    /**
     * Natural logarithm (ln) of each element.
     * Important in cross-entropy loss calculation.
     * 
     * f(x) = ln(x)
     * 
     * Used in: Cross-entropy loss, log-likelihood calculations
     */
    public function log(): MatrixInterface;

    /**
     * Base-10 logarithm of each element.
     * Useful for dealing with values of different scales.
     * 
     * f(x) = log_10(x)
     * 
     * Used in: Feature scaling, data preprocessing
     */
    public function log10(): MatrixInterface;

    /**
     * Raises each element to the specified power.
     * 
     * f(x) = x^power
     * 
     * Used in: Feature engineering, computing gradients
     */
    public function pow(float $power): MatrixInterface;

    /**
     * Square root of each element.
     * 
     * f(x) = √x
     * 
     * Used in: Computing standard deviations, L2 normalization
     */
    public function sqrt(): MatrixInterface;

    /**
     * Sigmoid activation function, maps any input to (0,1).
     * Creates an S-shaped curve, useful for binary classification.
     * 
     * f(x) = 1 / (1 + e^(-x))
     * 
     * Used in: Binary classification, gates in LSTM networks
     */
    public function sigmoid(): MatrixInterface;

    /**
     * Derivative of the sigmoid function.
     * Key component in backpropagation with sigmoid activation.
     * 
     * f'(x) = sigmoid(x) * (1 - sigmoid(x))
     * 
     * Used in: Computing gradients in neural networks
     */
    public function sigmoidPrime(): MatrixInterface;

    /**
     * Hyperbolic tangent activation function, maps inputs to (-1,1).
     * Similar to sigmoid but zero-centered.
     * 
     * f(x) = (e^x - e^(-x)) / (e^x + e^(-x))
     * 
     * Used in: Hidden layers of neural networks, especially in NLP
     */
    public function tanh(): MatrixInterface;

    /**
     * Derivative of the tanh function.
     * 
     * f'(x) = 1 - tanh^2(x)
     * 
     * Used in: Gradient computation for tanh layers
     */
    public function tanhPrime(): MatrixInterface;

    /**
     * Rectified Linear Unit (ReLU) activation.
     * Simple but effective, helps prevent vanishing gradients.
     * 
     * f(x) = max(0, x)
     * 
     * Used in: Most modern neural networks, especially deep ones
     */
    public function relu(): MatrixInterface;

    /**
     * Derivative of ReLU function.
     * Simple step function: 1 for positive inputs, 0 for negative.
     * 
     * f'(x) = 1 if x > 0 else 0
     * 
     * Used in: Backpropagation through ReLU layers
     */
    public function reluPrime(): MatrixInterface;

    /**
     * Computes the sum of all elements in the matrix.
     * 
     * sum = Σ(x[i,j]) for all i,j
     * 
     * Used in: Loss computation, normalization
     */
    public function sum(): float;

    /**
     * Computes the arithmetic mean of all elements.
     * 
     * mean = sum / (rows * cols)
     * 
     * Used in: Feature normalization, loss computation
     */
    public function mean(): float;

    /**
     * Computes the variance of all elements.
     * Measures spread of values around the mean.
     * 
     * var = Σ((x[i,j] - mean)^2) / (rows * cols)
     * 
     * Used in: Batch normalization, feature scaling
     */
    public function var(): float;

    /**
     * Computes the standard deviation (square root of variance).
     * 
     * std = √var
     * 
     * Used in: Normalization, weight initialization
     */
    public function std(): float;

    /**
     * Normalizes the matrix using z-score normalization.
     * Transforms data to have mean=0 and std=1.
     * 
     * z = (x - mean) / std
     * 
     * Used in: Feature preprocessing, batch normalization
     */
    public function normalize(): MatrixInterface;

    /**
     * Clips all values to lie within [min, max] range.
     * Helps prevent exploding values.
     * 
     * Used in: Gradient clipping, preventing numerical instability
     */
    public function clip(float $min, float $max): MatrixInterface;

    /**
     * Softmax activation function, converts values to probabilities.
     * Each element is transformed to range (0,1) and all elements sum to 1.
     * 
     * softmax(x[i]) = e^x[i] / Σ(e^x[j]) for all j
     * 
     * Used in: Multi-class classification output layers
     */
    public function softmax(): MatrixInterface;

    /**
     * Derivative of softmax function.
     * Complex due to dependency between outputs.
     * 
     * Used in: Backpropagation in classification networks
     */
    public function softmaxPrime(): MatrixInterface;

    /**
     * Cross-entropy loss between predicted and actual values.
     * Measures difference between two probability distributions.
     * 
     * CE = -Σ(actual[i] * log(predicted[i]))
     * 
     * Used in: Training classification models
     */
    public function crossEntropy(MatrixInterface $actual): float;

    /**
     * Derivative of cross-entropy loss.
     * Key for updating classification model weights.
     * 
     * Used in: Backpropagation in classification tasks
     */
    public function crossEntropyPrime(MatrixInterface $actual): MatrixInterface;

    /**
     * Mean Squared Error loss function.
     * Measures average squared difference between predictions and targets.
     * 
     * MSE = Σ((predicted - actual)^2) / n
     * 
     * Used in: Regression tasks, training autoencoders
     */
    public function mseLoss(MatrixInterface $actual): float;

    /**
     * Derivative of MSE loss.
     * 
     * MSE' = 2(predicted - actual) / n
     * 
     * Used in: Backpropagation for regression tasks
     */
    public function mseLossPrime(MatrixInterface $actual): MatrixInterface;

    /**
     * Mean Absolute Error loss function.
     * Less sensitive to outliers than MSE.
     * 
     * MAE = Σ|predicted - actual| / n
     * 
     * Used in: Robust regression tasks
     */
    public function maeLoss(MatrixInterface $actual): float;

    /**
     * Derivative of MAE loss.
     * 
     * MAE' = sign(predicted - actual) / n
     * 
     * Used in: Robust regression training
     */
    public function maeLossPrime(MatrixInterface $actual): MatrixInterface;

    /**
     * Huber loss: combines MSE and MAE.
     * MSE for small errors, MAE for large ones.
     * Delta controls the threshold between "small" and "large" errors.
     * 
     * Used in: Robust regression, less sensitive to outliers
     */
    public function huberLoss(MatrixInterface $actual, float $delta = 1.0): float;

    /**
     * Derivative of Huber loss.
     * Smooth transition between linear and quadratic regions.
     * 
     * Used in: Robust regression training
     */
    public function huberLossPrime(MatrixInterface $actual, float $delta = 1.0): MatrixInterface;

    /**
     * Clips gradient by its L2 norm.
     * Prevents exploding gradients in deep networks.
     * 
     * If ||gradient|| > maxNorm:
     *     gradient = (maxNorm * gradient) / ||gradient||
     * 
     * Used in: Training deep networks, RNNs
     */
    public function gradientClip(float $maxNorm): MatrixInterface;

    /**
     * Updates velocity in momentum-based optimization.
     * Helps overcome local minima and speeds up convergence.
     * 
     * v = momentum * v + gradient
     * 
     * Used in: SGD with momentum, Adam optimizer
     */
    public function momentumUpdate(MatrixInterface $velocity, float $momentum): MatrixInterface;

    /**
     * Updates cache in RMSprop optimization.
     * Adapts learning rates for each parameter.
     * 
     * cache = decay * cache + (1-decay) * gradient^2
     * 
     * Used in: RMSprop, Adam optimizer
     */
    public function rmspropUpdate(MatrixInterface $cache, float $decay): MatrixInterface;

    /**
     * Computes L1 regularization term and its gradient.
     * Promotes sparsity in the weights.
     * 
     * L1 = λ * Σ|w|
     * 
     * Used in: Feature selection, sparse models
     */
    public function l1Regularization(float $lambda): MatrixInterface;

    /**
     * Computes L2 regularization term and its gradient.
     * Prevents weights from becoming too large.
     * 
     * L2 = λ * Σ(w^2)
     * 
     * Used in: Preventing overfitting
     */
    public function l2Regularization(float $lambda): MatrixInterface;

    /**
     * Applies dropout regularization.
     * Randomly sets a fraction (rate) of inputs to zero during training.
     * 
     * Used in: Preventing overfitting, ensemble behavior
     */
    public function dropout(float $rate): MatrixInterface;

    /**
     * Element-wise "greater than" comparison.
     * Returns binary matrix of 1s and 0s.
     * 
     * Used in: Creating masks, thresholding
     */
    public function gt(float $value): MatrixInterface;

    /**
     * Element-wise "greater than or equal" comparison.
     * 
     * Used in: Creating masks, thresholding
     */
    public function gte(float $value): MatrixInterface;

    /**
     * Element-wise "less than" comparison.
     * 
     * Used in: Creating masks, thresholding
     */
    public function lt(float $value): MatrixInterface;

    /**
     * Element-wise "less than or equal" comparison.
     * 
     * Used in: Creating masks, thresholding
     */
    public function lte(float $value): MatrixInterface;

    /**
     * Element-wise equality comparison.
     * 
     * Used in: Creating masks, checking conditions
     */
    public function eq(float $value): MatrixInterface;

    /**
     * Computes absolute value of each element.
     * 
     * Used in: Computing L1 norms, error measures
     */
    public function abs(): MatrixInterface;

    /**
     * Returns sign (-1, 0, or 1) of each element.
     * 
     * Used in: Gradient operations, optimization
     */
    public function sign(): MatrixInterface;

    /**
     * Checks for NaN (Not a Number) values.
     * Important for debugging training issues.
     * 
     * Used in: Debugging, preventing invalid computations
     */
    public function isnan(): MatrixInterface;

    /**
     * Replaces NaN values with a specified value.
     * Helps recover from numerical instability.
     * 
     * Used in: Handling training instability, preprocessing data
     */
    public function fillNan(float $value): MatrixInterface;

    /**
     * Xavier/Glorot initialization for neural network weights.
     * Initializes weights from a uniform distribution with variance = 2/(fan_in + fan_out)
     * where fan_in is the number of input units and fan_out is the number of output units.
     * 
     * range = [-sqrt(6/(fan_in + fan_out)), sqrt(6/(fan_in + fan_out))]
     * 
     * Why it works:
     * - Maintains same variance of activations and gradients across layers
     * - Prevents vanishing/exploding gradients in deep networks
     * - Particularly good for tanh and sigmoid activations
     * 
     * Used in: Initializing weights in neural networks with tanh/sigmoid
     */
    public function initXavier(): MatrixInterface;

    /**
     * He initialization for neural network weights.
     * Similar to Xavier but scaled for ReLU activation functions.
     * Initializes weights from a normal distribution with:
     * variance = 2/fan_in
     * 
     * Why it works:
     * - Accounts for ReLU dropping half its inputs (setting them to zero)
     * - Maintains variance of activations through deep networks
     * - Prevents dead neurons in deep ReLU networks
     * 
     * Used in: Initializing weights in networks using ReLU activation
     */
    public function initHe(): MatrixInterface;

    /**
     * Initializes weights from a normal (Gaussian) distribution.
     * 
     * Properties:
     * - Bell-shaped curve centered at 'mean'
     * - About 68% of values within 1 std of mean
     * - About 95% of values within 2 std of mean
     * 
     * Parameters:
     * @param float $mean Center of the distribution
     * @param float $std Standard deviation controls spread
     * 
     * Used in: 
     * - General weight initialization
     * - Generating random noise for VAEs
     * - Creating synthetic datasets
     */
    public function initNormal(float $mean = 0.0, float $std = 1.0): MatrixInterface;

    /**
     * Initializes weights from a uniform distribution.
     * Every value in [min, max] has equal probability.
     * 
     * Properties:
     * - Rectangular probability distribution
     * - Mean = (max + min)/2
     * - Variance = (max - min)^2/12
     * 
     * Parameters:
     * @param float $min Lower bound of distribution
     * @param float $max Upper bound of distribution
     * 
     * Used in:
     * - Simple weight initialization
     * - Exploration in reinforcement learning
     * - When you need bounded random values
     */
    public function initUniform(float $min = -1.0, float $max = 1.0): MatrixInterface;

    /**
     * Returns the shape of the matrix as [rows, cols].
     * 
     * Important for:
     * - Verifying matrix dimensions before operations
     * - Debugging network architectures
     * - Computing fan-in/fan-out for initialization
     * 
     * Example:
     * - [784, 256] might be weights connecting input to hidden layer
     * - [256, 10] might be weights connecting hidden to output layer
     * 
     * Used in: Debugging, architecture verification, weight initialization
     */
    public function shape(): array;

    /**
     * Returns total number of elements in the matrix (rows * cols).
     * 
     * Applications:
     * - Computing average loss per element
     * - Determining model capacity/parameter count
     * - Memory requirement calculations
     * 
     * Example:
     * - A [784, 256] weight matrix has 200,704 parameters
     * - Useful for knowing model complexity
     * 
     * Used in: Model analysis, normalization, complexity assessment
     */
    public function size(): int;
}