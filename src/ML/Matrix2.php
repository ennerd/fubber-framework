<?php
namespace Fubber\ML;

use Closure;
use IteratorAggregate;
use SplFixedArray;
use Traversable;

class Matrix implements MatrixInterface, IteratorAggregate {

    private int $width;
    private int $height;
    private int $strideX;
    private int $strideY;
    private int $offset;
    private SplFixedArray $data;
    private int $refCount;

    private function __construct(int $width, int $height) {
        $this->width = $width;
        $this->height = $height;
        $this->offset = 0;
        $this->strideX = 1;
        $this->strideY = $this->width;
    }

    public function getIterator(): Traversable {
        $o = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        for ($x = 0; $x < $this->width; $x++) {
            for ($y = 0; $y < $this->height; $y++) {
                yield $this->data[$o + $x * $strideX + $y * $strideY];
            }
        }
    }

    public function __destruct() {
        --$this->refCount;
    }

    public function __clone() {
        ++$this->refCount;
    }

    private function detach(): void {
        if ($this->refCount > 1) {
            $this->data = SplFixedArray::fromArray($this->data->toArray());
            --$this->refCount;
            $refCount = 1;
            $this->refCount = $refCount;
        }
    }

    public static function create(int $width, int $height): Matrix {
        $m = new self($width, $height);
        $m->data = new SplFixedArray($m->size());
        $refCount = 1;
        $m->refCount = &$refCount; // Ensure when cloning that a reference is cloned
        return $m;
    }

    public function get(int $x, int $y): float {
        return $this->data[$this->offset + $x * $this->strideX + $y * $this->strideY];
    }

    public function set(int $x, int $y, float $value): void {
        $this->detach();
        $this->data[$this->offset + $x * $this->strideX + $y * $this->strideY] = $value;
    }

    public function transpose(): MatrixInterface
    {
        $m = clone $this;
        $m->strideX = $this->strideY;
        $m->strideY = $this->strideX;
        $m->width = $this->height;
        $m->height = $this->width;
        return $m;
    }

    public function size(): int
    {
        return $this->width * $this->height;
    }

    public function exp(): MatrixInterface {
        return $this->map(exp(...));
    }

    public function log(): MatrixInterface {
        return $this->map(fn($x) => log(max($x, PHP_FLOAT_MIN)));
    }

    public function log10(): MatrixInterface {
        return $this->map(fn($x) => log10(max($x, PHP_FLOAT_MIN)));
    }

    public function pow(float $power): MatrixInterface {
        return $this->map(fn($x) => pow($x, $power));
    }

    public function sqrt(): MatrixInterface {
        return $this->map(fn($x) => sqrt(max($x, 0.0)));
    }

    public function sigmoid(): MatrixInterface {
        return $this->map(fn($x) => 1 / (1 + exp(-$x)));
    }

    public function sigmoidPrime(): MatrixInterface {
        $s = $this->sigmoid();
        return $s->map(fn($x) => $x * (1 - $x));
    }

    public function tanh(): MatrixInterface {
        return $this->map(tanh(...));
    }

    public function tanhPrime(): MatrixInterface {
        $t = $this->tanh();
        return $t->map(fn($x) => 1 - $x * $x);
    }

    public function relu(): MatrixInterface {
        return $this->map(fn($x) => max(0, $x));
    }

    public function reluPrime(): MatrixInterface {
        return $this->map(fn($x) => $x > 0 ? 1.0 : 0.0);
    }

    public function abs(): MatrixInterface {
        return $this->map(abs(...));
    }

    public function sign(): MatrixInterface {
        return $this->map(fn($x) => $x > 0 ? 1.0 : ($x < 0 ? -1.0 : 0.0));
    }

    public function isnan(): MatrixInterface {
        return $this->map(fn($x) => is_nan($x) ? 1.0 : 0.0);
    }

    public function fillNan(float $value): MatrixInterface {
        return $this->map(fn($x) => is_nan($x) ? $value : $x);
    }

    public function clip(float $min, float $max): MatrixInterface {
        return $this->map(fn($x) => min(max($x, $min), $max));
    }

    public function normalize(): MatrixInterface {
        $mean = $this->mean();
        $std = $this->std();
        $std = $std > 0 ? $std : 1.0;  // Prevent division by zero
        return $this->sub($mean)->mul(1 / $std);
    }

    public function sum(): float {
        $data = $this->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        $sum = 0.0;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $sum += $data[$baseY + $x * $strideX];
            }
        }
        
        return $sum;
    }

    public function mean(): float {
        return $this->sum() / $this->size();
    }

    public function var(): float {
        $mean = $this->mean();
        $data = $this->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        $sum = 0.0;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $diff = $data[$baseY + $x * $strideX] - $mean;
                $sum += $diff * $diff;
            }
        }
        
        return $sum / $this->size();
    }

    public function std(): float {
        return sqrt($this->var());
    }

    public function shape(): array {
        return [$this->width, $this->height];
    }

    public function add(MatrixInterface|float|int $other): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        // Direct array access for scalar addition
        if (is_numeric($other)) {
            $data = $this->data;
            $resultData = $result->data;
            $offset = $this->offset;
            $strideX = $this->strideX;
            $strideY = $this->strideY;
            $other = (float)$other;
            
            for ($y = 0; $y < $this->height; $y++) {
                $baseY = $offset + $y * $strideY;
                for ($x = 0; $x < $this->width; $x++) {
                    $idx = $baseY + $x * $strideX;
                    $resultData[$idx] = $data[$idx] + $other;
                }
            }
            return $result;
        }

        // Matrix addition with stride handling
        if ($other instanceof Matrix) {
            if ($this->width !== $other->width || $this->height !== $other->height) {
                throw new \InvalidArgumentException("Matrix dimensions must match");
            }

            $data = $this->data;
            $otherData = $other->data;
            $resultData = $result->data;
            $offset = $this->offset;
            $otherOffset = $other->offset;
            $strideX = $this->strideX;
            $strideY = $this->strideY;
            $otherStrideX = $other->strideX;
            $otherStrideY = $other->strideY;

            for ($y = 0; $y < $this->height; $y++) {
                $baseY = $offset + $y * $strideY;
                $otherBaseY = $otherOffset + $y * $otherStrideY;
                for ($x = 0; $x < $this->width; $x++) {
                    $idx = $baseY + $x * $strideX;
                    $otherIdx = $otherBaseY + $x * $otherStrideX;
                    $resultData[$idx] = $data[$idx] + $otherData[$otherIdx];
                }
            }
            return $result;
        }

        throw new \InvalidArgumentException("Unsupported operand type");
    }

    public function mul(MatrixInterface|float|int $other): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        // Direct array access for scalar multiplication
        if (is_numeric($other)) {
            $data = $this->data;
            $resultData = $result->data;
            $offset = $this->offset;
            $strideX = $this->strideX;
            $strideY = $this->strideY;
            $other = (float)$other;
            
            for ($y = 0; $y < $this->height; $y++) {
                $baseY = $offset + $y * $strideY;
                for ($x = 0; $x < $this->width; $x++) {
                    $idx = $baseY + $x * $strideX;
                    $resultData[$idx] = $data[$idx] * $other;
                }
            }
            return $result;
        }

        // Element-wise matrix multiplication
        if ($other instanceof Matrix) {
            if ($this->width !== $other->width || $this->height !== $other->height) {
                throw new \InvalidArgumentException("Matrix dimensions must match");
            }

            $data = $this->data;
            $otherData = $other->data;
            $resultData = $result->data;
            $offset = $this->offset;
            $otherOffset = $other->offset;
            $strideX = $this->strideX;
            $strideY = $this->strideY;
            $otherStrideX = $other->strideX;
            $otherStrideY = $other->strideY;

            for ($y = 0; $y < $this->height; $y++) {
                $baseY = $offset + $y * $strideY;
                $otherBaseY = $otherOffset + $y * $otherStrideY;
                for ($x = 0; $x < $this->width; $x++) {
                    $idx = $baseY + $x * $strideX;
                    $otherIdx = $otherBaseY + $x * $otherStrideX;
                    $resultData[$idx] = $data[$idx] * $otherData[$otherIdx];
                }
            }
            return $result;
        }

        throw new \InvalidArgumentException("Unsupported operand type");
    }

    public function matMul(MatrixInterface $other): MatrixInterface {
        if (!$other instanceof Matrix) {
            throw new \InvalidArgumentException("Unsupported operand type");
        }
        if ($this->width !== $other->height) {
            throw new \InvalidArgumentException("Invalid dimensions for matrix multiplication");
        }

        $result = self::create($this->height, $other->width);
        
        $data = $this->data;
        $otherData = $other->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $otherOffset = $other->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        $otherStrideX = $other->strideX;
        $otherStrideY = $other->strideY;
        
        // Optimized matrix multiplication with stride handling
        for ($i = 0; $i < $this->height; $i++) {
            $baseI = $offset + $i * $strideY;
            for ($j = 0; $j < $other->width; $j++) {
                $sum = 0.0;
                $otherBaseJ = $otherOffset + $j * $otherStrideX;
                for ($k = 0; $k < $this->width; $k++) {
                    $sum += $data[$baseI + $k * $strideX] * 
                           $otherData[$otherBaseJ + $k * $otherStrideY];
                }
                $resultData[$i * $result->width + $j] = $sum;
            }
        }
        
        return $result;
    }

    public function sub(MatrixInterface|float|int $other): MatrixInterface {
        // Direct array access for scalar subtraction
        if (is_numeric($other)) {
            return $this->add(-$other);
            $data = $this->data;
            $resultData = $result->data;
            $offset = $this->offset;
            $strideX = $this->strideX;
            $strideY = $this->strideY;
            $other = (float)$other;
            
            for ($y = 0; $y < $this->height; $y++) {
                $baseY = $offset + $y * $strideY;
                for ($x = 0; $x < $this->width; $x++) {
                    $idx = $baseY + $x * $strideX;
                    $resultData[$idx] = $data[$idx] - $other;
                }
            }
            return $result;
        }

        if ($this->width !== $other->width || $this->height !== $other->height) {
            throw new \InvalidArgumentException("Matrix dimensions must match");
        }

        $result = clone $this;
        $result->detach();

        $data = $this->data;
        $otherData = $other->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $otherOffset = $other->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        $otherStrideX = $other->strideX;
        $otherStrideY = $other->strideY;

        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            $otherBaseY = $otherOffset + $y * $otherStrideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $otherIdx = $otherBaseY + $x * $otherStrideX;
                $resultData[$idx] = $data[$idx] - $otherData[$otherIdx];
            }
        }
        return $result;
    }

    public function gt(float $value): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        $data = $this->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $resultData[$idx] = $data[$idx] > $value ? 1.0 : 0.0;
            }
        }
        
        return $result;
    }

    public function gte(float $value): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        $data = $this->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $resultData[$idx] = $data[$idx] >= $value ? 1.0 : 0.0;
            }
        }
        
        return $result;
    }

    public function lt(float $value): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        $data = $this->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $resultData[$idx] = $data[$idx] < $value ? 1.0 : 0.0;
            }
        }
        
        return $result;
    }

    public function lte(float $value): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        $data = $this->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $resultData[$idx] = $data[$idx] <= $value ? 1.0 : 0.0;
            }
        }
        
        return $result;
    }

    public function eq(float $value): MatrixInterface {
        $result = clone $this;
        $result->detach();
        
        $data = $this->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;
        
        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $resultData[$idx] = abs($data[$idx] - $value) < 1e-10 ? 1.0 : 0.0;
            }
        }
        
        return $result;
    }
    
    public function map(Closure $mapFunction): MatrixInterface
    {
        $result = clone $this;
        $result->detach();
        
        $data = $this->data;
        $resultData = $result->data;
        $offset = $this->offset;
        $strideX = $this->strideX;
        $strideY = $this->strideY;

        for ($y = 0; $y < $this->height; $y++) {
            $baseY = $offset + $y * $strideY;
            for ($x = 0; $x < $this->width; $x++) {
                $idx = $baseY + $x * $strideX;
                $resultData[$idx] = $mapFunction($data[$idx], $x, $y);
            }
        }
        
        return $result;
    }
}