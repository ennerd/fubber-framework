<?php

namespace Fubber\ML;

use FFI;
use FFI\CData;
use InvalidArgumentException;

class Matrix {

    /**
     * Float16 is generally not supported, but included here for completeness
     */
    public const FLOAT16 = 2; // 2 bytes per element

    /**
     * Float32 using 4 bytes per element
     */
    public const FLOAT32 = 4; // 4 bytes per element

    /**
     * Float64 using 8 bytes per element
     */
    public const FLOAT64 = 8; // 8 bytes per element

    private readonly ?FFI $ffi = null;

    /**
     * Preinitialized matrixes with cells valued 1
     * per type.
     * 
     * @var array<int,CData<float[]>>
     */
    private static ?array $ones = null;

    /**
     * Preinitialized matrixes with cells valued -1
     * per type.
     * 
     * @var array<int,CData<float[]>>
     */
    private static ?array $neg_ones = null;

    /**
     * The amount of bitwise shift needed to get the width (or height) of each
     * square tile that the matrix is built from.
     * 
     * @var int
     */
    private int $tileShift;

    /**
     * A bitmap that corresponds with the width of the tiles. So for a tile width
     * of 4, the mask is 0b1111.
     * 
     * @var int
     */
    private int $tileMask;

    /**
     * The data type of the elements in the matrix.
     * 
     * @see Matrix::FLOAT*
     * @var int
     */
    private int $type;

    /**
     * The apparent width of the matrix without padding.
     * 
     * @var int
     */
    private int $width;

    /**
     * The apparent height of the matrix without padding.
     * 
     * @var int
     */
    private int $height;

    /**
     * The number of horizontal tiles in the matrix.
     * 
     * @var int
     */
    private int $horizontalTiles;

    /**
     * The number of vertical tiles in the matrix.
     * 
     * @var int
     */
    private int $verticalTiles;

    /**
     * The CData pointer to the underlying data structure.
     * 
     * @var CData
     */
    private CData $data;

    /**
     * The number of Matrix instances that share the same underlying
     * data structure.
     * 
     * @var int
     */
    private int $refCount = 1;

    /**
     * The element wise distance between each tile horizontally in the matrix.
     * 
     * @var int
     */
    private int $tileStrideX;

    /**
     * The element-wise distance between each tile vertically in the matrix.
     * 
     * @var int
     */
    private int $tileStrideY;

    /**
     * The underlying tiles in the matrix are stoled in row-major order. If this
     * is true, then accessing these elements will be done in col-major order.
     * 
     * @var bool
     */
    private bool $isTransposed = false;

    /**
     * Construct an empty matrix
     * 
     * @param int $width 
     * @param int $height 
     * @param int $type 
     * @return Matrix 
     * @throws InvalidArgumentException 
     */
    public static function create(int $width, int $height, int $type = self::FLOAT32): Matrix {
        $m = new self($width, $height, $type);
        $m->data = self::createTypedArray($type, $m->getPaddedWidth() * $m->getPaddedHeight());
        return $m;
    }

    /**
     * Create an identity matrix
     * 
     * @param int $size 
     * @param int $type 
     * @return Matrix 
     * @throws InvalidArgumentException 
     */
    public static function createIdentity(int $size, int $type = self::FLOAT32): Matrix {
        $m = self::create($size, $size, $type);        
        for ($i = 0; $i < $size; $i++) {
            $m->set($i, $i, 1.0);
        }
        return $m;
    }

    private function __construct(int $width, int $height, int $type = self::FLOAT32) {
        if (self::$ffi === null) {
            self::init();
        }

        switch ($type) {
            case self::FLOAT16:
            case self::FLOAT32:
                $this->tileShift = 2;
                break;
            case self::FLOAT64:
                $this->tileShift = 1;
                break;
        }
        $this->tileMask = (1 << $this->tileShift) - 1;
        $this->type = $type;
        $this->width = $width;
        $this->height = $height;
        $this->horizontalTiles = ($width + $this->tileMask) >> $this->tileShift; // Calculate number of tiles horizontally        
        $this->verticalTiles = ($height + $this->tileMask) >> $this->tileShift;
        $tileLength = 1 << $this->tileShift << $this->tileShift;
        $this->tileStrideX = $tileLength;
        $this->tileStrideY = $this->tileStrideX * $this->horizontalTiles;
    }

    public function __destruct() {
        if (--$this->refCount === 0) {
            FFI::free($this->data);
        }
    }

    public function fill(float $value): Matrix {
        $pw = $this->getPaddedWidth();
        $ph = $this->getPaddedHeight();
        $size = $this->getPaddedHeight() * $this->getPaddedHeight();
        if ($size < 128
    }

    public function getWidth(): int {
        return $this->width;
    }

    public function getHeight(): int {
        return $this->height;
    }

    public function clone(): Matrix {
        $m = clone $this;
        ++$this->refCount;
        $m->data = $this->data;
        $m->refCount = &$this->refCount;
        return $m;
    }

    public function get(int $x, int $y): float {
        // Calculate tile coordinates using bit shifts
        $tileX = $x >> $this->tileShift;  // Horizontal tile index
        $tileY = $y >> $this->tileShift;  // Vertical tile index
        
        // Calculate the tile-level offset in memory
        $tileOffset = $tileX * $this->tileStrideX + $tileY * $this->tileStrideY;
        
        // Calculate intra-tile element offsets using strideX and strideY
        $inTileX = $x & $this->tileMask;  // Intra-tile x position
        $inTileY = $y & $this->tileMask;  // Intra-tile y position
        
        // Final element offset within the tile
        if ($this->isTransposed) {
            $elementOffset = $inTileY + ($inTileX << $this->tileShift);
        } else {
            $elementOffset = $inTileX + ($inTileY << $this->tileShift);
        }
        
        // Return the value from the calculated offset
        return $this->data[$tileOffset + $elementOffset];
    }
        
    public function set(int $x, int $y, float $value): void {
        // Ensure copy-on-write semantics for data
        $this->detach();
        
        // Calculate tile coordinates using bit shifts
        $tileX = $x >> $this->tileShift;  // Horizontal tile index
        $tileY = $y >> $this->tileShift;  // Vertical tile index
        
        // Calculate the tile-level offset in memory
        $tileOffset = $tileX * $this->tileStrideX + $tileY * $this->tileStrideY;
        
        // Calculate intra-tile element offsets using strideX and strideY
        $inTileX = $x & $this->tileMask;  // Intra-tile x position
        $inTileY = $y & $this->tileMask;  // Intra-tile y position
        
        // Final element offset within the tile
        if ($this->isTransposed) {
            $elementOffset = $inTileY + ($inTileX << $this->tileShift);
        } else {
            $elementOffset = $inTileX + ($inTileY << $this->tileShift);
        }
        
        // Set the value at the calculated offset
        $this->data[$tileOffset + $elementOffset] = $value;
    }
    
    public function exp(): Matrix {
        $this->detach();
        TODO: apply exp
    }

    public function transpose(): Matrix {
        $other = $this->clone();

        // Swap the width and height, since the transpose will invert them
        [$other->width, $other->height] = [$other->height, $other->width];
        
        // Toggle the transposition flag
        $other->isTransposed = !$other->isTransposed;
        
        // Swap the tile strides to reflect transposed memory access
        [$other->tileStrideX, $other->tileStrideY] = [$other->tileStrideY, $other->tileStrideX];
        
        return $other;
    }
    
    public function add(Matrix|float $other): Matrix {
        if (is_float($other)) {
            return $this->addScalar($other);
        }
        self::matrix_assert_same_shape($this, $other);
        [$self, $result] = self::matrix_type_unify($this, $other);

        // We will be writing into $result
        $result->detach();

        if ($self->isTransposed === $result->isTransposed) {
            // both are transposed, so tiles can be treated the same way
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                $selfOffsetBase = $i * $self->tileStrideX;
                $resultOffsetBase = $i * $other->tileStrideX;
                for ($j = 0; $j < $self->verticalTiles; $j++) {
                    $selfOffset = $selfOffsetBase + $self->tileStrideX * $j;
                    $resultOffset = $resultOffsetBase + $result->tileStrideX * $j;
                    self::tile_add($self->type, $self->tileShift, $self->data, $result->data, $selfOffset, $resultOffset);
                }
            }                
        } else {
            $temp = self::createTypedArray($self->type, 1 << $self->tileShift << $self->tileShift, 0);
            // one is transposed, make both transposed or neither transposed
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                $selfOffsetBase = $i * $self->tileStrideX;
                $resultOffsetBase = $i * $other->tileStrideX;
                for ($j = 0; $j < $self->verticalTiles; $j++) {
                    $selfOffset = $selfOffsetBase + $self->tileStrideX * $j;
                    $resultOffset = $resultOffsetBase + $result->tileStrideX * $j;
                    self::tile_transpose($self->type, $self->tileShift, $self->data, $selfOffset, $temp, 0);
                    self::tile_add($self->type, $self->tileShift, $temp, $result->data, 0, $resultOffset);
                }
            }                
        }

        return $result;
    }

    public function addScalar(float $scalar): Matrix {
        $result = $this->clone();
        $result->detach();
        for ($i = 0; $i < $this->horizontalTiles; $i++) {
            for ($j = 0; $j < $this->verticalTiles; $j++) {
                $offset = $this->getTileOffset($i, $j);
                self::tile_add_scalar($this->type, $this->tileShift, $result->data, $offset, $scalar);
            }
        }
        return $result;
    }

    public function sub(Matrix|float $other): Matrix {
        if (is_float($other)) {
            return $this->addScalar(-$other);
        }
        self::matrix_assert_same_shape($this, $other);
        [$self, $result] = self::matrix_type_unify($this, $other);

        // We will be writing into $result
        $result->detach();

        if ($self->isTransposed === $result->isTransposed) {
            // both are transposed, so tiles can be treated the same way
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                $selfOffsetBase = $i * $self->tileStrideX;
                $resultOffsetBase = $i * $other->tileStrideX;
                for ($j = 0; $j < $self->verticalTiles; $j++) {
                    $selfOffset = $selfOffsetBase + $self->tileStrideX * $j;
                    $resultOffset = $resultOffsetBase + $result->tileStrideX * $j;
                    self::tile_sub($self->type, $self->tileShift, $self->data, $result->data, $selfOffset, $resultOffset);
                }
            }                
        } else {
            $temp = self::createTypedArray($self->type, 1 << $self->tileShift << $self->tileShift, 0);
            // one is transposed, make both transposed or neither transposed
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                $selfOffsetBase = $i * $self->tileStrideX;
                $resultOffsetBase = $i * $other->tileStrideX;
                for ($j = 0; $j < $self->verticalTiles; $j++) {
                    $selfOffset = $selfOffsetBase + $self->tileStrideX * $j;
                    $resultOffset = $resultOffsetBase + $result->tileStrideX * $j;
                    self::tile_transpose($self->type, $self->tileShift, $self->data, $selfOffset, $temp, 0);
                    self::tile_sub($self->type, $self->tileShift, $temp, $result->data, 0, $resultOffset);
                }
            }                
        }

        return $result;
    }    

    public function matMul(Matrix $other): Matrix {
        // Ensure matrices can be multiplied
        if ($this->width !== $other->height) {
            throw new InvalidArgumentException("Matrix dimensions are incompatible for multiplication");
        }
    
        [$self, $other] = self::matrix_type_unify($this, $other);
        $result = self::create($other->width, $self->height, $self->type);
    
        if ($self->isTransposed === $other->isTransposed) {
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                for ($j = 0; $j < $other->verticalTiles; $j++) {
                    $resultOffset = $i * $result->tileStrideX + $j * $result->tileStrideY;
        
                    for ($k = 0; $k < $self->verticalTiles; $k++) {
                        $aOffset = $i * $self->tileStrideX + $k * $self->tileStrideY;
                        $bOffset = $k * $other->tileStrideX + $j * $other->tileStrideY;

                        self::tile_matmul($self->type, $self->tileShift, $self->data, $other->data, $result->data, $aOffset, $bOffset, $resultOffset);
                    }
                }
            }    
        } else{
            $temp = self::createTypedArray($self->type, 1 << $self->tileShift << $self->tileShift, 0);
            // one is transposed, make both transposed or neither transposed
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                for ($j = 0; $j < $other->verticalTiles; $j++) {
                    $resultOffset = $i * $result->tileStrideX + $j * $result->tileStrideY;
        
                    for ($k = 0; $k < $self->verticalTiles; $k++) {
                        $aOffset = $i * $self->tileStrideX + $k * $self->tileStrideY;
                        $bOffset = $k * $other->tileStrideX + $j * $other->tileStrideY;
        
                        self::tile_transpose($self->type, $self->tileShift, $self->data, $aOffset, $temp, 0);
                        self::tile_matmul($self->type, $self->tileShift, $temp, $other->data, $result->data, 0, $bOffset, $resultOffset);
                    }
                }
            }    
        }
        
        return $result;
    }
    
    /**
     * Hadamard product or simple element wise multiplication
     * 
     * @param Matrix|float $other 
     * @return Matrix 
     * @throws InvalidArgumentException 
     */
    public function multiply(Matrix|float $other): Matrix {
        if (is_float($other)) {
            return $this->multiplyScalar($other);
        }
        self::matrix_assert_same_shape($this, $other);
        [$self, $result] = self::matrix_type_unify($this, $other);

        // We will be writing into $result
        $result->detach();

        if ($self->isTransposed === $result->isTransposed) {
            // both are transposed, so tiles can be treated the same way
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                $selfOffsetBase = $i * $self->tileStrideX;
                $resultOffsetBase = $i * $other->tileStrideX;
                for ($j = 0; $j < $self->verticalTiles; $j++) {
                    $selfOffset = $selfOffsetBase + $self->tileStrideX * $j;
                    $resultOffset = $resultOffsetBase + $result->tileStrideX * $j;
                    self::tile_multiply($self->type, $self->tileShift, $self->data, $result->data, $selfOffset, $resultOffset);
                }
            }                
        } else {
            $temp = self::createTypedArray($self->type, 1 << $self->tileShift << $self->tileShift, 0);
            // one is transposed, make both transposed or neither transposed
            for ($i = 0; $i < $self->horizontalTiles; $i++) {
                $selfOffsetBase = $i * $self->tileStrideX;
                $resultOffsetBase = $i * $other->tileStrideX;
                for ($j = 0; $j < $self->verticalTiles; $j++) {
                    $selfOffset = $selfOffsetBase + $self->tileStrideX * $j;
                    $resultOffset = $resultOffsetBase + $result->tileStrideX * $j;
                    self::tile_transpose($self->type, $self->tileShift, $self->data, $selfOffset, $temp, 0);
                    self::tile_multiply($self->type, $self->tileShift, $temp, $result->data, 0, $resultOffset);
                }
            }                
        }

        return $result;
    }

    private function multiplyScalar(float $scalar): Matrix {
        $result = $this->clone();
        $result->detach();
        for ($i = 0; $i < $this->horizontalTiles; $i++) {
            for ($j = 0; $j < $this->verticalTiles; $j++) {
                $offset = $this->getTileOffset($i, $j);
                self::tile_multiply_scalar($this->type, $this->tileShift, $result->data, $offset, $scalar);
            }
        }
        return $result;
    }    

    /**
     * Hadamard Product with accumulation
     * 
     * @param Matrix $other 
     * @param Matrix $result 
     * @throws InvalidArgumentException 
     */
    public function dotProductWithAccumulation(Matrix $other, Matrix $result): void {
        // Ensure matrices can be multiplied
        if ($this->width !== $other->height) {
            throw new InvalidArgumentException("Matrix dimensions are incompatible for multiplication");
        }

        // Ensure result is the correct size
        if ($result->width !== $other->width || $result->height !== $this->height) {
            throw new InvalidArgumentException("Result matrix has incorrect size");
        }

        $self = $this->ensureType($result->type);
        $other = $other->ensureType($result->type);
        
        // Loop over the tiles of the result matrix
        for ($i = 0; $i < $self->horizontalTiles; $i++) {
            for ($j = 0; $j < $other->verticalTiles; $j++) {
                // Calculate the tile offset in the result matrix
                $resultOffset = $result->getTileOffset($i, $j);
    
                // Multiply the tiles from matrix $a and matrix $b and accumulate in $result
                for ($k = 0; $k < $self->verticalTiles; $k++) {
                    $aOffset = $self->getTileOffset($i, $k);
                    $bOffset = $other->getTileOffset($k, $j);
    
                    // Perform the tile matrix multiply-accumulate operation (mma)
                    self::tile_matmul($self->type, $self->tileShift, $self->data, $other->data, $result->data, $aOffset, $bOffset, $resultOffset);
                }
            }
        }
    }
    
    /**
     * Returns the offset of the tile represented by (tileX, tileY)
     * coordinates. These are the underlying data tile coordinates,
     * so coordinate [1, 0] will return the same tile regardless of
     * the isTransposed property.
     * 
     * @param int $tileX 
     * @param int $tileY 
     * @return int 
     */
    private function getTileOffset(int $tileX, int $tileY): int {
        // Offset = (tileX + tileY * numHorizontalTiles) * elementsPerTile
        return (($tileY * $this->horizontalTiles) + $tileX) << ($this->tileShift << 1);
    }
    
    /**
     * Ensure the Matrix is backed by an independent copy of the data which
     * will be identical in size to the original data. Often it might be
     * preferable to use compact() instead of detach().
     * 
     * @throws InvalidArgumentException 
     */
    private function detach(): void {
        if ($this->refCount === 1) {
            // already detached
            return;
        }
        --$this->refCount;
        $this->refCount = 1;
        $oldData = $this->data;        
        $this->data = self::createTypedArray($this->type, $this->getPaddedHeight() * $this->getPaddedWidth());
        FFI::memcpy($this->data, $oldData, FFI::sizeof($this->data));
    }    

    /**
     * Ensure the matrix has a particular precision, without duplicating
     * the data in memory if possible.
     * 
     * @param int $type 
     * @return Matrix 
     */
    private function ensureType(int $type): Matrix {
        // If the matrix already has the required precision, return the current matrix
        if ($type === $this->type) {
            return $this;
        }
        
        $other = Matrix::create($this->height, $this->width, $type);

        // Tiles are sized according to cache line sizes on purpose, so we should
        // attempt to leverage this to modify the precision of a matrix.

        if ($this->tileShift === $other->tileShift) {
            /**
             * Tiles are the same size.
             */
            $length = $this->getPaddedWidth() * $this->getPaddedHeight();
            for ($o = 0; $o < $length; $o++) {
                // implicit type conversion
                $other->data[$o] = $this->data[$o];
            }
        } else {
            /**
             * Approach tries to write one tile at a time, which indirectly causes it
             * to also read from as few tiles in the source as possible - which should
             * be efficient with regards to cache lines.
             */
            $otherTileCount = 1 << $other->tileShift << $other->tileShift; // Number of elements per tile
            $otherMask = $other->tileMask;
            for ($i = 0; $i < $other->horizontalTiles; $i++) {
                for ($j = 0; $j < $other->verticalTiles; $j++) {
                    $tileOffset = $other->getTileOffset($i, $j);  // Get the tile's starting offset
                    $tx = $i << $other->tileShift;  // X-coordinate of the top-left of the tile
                    $ty = $j << $other->tileShift;  // Y-coordinate of the top-left of the tile
    
                    // Loop over each element within the tile
                    for ($o = 0; $o < $otherTileCount; $o++) {
                        $x = $o & $otherMask;  // Intra-tile x position
                        $y = $o >> $other->tileShift;  // Intra-tile y position
                        
                        // Copy the data from the source matrix to the new matrix, respecting tiles
                        $other->data[$tileOffset + $o] = $this->get($tx + $x, $ty + $y);
                    }
                }
            }
        }
   
        // Return the new matrix with the desired precision
        return $other;
    }
    
    private function getPaddedWidth(): int {
        return ($this->width + $this->tileMask) & ~$this->tileMask;
    }

    private function getPaddedHeight(): int {
        return ($this->height + $this->tileMask) & ~$this->tileMask;
    }


    /**
     * Check that both matrixes have the same shape
     * 
     * @param Matrix $a 
     * @param Matrix $b 
     * @throws InvalidArgumentException 
     */
    private static function matrix_assert_same_shape(Matrix $a, Matrix $b): void {
        if ($a->width !== $b->width) {
            throw new InvalidArgumentException("Matrixes have different width");
        }
        if ($a->height !== $b->height) {
            throw new InvalidArgumentException("Matrixes have different height");
        }
    }

    /**
     * Type cast to the highest precision and clone.
     * 
     * @param Matrix $a 
     * @param Matrix $b 
     * @return array{0: Matrix, 1: Matrix}
     * @throws InvalidArgumentException 
     */
    private static function matrix_type_unify(Matrix $a, Matrix $b): array {
        if ($a->type > $b->type) {
            $b = $b->ensureType($a->type);
            $a = $a->clone();
        } elseif ($a->type < $b->type) {
            $a = $a->ensureType($b->type);
            $b = $b->clone();
        }
        return [$a, $b];
    }

    /**
     * Perform dot product accumulation, the same operation as accelerated by NVIDIA
     * Tensor Cores. This function is intended to be accelerated using a PECL extension.
     * 
     * @param int $type 
     * @param int $tileShift
     * @param CData $a 
     * @param CData $b 
     * @param CData $c 
     * @param int $aOffset 
     * @param int $bOffset 
     * @param int $cOffset 
     */
    private static function tile_matmul(int $type, int $tileShift, CData $a, CData $b, CData $c, int $aOffset, int $bOffset, int $cOffset): void {
        // $type is ignored currently, but a required argument if we want to use vectorized instructions
        $tileSize = 1 << $tileShift;
        
        for ($i = 0; $i < $tileSize; $i++) {
            for ($j = 0; $j < $tileSize; $j++) {
                $sum = 0;
                for ($k = 0; $k < $tileSize; $k++) {
                    $sum += $a[$aOffset + ($i << $tileShift) + $k] * $b[$bOffset + ($k << $tileShift) + $j];
                }
                $c[$cOffset + ($i << $tileShift) + $j] += $sum;
            }
        }
    }

    /**
     * Perform element-wise multiplication and accumulation. This function is intended to be accelerated
     * using a PECL extension.
     * 
     * @param int $type 
     * @param int $tileShift
     * @param CData $a 
     * @param CData $b 
     * @param CData $c 
     * @param int $aOffset 
     * @param int $bOffset 
     * @param int $cOffset 
     */
    private static function tile_multiply(int $type, int $tileShift, CData $a, CData $b, int $aOffset, int $bOffset): void {
        // $type is ignored currently, but a required argument if we want to use vectorized instructions
        $tileLength = 1 << $tileShift << $tileShift;
        for ($i = 0; $i < $tileLength; $i++, $aOffset++, $bOffset++) {
            $b[$bOffset] = $b[$bOffset] * $a[$aOffset];
        }
    }

    private static function tile_multiply_scalar(int $type, int $tileShift, CData $data, int $offset, float $value): void {
        $tileLength = 1 << $tileShift << $tileShift;
        for ($i = 0; $i < $tileLength; $i++, $offset++) {
            $data[$offset] = $data[$offset] * $value;
        }
    }

    private static function tile_add(int $type, int $tileShift, CData $a, CData $b, int $aOffset, int $bOffset): void {
        $tileLength = 1 << $tileShift << $tileShift;
        for ($i = 0; $i < $tileLength; $i++, $aOffset++, $bOffset++) {
            $b[$bOffset] = $b[$bOffset] + $a[$aOffset];
        }
    }    

    private static function tile_add_scalar(int $type, int $tileShift, CData $data, int $offset, float $value): void {
        $tileLength = 1 << $tileShift << $tileShift;
        for ($i = 0; $i < $tileLength; $i++, $offset++) {
            $data[$offset] = $data[$offset] + $value;
        }
    }

    private static function tile_sub(int $type, int $tileShift, CData $a, CData $b, int $aOffset, int $bOffset): void {
        $tileLength = 1 << $tileShift << $tileShift;
        for ($i = 0; $i < $tileLength; $i++, $aOffset++, $bOffset++) {
            $b[$bOffset] = $b[$bOffset] - $a[$aOffset];
        }
    }


    /**
     * Transpose the values of a specific tile into a target matrix. This function is intended to 
     * eventually be accelerated using a native PECL extension.
     * 
     * @param int $type 
     * @param int $tileShift 
     * @param CData $a 
     * @param int $aOffset 
     * @param CData $b 
     * @param int $bOffset 
     */
    private static function tile_transpose(int $type, int $tileShift, CData $a, int $aOffset, CData $b, int $bOffset): void {
        $tileSize = 1 << $tileShift;  // Calculate the actual tile size using a bit shift
    
        for ($i = 0; $i < $tileSize; $i++) {
            for ($j = 0; $j < $tileSize; $j++) {
                $b[$bOffset + ($j << $tileShift) + $i] = $a[$aOffset + ($i << $tileShift) + $j];
            }
        }
    }

    
    /**
     * Create a typed array using FFI
     * 
     * @param int $type 
     * @param int $size 
     * @return CData 
     * @throws InvalidArgumentException 
     */
    private static function createTypedArray(int $type, int $size, float $value=0): CData {
        switch ($type) {
            case self::FLOAT16: return FFI::new("float16[$size]");
            case self::FLOAT32: return FFI::new("float32[$size]");
            case self::FLOAT64: return FFI::new("double[$size]");
        }
        throw new InvalidArgumentException("Unsupported type");
    }

    private static function init(): void {
        self::$ffi = FFI::cdef();
        
        if (self::$ones === null) {
            self::$ones = [];
            self::$neg_ones = [];
            foreach ([self::FLOAT16 => 16, self::FLOAT32 => 16, self::FLOAT64 => 4] as $type => $size) {
                self::$ones[$type] = self::createTypedArray($type, $size, 1);
                self::$neg_ones[$type] = self::createTypedArray($type, $size, -1);
            }
        }
    }

}
