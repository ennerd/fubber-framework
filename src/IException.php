<?php
namespace Fubber;

use Fubber\I18n\Translatable;
use Throwable;

interface IException extends Throwable {

    /**
     * Returns a description of the exception for developers
     * 
     * @return string 
     */
    public function getExceptionDescription(): string;
    
    /**
     * Returns the best HTTP status code if this exception is
     * unhandled. Default is 500.
     * 
     * @return int 
     */
    public function getStatusCode(): int;
    
    /**
     * Returns the best HTTP reason phrase if this exception is
     * unhandled. Default is 'Internal Server Error'.
     * 
     * @return string 
     */
    public function getReasonPhrase(): string;

    /**
     * Returns a helpful suggestion for developers to assist them
     * in resolving the error if possible.
     * 
     * @return null|string 
     */
    public function getSuggestion(): ?string;
    
}