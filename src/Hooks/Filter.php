<?php
namespace Fubber\Hooks;

use Closure;
use Throwable;

/**
 * A Filter is a chain of listeners receiving the source value and then
 * returning a modified version of the value until all filters have done
 * their transformations on the source value.
 * 
 * All listeners MUST return the value, event if they don't change anything.
 * 
 * @package Fubber\Hooks
 */
class Filter extends Dispatcher {
    protected array $listeners = [];

    /**
     * Filter a value using the registered callbacks.
     * 
     * @param mixed $value The value to filter
     * @param mixed ...$args Any extra arguments to pass to listeners
     * @return mixed 
     */
    public function filter(mixed $value, mixed ...$args): mixed {
        try {
            foreach ($this->listeners as $listener) {
                try {
                    $value = $listener($value, ...$args);
                } catch (Throwable $e) {
                    self::handleException($e, $listener, $this);
                }
            }
            return $value;
        } finally {
            self::runEvents();
        }
    }

    /**
     * Register a filter function. The function will receive the value to filter
     * as the first argument, and any additional data as additional arguments.
     * 
     * The filter function MUST ALWAYS return the original value if it doesn't make
     * any changes.
     * 
     * @param Closure ...$listeners
     */
    public function listen(Closure ...$listeners): void {
        foreach ($listeners as $listener) {
            $this->listeners[] = $listener;
        }
    }

    /**
     * Stop receiving notifications whenever this event occurs. This
     * will remove ALL subscriptions that this closure has.
     * 
     * @param Closure ...$listeners 
     */
    public function off(Closure ...$listeners): void {
        self::filterArrays($listeners, $this->listeners);
    }
}