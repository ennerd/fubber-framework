<?php
namespace Fubber\Hooks;

use Closure;

/**
 * Create an event dispatcher for an event of any type.
 * 
 * @package Fubber\Hooks
 */
class Event extends Dispatcher {

    protected array $listeners = [];
    protected array $onceListeners = [];

    /**
     * Invoke all event listeners with the arguments provided. If you pass an
     * object, the event listeners MAY modify the object (unless you take care
     * to design it as an immutable object).
     * 
     * Listeners may throw exceptions.
     * 
     * @param mixed ...$args 
     * @throws \Throwable
     */
    public function trigger(mixed ...$args): void {
        $this->invokeAll($this->listeners, ...$args);

        $once = $this->onceListeners;
        $this->onceListeners = [];
        $this->invokeAll($once, ...$args);
    }

    /**
     * Subscribe to get notified whenever this event occurs.
     * 
     * @param Closure $listeners
     */
    public function listen(Closure ...$listeners): void {
        foreach ($listeners as $listener) {
            $this->listeners[] = $listener;
        }
    }

    /**
     * Subscribe to this event, and immediately unsubscribe after the
     * event has been triggered ONE time.
     * 
     * @param Closure ...$listeners 
     */
    public function once(Closure ...$listeners): void {
        foreach ($listeners as $listener) {
            $this->onceListeners[] = $listener;
        }
    }

    /**
     * Stop receiving notifications whenever this event occurs. This
     * will remove ALL subscriptions that this closure has.
     * 
     * @param Closure ...$listeners
     */
    public function off(Closure ...$listeners): void {
        self::filterArrays(
            $listeners,
            $this->listeners,
            $this->onceListeners,
        );
    }
}