<?php
namespace Fubber\Hooks;

use Closure;
use CodingErrorException;
use Fubber\InternalErrorException;
use Fubber\Kernel;
use Fubber\Kernel\Debug\Describe;
use LogicException;
use Throwable;

/**
 * This abstract event dispatcher is the root class for most Fubber\Hooks
 * mechanisms.
 * 
 * @package Fubber\Hooks
 */
abstract class Dispatcher implements DispatcherInterface {

    private static int $queueFirstIndex = 0;
    private static int $queueLastIndex = 0;
    private static array $queuedListeners = [];
    private static array $queuedArgs = [];
    private static array $queuedDispatchers = [];

    /**
     * Holds a stack trace of where this event dispatcher was constructed
     * from - which can be helpful in case of exceptins.
     * 
     * @var array
     */
    private readonly array $constructLocation;

    /**
     * A Closure which will be invoked whenever an event listener throws an
     * exception. 
     * 
     * @var Closure `function(Throwable $exception, Closure $listener, AbstractEvent $event): void`
     */
    private static ?Closure $exceptionHandler = null;

    /**
     * A Closure which schedules a function to be invoked asynchronously.
     * 
     * @var null|Closure
     */
    private static ?Closure $deferFunction = null;

    /**
     * A Closure which starts the event loop and runs all functions which have been scheduled
     * so far. The Closure MUST NOT run scheduled functions that are added later.
     * 
     * @var null|Closure
     */
    private static ?Closure $runEventsFunction = null;

    public function __construct(
        private readonly string|null $description=null,
    ) {
        // Benchmarked: Fetching and storing this trace 1 000 000 000 times takes 0.3 seconds
        $this->constructTrace = \debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS, 1)[0];
    }

    /**
     * Get a description of the event dispatcher
     * 
     * @return null|string 
     */
    public final function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Get the filename where this event dispatcher was created
     * 
     * @return string 
     */
    public final function getFile(): string {
        return $this->constructLocation['file'];
    }

    /**
     * Get the line number where this event dispatcher was created
     * 
     * @return int 
     */
    public final function getLine(): int {
        return $this->constructLocation['line'];
    }

    public function getListeners(): array {
        return $this->listeners;
    }

    /**
     * Configure a function for queuing a new job in the event loop, and a function for running all pending events in the
     * event loop.
     * 
     * @param Closure $deferFunction 
     * @param Closure $runEventsFunction 
     * @param Closure $exceptionHandler
     * @throws LogicException 
     */
    public final static function configure(Closure $deferFunction, Closure $runEventsFunction, Closure $exceptionHandler): void {
        if (self::$deferFunction !== null) {
            throw new LogicException("Can't set the event loop twice; generally there should be only one event loop in an application");
        }
        self::$deferFunction = $deferFunction;
        self::$runEventsFunction = $runEventsFunction;
        self::$exceptionHandler = $exceptionHandler;
        if (self::$queueLastIndex > self::$queueFirstIndex) {
            for ($i = self::$queueFirstIndex; $i < self::$queueLastIndex; $i++) {
                $func = self::$queuedListeners[$i];
                $args = self::$queuedArgs[$i];
                $event = self::$queuedDispatchers[$i];
                self::defer($event, $func, $args);
            }
            throw new LogicException("Dah");

        }
    }

    /**
     * Utility function for invoking all listeners in an array
     * 
     * @param Closure[] $listeners Array of closures
     * @param mixed ...$args Any arguments to pass to listeners
     * @return mixed[] The return values from each listener
     */
    protected function invokeAll(array $listeners, mixed ...$args): void {
        foreach ($listeners as $listener) {
            self::defer($this, $listener, $args);
        }
        self::runEvents();
    }

    /**
     * Handles an exception using a configured exception handler, or throws the
     * exception if no exception handler was configured.
     * 
     * @param Throwable $exception 
     * @param null|Closure $listener 
     * @param null|Dispatcher $source 
     * @throws Throwable 
     */
    protected static function handleException(Throwable $exception, ?Closure $listener, ?Dispatcher $source): void {
        if (self::$exceptionHandler !== null) {
            try {
                (self::$exceptionHandler)(exception: $exception, listener: $listener, event: $source);
            } catch (\Throwable $e) {
                Kernel::handleFatalException($e);
            }
        } else {
            throw $exception;
        }
    }
    
    /**
     * Utilify function which filters an array to remove the element `$valueToRemove`
     * 
     * @param array $array The source array
     * @param mixed $valueToRemove The value to remove
     * @param null|int $count Will contain the number of elements removed from the array
     * @param int|null $limit Limits the number of elements removed
     * @return array 
     * @throws InternalErrorException 
     */
    protected static function filterArray(array $array, mixed $valueToRemove, ?int &$count=0, int $limit=null): array {
        $result = [];
        $index = 0;
        foreach ($array as $k => $v) {
            if ($k !== $index++) {
                throw new InternalErrorException(__METHOD__ . ' only supports simple vector arrays with numeric keys in order');
            }
            if ($limit !== $count && $v == $valueToRemove) {
                ++$count;
                continue;
            }
            $result[] = $k;
        }
        return $result;
    }

    /**
     * Utility function for scheduling a function to run when {@see self::runEvents()} is
     * called.
     * 
     * @param Dispatcher $dispatcher 
     * @param Closure $listener 
     * @param array $args 
     */
    protected static function defer(Dispatcher $dispatcher, Closure $listener, array $args): void {
        if (self::$deferFunction) {
            (self::$deferFunction)(self::invoke(...), $dispatcher, $listener, $args);
        } else {
            self::$queuedListeners[self::$queueLastIndex] = $listener;
            self::$queuedArgs[self::$queueLastIndex] = $args;
            self::$queuedDispatchers[self::$queueLastIndex] = $dispatcher;
            ++self::$queueLastIndex;    
        }
    }

    /**
     * Utility function for running scheduled events.
     * 
     * @throws Throwable 
     */
    protected static function runEvents(): void {
        if (self::$runEventsFunction) {
            (self::$runEventsFunction)();
            return;
        }
        /**
         * The $runUntil ensures that we won't be running new tasks that may be added to the
         * queue by any of these listeners.
         */
        $runUntil = self::$queueLastIndex;
        while (self::$queueFirstIndex < $runUntil) {
            $listener = self::$queuedListeners[self::$queueFirstIndex];
            $args = self::$queuedArgs[self::$queueFirstIndex];
            $source = self::$queuedDispatchers[self::$queueFirstIndex];
            unset(
                self::$queuedListeners[self::$queueFirstIndex],
                self::$queuedArgs[self::$queueFirstIndex],
                self::$queuedDispatchers[self::$queueFirstIndex]
            );
            ++self::$queueFirstIndex;
            try {
                $listener(...$args);
            } catch (Throwable $exception) {
                self::handleException($exception, $listener, $source);
            }
        }
    }

    /**
     * Removes values from a set of arrays by reference.
     * 
     * @param array $values 
     * @param array $arrays 
     * @throws InternalErrorException 
     */
    protected function filterArrays(array $values, array &...$arrays) {
        foreach ($arrays as &$array) {
            foreach ($values as $value) {
                foreach ($array as $k => $v) {
                    if ($v == $value) {
                        unset($array[$k]);
                    }
                }
            }
        }
    }

    protected static function invoke(Dispatcher $dispatcher, Closure $listener, array $args): mixed {
        try {
            return $listener(...$args);
        } catch (\Throwable $e) {
            self::handleException($e, $listener, $dispatcher);
        }

    }
}