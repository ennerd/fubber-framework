<?php
namespace Fubber\Hooks;

use Closure;

/**
 * A Handler is a special event dispatcher where only one event listener
 * will process the value. Any number of listeners can be attached to
 * the dispatcher. When dispatched, the first listener to return a response
 * will be used; no further listeners will be invoked.
 * 
 * @package Fubber\Hooks
 */
class Handler extends Dispatcher {

    public function off(Closure ...$listeners): void {
        self::filterArrays(
            $listeners,
            $this->listeners,
        );
    }


    /**
     * Try each listener in order, and the first listener that responds with a non-null value
     * will be used. The remaining listeners will not be invoked.
     * 
     * Listeners may throw exceptions.
     * 
     * @param mixed $data The value that needs processing
     * @param mixed ...$args Optional extra data
     * @return mixed The processed value
     * @throws \Throwable
     */
    public function trigger(mixed $data, mixed ...$args): mixed {
        foreach ($this->listeners as $listener) {
            $result = self::invoke($this, $listener, [ $data, ...$args] );
            if ($result !== null) {
                return $result;
            }
        }
        return $data;
    }

}