<?php
namespace Fubber\Hooks;

use Closure;
use Fubber\CodingErrorException;
use Fubber\Kernel;
use Fubber\LogicException;
use Fubber\PHP\ClosureTool;
use WeakMap;

/**
 * An event dispatcher which will invoke event listers ONCE per source.
 * After the event has been triggered, any new subscribers will immediately be invoked.
 * 
 * Subscribers can subscribe to a particular source with the {@see static::listenFor()}
 * method, or to all events regardless of source with the {@see static::listen()} method.
 * 
 * A source is defined as a string (class-name) or an object instance.
 * 
 * @package Fubber\Hooks
 */
class PerItemTriggers extends Dispatcher {

    /**
     * Records string sources that triggered an event
     * 
     * @var array<string, array>
     */
    protected array $triggeredStrings = [];

    /**
     * Records object sources that triggered an event
     * 
     * @var WeakMap<object, array>
     */
    protected readonly WeakMap $triggeredObjects;

    /**
     * Tracks listeners on string sources
     * 
     * @var array<string, Closure[]>
     */
    protected array $stringListeners = [];

    /**
     * Tracks listeners on object sources
     * 
     * @var WeakMap<object, Closure[]>
     */
    protected readonly WeakMap $objectListeners;

    /**
     * Tracks listeners on every event unfiltered
     * 
     * @var Closure[]
     */
    protected array $listeners = [];

    public function __construct(string $description=null) {
        parent::__construct($description);
        $this->triggeredObjects = new WeakMap();
        $this->objectListeners = new WeakMap();
    }

    /**
     * Was this event triggered?
     * 
     * @return bool 
     */
    public function wasTriggeredFor(string|object $source): bool {
        if (\is_string($source)) {
            return \array_key_exists($source, $this->triggeredStrings);
        } else {
            return isset($this->triggeredObjects[$source]);
        }
    }

    /**
     * Trigger this event for a particular source.
     * 
     * @param string|object $source
     * @param mixed $data 
     * @return mixed 
     */
    public function triggerFor(string|object $source, mixed ...$data): void {
        if ($this->wasTriggeredFor($source)) {
            throw new CodingErrorException("This event was already triggered");
        }

        // record that this source has been triggered
        if (\is_string($source)) {
            $this->triggeredStrings[$source] = $data;
        } else {
            $this->triggeredObjects[$source] = $data;
        }

        $this->invokeAll($this->listeners, $source, ...$data);

        if (\is_string($source)) {
            if (empty($this->stringListeners[$source])) {
                return;
            }
            $this->triggeredStrings[$source] = $data;

            $listeners = $this->stringListeners[$source];
            unset($this->stringListeners[$source]);
            $this->invokeAll($listeners, $source, ...$data);
        } else {
            if (!isset($this->objectListeners[$source])) {
                return;
            }
            $this->triggeredObjects[$source] = $data;

            $listeners = $this->objectListeners[$source];
            unset($this->objectListeners[$source]);
            $this->invokeAll($listeners, $source, ...$data);
        }
    }

    /**
     * Subscribe to all events dispatched. The listener will receive the source string|object
     * as its first argument.
     * 
     * @param Closure ...$listeners 
     * @throws LogicException 
     */
    public function listen(Closure ...$listeners): void {
        foreach ($listeners as $listener) {
            $this->listeners[] = $listener;
        }
    }

    /**
     * Subscribe to a particular event source. The listener will receive the source string|object
     * as its first argument.
     * 
     * @param string|object $source 
     * @param Closure ...$listeners 
     * @throws LogicException 
     */
    public function listenFor(string|object $source, Closure ...$listeners): void {
        if (\is_object($source)) {
            if (isset($this->triggeredObjects[$source])) {
                // Previously invoked, no need to store this listener
                $this->invokeAll($listeners, $source, ...$this->triggeredObjects[$source]);
            } else {
                // Add the listener
                if (!isset($this->objectListeners[$source])) {
                    $this->objectListeners[$source] = [
                        ...$this->objectListeners[$source],
                        ...$listeners
                    ];
                } else {
                    $listeners = [ ...$this->objectListeners[$source], ...$listeners ];
                    $this->objectListeners[$source] = $listeners;
                }
            }
        } else {
            if (\array_key_exists($source, $this->triggeredStrings)) {
                // Previously invoked, no need to store this listener
                $this->invokeAll($listeners, $source, ...$this->triggeredStrings[$source]);
            } else {
                // Add the listener
                $this->stringListeners[$source] = [ ...($this->stringListeners[$source] ?? []), ...$listeners ];
            }
        }
    }

    public function off(Closure ...$listeners): void {
        self::filterArrays(
            $listeners,
            $this->listeners,
        );
        self::filterArrays(
            $listeners,
            ...$this->stringListeners
        );
        foreach ($this->objectListeners as $object => $array) {
            self::filterArrays($listeners, $array);
            if ($array === []) {
                unset($this->objectListeners[$object]);
            } else {
                $this->objectListeners[$object] = $array;
            }
        }
    }
}