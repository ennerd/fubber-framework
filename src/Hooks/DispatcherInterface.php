<?php
namespace Fubber\Hooks;

use Closure;

/**
 * @package Fubber\Hooks
 */
interface DispatcherInterface {

    /**
     * Get a description of the exception
     * 
     * @return null|string 
     */
    public function getDescription(): ?string;

    /**
     * Get the filename where this exception was created
     * 
     * @return string 
     */
    public function getFile(): string;

    /**
     * Get the line number where this exception was created
     * 
     * @return int 
     */
    public function getLine(): int;

    /**
     * Remove all references to the provided closure
     * 
     * @param Closure ...$closures 
     */
    public function off(Closure ...$closures): void;
}