<?php
namespace Fubber\Hooks;

use UnitEnum;
use Closure;
use Fubber\CodingErrorException;
use Fubber\Kernel;
use Fubber\LogicException;

/**
 * A state machine which validates that states only transition to legal
 * target states.
 * 
 * @package Fubber\Hooks
 */
class StateMachine extends Dispatcher {

    /**
     * All valid state transitions for every state permitted in the form of bitmasks:
     * `[ 'from-state' => [ 'to-state1', 'to-state2' ], ... ]`
     * 
     * @var array<string, string[]>
     */
    protected readonly array $transitions;

    /**
     * The current state
     * 
     * @var string
     */
    protected string|int|UnitEnum $state;

    /**
     * True during state transitioning.
     * 
     * @var ?string
     */
    protected null|string|int|UnitEnum $transitioningTo = null;

    /**
     * Subscribers to all state changes
     * 
     * @var Closure[]
     */
    protected array $listeners = [];

    /**
     * Subscribers for entering a particular state
     * 
     * @var array<string, Closure[]>
     */
    protected array $enteringListeners = [];

    /**
     * Subscribers for exiting a particular state
     * 
     * @var array<string, Closure[]>
     */
    protected array $exitingListeners = [];

    /**
     * Subscribers for completed entering a particular state
     * 
     * @var array<string, Closure[]>
     */
    protected array $enteredListeners = [];

    /**
     * Subscribers for completed exiting a particular state
     * 
     * @var array<string, Closure[]>
     */
    protected array $exitedListeners = [];

    /**
     * Subscribers to get notified ONCE when the current state exits
     * 
     * @var Closure[]
     */
    protected array $exitCurrentListeners = [];

    /**
     * Configure the states and valid transitions
     * 
     * @param array<string, string[]> $transitions A map from state to legal target states
     */
    public function __construct(array $transitions, string $description=null) {
        parent::__construct($description);

        $mappedTransitions = [];
        foreach ($transitions as $transition) {
            $mappedTransitions[self::scalarState($transition[0])] = $transition;
        }
        $this->transitions = $mappedTransitions;
        foreach ($transitions as $trans) {
            $this->state = $trans[0];
            break;
        }
    }

    /**
     * Return the current state
     * 
     * @return mixed 
     */
    public function getCurrentState(): mixed {
        return $this->state;
    }

    public function __toString() {
        return $this->state;
    }

    /**
     * Invoke all event listeners with the arguments provided. If you pass an
     * object, the event listeners MAY modify the object (unless you take care
     * to design it as an immutable object).
     * 
     * Listeners may throw exceptions.
     * 
     * @param mixed $targetState The state to transition to
     * @throws \Throwable
     */
    public function trigger(string|int|UnitEnum $targetState): void {
        try {
            $previousState = $this->state;

            $this->assertStateExists($targetState);
            $this->assertNotInTransition();
            $this->assertValidTargetState($targetState);

            $oldStateStr = self::scalarState($this->state);
            $newStateStr = self::scalarState($targetState);
            $stats = [];
            if (0 < ($count = count($this->exitCurrentListeners ?? []))) {
                $stats['exit current listeners'] = $count;
            }
            if (0 < ($count = count($this->exitingListeners[$oldStateStr] ?? []))) {
                $stats['`'.$oldStateStr.'`-> listeners'] = $count;
            }
            if (0 < ($count = count($this->enteringListeners[$newStateStr] ?? []))) {
                $stats['->`'.$oldStateStr.'` listeners'] = $count;
            }
            if (0 < ($count = count($this->listeners ?? []))) {
                $stats['->*-> listeners'] = $count;
            }
            Kernel::debug('Changing state from `{oldState}` to `{newState}`', [ 'oldState' => $oldStateStr, 'newState' => $newStateStr ], $stats);
            $this->transitioningTo = $targetState;

            $exitCurrentListeners = $this->exitCurrentListeners;
            $this->exitCurrentListeners = [];
            $this->invokeAll($exitCurrentListeners, $previousState, $targetState);

            $this->invokeAll($this->exitingListeners[self::scalarState($this->state)] ?? [], $previousState, $targetState);
            $this->invokeAll($this->listeners ?? [], $previousState, $targetState);
            $this->invokeAll($this->enteringListeners[self::scalarState($this->transitioningTo)] ?? [], $previousState, $targetState);

            $this->state = $this->transitioningTo;
            
            $this->invokeAll($this->exitedListeners[self::scalarState($previousState)] ?? [], $previousState, $this->state);
            $this->invokeAll($this->enteredListeners[self::scalarState($this->state)] ?? [], $previousState, $this->state);
        } finally {
            $this->transitioningTo = null;
        }
    }

    /**
     * Triggers once when the current state ends.
     * 
     * @param Closure $listener `function(string $oldState, string $newState): void`
     */
    public function onExitCurrentState(Closure $listener): void {
        $this->exitCurrentListeners[] = $listener;
    }

    /**
     * Subscribe to when a particular state is going to be entered
     * 
     * @param string|string[] $targetStates
     * @param Closure $listener `function(string $oldState, string $newState): void`
     * @throws CodingErrorException 
     */
    public function onEnteringState(string|array|UnitEnum $targetStates, Closure $listener): void {
        foreach ($this->filterStatesArgument($targetStates) as $state) {
            $this->enteringListeners[self::scalarState($state)][] = $listener;
        }
    }

    /**
     * Subscribe to when a particular state has been entered
     * 
     * @param string|string[] $targetStates
     * @param Closure $listener `function(string $oldState, string $newState): void`
     * @throws CodingErrorException 
     */
    public function onEnteredState(string|array|UnitEnum $targetStates, Closure $listener): void {
        foreach ($this->filterStatesArgument($targetStates) as $state) {
            $this->enteredListeners[self::scalarState($state)][] = $listener;
        }
    }

    /**
     * Subscribe to when a particular state is going to be exited
     * 
     * @param string|string[] $targetStates
     * @param Closure $listener `function(string $oldState, string $newState): void`
     * @throws CodingErrorException 
     */
    public function onExitingState(string|array|UnitEnum $targetStates, Closure $listener): void {
        foreach ($this->filterStatesArgument($targetStates) as $state) {
            $this->exitingListeners[self::scalarState($state)][] = $listener;
        }
    }

    /**
     * Subscribe to when a particular state has been exited
     * 
     * @param string|string[] $targetStates
     * @param Closure $listener `function(string $oldState, string $newState): void`
     * @throws CodingErrorException 
     */
    public function onExitedState(string|array|UnitEnum $targetStates, Closure $listener): void {
        foreach ($this->filterStatesArgument($targetStates) as $state) {
            $this->exitedListeners[self::scalarState($state)][] = $listener;
        }
    }

    /**
     * Subscribe to get notified about all events
     * 
     * @param Closure ...$listeners `function(mixed $oldState, mixed $newState): void`
     */
    public function listen(Closure ...$listeners): void {
        foreach ($listeners as $listener) {
            $this->listeners[] = $listener;
        }
    }

    /**
     * Stop receiving notifications whenever this event occurs. This
     * will remove ALL subscriptions that this closure has.
     * 
     * @param Closure ...$listeners
     */
    public function off(Closure ...$listeners): void {
        self::filterArrays($listeners,
            $this->listeners, 
            $this->enteringListeners,
            $this->exitingListeners,
            $this->enteredListeners,
            $this->exitedListeners,
            $this->exitCurrentListeners,
        );
    }

    protected function assertNotInTransition(): void {
        if ($this->transitioningTo !== null) {
            throw new CodingErrorException("Already transitioning to state `" . $this->transitioningTo . "`, can't begin another transition yet.");
        }
    }

    /**
     * Assert that the provided states exists
     * 
     * @param string ...$states 
     * @throws CodingErrorException 
     */
    protected function assertStateExists(string|int|UnitEnum ...$states): void {
        foreach ($states as $state) {
            if (!isset($this->transitions[self::scalarState($state)])) {
                throw new CodingErrorException("Unknown state `".self::scalarState($state)."`");
            }
        }
    }

    /**
     * Assert that the provided state is a valid target state
     * 
     * @param string $state 
     * @throws CodingErrorException 
     * @throws LogicException 
     */
    protected function assertValidTargetState(string|int|UnitEnum $state): void {
        $this->assertStateExists($state);
        $targets = $this->transitions[self::scalarState($this->state)];
        if (\in_array($state, \array_slice($targets, 1))) {
            return;
        }
        if (count($targets) === 1) {
            $tail = 'there are no valid future states.';
        } else {
            $tail = 'valid target states are `' . implode('`, `', \array_map(self::scalarState(...), \array_slice($this->transitions[self::scalarState($this->state)], 1))) . '`';
        }
        throw new LogicException("Illegal transition from `".self::scalarState($this->state)."` to `".self::scalarState($state)."`, $tail");
    }

    /**
     * Recursively finds all unique strings in the passed strings and arrays.
     * 
     * @param string|array ...$states 
     * @return array 
     */
    protected function filterStatesArgument(string|int|UnitEnum|array ...$states): array {
        $result = [];
        foreach ($states as $state) {
            if (\is_array($state)) {
                foreach (self::filterStatesArgument(...$state) as $state) {
                    $result[self::scalarState($state)] = $state;
                }
            } else {
                $result[self::scalarState($state)] = $state;
            }
        }
        $this->assertStateExists(...$result);
        return \array_values($result);;
    }

    private static function scalarState(string|int|UnitEnum $state): string|int {
        if ($state instanceof UnitEnum) {
            return $state->name;
        }
        return $state;
    }
}