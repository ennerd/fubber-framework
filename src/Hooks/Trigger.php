<?php
namespace Fubber\Hooks;

use Closure;
use Fubber\CodingErrorException;

/**
 * A trigger is a special event type which can only be triggered at most ONE time.
 * After it has been triggered, all new subscribers added will immediately be invoked
 * with the arguments that triggered the event in the first place.
 * 
 * Triggers are nice if you need to check for availability of things.
 * 
 * You can check if a trigger has been activated with the {@see Trigger::wasTriggered()}
 * method.
 * 
 * @package Fubber\Hooks
 */
class Trigger extends Dispatcher {

    protected bool $triggered = false;
    protected array $data;
    protected array $listeners = [];

    /**
     * Has this event already been triggered?
     * 
     * @return bool 
     */
    public function wasTriggered(): bool {
        return $this->triggered;
    }

    /**
     * Activate the trigger and run all listener functions.
     * 
     * @param mixed $args 
     * @return mixed 
     */
    public function trigger(mixed ...$args): void {
        if ($this->triggered) {
            throw new CodingErrorException("This event was already triggered");
        }
        $this->triggered = true;
        $this->data = $args;
        $listeners = $this->listeners;
        $this->listeners = [];
        $this->invokeAll($listeners, ...$args);
    }

    /**
     * Subscribe to get notified whenever this event occurs.
     * 
     * @param Closure ...$listeners
     */
    public function listen(Closure ...$listeners): void {
        if ($this->triggered) {
            $this->invokeAll($listeners, ...$this->data);
        } else {
            foreach ($listeners as $listener) {
                $this->listeners[] = $listener;
            }
        }
    }

    /**
     * Stop receiving notifications whenever this trigger occurs. This
     * will remove ALL subscriptions that this closure has.
     * 
     * @param Closure ...$listeners
     */
    public function off(Closure ...$listeners): void {
        self::filterArrays($listeners, $this->listeners);
    }
}