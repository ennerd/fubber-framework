<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;

/**
 * The exception for when a request attempts to perform an operation
 * that they don't have the privileges to do. It is primarily meant for
 * authenticated requests, where the user don't have enough privileges.
 *
 * @see UnauthorizedException
 * @see NotAuthenticatedException
 * @see AccessDeniedException
 */
class UnauthorizedException extends AccessDeniedException {

    public function getDefaultStatus(): array {
        return [403, "Unauthorized"];
    }
    
    public function getExceptionDescription(): string {
        return "Unauthorized";
    }
}
