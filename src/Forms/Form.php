<?php
namespace Fubber\Forms;

use Fubber\Html\DomNode;
use Fubber\Html\CData;
use Fubber\Html\Tag;
use Fubber\ForbiddenException;
use Fubber\Kernel\State;

/**
 *  Forms API - to structure preparing and handling forms in an MVC environment. Basic usage:
 * 
 *  use \Fubber\Forms\Form;
 * 
 *  class LoginForm extends Form {
 *      public function __construct($request) {
 *          parent::__construct('login-form');
 *          $this->setRequest($request);
 * 
 *          $this->addProperty('name', '');
 *          $this->addProperty('gender', '', ['' => 'Not Selected', 'm' => 'Male', 'f' => 'Female']);
 * 
 *          if($this->accept()) {
 *              // Insert into database or something like that.
 *              $this->success = true;
 *          }
 *      }
 *    
 *      public function isInvalid() {
 *          $errors = new Errors($this);
 *          $errors->oneOf('gender', ['m','f'])
 *          return $errors->isInvalid();
 *      }
 *  }
 */

class Form {
    const POST = "POST";
    const GET = "GET";
    
    public $hasData = false;
    private $_success = null;        // true if the form has been successfully handled, false if there were errors, null if not submitted
    public $error = null;           // Error message for the form.
    public $errors = null;         // Field errors as array
    public $_id = null;
    protected $_extraData = null;
    protected $_state = null;       // The State object.
    protected $_fields = [];        // Fields and their value
    protected $_options = [];       // Some fields have different alternatives available, such as a <select>
    protected $_method = self::POST;    // POST or GET
    protected $_accepted = false;
    protected $_usedFields = [];    // Field names of fields used in post data
    protected $_optionalFields = [];// Fields that are optional 
    protected $_arrayFields = [];
    
    
    /**
     *      $fields = ['field1' => 'value1'];
     * or
     *      $fields = ['field1' => ['value1', ['value1','value2','value3']]]
     */
    public function __construct(State $state, $id, $fields = [], $extraData = null) {
        $this->_state = $state;
        $this->_id = $id;
        foreach($fields as $name => $value) {
            if(is_array($value)) {
                list($value, $options) = $value;
                $this->addField($name, $value, $options);
            } else {
                $this->addField($name, $value);
            }
        }
        $this->_extraData = $extraData;
    }
    
    public function addOptionalField($name) {
        $this->_optionalFields[] = $name;
        return $this;
    }
    
    public function getExtraData() {
        if(!$this->_accepted)
            throw new Exception("Must call ->accept() before calling ->getExtraData()");
        return $this->_extraData;
    }

    public function __set($name, $value) {
        if ($name === 'success') {
            if ($value === null || is_bool($value)) {
                $this->_success = $value;
                return;
            } else {
                throw new \TypeError("Property \$success must be ?bool");
            }
        }
/*
        if(isset($this->_options[$name]) && !isset($this->_options[$name][$value])) {
            throw new Exception("The value '$value' is not legal for the form field '$name'.");
        }
*/
        if(is_object($value)) {
            throw new Exception("Values for forms can't be objects (field name $name)");
        }
        $this->_fields[$name] = $value;
    }
    
    public function __get($name) {
        if ($name === 'success') {
            return $this->_success;
        }
        if(substr($name, -2)=='[]') {
            throw new \Exception("Careful! Arrays must be handled (use getId() and getValue()).");
        }
        if(isset($this->_fields[$name]))
            return $this->_fields[$name];
        return null;
    }
    
    public function get($name) {
        return $this->__get($name);
    }
    
    public function __isset($name) {
        return isset($this->_fields[$name]);
    }
    
    /**
     * Returns false, or the error text.
     * 
     * @param string $name  The name of the field
     */
    public function hasError($name) {
        if($this->hasData && $this->errors === null) {
            $this->errors = $this->isInvalid();
        }
        if($this->errors) {
            if(substr($name, -2)=='[]') {
                $rName = substr($name, 0, -2);
                if(!isset($this->counters[$rName]))
                    $this->counters[$rName] = 0;
                $rName = $rName.'|'.$this->counters[$rName];
                if(!isset($this->errors[$rName]))
                    return false;
                return $this->errors[$rName];
            } else {
                if(!isset($this->errors[$name]))
                    return false;
                return $this->errors[$name];
            }
        }
        return false;
    }
    
    /**
     * Add a new field to the form. For example:
     * 
     * $form->addField('username', null);
     * $form->addField('age', 18, [18, 19, 20]);
     * 
     * @param string $name  The name of the field
     * @param mixed $value  The default value for this field
     * 
     */
    public function addField($name, $value=null, $alternatives=null) {
        if(substr($name, -2)=='[]') {
            $name = substr($name, 0, -2);
            $this->_arrayFields[$name] = $name;
        }
        if($alternatives !== null)
            $this->_options[$name] = $alternatives;

        // The value may already be set through signed post data            
        if(!isset($this->$name))
            $this->$name = $value;

        return $this;
    }
    
    public function getAllFields() {
        return array_keys($this->_fields);
    }
    
    /**
     * Accept form data from the request. If there was data, return true - else
     * return false.
     */
    public function accept(): bool {
        $src = $this->_method == self::POST ? $this->_state->post : $this->_state->get;
        $foundSomething = false;
        if(!isset($src['_s'])) {
            return false;
        }
       
        $_s = explode("|", $src['_s']);
        $hash = $_s[0];
        if(isset($_s[1])) {
            $optionalFields = explode(",", $_s[1]);
        } else {
            $optionalFields = [];
        }
        if(isset($src['_extra'])) {
            $extraData = unserialize(base64_decode($src['_extra']));
        } else {
            $extraData = null;
        }
        // Check hash signature against the fields that were actually submitted. This avoids
        $usedFields = [];
        $datetimeFields = [];
        foreach($src as $k => $v) {
            if($k === '_s') {
                continue;
            }
            if (in_array(substr($k, -6), $datetimeFields)) {
                continue;
            }
            if (strpos($k, '__date')!==false || strpos($k, '__time')!==false) {
                $datetimeFieldName = substr($k, 0, -6);
                $datetimeFields[] = $datetimeFieldName;
                $usedFields[] = $datetimeFieldName;
            } else {
                $usedFields[] = $k;
            }
        }
        // Make sure optionalFields are always part of the hash, even if they are not submitted
        foreach($optionalFields as $optionalField) {
            $usedFields[] = $optionalField;
        }
        $usedFields = array_unique($usedFields);
        sort($usedFields);
        /*
        foreach($usedFields as &$usedField) {
            if(isset($src[$usedField]) && is_array($src[$usedField]))
                $usedField .= '[]';
        }
        */
        $hashBase = serialize($usedFields);
        if($extraData) {
            $hashBase .= serialize($extraData);
        }
        //var_dump($hashBase);die();
        //FRODE1: var_dump($hashBase);die();

        $firstTime = time() - 3600*4;
        $lastTime = time();
        $salt = \Fubber\Kernel::$instance->env->salt;
        $hashOK = false;
        for($ts = $lastTime; $ts > $firstTime; $ts--) {
            $testHash = hash_hmac('sha1', $hashBase . $ts, $salt);
            if($hash == $testHash) {
                $hashOK = true;
                break;
            }
        }
        if(!$hashOK) {
            throw new ForbiddenException("CSRF token invalid or expired");

            return false;
        }
        foreach ($datetimeFields as $k) {
            if (isset($src[$k.'__date']) && isset($src[$k.'__time'])) {
                $value = null;
                if (is_array($src[$k.'__date']) && is_array($src[$k.'__time'])) {
                    $value = [];
                    foreach ($src[$k.'__date'] as $i => $date) {
                        if (!$date[$i.'__date'] || !$date[$i.'__time']) {
                            $value[] = '';
                        } else {
                            $ts = strtotime(($date??gmdate('Y-m-d')).' '.$src[$i.'__time']);
                            $value[] = gmdate('Y-m-d H:i:s', $ts);
                        }
                    }
                } else {
                    if (!$src[$k.'__date'] || !$src[$k.'__time']) {
                        $value = '';
                    } else {
                        $ts = strtotime(( $src[$k.'__date'] ?? gmdate('Y-m-d') ) . ' ' . $src[$k.'__time'] );
                        $value = gmdate('Y-m-d H:i:s', $ts);
                    }
                }
                $src[$k] = $value;
            }
        }

        foreach($usedFields as $k) {
            if(substr($k, -2)=='[]') {
                $array = true;
                $k = substr($k, 0, -2);
            } else {
                $array = false;
            }
            $v = 0;
            if(isset($src[$k]))
                $v = $src[$k];
            if( is_array( $v ) && isset($v['type']) && method_exists(static::class, $methodName = $v['type'].'Parser')) {
                $this->$k = static::$methodName($v);
            } else {
                $this->$k = $v;
            }
        }

        $this->_extraData = $extraData;
//        if(isset($src['_extra'])) {
//            $this->_extraData = unserialize(base64_decode($src['_extra']));
//        }
        $this->errors = $this->isInvalid();
        return $this->_accepted = true;
    }
    
    /**
     * Returns the <form>-tag
     */
    public function begin(array $attrs=[]) {
        $html = new Tag('form', self::mergeAttrs($attrs, [
            'class' => 'f-form',
            'method' => $this->_method,
            ]));
        if($this->_extraData) {
            $html .= '<input type="hidden" name="_extra" value="'.base64_encode(serialize($this->_extraData)).'">';
            $this->_usedFields[] = '_extra';
        }
        return $html;
    }
    
    /**
     * Returns the </form>-tag
     */
    public function end() {
        $usedFields = array_unique($this->_usedFields);
        foreach($usedFields as $k => $fieldName) {
            if(substr($fieldName, -2)=='[]')
                $usedFields[$k] = substr($fieldName, 0, -2);
        }
        sort($usedFields);
        // Add timestamp to last hash. This makes sure the form will only work for a certain time, and also the hash changes on every page view.
        // This ruins cacheability a bit.

        $hashBase = serialize($usedFields);
        if(isset($this->_extraData))
            $hashBase .= serialize($this->_extraData);
        $hashBase .= time();


        //var_dump($usedFields);
        //var_dump(get_class($this));die();
        
        $hash = hash_hmac('sha1', $hashBase, \Fubber\Kernel::$instance->env->salt);
        // We must inform about optional fields to the accept function.
        if(sizeof($this->_optionalFields) > 0) {
            $optionalFields = $this->_optionalFields;
            foreach($optionalFields as $k => $fieldName) {
                if(substr($fieldName, -2)=='[]')
                    $optionalFields[$k] = substr($fieldName, 0, -2);
            }
            $optionalFields = array_unique($optionalFields);
            $hash .= "|".implode(",", $optionalFields);
        }
        return '<input type="hidden" name="_s" value="'.$hash.'"></form>';
    }
    
    public function fieldset($legend, array $fields) {
        
        array_unshift($fields, new DomNode(new Tag('legend'), [$legend]));
        
        return new DomNode(new Tag('fieldset', ['class' => 'f-fieldset']), $fields);
            
    }
    
    public function form(array $attrs=[], $children=[]) {
        return new DomNode($this->begin($attrs), $children);
    }
    
    /**
     * Renders a basic field
     */
    public function field($name, $type, $label) {
        $wrapper = new DomNode(new Tag("div", ["class" => "field"]));
        //$labelWrapper = new DomNode(new Tag('span', ["class" => "f-form f-label-wrap"]));
        $wrapper->append($this->label($name, $label));
        //$fieldWrapper = new DomNode(new Tag('span', ["class" => "f-form f-input-wrap"]));
        $wrapper->append($this->$type($name));
        //$wrapper->append($labelWrapper);
        //$wrapper->append($fieldWrapper);
        return $wrapper;
    }
    
    /**
     * Returns <select>-tag
     */
    public function select($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        $choices = [];
        $value = $this->getValue($name);
        if(!isset($this->_options[rtrim($name, '[]')]))
            throw new \Exceptions\CodingErrorException("The field '$name' has no options declared.");
            
        $options = $this->_options[rtrim($name, '[]')];
            
        foreach($options as $key => $choice) {
            if(is_object($choice)) {
                if(!method_exists($choice, '__toString'))
                    throw new \Fubber\CodingErrorException('The object of class '.get_class($choice).' must implement the __toString method.');
                if($value == $key)
                    $choices[] = new DomNode(new Tag('option', ['value' => $key, 'selected' => 'selected']), [new CData($choice->__toString())]);
                else
                    $choices[] = new DomNode(new Tag('option', ['value' => $key]), [new CData($choice->__toString())]);
            } else {
                if($value == $key)
                    $choices[] = new DomNode(new Tag('option', ['value' => $key, 'selected' => 'selected']), [new CData($choice)]);
                else
                    $choices[] = new DomNode(new Tag('option', ['value' => $key]), [new CData($choice)]);
            }
        }

        $res = new DomNode(new Tag('select', self::mergeAttrs($attrs, [
            "id" => $this->getId($name),
            "name" => $this->getName($name, true),
            "class" => "f-form f-select",
            ])), $choices);
        return $res;
    }
    
    /**
     * Returns <input type="hidden">-tag
     */
    public function hidden($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'hidden',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-text',
            ])));
    }
    
    /**
     * Returns <input type="text">-tag
     */
    public function text($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'text',
            'id' => $this->getId($name),
            'name' => $this->getName($name),
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-text',
            ])));
    }
    
    public function upload($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'file',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-file',
            ])));
    }
    
    /**
     * Returns <input type="color">-tag
     */
    public function color($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'color',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-color',
            ])));
    }

    /**
     * Returns <input type="date">-tag
     */
    public function date($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'date',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-date',
            ])));
    }
    
    /**
     * Returns <input type="datetime">-tag
     */
    public function datetime($name, array $attrs=[]) {
        $this->_usedFields[] = $name;

        $value = $this->getValue($name);
//var_dump($value);die();

        $value = strtotime($this->getValue($name, true));
        $datePicker = new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'date',
            'id' => $this->getId($name),
            'name' => $name.'__date',
            'value' => $value ? date('Y-m-d', $value) : '',
            'class' => 'f-form f-date',
            ])));

        $timePicker = new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'time',
            'id' => $this->getId($name),
            'name' => $name.'__time',
            'value' => $value ? date('H:i:s', $value) : '',
            'class' => 'f-form f-time',
            ])));

        $res = new DomNode(new Tag('div', ['class' => 'f-datetime-combo']), [$datePicker, $timePicker]);
        return $res;

        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'datetime',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-datetime',
            ])));
    }
    
    /**
     * Returns <input type="datetime-local">-tag
     */
    public function datetimeLocal($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'datetime-local',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-datetime-local',
            ])));
    }
    
    /**
     * Returns <input type="email">-tag
     */
    public function email($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'email',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-email',
            ])));
    }
    
    /**
     * Returns <input type="search">-tag
     */
    public function search($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'search',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-search',
            ])));
    }
    
    /**
     * Returns <input type="tel">-tag
     */
    public function tel($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'tel',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-tel',
            ])));
    }

    /**
     * Returns <input type="url">-tag
     */
    public function url($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'url',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-url',
            ])));
    }
    
    
    /**
     * Returns <input type="range">-tag
     */
    public function range($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'range',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-range',
            ])));
    }
    
    /**
     * Returns <input type="week">-tag
     */
    public function week($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'week',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-week',
            ])));
    }
    
    /**
     * Returns <input type="time">-tag
     */
    public function time($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'time',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-time',
            ])));
    }
    
    
    /**
     * Returns <input type="month">-tag
     */
    public function month($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('month', self::mergeAttrs($attrs, [
            'type' => 'month',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-month',
            ])));
    }
    
    /**
     * Returns <input type="number">-tag
     */
    public function number($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'number',
            'id' => $this->getId($name),
            'name' => $name,
            'value' => $this->getValue($name, true),
            'class' => 'f-form f-number',
            ])));
    }
    
    
    /**
     * Returns <textarea>-tag
     */
    public function textarea($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('textarea', self::mergeAttrs($attrs, [
            'id' => $this->getId($name),
            'name' => $name,
            'class' => 'f-form f-textarea'])), [new CData($this->getValue($name, true) . '')]);
    }
    
    /**
     * Returns <input type="checkbox">-tag
     */
    public function checkbox($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        $this->_optionalFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'checkbox',
            'id' => $this->getId($name),
            'name' => $this->getName($name),
            'value' => '1',
            'class' => 'f-form f-checkbox',
            ] + ($this->getValue($name, true) ? ['checked' => 'checked'] : []))));
    }
    
    public function radio($name, $value, array $attrs=[]) {
        if(!in_array($name, $this->_usedFields))
            $this->_usedFields[] = $name;
            
        if($this->getValue($name) == $value)
            $attrs['checked'] = 'checked';
            
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'radio',
            'id' => $this->getId($name, true).'_'.$value,
            'name' => $name,
            'value' => $value,
            'class' => 'f-form f-radio',
            ])));
    }
    
    public function radios($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        $choices = [];
        foreach($this->_options[$name] as $key => $choice) {
            $label = new Tag('label');
            $labelText = is_object($choice) ? new CData($choice->__toString()) : new CData($choice);
            $inputAttrs = ['id' => $name.'_'.$key, 'name' => $name, 'type' => 'radio', 'value' => $key];
            if($this->getValue($name) == $key)
                $inputAttrs['checked'] = 'checked';
            $input = new Tag('input', $inputAttrs);
            $node = new DomNode($label, [$input, $labelText]);
            $choices[] = $node;
        }

        $res = new DomNode(new Tag('fieldset', self::mergeAttrs($attrs, [
            "id" => $this->getId($name, true),
            "name" => $name,
            "class" => "f-form f-radios",
            ])), $choices);
        return $res;
    }
    
    /**
     * Returns <label for="">-tag
     */
    public function label($name, $label, array $attrs=[]) {
        /**
         * 
         * You may have a problem with id, if you're using labels and arrayed names $name[]. This
         * is probably because label does not increase the $this->counters[] value. Solution is
         * to fetch the ->label() before you fetch the ->$fieldType()
         * 
         */
        return new DomNode(new Tag('label', self::mergeAttrs($attrs, [
            'for' => $this->getId($name),
            'class' => 'f-form f-label',
            ])), [new CData($label)]);
    }
    
    public function password($name, array $attrs=[]) {
        $this->_usedFields[] = $name;
        return new DomNode(new Tag('input', self::mergeAttrs($attrs, [
            'type' => 'password',
            'id' => $this->getId($name, true),
            'name' => $name,
            'value' => '',
            'class' => 'f-form f-password',
            ])));
    }
    
    public function submit($name, $caption, array $attrs=[]) {
        if(!is_array($caption))
            $caption = [$caption];
        foreach($caption as $k => $v)
            if(is_string($v))
                $caption[$k] = new CData($v);

        return new DomNode(new Tag('button', self::mergeAttrs($attrs, ["type" => "submit", "class" => "f-form f-submit"])), $caption);
    }
    
    /**
     * Validate the properties, and return an associative array of fieldname => error. If there is no errors
     * return false
     */
    protected function isInvalid() {
       return false; 
    }
    
    public static function mergeAttrs($attrs, $defaults) {
        $res = $attrs + $defaults;
        
        foreach($defaults as $k => $v) {
            if(isset($defaults[$k]) && isset($attrs[$k])) {
                switch(strtolower($k)) {
                    case 'style' :
                        $res[$k] = $attrs[$k].';'.$defaults[$k];
                        break;
                    case 'class' :
                        $res[$k] = $attrs[$k].' '.$defaults[$k];
                        break;
                }
            }
        }
        return $res;
    }
    
    /**
     * Allows extending Form with more field types
     */
    public function __call($method, $args) {
        if(class_exists($method)) {
            // field type is a class, special case
            // check the class, then that class parents
            $className = $method;
            $hook = self::class.'.Method.'.$className;
            while(!\Fubber\Hooks::hasListeners($hook)) {
                $parent = get_parent_class($className);
                if(!$parent) {
                    $e = new \BadMethodCallException("No hooks defined for field type '$method'.");
                    $e->suggestion = "Hook into '".self::class.".Method.".$hook."'";
                    throw $e;
                } else {
                    $className = $parent;
                    $hook = self::class.'.Method.'.$className;
                }
            }
        } else {
            $hook = self::class.'.Method.'.$method;
        }
        
        if(!\Fubber\Hooks::hasListeners($hook)) {
            $e = new \BadMethodCallException("There is no field type '$method' on forms and no hook '$hook' is defined.");
            $e->suggestion = "Hook into '$hook'.";
            throw $e;
        }
        $this->_usedFields[] = $args[0];
        array_unshift($args, $hook, $this->_state, $this);
        // Trick to pass arguments by reference
        $cb = [\Fubber\Hooks::class, 'dispatchToFirst'];
        while(sizeof($args)<12) $args[] = null;
        
        // callback($state, $this, $name, $attributes)
        return $cb($args[0], $args[1], $args[2], $args[3], $args[4], $args[5], $args[6], $args[7], $args[8], $args[9], $args[10], $args[11], $args[12]);
    }
    
    protected $counters = [];
    public function hasMoreValues($name) {
        if(substr($name, -2) != '[]')
            throw new Exception("Only array fields can have more values");
        $rName = substr($name, 0, -2);
        if(!isset($this->counters[$rName]))
            $this->counters[$rName] = 0;
        $counter = $this->counters[$rName];
        $values = $this->$rName;
        if(isset($values[$counter]) !== isset($this->$rName[$counter]))
            die("MISMATCH $rName");

        if(isset($this->$rName[$counter])) {
            return true;
        }
        return false;
    }
    public function getId($name, $inc = false) {
        if(substr($name, -2) == '[]') {
            $rName = substr($name, 0, -2);
            if(!isset($this->counters[$rName]))
                $this->counters[$rName] = 0;
            $id = $rName.'_'.$this->counters[$rName];
            if($inc)
                $this->counters[$rName]++;
                
            return $id;            
        }
        return $name;
    }
    public function getValue($name, $inc = false) {
        if(substr($name, -2) == '[]') {
            $rName = substr($name, 0, -2);
            if(!isset($this->counters[$rName]))
                $this->counters[$rName] = 0;
            if(!isset($this->$rName[$this->counters[$rName]])) {
                if($inc)
                    $this->counters[$rName]++;
                return null;
            }
            $res = $this->$rName[$this->counters[$rName]];
            if($inc)
                $this->counters[$rName]++;
            return $res;
        }
        return $this->$name;
    }

    public function getName($name, $inc = false) {
        if(substr($name, -2) == '[]') {
            $rName = substr($name, 0, -2);
            if(!isset($this->counters[$rName]))
                $this->counters[$rName] = 0;
            if($inc)
                $this->counters[$rName]++;
            return $name;
            return $rName.'['.$this->counters[$rName].']';
        }
        return $name;
    }
    
    protected static function isArrayField($name) {
        return substr($name, -2)=='[]';
    }
    
    public function getOptions($name) {
        if(!isset($this->_options[$name]))
            return null;
        return $this->_options[$name];
    }
}
