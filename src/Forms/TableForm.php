<?php
namespace Fubber\Forms;

use Fubber\Kernel\State;
use Fubber\Table\{IBasicTable, IBasicTableSet};

/**
 * Generic Table Row form. Uses validation from db table object.
 */
class TableForm extends Form {
    protected $object;
    
    /**
     *      $cols = ['col1','col2','col3']
     * or
     *      $cols = ['col1' => ['value1','value2','value3']
     */
    public function __construct(State $state, IBasicTable $object, array $cols=null, $extraData = null) {
        $this->object = $object;
    
        $className = get_class($this->object);
        $id = $className."-form";
        parent::__construct($state, $id, [], $extraData);
        if(!$cols)
            $cols = $className::$_cols;

        foreach($cols as $col) {
            if(is_array($col)) {
                $vals = $col[1];
                $col = $col[0];
                $this->addField($col, $this->object->$col, $vals);
            } else
                $this->addField($col, $this->object->$col);
        }

        if($this->accept()) {
            // TODO, CHECK THAT USED FIELDS MATCH SIGNATURE
            
            foreach($cols as $col) {
                if(is_array($col)) {
                    $vals = $col[1];
                    $col = $col[0];
                }
                $this->object->$col = $this->$col;
            }
            $this->errors = $this->isInvalid();
            
            if($this->errors) {
                $html = "<div>Validation Errors";
                foreach($this->errors as $k => $v) {
                    $html .= "<br><small>$k: $v</small>";
                }
                $html .= "</div>";
                new \Fubber\FlashMessage($state, $html, \Fubber\FlashMessage::ERROR);
            } else {
                $this->success = !!$this->object->save();
            }
        }
    }
    
    public function isInvalid(): ?array {
        return $this->object->isInvalid();
    }
    
    public function end() {
        return parent::end();
        die("SHOULD ADD HASH SIGN OF USED FIELDS IN FORM, TO AVOID HACKING");
    }
}
