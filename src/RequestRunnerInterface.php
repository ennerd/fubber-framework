<?php
namespace Fubber;

use Psr\Http\Server\RequestHandlerInterface;

/**
 * Implementations provide an abstraction between the client/source
 * of a request and Fubber Framework. One example is an abstraction
 * for sending a request
 * 
 * @package Fubber
 */
interface RequestRunnerInterface {

    /**
     * Process requests and invoke the handler. Keep processing requests
     * until {@see RequestDispatcherInterface::stop()} is called.
     * 
     * @param float|null $maxRunTime 
     */
    public function run(RequestHandlerInterface $handler): void;

    /** 
     * Stop processing further requests after the current request
     * completes.
     */
    public function stop(): void;
}