<?php
namespace Fubber\Actions;

use Fubber\I18n\Translatable;
use Fubber\Kernel;
use Psr\Log\LoggerInterface;
use Throwable;

/**
 * This abstract class is empty, but it may be helpful in the
 * future if the Action API evolves.
 */
abstract class Action implements IAction {

    /**
     * Execute the action.
     *
     * @return boolean
     */
    public final function invoke(): bool {
        if ($this->isInvalid() === null) {
            return $this->execute();
        }
        return false;
    }

    protected final function getLogger(): LoggerInterface {
        return Kernel::init()->getLogger(static::class);
    }

    public final function logException(Throwable $error): void {
        $this->getLogger()->error($error->getMessage(), [
            'code' => $error->getCode(),
            'line' => $error->getLine(),
            'file' => $error->getFile(),
            'trace' => $error->getTraceAsString(),
        ]);
    }

    public final function logResult(Translatable|string $message): void {
        if ($message instanceof Translatable) {
            $vars = $message->getVars();
            $vars['_textDomain'] = $message->getTextDomain();
            $this->getLogger()->info($message->getMessage(), $vars);
        } else {
            $this->getLogger()->info($message);
        }
    }

    /**
     * Implement the logic to run the function.
     *
     * @return boolean True if the action was successfully performed.
     */
    abstract protected function execute(): bool;
    
}