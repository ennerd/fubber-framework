<?php
namespace Fubber\Actions;

use Fubber\I18n\Translatable;
use Serializable;

/**
 * Interface for describing and performing actions and mutations.
 * 
 * Writing an action class in not required for every operation possible,
 * but action classes have additional benefits for writing code that can
 * be accessed via APIs, scheduled to run asynchronously in a queue and
 * also easily run directly in your code.
 * 
 * Actions should NOT perform any access checking except in the constructor.
 * Once an action has been created, it can be serialized and stored in a
 * queue to run at a later time.
 */
interface IAction {

    /**
     * Perform the described action and return a result. If the action
     * is unsuccessful an exception MUST be thrown.
     *
     * @throws \Throwable
     * @return mixed
     */
    public function invoke(): bool;

    /**
     * Check that the action is legal to perform. The returned integer
     * can be used via {@see static::describe()} to 
     */
    public function isInvalid(): ?Translatable;
}