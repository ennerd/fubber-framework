<?php
declare(strict_types=1);

namespace Fubber\I18n;

use Fubber\Kernel\State;

interface II18n {

    /**
     * Create an instance based on an HTTP Accept-Language formatted
     * string
     */
    public static function fromAcceptLanguageHeader(string $header): II18n;

    /**
     * Translate a string to the requested language
     *
     * @param $string       The original text to be translated. Normally only english text, unless you develop your application in a different language.
     * @param $vars         Variables that can be replaced inside the string. These won't be translated.
     * @param $textDomain   Path to the source file that contains the string, or another string used to group the translatable strings together.
     */
    public function translate(TranslatableInterface $message): string;
}
