<?php
namespace Fubber\I18n;

use Fubber\I18n\Abstract\TranslatablePrimitive;
use Fubber\Kernel;
use Fubber\Kernel\Phases\Phase;
use JsonSerializable;

use function Fubber\kernel;

/**
 * A string which can be serialized and translated when needed.
 */
final class Translatable extends TranslatablePrimitive implements TranslatableInterface {

    private readonly string $message;
    private readonly array $vars;
    private readonly string $file;
    private readonly ?string $description;
    private bool $wasUsed = false;

    public function __construct(string $message, array|JsonSerializable $vars=[], string $file=null, string $description=null) {
        $this->message = $message;
        $this->vars = \is_array($vars) ? $vars : (array) $vars->jsonSerialize();
        $this->description = $description;
        $file = $file ?? \debug_backtrace(false, 1)[0]['file'];
        $this->file = self::$pathAliases[$file] ?? $file;

        Kernel::instance()->phases->onExitedState(Phase::Ready, function() {
            if (!$this->wasUsed) {
                $this->__toString();
            }
        });
    }

    public function getMessage(): string {
        return $this->message;
    }

    public function getVars(): array {
        return $this->vars;
    }

    public function getFile(): string {
        return $this->file;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function __toString() {
        $this->wasUsed = true;
        return I18n::getCurrent()->translate($this);
    }

    public function jsonSerialize() {
        return $this->__toString();
    }

}