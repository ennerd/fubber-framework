<?php
namespace Fubber\I18n\Abstract;

use Fubber\I18n\TranslatableInterface;

abstract class TranslatablePrimitive {

    /**
     * Path aliases
     * 
     * @var array
     */
    protected static array $pathAliases = [];

    /**
     * Add path aliases for when a source file has been translated (or "compiled")
     * for example in a template engine. This ensures that the translation is associated
     * correctly.
     * 
     * @param string $fakePath 
     * @param string $realPath 
     */
    public final static function addPathAlias(string $fakePath, string $realPath): void {
        self::$pathAliases[$fakePath] = $realPath;
    }

}