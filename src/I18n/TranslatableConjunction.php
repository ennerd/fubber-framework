<?php
namespace Fubber\I18n;

use Fubber\CodingErrorException;
use Fubber\I18n\Abstract\TranslatablePrimitive;
use Fubber\Kernel;
use Fubber\Kernel\Phases\Phase;
use JsonSerializable;

use function Fubber\kernel;

/**
 * Annotations in translatable strings:
 * 
 *  - referencing variables and properties of arrays and objects
 * 
 *      `user_name` `user.first_name` `user.department.location`
 *      `users.0` // array item where key is 0 
 * 
 *  - list arrays (https://www.php.net/array_is_list)
 * 
 *      `users[0]` first element
 *      `users[-1]` last element
 *      `users[1000]` (if the index does not exist, the value is null)
 *      `users[0..0]` equivalent to \array_slice($users, 0, 1)
 *      `users[-1..0]` a reversed list from last element to first
 *      `users[-10..-1]` the 10 last elements of the list
 * 
 * 
 *  - referencing list members (only simple numeric 0-based arrays can be used for numeric indexes)
 * 
 *      `users[0]` `users[-1]`
 * 
 *      // strings, integers, numbers
 *      `Welcome, {user_name}!` // simple variable, works with strings and numbers only
 * 
 *      // arrays with numeric indexes
 *      `First item: {array[0]} Last item: {array[-1]}`
 *      `Nested item: {array[0][4]}`
 * 
 *      // objects and arrays with string keys
 *      `{user.first_name}`
 *      `{user.department.name}`
 * 
 *      // combined
 *      `{user.coworkers[0]}
 * 
 *      // counting the size of an array or number of properties of an object
 *      `{user|count}`
 * 
 *      // last 10 elements of an array
 *      `{users[-10.. -1]}`
 * 
 *      // an array containing the first_name property of the first 5 elements of an array
 *      `{users[..5].first_name}`
 * 
 *      // an array containing every even value of an array
 *      `{users[%2]}`
 *      // An array containing every third value of an array, starting with index 1
 *      `{users[1..][%3]}`
 *      // An array containing element number 10, 12, 14
 *      `{users[%2][5..]}
 * 
 *  - Simple variables in a translatable string is inserted directly:
 *    
 *    "Hello {name}"
 * 
 *  - if($value)
 *    "Value is <value?>truthy<!>
 * 
 *  - if($value==1) elseif ($value==2) elseif ($value==3) else 
 * 
 *    "Value is <value=1?>one<value=2>two<value=3>three<??>{value}<!>"
 * 
 *  - <loop values as value>{value}
 * 
 * 
 *  - Numbers can be formatted
 * 
 *    "You have <amount|number_format(2, " ", ".")>"
 * 
 *  - Existence/truthyness of variables can be tested; true is anything except '', 0, false and null.
 * 
 *    "There are <people_count?>{people_count}<!>nobody<?> here"
 * 
 *  - Arrays can be indexed
 * 
 *    "First item is {some_array[0]} and last item is {some_array[-1]}"
 * 
 *  - Arrays can be sliced
 * 
 *    "<names[0..-1]|each><!>
 *    "I want to say welcome to <names[1]?>{names[0..-1]|join(", ")} and <?>{names[-1]}"
 *    - If names[1] exists, join elements 0 .. -1 with ", "
 * 
 *  - Arrays can be counted
 * 
 *    "There are {names|count} names"
 * 
 *  - Terms can be pluralized
 * 
 *    "There {people_count=0?}are nobody{=1?}is one{=2}are two{!}{people_count}{?}
 * 
 * A string which can be serialized and translated when needed.
 */
final class TranslatableConjunction extends TranslatablePrimitive {

    /**
     * Each clause of the conjunction, if any clauses are added
     * 
     * @var TranslatableInterface[]
     */
    private array $clauses;

    /**
     * The conjunction to use between clauses - including spaces, such as < and >, < or >, < but not > etc.
     * 
     * @var TranslatableInterface
     */
    private readonly TranslatableInterface $conjunction;

    /**
     * The string to use if no clauses are added
     * 
     * @var TranslatableInterface
     */
    private readonly TranslatableInterface $none;

    /**
     * The template variables which will replace variables encoded with {varName}
     * 
     * @var array
     */
    private readonly array $vars;

    /**
     * The file name where this string originated from. Extracted using {@see \debug_backtrace()}
     * if not provided.
     * 
     * @var string
     */
    private readonly string $file;

    /**
     * @param string $conjunction The conjunction to use including spacing - such as < and >, < or >, <, but not >
     * @param string|array $none The string to use if zero words, phrases or clauses are added with {@see self::add()}. 
     * @param array|JsonSerializable $vars 
     * @param string|null $file 
     * @throws CodingErrorException 
     */
    public function __construct(string $conjunction, string $none, array|JsonSerializable $vars=[], string $file=null) {
        $file = $file ?? \debug_backtrace(false, 1)[0]['file'];
        $this->file = self::$pathAliases[$file] ?? $file;
        $this->conjunction = new Translatable($conjunction, $vars, $this->file);
        $this->none = new Translatable($none, $vars, $this->file);
        $this->vars = \is_array($vars) ? $vars : (array) $vars->jsonSerialize();
    }

    public function add(Translatable|string ...$clause): void {
        $file = \debug_backtrace(false, 1)[0]['file'];
        foreach ($clause as $c) {
            if (\is_string($c)) {
                $this->clauses[] = new Translatable($c, $this->vars, $file);
            } else {
                $this->clauses[] = $c;
            }
        }
    }

    public function getMessage(): string {
        return $this->message;
    }

    public function getVars(): array {
        return $this->vars;
    }

    public function getFile(): string {
        return $this->file;
    }

    public function __toString() {
        $count = \count($this->clauses);
        if ($count === 0) {
            return $this->none;
        } elseif ($count === 1) {
            return $this->clauses[0];
        } else {
            $lastClause = $this->clauses[$count - 1];
            /**
             * @todo We probably can't use <, > as a delimiter in all languages; must find a
             * way to encode variations in the translation string. Probably there should be
             * some annotation like {clause, ...}
             */
            $clauses = \implode(", ", \array_slice($this->clauses, $count - 1));
            return $clauses . $this->conjunction . $lastClause;
        }
    }

    public function jsonSerialize() {
        return $this->__toString();
    }

}