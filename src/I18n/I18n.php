<?php
namespace Fubber\I18n;

use Fubber\Kernel;

use function Fubber\state;

class I18n implements II18n {

    public readonly array $languages;
    protected readonly Translations $translations;

    protected static ?II18n $defaultLocale = null;
    protected static ?II18n $activeLocale = null;
    protected static $domainCache = [];

    public static function fromAcceptLanguageHeader(string $header): II18n {
        return new self($header);
    }

    public static function getCurrent(): II18n {
        return state()?->getLocale() ?? self::getDefaultLocale();;
    }

    public static function getDefaultLocale(): II18n {
        if (self::$defaultLocale) {
            return self::$defaultLocale;
        }
        return self::$defaultLocale = new self('default');
    }

    protected function injectVaryAcceptLanguage(): void {
        $state = state();
        if (!isset($state->cache['VaryAcceptLanguage'])) {
            $state->response->addHeader('Vary', 'Accept-Language');
            $state->cache['VaryAcceptLanguage'] = true;
        }
    }

    protected function __construct(string $acceptLanguage) {
        $this->translations = Translations::getInstance();

        // sort according to q
        $languages = \array_map(function($s) {
            $parts = \explode(";", $s);
            if (!isset($parts[1])) {
                $weight = 1.0;
            } else {
                $p = \strpos($parts[1], 'q=');
                if ($p === false) {
                    $weight = 1.0;
                } else {
                    $weight = \floatval(\substr($parts[1], $p+2));
                }
            }
            return [\trim($parts[0]), $weight];
        }, explode(",", $acceptLanguage));

        // filter away any languages we don't support
        $validLanguages = Translations::getInstance()->getEnabledLanguageCodes();

        usort($languages, function($a, $b) {
            if ($a[1] > $b[1]) return -1;
            else if ($a[1] < $b[1]) return 1;
            else return 0;
        });

        $languages = \array_reduce($languages, function(array $carry, array $item) use ($validLanguages) {
            if (\in_array($item[0], $validLanguages)) {
                $carry[$item[0]] = $item[0];
            }
            foreach ($validLanguages as $validLanguage) {
                if (\str_starts_with($validLanguage . '-', $item[0])) {
                    $carry[$validLanguage] = $validLanguage;
                    break;
                }
            }
            return $carry;
        }, []);

        $this->languages = \array_values($languages);
    }

    public function getPrimaryLanguageCode(): string {
        return $this->languages[0];
    }


    public function translate(TranslatableInterface $message): string {
        if (state()) {
            $this->injectVaryAcceptLanguage();
        }
        //Kernel::debug("FRODE: Getting translation for `".$message->getMessage()."` file `".$message->getFile()."`");
        $tString = $this->translations->get($message->getFile(), $this->languages, $message->getMessage());
        $map = [];
        foreach ($message->getVars() as $k => $v) {
            $map['{'.$k.'}'] = $v;
            $map[':'.$k] = $v;
        }
        return \strtr($tString, $map);
    }

}
