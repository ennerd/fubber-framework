<?php
namespace Fubber\I18n;

use Fubber\Kernel;
use Fubber\LogicException;

use function Fubber\config;
use function Fubber\environment;
use function Fubber\kernel;

class Translations implements TranslationsInterface {

    private static ?Translations $instance = null;

    public static function getInstance(): self {
        if (self::$instance === null) {
            $languages = config()->load('language-codes', false, ['en']);
            $root = environment()->appRoot;
            self::$instance = new self($languages, $root . '/translations');
        }
        return self::$instance;
    }

    protected array $translations = [];
    protected array $shortenedCache = [];
    protected readonly array $enabledLanguageCodes;
    protected readonly string $defaultLanguageCode;

    protected function __construct(
        array $enabledLanguageCodes, 
        protected readonly string $translationsPath
    ) {        
        $this->enabledLanguageCodes = $enabledLanguageCodes;
        $this->defaultLanguageCode = environment()->lang;
    }

    /**
     * Enabled language codes are intersected with the requested list
     * of language codes.
     * 
     * Enabled language codes are generally configured via an array
     * `['no', 'de', 'se', 'en']` in `config/language-codes.php`,
     * while `environment()->lang` (DEFAULT_LANG) provides the default
     * language code and falls back to `en` if it is not configured.
     */
    public function getEnabledLanguageCodes(): array {
        return $this->enabledLanguageCodes;
    }

    /**
     * The default language code is fetched from environment variables
     * via `environment()->lang`. The environment variable is named
     * `DEFAULT_LANG` and falls back to `en` if it is not set.
     */
    public function getDefaultLanguageCode(): string {
        return $this->defaultLanguageCode;
    }

    public function getTranslationsPath(): string {
        return $this->translationsPath;
    }

    /**
     * Get a particular translation for a string in a file, given a language code.
     */
    public function get(string $filename, array $languageCodes, string $originalString): string {

        $defaultLanguageCode = $this->getDefaultLanguageCode();

        if (empty($languageCodes)) {
            $languageCodes[] = $defaultLanguageCode;
            //throw new \LogicException("Trying to load a translation for zero languageCodes");
        }
        Kernel::debug("get({filename}, {languageCodes}, {string})", [
            'filename' => $filename,
            'languageCodes' => \json_encode($languageCodes),
            'string' => $originalString,
        ]);
        $hash = self::hash($originalString);

        // Ensure we always load the original language; it is used for checking that we have a translated version
        if (!isset($this->translations[$defaultLanguageCode][$filename])) {
            $this->loadTranslations($filename, $defaultLanguageCode);
        }

        // Ensure that the string is available in the default language
        if (!isset($this->translations[$defaultLanguageCode][$filename][$hash])) {
            $this->translations[$defaultLanguageCode][$filename][$hash] = $originalString;
            foreach ($this->getPaths($filename, $defaultLanguageCode) as $path) {
                $this->saveArray($path, $this->translations[$defaultLanguageCode][$filename]);
            }
        }

        // Check for a translated version of this string
        foreach ($languageCodes as $language) {
            // Stop if we have arrived at the default language
            if ($language === $defaultLanguageCode) {
                return $originalString; // .'('.$defaultLanguageCode.')';
            }

            if (!in_array($language, $this->enabledLanguageCodes)) {
                throw new LogicException("Language code `$language` is not enabled and should not have been requested");
            }

            // Load the language files if we haven't already
            if (!isset($this->translations[$language][$filename])) {
                $paths = $this->getPaths($filename, $language);
                $localTranslations = $this->loadArray($paths[0]);
                if (isset($paths[1])) {
                    $externalTranslations = $this->loadArray($paths[1]);
                    // update the local translations with any new strings
                    $localTranslationsWasChanged = false;
                    foreach ($externalTranslations as $k => $v) {
                        if (!isset($localTranslations[$k])) {
                            $localTranslations[$k] = $v;
                            $localTranslationsWasChanged = true;
                        }
                    }
                    if ($localTranslationsWasChanged) {
                        $this->saveArray($paths[0], $localTranslations);
                    }
                }
                $this->translations[$language][$filename] = $localTranslations;
            }

            // Add the string to the translation if it doesn't already exist
            if (!isset($this->translations[$language][$filename][$hash])) {
                $this->translations[$language][$filename][$hash] = $originalString;
                $paths = $this->getPaths($filename, $language);
                $this->saveArray($paths[0], $this->translations[$language][$filename]);
                if (isset($paths[1])) {
                    $this->saveArray($paths[1], $this->translations[$language][$filename]);
                }
            }

            // If we have a translation then we can return it
            if ($this->translations[$language][$filename][$hash] !== $originalString) {
                return $this->translations[$language][$filename][$hash]; // . '('.$language.')';
            }
        }

        return $originalString; // .'('.$defaultLanguageCode.')';
    }

    /**
     * Create a short hash for the string.
     * 
     * @param mixed $string 
     * @return string 
     */
    protected static function hash($string) {
        // Keeps the first and last characters of each word, and removes any whitespace
        return \preg_replace('/\W+|(?<=\w{3})[\w]+(?=\w{2})/u', '', $string);
    }

    protected function loadTranslations(string $filename, string $language): array {
        if (!\in_array($language, $this->enabledLanguageCodes)) {
            throw new LogicException("Language `" . $language . "` is not enabled. Don't try to fetch it.");
        }

        $translations = [];

        foreach ($this->getPaths($filename, $language) as $path) {
            $foundTranslations = $this->loadArray($path);
            if ($foundTranslations !== null) {
                $translations += $foundTranslations;
            }
        }

        return $this->translations[$language][$filename] = $translations;
    }

    /**
     * Save translations to a file.
     * 
     * @param string $filename 
     * @param array $data 
     * @param bool $safe If true, then we will only add new translations to the file - not overwrite existing translations.
     */
    protected function saveArray(string $filename, array $data, bool $safe = true): void {
        if ($safe && \file_exists($filename)) {
            $existingTranslations = $this->loadArray($filename);
            $data = $existingTranslations + $data;
        }
        $dir = \dirname($filename);
        if (!\is_dir($dir)) {
            $m = \umask(0);
            \mkdir($dir, 0755, true);
            \umask($m);
        }
        Kernel::debug("Writing translations to {path}", ['path' => $filename]);
        \file_put_contents($filename, '<?php return ' . \var_export($data, true) . ";\n");
    }

    /**
     * Load translations from a file
     */
    protected function loadArray(string $filename): ?array {
        if (!\is_file($filename)) {
            Kernel::debug("Found no translations in {path}", ['path' => $filename]);
            return null;
        }
        Kernel::debug("Found translations in {path}", ['path' => $filename]);
        return require($filename);
    }

    /*
    protected function saveTranslations(string $filename, string $language): void {
        foreach ($this->getPaths($filename, $language) as $path) {
            var_dump($path);die();
            $this->saveArray($paths[0], $this->translations[$language][$filename]);
        }
    }
    */

    /**
     * Returns 1 or 2 paths for where the translation should be stored. If 2 paths
     * are returned, the 2nd path is translations from an external bundle or a composer package
     */
    protected function getPaths(string $filename, string $languageCode): array {
        $shortPath = $this->shorten($filename);
        if ($shortPath[0] === null) {
            return [ $this->translationsPath . \DIRECTORY_SEPARATOR . $shortPath[1] . '.'.$languageCode.'.php' ];
        } else {
            return [
                $this->translationsPath . \DIRECTORY_SEPARATOR . $shortPath[1] . '.' . $languageCode . '.php',
                $shortPath[0][0] . \DIRECTORY_SEPARATOR . $shortPath[0][1] . '.' . $languageCode . '.php',
            ];
        }
    }

    protected function shorten(string $filename): array {
        if(isset($this->shortenedCache[$filename])) {
            // Caching text domain transformations, because they can be many per file
            return $this->shortenedCache[$filename];
        } else {
            $key = $filename;
            if($filename[0] !== '/') {
                throw new LogicException("Require absolute path");
            }
            // removing trailing path and extension
            foreach (static::getRoots() as $root => $info) {
                if (\str_starts_with($filename, $root)) {
                    if ($info[1] === 'FILECACHE') {
                        throw new \Exception("Won't translate temp file");
                    }
                    return $this->shortenedCache[$key] = [
                        $info[2] ? [ $info[2], \substr($filename, $info[0] + 1) ] : null,
                        $info[1] . \substr($filename, $info[0])
                    ];
                }
            }
            throw new LogicException("Filename `$filename` is outside of acceptable paths");
        }
    }

    /**
     * Returns an array of path aliases to make translations independent
     * of the application installation path. The aliases contain the number
     * of characters to remove from the original path, and then the new prefix.
     */
    protected static function getRoots(): array {
        static $roots;

        if ($roots) {
            return $roots;
        }

        $env = environment();

        $roots = [
            $env->root => [\strlen($env->root), 'ROOT', null],
            $env->appRoot => [\strlen($env->appRoot), 'APP', null],
            $env->privRoot => [\strlen($env->privRoot), 'PRIV', null],
            $env->configRoot => [\strlen($env->configRoot), 'CONFIG', null],
            $env->fileCacheRoot => [\strlen($env->fileCacheRoot), 'FILECACHE', null],
            $env->viewsRoot => [\strlen($env->viewsRoot), 'VIEWS', null],
            Kernel::getFrameworkPath() => [\strlen(Kernel::getFrameworkPath()), 'FUBBER', Kernel::getFrameworkPath() . '/translations' ],
        ];


        foreach (kernel()->bundles as $name => $manifest) {
            $roots[$manifest->path] = [ \strlen($manifest->path), $name . '.bundle', $manifest->path . '/translations' ];
        }

        \uasort($roots, function($a, $b) {
            if ($a[0] < $b[0]) { return 1; }
            elseif ($a[0] > $b[0]) { return -1; }
            return 0;
        });

        $commonRootParts = \explode("/", $commonRoot = \key($roots));
        $l = \strlen($commonRoot);
        foreach ($roots as $path => $id) {
            if (\substr($path, 0, $l) !== $commonRoot) {
                foreach (\explode("/", $path) as $k => $part) {
                    if ($k >= \sizeof($commonRootParts) || $commonRootParts[$k] !== $part) {
                        $commonRootParts = \array_slice($commonRootParts, 0, $k);
                        $commonRoot = \implode("/", $commonRootParts);
                        $l = \strlen($commonRoot);
                    }
                }
            }
        }

        $roots[$commonRoot] = [$l, 'TOP'];
        return $roots;
    }
}