<?php
namespace Fubber\I18n;

use Fubber\{Kernel, Service, PlainView};
use Fubber\Kernel\AbstractController;
use Fubber\Kernel\State;
use PDO;
use Psr\Http\Message\ResponseInterface;

class I18nController extends AbstractController {

    public function init(Kernel $kernel): void {
        $kernel->router->get('/fubber/i18n/', $this->index(...), 'fubber_i18n');
    }

    public function index(): ResponseInterface {
        return $this->view('fubber/i18n/index');
    }
    

    public static function languages(State $state) {



        $get = $state->get;
        $post = $state->post;

        $page = isset($get['page']) ? $get['page'] : 'main';
        $page = 'page_'.$page;
        if(method_exists(static::class, $page)) {
            return static::$page($state);
        } else {
            return static::page_main($state);
        }
    }
    
    protected static function db() {
        return Service::use(PDO::class);
//        return \Nerd\Glue::get(\PDO::class);
    }
    
    protected static function q($s, $v=[], $opts=\PDO::FETCH_ASSOC){
        $db = static::db();
        $stmt = $db->prepare($s);
        $stmt->execute($v);
        return $stmt->fetchAll($opts);
    }
    
    protected static function e($s, $v=[]) {
        $db = static::db();
        $stmt = $db->prepare($s);
        return $stmt->execute($v);
    }
    
    protected static function page_main(State $state) {
        $langs = static::q('SELECT DISTINCT locale FROM ff_i18n_trans');
        ob_start(); ?>
        
        <table>
            <thead><tr><th>Locale</th></tr></thead>
            <tbody>
                <?php foreach($langs as $lang) { ?>
                <tr>
                    <td><a href='?page=locale&locale=<?=rawurlencode($lang['locale']); ?>'><?=htmlspecialchars($lang['locale']); ?></a></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
        <?php 
        $contents = ob_get_contents(); ob_end_clean();
        return new PlainView(static::wrap("Main", $contents));
    }
    
    protected static function page_locale(State $state) {
        $showAll = false;
        $get = $state->get;
        $post = $state->post;
        if(isset($post['hash'])) {

            $orig = static::q("SELECT * FROM ff_i18n_trans WHERE locale=? AND hash=? AND text_domain=?", [
                Kernel::$instance->env->lang,
                $post['hash'],
                $post['text_domain'],
            ])[0];

            static::e('INSERT INTO ff_i18n_trans (text_domain, locale, hash, string, ts) VALUES (?, ?, ?, ?, NOW()) ON DUPLICATE KEY 
                UPDATE string=?', [
                $post['text_domain'],
                $get['locale'],
                $orig['hash'],
                $post['string'],
                $post['string'],
                ]);
            die("OK");
        }
        $translations = [];
        foreach (static::q('SELECT hash,string,text_domain FROM ff_i18n_trans WHERE locale=?', [$state->get['locale']]) as $trans) {
            $translations[$trans['text_domain'].':'.$trans["hash"]] = $trans;
        }
        $defaults = [];
        foreach (static::q('SELECT hash,string,text_domain FROM ff_i18n_trans WHERE locale=?', [Kernel::$instance->env->lang]) as $orig) {
            $defaults[$orig['text_domain'].':'.$orig['hash']] = $orig;
        }
        ob_start(); ?>
        
        <table>
            <thead><tr><th>key</th><th>String</th></tr></thead>
            <tbody>
                <?php foreach($defaults as $default) {
                    $trans = $translations[$default['hash']] ?? null;
                    if (!$showAll && $trans && $trans['string'] !== $default['string']) {
                        continue;
                    }
                    if ($trans === null) {
                        $trans = $default;
                    }
                ?>
                <tr>
                    <td><?=htmlspecialchars($default['hash']); ?></a></td>
                    <td>
                        <textarea placeholder="<?=htmlspecialchars($default['hash']); ?>" id="<?=htmlspecialchars($default['hash']); ?>" data-textdomain="<?=htmlspecialchars($default['text_domain']); ?>"><?=htmlspecialchars($trans['string']); ?></textarea>
                        <div class="details">
                            <small><?=htmlspecialchars($default['string']); ?></small><br>
                            <em><?=htmlspecialchars($trans['text_domain']); ?></em>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <script>
            $("body").on('change', 'textarea', function() {
                $.post(null, { hash: $(this).attr('id'), text_domain: $(this).attr('data-textdomain'), string: $(this).val() });
            });
            $("body").on("focus", "textarea", function() {
                $(this).select();
            });          
        </script>
        
        <?php 
        $contents = ob_get_contents(); ob_end_clean();
        return new PlainView(static::wrap("Main", $contents));
    }
    
    protected static function wrap($title, $contents) {
        $contents = <<<EOD
<!DOCTYPE html>
<html>
    <head>
        <title>$title</title>
        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    </head>
    <body>
        <style>
            body {
                font-family: 'Roboto', sans-serif;
            }
            table {
                width: 100%;
            }
            td {
                vertical-align: top;
                padding-bottom: 10px;
            }
            td:first-child {
                width: 1px;
            }
            textarea {
                width: 100%;
                border: 0;
            }
            textarea + .details {
                display: none;
            }
            textarea:focus + .details {
                display: block;
            }
        </style>
$contents
    </body>
</html>
EOD;
        return $contents;
    }
    
}
