<?php
namespace Fubber\I18n;

use JsonSerializable;
use Stringable;

/**
 * Encapsulates a message for later translation. This is useful in situations
 * where you need to serialize a message and present it later without knowing
 * any localization requirements in advance - for example with logging.
 */
interface TranslatableInterface extends Stringable, JsonSerializable {

    public function getMessage(): string;
    public function getVars(): array;
    public function getFile(): string;
    public function getDescription(): ?string;

}