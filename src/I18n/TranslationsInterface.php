<?php
namespace Fubber\I18n;

/**
 * Interface for providing translation strings to I18n.
 * 
 * @package Fubber\I18n
 */
interface TranslationsInterface {

    /**
     * Return the best translation available for a given original string. If no translation is
     * available, returns the original string.
     * 
     * @param string $filename 
     * @param array $languageCodes 
     * @param string $originalString 
     * @return string 
     */
    public function get(string $filename, array $languageCodes, string $originalString): string;
    
}