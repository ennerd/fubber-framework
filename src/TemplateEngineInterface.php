<?php
namespace Fubber;

use Fubber\Microcache\MicrocacheInterface;

interface TemplateEngineInterface {

    /**
     * Generate a templated result.
     * 
     * @param string $view The view name as requested by the caller without extension and path
     * @param array $vars The variables used by the caller
     * @param string $pathHint If available, the actual template path may be provided here if it is known - to short circuit template path resolution
     * @return TemplateInterface 
     */
    public function render(string $view, array $vars, string $pathHint=null): TemplateInterface;

    /**
     * Get the filename extensions that this template engine supports
     * 
     * @return array 
     */
    public function getExtensions(): array;
}