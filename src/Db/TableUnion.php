<?php
namespace Fubber\Db;

trigger_error('Class ' . TableUnion::class . ' is deprecated. ' . \Fubber\Table\TableUnion::class .' is a drop in replacement.', E_USER_DEPRECATED);

class TableUnion extends \Fubber\Table\TableUnion {}