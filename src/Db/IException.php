<?php
declare(strict_types=1);

namespace Fubber\Db;

/**
 * An interface for grouping all exceptions originating
 * in the Fubber\Db namespace.
 */
interface IException {}
