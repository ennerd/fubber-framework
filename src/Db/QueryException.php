<?php
namespace Fubber\Db;

use Fubber\CodingErrorException;

/**
*	Exception thrown whenever a query fails
*/
class QueryException extends CodingErrorException implements IException {

    public function jsonSerialize() {
        return ['error' => 'Problem with server-side query', 'message' => $this->getMessage(), 'code' => $this->getCode()];
    }
}
