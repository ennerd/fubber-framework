<?php
namespace Fubber\Db;

use Fubber\Service;
use Fubber\Parsing\StringParser;
use Fubber\ValidationException;
use Fubber\CodingErrorException;
use Fubber\Kernel\State;
use Fubber\LazyProp;
use Fubber\WrappedException;
use Fubber\Table\IBasicTable;
use Fubber\Table\IBasicTableSet;
/**
*	Represents tabular data from any database backend.
*/
abstract class DbTable implements IBasicTable {
	use LazyProp;

    /**
     * @var string
     */
	public static $_table = NULL;
	// ONLY SET THIS IF USING MULTIPLE COLUMN PRIMARY KEYS. The $_cols[0] will otherwise be considered the primary key
	public static $_primaryKey = NULL;
	public static $_cols = NULL;
	public static $_foreignKeys = [];
	protected static $_triedUpdateSchema = false;

	/**
	 * Internal property that records if this post was fetched from the DB. If
	 * it was fetched from the DB, this holds the primary keys.
     * 
     * @internal
	 */
	public $_fromDb = null;

	protected static $_debugNext = 0;

	/**
	 * This method is intended to be overridden - so that you can seed the object. The constructor
	 * is not suitable for this - since PDO calls the constructor with no arguments when loading
	 * objects from the database.
	 */
    public static function create(?State $state) {
		return new static();
	}


/*
    // belongs to DbTableSet
    public function parseQuery(array $get) {
        foreach (static::$_cols as $col) {
            if (isset($get[$col])) {
                $val = $get[$col];
                if (is_array($val)) {
                    foreach ($val as $op => $v) {
                        $this->where($col, $op, $v);
                    }
                } else {
                    $this->where($col, '=', $val);
                }
            }
        }
        if (isset($get['l'])) {
            if (intval($get['l']) > 1000) {
                throw new \Fubber\BadRequestException("Not possible to fetch more than 1000 rows");
            }
            if (isset($get['o'])) {
                $this->limit(intval($get['l']) && intval($get['o']));
            } else {
                $this->limit(intval($get['l']));
            }
        } elseif (isset($get['o'])) {
            throw new \Fubber\BadRequestException("Not possible to fetch with an offset, unless also fetching with a limit");
        }

        if (isset($get['s'])) {
            $reverse = $get['s'][0] === '-';
            if ($reverse) {
                $c = substr($get['s'], 1);
                if (in_array($c, static::$_cols)) {
                    $this->order($c, true);
                }
            } elseif (in_array($get['s'], static::$_cols)) {
                $this->order($get['s']);
            }
        }

    }
*/

	public function jsonSerialize() {
		$res = [];
		foreach(static::$_cols as $col) {
			$res[$col] = $this->$col;
		}
		return $res;
	}

	/**
	 *  Interface for querying arbitrary date from various backends.
	 *
	 *  Table::q('SELECT nickname FROM User'); // All User rows, but we only fetch the nickname column.
	 *
	 *  Table::q('User'); // All User rows
	 *
     *  Table::q('User WHERE id>?', [100]);
     *
	 *  @param string $query
	 *  @param array $vars
	 *  @return TableSet Returns
	 */
	public static final function q($query, array $vars=array()) {
		// Table::q('User WHERE id=?')
		$p = new StringParser($query, TRUE);
		$table = $p->consume();
		$cols = null;
		if($p->type === StringParser::CONSUMED_WORD && $table === 'SELECT') {
			$cols = array();
			// This is a special query, selecting only a specific column or more
			while(TRUE) {
				$col = $p->consume();
				if($p->type !== StringParser::CONSUMED_WORD) {
                    throw new QueryException("Expects string column names after SELECT keyword, separated by commas.");
                }
				$cols[] = $col;
				$peek = $p->consume();
				if($p->type !== StringParser::CONSUMED_SYMBOLS || $peek !== ',') {
					$p->push($peek);
					break;
				}
			}
			$from = $p->consume();
            if($p->type !== StringParser::CONSUMED_WORD || $from !== 'FROM') {
                throw new QueryException("Expects FROM keyword after SELECT clause.");
            }
            $table = $p->consume();
		}
		if(!$table) throw new QueryException("Empty query");
		if($p->type !== StringParser::CONSUMED_WORD) throw new QueryException("Expected class name or SELECT keyword.");
		if(!class_exists($table)) throw new QueryException("Class '".$table."' not found.");
		if(!is_subclass_of($table, 'Table')) throw new QueryException("Class '".$table."' must extend Table");

		$rows = $table::all();

		if($cols !== null) {
			$rows->fields(implode(",", $cols));
		}

		$wheres = array();

		while($word = $p->consume()) {
			if($p->type !== StringParser::CONSUMED_WORD) throw new QueryException("Expects keyword after class name.");
			switch($word) {
				case 'WHERE' :
					while(true) {
						// Consume WHERE parts
						$col = $p->consume();
						if($p->type !== StringParser::CONSUMED_WORD) throw new QueryException("Expects column name.");
//						if(!in_array($col, $table::$_cols)) throw new QueryException("Column '".$col."' not found in '".$table."'.");
						$op = $p->consume();
						if($p->type !== StringParser::CONSUMED_SYMBOLS) throw new QueryException("Expects operator.");
						switch($op) {
							case '=?' :
							case '>?' :
							case '<?' :
								$rows->where($col,$op[0],array_shift($vars));
								break;
							case '<=?' :
							case '>=?' :
								$rows->where($col,$op[0].$op[1],array_shift($vars));
								break;
							default :
								throw new QueryException("Unknown operator '$op'.");
						}
						$peek = $p->consume();
						if($peek !== 'AND') {
							$p->push($peek);
							break;
						}
					}
					break;
				case 'LIMIT' :
					$limit = intval($p->consume());
					$offset = FALSE;
					if($p->type !== StringParser::CONSUMED_INTEGER) {
                        throw new QueryException("Expects integer after LIMIT keyword.");
                    }
					$peek = $p->consume();
					if($peek === ',') {
						$offset = $limit;
						$limit = intval($p->consume());
						if($p->type !== StringParser::CONSUMED_INTEGER) {
                            throw new QueryException("Expects integer offset and length after LIMIT keyword.");
                        }
					} else {
						$p->push($peek);
					}
					if($offset !== FALSE) {
						$rows->limit($limit, $offset);
					} else {
						$rows->limit($limit);
					}
					break;
				case 'ORDER' :
					$col = $p->consume();
					if($p->type !== StringParser::CONSUMED_WORD) {
                        throw new QueryException("Expects column name after ORDER keyword.");
                    }
					$peek = $p->consume();
					if($p->type === StringParser::CONSUMED_WORD && $peek === 'DESC') {
						$rows->order($col, TRUE);
					} else {
						$p->push($peek);
						$rows->order($col);
					}
					break;
				default :
					throw new QueryException('Unexpected keyword "'.$word.'" encountered. (Queries are case sensitive!)');
			}
		}

		return $rows;
	}

	public function trackId() {
		$primaryKeys = static::$_primaryKey;
		if(!$primaryKeys) $primaryKeys = array(static::$_cols[0]);
		$vals = array();
		foreach($primaryKeys as $pk) {
			if(!$this->$pk) {
				throw new CodingErrorException("Can't create track-id without ".static::class."::\$_primaryKey set");
			}
			$vals[] = $this->$pk;
		}
		return get_class($this).':'.implode(",", $vals);
	}

	public function getValidator() {
		return new \Fubber\Util\Errors($this);
	}

	public function isInvalid(): ?array {
		return $this->getValidator()->isInvalid();
	}

	public function getClientData() {
		return array();
	}

	/**
     * Removed. Hard to explain the functionality, and isn't really used.
     *
	 * Makes sure you have an instance, instead of an integer. Used
	 * to allow methods to accept both integers and instances as arguments.
	 */
	public static function getObjectInstance($object) {
		if(is_a($object, static::class)) {
			return $object;
		} else if(is_integer($object)) {
			return static::load($object);
		} else {
			throw new CodingErrorException('Expects id or instance as argument');
        }
	}

	/**
     * Removed. Hard to explain the functionality, and isn't really used.
     *
	 * Makes sure you have an ID - in case the callee provides an instance
	 * @return int|array $primaryKey
	 */
    /*
	public static function getObjectId($object) {
		if(is_a($object, static::class)) {
			$primaryKeys = static::$_primaryKey;
			if(!$primaryKeys) $primaryKeys = array(static::$_cols[0]);
			if(sizeof($primaryKeys)==1) {
				$pk = $primaryKeys[0];
				return $object->$pk;
			} else {
				$res = [];
				foreach($primaryKeys as $pk) {
					$res[] = $object->$pk;
				}
				return $res;
			}
		} else if(is_numeric($object)) {
			return $object;
		} else {
			throw new CodingErrorException("Expects id or instance as argument");
		}
	}
    */
	
	public static function sql($sql, array $vars=[]) {
		return DbTableSet::createFromSql(get_called_class(), $sql, $vars);
	}

	public static function all(): IBasicTableSet {
		$res = new DbTableSet(get_called_class());
		return $res;
	}

	public static function tableExpose($rows) {
		$res = array();
		foreach($rows as $row)
			$res[] = $row->expose();
		return $res;
	}

	/**
	*	Return an unfiltered TableSet - bypassing any security built into overriding the all()-method
	*
	*	@see all()
	*	@return DbTableSet
	*/
	public static function allUnsafe() {
		$res = new DbTableSet(get_called_class());
		return $res;
	}

	public static function beginTransaction() {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
		return $db->beginTransaction();
	}
	
	public static function commit() {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
		return $db->commit();
	}
	
	public static function rollBack() {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
		return $db->rollBack();
	}
	
	/**
	*	Load a single row as an instance of the given class. Bypass any filtering built into the all()-method.
	*
	*	@param mixed $primaryKey,...
	*	@return Table
	*/
	public static function loadUnsafe($key) {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);

		$primaryKeys = static::$_primaryKey;
		if(!$primaryKeys) $primaryKeys = array(static::$_cols[0]);

		$args = func_get_args();
		if(sizeof($args)!=sizeof($primaryKeys)) {
            throw new CodingErrorException("Invalid number of arguments. Expects ".implode(", ", $primaryKeys).".");
        }

		$wheres = array();
		foreach($primaryKeys as $k) {
			$wheres[] = $k."=?";
		}
		$sql = 'SELECT * FROM '.static::$_table.' WHERE '.implode(" AND ",$wheres);

		return $db->queryOne($sql, $args, get_called_class());
	}

	/**
	*	Load a single instance of the given class.
	*
	*	@param mixed $primaryKey,...
	*/
	public static function load(...$key) {
        $key = $key[0];
		$primaryKeys = static::$_primaryKey;
		if(!$primaryKeys) $primaryKeys = array(static::$_cols[0]);

		$args = func_get_args();
		if(sizeof($args)!=sizeof($primaryKeys)) {
            throw new CodingErrorException("Invalid number of arguments. Expects ".implode(", ", $primaryKeys).".");
        }

		$className = get_called_class();
		$all = $className::all();
		$wheres = array();
		foreach($primaryKeys as $k) {
			$all->where($k,'=',array_shift($args));
		}
		$all->limit(1);
		foreach($all as $res) {
            return $res;
        }
		return null;
	}
	
	public static function loadMany(array $keys) {
		$pk = static::getPrimaryKeyName();
		$res = [];
		foreach(static::all()->where($pk,'IN',$keys) as $o) {
			$res[$o->$pk] = $o;
		}
		return $res;
	}
	
	/**
	 * Check that a row exists
	 */
	public static function exists($key) {
		$primaryKeys = static::$_primaryKey;
		if(!$primaryKeys) $primaryKeys = array(static::$_cols[0]);

		$args = func_get_args();
		if(sizeof($args)!=sizeof($primaryKeys)) {
            throw new CodingErrorException("Invalid number of arguments. Expects ".implode(", ", $primaryKeys).".");
        }

		$className = get_called_class();
		$all = $className::all()->fields(implode(",", $primaryKeys));
		$wheres = array();
		foreach($primaryKeys as $k) {
			$all->where($k,'=',array_shift($args));
		}
		$all->limit(1);
		foreach($all as $res) return true;
		return false;
	}
	
	public static function existsUnsafe($key) {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);

		$primaryKeys = static::$_primaryKey;
		if(!$primaryKeys) $primaryKeys = array(static::$_cols[0]);

		$args = func_get_args();
		if(sizeof($args)!=sizeof($primaryKeys)) {
            throw new CodingErrorException("Invalid number of arguments. Expects ".implode(", ", $primaryKeys).".");
        }

		$wheres = array();
		foreach($primaryKeys as $k) {
			$wheres[] = $k."=?";
		}
		$sql = 'SELECT COUNT('.implode(",", $primaryKeys).') FROM '.static::$_table.' WHERE '.implode(" AND ",$wheres);

		return $db->queryField($sql, $args, get_called_class()) > 0;
	}

	/**
	*	Delete this object
	*
	*	@see IDb:::exec()
	*	@return bool
	*/
	public function delete(): bool {
		$this->beforeDelete();
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
		
		if(!static::$_primaryKey)
			$primaryKey = static::$_cols[0];
		else
			$primaryKey = static::$_primaryKey;

		$wheres = array();
		if(is_array($primaryKey)) {
			foreach($primaryKey as $pk) {
				$wheres[] = $pk.'=?';
				$vals[] = $this->$pk;
			}
		} else {
			$wheres[] = $primaryKey.'=?';
			$vals[] = $this->$primaryKey;
		}
		$sql = 'DELETE FROM '.static::$_table.' WHERE '.implode(" AND ", $wheres).' LIMIT 1';
		return $db->exec($sql, $vals);
	}

	/**
	 * Override this to handle before-save activities. This is performed right before 
	 * validation. It may be performed multiple times before save.
	 */
	public function beforeSave(array $properties = null) {
	}
	public function beforeDelete() {		
	}

	/**
	 * Save any changes for this object. If you specify a list of properties,
	 * only these properties are written to the database.
	 * 
	 * @param array $properties List of properties to save
	 * @return bool
	 */
	public function save(array $properties = null) {
		$this->beforeSave($properties);
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);

		$errors = $this->isInvalid();
		if($errors) {
			throw new ValidationException($errors);
		}

		if(!static::$_primaryKey)
			$primaryKey = static::$_cols[0];
		else
			$primaryKey = static::$_primaryKey;

		$vals = [];
		if($this->_fromDb) {
			// Perform update
			$pairs = [];
			
			// Optimized the if out of the loop
			if($properties) {
				foreach($properties as $col) {
					$pairs[] = "$col=?";
					$vals[] = $this->$col;
				}
			} else {
				foreach(static::$_cols as $col) {
					$pairs[] = "$col=?";
					$vals[] = $this->$col;
				}
			}
			$wheres = [];
			if(is_array($primaryKey)) {
				$idx = 0;
				foreach($primaryKey as $col) {
					$wheres[] = $col.'=?';
					$vals[] = $this->_fromDb[$idx++];
				}
			} else {

				$wheres[] = $primaryKey."=?";
				$vals[] = $this->_fromDb[0];
			}
					
			$sql = 'UPDATE '.static::$_table.' SET '.implode(",", $pairs).' WHERE '.implode(" AND ",$wheres);
		} else {
			if($properties) {
				throw new CodingErrorException("Can't save specific properties when inserting new objects into database");
			}

			// Perform insert
			$sql = 'INSERT INTO '.static::$_table;

			$cols = [];
			$qs = [];
			
			if(is_array($primaryKey) && sizeof($primaryKey) > 1) {
				// Complex primary key, so no auto increment
				foreach(static::$_cols as $col) {
					$qs[] = '?';
					$vals[] = $this->$col;
					$cols[] = $col;
				}
			} else {
				// Simple primary key.
				
				// This is an optimization (one loop for string primary key, another for array)
				if(is_array($primaryKey)) {
					foreach(static::$_cols as $col) {
						if(!in_array($col, $primaryKey)) {
							$qs[] = '?';
							$vals[] = $this->$col;
							$cols[] = $col;
						}
					}
				} else {
					foreach(static::$_cols as $col) {
						if($col != $primaryKey) {
							$qs[] = '?';
							$vals[] = $this->$col;
							$cols[] = $col;
						}
					}
				}
			}
			$sql .= ' (`'.implode("`,`", $cols).'`) VALUES ('.implode(",", $qs).')';
		}

		try {
			$res = $db->exec($sql, $vals);

			if(!(is_array($primaryKey) && sizeof($primaryKey) > 1)) {
				// We should have an inserted ID for this record
				$id = $db->lastInsertId();
                if ($id !== null && is_numeric($id) && intval($id) == $id) {
                    $id = intval($id);
                }
				if($id !== null) {
					if (is_array($primaryKey)) {
						$pk = $primaryKey[0];
					} else {
						$pk = $primaryKey;
					}
					$this->$pk = $id;
				}
			}
		} catch (\PDOException $e) {
			$res = FALSE;
			if($e->getCode() == '42S02' && !static::$_triedUpdateSchema) {
				static::$_triedUpdateSchema = true;
				if(method_exists(static::class, 'updateSchema')) {
					static::updateSchema($db);
					return $this->save($properties);
				}
			}
            throw $e;
			throw WrappedException($e);
		}

		if($res===FALSE) {
			throw new RowNotSavedException("Unable to save object");
		}
		
		if(!$this->_fromDb) {
			if(!is_array($primaryKey)) {
				$lastInsertId = $db->lastInsertId();
                if ($lastInsertId !== null && is_numeric($lastInsertId) && intval($lastInsertId) == $lastInsertId) {
                    $lastInsertId = intval($lastInsertId);
					$this->$primaryKey = $lastInsertId;
                }
				$this->_fromDb = [$lastInsertId];
			} else {
				$this->_fromDb = [];
				foreach($primaryKey as $col)
					$this->_fromDb[] = $this->$col;
			}
		}
		
		return TRUE;
	}

	public function __get($name) {
		if(isset($this->_lazyProps[$name]))
			return $this->$name = call_user_func($this->_lazyProps[$name]);
		if(method_exists($this, $methodName = 'get'.ucfirst($name)))
			return $this->$name = $this->$methodName();
		if(in_array($name, static::$_cols))
			return null;
		throw new CodingErrorException("Uninitialized property '$name' in '".get_called_class()."' ($methodName)");
	}
	
    public static function getPrimaryKeyNames(): array {
        if (static::$_primaryKey) {
            if (!is_array(static::$_primaryKey)) {
                return [static::$_primaryKey];
            } else {
                return static::$_primaryKey;
            }
        } else {
            return [static::$_cols[0]];
        }
    }

    public static function getColumnNames(): array {
        return static::$_cols;
    }

    public static function getForeignKeys(): array {
        return static::$_foreignKeys;
    }

	public function getPrimaryKey() {
		$name = static::getPrimaryKeyName();
		return $this->$name;
	}

	public static function getPrimaryKeyName() {
		if(static::$_primaryKey) {
			if(is_array(static::$_primaryKey)) {
				throw new CodingErrorException("Composite primary keys are no longer supported, used in class ".static::class);
            }

			return static::$_primaryKey;
		} else {
			$cols = static::$_cols;
			return $cols[0];
		}
	}
	
	public function isNew(): bool {
		return !$this->_fromDb;
	}
	
	public function unNew() {
		$this->_fromDb = [$this->getPrimaryKey()];
	}
	
	public function reNew() {
		$this->_fromDb = null;
	}
}

