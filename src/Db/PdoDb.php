<?php
namespace Fubber\Db;

use PDO;
use Fubber\Service;
use Fubber\Hooks;
use Fubber\LogicException;
use WeakMap;

/**
*	PDO database wrapper using the IDb interface
*/
class PdoDb implements IDb {
	protected static $idx = 1;
	public $pdo;
	protected $_lastInsertId = NULL;
    protected ?string $_previousStatementString = null;

	protected $statements = array();

	public static function create(): IDb {
        return new static(Service::use(PDO::class));
//		return new static(\Nerd\Glue::get(\PDO::class));
	}

	/**
	*	@param PDO $pdo
	*/
	public function __construct(\PDO $pdo) {
		$this->pdo = $pdo;
	}

	/**
	*	@see IDb::lastInsertId()
	*/
	public function lastInsertId() {
        /*
        if ($this->_previousStatementString === null) {
            throw new LogicException("Have no previous statement, unable to get lastInsertId");
        }
        */
		if($this->_lastInsertId) {
			return $this->_lastInsertId;
        }
		return NULL;
	}

	protected function prepare(string $statement) {
        // Caching the prepared statements
		if(isset($this->statements[$statement])) {
			return $this->statements[$statement];
        } else {
			return $this->statements[$statement] = $this->pdo->prepare($statement);
        }
	}

	/**
	*	Query the database and return an array of objects
	*
	*	@see IDb::query()
	*/
	public function query(string $statement, array $vars=array(), string $className='stdClass') {
        $result = null;
        extract(Hooks::filter(IDb::class.'::query', ['statement' => $statement, 'vars' => $vars, 'className' => $className, 'result' => $result], $this), \EXTR_IF_EXISTS);
        if ($result !== null) {
            return $result;
        }
		foreach($vars as $v)
			if(!is_scalar($v)) {
                $type = \gettype($v);
                if ($type === 'object') {
                    $type = \get_class($v);
                }
				throw new \TypeError(static::class."::query() vars can't be of type '$type'.");
            }
		try {
			$prepared = $this->prepare($statement);
			$res = $prepared->execute($vars);
		} catch (\PDOException $e) {
            throw $e;
            $newException = new Exception($e->getMessage());
            $newException->debug = ['sql' => $statement, 'vars' => $vars];
            throw $newException;
		}
		if(!$res) throw new Exception("Invalid query ($sql).");
		$result = $prepared->fetchAll(\PDO::FETCH_CLASS, $className);;
		$prepared->closeCursor();
        return Hooks::filter(IDb::class.'::query()', $result, $this, ['statement' => $statement, 'vars' => $vars, 'className' => $className, 'result' => $result]);
	}

	/**
	*	Query the database and return a single object that is the first row in the result set
	*
	*	@see IDb::queryOne()
	*/
	public function queryOne(string $statement, array $vars=array(), string $className='stdClass') {
        $result = null;
        extract(Hooks::filter(IDb::class.'::queryOne', ['statement' => $statement, 'vars' => $vars, 'className' => $className, 'result' => $result], $this), \EXTR_IF_EXISTS);
        if ($result !== null) {
            return $result;
        }
		$prepared = $this->prepare($statement);
		$res = $prepared->execute($vars);
		if(!$res) throw new Exception("Invalid query ($sql).");
		$result = $prepared->fetchObject($className);
		$prepared->closeCursor();
        return Hooks::filter(IDb::class.'::queryOne()', $result, $this, ['statement' => $statement, 'vars' => $vars, 'className' => $className, 'result' => $result]);
	}

	/**
	*	Query the database and return the first column of the first row
	*
	*	@see IDb::queryField()
	*/
	public function queryField(string $statement, array $vars=array()) {
        $result = null;
        extract(Hooks::filter(IDb::class.'::queryField', ['statement' => $statement, 'vars' => $vars, 'result' => $result], $this), \EXTR_IF_EXISTS);
        if ($result !== null) {
            return $result;
        }
		$prepared = $this->prepare($statement);
		$res = $prepared->execute($vars);
		if(!$res) throw new Exception("Invalid query ($sql).");
		return Hooks::filter(IDb::class.'::queryField()', $prepared->fetchColumn(), $this, ['statement' => $statement, 'vars' => $vars, 'result' => $result]);
	}

	/**
	*	Query the database and return an array of values with the first column of the result
	*
	*	@see IDb::queryColumn()
	*/
	public function queryColumn(string $statement, array $vars=array()): iterable {
        $result = null;
        extract(Hooks::filter(IDb::class.'::queryColumn', ['statement' => $statement, 'vars' => $vars, 'result' => null], $this), \EXTR_IF_EXISTS);
        if ($result !== null) {
            return $result;
        }
		$prepared = $this->prepare($statement);
		$res = $prepared->execute($vars);
		if(!$res) throw new Exception("Invalid query ($sql).");
		return Hooks::filter(IDb::class.'::queryColumn()', $prepared->fetchAll(\PDO::FETCH_COLUMN), $this, ['statement' => $statement, 'vars' => $vars, 'result' => null]);
	}

	/**
	*	Execute a query. If the query fails, throws an exception. Else it returns the number of affected rows.
	*
	*	@param string $statement
	*	@param array $vars
	*	@return int
	*/
	public function exec(string $statement, array $vars=array()) {
        $result = null;
        extract(Hooks::filter(IDb::class.'::exec', ['statement' => $statement, 'vars' => $vars, 'result' => $result], $this), \EXTR_IF_EXISTS);
        if ($result !== null) {
            return $result;
        }

		$prepared = $this->prepare($statement);
		try {
			$res = $prepared->execute(array_values($vars));
		} catch (\PDOException $e) {
			$e->suggestion = "Fix the statement: '$statement.'";
			throw $e;
		}
		if($res) {
			$this->_lastInsertId = $this->pdo->lastInsertId();
            $result = $prepared->rowCount();
		} else {
			$this->_lastInsertId = NULL;
            $result = null;
		}

		return Hooks::filter(IDb::class.'::exec()', $result, $this, ['statement' => $statement, 'vars' => $vars, 'result' => $result]);
	}

	/**
	*	@see IDb:::beginTransaction()
	*/
	public function beginTransaction() {
		return $this->pdo->beginTransaction();
	}

    public function inTransaction(): bool {
        return $this->pdo->inTransaction();
    }

	/**
	*	@see IDb::commit()
	*/
	public function commit() {
		return $this->pdo->commit();
	}

	/**
	*	@see IDb::rollBack();
	*/
	public function rollBack() {
		return $this->pdo->rollBack();
	}
	
	public function quote($value) {
		return $this->pdo->quote($value);
	}

	protected function _rewrite($statement, array $vars) {
		$parts = explode("?", $statement);
		if(sizeof($parts) !== sizeof($vars)+1)
			throw new Exception("The number of ? does not match the number of vars");
		$sql = '';
		foreach($vars as $var) {
			if($var === NULL)
				$sql .= array_shift($parts).'NULL';
			else
				$sql .= array_shift($parts).$this->pdo->quote($var);
		}
		$sql .= array_shift($parts);
		return $sql;
	}
}
