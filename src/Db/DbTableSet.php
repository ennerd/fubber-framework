<?php
namespace Fubber\Db;

use Fubber\Service;
use Fubber\Parsing\StringParser;
use Fubber\BadRequestException;
use Fubber\CodingErrorException;
use Fubber\Table\IBasicTableSet;

/**
*	Represents a subset of data from a class that extends Table. Allows filtering of the data.
*/
class DbTableSet implements \IteratorAggregate, \Countable, \JsonSerializable, IBasicTableSet {
	protected $_class;
	protected $_customSql;
	protected $_customVars;
    protected $_forUpdate = false;
    protected $_skipLocked = false;
	protected $_wheres = array();
	protected $_complexWheres = array();
	protected $_order = NULL;
	protected $_orderDesc = FALSE;
	protected $_limit = 1000;
	protected $_offset = 0;
	protected $_fields = ['*'];
	protected $_joins = array();
	protected $_noLimit = FALSE;
	protected $_cache = array();


	public function __construct($class) {
//echo "CONSTRUCT TableSet($class)<br>\n";

		$this->_class = $class;
		
	}
	
	public function getModelClass(): string {
		return $this->_class;
	}
	
	public function getOrder(): string {
		return $this->_order;
	}
	
	public function isOrderDesc(): bool {
		return $this->_orderDesc;
	}

	public function getLimit(): int {
		return $this->_limit;
	}
	
	public function getOffset(): int {
		return $this->_offset;
	}
	
	public static function createFromSql($class, $sql, array $vars=[]) {
		$res = new static($class);
		$res->_customSql = $sql;
		$res->_customVars = $vars;
		return $res;
	}

	/**
	 * Parses queries like ?birthday[>]=2018-09-20&name=Frode
	 * 
	 * fieldname[>]=value			fieldname > value
	 * fieldname[<]=value			fieldname < value
	 * fieldname[>=]=value			fieldname >= value
	 * 
	 * l = limit
	 * o = offset
	 */ 
	public function parseQuery(array $get): IBasicTableSet {
		
        $model = $this->getModelClass();
        $cols = $this->getCols();
        /*
        $foreignKeys = $model::$_foreignKeys;
        */
        foreach($cols as $col) {
            if(isset($get[$col])) {
                $val = $get[$col];
                if(is_array($val)) {
                    foreach($val as $op => $v) {
                        $this->where($col, $op, $v);
                    }
                } else {
                    $this->where($col,'=',$val);
                }
            }
        }
        if(isset($get['l']) && intval($get['l'])>1000)
            throw new BadRequestException("Not possible to fetch more than 1000 rows per query");
        if(isset($get['l']) && isset($get['o']))
            $this->limit($get['l'], $get['o']);
        else if(isset($get['l']))
            $this->limit($get['l']);
        else if(isset($get['o']))
            $this->limit(1000, $get['o']);
        
        if(isset($get['s'])) {
            if ($get['s'][0] === '-') {
                $s = substr($get['s'], 1);
                if (in_array($s, $cols)) {
                    $this->order($s, true);
                }
                // reversefrode
            } elseif(in_array($get['s'], $cols)) {
                $this->order($get['s']);
            }
        }
        /*
        $includes = null;
        if(isset($get['include'])) {
            $includes = explode(",", $get['include']);
            foreach($includes as $include) {
                if(!isset($foreignKeys[$include])) {
                    throw new BadRequestException("Can't include '".$include."'. No such foreign key.");
                }
            }
        }
        */
        return $this;
	}

	public function parseWhereFromString($where) {
		$this->_cache = array();
		if($p = strpos($where, ' >= '));
		else if($p = strpos($where, ' <= '));
		else if($p = strpos($where, ' <> '));
		else if($p = strpos($where, ' = '));
		else if($p = strpos($where, ' > '));
		else if($p = strpos($where, ' < '));
		else throw new CodingErrorException("Invalid or missing operator");
		$res = array(substr($where, 0, $p));
		$where = substr($where, $p);
		if(strpos($where, ' >= ')===0) {
			$res[] = '>=';
			$rest = substr($where, 4);
		} else if(strpos($where, ' <= ')===0) {
			$res[] = '<=';
			$rest = substr($where, 4);
		} else if(strpos($where, ' <> ')===0) {
			$res[] = '<>';
			$rest = substr($where, 4);
		} else if($where[0]==" " && $where[2]==" " && ($where[1]=='=' || $where[1]=='>' || $where[1]=='<')) {
			$res[] = $where[1];
			$rest = substr($where, 3);
		}
		if($rest[0]=='"' && $rest[strlen($rest)-1]=='"') {
			$res[] = substr($rest, 1, -1);
		} else if($rest[0]=="'" && $rest[strlen($rest)-1]=="'") {
			$res[] = substr($rest, 1, -1);
		} else if($rest==='NULL') {
			$res[] = NULL;
		} else if(is_numeric($rest)) {
			settype($rest, 'float');
			$res[] = $rest;
		} else {
			throw new CodingErrorException("Unable to parse value from query ($rest).");
		}
		$this->where($res[0], $res[1], $res[2]);
	}

	protected function _buildWheres($prefix='') {
		$wheres = array();
		$vars = array();
		$ins = array();
		$_class = $this->_class;
		if($prefix!='') $prefix .= '.';
        else $prefix = $_class::$_table.'.';
		foreach($this->_wheres as $where) {
			// Validate that the field name exists
			if(!in_array($where[0], $_class::$_cols))
				throw new CodingErrorException("Unknown column '".$where[0]."' in '".$_class."'");

			// Handle the operator
			switch($where[1]) {
				case '>' :
				case '<' :
				case '>=' :
				case '<=' :
				case '<>' :
				case '=' :
					if($where[2]===null) {
						if($where[1]=='=')
							$wheres[] = $prefix.$where[0].' IS NULL';
						else if($where[1]=='<>')
							$wheres[] = $prefix.$where[0].' IS NOT NULL';
						else
							throw new CodingErrorException("Can't compare '".$where[0]."' to NULL.");
					} else {
						$wheres[] = $prefix.$where[0].$where[1].'?';
						$vars[] = $where[2];
					}
					break;
				case 'IN' :
                    // Handle IN [ 123, 234 ]
					if(is_array($where[2])) {
						if(sizeof($where[2])===0) {
							// Nothing matches
							$wheres[] = '1=0';
						} else {
                            $db = Service::use(IDb::class);
							$where[2] = array_map([$db, 'quote'], $where[2]);
							$wheres[] = $prefix.$where[0].' IN ('.implode(",", $where[2]).')';
						}
					} else {
                        // Handle IN TableSet
						if(!is_object($where[2]) || !($where[2] instanceof DbTableSet)) {
							throw new CodingErrorException("Argument to IN operator must be an array or a TableSet (got ".get_class($where[2]).")");
                        }

						$thatClass = $where[2]->getModelClass(); // _class;

                        if (sizeof($where[2]->getColumns()) !== 1 || $where[2]->getColumns()[0] === '*') {
							throw new CodingErrorException("You must specify a single field to compare to in the 'IN' clause for ".$thatClass);
                        }

                        if (get_class($where[2]) !== static::class ) {
                            // Another backend, so we must use normal API to get the ids.
                            $slots = [];
                            $thatCol = $where[2]->getColumns()[0];
                            
                            foreach ($where[2] as $row) {
                                $vars[] = $row->$thatCol;
                                $slots[] = '?';
                            }

                            if (sizeof($ids) === 0) {
                                $wheres[] = '123=456';
                            } else {
                                $wheres[] = $prefix.$where[0].' IN ('.implode(",", $slots).')';
                            }

                        } else {
                            // Same backend, we can optimize
                            $thatPrefix = "t".md5(serialize($where[2]));

                            $part = 'EXISTS (SELECT 1 FROM '.$thatClass::$_table.' AS '.$thatPrefix.' WHERE ';
    						list($thatWheres, $thatVars) = $where[2]->_buildWheres($thatPrefix);

                            $thatWheres[] = $thatPrefix.'.'.$where[2]->getColumns()[0].'='.$prefix.$where[0];

                            $part .= implode(" AND ", $thatWheres).')';
                            foreach ($thatVars as $var) {
                                $vars[] = $var;
                            }

                            $wheres[] = $part;
/*
    						$subQuery = 'SELECT DISTINCT '.implode(",", $where[2]->_fields).' FROM '.$thatClass::$_table.' AS '.$thatPrefix;
    						if(sizeof($thatWheres)>0) {
    							$subQuery .= ' WHERE '.implode(" AND ", $thatWheres);
    							foreach($thatVars as $thatVar) {
    								$vars[] = $thatVar;
                                }
    						}
    						$wheres[] = $prefix.$where[0].' IN ('.$subQuery.')';
*/
                        }
					}
//					$ins[] = array($where[0], $where[1], $where[2], $prefix);

					break;
				default :
					throw new CodingErrorException("Unsupported operator ".$where[1]." in ".$_class);
			}
		}
		foreach($this->_complexWheres as $spec) {
			$wheres[] = $spec[0];
			foreach($spec[1] as $var) {
				$vars[] = $var;
			}
			
		}
		return array($wheres, $vars, $ins);
	}

	/**
	*	$rows->join('SomeTable.userId=id AND SomeTable.groupId=?', array(12), array('createdDate' => 'joinedDate'))
	*/
	public function join($joinSpec, array $one, array $two) {
		$this->_cache = array();
		$this->_joins[] = array($joinSpec, $one, $two);
		return $this;
	}

	public function count() {
		if(isset($this->_cache['count']))
			return $this->_cache['count'];
		
//        Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
        $db = \Fubber\Db::create();

		if($this->_customSql) {
			$sql = 'SELECT COUNT(*) '.substr($this->_customSql, strpos($this->_customSql, 'FROM'));
			$res = $db->queryField($sql, $this->_customVars);
			return $this->_cache['count'] = intval($res);
		}
		
		$_class = $this->_class;
		list($wheres,$vars) = $this->_buildWheres();
		$sql = 'SELECT COUNT(*) FROM '.$_class::$_table;
		if(sizeof($wheres)>0) $sql .= ' WHERE '.implode(" AND ", $wheres);
		$res = $db->queryField($sql, $vars, $_class);
		return $this->_cache['count'] = intval($res);
	}

	protected function _buildSql() {
		$_class = $this->_class;
		$fields = array();
		$tables = array();
        $prefix = 'm'.md5(serialize($this));
		list($wheres, $vars) = $this->_buildWheres($prefix);

		if(implode(",", $this->_fields) === '*') {
			$fields[] = $prefix.'.*';
		} else {
			// Validate fields
			foreach($this->_fields as $part) {
				$part = trim($part);
				if(!in_array($part, $_class::$_cols)) {
					throw new CodingErrorException("Unknown column ".$part." in ".$_class);
                }
				$fields[] = $prefix.'.'.$part;
			}
		}
		if(sizeof($this->_joins)>0) {
			$joinNum = 1;
			foreach($this->_joins as $join) {
				$p = new StringParser($join[0], TRUE);
				$nextVar = function() use ($p, $_class) {
					$res = array();
					$a = $p->consume();
					if($p->type !== StringParser::CONSUMED_WORD) throw new QueryException("Expects a [column name] or [table name].[column name] in join statement.");
					$dot = $p->consume();
					if($p->type === StringParser::CONSUMED_SYMBOLS && $dot === '.') {
						$col = $p->consume();
						if($p->type !== StringParser::CONSUMED_WORD) throw new QueryException("Expects a column name after '$a.' in join statement.");
						if(!in_array($col, $a::$_cols)) throw new QueryException("Unknown column '$a.$col' in join statement.");
						return array($a, $col);
					} else {
						$p->push($dot);
						return $a;
					}
				};

				$joinWheres = array();
				while(TRUE) {
					$part1 = $nextVar();
					$eq = $p->consume();
					if($p->type !== StringParser::CONSUMED_SYMBOLS || ($eq !== '=' && $eq !== '=?')) throw new QueryException("Expects the '=' or '=?' operator in join statement, got '$eq'.");
					$toAdd = array();
					if($eq === '=') {
						$part2 = $nextVar();
						if(is_array($part1))
							$joinWheres[] = array($part1, $part2);
						else
							$joinWheres[] = array($part2, $part1);
					} else {
						$joinWheres[] = array($part1, '?', array_shift($join[1]));
					}
					$peek = $p->consume();
					if($peek === FALSE) {
						break;
					}
					if($p->type !== StringParser::CONSUMED_WORD || $peek !== 'AND') {
						throw new QueryException("Unexpected '$peek' in join statement. Expected 'AND' or nothing.");
					}
				}

				// Check that this join does not use multiple tables. Can't do that without calling ->join multiple times
				$joinTables = array();
				foreach($joinWheres as $joinWhere) {
					if(is_array($joinWhere[0])) {
						$joinTables[$joinWhere[0][0]] = true;
					}
				}
				if(sizeof($joinTables)!==1) throw new QueryException("Can't use a single join that spans multiple tables (".implode(",",array_keys($tables)).").");

				// joinSpec should be fine
				$arrayKeys = array_keys($joinTables);
				$joinClass = array_shift($arrayKeys);
				$tables[] = $joinClass::$_table.' AS t'.$joinNum;
				$joinFields = $join[sizeof($join)-1];
				foreach($joinFields as $from => $to) {
					if(!in_array($from, $joinClass::$_cols)) throw new QueryException("Column '$from' not found in class '$joinClass'.");
					$fields[] = 't'.$joinNum.'.'.$from.' AS '.$to;
				}

				foreach($joinWheres as $joinWhere) {
					if($joinWhere[1]==='?') {
						$wheres[] = 't'.$joinNum.'.'.$joinWhere[0][1].'=?';
						$vars[] = $joinWhere[2];
					} else {
						$wheres[] = 't'.$joinNum.'.'.$joinWhere[0][1].'='.$_class::$_table.'.'.$joinWhere[1];
					}
				}
			}
			$joinNum++;
		}

		$sql = 'SELECT '.implode(",", $fields).' FROM '.$_class::$_table.' AS '.$prefix;
		if(sizeof($tables)>0) $sql .= ','.implode(",", $tables);
		if(sizeof($wheres)>0) $sql .= ' WHERE '.implode(" AND ", $wheres);
		if($this->_order) {
			if(!in_array($this->_order, $_class::$_cols))
				throw new CodingErrorException("Unknown column ".$this->_order." in ".$_class);
			$sql .= ' ORDER BY '.$prefix.'.'.$this->_order;
			if($this->_orderDesc)
				$sql .= ' DESC';
		}
		if(!$this->_noLimit) {
			if($this->_limit < 1) throw new BadRequestException("The limit must be at least 1 when querying ".$_class);
			if($this->_offset < 0) throw new BadRequestException("The offset must not be negative when querying ".$_class);
			$sql .= ' LIMIT '.$this->_offset.','.$this->_limit;
		}

        $sql = str_replace([
            $_class::$_table.'.'
        ], [
            $prefix.'.'
        ], $sql);

        if ($this->_forUpdate) {
            $db = Service::use(IDb::class);
            if (!$db->inTransaction()) {
				throw new QueryException("When selecting FOR UPDATE you must first start a transaction.");
            }
            $sql .= ' FOR UPDATE';
            if ($this->_skipLocked) {
                $sql .= ' SKIP LOCKED';
            }
        } elseif ($this->_skipLocked) {
            throw new QueryException("Can't SKIP LOCKED without FOR UPDATE");
        }
		return array($sql, $vars);
	}
	
	public function column($field) {
		return $this->columns([$field]);
	}

	public function fields($fields) {
		if(is_string($fields))
			$fields = explode(",", $fields);
		return $this->columns($fields);
	}
	
	public function columns(array $columns) {
		$this->_cache = array();
        $this->_fields = $columns;
		return $this;
	}

	public function one() {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);

		$_class = $this->_class;

		if($this->_customSql) {
			$o = $db->queryOne($this->_customSql, $this->_customVars, $_class);
		} else {
			$limit = $this->_limit;
			$this->_limit = 1;
			list($sql, $vars) = $this->_buildSql();
			$this->_limit = $limit;
			
		//var_dump($sql);var_dump($vars);die();
            try {
			    $o = $db->queryOne($sql, $vars, $_class);
            } catch (\PDOException $e) {
                throw $e;
echo $sql;die("DbTableSet");
            }
		}
		return $this->wasFromDb($o);
	}
	
	public function getSql() {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
		if($this->_customSql) {
			$sql = $this->_customSql;
			$vars = $this->_customVars;
		} else {
			list($sql, $vars) = $this->_buildSql();
		}
		$replaceOne = function($from, $to, $content) {
		    $from = '/'.preg_quote($from, '/').'/';
		    return preg_replace($from, $to, $content, 1);			
		};
		foreach($vars as $k => $v) {
			if(is_numeric($k)) {
				$sql = $replaceOne("?", $db->quote($v), $sql);
			} else {
				$sql = str_replace(":".$k, $db->quote($v), $sql);
			}
		}
		return $sql;
	}
	
	protected function wasFromDb(&$object) {
		if(!$object) return $object;
		$className = $this->_class;
		$primaryKey = $className::$_primaryKey;
		if(!$primaryKey)
			$primaryKey = [$className::$_cols[0]];
		$tag = [];
		foreach($primaryKey as $col)
			$tag[] = $object->$col;

		$object->_fromDb = $tag;
		return $object;
	}

    /**
     * All the columns that exists
     */
	public function getCols(): array {
		$className = $this->_class;
		return $className::$_cols;
	}

    /**
     * All the columns that we're fetching
     */
	public function getColumns(): array {
		return $this->_fields;
	}

	public function getColumnValues($fieldName) {
        $db = Service::use(IDb::class);
//		$db = \Nerd\Glue::get(\Fubber\Db\IDb::class);
		$fields = $this->_fields;
		$this->_fields = [ $fieldName ];
		list($sql, $vars) = $this->_buildSql();
		$res = $db->queryColumn($sql, $vars);
		$this->_fields = $fields;
		return $res;
	}

	public function getIterator() {
        $db = Service::use(IDb::class);
		$_class = $this->_class;
		$primaryKey = $_class::$_primaryKey;
		if(!$primaryKey)
			$primaryKey = [$_class::$_cols[0]];
		if($this->_customSql) {
			$sql = $this->_customSql;
			$vars = $this->_customVars;
		} else {
			list($sql, $vars) = $this->_buildSql();
		}
		if(implode(",", $this->_fields) === '*') {
			$rs = $db->query($sql, $vars, $_class);
			$res = [];
			if(sizeof($primaryKey)===1) {
				$primaryKey = $primaryKey[0];
				foreach($rs as &$row) {
					$this->wasFromDb($row);
					$res[] = $row;
				}
			} else {
				foreach($rs as &$row) {
					$this->wasFromDb($row);
					$k = [];
					foreach($primaryKey as $k)
						$k[] = $row->$k;
					$res[] = $row;
				}
			}
		}
		else {
			$res = $db->query($sql, $vars, $_class);
		}
//throw new \Exceptions\ApiException();
		return new \ArrayIterator($res);
	}

    public function forUpdate() {
        $this->_forUpdate = true;
        return $this;
    }

    public function skipLocked() {
        $this->_skipLocked = true;
        return $this;
    }

	public function where($field, $op, $value): IBasicTableSet {
		if(is_object($value) && is_subclass_of($value, \Fubber\Table::class)) {
			$className = get_class($value);

			$primaryKey = $className::$_primaryKey;
			if(!$primaryKey)
				$primaryKey = [$className::$_cols[0]];
			if(sizeof($primaryKey)>1)
				throw new QueryException("Where clause can't receive composite primary key objects.");
			$primaryKey = $primaryKey[0];
			$value = $value->$primaryKey;
		}
		$this->_cache = array();
		$this->_wheres[] = array($field, $op, $value);
		return $this;
	}

	public function complexWhere($sql, $vars=[]): DbTableSet {
		$this->_complexWheres[] = [ '('.$sql.')', $vars ];
		return $this;
	}

	public function order($field, $desc = FALSE): IBasicTableSet {
		$this->_cache = array();
		$this->_order = $field;
		$this->_orderDesc = $desc;
		return $this;
	}

	public function limit(int $limit, int $offset = 0): IBasicTableSet {
		$this->_cache = array();
		$this->_limit = intval($limit);
		$this->_offset = intval($offset);
		return $this;
	}
	
	public function clone(): IBasicTableSet {
		return unserialize(serialize($this));
	}
	
	public function jsonSerialize() {
		$res = [];
		foreach($this as $row) {
			$res[] = $row;
		}
		return $res;
	}
	
	public function serialize() {
		return serialize(get_object_vars($this));
	}
	
	public function unserialize($string) {
		foreach(unserialize($string) as $k => $v) {
			$this->$k = $v;
		}
	}
}
