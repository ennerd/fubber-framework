<?php
namespace Fubber\Db;

use Fubber\CodingErrorException;

class RowNotSavedException extends CodingErrorException implements IException {

    public function jsonSerialize() {
        return ['error' => 'Row not written to database', 'message' => $this->getMessage(), 'code' => $this->getCode()];
    }
}
