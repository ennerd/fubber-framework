<?php
namespace Fubber\Db;

trigger_error('Class ' . EmptyTableSet::class . ' is deprecated. ' . \Fubber\Table\EmptyTableSet::class .' is a drop in replacement.', E_USER_DEPRECATED);

/**
 * This file exists here for backward compatability reasons. You should use Fubber\Table\EmptyTableSet directly.
 * 
 * @see Fubber\Table\EmptyTableSet
 */
class EmptyTableSet extends \Fubber\Table\EmptyTableSet {}
