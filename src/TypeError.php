<?php
namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\Traits\ExceptionTrait;
use TypeError as GlobalTypeError;

class TypeError extends GlobalTypeError implements IException {
    use ExceptionTrait;

    protected function getDefaultStatus(): array {
        return [500, 'Internal Server Error'];
    }

    public function getExceptionDescription(): string
    {
        return "Error related to typed arguments, typed return values or typed operands";
    }

}