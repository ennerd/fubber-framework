<?php
declare(strict_types=1);

namespace Fubber\Mailer;

interface IMail {

    const PRIORITIES = [
        'LOW' => self::PRIORITY_LOW,
        'MEDIUMLOW' => self::PRIORITY_MEDIUMLOW,
        'MEDIUM' => self::PRIORITY_MEDIUM,
        'MEDIUMHIGH' => self::PRIORITY_MEDIUMHIGH,
        'HIGH' => self::PRIORITY_HIGH,
    ];

    const PRIORITY_LOW = 5;
    const PRIORITY_MEDIUMLOW = 4;
    const PRIORITY_MEDIUM = 3;
    const PRIORITY_MEDIUMHIGH = 2;
    const PRIORITY_HIGH = 1;

    public function __construct(string $template, array $vars=[]);

    public function send(string $toAddress=null, string $toName=null): int;

    public function setSubject(string $subject): self;

    public function setFrom(string $email, string $name=null): self;

    public function setTo(array $recipients): self;

    public function setCc(array $recipients): self;

    public function addCc(string $email, string $name=null): self;

    public function setBcc(array $recipients): self;

    public function addBcc(string $mail, string $name=null): self;

    public function setBody(string $body, string $contentType=null, string $charset=null): self;

    public function addPart(string $body, string $contentType=null, string $charset=null): self;

    public function attach(string $path, string $filename=null): self;

    public function embed(string $path);

    /**
     * @see self::PRIORITIES
     */
    public function setPriority(int $priority): self;

}
