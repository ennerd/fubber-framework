<?php
namespace Fubber\Mailer;

use Fubber\Kernel;
use Fubber\Template;
use Fubber\Mailer;

/**
 * Represents an e-mail message
 */
class Mail implements IMail {
    protected $_template;
    protected $_swift;
    protected $_tos = [];
    protected $_ccs = [];
    protected $_bccs = [];

    public function __construct(string $template, array $vars=[]) {
        if(!is_object($template)) {
            $template = Template::create($template, $vars);
        }
        $this->_swift = new \Swift_Message();
        $this->_swift->setBody($template->__toString(), 'text/html');
        $kernel = Kernel::init();
        if ($kernel->env->emailName) {
            $from = [ $kernel->env->emailAddress => $kernel->env->emailName ];
        } else {
            $from = $kernel->env->emailAddress;
        }
        $this->_swift->setFrom($from);
    }

    /**
     * @return int number of mails sent
     */
    public function send(string $toAddress=null, string $toName=null): int {
        if($toAddress) {
            if($toName)
                $this->_swift->setTo([$toAddress => $toName]);
            else
                $this->_swift->setTo([$toAddress]);
        } else {
            $this->_swift->setTo($this->_tos);
        }
        if(sizeof($this->_ccs)>0) {
            $this->_swift->setCc($this->_ccs);
        }

        if (sizeof($this->_bccs)>0) {
            $this->_swift->setBcc($this->_bccs);
        }

        $mailer = Kernel::init()->get(IMailer::class);
        return $mailer->send($this);
    }

    public function setSubject(string $subject): IMail {
        $this->_swift->setSubject($subject);
        return $this;
    }

    public function setFrom(string $email, string $name=null): IMail {
        if($name)
            $this->_swift->setFrom([$email => $name]);
        else
            $this->_swift->setFrom([$email]);
        return $this;
    }
    
    public function setTo(array $recipients): IMail {
        $this->_tos = $recipients;
        return $this;
    }
    
    public function addTo(string $email, string $name=null): IMail {
        if($name)
            $this->_tos[$email] = $name;
        else
            $this->tos[] = $email;
        return $this;
    }
    
    public function setCc(array $recipients): IMail {
        $this->_ccs = $recipients;
        return $this;
    }
    
    public function addCc(string $email, string $name=null): IMail {
        if($name)
            $this->_ccs[$email] = $name;
        else
            $this->_ccs[] = $email;
        return $this;
    }
    
    public function setBcc(array $recipients): IMail {
        $this->_bccs = $recipients;
        return $this;
    }
    
    public function addBcc(string $email, string $name=null): IMail {
        if($name)
            $this->_bccs[$email] = $name;
        else
            $this->_bccs[] = $email;
        return $this;
    }
    
    public function setBody(string $body, string $contentType=null, string $charset=null): IMail {
        $this->_swift->setBody($body, $contentType, $charset);
        return $this;
    }
    
    public function addPart(string $body, string $contentType=null, string $charset=null): IMail {
        $this->_swift->addPart($body, $contentType, $charset);
        return $this;
    }
    
    public function attach(string $path, string $filename=null): IMail {
        $attachment = \Swift_Attachment::fromPath($path);
        if($filename) $attachment->setFilename($filename);
        $this->_swift->attach($attachment);
        
        return $this;
    }
    
    public function embed(string $path) {
        return $this->_swift->embed(\Swift_EmbeddedFile::fromPath($path));
    }
    
    /**
     * 1 = Highest
     * 5 = Lowest
     */
    public function setPriority(int $priority): IMail {
        $this->_swift->setPriority($priority);
        return $this;
    }
    
    public function _getSwiftMessage() {
        return $this->_swift;
    }
}
