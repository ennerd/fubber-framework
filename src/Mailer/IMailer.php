<?php

namespace Fubber\Mailer;

/**
 * Interface for e-mail sending backends.
 */
interface IMailer {

    /**
     * Send, or queue for sending an e-mail.
     */
    public function send(IMail $message): int;

}
