<?php
namespace Fubber\Mailer;
use \Fubber\Kernel;

class SwiftMailer implements IMailer {

    private \Swift_Transport $swiftMailer;

    public function __construct() {
        $kernel = Kernel::init();
        if ($kernel->env->smtpHost) {
            $transport = new \Swift_SmtpTransport($kernel->env->smtpHost, $kernel->env->smtpPort);
        } else {
            $transport = new \Swift_SendmailTransport("/usr/sbin/sendmail -bs");
        }

        $this->swiftMailer = new \Swift_Mailer($transport);
    }

    public function send(IMail $message): int {
        /**
         * @todo This is a hackish way to get the underlying SwiftMessage implementation.
         */
        return $this->swiftMailer->send($message->_getSwiftMessage());
    }
}
