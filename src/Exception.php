<?php
declare(strict_types=1);

namespace Fubber;

use Fubber\I18n\Translatable;
use Fubber\Traits\ExceptionTrait;
use JsonSerializable;

/**
*	Top level exception for all Fubber Framework generic exceptions.
*/
class Exception extends \Exception implements JsonSerializable, IException {
    use ExceptionTrait;

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return "General Error";
    }

}
