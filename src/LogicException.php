<?php
namespace Fubber;

use Fubber\I18n\Translatable;
use JsonSerializable;

/**
 * The exception for when a logical flaw is detected - not related
 * to the usage of APIs, where the CodingErrorException is more
 * appropriate.
 */
class LogicException extends \LogicException implements JsonSerializable, IException {
    use Traits\ExceptionTrait;

    public function getDefaultStatus(): array {
        return [500, "Internal Server Error"];
    }

    public function getExceptionDescription(): string {
        return 'A logic exception occurred in the application';
    }
}
