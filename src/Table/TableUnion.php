<?php
namespace Fubber\Table;

use ArrayAccess;
use Collator;
use Fubber\NotSupportedException;
use Fubber\Service;
use Traversable;

/**
* This class combines two TableSets into one. This works similarly to using the OR clause in SQL.
* The TableSet number 2 and after will inherit the sort order and column names of the first set.
*/
class TableUnion implements \IteratorAggregate, ITableSet {
	protected $_class;
	/**
	 * @var ITableSet[]
	 */
	protected $_sets = [];
	protected $_limit = 1000;
	protected $_offset = 0;
	protected $_order;
	protected $_orderDesc = false;

	public function __construct(ITableSet ...$sets) {
		if(!isset($sets[1]))
			throw new \TypeError('At least 2 '.ITableSet::class.' sets must be provided.');
		
		$className = $sets[0]->getModelClass();
		$this->_order = $sets[0]->getOrder();
		$this->_orderDesc = $sets[0]->isOrderDesc();
		$columns = $sets[0]->getColumns();

		
		foreach($sets as $set) {
			if($set->getModelClass() !== $className)
				throw new \TypeError("Can't mix TableSets for different classes. Received ".$set->getModelClass().'.');
			$newSet = clone $set;
			if($columns)
				$newSet->columns($columns);
			if($this->_order)
				$newSet->order($this->getOrder(), $this->isOrderDesc());
			$this->_sets[] = $newSet;
		}
	}

	public function getColumnNames(): array {
		return $this->_sets[0]->getColumnNames();
	}

	public function complexWhere(string $sql, $vars = []): ITableSet {
		$sets = [];
		foreach ($this->_sets as $k => $set) {
			$sets[] = $this->_sets[$k]->complexWhere($sql, $vars);
		}
		return new TableUnion(...$sets);
	}

	public function jsonSerialize(): mixed { 
		$parts = [];
		foreach ($this as $row) {
			$parts[] = $row->jsonSerialize();
		}
		return $parts;
	}

	public function parseQuery(array|ArrayAccess $get): IBasicTableSet { 
		$sets = [];
		foreach ($this->_sets as $k => $set) {
			$sets[] = $this->_sets[$k]->parseQuery($get);
		}
		return new TableUnion(...$sets);
	}
	
	public function getModelClass(): string {
		return $this->_class;
	}
	
	public function getOrder(): ?string {
		return $this->_order;
	}
	
	public function getLimit(): int {
		return $this->_limit;
	}
	
	public function getOffset(): int {
		return $this->_offset;
	}
	
	public function isOrderDesc(): bool {
		return $this->_orderDesc;
	}

	public function serialize() {
		return serialize([
			'_sets' => $this->_sets,
			'_limit' => $this->_limit,
			'_order' => $this->_order,
			'_orderDesc' => $this->_orderDesc,
			]);
	}
	
	public function unserialize($string) {
		foreach(unserialize($string) as $k => $v)
			$this->$k = $v;
	}
	
	public function limit(int $limit, int $offset = 0): static
	{
		$this->_limit = $limit;
		$this->_offset = $offset;
		return $this;
	}
	
	public function order($column, $desc = false): static
	{
		$this->_order = $column;
		$this->_orderDesc = $desc;
		foreach($this->_sets as $set) {
			$set->order($column, $desc);
		}
		return $this;
	}
	
	public function getColumns(): ?array {
		return $this->_sets[0]->getColumns();
	}
	
	public function column($column) {
		$this->columns([$column]);
		return $this;
	}
	
	public function columns(?array $columns) {
		foreach($this->_sets as $set)
			$set->columns($columns);
		return $this;
	}
	
	public function getColumnValues($column) {
		throw new \Exception("NOT SUPPORTED");
	}

	public function count(): int {
		global $config;
		$statements = array();
		$vars = array();
		foreach($this->_sets as $s) {
			$set = $s['set'];
			$set->_noLimit = TRUE;
			$sql = $set->_buildSql();
			$set->_noLimit = FALSE;
			$statements[] = $sql[0];
			foreach($sql[1] as $v)
				$vars[] = $v;
		}
		$sql = "(".implode(") UNION (", $statements).")";

		$countSql = 'SELECT COUNT(*) FROM ('.$sql.') AS tmp';
		$res = $config->db->queryField($countSql, $vars);
		return intval($res);
	}

	public function where($field, $op, $value): static {
		foreach($this->_sets as $set)
			$set['set']->where($field, $op, $value);
		return $this;
	}

	public function one(): ?object
	{
        $clone = clone $this;
        $clone->limit(1, $this->getOffset());
        
        // Apparently current($clone) will not work in HHVM
        // Would call unNew() on the result - but that should already have
        // happened in iterator.
        foreach($clone as $result)
            return $result;
	}

	public function getIterator(): Traversable {
		$limit = $this->_limit;
		$offset = $this->_offset;
		$order = $this->getOrder();
		$orderDesc = $this->isOrderDesc();
		
		$sets = [];
		foreach($this->_sets as $origSet) {
			// Clone this, because we don't want to tamper with the originals in case we're iterating again
			$set = clone $origSet;
			if($set->getLimit() > $this->getLimit())
				$set->limit($this->getLimit(), $set->getOffset());
				
			if($order)
				$set->order($order, $orderDesc);
			if(is_a($set, \IteratorAggregate::class))
				$sets[] = $set->getIterator();
			else
				$sets[] = $set;
		}
		
		if($order) {
			$generator = function() use($sets, $offset, $limit, $order, $orderDesc) {
				// Must find the lowest value
                $collator = Service::use(Collator::class);
//				$collator = \Nerd\Glue::get(\Collator::class);

				while(true) {
					$items = [];
					foreach($sets as $setKey => $set) {
						if($item = $set->current())
							$items[] = [$item, $set, $setKey];
					}
					
					if(sizeof($items)===0) return;
	
					$lowest = null;
					$lowestVal = null;
					
					foreach($items as $key => $item) {
						if($lowest === null) {
							$lowest = $key;
							$lowestVal = $item[0]->$order;
						}
						else {
							$strings = null;
							if($strings === true || ($strings === null && is_string($lowestVal))) {
								$strings = true;
								// Should use collator
								$res = $collator->compare($lowestVal, $item[0]->$order);
								if($orderDesc) {
									if($res < 0) {
										$lowest = $key;
										$lowestVal = $item[0]->$order;
									}
								} else {
									if($res > 0) {
										$lowest = $key;
										$lowestVal = $item[0]->$order;
									}
								}
							} else {
								$strings = false;
								if($orderDesc) {
									if($item[0]->$order > $items[$lowest][0]->$order)
										$lowest = $key;
								} else {
									if($item[0]->$order < $items[$lowest][0]->$order)
										$lowest = $key;
								}
							}
						}
					}
					
					if(--$offset < 0) {
						if($limit-- > 0) {
							$items[$lowest][1]->next();
							if(!$items[$lowest][1]->valid())
								unset($sets[$items[$lowest][2]]);
							yield($items[$lowest][0]);
						} else {
							// We have found enough items
							return;
						}
					}
				}
			};
		} else {
			$generator = function() use($sets, $offset, $limit) {
				while($set = array_shift($sets)) {
					foreach($set as $item) {
						if(--$offset < 0)
							if($limit-- > 0)
								yield($item);
					}
				}
			};
		}
		foreach($generator() as $item)
			yield($item);
	}
}
