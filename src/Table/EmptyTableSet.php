<?php
namespace Fubber\Table;

use Traversable;

/**
 * Useful method for returning an empty result set, for example if filtering will remove all items.
 *
 * <code>
 * <?php
 * return new EmptyTableSet('User'); // User class must extend the Table class
 * ?>
 * </code>
 * 
 * @template T
 */
class EmptyTableSet extends \Fubber\Table\TableSet {

	/**
	*	@see TableSet::count()
	*/
	public function count(): int {
		return 0;
	}
	
	public function getColumnValues(string $column) {
		return [];
	}

	/**
	 * @see TableSet::one()
	 * 
	 * @template ?T
	 */
	public function one(): ?object {
		return NULL;
	}

	/**
	 * @see \IteratorAggregate::getIterator()
	 *
	 * @return Traversable<T>
	 */
	public function getIterator(): Traversable {
		return new \EmptyIterator();
	}

    public function jsonSerialize() {
        return [];
    }
}
