<?php
namespace Fubber\Table;

/**
 * Common ancestor for Table and TableSet, allows for sharing some data between
 * classes without making values public.
 */
class TableCore {
    protected static $_staticShared = [];
    protected $_shared = [];
}
