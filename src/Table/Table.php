<?php
namespace Fubber\Table;

use ArgumentCountError;
use Fubber\Kernel\State;
use Fubber\IValidatable;

/**
* You normally want to implement a class from Fubber\Table\Backends in your models.
*/
abstract class Table extends TableCore implements ITable, IValidatable
{
    /**
     * Return the name(s) of the primary key properties
     *
     * @return string[]
     */
    abstract public static function getPrimaryKeyNames(): array;

    /**
     * Return the names of all the storable properties
     * 
     * @return string[]
     */
    abstract public static function getColumnNames(): array;

    /**
     * Delete this row.
     * Must call $this->beforeDelete() before deleting.
     * If deletion is successful, make sure that $this->isNew() returns true
     * Must call $this->afterDelete() after successful delete.
     *
     * @return bool
     */
    abstract public function delete();

    abstract public static function getForeignKeys(): array;

    /**
     * Save this row. Take care to save only the columns specified.
     *
     * Must call $this->beforeSave($columns) before actually saving.
     * If save was successful, make sure that $this->isNew() returns false.
     * Must call $this->afterSave($columns) after successful save.
     *
     * @return self
     */
    abstract public function save(array $columns = null);

    /**
     * Return a new instance of ITableSet corresponding to every row in the backend.
     * 
     * @returns IBasicTableSet<static>
     */
    abstract public static function allUnsafe(): IBasicTableSet;

    /**
     * Return true if this object was created, not retrieved and has not been stored
     * to the database.
     */
    abstract public function isNew(): bool;

    /**
     * This method may be overridden by implementors to handle special initial filtering
     * of the data - for example to remove expired values, or deleted rows. Should not
     * be used for access filtering.
     * 
     * @return IBasicTableSet<static>
     */
    public static function all() {
        return static::allUnsafe();
    }

    /**
     * Create a new instance of this class. May be used to set properties to initial values
     * etc.
     * 
     * @return static
     */
    public static function create(?State $state) {
        return new static();
    }

    /**
     * This ensures that we don't get undeclared property notice on new objects
     */
    public function __get($name) {
        return null;
    }

    public static function load(...$values): ?static {
        $result = static::all();
        foreach (static::getPrimaryKeyNames() as $i => $keyName) {
            if ($i > 0 && $i >= sizeof($values)) {
                throw new \ArgumentCountError(static::class.'::load() expects '.sizeof(static::getPrimaryKeyNames()).' arguments.');
            }
            $result->where($keyName, '=', $values[$i]);
        }
        return $result->one();
    }

    /**
     * @param mixed $values 
     * @return static|null 
     * @throws ArgumentCountError 
     */
    public static function loadUnsafe(...$values) {
        $values = func_get_args();
        $result = static::allUnsafe();
        foreach (static::getPrimaryKeyNames() as $i => $keyName) {
            if ($i > 0 && $i >= sizeof($values)) {
                throw new \ArgumentCountError(static::class.'::loadUnsafe() expects '.sizeof(static::getPrimaryKeyNames()).' arguments.');
            }
            $result->where($keyName, '=', $values[$i]);
        }
        return $result->one();
    }

    public static function exists(...$values): bool {
        return static::load(...$values) ? true : false;
    }

    public static function existsUnsafe(...$values): bool {
        return static::loadUnsafe(...$values) ? true : false;
    }

    /**
     * Called before saving. It should be safe to call this function several times
     * before an actual save is performed.
     *
     * @throws \Exception
     * @return self
     */
    public function beforeSave(array $columns = null) {
        return $this;
    }

    /**
     * Called ONCE after successful save
     *
     * @return self
     */
    public function afterSave(array $columns = null) {
        return $this;
    }

    /**
     * Called before deleting.
     *
     * @throws \Exception
     * @return self
     */
    public function beforeDelete() {
        return $this;
    }

    /**
     * Called after successful delete.
     *
     * @return self
     */
    public function afterDelete() {
        return $this;
    }

    /**
     * Return null if the object has no errors. Return an array
     * of key-value pairs with error messages, if there are validation errors
     * on any of the properties.
     *
     * @return array|null
     */
    public function isInvalid() {
        return null;
    }

    /**
     * @see \JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize() {
        $res = [];

        foreach ($this->getColumnNames() as $col) {
            $res[$col] = $this->$col;
        }

        return $res;
    }

    /**
     * Return many rows in a single query. Mostly for optimization purposes.
     * 
     * @return array<static>
     */
    public static function loadMany(array $keys): array {
        $res = [];
        foreach ($keys as $k) {
            if (is_array($k)) {
                $res[$k] = static::load(...$k);
            } else {
                $res[$k] = static::load($k);
            }
        }
        return $res;
    }
}
