<?php
namespace Fubber\Table;

use JsonSerializable;

/**
 * Represents a persistent backend for storing objects and properties. Can be powered
 * by SQL-databases, CSV-files or whatever backend you want. Must complement an 
 * ITableSet class.
 */

interface ITableLegacy extends JsonSerializable {
    /**
     * Return the declared column names of this table source
     */
    public static function getCols(): array;

    /**
     * Create an instance of this class. Optionally seed the object with values
     * such as created date or created by.
     * 
     * @return static
     */
    public static function create();

    /**
     * Return an ITableSet that should be available to the user. Often, this means
     * all rows. Here you can remove rows that are in trash or that the user
     * does not have access to.
     *
     * @return ITableSet<static>
     */
    public static function all();

    /**
     * Return all rows without question
     * 
     * @return ITableSet<static>
     */
    public static function allUnsafe();
    
    /**
     * Return one row by primary key, subject to same filtering as in 
     * ITable::all().
     * 
     * @return static|null
     */
    public static function load($key);
    
    /**
     * Returns an associative array of keys => object instance
     * 
     * @return array<static>
     */
    public static function loadMany(array $keys);
    
    /**
     * Return one row, without access filtering.
     * 
     * @return static|null
     */
    public static function loadUnsafe($key);
    
    /**
     * Check that the primary key exists.
     * 
     * @return bool
     */
    public static function exists($key);

    /**
     * Check that the primary key exists.
     * 
     * @param mixed $key 
     * @return bool
     */
    public static function existsUnsafe($key);
    
    /**
     * Delete this row from the database. Throw \Fubber\AccessDeniedException if
     * access denied.
     * 
     * @return bool
     */
    public function delete();
    
    /**
     * Save the row to the database
     * 
     * @return bool
     */
    public function save(array $properties = null);
    
    /**
     * Return the primary key value of this object.
     * 
     * @return mixed
     */
    public function getPrimaryKey(): array;
    
    /**
     * Return the primary key property name of this object.
     * 
     * @return string
     */
    public static function getPrimaryKeyName();
    
    /**
     * Returns false once the row is saved.
     * Returns false if the row came from a backend.
     * 
     * @return bool
     */
    public function isNew(): bool;
    
    /**
     * Must be called after saving, and for every object that comes from
     * a backend storage.
     * 
     * @return self
     */
    public function unNew();
    
    /**
     * Must be called if the object is deleted from the database, or in other
     * ways considered not saved to database.
     * 
     * @return self
     */
    public function reNew();
}
