<?php
namespace Fubber\Table\Attributes;

use Attribute;

/**
 * Declare properties as column names for mapping to the database
 */
#[Attribute(Attribute::TARGET_PROPERTY | Attribute::IS_REPEATABLE)]
class Column {

}