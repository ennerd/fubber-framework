<?php
namespace Fubber\Table\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class TableBackend {

    public readonly string $tableName;

    public function __construct(string $tableName) {
        $this->tableName = $tableName;
    }
}