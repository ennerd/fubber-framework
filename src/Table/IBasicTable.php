<?php
namespace Fubber\Table;

use JsonSerializable;

/**
 * Represents a persistent backend for storing objects and properties. Can be powered
 * by SQL-databases, CSV-files or whatever backend you want. Must complement an 
 * ITableSet class.
 */
interface IBasicTable extends JsonSerializable {

    /**
     * Return one row by primary key, subject to same filtering as in
     * ITable::all().
     *
     * @return static|null
     */
    public static function load(...$values);

    /**
     * Return an ITableSet that should be available to the user. Often, this means
     * all rows. Here you can remove rows that are in trash or that the user
     * does not have access to.
     *
     * @return IBasicTableSet<static>
     */
    public static function all();

    /**
     * Return all rows without question
     *
     * @return IBasicTableSet<static>
     */
    public static function allUnsafe();

    /**
     * Return the primary key property name of this object.
     *
     * @return string[]
     */
    public static function getPrimaryKeyNames(): array;

    /**
     * Return the names of all the storable properties
     * 
     * @return string[]
     */
    public static function getColumnNames(): array;

    /**
     * Return the column names that are foreign keys to other models in the form [ 'keyName' => 'className' ].
     * 
     * @return array<string, class-string>
     */
    public static function getForeignKeys(): array;

    /**
     * Returns false once the row is saved.
     * Returns false if the row came from a backend.
     *
     * @return bool
     */
    public function isNew(): bool;

    /**
     * Delete this row from the database. Throw \Fubber\AccessDeniedException if
     * access denied.
     *
     * @return bool
     */
    public function delete();

    /**
     * Save the row to the database
     *
     * @return bool
     */
    public function save(array $properties = null);

    /**
     * Return null if the object has no errors. Return an array
     * of key-value pairs with error messages, if there are validation errors
     * on any of the properties.
     *
     * @return array|null
     */
    public function isInvalid();
}
