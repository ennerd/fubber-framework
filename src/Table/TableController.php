<?php
namespace Fubber\Table;

use Fubber\{Hooks};

class TableController {

    public static function init() {

        // Filter database queryies to allow using IBasicTable objects to be used as values in queries
        Hooks::listen('Fubber\Db\IDb::query', \Closure::fromCallable([ static::class, 'filter_query_vars' ]));
        Hooks::listen('Fubber\Db\IDb::queryOne', \Closure::fromCallable([ static::class, 'filter_query_vars' ]));
        Hooks::listen('Fubber\Db\IDb::queryField', \Closure::fromCallable([ static::class, 'filter_query_vars' ]));
        Hooks::listen('Fubber\Db\IDb::queryColumn', \Closure::fromCallable([ static::class, 'filter_query_vars' ]));
        Hooks::listen('Fubber\Db\IDb::exec', \Closure::fromCallable([ static::class, 'filter_query_vars' ]));
    }

    /**
     * Replaces IBasicTable objects with their primary key value in arrays 'vars' key.
     */
    protected static function filter_query_vars(array $args, $self) {

        if (empty($args['vars'])) {
            return [];
        }

        $vars = $args['vars'];

        \array_walk($vars, function(&$value) {
            if ($value instanceof \Fubber\Table\IBasicTable) {
                $pks = $value::getPrimaryKeyNames();
                if (sizeof($pks)===1) {
                    $pk = $pks[0];
                    $value = $value->$pk;
                    $did = true;
                }
            }
        });

        return ['vars' => $vars];
    }

}
