<?php
namespace Fubber\Table;

use Fubber\Kernel\State;
use JsonSerializable;
use Traversable;

/**
 * Represents a persistent backend for storing objects and properties. Can be powered
 * by SQL-databases, CSV-files or whatever backend you want. Must complement an 
 * ITableSet class.
 * 
 * @template T of static
 * @inherits IBasicTable<T>
 */
interface ITable extends IBasicTable {
    /**
     * Create an instance of this class. Optionally seed the object with values
     * such as created date or created by.
     *
     * @return static
     */
    public static function create(?State $state);


    /**
     * Return an ITableSet that should be available to the user. Often, this means
     * all rows. Here you can remove rows that are in trash or that the user
     * does not have access to.
     * 
     * @return ITableSet<T>
     */
    public static function all();

    /**
     * Return all rows without question
     *
     * @return ITableSet<T>
     */
    public static function allUnsafe();

    /**
     * Returns an associative array of keys => object instance
     * 
     * @return array<T>
     */
    public static function loadMany(array $keys): array;

    /**
     * Return one row, without access filtering.
     *
     * @return T|null
     */
    public static function loadUnsafe(...$values);

    /**
     * Check that the primary key exists.
     *
     * @return bool
     */
    public static function exists(...$key): bool;

    /**
     * Check that the primary key exists without filtering rows
     *
     * @param mixed ...$key
     * @return boolean
     */
    public static function existsUnsafe(...$key): bool;

    /**
     * Returns true if the row is dirty (if it should be saved)
     */
    public function isDirty(): bool;
}
