<?php
namespace Fubber\Table;

use ArrayAccess;
use Fubber\CodingErrorException;
use InvalidArgumentException;

abstract class ImmutableTableSet extends TableSet {
    public function where($column, $operator, $value): IBasicTableSet {
        if (!in_array($column, $this->getColumnNames())) {
            $columnNames = implode("', '", $this->getColumnNames());
            throw new InvalidArgumentException("Unknown column name '$column', available column names are '".$columnNames."'");
        }
        return (clone $this)->_where($column, $operator, $value);
    }

    private function _where($column, $operator, $value): IBasicTableSet {
        parent::where($column, $operator, $value);
        return $this;
    }

    public function order($column, $desc = false): IBasicTableSet {
        return (clone $this)->_order($column, $desc);
    }

    private function _order($column, $desc = false): IBasicTableSet {
        parent::order($column, $desc);
        return $this;
    }

    public function limit(int $limit, int $offset = 0): IBasicTableSet {
        return (clone $this)->_limit($limit, $offset);
    }

    private function _limit(int $limit, int $offset = 0): IBasicTableSet {
        parent::limit($limit, $offset);
        return $this;
    }


    /**
     * @see ITableSet::columns()
     */
    public function columns(?array $columns) {
        return (clone $this)->columns($columns);
    }

	public function complexWhere(string $sql, $vars=[]): ITableSet {
        return (clone $this)->complexWhere($sql, $vars);
    }

        /**
     * @see ITableSet::parseQuery()
     */
    public function parseQuery(ArrayAccess|array $get): IBasicTableSet {
        return (clone $this)->parseQuery($get);
    }

}
