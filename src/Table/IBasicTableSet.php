<?php
namespace Fubber\Table;

use Closure;

#todo: support computed columns, prefixed with # or @ or somethign

/**
 * @template T of IBasicTable
 * @extends Traversable<T>
 */
interface IBasicTableSet extends \Traversable, \Countable, \Serializable, \JsonSerializable {

    /**
     * Filters the result by comparing $column using $operator to $value.
     * May be called several times, to combine multiple.
     *
     * Acceptable operators MUST include '=', '>', '<', '>=', '<=', 'IN'
     *
     * If $value is an instance of ITable, use $value->getPrimaryKey()
     *
     * The special operator 'IN' must accept an array of values, as well as
     * an ITableSet where a single column has been specified using ::column($name)
     *
     * Suggest implementing this in SQL using the EXISTS subquery, or using IN
     * with constants.
     *
     * @param string $column 
     * @param string $operator 
     * @param mixed $value 
     * @return static
     */
    public function where($column, $operator, $value);

    /**
     * Define a sort order for the result set.
     *
     * @param string $column 
     * @param bool $desc 
     * @return static
     */
    public function order($column, $desc = false);

    /**
     * Define a limit for the result set. The default limit should be 1000 rows.
     *
     * @return static
     */
    public function limit(int $limit, int $offset = 0): IBasicTableSet;

    /**
     * Return the sort order defined
     *
     * @see self::order()
     *
     * @return string
     */
    public function getOrder(): ?string;

    /**
     * Return if the sort order is descending
     *
     * @see self::order()
     *
     * @return bool
     */
    public function isOrderDesc(): bool;

    /**
     * Get the row count limit
     *
     * @return integer
     */
    public function getLimit(): int;

    /**
     * Get the row start offset
     *
     * @return integer
     */
    public function getOffset(): int;

    /**
     * @see ITableSet::getModelClass()
     * @return class-string<T>
     */
    public function getModelClass(): string;

    /**
	 * Parses queries like ?birthday[>]=2018-09-20&name=Frode
	 *
	 * fieldname[>]=value			fieldname > value
	 * fieldname[<]=value			fieldname < value
	 * fieldname[>=]=value			fieldname >= value
	 *
	 * l = limit
	 * o = offset
     * s = sort (keyName for ascending sort, -keyName for descending sort)
     * 
     * @return static
	 */
	public function parseQuery(array $get);

    /**
     * Get one row
     *
     * @return T|null
     */
    public function one();
}
