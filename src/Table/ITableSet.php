<?php
namespace Fubber\Table;

use Traversable;

/**
 * @template T of IBasicTable
 * @package Fubber\Table
 * @extends IBasicTableSet<T>
 * @extends Traversable<T>
 */
interface ITableSet extends \JsonSerializable, \Traversable, \Countable, \Serializable, IBasicTableSet {

    /**
     * Return all the values of a specific column in the query
     * 
     * @return array<mixed>
     */
    public function getColumnValues(string $column);

    /**
     * Specify which fields we want to fetch
     *
     * @return static
     */
    public function columns(?array $columns);
    
    /**
     * Deprecated: Specify a single field we want to fetch. Same as calling ITableSet::columns([$field])
     * 
     * @return static
     */
    public function column(string $column);
    
    /**
     * Return null, if no specific columns have been requested, or returns an array
     * of column names.
     * 
     * @return string[]|null
     */
    public function getColumns(): ?array;

    /**
     * Returns an array of columns that exists for this table.
     * 
     * @return string[]
     */
    public function getColumnNames(): array;

    /**
     * Use normal SQL syntax for a table query. For example
     * `complexWhere("id = ? OR id = ?", [ 11, 12 ])`. May not be supported by all backends.
     * 
     * @return static
     */
    public function complexWhere(string $sql, $vars=[]): ITableSet;
}
