<?php
namespace Fubber\Table;

use ArrayAccess;
use Traversable;

/**
 * @template T
 * @extends ITableSet<T>
 */
abstract class TableSet extends TableCore implements \IteratorAggregate, ITableSet {
    /**
     * The columns we're fetching, if fetching specific columns
     */
    protected $_class;
    protected $_columns = null;
    protected $_wheres = [];
    protected $_order = null;
    protected $_orderDesc = false;
    protected $_offset = 0;
    protected $_limit = 1000;
    protected $_extra = [];
    protected $_options;

    public function __construct(string $className, array $options = null) {
        if(!is_a($className, ITable::class, true)) {
            throw new \TypeError("The class '$className' must implement '".ITable::class."'");
        }

        $this->_class = $className;

        $this->_options = $options;
    }

    /**
     * @see ITableSet::getModelClass()
     * @return class-string<T>
     */
    public function getModelClass(): string {
        return $this->_class;
    }

    /**
     * {@inheritDoc}
     * 
     * @return T|null
     */
    public function one(): ?object {
        $clone = clone $this;
        $clone->limit(1);

        foreach($clone as $result) {
            return $result;
        }

        return null;
    }

    /**
     * Must return an iteratable result. For example an array of ITable objects.
     *
     * @see \IteratorAggregate::getIterator()
     * 
     * @return Traversable<T>
     */
    abstract public function getIterator(): Traversable;

    /**
     * @see ITableSet::getColumnValues()
     */
    abstract public function getColumnValues(string $column);

    /**
     * @see ITableSet::getColumns()
     */
    public function getColumns(): ?array {
        return $this->_columns;
    }

    /**
     * @see ITableSet::getColumnNames()
     * 
     * @return string[]
     */
    public function getColumnNames(): array {
        return ($this->getModelClass())::getColumnNames();
    }

    /**
     * Deprecated function to get the class name of the data model.
     *
     * @deprecated 1.1
     * @return class-string<T>
     */
    public function getClassName(): string {
        return $this->getModelClass();
    }

    /**
     * Add a criteria to the source data
     *
     * @see ITableSet::where()
     * @return $this
     */
    public function where($column, $operator, $value): IBasicTableSet {
        $this->_wheres[] = [$column, $operator, $value];
        return $this;
    }

    /**
     * 
     * @param mixed $column 
     * @param bool $desc 
     * @return $this 
     */
    public function order($column, $desc = false): IBasicTableSet {
        $this->_order = $column;
        $this->_orderDesc = $desc;
        return $this;
    }

    public function limit(int $limit, int $offset = 0): IBasicTableSet {
        $this->_limit = $limit;
        $this->_offset = $offset;
        return $this;
    }

    /**
     * @see ITableSet::columns()
     */
    public function columns(?array $columns) {
        $this->_columns = $columns;
        return $this;
    }

    protected function getWheres(): array {
        return $this->_wheres;
    }

    /**
     * @see ITableSet::column()
     */
    public function column(string $column) {
        return $this->columns([$column]);
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($string) {
        $vals = unserialize($string);
        $this->_class = $vals['class'];
        $this->_columns = $vals['columns'];
        $this->_wheres = $vals['wheres'];
        $this->_order = $vals['order'];
        $this->_orderDesc = $vals['orderDesc'];
        $this->_offset = $vals['offset'];
        $this->_limit = $vals['limit'];
        $this->_options = $vals['options'];
        $this->_extra = $vals['extra'];
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize([
            'class' => $this->_class,
            'columns' => $this->_columns,
            'wheres' => $this->_wheres,
            'order' => $this->_order,
            'orderDesc' => $this->_orderDesc,
            'offset' => $this->_offset,
            'limit' => $this->_limit,
            'options' => $this->_options,
            'extra' => $this->_extra,
            ]);
    }

    public function getOrder(): ?string {
        return $this->_order;
    }

    public function isOrderDesc(): bool {
        return $this->_orderDesc;
    }

	public function getLimit(): int {
		return $this->_limit;
	}

	public function getOffset(): int {
		return $this->_offset;
	}

	public function complexWhere(string $sql, $vars=[]): ITableSet {
        throw new Exception("Table::complexWhere is not supported for this backend");
    }

    /**
     * @see ITableSet::parseQuery()
     */
    public function parseQuery(array|ArrayAccess $get): IBasicTableSet {
        $model = static::getModelClass();
        $cols = $model::getColumnNames(); //static::getCols();
        foreach ($cols as $col) {
            if (isset($get[$col])) {
                $val = $get[$col];
                if (is_array($val)) {
                    foreach ($val as $op => $v) {
                        $this->where($col, $op, $v);
                    }
                } else {
                    $this->where($col, '=', $val);
                }
            }
        }
        if (isset($get['l']) && intval($get['l'])>1000) {
            throw new Exception("Not possible to fetch more than 1000 rows per query");
        }
        if (isset($get['l']) && isset($get['o'])) {
            $this->limit($get['l'], $get['o']);
        } elseif (isset($get['l'])) {
            $this->limit($get['l']);
        } elseif (isset($get['o'])) {
            $this->limit(1000, $get['o']);
        }
        
        if (isset($get['s'])) {
            if (in_array($get['s'], $cols)) {
                $this->order($get['s']);
            }
        }

        return $this;
    }

    public function jsonSerialize() {
        $res = [];
        foreach($this as $row) {
            $res[] = $row;
        }
        return $res;
    }
}
