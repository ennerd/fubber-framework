<?php
namespace Fubber\Table\Backends;

use Fubber\CodingErrorException;
use Fubber\Db\IDb;
use Fubber\Service;
use Fubber\Table\Table;
use Fubber\InvalidArgumentException;
use Fubber\Table\Attributes\Column;
use Fubber\Table\Attributes\TableBackend;
use Fubber\Table\IBasicTableSet;
use Fubber\Table\Exception;
use Fubber\ValidationException;
use ReflectionClass;

class DbTable extends Table {
    private $checksum = null;

    public function isDirty(): bool {
        if ($this->checksum === null) {
            return true;
        }
    }

    /**
     * @see Table::getPrimaryKeyNames()
     */
    public static function getPrimaryKeyNames(): array {
        if (isset(static::$_staticShared[static::class]['primaryKeyNames'])) {
            return static::$_staticShared[static::class]['primaryKeyNames'];
        }
        if (\property_exists(static::class, '_primaryKey') && !empty(static::$_primaryKey)) {
            if (is_array(static::$_primaryKey)) {
                return static::$_staticShared[static::class]['primaryKeyNames'] = static::$_primaryKey;
            } else {
                return static::$_staticShared[static::class]['primaryKeyNames'] = [ static::$_primaryKey ];
            }
        }
        return static::$_staticShared[static::class]['primaryKeyNames'] = [ static::getColumnNames()[0] ];
    }

    /**
     * @see IBasicTableSet::getForeignKeys()
     */
    public static function getForeignKeys(): array {
        if (array_key_exists(static::class, static::$_staticShared) && array_key_exists('foreignKeys', static::$_staticShared[static::class])) {
            return static::$_staticShared[static::class]['foreignKeys'];
        }
        $fks = [];
        if (\property_exists(static::class, '_foreignKeys')) {
            foreach (static::$_foreignKeys as $keyName => $model) {
/*
    don't need this check
                if (!class_exists($model)) {
                    throw new Exception("Foreign key '$keyName' refers to a model '$model' that does not exist.");
                }
*/
                $fks[$keyName] = $model;
            }
        }
        return static::$_staticShared[static::class]['foreignKeys'] = $fks;
    }

    public static function getColumnNames(): array {
        if (isset(static::$_staticShared[static::class]['columnNames'])) {
            return static::$_staticShared[static::class]['columnNames'];
        }

        /**
         * Get column names from properties with the Column attribute
         */
        $properties = (new ReflectionClass(static::class))->getProperties();
        $reflectionPropertyNames = [];
        foreach ($properties as $property) {
            $attributes = $property->getAttributes(Column::class);
            if ($attributes === []) {
                continue;
            }
            $reflectionPropertyNames[] = $property->getName();
        }
        if ($reflectionPropertyNames !== []) {
            return static::$_staticShared[static::class]['columnNames'] = $reflectionPropertyNames;
        }

        if (\property_exists(static::class, '_cols')) {
            return static::$_staticShared[static::class]['columnNames'] = static::$_cols;
        }
        throw new CodingErrorException("Unable to determine column names. Override the ".static::class."::getColumnNames(): array or declare ".static::class."::\$_cols = []");
    }

    public static function getTableName(): string
    {
        if (isset(static::$_staticShared[static::class]['tableName'])) {
            return static::$_staticShared[static::class]['tableName'];
        }
        $attributes = (new ReflectionClass(static::class))->getAttributes(TableBackend::class);
        if ($attributes !== []) {
            $tableName = $attributes[0]->getArguments()[0];
            return static::$_staticShared[static::class]['tableName'] = $tableName;
        }
        if (\property_exists(static::class, '_table')) {
            return static::$_staticShared[static::class]['tableName'] = static::$_table;
        }
        throw new CodingErrorException("Unable to determine table name. Declare the #[TableBackend('tableName')] class attribute.");
    }

    /**
     * Return a database connection to use for this object. Override this to implement table level
     * sharding.
     *
     * @return IDb
     */
    public static function getDb(array $options=[]): IDb {
        return Service::use(IDb::class);
    }

    final public function isNew(): bool {
        return empty($this->_shared['fromDb']);
    }

    /**
     * @see Table::delete();
     */
    public function delete() {
        if ($this->isNew()) {
            return false;
        }
        $this->beforeDelete();
        $values = [];
        $wheres = array_map(function($col) use (&$values)  {
            $values[] = $this->$col;
            return '`'.$col.'`=?';
        }, $this->getPrimaryKeyNames());
        $res = static::getDb()->exec('DELETE FROM `'.static::getTableName().'` WHERE '.implode(" AND ", $wheres), $values).' LIMIT 1';
        if ($res) {
            $this->_shared['fromDb'] = true;
        }
        return $res;
    }

    /**
     * @see Table::save()
     */
    private $_skipBeforeSave = false;
    public function save(array $columns = null): bool {
        if (!$this->_skipBeforeSave) {
            $this->beforeSave($columns);
            $this->_skipBeforeSave = false;
        }

        if ($columns !== null) {
            foreach (array_diff($columns, static::getColumnNames()) as $missingColumnName) {
                throw new InvalidArgumentException("Column name '$missingColumnName' is not declared for the class '".static::class."'");
            }
        } else {
            $columns = static::getColumnNames();
        }

        $errors = $this->isInvalid();
        if($errors) {
            throw new ValidationException($errors);
        }

        if ($this->isNew()) {
            $values = array_map(function ($col) {
                return $this->$col;
            }, $columns);
            $sql = 'INSERT INTO '.static::getTableName().' ('.implode(",", $columns).') VALUES ('.implode(",", array_fill(0, sizeof($columns), '?')).')';
            try {
                $done = static::getDb()->exec($sql, $values);
            } catch (\PDOException $e) {
                if (static::tryUpdateSchema()) {
                    $this->_skipBeforeSave = true;
                    return $this->save($columns, true);
                }
                throw $e;
            }
            if ($done) {
                $this->_shared['fromDb'] = true;
                $pk = $this->getPrimaryKeyNames();
                if (sizeof($pk) === 1) {
                    $pk = $pk[0];
                    if (empty($this->$pk)) {
                        $pkValue = static::getDb()->lastInsertId();
                        if ($pkValue) {
                            $this->$pk = (int) static::getDb()->lastInsertId();
                        }
                    }
                }
            }
        } else {
            $pairs = array_map(function ($col) {
                return $col.'=?';
            }, $columns);
            $values = array_map(function ($col) {
                return $this->$col;
            }, $columns);
            $pkPairs = [];
            foreach ($this->getPrimaryKeyNames() as $pkName) {
                $pkPairs[] = "`$pkName`=?";
                $values[] = $this->$pkName;
            }
            $sql = 'UPDATE '.static::getTableName().' SET '.implode(",", array_map(function ($col) {
                return $col.'=?';
            }, $columns)).' WHERE '.implode(" AND ", $pkPairs);
            try {
                $done = static::getDb()->exec($sql, $values);
            } catch (\PDOException $e) {
                if (static::tryUpdateSchema()) {
                    $this->_skipBeforeSave = true;
                    return $this->save($columns);
                }
                throw $e;
            }
        }

        $this->afterSave($columns);
        return $done;
    }

    /**
     * @see Table::allUnsafe()
     */
    public static function allUnsafe(): IBasicTableSet {
        return new DbTableSet(static::class);
    }

    public function jsonSerialize() {
        $res = [];

        foreach ($this->getColumnNames() as $col) {
            $res[$col] = $this->$col;
        }

        return $res;
    }

    private static function tryUpdateSchema() {
        if (!method_exists(static::class, 'updateSchema')) {
            return false;
        }
        if (isset(static::$_staticShared[static::class]['updatedSchema'])) {
            return false;
        }
        static::$_staticShared[static::class]['updatedSchema'] = true;
        static::updateSchema(static::getDb());
        return true;
    }
}
