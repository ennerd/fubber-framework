<?php
namespace Fubber\Table\Backends;

use Collator;
use Fubber\{Table\TableSet, Table\Table};
use Fubber\Service;
use Traversable;

class CsvTableSet extends TableSet {
    use Traits\WhereTester;
    
    public function __construct($className) {
        if(!is_a($className, CsvTable::class, true))
            throw new \TypeError($className.' must extend '.CsvTable::class);
        parent::__construct($className);
    }
    
    public function getColumnValues($column) {
        
    }
    
    public function count(): int {
        return 0;
    }
    
    public function getIterator(): Traversable {
        $availMemory = str_replace(array('G', 'M', 'K'), array('000000000', '000000', '000'), ini_get('memory_limit'));
        $availMemory -= memory_get_usage();
        $filesize = filesize($this->getModelClass()::getFileName());
        $shouldBuffer = 0.1 > ($filesize * 10) / $availMemory;
        $source = $this->fileIterator();                                        // All rows that match the where clauses
        if($shouldBuffer)
            $source = $this->bufferedIterator($source);
        
        $source->current();
        $t = microtime(true);
        if($this->getOrder()) {
            $collator = Service::use(Collator::class);
//            $collator = \Nerd\Glue::get(\Collator::class);
            $key = $this->getOrder();
            if($this->isOrderDesc())
                $cmp = function($a,$b) use($collator, $key) {
                    return $collator->compare($a[$key], $b[$key]) * -1;
                };
            else
                $cmp = function($a,$b) use($collator, $key) {
                    return $collator->compare($a[$key], $b[$key]);
                };
            $source = $this->nthLowestIterator($source, $this->getOffset() + $this->getLimit(), $cmp);
            $source = $this->skipIterator($source, $this->getOffset());
        } else {
            if($this->getOffset()>0)
                $source = $this->skipIterator($source, $this->getOffset());
            if($this->getLimit())
                $source = $this->limitIterator($source, $this->getLimit());
        }
        foreach($source as $row)
            yield($row);
    }
    
    /**
     * Keeps only rows that would sort in the first $n elements
     */
    protected function &nthLowestIterator($source, $n, $cmp) {
        $lowerHalf = $this->callbackGenerator($this->lowerHalfIterator($source, $n, $cmp, 0.2));
        $count = 0;
        $result = [];
        foreach($lowerHalf as &$item) {
            $result[] = $item;
            $count++;
        }
        if(isset($item['_heap'])) {
            $heap = &$item['_heap'];
            unset($item['_heap']);
        } else {
            $heap = [];
        }
        $count--;
        $split = array_shift($result);
        $result[] = $split;

        $heapCount = sizeof($heap);
        $totalCount = $count + $heapCount;
        if($totalCount < min($n * 10, $n + 5000)) {
            $t = microtime(true);
            $result = array_merge($result, $heap);
            usort($result, $cmp);
            foreach($this->limitIterator($result, $n) as $item) {
                yield($item);
            }
            return;
        } /* else if($count + sizeof($heap) < $n) {
            foreach($result as $item)
                yield($item);
            foreach($heap as $item)
                yield($item);
        } else if($count === $n) {
            // We have arrived!
            foreach($this->quickSortIterator($result, $cmp) as $item) {
                yield($item);
            }
        }*/ else if($count > $n) {
            // $result has enough answers, but is too big so we'll run again
            foreach($this->nthLowestIterator($result, $n, $cmp) as $item) {
                yield($item);
            }
        } else {
            // $result is too small, and the $heap has more
            // must sort here, to be consistent
//            foreach($this->quickSortIterator($this->arrayThenIteratorIterator($result, $this->nthLowestIterator($heap, $n - $count, $cmp)), $cmp) as $item) {
            foreach($this->arrayThenIteratorIterator($this->quickSortIterator($result, $cmp), $this->nthLowestIterator($heap, $n - $count, $cmp)) as $item) {
                yield($item);
            }
        }
    }
    
    protected function arrayThenIteratorIterator($array, $iterator) {
        foreach($array as $item)
            yield($item);
        foreach($iterator as $item)
            yield($item);
    }
    
    /**
     * Fetches $count elements and yields them as an array, before emitting
     * the entire source.
     */
    protected function spyIterator($source, $count = 10) {
        $buffer = [];
        foreach($source as $item) {
            if($count < 0)
                yield($item);
            else if($count > 0) {
                $buffer[] = $item;
                $count--;
            } else {
                yield($buffer);
                foreach($buffer as $bufferedItem)
                    yield($bufferedItem);
                unset($buffer);
                yield($item);
                $count--;
            }
        }
    }
    
    protected function conservativeLowerHalfIterator($source, $n, $cmp) {
        $source = $this->spyIterator($source, 5000);
        $spied = $source->current();
        $source->next();
        $split = $spied[0];
        foreach($spied as $spy) {
            if($cmp($spy, $split) < 0)
                $split = $spy;
        }
        unset($spied);

        $downCount = 10;
        $heap = [];
        $returnCount = 0;
        $a = function($item) use (&$split, &$cmp, &$heap, &$returnCount, $n) {
            if($cmp($item, $split) <= 0) {
                return $item;
            } else if($returnCount < $n) {
                // No point building the heap, when we have returned enough elements for the query
                $heap[] = $item;
            } else if($heap !== null) {
                $heap = null;
            }
        };
        while($source->valid()) {
            $item = $source->current();
            $source->next();
            $res = $a($item);
            if($res) {
                yield($res);
                $returnCount++;
            }
        }
        if($returnCount > $n) {
            yield([]);
        } else if($heap === null) {
            yield([]);
        } else {
            $t = microtime(true);
            shuffle($heap);
            yield($heap);
        }
    }
    
    protected function &arrayGenerator(&$source) {
        foreach($source as &$item)
            yield($item);
    }
    
    protected function &callbackGenerator($source) {
        if(is_array($source)) {
            $newSource = $this->arrayGenerator($source);
            $source = &$newSource;
        }
        while($source->valid() && is_callable($source->current())) {
            $cb = $source->current();
            $i = $this->skipIterator($source, 1);
            $source = $cb($i);
        }
        while($source->valid()) {
            $item = $source->current();
            yield($item);
            $source->next();
        }
    }
    
    protected function &lowerHalfIterator($source, $n, $cmp, $maxPercentage=0.01) {
        echo "half\n";
        $source = $this->callbackGenerator($source);
        $split = $source->current();
        $returnCount = 0;
        $buffer = [];
        $bufferSize = 0;
        $bufferLimit = 1000;
        $heap = [];
        $heapSize = 0;
        $heapLimit = $bufferLimit / $maxPercentage;
        while($source->valid() && $bufferSize < $bufferLimit && $heapSize < $heapLimit) {
            $item = $source->current();
            $source->next();
            if($cmp($item, $split) <= 0) {
                $buffer[] = $item;
                $bufferSize++;
            } else {
                $heap[] = $item;
                $heapSize++;
            }
        }
        $percentageReturned = $bufferSize / ($bufferSize + $heapSize);
        if($percentageReturned > $maxPercentage) {
            $f = function & (&$source) use($n, $cmp, $maxPercentage) { 
                echo "Halving one extra time\n";
                foreach($this->lowerHalfIterator($source, $n, $cmp, $maxPercentage) as &$item) 
                    yield($item); 
            };
            yield($f);
            // Must pick a new split
            usort($buffer, $cmp);
            $split = $buffer[floor($bufferSize * $maxPercentage)];
            $buffer = array_merge($buffer, $heap);
            $heap = [];
            $heapSize = 0;
        } else if($percentageReturned < $maxPercentage/2) {
            // getting too few
            usort($buffer, $cmp);
            $split = $buffer[floor($bufferSize * $maxPercentage)];
            // put heap back on buffer
            $buffer = array_merge($buffer, $heap);
            $heap = [];
            $heapSize = 0;
        } else {
            foreach($buffer as $item) {
                yield($item);
                $returnCount++;
            }
            $buffer = [];
            $bufferSize = 0;
        }
        foreach($buffer as $item) {
            if($cmp($item, $split) <= 0) {
                yield($item);
                $returnCount++;
            } else if($returnCount < $n) {
                $heap[] = $item;
            } else {
                $heap = null;
            }
        }
        
        while($source->valid()) {
            $item = $source->current();
            $source->next();
            if($cmp($item, $split) <= 0) {
                yield($item);
                $returnCount++;
            } else if($returnCount < $n) {
                $heap[] = $item;
            } else {
                $heap = null;
            }
        }
        
        if(isset($item['_heap'])) {
            die("WE HAVE HEAP");
        }
        
        if($returnCount < $n) {
            $item = &$heap[0];
            $item['_heap'] = &$heap;
            unset($heap);
            yield($item);
        }
    }
    
    protected function oldLowerHalfIterator($source, $n, $cmp, $split=null) {
        $heap = [];
        $returnCount = 0;
        $b = function($item) use (&$split, &$cmp, &$heap, &$returnCount, $n) {
            if($cmp($item, $split) <= 0) {
                return $item;
            } else if($returnCount < $n) {
                // No point building the heap, when we have returned enough elements for the query
                $heap[] = $item;
            }
        };
        
        // Function is called only the first iteration. Then replaces itself with
        // $b. Performance optimization.
        $a = function($item) use (&$split, &$a, &$b) {
            // First item is always the split
            $split = $item;
            // Replace self
            $a = $b;
            // First item is always returned - it is considered to be in the lower half
            return $item;
        };
        
        if($split) {
            $a = $b;
        }
        
        foreach($source as $item) {
            $res = $a($item);
            if($res) {
                yield($res);
                $returnCount++;
            }
        }
        if($returnCount > $n) {
//            yield([]);
        } else {
            $t = microtime(true);
            shuffle($heap);
            yield($heap);
        }
    }
    
    protected function sqliteIterator() {
        $delimiter = $this->getModelClass()::getDelimiter();
        $enclosure = $this->getModelClass()::getEnclosure();
        $escape = $this->getModelClass()::getEscape();
        $commentPrefix = $this->getModelClass()::getCommentPrefix();
        $commentPrefixLength = 0;
        if($commentPrefix !== null)
            $commentPrefixLength = strlen($commentPrefix);
        $primaryKey = $this->getModelClass()::getPrimaryKeyName();
        $cols = $this->getModelClass()::getColumns();
        $skipFirstLine = $this->getModelClass()::shouldSkipFirstLine();
        $offset = $this->getOffset();
        $limit = $this->getLimit();
        $filename = $this->getModelClass()::getFileName();

        $tempnam = tempnam(sys_get_temp_dir(), 'csvtableset');
        
        $t = microtime(true);
        
        $script = 'CREATE TABLE data ('.implode(",", $cols).');
.mode csv
.separator '.($delimiter=="'" ? '"\'"' : "'$delimiter'").'
.import '.realpath($filename).' data
';
        $res = shell_exec('echo '.escapeshellarg($script).' | /usr/bin/sqlite3 '.escapeshellarg($tempnam));
        echo microtime(true) - $t;

        $sqlite = new \SQLite3($tempnam);
        foreach($sqlite->query('SELECT * FROM data LIMIT 1') as $row) {
            var_dump("ROW", $row);
        }
        
        die("OK");
    }
    
    /**
     * Skips the first $skip items before it starts yielding
     */
    protected function &skipIterator($source, $skip) {
        foreach($source as $row)
            if($skip-- <= 0)
                yield($row);
    }
    
    /**
     * Stops yielding after $limit items
     */
    protected function limitIterator($source, $limit) {
        foreach($source as $row) {
            if($limit-- == 0)
                return;
            yield($row);
        }
    }

    protected function arrayIterator(array $array) {
        foreach($array as $row) yield($row);
    }
    
    protected function bufferedIterator($source) {
        
        //$t = microtime(true);
        $rows = [];
        foreach($source as $row)
            $rows[] = $row;
            
        //echo "Time spent buffering: ".(microtime(true)-$t)."\n";
        foreach($rows as $row)
            yield($row);
    }
    
    protected function quickSortIterator($source, $cmp) {
        $collator = Service::use(Collator::class);
//        $collator = \Nerd\Glue::get(\Collator::class);
        $buffer = [];
        foreach($source as $row) 
            $buffer[] = $row;
        //$t = microtime(true);
        usort($buffer, $cmp);
        //echo "QUICK SORT TIME: ".(microtime(true) - $t)."\n";

        foreach($buffer as $row)
            yield($row);
    }
    
    /**
     * This fetches the file, one line at a time. Filters every line according to
     * the where clauses.
     */
    protected function fileIterator() {
        $delimiter = $this->getModelClass()::getDelimiter();
        $enclosure = $this->getModelClass()::getEnclosure();
        $escape = $this->getModelClass()::getEscape();
        $commentPrefix = $this->getModelClass()::getCommentPrefix();
        $commentPrefixLength = 0;
        if($commentPrefix !== null)
            $commentPrefixLength = strlen($commentPrefix);
        $primaryKey = $this->getModelClass()::getPrimaryKeyName();
        $cols = $this->getModelClass()::getColumns();
        $skipFirstLine = $this->getModelClass()::shouldSkipFirstLine();
        $offset = $this->getOffset();
        $limit = $this->getLimit();

        $fp = fopen($this->getModelClass()::getFileName(), 'r');
        
        while($skipFirstLine && false !== ($line = fgets($fp))) {
            if($commentPrefix!==null && strpos($line, $commentPrefix)===0)
                continue;
            if(trim($line)==='')
                continue;
            $skipFirstLine = false;
        }
        
        $tester = $this->getTesterForArrays();
        $wheres = $this->_wheres;
        $c = 0;
        while(false !== ($line = fgets($fp, 4096))) {
            $c++;
            if($c % 10000 == 0) echo "$c ";
            $row = array_combine($cols, str_getcsv($line, $delimiter, $enclosure, $escape));
            foreach($wheres as $w) {
                if(!$tester[$w[1]]($row[$w[0]], $w[2]))
                    continue 2;
            }
            yield($row);
        }
    }
}
