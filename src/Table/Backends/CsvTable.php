<?php
namespace Fubber\Table\Backends;
use Fubber\Table\Table;
use Fubber\Table\Backends\Traits\ReadOnlyTable;
use Fubber\Table\ReadOnlyException;

abstract class CsvTable extends Table {
    use ReadOnlyTable;
    
    /**
     * filename
     */
    protected static $_file = null;
    
    /**
     * Field delimiter, usually ; or ,
     */
    protected static $_delimiter = null;
    
    /**
     * Field enclosure character, usually " or '
     */
    protected static $_enclosure = null;
    
    /**
     * Enclosure escape character, usually same as static::$_enclosure or \
     */
    protected static $_escape = null;
    
    /**
     * Primary key. If this is not set, the first column in $_cols is selected.
     */
    protected static $_primaryKey = null;
    
    /**
     * Column names. If this is not set, the first non-empty line is fetched
     */
    protected static $_cols = null;
    
    /**
     * This is automatically set to true, if $_cols is not set
     */
    protected static $_skipFirstLine = null;
    
    /**
     * Lines starting with this is ignored. For example # or ;
     */
    protected static $_commentPrefix = null;
    
    /**
     * Names of numeric columns
     */
    protected static $_numericCols = null;


    /**
     * CSV TABLE INTERNAL
     * 
     * Returns the declared column names for this table, excluding the primary
     * key.
     */
    public static function getColumns() {
        if(isset(static::$_internalCache[static::class]['columns']))
            return static::$_internalCache[static::class]['columns'];
        
        $cols = static::$_cols;
        if(!$cols) {
            $cols = static::getColumnsFromFile();
        }
        
        if(!is_array($cols))
            throw new \TypeError(static::class.'::$_cols must be set, or you must override '.static::class.'::getColumns().');
        
        return static::$_internalCache[static::class]['columns'] = $cols;
    }

    /**
     * CSV TABLE INTERNAL
     * 
     * Used to improve performance. Remember this cache is shared between every model.
     */
    protected static $_internalCache = [];

    
    protected static function getColumnsFromFile() {
        if(isset(static::$_internalCache[static::class]['columns_from_file']))
            return static::$_internalCache[static::class]['columns_from_file'];
        if(static::$_skipFirstLine === false)
            throw new \TypeError(static::class.'::$_skipFirstLine is false. Can\'t get columns from file.');
        static::$_skipFirstLine = true;
        $fp = fopen(static::getFileName(), 'r');
        while(false !== ($line = fgets($fp))) {
            if(static::getCommentPrefix()!==null && strpos($line, static::getCommentPrefix())===0)
                continue;
            if(trim($line)==='')
                continue;
            $cols = str_getcsv($line, static::getDelimiter(), static::getEnclosure(), static::getEscape());
            fclose($fp);
            return static::$_internalCache[static::class]['columns_from_file'] = $cols;
        }
        return static::$_internalCache[static::class]['columns_from_file'] = null;
    }
    

    public static function allUnsafe(): CsvTableSet {
        return new CsvTableSet(static::class);
    }

    /**
     * @see Table::getPrimaryKeyName()
     */
    public static function getPrimaryKeyName() {
        if(isset(static::$_internalCache[static::class]['primary_key']))
            return static::$_internalCache[static::class]['primary_key'];
        
        if(static::$_primaryKey)
            return static::$_internalCache[static::class]['primary_key'] = static::$_primaryKey;
        
        $cols = static::getColumnsFromFile();
        
        return static::$_internalCache[static::class]['primary_key'] = $cols[0];
    }
    
    public static function getFileName() {
        if(!static::$_file)
            throw new \TypeError(static::class.'::$_file must be set, or you must override '.static::class.'::getFileName().');
        return static::$_file;
    }
    
    public static function getDelimiter() {
        if(!static::$_delimiter)
            throw new \TypeError(static::class.'::$_delimiter must be set, or you must override '.static::class.'::getDelimiter().');
        return static::$_delimiter;
    }

    public static function getEnclosure() {
        if(!static::$_enclosure)
            throw new \TypeError(static::class.'::$_enclosure must be set, or you must override '.static::class.'::getEnclosure().');
        return static::$_enclosure;
    }
    
    public static function getEscape() {
        if(!static::$_escape)
            throw new \TypeError(static::class.'::$_escape must be set, or you must override '.static::class.'::getEscape().');
        return static::$_escape;
    }
    
    public static function getCommentPrefix() {
        return static::$_commentPrefix;
    }
    
    public static function getNumericCols() {
        return static::$_numericCols ? static::$_numericCols : [];
    }
    
    public static function shouldSkipFirstLine() {
        if(static::$_skipFirstLine === null)
            throw new \TypeError(static::class.'::$_skipFirstLine is null.');
        return static::$_skipFirstLine;
    }
}
