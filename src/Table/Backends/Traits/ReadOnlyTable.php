<?php
namespace Fubber\Table\Backends\Traits;
use Fubber\Table\ReadOnlyException;

trait ReadOnlyTable {
    public function delete() {
        throw new ReadOnlyException();
    }
    public function save(array $columns = null) {
        throw new ReadOnlyException();
    }
}