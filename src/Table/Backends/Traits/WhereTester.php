<?php
namespace Fubber\Table\Backends\Traits;

use Collator;
use Fubber\Service;

trait WhereTester {
    public function getTesterForArrays() {
        $collator = Service::use(Collator::class);
//        $collator = \Nerd\Glue::get(\Collator::class);
        return [
            '>' => function($a, $b) use ($collator) {
                if(is_string($a)) {
                    return $collator->compare($a, $b) > 0;
                } else {
                    return $a > $b;
                }
            },
            '<' => function($a, $b) use ($collator) {
                if(is_string($a)) {
                    return $collator->compare($a, $b) < 0;
                } else {
                    return $a < $b;
                }
            },
            '>=' => function($a, $b) use ($collator) {
                if(is_string($a)) {
                    return $collator->compare($a, $b) >= 0;
                } else {
                    return $a >= $b;
                }
            },
            '<=' => function($a, $b) use ($collator) {
                if(is_string($a)) {
                    return $collator->compare($a, $b) <= 0;
                } else {
                    return $a <= $b;
                }
            },
            '<>' => function($a, $b) use ($collator) {
                if(is_string($a)) {
                    return $collator->compare($a, $b) != 0;
                } else {
                    return $a != $b;
                }
            },
            '=' => function($a, $b) use ($collator) {
                if(is_string($a)) {
                    return $collator->compare($a, $b) == 0;
                } else {
                    return $a == $b;
                }
            },
            'IN' => function($a, array $b) {}
            ];
    }

    public function testObject($row) {
        
    }
}
