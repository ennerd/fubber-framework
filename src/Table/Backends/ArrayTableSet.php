<?php
namespace Fubber\Table\Backends;

use Closure;
use Opis\Closure\SerializableClosure;
use Fubber\Table\TableSet;
use InvalidArgumentException;
use Traversable;

use function array_column;

class ArrayTableSet extends TableSet {

    public function __construct(string $className, array $rows, array $columnNames) {
        parent::__construct($className);

        $length = sizeof($rows);

        // Check that we've got a simple sequential array
        for($i = 0; $i < $length; $i++) {
            if (!isset($rows[$i])) {
                throw new InvalidArgumentException(static::class." data must be an array with numerical indexes in sequence starting from 0");
            }
            if (!is_object($rows[$i])) {
                throw new InvalidArgumentException(static::class." data must be an array of objects");
            }
        }
        $this->_extra['rows'] = $rows;
        $this->_extra['columnNames'] = $columnNames;
    }

    /**
     * *** METHODS ACCORDING TO ITableSet ***
     */
    public function getColumnNames(): array {
        return $this->_extra['columnNames'];
    }

    public function getColumnValues($column) {
        if (!in_array($column, $this->getColumnNames())) {
            $columnNames = implode(", ", $this->getColumnNames());
            throw new InvalidArgumentException("Unknown column name '$column', available column names are ".$columnNames);
        }

        return array_column($this->rows, $column);
    }

    public function count(): int {
        $result = $this->doWheres($this->_extra['rows']);
        return count($result);
    }

    protected function doWheres(array $records): array {
        $result = [];
        foreach ($records as $key => $record) {
            if (!is_object($record)) {
                $record = (object) $record;
            }
            foreach ($this->_wheres as $spec) {
                list ($prop, $op, $val) = $spec;

                if (!isset($record->$prop)) {
                    continue 2;
                }

                switch ($op) {
                    case '=' :
                        if (!($record->$prop == $val)) {
                            continue 3;
                        }
                        break;
                    case '==' :
                        if (!($record->$prop === $val)) {
                            continue 3;
                        }
                        break;
                    case '>' :
                        if (!($record->$prop > $val)) {
                            continue 3;
                        }
                        break;
                    case '>=' :
                        if (!($record->$prop >= $val)) {
                            continue 3;
                        }
                        break;
                    case '<' :
                        if (!($record->$prop < $val)) {
                            continue 3;
                        }
                        break;
                    case '<=' :
                        if (!($record->$prop <= $val)) {
                            continue 3;
                        }
                        break;
                    case '!=' :
                    case '<>' :
                        if (!($record->$prop != $val)) {
                            continue 3;
                        }
                        break;
                    case '!==' :
                        if (!($record->$prop !== $val)) {
                            continue 3;
                        }
                        break;
                    case '>' :
                        if (!($record->$prop > $val)) {
                            continue 3;
                        }
                        break;
                }
            }
            $result[] = $records[$key];
        }
        return $result;
    }

    protected function doOrders(array $records): array {
        if ($this->_order !== null) {
            $prop = $this->_order;
            if ($this->_orderDesc) {
                // Descending
                usort($records, function ($a, $b) use ($prop) {
                    return $a->$prop > $b->$prop ? -1 : ($a->$prop === $b->$prop ? 0 : 1);
                });
            } else {
                // Ascending
                usort($records, function ($a, $b) use ($prop) {
                    return $a->$prop < $b->$prop ? -1 : ($a->$prop === $b->$prop ? 0 : 1);
                });
            }
        }
        return $records;
    }

    protected function doLimits(array $records): array {
        return array_slice($records, $this->_offset, $this->_limit);
    }

    public function getIterator(): Traversable {
        $result = $this->doWheres($this->_extra['rows']);
        $result = $this->doOrders($result);
        $result = $this->doLimits($result);
        return new \ArrayIterator($result);
    }
}
