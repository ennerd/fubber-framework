<?php
namespace Fubber\Table\Backends;

use Fubber\Table\ITableSet;
use Fubber\Table\IBasicTableSet;
use Fubber\Table\TableSet;
use Fubber\CodingErrorException;
use Traversable;

class DbTableSet extends TableSet {
    protected static $tableAliasIndex = 0;

    /**
     * *** METHODS ACCORDING TO ITableSet ***
     */
    public function getColumnNames(): array {
        return ($this->getModelClass())::getColumnNames();
    }

    protected function quoteIdentifier(string $identifier) {
        if (strpos($identifier, '(') !== false) {
            return $identifier;
        }
        return "`$identifier`";
        return $this->getModelClass()::getDb()->quote($identifier);
    }

    public function getColumnValues(string $column) {
        $cols = $this->getColumns();
        $this->column($column);
        $sql = $this->buildSqlQuery();
        $this->columns($cols);
        return ($this->getModelClass())::getDB()->queryColumn($sql[0], $sql[1]);
    }

    public function getIterator(): Traversable {
        $sql = $this->buildSqlQuery();
        $res = ($this->getModelClass())::getDb()->query($sql[0], $sql[1], $this->getModelClass());
        return (function() use ($res) {
            foreach ($res as $row) {
                $row->_shared['fromDb'] = true;
                yield $row;
            }
        })();

        array_walk($res, function (&$row) {
            $row->_shared['fromDb'] = true;
        });
        return new \ArrayIterator($res);
    }

    public function complexWhere(string $sql, $vars=[]): ITableSet {
        $this->_extra['complexWheres'][] = [ $sql, $vars ];
        return $this;
    }

    public function count(): int {
        $className = $this->getModelClass();
        $sql = $this->buildSqlQuery([
            'columns' => ['COUNT(1)'],
            'limit' => null,
            'offset' => null,
        ]);
        return intval($className::getDb()->queryField($sql[0], $sql[1]));
    }

    /**
     * *** NOT IN INTERFACE ***
     */
    public function getSqlQuery(): array {
        return $this->buildSqlQuery();
    }

/*
    protected function buildSqlQuery(array $opts=[]) {
        $res = $this->buildSqlQuery2($opts);
        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/query.log', 'a');
        fwrite($fp, $res[0]."\n");
        return $res;
    }
*/
    /**
     * Undocumented function
     *
     * @param array $opts
     * @return array{0: string, 1: array}
     */
    protected function buildSqlQuery(array $opts=[]): array {
        $className = $this->getModelClass();

        $tableName = '_t'.static::$tableAliasIndex++;
        $tableNameMap = [$className::getTableName() => $tableName];

        $sql = 'SELECT';

        foreach ($opts as $optName => $optValue) {
            if (is_callable($optValue)) {
                $opts[$optName] = $optValue($tableName, $this);
            }
            switch ($optName) {
                case 'columns' : // array|string
                    if (is_iterable($optValue)) {
                        $opts['columns'] = implode(",", array_map(\Closure::fromCallable([$this, 'quoteIdentifier']), $opts['columns']));
                    } else if (!is_string($optValue)) {
                        throw new CodingErrorException("DbTableSet::buildSqlQuery option 'columns' must be a string or an array");
                    }
                    break;
                case 'limit' :
                case 'offset' :
                    if ($optValue !== null && !is_int($optValue)) {
                        throw new CodingErrorException("DbTableSet::buildSqlQuery option '$optName' must be an int or null");
                    }
                    break;
                default :
                    throw new CodingErrorException("DbTableSet::buildSqlQuery option '$optName' not supported");
            }
        }
        if (!empty($opts['columns'])) {
            $sql .= ' '.$opts['columns'];
        } else {
            if (!$this->getColumns()) {
                $sql .= ' '.$this->quoteIdentifier($tableName).'.*';
            } else {
                $sql .= ' '.$this->quoteIdentifier($tableName).'.'.implode(','.$this->quoteIdentifier($tableName).'.', array_map(\Closure::fromCallable([$this, 'quoteIdentifier']), $this->getColumns()));
            }
        }

        $sql .= ' FROM '.$this->quoteIdentifier($className::getTableName()).' AS '.$this->quoteIdentifier($tableName);

        $values = [];
        $wheres = $this->getWheres();
        if (!empty($this->_extra['complexWheres'])) {
            foreach ($this->_extra['complexWheres'] as $cw) {
                $wheres[] = [ 'complex' => $cw ];
            }
        }

        if (!empty($wheres)) {
            $sql .= ' WHERE '.implode(" AND ", array_map(function ($where) use ($tableName, &$values, $tableNameMap) {
                if (isset($where['complex'])) {
                    // Quote tablenames and column names
                    $part = $where['complex'][0];
                    $fixes = [];
                    foreach ($this->getColumnNames() as $col) {
                        $part = preg_replace('|`?(?<!\.)\b'.preg_quote($col).'\b`?|', $this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($col), $part);
                        $fixes['.'.$tableName.'.`'.$col.'`'] = '.`'.$col.'`';
                    }
                    foreach ($tableNameMap as $table => $alias) {
                        $part = preg_replace('|`?'.preg_quote($table).'`?\.|', $this->quoteIdentifier($alias).'.', $part);
                    }
                    $part = strtr($part, $fixes);
                    foreach($where['complex'][1] as $value) {
                        $values[] = $value;
                    }
                    return '('.$part.')';
                } elseif ($where[1]==='IN') {

                    /**
                     * When using ->where('something', 'IN', $tableSet)
                     */
                    if ($where[2] instanceof IBasicTableSet) {
                        if (sizeof($where[2]->getColumns()) === 1) {
                            $where[2] = $where[2]->getColumnValues($where[2]->getColumns()[0]);
                        } else {
                            $pk = $where[2]->getModelClass()::getPrimaryKeyName();
                            $where[2] = $where[2]->getColumnValues($pk);
                        }
                    }
                    if (is_array($where[2])) {
                        foreach ($where[2] as $w) {
                            $values[] = $w;
                        }
                        return $this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($where[0]).' IN ('.substr(str_repeat('?,', sizeof($where[2])), 0, -1).')';
                    } else {
                        $values[] = $where[2];
                        return $this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($where[0]).' '.$where[1].' ?';
                    }
                } else {
                    if ($where[2] === null) {
                        if ($where[1] == '=') {
                            return $this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($where[0]).' IS NULL';
                        }
                        if ($where[1] == '<>') {
                            return $this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($where[0]).' IS NOT NULL';
                        }
                    }
                    $values[] = $where[2];
                    return $this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($where[0]).' '.$where[1].' ?';
                }
            }, $wheres));
        }
        if ($this->getOrder()) {
            $sql .= ' ORDER BY '.$this->quoteIdentifier($tableName).'.'.$this->quoteIdentifier($this->getOrder()).($this->isOrderDesc() ? ' DESC' : '');
        }

        $offset = array_key_exists('offset', $opts) ? $opts['offset'] : $this->getOffset();
        $limit = array_key_exists('limit', $opts) ? $opts['limit'] : $this->getLimit();

        if ($offset !== null && $limit !== null) {
            $sql .= ' LIMIT '.$offset.','.$limit;
        } elseif ($offset !== null) {
            $sql .= ' LIMIT '.$offset.',1000';
        } elseif ($limit !== null) {
            $sql .= ' LIMIT '.$limit;
        } else {
            // no limit clause
        }

        return [$sql, $values];
    }
}
