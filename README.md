Fubber Framework
================

Fubber Framework is a simplistic small PHP framework that focuses on performance and scalability. It keeps
the framework small, and uses lazy loading for most features. It provides:

* An advanced Model implementation with an SQL-inspired query language that can work on any tabular backend.
* Abstraction layer for Config, Caching and Session - which allows you to integrate Fubber Framework in any
  existing website. Simply map the appropriate Config, Caching and Session handling to your own implementation.
* A turn-key Model-View-Controller regime that you'll be up and running with immediately after checkout.

Special Thank You!
------------------

I want to thank www.browserstack.com for sponsoring me with their service. It allow
me to test the upcoming UI-functionality in a multitude of browsers on various devices!