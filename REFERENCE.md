Fubber Framework Reference Cheat Sheet
======================================

State
-----

The `State` class is a central component of the Fubber Framework. 
The State object is created for every new request to the application, and
encapsulates the user request for the entire life cycle of that request.
This means that the object instance will act as a form of "identity card"
for the request and it will be passed around throughout the various APIs
you are using. This allows each API to automatically perform perform access
control, optimize the cache headers and more for the response.

> This is one of the key enabling technologies that allow us to place access
> control as near to the data as possible. For example, by implementing access
> control inside the Table API, your controller functions that utilize the
> Table API will inherently be more secure.
> 
> We still do recommend you implement additional access control inside your
> controller code as well. When you need to access data without restriction,
> the convention is that another version of the same function is available
> with the `Unsafe` suffix. For example `User::load($id)` becomes
> `User::loadUnsafe($id)` when you need to be able to load user account data
> that the currently logged in user does not have access to.

You would normally not be creating new instances of the `State`
class yourself. Instead, it will be passed to your controller if you add
an argument that type-hints `State` OR if the *first* argument
is named `$state`.

```
// Access the Psr\Http\Message\ServerRequestInterface
$state->request;

// Access the Psr\Http\Message\ResponseInterface
$state->response;

// Access the `Fubber\Session\ISession` interface
$state->session;
```


Managing HTTP caching headers
-----------------------------

Caching constraints can only be reduced. This means that whenever an API
is used which has access to the `$state` instance, that API may restrict
the caching constraints somewhat.

For example, if you access the `$state->session` API, the caching will
automatically be configured to `private`. If you destroy the session
via `$state->session->destroy()` the page will automatically not be cached,
because the `Fubber\Session` object automatically calls
`$state->response->noCache()`.

```
$state->response->publicCache(30);
// public, max-age=30

$state->request->privateCache(60);
// private, max-age=30

$state->request->privateCache(3600);
// private, max-age=30

$state->request->noCache();
// no-cache, no-store

$state->request->publicCache(86400);
// no-cache, no-store
```


Object and value cache
----------------------

The caching backend can easily be replaced by configuring a class alias
for `Fubber\Cache`. There is a selection of backends available in the
`src/Cache/` path. The backend must implement `Fubber\Cache\ICache`.

Accessing the `Fubber\Cache\ICache`:

  * `$cache = \Fubber\Cache::create()` or
  * `$cache = \Fubber\Service::use(\Fubber\Cache\ICache::class);`

```php
// Access the cache
$cache = \Fubber\Cache::create()

// Cache a value
$cache->set('cache-key', [ 'cached' => 'value' ], 60);

// Retrieve a value
$cache->get('cache-key');

// Delete a value
$cache->set('cache-key', null);
```


Logging
-------

The logging backend can easily be replaced by configuring a class alias for
`Fubber\Logger`. The backend must implement `Fubber\Logger\ILogger`, which
extends the `Psr\Log\LoggerInterface`.

Accessing the `Fubber\Logger\ILogger`:

  * `$logger = \Fubber\Logger::create()` or
  * `$logger = \Fubber\Service::use(\Fubber\Logger\ILogger::class);`

```php
$logger = \Fubber\Logger::create();
/**
 * Action must be taken immediately.
 *
 * Example: Entire website down, database unavailable, etc. This should
 * trigger the SMS alerts and wake you up.
 */
$logger->emergency('This is an {criticality}', ['criticality' => 'emergency']);

/**
 * Action must be taken immediately.
 *
 * Example: Entire website down, database unavailable, etc. This should
 * trigger the SMS alerts and wake you up.
 */
$logger->alert('This is an {criticality}', ['criticality' => 'alert']);

/**
 * Critical conditions.
 *
 * Example: Application component unavailable, unexpected exception.
 */
$logger->critical('This is an {criticality}', ['criticality' => 'critical']);

/**
 * Runtime errors that do not require immediate action but should typically
 * be logged and monitored.
 */
$logger->error('This is an {criticality}', ['criticality' => 'error']);

/**
 * Exceptional occurrences that are not errors.
 *
 * Example: Use of deprecated APIs, poor use of an API, undesirable things
 * that are not necessarily wrong.
 */
$logger->warning('This is an {criticality}', ['criticality' => 'warning']);

/**
 * Normal but significant events.
 */
$logger->notice('This is an {criticality}', ['criticality' => 'notice']);

/**
 * Interesting events. Example: User logs in, SQL logs.
 */
$logger->info('This is an {criticality}', ['criticality' => 'info']);

/**
 * Detailed debug information.
 */
$logger->debug('This is an {criticality}', ['criticality' => 'debug']);

/**
 * Logs with an arbitrary level.
 */
$logger->log(\Psr\Log\LogLevel::CRITICAL, 'This is an {criticality}', ['criticality' => \Psr\Log\LogLevel::CRITICAL]);
```


Internationalization
--------------------

Translations are stored in the database table `f_i18n_trans`.

The translation backend can easily be replaced by configuring a different class
alias for `Fubber\I18n`. The backend must implement `Fubber\I18n\II18n`.

The translation system will inspect the `Accept-Language` header, and automatically
add 'Vary: Accept-Language' to your response.

Accessing the `Fubber\I18n\II18n`:

  * `$i18n = \Fubber\I18n::frodee()` or
  * `$cache = \Fubber\Service::use(\Fubber\Cache\ICache::class);`

```
// Most common usage:
$state->t(':num bottles of beer on the wall', [ 'num' => 99 ]);

// Access the underlying engine
$toNorwegian = new Fubber\I18n('nb-NO');
$toNorwegian->t(':num bottles of beer on the wall', [ 'num' => 99 ], __FILE__);
```


E-mail sending
--------------

The mailer backend can easily be replaced by configuring a different class alias
for `Fubber\Mailer`. The mailer must implement `Fubber\Mailer\IMailer`.

```
// Create the mail
$mail = new Fubber\Mail('some/view/template', ['some' => 'variables']);

// Send the mail using the default engine
$mail->send('you@example.com', 'John Doe');

// Send using specific engine
Fubber\Mailer\SwiftMailer::send($mail);
```


Database Abstraction
--------------------

`Fubber\Db\IDb` is an interface with a simplified interface for querying
the database. It is powered by `PDO` normally.

The database backend can easily be replaced by configuring a different class alias
for `Fubber\Db`. Normally, you would instead configure a different service for the
`PDO` service id.

```
// Access the database
$db = Fubber\Db::create();

// Normal SELECT query into `User` object instances (default is `stdClass`)
foreach ($db->query('SELECT * FROM users WHERE gender=?', [ 'female' ], User::class) as $user) {
    // $user is an instance of User
}

// Normal UPDATE/DELETE/CREATE queries
if ($db->exec('DELETE FROM users WHERE gender=?', [ 'male' ])) {
    // successful
}

// Select a single field of the first row
$name = $db->queryField('SELECT name FROM users LIMIT 1');

// Select a single row
$row = $db->queryOne('SELECT * FROM users WHERE id > ? LIMIT 1', [ 100 ]);
```


Templating Engine
-----------------

The templating engine can easily be replaced by configuring a different class alias
for `Fubber\Template`. The templating engine must implement `Fubber\Template\ITemplate`.

The Blade template engine is available in the composer package `ennerd/fubber-blade`.

```
// Prepare a template
$template = Fubber\Template::create('template/name', ['some' => 'vars']);

// Render the template
echo $template;
```


User Session Management
-----------------------

Session data is by default stored in the default `Fubber\Cache` API. The session
storage backend can easily be replaced by configuring a different class alias for
`Fubber\Session` that implements the `Fubber\Session\ISession` interface.

```
// Access the session
$session = Fubber\Session::getCurrent($state);

// Store a session value
$session->some_value = "This is stored";

// Retrieve a session value
$value = $session->some_value
```


Hooks
-----

Hooks are used to run certain code on specific events, or to modify inputs and results
from certain functions.

Hooks are identified by a unique string, and you attach a callback with an optional priority
to that string.

Some conventions exist for declaring hook identifier strings:

  * Filter the return value from a method: __METHOD__.'()' (magic constant with parentheses
    appended, example: 'SomeClass::someMethod()'.
    
    ```
    // Filterable return values are implemented like this:
    return \Fubber\Hooks::filter(__METHOD.'(), $returnValue[, $state, $and, $other, $vars]);

    // Filtering a return value is done like this:
    \Fubber\Hooks::listen('ClassName::methodName()', function ($returnValue, $state) {
        return str_replace('foo', 'bar', $returnValue);
    });
    ```

  * Filter a parameter for a method: __METHOD__ (magic constant without parentheses, example:
    'SomeClass::someMethod'.

    ```
    // Filterable parameters are implemented like this:
    function myFunction($a, $b, $c) {
        extract(\Fubber\Hooks::filter(__METHOD__, func_get_args()), \EXTR_IF_EXISTS);
        // $a, $b, $c may have been overridden
    }

    // Creating a filter:
    \Fubber\Hooks::listen('myFunction', function($args) {
        if (!is_bool($args[0])) {
            $args[0] = 
        }
    });
    ```




A few conventions have emerged:

* Modify the output of a function
