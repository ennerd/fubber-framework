<?php

use Fubber\Blade\BladeTemplateEngine;
use Fubber\ConfigErrorException;
use Fubber\ExtensionRequiredException;
use Fubber\Kernel;
use Fubber\Kernel\Container\Factory;
use Fubber\Kernel\Container\GeneratorFactory;
use Fubber\Kernel\Container\Manifest;
use Fubber\Kernel\Container\Scope;
use Fubber\Kernel\Debug\Describe;
use Fubber\Mailer;
use Fubber\Service;
use Fubber\TemplateEngineInterface;
use Fubber\TemplateInterface;

return [
    \Collator::class => function () {
        $collator = new \Collator(str_replace("_POSIX", "", \Locale::getDefault()));
        $collator->setStrength(\Collator::PRIMARY);
        $collator->setAttribute(\Collator::NUMERIC_COLLATION, \Collator::ON);
        return $collator;
    },
    \Memcached::class => function () {
        static $instance, $myPid;
        if ($myPid === null || $myPid !== getmypid()) {
            $instance = null;
            $myPid = getmypid();
        }
        if ($instance) {
            return $instance;
        }
        if (!class_exists("\Memcached")) {
            throw new ExtensionRequiredException("ext-memcached is not installed");
        }

        // If running in Google App Engine, memcache is automatically configured
        if (
            strpos(Kernel::$instance->env->get('SERVER_SOFTWARE') ?? '', "Google App Engine")!==false ||
            strpos(Kernel::$instance->env->get('SERVER_SOFTWARE') ?? '', "Devel")!==false
        ) {
            return $instance = new \Memcached();
        } else {
            $configFile = Kernel::$instance->getConfigPath("memcache.php");
            $config = include($configFile);
            $memcached = new \Memcached();
            foreach ($config as $mc) {
                $memcached->addServer($mc['host'], $mc['port'], $mc['weight']);
            }
        }
        return $instance = $memcached;
    },
    \Fubber\Db\IDb::class => new Factory(function(\PDO $pdo) {
        return new \Fubber\Db\PdoDb($pdo);
    }),
    /*
    \Fubber\Db\IDb::class => function (\PDO $pdo) {
        var_dump($pdo);
        die("GOT PDO");
        static $instance, $myPid;
        if ($myPid === null || $myPid !== getmypid()) {
            $instance = null;
            $myPid = getmypid();
        }
        if (!$instance) {
            $pdo = Kernel::$instance->get(\PDO::class);

            $instance = new \Fubber\Db\PdoDb(Service::use(\PDO::class));
//            $result = new \Fubber\Db\PdoDb(\Nerd\Glue::get(\PDO::class));
        }
        return $instance;
    },
    */
    TemplateEngineInterface::class => function(Kernel $kernel) {
        return $kernel->template;
    },

    \Fubber\Mailer\IMailer::class => function() {
        static $instance;
        if ($instance) {
            return $instance;
        }
        return $instance = new Mailer();
    },

    \Fubber\Caching\ICache::class => function() {
        static $instance;
        if ($instance) {
            return $instance;
        }
        return $instance = \Fubber\Cache::create();
    },
    
    \PDO::class => function () {
        static $instance, $myPid;

        /**
         * Avoid reusing the PDO connection if the process is a child/fork
         */
        if ($myPid === null || $myPid !== getmypid()) {
            if ($instance === null) {
                Kernel::debug("PDO CREATION: ".Describe::stackTrace());
            }
            $instance = null;
            $myPid = getmypid();
        }
        if ($instance) {
            return $instance;
        }

        if ($value = Kernel::$instance->env->get('DATABASE_URL')) {
            $url = parse_url($value);
            $dns = '';
            if (!isset($url['scheme'])) {
                throw (new ConfigErrorException('Invalid DATABASE_URL config (missing scheme)'))
                    ->withSuggestion("Check your .env file or update your environment with DATABASE_URL=mysql://username:password@hostname:portnumber/dbname");
            }

            try {
                switch ($url['scheme']) {
                    case 'mysql':
                        $dsn = 'mysql:';
                        if (!isset($url['host'])) {
                            throw new ConfigErrorException('Invalid DATABASE_URL config (missing hostname)');
                        }
                        $dsn .= 'host='.$url['host'];
                        if (isset($url['path'])) {
                            $path = explode("/", $url['path']);
                            $dsn .= ';dbname='.$path[1];
                        }
                        $user = isset($url['user']) ? $url['user'] : null;
                        $pass = isset($url['pass']) ? $url['pass'] : null;
                        $db = new \PDO($dsn, $user, $pass);
                        $db->exec('SET NAMES utf8mb4');
                        $db->exec("SET time_zone = '".date("P")."'");
                        break;
                    default:
                        throw new ConfigErrorException('Unsupported database engine "'.$url['scheme'].'".');
                }
                $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $db->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);
                $db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
                return $instance = $db;
            } catch (\PDOException $e) {
                throw new ConfigErrorException('Unable to connect to database using DATABASE_URL ('.$e->getMessage().').');
            }
        } else {
            throw (new ConfigErrorException("No DATABASE_URL environment variable configured"))
                ->withSuggestion("Create or edit your .env file and add DATABASE_URL=mysql://username:password@hostname/dbname");

        }
    },
];
