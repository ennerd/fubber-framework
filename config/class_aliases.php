<?php return [
    /**
     * ORM implementation
     */
    \Fubber\Table::class => \Fubber\Table\Backends\DbTable::class,
    \Fubber\TableSet::class => \Fubber\Table\Backends\DbTableSet::class,
//    \Fubber\Table::class => \Fubber\Db\DbTable::class,
//    \Fubber\TableSet::class => \Fubber\Db\DbTableSet::class,
    \Fubber\EmptyTableSet::class => \Fubber\Table\EmptyTableSet::class,

    /**
     * State object for storing request specific state
     */
    \State::class => \Fubber\Kernel\State::class,

    /**
     * Session implementation
     */
    \Fubber\Session::class => \Fubber\Session\CacheSession::class,

    /**
     * Caching implementation
     */
    \Fubber\Cache::class => \Fubber\Caching\DbCache::class,

    /**
     * Internationalization implementation
     */
    \Fubber\I18n::class => \Fubber\I18n\I18n::class,

    /**
     * Simple database abstraction
     */
    \Fubber\Db::class => \Fubber\Db\PdoDb::class,

    /**
     * Logging implementation
     */
    \Fubber\Logger::class => \Fubber\Logging\MonologLogger::class,

    /**
     * E-mail sending
     */
    \Fubber\Mail::class => \Fubber\Mailer\Mail::class,
    \Fubber\Mailer::class => \Fubber\Mailer\SwiftMailer::class,

    /**
     * Other services
     */
    \Fubber\Service::class => \Fubber\Services\Service::class,

    /**
     * For compatability
     */
    \Fubber\FlashMessage::class => \Fubber\Util\FlashMessage::class,
    ];
